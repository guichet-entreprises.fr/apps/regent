# Génération du numéro de liasse

## DESCRIPTION

Une liasse est un identifiant unique de référence dossier sous forme de chaîne de caractères alphanumérique à destination des autorités compétentes et est générée à partir d'un préfixe unique "H1000" suivi d'un nombre aléatoire situé entre 0 et 9 000 000.

Cette liasse est notamment présente dans les fichiers XML Regent générés par la brique technique "REGENT-WS"

## EXEMPLES
	H10001583815
	H10008695350
