# Changelog
Ce fichier contient les modifications techniques du module.

- Projet : [Regent](https://tools.projet-ge.fr/gitlab/apps/regent)
-
---

## [2.14.1.0] - 2020-07-31
MINE-1253 : Générer un numéro de liasse pour FORMS2

###  Ajout

  __Properties__ :

Concerne uniquement l'application regent-ws

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| application.properties | jndi.db.datasource | Nom de la JNDI de la base ge_regent | jdbc/ge_regent |


## [2.14.1.0] - 2020-03-06
MINE-1253 : Générer un numéro de liasse pour FORMS2

###  Ajout

  __Properties__ :

Concerne uniquement l'application regent-ws

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| application.properties | jndi.db.datasource | Nom de la JNDI de la base ge_technique | jdbc/ge_technique |


## [2.13.5.6] - 2020-02-28

MINE-1237 : Génération du XML-TC en fonction du type d'authorité  .


---
## [2.11.2.0] - 2019-05-03

- Projet : [Regent](https://tools.projet-ge.fr/gitlab/apps/regent)

###  Ajout

- __Nouveau fichier__ : _Changelog.md_


- __Properties__ :

N/A

---
---
## Liens

[Regent](https://tools.projet-ge.fr/gitlab/apps/regent)