<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016) This software is a computer program whose purpose is to maintain and administrate standalone forms. This software is governed 
    by the CeCILL license under French law and abiding by the rules of distribution of free software. You can use, modify and/ or redistribute the software under the terms of the CeCILL license as circulated 
    by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided 
    only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability. In this respect, the user's attention is drawn to the 
    risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, 
    and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability 
    as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security. The fact 
    that you are presently reading this means that you have had knowledge of the CeCILL license and that you accept its terms. -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>fr.ge.core</groupId>
        <artifactId>java-pom</artifactId>
        <version>2.3.9.1</version>
        <relativePath>../..</relativePath>
    </parent>

    <groupId>fr.ge.common.regent</groupId>
    <artifactId>regent</artifactId>
    <version>2.15.1.2-SNAPSHOT</version>
    <packaging>pom</packaging>

    <name>Regent</name>

    <scm>
        <connection>scm:git:https://gitlab.com/guichet-entreprises.fr/apps/regent.git</connection>
        <developerConnection>scm:git:https://gitlab.com/guichet-entreprises.fr/apps/regent.git</developerConnection>
        <url>https://gitlab.com/guichet-entreprises.fr/apps/regent</url>
        <tag>head</tag>
    </scm>

    <properties>
        <webapp-target-dir>${project.build.directory}/${project.build.finalName}</webapp-target-dir>
        <common-utils.version>2.13.3.0</common-utils.version>
        <layout-gp.version>2.4.5.1</layout-gp.version>
        <xml-apis.version>1.4.01</xml-apis.version>
        <hibernate-entitymanager.version>4.2.8.Final</hibernate-entitymanager.version>
		<referentiel-supra.version>2.5.4.0</referentiel-supra.version>
        <ge-i18n.version>2.3.9.1</ge-i18n.version>
    </properties>

    <developers>
        <developer>
            <id>ccougourdan</id>
            <name>Christian Cougourdan</name>
            <email>christian.cougourdan AT capgemini.com</email>
            <organization>Capgemini</organization>
        </developer>
        <developer>
            <id>absibiss</id>
            <name>Adil Bsibiss</name>
            <email>adil.bsibiss AT capgemini.com</email>
            <organization>Capgemini</organization>
        </developer>
        <developer>
            <id>hhichri</id>
            <name>Houda Hichri</name>
            <email>houda.hichri AT capgemini.com</email>
            <organization>Capgemini</organization>
        </developer>
        <developer>
            <id>aolubi</id>
            <name>Ademola Olubi</name>
            <email>ademola.olubi AT capgemini.com</email>
            <organization>Capgemini</organization>
        </developer>
        <developer>
            <id>abouadard</id>
            <name>Arnaud Bouadard</name>
            <email>arnaud.bouadard AT capgemini.com</email>
            <organization>Capgemini</organization>
        </developer>
    </developers>

    <distributionManagement>
        <site>
            <id>iot-channel</id>
            <url>http://pic.projet-ge.fr/iot-channel</url>
        </site>
    </distributionManagement>


    <modules>
        <module>modules/regent-core</module>
        <module>modules/regent-data-services</module>
        <module>modules/regent-webservices/modules/regent-ws-contract</module>
        <module>modules/regent-webservices/modules/regent-ws-server</module>
    </modules>

    <dependencyManagement>
        <dependencies>
	        <dependency>
	            <groupId>fr.ge.common.i18n</groupId>
	            <artifactId>ge-i18n-thymeleaf3</artifactId>
                <version>${ge-i18n.version}</version>
	        </dependency>
			<dependency>
				<groupId>fr.ge.common.referentiel</groupId>
				<artifactId>referentiel-supra-services</artifactId>
				<version>${referentiel-supra.version}</version>
			</dependency>
			<dependency>
				<groupId>fr.ge.common.referentiel</groupId>
				<artifactId>referentiel-supra-services</artifactId>
				<version>${referentiel-supra.version}</version>
				<type>test-jar</type>
			</dependency>
            <dependency>
                <groupId>fr.ge.common.regent</groupId>
                <artifactId>regent-core</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>fr.ge.common.regent</groupId>
                <artifactId>regent-ws-contract</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>fr.ge.common.regent</groupId>
                <artifactId>regent-ws-server</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>fr.ge.common.layouts</groupId>
                <artifactId>layout-gp</artifactId>
                <version>${layout-gp.version}</version>
            </dependency>
            <dependency>
                <groupId>fr.ge.common.utils</groupId>
                <artifactId>common-utils</artifactId>
                <version>${common-utils.version}</version>
            </dependency>
            <dependency>
                <groupId>fr.ge.common.utils</groupId>
                <artifactId>common-web-utils</artifactId>
                <version>${common-utils.version}</version>
            </dependency>
            <dependency>
                <groupId>fr.ge.common.utils</groupId>
                <artifactId>common-web-search-utils</artifactId>
                <version>${common-utils.version}</version>
            </dependency>
            <dependency>
                <groupId>fr.ge.common.utils</groupId>
                <artifactId>common-test-utils</artifactId>
                <version>${common-utils.version}</version>
            </dependency>
            <dependency>
                <groupId>com.googlecode.json-simple</groupId>
                <artifactId>json-simple</artifactId>
                <version>${json-simple.version}</version>
            </dependency>
			<dependency>
		        <groupId>xml-apis</groupId>
		        <artifactId>xml-apis</artifactId>
                <version>${xml-apis.version}</version>
		    </dependency>
			<dependency>
				<groupId>org.hibernate</groupId>
				<artifactId>hibernate-entitymanager</artifactId>
                <version>${hibernate-entitymanager.version}</version>
			</dependency>
			<dependency>
				<groupId>com.fasterxml.jackson.jaxrs</groupId>
				<artifactId>jackson-jaxrs-json-provider</artifactId>
                <version>${jackson.version}</version>
			</dependency>
			<dependency>
			    <groupId>com.fasterxml.jackson.dataformat</groupId>
			    <artifactId>jackson-dataformat-xml</artifactId>
                <version>${jackson.version}</version>
			</dependency>
			<dependency>
			    <groupId>org.json</groupId>
			    <artifactId>json</artifactId>
			    <version>20160212</version>
			</dependency>
			<dependency>
				<groupId>fr.ge.common.regent</groupId>
				<artifactId>regent-data-services</artifactId>
				<version>${project.version}</version>
			</dependency>
			<dependency>
				<groupId>fr.ge.common.regent</groupId>
				<artifactId>regent-data-services</artifactId>
				<version>${project.version}</version>
				<type>tar.gz</type>
				<classifier>bdd</classifier>
			</dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <plugins>
            <plugin>
                <groupId>com.github.jeluard</groupId>
                <artifactId>plantuml-maven-plugin</artifactId>
                <configuration>
                    <sourceFiles>
                        <directory>${basedir}/src/site/uml</directory>
                        <includes>
                            <include>**/*.uml</include>
                        </includes>
                    </sourceFiles>
                    <outputDirectory>${project.build.directory}/site/images/uml</outputDirectory>
                </configuration>
                <executions>
                    <execution>
                        <id>uml-generate</id>
                        <phase>pre-site</phase>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                    </execution>
                </executions>
                <dependencies>
                    <dependency>
                        <groupId>net.sourceforge.plantuml</groupId>
                        <artifactId>plantuml</artifactId>
                        <version>8043</version>
                    </dependency>
                </dependencies>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <profile>
            <id>m2e</id>
            <activation>
                <property>
                    <name>m2e.version</name>
                </property>
            </activation>
            <build>
                <resources>
                    <resource>
                        <filtering>false</filtering>
                        <directory>src/main/resources</directory>
                    </resource>
                    <resource>
                        <filtering>true</filtering>
                        <directory>src/main/config</directory>
                    </resource>
                </resources>
            </build>
            <properties>
                <environment>Developpement</environment>
                <webapp-target-dir>${project.build.directory}/m2e-wtp/web-resources</webapp-target-dir>

                <hibernate.dialect>org.hibernate.dialect.H2Dialect</hibernate.dialect>
                <hibernate.hbm2ddl.auto>update</hibernate.hbm2ddl.auto>
                <hibernate.show_sql>true</hibernate.show_sql>

                <log.file.technical>${project.build.directory}/logs/tech.log</log.file.technical>
                <log.file.functional>${project.build.directory}/logs/func.log</log.file.functional>
                <log.file.thirdParty>${project.build.directory}/logs/thirdParty.log</log.file.thirdParty>
                <log.max_size>1 MB</log.max_size>
                <log.max_index>20</log.max_index>
                <log.level>info</log.level>
                <log.level_fwk>debug</log.level_fwk>
                <log.level_app>debug</log.level_app>

                <log.appender.root>console</log.appender.root>
                <log.appender.func>console</log.appender.func>
                <log.appender.tech>console</log.appender.tech>
                <log.appender.thirdParty>console</log.appender.thirdParty>
                <frontUrl>http://www.dev.guichet-qualifications.fr/fr</frontUrl>
                <liasse.generation.service.url>http://localhost:40080/forms-ws-server/api</liasse.generation.service.url>
                <tracker.nb.retry>5</tracker.nb.retry>
                <jndi.db.datasource>jdbc/ge_regent</jndi.db.datasource>


            </properties>
        </profile>
    </profiles>

</project>