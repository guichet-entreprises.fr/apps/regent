package fr.ge.common.tc.ws.v1.model;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IXmlMajduSES.
 */
@ResourceXPath("/GUEN_MAJDU")
public interface IXmlMajduSES {

  /**
   * Getter de l'attribut numeroDossierUnique.
   * 
   * @return la valeur de numeroDossierUnique
   */
  @FieldXPath("dossierUnique/identifiantDossierUnique/numeroDossierUnique")
  String getNumeroDossierUnique();

  /**
   * Setter de l'attribut numeroDossierUnique.
   * 
   * @param numeroDossierUnique
   *          la nouvelle valeur de numeroDossierUnique
   */
  void setNumeroDossierUnique(String numeroDossierUnique);

  /**
   * Getter de l'attribut codePartenaire.
   * 
   * @return la valeur de codePartenaire
   */
  @FieldXPath("dossierUnique/emetteurMiseAJour/codePartenaire")
  String getCodePartenair();

  /**
   * Setter de l'attribut codePartenair.
   * 
   * @param codePartenair
   *          la nouvelle valeur de codePartenair
   */
  void setCodePartenair(String codePartenair);

  /**
   * Getter de l'attribut emetteur.
   *
   * @return le emetteur
   */
  @FieldXPath("emetteur")
  String getEmetteur();

  /**
   * Getter de l'attribut destinataire.
   *
   * @return le destinataire
   */
  @FieldXPath("destinataire")
  String getDestinataire();

  /**
   * Setter de emetteur.
   *
   * @param emetteur
   *          le nouveau emetteur
   */
  void setEmetteur(String emetteur);

  /**
   * Setteur de destinataire.
   *
   * @param destinataire
   *          le nouveau destinataire
   */
  void setDestinataire(String destinataire);

  /**
   * Getter de l'attribut etatsDossier.
   * 
   * @return la valeur de etatsDossier
   */
  @FieldXPath("dossierUnique/majEtatSes/etatDossier")
  String getEtatDossier();

  /**
   * Setter de l'attribut etatsDossier.
   * 
   * @param etatsDossier
   *          la nouvelle valeur de etatsDossier
   */
  void setEtatDossier(String etatsDossier);

  /**
   * Getter de l'attribut codePartenairEmetteur.
   * 
   * @return la valeur de codePartenairEmetteur
   */
  @FieldXPath("dossierUnique/identifiantDossierUnique/codePartenaireEmetteur")
  String getCodePartenairEmetteur();

  /**
   * Setter de l'attribut codePartenairEmetteur.
   * 
   * @param codePartenairEmetteur
   *          la nouvelle valeur de codePartenairEmetteur
   */
  void setCodePartenairEmetteur(String codePartenairEmetteur);

  /**
   * Getter de l'attribut transmissionCodePartenair.
   * 
   * @return la valeur de transmissionCodePartenair
   */
  @FieldXPath("dossierUnique/indiceTransmission/codePartenaireEmetteur")
  String getTransmissionCodePartenair();

  /**
   * Setter de l'attribut transmissionCodePartenair.
   * 
   * @param transmissionCodePartenair
   *          la nouvelle valeur de transmissionCodePartenair
   */
  void setTransmissionCodePartenair(String transmissionCodePartenair);

  /**
   * Getter de l'attribut indiceTransmission.
   * 
   * @return la valeur de indiceTransmission
   */
  @FieldXPath("dossierUnique/indiceTransmission/indiceTransmission")
  String getIndiceTransmission();

  /**
   * Setter de l'attribut indiceTransmission.
   * 
   * @param indiceTransmission
   *          la nouvelle valeur de indiceTransmission
   */
  void setIndiceTransmission(String indiceTransmission);

  /**
   * Getter de l'attribut typeDossierMisAJour.
   * 
   * @return la valeur de typeDossierMisAJour
   */
  @FieldXPath("dossierUnique/majEtatSes/typeDossierMisAJour")
  String getTypeDossierMisAJour();

  /**
   * Setter de l'attribut typeDossierMisAJour.
   * 
   * @param typeDossierMisAJour
   *          la nouvelle valeur de typeDossierMisAJour
   */
  void setTypeDossierMisAJour(String typeDossierMisAJour);

}
