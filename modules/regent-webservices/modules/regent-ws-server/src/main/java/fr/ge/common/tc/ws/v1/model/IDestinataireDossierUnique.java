package fr.ge.common.tc.ws.v1.model;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IDestinataireDossierUnique.
 */
@ResourceXPath("destinataireDossierUnique")
public interface IDestinataireDossierUnique {

  /**
   * Getter.
   * 
   * @return roleDestinataire
   */
  @FieldXPath("roleDestinataire")
  String getRoleDestinataire();

  /**
   * Setter.
   *
   * @param roleDestinataire
   *          le nouveau role destinataire
   */
  void setRoleDestinataire(String roleDestinataire);

  /**
   * Getter.
   * 
   * @return codePartenaire
   */
  @FieldXPath("codeDestinataire/codePartenaire")
  String getCodePartenaire();

  /**
   * Setter.
   *
   * @param codePartenaire
   *          le nouveau code partenaire
   */
  void setCodePartenaire(String codePartenaire);

  /**
   * Getter.
   * 
   * @return codeEdi
   */
  @FieldXPath("codeDestinataire/codeEdi")
  String getCodeEdi();

  /**
   * Setter.
   *
   * @param codeEdi
   *          le nouveau code edi
   */
  void setCodeEdi(String codeEdi);

  /**
   * Getter.
   * 
   * @return libelleIntervenant
   */
  @FieldXPath("codeDestinataire/libelleIntervenant")
  String getLibelleIntervenant();

  /**
   * Setter.
   *
   * @param libelleIntervenant
   *          le nouveau libelle intervenant
   */
  void setLibelleIntervenant(String libelleIntervenant);

  /**
   * Getter.
   * 
   * @return typeautorisation
   */
  @FieldXPath("typeAutorisation")
  String getTypeAutorisation();

  /**
   * Setter.
   *
   * @param typeAutorisation
   *          le nouveau type autorisation
   */
  void setTypeAutorisation(String typeAutorisation);

}
