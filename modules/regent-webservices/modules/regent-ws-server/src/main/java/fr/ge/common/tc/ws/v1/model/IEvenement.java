package fr.ge.common.tc.ws.v1.model;

import org.joda.time.DateTime;
import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;
import org.xmlfield.validation.annotations.NotEmpty;

/**
 * Le Interface IEvenement.
 */
@ResourceXPath("/evenement")
public interface IEvenement {

    /**
     * Get le etat.
     *
     * @return le etat
     */
    @FieldXPath("etat")
    @NotEmpty(groups = { Save.class })
    String getEtat();

    /**
     * Set le etat.
     *
     * @param etat
     *            le nouveau etat
     */
    void setEtat(String etat);

    /**
     * Get le date.
     *
     * @return le date
     */
    @FieldXPath("date")
    @NotEmpty(groups = { Save.class })
    DateTime getDate();

    /**
     * Set le date.
     *
     * @param date
     *            le nouveau date
     */
    void setDate(DateTime date);

    /**
     * Get le erreur.
     *
     * @return le erreur
     */
    @FieldXPath("erreur")
    String getErreur();

    /**
     * Set le erreur.
     *
     * @param erreur
     *            le nouveau erreur
     */
    void setErreur(String erreur);

    /**
     * Retourne la liste d'erreur du dossier
     * 
     * @return
     */
    @FieldXPath("erreurDossier")
    IErreurDossier[] getErreurDossier();

    /**
     * Ajoute une nouvelle erreur au dossier.
     * 
     * @return la nouvelle erreur
     */
    IErreurDossier addToErreurDossier();

}
