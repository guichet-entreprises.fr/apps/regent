package fr.ge.common.tc.ws.v1.model;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IDestinataireDossierAh.
 */
@ResourceXPath("/informationDestinataireAutorisation")
public interface IDestinataireDossierAh extends IDestinataireDossier {

  /**
   * Getter de l'attribut codePartenaireDestinataireAutorisation.
   * 
   * @return la valeur de codePartenaireDestinataireAutorisation
   */
  @Override
  @FieldXPath("codePartenaireDestinataireAutorisation")
  String getCodeDestinataire();

  /**
   * Setter de l'attribut codeDestinataire.
   * 
   * @param codeDestinataire
   *          la nouvelle valeur de codeDestinataire
   */
  void setCodeDestinataire(String codeDestinataire);

  /**
   * Getter de l'attribut typeAutorisation.
   * 
   * @return la valeur de typeAutorisation
   */
  @FieldXPath("typeAutorisation")
  String getTypeAutorisation();

  /**
   * Setter de l'attribut typeAutorisation.
   * 
   * @param typeAutorisation
   *          la nouvelle valeur de typeAutorisation
   */
  void setTypeAutorisation(String typeAutorisation);

  /**
   * Getter de l'attribut typeActivite.
   * 
   * @return la valeur de typeActivite
   */
  @FieldXPath("typeActivite")
  String getTypeActivite();

  /**
   * Setter de l'attribut typeActivite.
   * 
   * @param typeActivite
   *          la nouvelle valeur de typeActivite
   */
  void setTypeActivite(String typeActivite);

  /**
   * Getter de l'attribut informationSousDossierAutorisation.
   * 
   * @return la valeur de informationSousDossierAutorisation
   */
  @FieldXPath("informationSousDossierAutorisation")
  IInformationSousDossierAutorisation[] getInformationsSousDossiersAutorisations();

  /**
   * Setter de l'attribut informationSousDossierAutorisation.
   * 
   * @param informationSousDossierAutorisation
   *          la nouvelle valeur de informationSousDossierAutorisation
   */
  void setInformationsSousDossiersAutorisations(IInformationSousDossierAutorisation[] informationSousDossierAutorisation);

  /**
   * Ajoute une informationSousDossierAutorisation à la liste.
   * 
   * @return IInformationSousDossierAutorisation
   */
  IInformationSousDossierAutorisation addToInformationsSousDossiersAutorisations();

}
