/**
 * 
 */
package fr.ge.common.tc.ws.v1.service.impl;

import java.nio.charset.StandardCharsets;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import fr.ge.common.regent.constante.enumeration.InfosXmlTcEnum;
import fr.ge.common.regent.ws.v1.bean.TcBean;
import fr.ge.common.regent.ws.v1.service.ITcRestService;
import fr.ge.common.tc.ws.v1.service.contract.IXmlTcGenerator;
import fr.ge.common.utils.exception.TechnicalException;
import io.swagger.annotations.Api;

/**
 * Service de génération de XML-TC.
 * 
 * @author $Author: lhoffste $
 * @version $Revision: 0 $
 */

@Api("Regent REST Private Services")
@Path("/v1")
public class TcServiceImpl implements ITcRestService {

	@Autowired
	@Qualifier("xmlTcGuenGenerator")
	IXmlTcGenerator ixmlTcGuenGenerator;

	@Autowired
	@Qualifier("xmlTcMgunGenerator")
	IXmlTcGenerator ixmlTcMgunGenerator;

	/** The logger. */
	private static final Logger LOGGER = LoggerFactory.getLogger(TcServiceImpl.class);

	@Override
	/**
	 * {@inheritDoc}
	 */
	public Response generateXmlTc(TcBean xmltcBean) throws Exception {
		LOGGER.info("Génération du XML-TC");
		String retour = "";

		if (xmltcBean != null && xmltcBean.getDossierUnique() != null
				&& xmltcBean.getDossierUnique().getDestinataireDossierUnique() != null
				&& !xmltcBean.getDossierUnique().getDestinataireDossierUnique().isEmpty()) {
			if (xmltcBean.getDossierUnique().getDestinataireDossierUnique().get(0).getCodeDestinataire()
					.getCodeEdi() != null
					&& !xmltcBean.getDossierUnique().getDestinataireDossierUnique().get(0).getCodeDestinataire()
							.getCodeEdi().isEmpty()) {
				String network = xmltcBean.getDossierUnique().getDestinataireDossierUnique().get(0)
						.getCodeDestinataire().getCodeEdi().substring(0, 1);

				if (InfosXmlTcEnum.CA.getNetwork().equals(network)) {
					retour = ixmlTcMgunGenerator.generate(xmltcBean, InfosXmlTcEnum.CA);
				} else if (InfosXmlTcEnum.CMA.getNetwork().equals(network)) {
					retour = ixmlTcMgunGenerator.generate(xmltcBean, InfosXmlTcEnum.CMA);
				} else if (InfosXmlTcEnum.CCI.getNetwork().equals(network)) {
					retour = ixmlTcMgunGenerator.generate(xmltcBean, InfosXmlTcEnum.CCI);
				} else if (InfosXmlTcEnum.URSSAF.getNetwork().equals(network)) {
					retour = ixmlTcMgunGenerator.generate(xmltcBean, InfosXmlTcEnum.URSSAF);
				} else {
					retour = ixmlTcGuenGenerator.generate(xmltcBean, InfosXmlTcEnum.GREFFE);
				}
			} else {
				throw new TechnicalException("Code EDI manquant ou vide");
			}
		}
		byte[] tb = retour.getBytes(StandardCharsets.UTF_8);
		return Response.ok(tb).build();

	}

}