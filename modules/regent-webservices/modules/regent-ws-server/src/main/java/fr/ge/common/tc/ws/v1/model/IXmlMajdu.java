package fr.ge.common.tc.ws.v1.model;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IXmlMajdu.
 */
@ResourceXPath("/GUEN_MAJDU")
public interface IXmlMajdu {

  /**
   * Getter de l'attribut version.
   *
   * @return le version
   */
  @FieldXPath("version")
  String getVersion();

  /**
   * Setter de version.
   *
   * @param version
   *          le nouveau version
   */
  void setVersion(String version);

  /**
   * Getter de l'attribut emetteur.
   *
   * @return le emetteur
   */
  @FieldXPath("emetteur")
  String getEmetteur();

  /**
   * Setter de emetteur.
   *
   * @param emetteur
   *          le nouveau emetteur
   */
  void setEmetteur(String emetteur);

  /**
   * Getter de l'attribut destinataire.
   *
   * @return le destinataire
   */
  @FieldXPath("destinataire")
  String getDestinataire();

  /**
   * Setteur de destinataire.
   *
   * @param destinataire
   *          le nouveau destinataire
   */
  void setDestinataire(String destinataire);

  /**
   * Getter de l'attribut dateHeureGenerationXml.
   *
   * @return le date heure generation xml
   */
  @FieldXPath("dateHeureGenerationXml")
  String getDateHeureGenerationXml();

  /**
   * Setteur de dateHeureGenerationXml.
   *
   * @param dateHeureGenerationXml
   *          le nouveau date heure generation xml
   */
  void setDateHeureGenerationXml(String dateHeureGenerationXml);

  /**
   * Getter de l'attribut commentaire.
   *
   * @return le commentaire
   */
  @FieldXPath("commentaire")
  String getCommentaire();

  /**
   * Setteur de commentaire.
   *
   * @param commentaire
   *          le nouveau commentaire
   */
  void setCommentaire(String commentaire);

  /**
   * getter de l'attribut dossierUnique.
   * 
   * @return IDossierUnique[]
   */
  @FieldXPath("dossierUnique")
  IDossierUnique[] getDossiersUnique();

  /**
   * Ajoute un dossierUnique à la liste du Xml Majdu.
   * 
   * @return IDossierUnique à ajouter
   */
  IDossierUnique addToDossiersUnique();

  /**
   * Retire le from dossiers unique.
   *
   * @param dossierUnique
   *          le dossier unique
   */
  void removeFromDossiersUnique(IDossierUnique dossierUnique);

  /**
   * setter de l'attribut dossierUnique.
   * 
   * @param dossiersUnique
   *          IDossierUnique
   */
  void setDossiersUnique(IDossierUnique[] dossiersUnique);

}
