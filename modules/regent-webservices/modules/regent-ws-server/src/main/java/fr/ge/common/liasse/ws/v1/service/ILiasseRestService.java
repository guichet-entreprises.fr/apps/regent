/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 */

package fr.ge.common.liasse.ws.v1.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 * Liasse generation Rest service contract, private methods.
 * 
 * @author mtakerra
 *
 */
@Path("/v1")
public interface ILiasseRestService {

    /**
     * WS to generate a liasse number
     * 
     * @return
     */
    @GET
    @Path("/liasse/generate")
    String generateLiasseNumber();

}
