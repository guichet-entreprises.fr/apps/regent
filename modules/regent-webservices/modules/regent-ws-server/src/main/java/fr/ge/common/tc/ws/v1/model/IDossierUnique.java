package fr.ge.common.tc.ws.v1.model;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IDossierUnique.
 */
@ResourceXPath("/dossierUnique")
public interface IDossierUnique {

  /**
   * Getter de l'attribut numeroDossierUnique.
   * 
   * @return la valeur de numeroDossierUnique
   */
  @FieldXPath("identifiantDossierUnique/numeroDossierUnique")
  String getNumeroDossierUnique();

  /**
   * Setter de l'attribut numeroDossierUnique.
   * 
   * @param numeroDossierUnique
   *          la nouvelle valeur de numeroDossierUnique
   */
  void setNumeroDossierUnique(String numeroDossierUnique);

  /**
   * Getter de l'attribut codePartenaireEmetteur.
   * 
   * @return la valeur de codePartenaireEmetteur
   */
  @FieldXPath("identifiantDossierUnique/codePartenaireEmetteur")
  String getCodePartenaireEmetteure();

  /**
   * Setter de l'attribut codePartenaireEmetteur.
   * 
   * @param codePartenaireEmetteur
   *          la nouvelle valeur de codePartenaireEmetteur
   */
  void setCodePartenaireEmetteure(String codePartenaireEmetteur);

  /**
   * Getter de l'attribut codePartenaire.
   * 
   * @return la valeur de codePartenaire
   */
  @FieldXPath("emetteurMiseAJour/codePartenaire")
  String getCodePartenaire();

  /**
   * Setter de l'attribut codePartenaire.
   * 
   * @param codePartenaire
   *          la nouvelle valeur de codePartenaire
   */
  void setCodePartenaire(String codePartenaire);

  /**
   * Getter de l'attribut transmissionCodePartenair.
   * 
   * @return la valeur de transmissionCodePartenair
   */
  @FieldXPath("indiceTransmission/codePartenaireEmetteur")
  String getTransmissionCodePartenair();

  /**
   * Setter de l'attribut transmissionCodePartenair.
   * 
   * @param transmissionCodePartenair
   *          la nouvelle valeur de transmissionCodePartenair
   */
  void setTransmissionCodePartenair(String transmissionCodePartenair);

  /**
   * Getter de l'attribut indiceTransmission.
   * 
   * @return la valeur de indiceTransmission
   */
  @FieldXPath("indiceTransmission/indiceTransmission")
  String getIndiceTransmission();

  /**
   * Setter de l'attribut indiceTransmission.
   * 
   * @param indiceTransmission
   *          la nouvelle valeur de indiceTransmission
   */
  void setIndiceTransmission(String indiceTransmission);

  // /**
  // * Getter de l'attribut transmissionInitialCodePartenair.
  // *
  // * @return la valeur de transmissionInitialCodePartenair
  // */
  // @FieldXPath("dossierUnique/indiceTransmissionInitial/transmissionInitialCodePartenair")
  // String getTransmissionInitialCodePartenair();
  //
  // /**
  // * Setter de l'attribut transmissionInitialCodePartenair.
  // *
  // * @param transmissionInitialCodePartenair
  // * la nouvelle valeur de transmissionInitialCodePartenair
  // */
  // void setTransmissionInitialCodePartenair(String
  // transmissionInitialCodePartenair);
  //
  // /**
  // * Getter de l'attribut indiceTransmissionInitial.
  // *
  // * @return la valeur de indiceTransmissionInitial
  // */
  // @FieldXPath("dossierUnique/indiceTransmissionInitial/indiceTransmission")
  // String getIndiceTransmissionInitial();
  //
  // /**
  // * Setter de l'attribut indiceTransmissionInitial.
  // *
  // * @param indiceTransmissionInitial
  // * la nouvelle valeur de indiceTransmissionInitial
  // */
  // void setIndiceTransmissionInitial(String indiceTransmissionInitial);

  /**
   * Getter de l'attribut nomDossier.
   * 
   * @return la valeur de nomDossier
   */
  @FieldXPath("nomDossier")
  String getNomDossier();

  /**
   * Setter de l'attribut nomDossier.
   * 
   * @param nomDossier
   *          la nouvelle valeur de nomDossier
   */
  void setNomDossier(String nomDossier);

  /**
   * Getter de l'attribut codeEdi.
   * 
   * @return la valeur de codeEdi
   */
  @FieldXPath("emetteurMiseAJour/codeEdi")
  String getCodeEdi();

  /**
   * Setter de l'attribut codeEdi.
   * 
   * @param codeEdi
   *          la nouvelle valeur de codeEdi
   */
  void setCodeEdi(String codeEdi);

  /**
   * Getter de l'attribut typeDossierMisAJour.
   * 
   * @return la valeur de typeDossierMisAJour
   */
  @FieldXPath("majEtatSes/typeDossierMisAJour")
  String getTypeDossierMisAJour();

  /**
   * Setter de l'attribut typeDossierMisAJour.
   * 
   * @param typeDossierMisAJour
   *          la nouvelle valeur de typeDossierMisAJour
   */
  void setTypeDossierMisAJour(String typeDossierMisAJour);

  /**
   * Getter de l'attribut typeDossierMisAJour.
   * 
   * @return la valeur de typeDossierMisAJour
   */
  @FieldXPath("majEtatSes/etatDossier")
  String getEtatDossierMisAJour();

  /**
   * Setter de l'attribut typeDossierMisAJour.
   *
   * @param etatDossier
   *          le nouveau etat dossier mis a jour
   */
  void setEtatDossierMisAJour(String etatDossier);

  /**
   * Getter de l'attribut etatsDossier.
   * 
   * @return la valeur de etatsDossier
   */
  @FieldXPath("majDossierCfe/miseAJourDossier/etatDossier")
  IEtatDossier[] getEtatsDossier();

  /**
   * Setter de l'attribut etatsDossier.
   * 
   * @param etatsDossier
   *          la nouvelle valeur de etatsDossier
   */
  void setEtatsDossier(IEtatDossier[] etatsDossier);

  /**
   * getter de l'attribut etatSes.
   * 
   * @return IEtatSes
   */
  @FieldXPath("majEtatSes")
  IEtatSes[] getEtatsSes();

  /**
   * setter de l'attribut etatSes.
   *
   * @param etatSes
   *          IEtatSes[]
   */
  void setEtatsSes(IEtatSes[] etatSes);

  /**
   * Getter de l'attribut numeroDossierAutorisation.
   * 
   * @return la valeur de numeroDossierAutorisation
   */
  @FieldXPath("majDossierAutorisation/numeroDossierAutorisation")
  String getNumeroDossierAutorisation();

  /**
   * Setter de l'attribut numeroDossierAutorisation.
   * 
   * @param numeroDossierAutorisation
   *          la nouvelle valeur de numeroDossierAutorisation
   */
  void setNumeroDossierAutorisation(String numeroDossierAutorisation);

  /**
   * Getter de l'attribut codeEtat.
   * 
   * @return la valeur de codeEtat
   */
  @FieldXPath("majDossierAutorisation/sousDossierAutorisation/miseAJourSousDossier/etatDossier/codeEtat")
  String getAutorisationCodeEtat();

  /**
   * Setter de l'attribut codeEtat.
   * 
   * @param codeEtat
   *          la nouvelle valeur de codeEtat
   */
  void setAutorisationCodeEtat(String codeEtat);

  /**
   * Getter de l'attribut commentaireEtat.
   * 
   * @return la valeur de commentaireEtat
   */
  @FieldXPath("majDossierAutorisation/sousDossierAutorisation/miseAJourSousDossier/etatDossier/commentaireEtat")
  String getAutorisationCommentaireEtat();

  /**
   * Setter de l'attribut commentaireEtat.
   * 
   * @param commentaireEtat
   *          la nouvelle valeur de commentaireEtat
   */
  void setAutorisationCommentaireEtat(String commentaireEtat);

  /**
   * Getter de l'attribut indiceSousDossierAutorisation.
   * 
   * @return la valeur de indiceSousDossierAutorisation
   */
  @FieldXPath("majDossierAutorisation/sousDossierAutorisation/indiceSousDossierAutorisation")
  String getIndiceSousDossierAutorisation();

  /**
   * Setter de l'attribut indiceSousDossierAutorisation.
   *
   * @param indiceSousDossier
   *          le nouveau indice sous dossier autorisation
   */
  void setIndiceSousDossierAutorisation(String indiceSousDossier);

  /**
   * Getter.
   * 
   * @return indiceEtat
   */
  @FieldXPath("majDossierAutorisation/sousDossierAutorisation/miseAJourSousDossier/etatDossier/indiceEtat")
  String getIndiceEtat();

  /**
   * Set le indice etat.
   *
   * @param indiceEtat
   *          le nouveau indice etat
   */
  void setIndiceEtat(String indiceEtat);

  /**
   * Getter.
   * 
   * @return typeEtat
   */
  @FieldXPath("majDossierAutorisation/sousDossierAutorisation/miseAJourSousDossier/etatDossier/typeEtat")
  String getTypeEtat();

  /**
   * Setter.
   *
   * @param typeEtat
   *          le nouveau type etat
   */
  void setTypeEtat(String typeEtat);

  /**
   * Getter.
   * 
   * @return codeEtat
   */
  @FieldXPath("majDossierAutorisation/sousDossierAutorisation/miseAJourSousDossier/etatDossier/codeEtat")
  String getCodeEtat();

  /**
   * Setter.
   *
   * @param codeEtat
   *          le nouveau code etat
   */
  void setCodeEtat(String codeEtat);

  /**
   * Getter.
   * 
   * @return commentaireEtat
   */
  @FieldXPath("majDossierAutorisation/sousDossierAutorisation/miseAJourSousDossier/etatDossier/commentaireEtat")
  String getCommentaireEtat();

  /**
   * Setter.
   *
   * @param commentaireEtat
   *          le nouveau commentaire etat
   */
  void setCommentaireEtat(String commentaireEtat);

  /**
   * Getter.
   * 
   * @return dateHeureEtat
   */
  @FieldXPath("majDossierAutorisation/sousDossierAutorisation/miseAJourSousDossier/etatDossier/dateHeureEtat")
  String getDateHeureEtat();

  /**
   * Setter.
   *
   * @param dateHeureEtat
   *          le nouveau date heure etat
   */
  void setDateHeureEtat(String dateHeureEtat);

}
