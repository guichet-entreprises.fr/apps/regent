/**
 * 
 */
package fr.ge.common.tc.ws.v1.service.contract;

import fr.ge.common.regent.constante.enumeration.InfosXmlTcEnum;
import fr.ge.common.regent.ws.v1.bean.TcBean;

/**
 * Interface de generation du XML TC
 * 
 * @author $Author: traimbau $
 * @version $Revision: 0 $
 */

public interface IXmlTcGenerator {
	/**
	 * Generation du XML TC à partir des informations transmises via la formalité
	 * récupéré dans xmltcBean
	 * 
	 * @param xmltcBean
	 * @return XmlTC généré
	 * @throws Exception
	 */
	public String generate(TcBean xmltcBean, InfosXmlTcEnum infos) throws Exception;
}
