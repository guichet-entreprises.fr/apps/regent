package fr.ge.common.tc.ws.v1.model;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;
import org.xmlfield.core.types.XmlString;

/**
 * Le Interface IInformationSousDossierAutorisation.
 */
@ResourceXPath("/informationSousDossierAutorisation")
public interface IInformationSousDossierAutorisation {

  /**
   * Getter de l'attribut indiceSousDossierAutorisation.
   * 
   * @return la valeur de indiceSousDossierAutorisation
   */
  @FieldXPath("indiceSousDossierAutorisation")
  String getIndiceSousDossierAutorisation();

  /**
   * Setter de l'attribut indiceSousDossierAutorisation.
   * 
   * @param indiceSousDossierAutorisation
   *          la nouvelle valeur de indiceSousDossierAutorisation
   */
  void setIndiceSousDossierAutorisation(String indiceSousDossierAutorisation);

  /**
   * Getter de l'attribut indicePieceJointeAutorisation.
   * 
   * @return la valeur de indicePieceJointeAutorisation
   */
  @FieldXPath("indicePieceJointeAutorisation")
  XmlString[] getIndicesPiecesJointesAutorisation();

  /**
   * Setter de l'attribut indicePieceJointeAutorisation.
   *
   * @param indicesPiecesJointesAutorisation
   *          le nouveau indices pieces jointes autorisation
   */
  void setIndicesPiecesJointesAutorisation(XmlString[] indicesPiecesJointesAutorisation);

  /**
   * Ajoute une pièce jointe a une autorisation.
   *
   * @return le xml string
   */
  XmlString addToIndicesPiecesJointesAutorisation();

}
