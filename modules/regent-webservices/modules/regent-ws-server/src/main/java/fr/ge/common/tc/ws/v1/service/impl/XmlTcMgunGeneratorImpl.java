/**
 * 
 */
package fr.ge.common.tc.ws.v1.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xmlfield.core.XmlField;
import org.xmlfield.core.XmlFieldFactory;

import fr.ge.common.regent.constante.enumeration.InfosXmlTcEnum;
import fr.ge.common.regent.service.ServiceGeneationRegentFromDB;
import fr.ge.common.regent.service.impl.ServiceValidationXmlTcImpl;
import fr.ge.common.regent.ws.v1.bean.Correspondant;
import fr.ge.common.regent.ws.v1.bean.DestinataireDossierCfe;
import fr.ge.common.regent.ws.v1.bean.PieceJointe;
import fr.ge.common.regent.ws.v1.bean.TcBean;
import fr.ge.common.tc.ws.v1.model.IDestinataires;
import fr.ge.common.tc.ws.v1.model.IDocumentJustificatif;
import fr.ge.common.tc.ws.v1.model.IXmlTcMgun;
import fr.ge.common.tc.ws.v1.service.contract.IXmlTcGenerator;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * Implementation de la génération du XML TC pour une authorité de type MGUN
 * (URSSAF, CCI, CMA)
 * 
 * @author $Author: traimbau $
 * @version $Revision: 0 $
 */

@Service("xmlTcMgunGenerator")
public class XmlTcMgunGeneratorImpl implements IXmlTcGenerator {

    /** The logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(XmlTcMgunGeneratorImpl.class);

    @Autowired
    private XmlFieldFactory xmlFieldFactory;

    @Autowired
    protected ServiceGeneationRegentFromDB serviceGenerationRegentFromDB;

    @Autowired
    protected ServiceValidationXmlTcImpl ServiceValidationXmlTc;

    // Version of XSD for MGUN XML-TC
    private final String XMLTC_VERSION = "V2009.01";

    // Attachment code value
    private final String CERFA_CODE_TYPE = "LI";
    private final String OTHER_CODE_TYPE = "PJ";

    // Default value for codePays
    private final String DEFAULT_CODE_PAYS = "FR";

    private static final String TDR = "TDR";
    /** La constante CFE. */
    private static final String CFE = "CFE";

    /** La constante CFE_AC. */
    private static final String CFE_AC = "CFE+AC";

    /** La constante CFE_TDR. */
    private static final String CFE_TDR = "CFE+TDR";

    /** La constante AC. */
    private static final String AC = "AC";

    // Default value for BureauDistributeur
    private final String BUREAUDISTR = "BUREAUDISTR";

    private IXmlTcMgun xml;

    /**
     * Constructeur de la classe.
     *
     */
    public XmlTcMgunGeneratorImpl() {
        // TODO Auto-generated constructor stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String generate(TcBean xmltcBean, InfosXmlTcEnum infos) throws Exception {
        XmlField xf = this.xmlFieldFactory.getXmlField();
        xml = xf.newObject(IXmlTcMgun.class);

        LOGGER.info("Génération d'un XML-TC PMGUN");

        LOGGER.debug("Ajout des donnés à la racine du XML-TC MGUN : version, emetteur, destinataire et heure de génération");
        xml.setVersion(infos.getVersionXmlTc());
        xml.setEmetteur(infos.getEmetteurCode());
        xml.setDestinataire(infos.getDestinataireCode());
        xml.setDateHeureGenerationXml(getDateGeneration());

        if (xmltcBean.getCommentaire() != null && !xmltcBean.getCommentaire().isEmpty()) {
            xml.setCommentaire(xmltcBean.getCommentaire());
        }

        if (xmltcBean.getDossierUnique() != null && xmltcBean.getDossierUnique().getDossierCfe() != null && xmltcBean.getDossierUnique().getDossierCfe().getNumeroDeLiasse() != null
                && !xmltcBean.getDossierUnique().getDossierCfe().getNumeroDeLiasse().isEmpty()) {
            LOGGER.debug("Ajout du numéro de liasse à formalite/numeroDeLiasse");
            xml.setNumeroDeLiasse(xmltcBean.getDossierUnique().getDossierCfe().getNumeroDeLiasse());
        } else {
            throw new TechnicalException("Numéro de liasse manquante");
        }

        // Date de dépot de dossier
        xml.setDateDepot(getDateGeneration());

        if (xmltcBean.getDossierUnique() != null) {
            // Ajout des destinataire au XML-TC
            addDestinataires(xmltcBean);

            // Ajout du bloc correspondant (optionnel)
            addCorrespondant(xmltcBean);

            // Ajout des pièces jointes au XML-TC
            addDocumentJustificatif(xmltcBean);

        } else {
            throw new TechnicalException("Dossier unique manquant");
        }

        String retour = xf.objectToXml(xml);
        LOGGER.debug(retour);
        // Validation du XML TC
        this.ServiceValidationXmlTc.validateXmlTc(XMLTC_VERSION, retour);

        return retour;
    }

    /**
     * Get the generation date of XML-TC with the right format
     * 
     * @return generation date
     */
    private String getDateGeneration() {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date dateGenerationXml = new Date();

        return simpleDateFormat.format(dateGenerationXml);
    }

    /**
     * Ajout du bloc correspondant au XML-TC (optionnel)
     * 
     * @param correspondant
     * @param xmltcBean
     */
    private void addCorrespondant(TcBean xmltcBean) {
        LOGGER.debug("Ajout du correspondant à formalite/correspondant");
        if (xmltcBean.getDossierUnique().getIdentificationDossierUnique() != null && xmltcBean.getDossierUnique().getIdentificationDossierUnique().getCorrespondant() != null) {
            Correspondant correspondant = xmltcBean.getDossierUnique().getIdentificationDossierUnique().getCorrespondant();
            if (correspondant.getAdresseCorrespondant() != null && correspondant.getIdentiteCorrespondant().getNomCorrespondant() != null
                    && !correspondant.getIdentiteCorrespondant().getNomCorrespondant().isEmpty()) {
                // Set le nom du correspondant
                xml.setNomCorrespondant(correspondant.getIdentiteCorrespondant().getNomCorrespondant());
            } else if (xmltcBean.getDossierUnique().getIdentificationDossierUnique().getNomDossier() != null
                    && !xmltcBean.getDossierUnique().getIdentificationDossierUnique().getNomDossier().isEmpty()) {
                xml.setNomCorrespondant(xmltcBean.getDossierUnique().getIdentificationDossierUnique().getNomDossier());
            }
            if (correspondant.getAdresseCorrespondant().getLibelleDeVoie() != null && !correspondant.getAdresseCorrespondant().getLibelleDeVoie().isEmpty()) {
                // Set libelle de voie
                xml.setLibelleDeVoie(correspondant.getAdresseCorrespondant().getLibelleDeVoie());
            }
            if (correspondant.getAdresseCorrespondant().getLocalite() != null && !correspondant.getAdresseCorrespondant().getLocalite().isEmpty()) {
                // Set localite
                xml.setLocalite(correspondant.getAdresseCorrespondant().getLocalite());
            }
            if (correspondant.getAdresseCorrespondant().getCodePostal() != null && !correspondant.getAdresseCorrespondant().getCodePostal().isEmpty()) {
                // Set codePostal
                xml.setCodePostal(correspondant.getAdresseCorrespondant().getCodePostal());
            }

            // Set bureau distributeur !MANDATORY!
            if (correspondant.getAdresseCorrespondant().getBureauDistributeur() != null && !correspondant.getAdresseCorrespondant().getBureauDistributeur().isEmpty()) {
                xml.setBureauDistributeur(correspondant.getAdresseCorrespondant().getBureauDistributeur());
            } else {
                xml.setBureauDistributeur(BUREAUDISTR);
            }
            if (correspondant.getAdresseCorrespondant().getCodePays() != null && !correspondant.getAdresseCorrespondant().getCodePays().isEmpty()) {
                // Set code pays !MANDATORY!
                xml.setCodePays(correspondant.getAdresseCorrespondant().getCodePays());
            } else {
                xml.setCodePays(DEFAULT_CODE_PAYS);
            }
            if (correspondant.getAdresseCorrespondant().getAdresseEmail() != null && !correspondant.getAdresseCorrespondant().getAdresseEmail().isEmpty()) {
                // Set adresse Mail
                xml.setAdresseEmail(correspondant.getAdresseCorrespondant().getAdresseEmail());
            }
        }
    }

    /**
     * Ajout des destinataires au XML TC (code EDI et type de destinataire)
     * 
     * @param list
     */
    private void addDestinataires(TcBean xmltcBean) throws TechnicalException {
        LOGGER.debug("Ajout des destinataires à formalite/destinataires");
        if (xmltcBean.getDossierUnique().getDossierCfe().getDestinataireDossierCfe() != null && !xmltcBean.getDossierUnique().getDossierCfe().getDestinataireDossierCfe().isEmpty()) {
            List<DestinataireDossierCfe> listDestinataires = xmltcBean.getDossierUnique().getDossierCfe().getDestinataireDossierCfe();
            for (DestinataireDossierCfe destinataire : listDestinataires) {
                IDestinataires dest = xml.addToDestinataires();
                if (destinataire.getCodeEdiDestinataire() != null && !destinataire.getCodeEdiDestinataire().isEmpty()) {
                    dest.setCodeEdiDestinataire(destinataire.getCodeEdiDestinataire());
                } else {
                    throw new TechnicalException("Code EDI manquant");
                }
                if (destinataire.getRoleDestinataire() != null & !destinataire.getRoleDestinataire().isEmpty()) {

                    if (CFE.equals(destinataire.getRoleDestinataire())) {
                        dest.setTypeDestinataire(CFE);
                    } else if (TDR.equals(destinataire.getRoleDestinataire())) {
                        dest.setTypeDestinataire(AC);
                    } else if (CFE_TDR.equals(destinataire.getRoleDestinataire())) {
                        dest.setTypeDestinataire(CFE_AC);
                    }

                } else {
                    throw new TechnicalException("Role destinataire manquant");
                }
                // Ajout des informations de paiement
                if (destinataire.getPaiementDossier() != null) {
                    if (destinataire.getPaiementDossier().getMontantFormalite() != null && !destinataire.getPaiementDossier().getMontantFormalite().isEmpty()) {
                        if (Float.parseFloat(destinataire.getPaiementDossier().getMontantFormalite().replace(',', '.')) != 0) {
                            // Ajout de l'identité du payeur
                            if (destinataire.getPaiementDossier().getIdentitePayeur().getNomPayeur() != null && !destinataire.getPaiementDossier().getIdentitePayeur().getNomPayeur().isEmpty()) {
                                dest.setNomPayeur(destinataire.getPaiementDossier().getIdentitePayeur().getNomPayeur());
                            }
                            // Ajout de l'adresse du payeur
                            if (destinataire.getPaiementDossier().getAdressePayeur().getNumeroDeVoie() != null && !destinataire.getPaiementDossier().getAdressePayeur().getNumeroDeVoie().isEmpty()) {
                                dest.setNumeroDeVoie(destinataire.getPaiementDossier().getAdressePayeur().getNumeroDeVoie());
                            }
                            // Ajout du libelle de voie
                            if (StringUtils.isNotEmpty(destinataire.getPaiementDossier().getAdressePayeur().getLibelleDeVoie())) {

                                final String typeDeVoie = Optional.ofNullable(destinataire.getPaiementDossier().getAdressePayeur().getTypeDeVoie()) //
                                        .filter(StringUtils::isNotEmpty) //
                                        .orElse(StringUtils.EMPTY) //
                                ;
                                final String libelleDeVoie = Optional.of(destinataire.getPaiementDossier().getAdressePayeur().getLibelleDeVoie()) //
                                        .filter(StringUtils::isNotEmpty) //
                                        .orElse(StringUtils.EMPTY) //
                                ;
                                // Ajout du type de voie
                                dest.setTypeDeVoie(null);
                                dest.setLibelleDeVoie(String.join(" ", typeDeVoie, libelleDeVoie).trim());
                            }
                            // Ajout de la localite
                            if (destinataire.getPaiementDossier().getAdressePayeur().getLocalite() != null && !destinataire.getPaiementDossier().getAdressePayeur().getLocalite().isEmpty()) {
                                dest.setLocalite(destinataire.getPaiementDossier().getAdressePayeur().getLocalite());
                            }
                            // Ajout du bureau distributeur !MANDATORY!
                            if (destinataire.getPaiementDossier().getAdressePayeur().getBureauDistributeur() != null
                                    && !destinataire.getPaiementDossier().getAdressePayeur().getBureauDistributeur().isEmpty()) {
                                dest.setBureauDistributeur(destinataire.getPaiementDossier().getAdressePayeur().getBureauDistributeur());
                            } else {
                                dest.setBureauDistributeur(BUREAUDISTR);
                            }
                            // Ajout du code Pays !MANDATORY!
                            if (destinataire.getPaiementDossier().getAdressePayeur().getCodePays() != null && !destinataire.getPaiementDossier().getAdressePayeur().getCodePays().isEmpty()) {
                                dest.setCodePays(destinataire.getPaiementDossier().getAdressePayeur().getCodePays());
                            } else {
                                dest.setCodePays(DEFAULT_CODE_PAYS);
                            }
                            // Ajout de l'adresse mail
                            if (destinataire.getPaiementDossier().getAdressePayeur().getAdresseEmail() != null && !destinataire.getPaiementDossier().getAdressePayeur().getAdresseEmail().isEmpty()) {
                                dest.setAdresseMail(destinataire.getPaiementDossier().getAdressePayeur().getAdresseEmail());
                            }

                            dest.setMontantFormalite(destinataire.getPaiementDossier().getMontantFormalite());

                            if (destinataire.getPaiementDossier().getReferencePaiement() != null && !destinataire.getPaiementDossier().getReferencePaiement().isEmpty()) {
                                dest.setReferencePaiement(destinataire.getPaiementDossier().getReferencePaiement());
                            } else {
                                throw new TechnicalException("ReferencePaiement manquant ou vide pour le paiement");
                            }
                            if (destinataire.getPaiementDossier().getDateHeurePaiement() != null && !destinataire.getPaiementDossier().getDateHeurePaiement().isEmpty()) {
                                dest.setDateHeurePaiement(destinataire.getPaiementDossier().getDateHeurePaiement());
                            } else {
                                throw new TechnicalException("DateHeurePaiement manquant ou vide pour le paiement");
                            }
                        }
                    } else {
                        throw new TechnicalException("Montant formalité manquante ou vide pour le paiement");
                    }
                }
            }
        } else {
            throw new TechnicalException("Block destinataire manquant ou vide");
        }

    }

    /**
     * Ajout des pièces jointes au XML TC (type du documents et nom du fichier)
     * 
     * @param pieceJointe
     */
    private void addDocumentJustificatif(TcBean xmltcBean) {
        LOGGER.debug("Ajout de la liste des pièces jointes à formalite/documentJustificatif");
        if (xmltcBean.getDossierUnique().getPieceJointe() != null && !xmltcBean.getDossierUnique().getPieceJointe().isEmpty()) {
            List<PieceJointe> listPiecesJointes = xmltcBean.getDossierUnique().getPieceJointe();

            // Ajout de la liasse XML en premier
            for (PieceJointe pj : listPiecesJointes) {
                if (pj.getTypePieceJointe().equals("LIASSE_CFE_EDI") || pj.getTypePieceJointe().equals("LIASSE_TDR_EDI")) {
                    xml.setFichierLiasseXml(pj.getFichierPieceJointe());
                    listPiecesJointes.remove(pj);
                    break;
                }
            }
            for (PieceJointe pj : listPiecesJointes) {
                if (pj.getTypePieceJointe() != null && !pj.getTypePieceJointe().isEmpty()) {
                    // Si la pièce jointe est le cerfa
                    if (pj.getTypePieceJointe().equals("LIASSE_CFE_PDF") || pj.getTypePieceJointe().equals("LIASSE_TDR_PDF")) {
                        IDocumentJustificatif document = xml.addToDocumentJustificatif();
                        document.setCodeTypeDocumentJustificatif(CERFA_CODE_TYPE);
                        if (pj.getFichierPieceJointe() != null && !pj.getFichierPieceJointe().isEmpty()) {
                            document.setFichierPieceJointe(pj.getFichierPieceJointe());
                        } else {
                            throw new TechnicalException("Nom du cerfa manquant ou vide");
                        }
                    }
                    // Sinon c'est une autre pièce justificative
                    else {
                        IDocumentJustificatif document = xml.addToDocumentJustificatif();
                        document.setCodeTypeDocumentJustificatif(OTHER_CODE_TYPE);
                        if (pj.getFichierPieceJointe() != null && !pj.getFichierPieceJointe().isEmpty()) {
                            document.setFichierPieceJointe(pj.getFichierPieceJointe());
                        } else {
                            throw new TechnicalException("Nom d'une pièce jointe manquant ou vide");
                        }
                    }
                }
            }
        } else {
            throw new TechnicalException("Pièces jointes manquantes");
        }
    }

}
