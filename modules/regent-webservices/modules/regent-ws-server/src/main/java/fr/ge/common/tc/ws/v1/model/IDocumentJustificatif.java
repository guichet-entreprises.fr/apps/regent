/**
 * 
 */
package fr.ge.common.tc.ws.v1.model;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Interface de modélisation XML de la balise documentJustificatif du XML MGUN
 * 
 * @author $Author: traimbau $
 * @version $Revision: 0 $
 */

@ResourceXPath("/documentJustificatif")
public interface IDocumentJustificatif {

	/**
	 * Getter pour le codeTypeDocumentJustificatif
	 * 
	 * @return codeTypeDocumentJustificatif
	 */
	@FieldXPath("codeTypeDocumentJustificatif")
	String getCodeTypeDocumentJustificatif();

	/**
	 * Setter pour le codeTypeDocumentJustificatif
	 * 
	 * @param codeTypeDocumentJustificatif
	 */
	void setCodeTypeDocumentJustificatif(String codeTypeDocumentJustificatif);

	/**
	 * Getter pour la balise fichierPieceJointe
	 * 
	 * @return fichierPieceJointe
	 */
	@FieldXPath("fichierPieceJointe")
	String getFichierPieceJointe();

	/**
	 * Setter pour la balise fichierPieceJointe
	 * 
	 * @param fichierPieceJointe
	 */
	void setFichierPieceJointe(String fichierPieceJointe);
}
