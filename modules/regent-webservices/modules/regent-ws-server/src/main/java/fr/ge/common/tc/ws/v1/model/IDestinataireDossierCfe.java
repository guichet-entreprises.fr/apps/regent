package fr.ge.common.tc.ws.v1.model;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;
import org.xmlfield.core.types.XmlString;

/**
 * L'Interface IDestinataireDossierCfe; le bloc destinataireDossierCfe du
 * XML-TC, représente pour un destinataire les pièces du dossier et les
 * paiements qui lui sont destinés.
 */
@ResourceXPath("/dossierCfe/destinataireDossierCfe")
public interface IDestinataireDossierCfe extends IDestinataireDossier {

    /**
     * Getter de l'attribut roleDestinataire.
     * 
     * @return la valeur de roleDestinataire
     */
    @FieldXPath("roleDestinataire")
    String getRoleDestinataire();

    /**
     * Setter de l'attribut roleDestinataire.
     * 
     * @param roleDestinataire
     *            la nouvelle valeur de roleDestinataire
     */
    void setRoleDestinataire(String roleDestinataire);

    /**
     * Getter de l'attribut codeEdiDestinataire.
     * 
     * @return la valeur de codeEdiDestinataire
     */
    @Override
    @FieldXPath("codeEdiDestinataire")
    String getCodeDestinataire();

    /**
     * Setter de l'attribut codeDestinataire.
     * 
     * @param codeDestinataire
     *            la nouvelle valeur de codeDestinataire
     */
    void setCodeDestinataire(String codeDestinataire);

    /**
     * Getter de l'attribut indicesPiecesJointes.
     * 
     * @return la valeur de indicesPiecesJointes
     */
    @FieldXPath("indicePieceJointe")
    XmlString[] getIndicesPiecesJointes();

    /**
     * Setter de l'attribut indicesPiecesJointes.
     * 
     * @param indicesPiecesJointes
     *            la nouvelle valeur de indicesPiecesJointes
     */
    void setIndicesPiecesJointes(XmlString[] indicesPiecesJointes);

    /**
     * Ajoute un indice à la liste des PJs.
     *
     * @return XmlString
     */
    XmlString addToIndicesPiecesJointes();

    /**
     * Getter de l'attribut paiementDossier.
     * 
     * @return la valeur de paiementDossier
     */
    @FieldXPath("paiementDossier")
    IPaiementDossier getPaiementDossier();

    /**
     * Créer un nouveau IPaiementDossier.
     * 
     * @return {@link IPaiementDossier}
     */
    IPaiementDossier newPaiementDossier();

    /**
     * Setter de l'attribut paiementDossier.
     * 
     * @param paiementDossier
     *            la nouvelle valeur de paiementDossier
     */
    void setPaiementDossier(IPaiementDossier paiementDossier);

    /**
     * Getter de l'attribut indiceLiasseXml.
     * 
     * @return la valeur de indiceLiasseXml
     */
    @FieldXPath("indiceLiasseXml")
    String getIndiceLiasseXml();

    /**
     * Setter de l'attribut indiceLiasseXml.
     * 
     * @param indiceLiasseXml
     *            la nouvelle valeur de indiceLiasseXml
     */
    void setIndiceLiasseXml(String indiceLiasseXml);

}
