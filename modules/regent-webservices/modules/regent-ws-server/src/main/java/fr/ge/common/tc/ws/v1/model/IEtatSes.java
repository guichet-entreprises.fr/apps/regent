package fr.ge.common.tc.ws.v1.model;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IEtatSes.
 */
@ResourceXPath("/majEtatSes")
public interface IEtatSes {

  /**
   * Getter de l'attribut typeDossierMisAJour.
   * 
   * @return String
   */
  @FieldXPath("typeDossierMisAJour")
  String getTypeDossierMisAJour();

  /**
   * Setter de l'attribut typeDossierMisAJour.
   * 
   * @param typeDossierMisAJour
   *          String
   */
  void setTypeDossierMisAJour(String typeDossierMisAJour);

  /**
   * Getter de l'attribut codePartenaireAssocie.
   * 
   * @return String
   */
  @FieldXPath("codePartenaireAssocie")
  String getCodePartenaireAssocie();

  /**
   * Setter de l'attribut codePartenaireAssocie.
   * 
   * @param codePartenaireAssocie
   *          String
   */
  void setCodePartenaireAssocie(String codePartenaireAssocie);

  /**
   * Getter de l'attribut indiceSousDossierAutorisation.
   * 
   * @return String
   */
  @FieldXPath("indiceSousDossierAutorisation")
  String getIndiceSousDossierAutorisation();

  /**
   * Setter de l'attribut indiceSousDossierAutorisation.
   * 
   * @param indiceSousDossierAutorisation
   *          String
   */
  void setIndiceSousDossierAutorisation(String indiceSousDossierAutorisation);

  /**
   * Getter de l'attribut etatDossier.
   * 
   * @return IEtatDossier[]
   */
  @FieldXPath("etatDossier")
  IEtatDossier[] getEtatsDossier();

  /**
   * Setter de l'attribut etatDossier.
   * 
   * @param etatsDossier
   *          IEtatDossier[]
   */
  void setEtatsDossier(IEtatDossier[] etatsDossier);
}
