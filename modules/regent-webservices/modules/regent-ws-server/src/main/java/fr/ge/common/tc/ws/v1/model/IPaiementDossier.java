package fr.ge.common.tc.ws.v1.model;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IPaiementDossier.
 */
@ResourceXPath("/paiementDossier")
public interface IPaiementDossier {

  /**
   * Getter de l'attribut nomPayeur.
   * 
   * @return la valeur de nomPayeur
   */
  @FieldXPath("identitePayeur/nomPayeur")
  String getNomPayeur();

  /**
   * Getter prenom payeur.
   *
   * @return le prenom payeur
   */
  @FieldXPath("identitePayeur/prenomPayeur")
  String getPrenomPayeur();

  /**
   * Setter de l'attribut nomPayeur.
   * 
   * @param nomPayeur
   *          la nouvelle valeur de nomPayeur
   */
  void setNomPayeur(String nomPayeur);

  /**
   * Set le prenom payeur.
   *
   * @param prenomPayeur
   *          le nouveau prenom payeur
   */
  void setPrenomPayeur(String prenomPayeur);

  /**
   * Getter de l'attribut adressePayeur.
   * 
   * @return la valeur de adressePayeur
   */
  @FieldXPath("adressePayeur")
  IAdressePayeur getAdressePayeur();

  /**
   * créer une adressePayeur.
   * 
   * @return {@link IAdressePayeur}
   */
  IAdressePayeur newAdressePayeur();

  /**
   * Setter de l'attribut adressePayeur.
   * 
   * @param adressePayeur
   *          la nouvelle valeur de adressePayeur
   */
  void setAdressePayeur(IAdressePayeur adressePayeur);

  /**
   * Getter de l'attribut montantFormalite.
   * 
   * @return la valeur de montantFormalite
   */
  @FieldXPath("montantFormalite")
  String getMontantFormalite();

  /**
   * Setter de l'attribut montantFormalite.
   * 
   * @param montantFormalite
   *          la nouvelle valeur de montantFormalite
   */
  void setMontantFormalite(String montantFormalite);

  /**
   * Getter de l'attribut referencePaiement.
   * 
   * @return la valeur de referencePaiement
   */
  @FieldXPath("referencePaiement")
  String getReferencePaiement();

  /**
   * Setter de l'attribut referencePaiement.
   * 
   * @param referencePaiement
   *          la nouvelle valeur de referencePaiement
   */
  void setReferencePaiement(String referencePaiement);

  /**
   * Getter de l'attribut dateHeurePaiement.
   * 
   * @return la valeur de dateHeurePaiement
   */
  @FieldXPath("dateHeurePaiement")
  String getDateHeurePaiement();

  /**
   * Setter de l'attribut dateHeurePaiement.
   * 
   * @param dateHeurePaiement
   *          la nouvelle valeur de dateHeurePaiement
   */
  void setDateHeurePaiement(String dateHeurePaiement);

}
