package fr.ge.common.tc.ws.v1.model;

/**
 * Interface de modèlisation XML d'un destinataire de dossier.
 * 
 */
public interface IDestinataireDossier {

  /**
   * getter du code EDI du destinataire.
   * 
   * @return code EDI
   */
  String getCodeDestinataire();

}
