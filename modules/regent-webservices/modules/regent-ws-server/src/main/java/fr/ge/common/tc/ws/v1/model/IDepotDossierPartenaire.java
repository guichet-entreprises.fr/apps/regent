package fr.ge.common.tc.ws.v1.model;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IDepotDossierPartenaire.
 */
@ResourceXPath("/depot")
public interface IDepotDossierPartenaire {

  /**
   * Get le nom fichier depose.
   *
   * @return le nom fichier depose
   */
  @FieldXPath("nomFichierDepose")
  String getNomFichierDepose();

  /**
   * Set le nom fichier depose.
   *
   * @param nomFichierDepose
   *          le nouveau nom fichier depose
   */
  void setNomFichierDepose(String nomFichierDepose);

  /**
   * Get le chemin fichier depose.
   *
   * @return le chemin fichier depose
   */
  @FieldXPath("cheminFichierDepose")
  String getCheminFichierDepose();

  /**
   * Set le chemin fichier depose.
   *
   * @param cheminFichierDepose
   *          le nouveau chemin fichier depose
   */
  void setCheminFichierDepose(String cheminFichierDepose);
}
