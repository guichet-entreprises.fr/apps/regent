package fr.ge.common.tc.ws.v1.model;

/**
 * Classe d'état pour la validation xml. Il réprésente la validaion à effectuer à la création d'un
 * nouveau dossier et avant d'enregistrer un dossier en base
 * 
 */
public class Init {
  // Nothing to do
}
