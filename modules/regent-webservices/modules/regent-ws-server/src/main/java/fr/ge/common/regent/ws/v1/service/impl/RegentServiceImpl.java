/**
 *
 */
package fr.ge.common.regent.ws.v1.service.impl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.ws.rs.Path;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.regent.service.ServiceGeneationRegentFromDB;
import fr.ge.common.regent.service.ServiceRubriquesManagement;
import fr.ge.common.regent.ws.v1.service.IRegentRestPublicService;
import fr.ge.common.regent.ws.v1.service.IXmlService;
import fr.ge.common.regent.xml.XmlFieldElement;
import fr.ge.common.utils.exception.TechnicalException;
import io.swagger.annotations.Api;

/**
 * Rest Regent service implementation.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Api("Regent REST Public Services")
@Path("/v1")
public class RegentServiceImpl implements IRegentRestPublicService {

    /** The logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(RegentServiceImpl.class);

    @Autowired
    protected ServiceGeneationRegentFromDB serviceGenerationRegentFromDB;

    @Autowired
    protected ServiceRubriquesManagement serviceRubriquesManagment;

    @Autowired
    protected IXmlService xmlService;

    /**
     * {@inheritDoc}
     */
    @Override
    public Response generateMappingJs(String regentVersion, List<String> listTypeEvenement) {
        final List<XmlFieldElement> listTagsByEvenements = this.serviceGenerationRegentFromDB.getTagsByEventsAndVersion(listTypeEvenement, regentVersion);

        if (listTagsByEvenements != null && !listTagsByEvenements.isEmpty()) {
            final StringBuilder listTagsJs = new StringBuilder();
            String evenements = "";

            listTagsJs.append("//Fichier de mapping des champs Regent pour :\r\n");
            listTagsJs.append(String.format("//Version : %s \r\n", regentVersion));

            for (String evenement : listTypeEvenement) {
                evenements = evenements.concat(evenement).concat(", ");
            }

            listTagsJs.append(String.format("//Evenements : %s \r\n", evenements));

            String date = new SimpleDateFormat("yyyy-MM-dd_HHmmss").format(Calendar.getInstance().getTime());
            listTagsJs.append(String.format("//Fichier généré le : %s \r\n", date));

            listTagsJs.append("\r\n");

            listTagsJs.append("var regentVersion = null;  // (V2008.11, V2016.02) \r\n");
            listTagsJs.append("var eventRegent = null; //[] \r\n");
            listTagsJs.append("var authorityType = null; // (CFE, TDR) \r\n");
            listTagsJs.append("var authorityId = null; // (code EDI) \r\n \r\n");

            listTagsJs.append("var regentFields = {}; \r\n");

            for (final XmlFieldElement tag : listTagsByEvenements) {

                if (tag.getEndPath()) {

                    listTagsJs.append(String.format("regentFields['%s']= null;\r\n", String.format("/REGENT-XML%s", tag.getPath().substring(7))));

                    LOGGER.info("A path {} for the rubrique {} was obtained  for the version: {}.", String.format("/REGENT-XML%s", tag.getPath().substring(7)), tag.getRubrique(), regentVersion);
                }
            }

            listTagsJs.append(" \r\n \r\n \r\n// Call to the XML-REGENT generation WS \r\n");
            listTagsJs.append(
                    "var response = nash.service.request('${regent.baseUrl}/v1/private/xml-regent/generate/{regentVersion}/{authorityType}/{authorityId}', regentVersion, authorityType, authorityId) \r\n");
            listTagsJs.append(".dataType('application/json') // \r\n");
            listTagsJs.append(".accept('json') // \r\n");
            listTagsJs.append(".param('listTypeEvenement',eventRegent) \r\n");
            listTagsJs.append(".post(JSON.stringify(regentFields)); \r\n \r\n \r\n");

            listTagsJs.append("// Record the generated XML-REGENT \r\n");
            listTagsJs.append("if (response != null && response.status == 200) { \r\n");
            listTagsJs.append("    var xmlRegentStr = response.asBytes(); \r\n");
            listTagsJs.append("    nash.record.saveFile(\"XML_REGENT_\".concat(authorityId).concat(\"_\").concat(regentVersion).concat(\".xml\"),xmlRegentStr); \r\n");
            listTagsJs.append(" } \r\n");

            LOGGER.info("Call for the service is OK and  it obtained {} rubriques for the version: {}.", listTagsByEvenements.size(), regentVersion);
            return Response.ok(listTagsJs.toString().getBytes()) //
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_OCTET_STREAM) //
                    .header(HttpHeaders.CONTENT_DISPOSITION, String.format("inline; filename=%s.js", "ListTagRegent")) //
                    .build();
        } else {
            LOGGER.error("Technical error occured while getting the list of the tags by version : {}.", regentVersion);
            throw new TechnicalException("Technical error occured while getting the list of the tags by version & evenment");
        }
    }

}
