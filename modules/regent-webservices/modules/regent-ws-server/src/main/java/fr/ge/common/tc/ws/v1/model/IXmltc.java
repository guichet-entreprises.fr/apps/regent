package fr.ge.common.tc.ws.v1.model;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IXmltc.
 */
@ResourceXPath("/GUEN_DMTDU")
public interface IXmltc {

    /**
     * Getter de la version.
     * 
     * @return le version
     */
    @FieldXPath("version")
    String getVersion();

    /**
     * Setter.
     *
     * @param version
     *            le nouveau version
     */
    void setVersion(String version);

    /**
     * Getter.
     * 
     * @return emetteur
     */
    @FieldXPath("emetteur")
    String getEmetteur();

    /**
     * Setter.
     *
     * @param emetteur
     *            le nouveau emetteur
     */
    void setEmetteur(String emetteur);

    /**
     * Getter.
     * 
     * @return destinataire
     */
    @FieldXPath("destinataire")
    String getDestinataire();

    /**
     * Setter.
     *
     * @param destinataire
     *            le nouveau destinataire
     */
    void setDestinataire(String destinataire);

    /**
     * Getter de l'attribut dateHeureGenerationXml.
     * 
     * @return la valeur de dateHeureGenerationXml
     */
    @FieldXPath("dateHeureGenerationXml")
    String getDateHeureGenerationXml();

    /**
     * Setter de l'attribut dateHeureGenerationXml.
     * 
     * @param dateHeureGenerationXml
     *            la nouvelle valeur de dateHeureGenerationXml
     */
    void setDateHeureGenerationXml(String dateHeureGenerationXml);

    /**
     * Getter de l'attribut commentaire.
     * 
     * @return la valeur de commentaire
     */
    @FieldXPath("commentaire")
    String getCommentaire();

    /**
     * Setter de l'attribut commentaire.
     * 
     * @param commentaire
     *            la nouvelle valeur de commentaire
     */
    void setCommentaire(String commentaire);

    /**
     * Getter.
     * 
     * @return codePartenaireEmetteur
     */
    @FieldXPath("dossierUnique/identificationDossierUnique/identifiantDossierUnique/codePartenaireEmetteur")
    String getCodePartenaireEmetteur();

    /**
     * Setter.
     *
     * @param codePartenaireEmetteur
     *            le nouveau code partenaire emetteur
     */
    void setCodePartenaireEmetteur(String codePartenaireEmetteur);

    /**
     * Getter de l'attribut numeroDossierUnique.
     * 
     * @return la valeur de numeroDossierUnique
     */
    @FieldXPath("dossierUnique/identificationDossierUnique/identifiantDossierUnique/numeroDossierUnique")
    String getNumeroDossierUnique();

    /**
     * Setter de l'attribut numeroDossierUnique.
     * 
     * @param numeroDossierUnique
     *            la nouvelle valeur de numeroDossierUnique
     */
    void setNumeroDossierUnique(String numeroDossierUnique);

    /**
     * Getter.
     * 
     * @return typeDossierUnique
     */
    @FieldXPath("dossierUnique/identificationDossierUnique/typeDossierUnique")
    String getTypeDossierUnique();

    /**
     * Setter.
     *
     * @param typeDossierUnique
     *            le nouveau type dossier unique
     */
    void setTypeDossierUnique(String typeDossierUnique);

    /**
     * Getter de l'attribut nomDossier.
     * 
     * @return la valeur de nomDossier
     */
    @FieldXPath("dossierUnique/identificationDossierUnique/nomDossier")
    String getNomDossier();

    /**
     * Setter de l'attribut nomDossier.
     * 
     * @param nomDossier
     *            la nouvelle valeur de nomDossier
     */
    void setNomDossier(String nomDossier);

    /**
     * Getter.
     * 
     * @return nomCorrespondant
     */
    @FieldXPath("dossierUnique/identificationDossierUnique/correspondant/identiteCorrespondant/nomCorrespondant")
    String getNomCorrespondant();

    /**
     * Setter.
     *
     * @param nomCorrespondant
     *            le nouveau nom correspondant
     */
    void setNomCorrespondant(String nomCorrespondant);

    /**
     * Getter.
     * 
     * @return numeroDeVoie
     */
    @FieldXPath("dossierUnique/identificationDossierUnique/correspondant/adresseCorrespondant/numeroDeVoie")
    String getNumeroDeVoie();

    /**
     * Setter.
     *
     * @param numeroDeVoie
     *            le nouveau numero de voie
     */
    void setNumeroDeVoie(String numeroDeVoie);

    /**
     * Getter.
     * 
     * @return numeroDeVoie
     */
    @FieldXPath("dossierUnique/identificationDossierUnique/correspondant/adresseCorrespondant/indiceDeRepetition")
    String getIndiceDeRepetition();

    /**
     * Setter.
     *
     * @param numeroDeVoie
     *            le nouveau numero de voie
     */
    void setIndiceDeRepetition(String indiceDeRepetition);

    /**
     * Getter.
     * 
     * @return typeDeVoie
     */
    @FieldXPath("dossierUnique/identificationDossierUnique/correspondant/adresseCorrespondant/typeDeVoie")
    String getTypeDeVoie();

    /**
     * Setter.
     *
     * @param typeDeVoie
     *            le nouveau type de voie
     */
    void setTypeDeVoie(String typeDeVoie);

    /**
     * Getter.
     * 
     * @return libelleDeVoie
     */
    @FieldXPath("dossierUnique/identificationDossierUnique/correspondant/adresseCorrespondant/libelleDeVoie")
    String getLibelleDeVoie();

    /**
     * Setter.
     *
     * @param libelleDeVoie
     *            le nouveau libelle de voie
     */
    void setLibelleDeVoie(String libelleDeVoie);

    /**
     * Getter.
     * 
     * @return localite
     */
    @FieldXPath("dossierUnique/identificationDossierUnique/correspondant/adresseCorrespondant/localite")
    String getLocalite();

    /**
     * Setter.
     *
     * @param localite
     *            le nouveau localite
     */
    void setLocalite(String localite);

    /**
     * Getter.
     * 
     * @return complementDeLocalisation
     */
    @FieldXPath("dossierUnique/identificationDossierUnique/correspondant/adresseCorrespondant/complementDeLocalisation")
    String getComplementDeLocalisation();

    /**
     * Setter.
     *
     * @param complementDeLocalisation
     *            le nouveau complement de localisation
     */
    void setComplementDeLocalisation(String complementDeLocalisation);

    /**
     * Getter.
     * 
     * @return codePostal
     */
    @FieldXPath("dossierUnique/identificationDossierUnique/correspondant/adresseCorrespondant/codePostal")
    String getCodePostal();

    /**
     * Setter.
     *
     * @param codePostal
     *            le nouveau code postal
     */
    void setCodePostal(String codePostal);

    /**
     * Getter.
     * 
     * @return bureauDistributeur
     */
    @FieldXPath("dossierUnique/identificationDossierUnique/correspondant/adresseCorrespondant/bureauDistributeur")
    String getBureauDistributeur();

    /**
     * Setter.
     *
     * @param bureauDistributeur
     *            le nouveau bureau distributeur
     */
    void setBureauDistributeur(String bureauDistributeur);

    /**
     * Getter.
     * 
     * @return codePays
     */
    @FieldXPath("dossierUnique/identificationDossierUnique/correspondant/adresseCorrespondant/codePays")
    String getCodePays();

    /**
     * Setter.
     *
     * @param codePays
     *            le nouveau code pays
     */
    void setCodePays(String codePays);

    /**
     * Getter.
     * 
     * @return adresseEmail
     */
    @FieldXPath("dossierUnique/identificationDossierUnique/correspondant/adresseCorrespondant/adresseEmail")
    String getAdresseEmail();

    /**
     * Setter.
     *
     * @param adresseEmail
     *            le nouveau adresse email
     */
    void setAdresseEmail(String adresseEmail);

    /**
     * Getter.
     * 
     * @return IDestinataireDossierUnique
     */
    @FieldXPath("dossierUnique/destinataireDossierUnique")
    IDestinataireDossierUnique[] getDestinatairesDossierUnique();

    /**
     * Setter.
     *
     * @param destinataireDossierUnique
     *            le nouveau destinataires dossier unique
     */
    void setDestinatairesDossierUnique(IDestinataireDossierUnique[] destinataireDossierUnique);

    /**
     * Ajoute un element à la liste des destinataires dossier unique.
     * 
     * @return {@link IDestinataireDossierUnique}
     */
    IDestinataireDossierUnique addToDestinatairesDossierUnique();

    /**
     * Getter de l'attribut piecesJointes.
     * 
     * @return la valeur de piecesJointes
     */
    @FieldXPath("dossierUnique/pieceJointe")
    IPieceJointe[] getPiecesJointes();

    /**
     * Retire une IPieceJointe de la liste.
     *
     * @param pj
     *            le pj
     */
    void removeFromPiecesJointes(IPieceJointe pj);

    /**
     * Setter de l'attribut piecesJointes.
     * 
     * @param piecesJointes
     *            la nouvelle valeur de piecesJointes
     */
    void setPiecesJointes(IPieceJointe[] piecesJointes);

    /**
     * Ajoute une pièce jointe à la liste.
     * 
     * @return IPieceJointe
     */
    IPieceJointe addToPiecesJointes();

    /**
     * Getter de l'attribut numeroDeLiasse.
     * 
     * @return la valeur de numeroDeLiasse
     */
    @FieldXPath("dossierUnique/dossierCfe/numeroDeLiasse")
    String getNumeroDeLiasse();

    /**
     * Setter de l'attribut numeroDeLiasse.
     * 
     * @param numeroDeLiasse
     *            la nouvelle valeur de numeroDeLiasse
     */
    void setNumeroDeLiasse(String numeroDeLiasse);

    /**
     * Getter de l'attribut dateHeureDepot.
     * 
     * @return la valeur de dateHeureDepot
     */
    @FieldXPath("dossierUnique/dossierCfe/dateHeureDepot")
    String getDateHeureDepot();

    /**
     * Setter de l'attribut dateHeureDepot.
     * 
     * @param dateHeureDepot
     *            la nouvelle valeur de dateHeureDepot
     */
    void setDateHeureDepot(String dateHeureDepot);

    /**
     * Getter.
     * 
     * @return referenceLiasseFo
     */
    @FieldXPath("dossierUnique/dossierCfe/referenceLiasseFo")
    String getReferenceLiasseFo();

    /**
     * Setter.
     *
     * @param referenceLiasseFo
     *            le nouveau reference liasse fo
     */
    void setReferenceLiasseFo(String referenceLiasseFo);

    /**
     * Getter de l'attribut destinatairesDossierCfe.
     * 
     * @return la valeur de destinatairesDossierCfe
     */
    @FieldXPath("dossierUnique/dossierCfe/destinataireDossierCfe")
    IDestinataireDossierCfe[] getDestinatairesDossierCfe();

    /**
     * Retire IDestinataireDossierCfe de la liste.
     *
     * @param dest
     *            le dest
     */
    void removeFromDestinatairesDossierCfe(IDestinataireDossierCfe dest);

    /**
     * Setter de l'attribut destinatairesDossierCfe.
     * 
     * @param destinatairesDossierCfe
     *            la nouvelle valeur de destinatairesDossierCfe
     */
    void setDestinatairesDossierCfe(IDestinataireDossierCfe[] destinatairesDossierCfe);

    /**
     * Ajoute un IDestinataireDossierCfe à la liste.
     * 
     * @return IDestinataireDossierCfe
     */
    IDestinataireDossierCfe addToDestinatairesDossierCfe();

    /**
     * Getter de l'attribut numeroDossierAutorisation.
     * 
     * @return la valeur de numeroDossierAutorisation
     */
    @FieldXPath("dossierUnique/dossierAutorisation/numeroDossierAutorisation")
    String getNumeroDossierAutorisation();

    /**
     * Setter.
     * 
     * @param numeroDossierAutorisation
     *            numeroDossierAutorisation
     */
    void setNumeroDossierAutorisation(String numeroDossierAutorisation);

    /**
     * Getter.
     * 
     * @return dateHeureDepot
     */
    @FieldXPath("dossierUnique/dossierAutorisation/dateHeureDepot")
    String getDateHeureDepotAutorisation();

    /**
     * Setter.
     *
     * @param dateHeureDepot
     *            le nouveau date heure depot autorisation
     */
    void setDateHeureDepotAutorisation(String dateHeureDepot);

    /**
     * Getter de l'attribut sousDossierAutorisation.
     * 
     * @return la valeur de sousDossierAutorisation
     */
    @FieldXPath("dossierUnique/dossierAutorisation/sousDossierAutorisation")
    ISousDossierAutorisation[] getSousDossiersAutorisations();

    /**
     * Setter de l'attribut sousDossierAutorisation.
     *
     * @param sousDossiersAutorisations
     *            le nouveau sous dossiers autorisations
     */
    void setSousDossiersAutorisations(ISousDossierAutorisation[] sousDossiersAutorisations);

    /**
     * Ajoute un sous dossier autorisation à la liste.
     * 
     * @return ISousDossierAutorisation
     */
    ISousDossierAutorisation addToSousDossiersAutorisations();

    /**
     * Ajoute un dossierCfe.
     * 
     * @return ISousDossierAutorisation
     */
    IDossierCfe addToDossierCfe();

    /**
     * Getter de l'attribut destinataireDossierAh.
     * 
     * @return la valeur de destinataireDossierAh
     */
    @FieldXPath("dossierUnique/dossierAutorisation/informationDestinataireAutorisation")
    IDestinataireDossierAh[] getDestinatairesDossierAh();

    /**
     * Supprime un destinataireDossierAh de la liste.
     *
     * @param dest
     *            le dest
     */
    void removeFromDestinatairesDossierAh(IDestinataireDossierAh dest);

    /**
     * Ajoute un destinataireDossierAh à la liste.
     * 
     * @return IDestinataireDossierAh
     */
    IDestinataireDossierAh addToDestinatairesDossierAh();

    /**
     * Get le dossier cfe.
     *
     * @return le dossier cfe
     */
    // Utile au filtre XMLTC
    @FieldXPath("dossierUnique/dossierCfe")
    IDossierCfe getDossierCfe();

    /**
     * Set le dossier cfe.
     *
     * @param dossiercfe
     *            le nouveau dossier cfe
     */
    void setDossierCfe(IDossierCfe dossiercfe);

    /**
     * Get le dossier autorisation.
     *
     * @return le dossier autorisation
     */
    @FieldXPath("dossierUnique/dossierAutorisation")
    IDossierAutorisation getDossierAutorisation();

    /**
     * Set le dossier autorisation.
     *
     * @param dossierAutorisation
     *            le nouveau dossier autorisation
     */
    void setDossierAutorisation(IDossierAutorisation dossierAutorisation);

}
