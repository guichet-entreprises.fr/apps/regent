package fr.ge.common.tc.ws.v1.model;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;
import org.xmlfield.core.types.XmlString;

/**
 * Le Interface ISousDossierAutorisation.
 */
@ResourceXPath("/sousDossierAutorisation")
public interface ISousDossierAutorisation {

  /**
   * Getter.
   * 
   * @return indice sous dossier autorisation
   */
  @FieldXPath("indiceSousDossier")
  String getIndiceSousDossier();

  /**
   * Setter.
   * 
   * @param indiceSousDossier
   *          indiceSousDossier
   */
  void setIndiceSousDossier(String indiceSousDossier);

  /**
   * Getter.
   * 
   * @return indiceAutorisationXml
   */
  @FieldXPath("indiceAutorisationXml")
  String getIndiceAutorisationXml();

  /**
   * Setter.
   * 
   * @param indiceAutorisationXml
   *          indiceAutorisationXml
   */
  void setIndiceAutorisationXml(String indiceAutorisationXml);

  /**
   * Getter.
   * 
   * @return personnePhysique/nom
   */
  @FieldXPath("personnePhysique/nom")
  String getPersonnePhysiqueNom();

  /**
   * Setter.
   * 
   * @param personnePhysiqueNom
   *          personnePhysique/nom
   */
  void setPersonnePhysiqueNom(String personnePhysiqueNom);

  /**
   * Getter.
   * 
   * @return personnePhysique/prenom
   */
  @FieldXPath("personnePhysique/prenom")
  XmlString[] getPersonnePhysiquePrenoms();

  /**
   * Setter.
   * 
   * @param personnePhysiquePrenoms
   *          personnePhysique/prenom
   */
  void setPersonnePhysiquePrenoms(XmlString[] personnePhysiquePrenoms);

  /**
   * Ajoute un prenom à la liste.
   *
   * @return le xml string
   */
  XmlString addToPersonnePhysiquePrenoms();

  /**
   * Getter.
   * 
   * @return personneMorale/denomination
   */
  @FieldXPath("personneMorale/denomination")
  String getPersonneMoraleDenomination();

  /**
   * Setter.
   * 
   * @param personneMoraleDenomination
   *          personneMorale/denomination
   */
  void setPersonneMoraleDenomination(String personneMoraleDenomination);

}
