package fr.ge.common.tc.ws.v1.model;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * L'Interface IDossierCfe; représente le bloc dossierCfe du XML-TC, contient
 * les informations des dossiers pour chaque destinataire.
 */
@ResourceXPath("/dossierCfe")
public interface IDossierCfe {
    // utile au filtre XMLTC pour supprimer la balise

    /**
     * Getter.
     * 
     * @return numeroDeLiasse
     */
    @FieldXPath("numeroDeLiasse")
    String getNumeroDeLiasse();

    /**
     * Setter.
     *
     * @param numeroDeLiasse
     *            le nouveau numéro de liasse
     */
    void setNumeroDeLiasse(String numeroDeLiasse);

    /**
     * Getter.
     * 
     * @return dateHeureDepot
     */
    @FieldXPath("dateHeureDepot")
    String getDateHeureDepot();

    /**
     * Setter.
     *
     * @param dateHeureDepot
     *            le nouvelle date de dépot.
     */
    void setDateHeureDepot(String dateHeureDepot);

    /**
     * Getter.
     * 
     * @return referenceLiasseFo
     */
    @FieldXPath("referenceLiasseFo")
    String getReferenceLiasseFo();

    /**
     * Setter.
     *
     * @param referenceLiasseFo
     *            le nouvelle référence de liasse FO
     */
    void setReferenceLiasseFo(String referenceLiasseFo);
}
