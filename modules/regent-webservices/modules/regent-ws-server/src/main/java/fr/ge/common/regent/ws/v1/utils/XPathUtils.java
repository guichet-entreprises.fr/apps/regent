/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.regent.ws.v1.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * @author mtakerra
 *
 */
public class XPathUtils {

    private static final String SLASH = "/";
    private static final String R_BRACKET = "]";
    private static final String L_BRACKET = "[";

    /**
     * Looks for the last '/' and returns the name of the last element
     * 
     * @param xpath
     * @return the child element name or null
     */
    public static final String getChildElementName(String xpath) {
        if (StringUtils.isEmpty(xpath)) {
            return null;
        }
        String childName = xpath.substring(xpath.lastIndexOf(SLASH) + 1);
        return stripIndex(childName);
    }

    /**
     * returns the xpath if traversing up the tree one node i.e.
     * /root/suspension_rec returns /root
     * 
     * @param xpath
     * @return
     */
    public static final String getParentXPath(String xpath) {
        if (StringUtils.isEmpty(xpath) || xpath.lastIndexOf(SLASH) <= 0) {
            return null;
        }
        return xpath.substring(0, xpath.lastIndexOf(SLASH));
    }

    /**
     * returns the index of the child element xpath i.e. /suspension_rec[3]
     * returns 3. /suspension_rec defaults to 1
     * 
     * @param xpath
     * @return 1, the index, or null if the provided xpath is empty
     */
    public static Integer getChildElementIndex(String xpath) {
        if (StringUtils.isEmpty(xpath)) {
            return null;
        }

        if (xpath.endsWith(R_BRACKET)) {
            String value = xpath.substring(xpath.lastIndexOf(L_BRACKET) + 1, xpath.lastIndexOf(R_BRACKET));
            if (StringUtils.isNumeric(value)) {
                return Integer.valueOf(value);
            }
        }
        return 1;
    }

    /**
     * @param xpath
     * @param childIndex
     * @return
     */
    public static String createPositionXpath(String xpath, Integer childIndex) {
        if (StringUtils.isEmpty(xpath)) {
            return null;
        }
        return stripIndex(xpath) + "[position()<" + childIndex + "]";
    }

    /**
     * @param childName
     * @return
     */
    private static String stripIndex(String childName) {
        if (childName.endsWith(R_BRACKET)) {
            return childName.substring(0, childName.lastIndexOf(L_BRACKET));
        } else {
            return childName;
        }
    }

}