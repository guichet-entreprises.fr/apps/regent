package fr.ge.common.tc.ws.v1.model;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * L'Interface IAdressePayeur; représente le bloc adressePayeur du XML-TC.
 */
@ResourceXPath("/paiementDossier/adressePayeur")
public interface IAdressePayeur {

    /**
     * Getter de l'attribut bureauDistributeur.
     * 
     * @return la valeur de bureauDistributeur
     */
    @FieldXPath("bureauDistributeur")
    String getBureauDistributeur();

    /**
     * Setter de l'attribut bureauDistributeur.
     * 
     * @param bureauDistributeur
     *            la nouvelle valeur de bureauDistributeur
     */
    void setBureauDistributeur(String bureauDistributeur);

    /**
     * Getter de l'attribut codePays.
     * 
     * @return la valeur de codePays
     */
    @FieldXPath("codePays")
    String getCodePays();

    /**
     * Setter de l'attribut codePays.
     * 
     * @param codePays
     *            la nouvelle valeur de codePays
     */
    void setCodePays(String codePays);

    /**
     * Getter de l'attribut adresseEmail.
     * 
     * @return la valeur de adresseEmail
     */
    @FieldXPath("adresseEmail")
    String getAdresseEmail();

    /**
     * Setter de l'attribut adresseEmail.
     * 
     * @param adresseEmail
     *            la nouvelle valeur de adresseEmail
     */
    void setAdresseEmail(String adresseEmail);

    /**
     * Getter de l'attribut numeroDeVoie.
     * 
     * @return la valeur de numeroDeVoie
     */
    @FieldXPath("numeroDeVoie")
    String getNumeroDeVoie();

    /**
     * Setter de l'attribut numeroDeVoie.
     * 
     * @param numeroDeVoie
     *            la nouvelle valeur de numeroDeVoie
     */
    void setNumeroDeVoie(String numeroDeVoie);

    /**
     * Getter de l'attribut indiceDeRepetition.
     * 
     * @return la valeur de indiceDeRepetition
     */
    @FieldXPath("indiceDeRepetition")
    String getIndiceDeRepetition();

    /**
     * Setter de l'attribut indiceDeRepetition.
     * 
     * @param indiceDeRepetition
     *            la nouvelle valeur de indiceDeRepetition
     */
    void setIndiceDeRepetition(String indiceDeRepetition);

    /**
     * Getter de l'attribut typeDeVoie.
     * 
     * @return la valeur de typeDeVoie
     */
    @FieldXPath("typeDeVoie")
    String getTypeDeVoie();

    /**
     * Setter de l'attribut typeDeVoie.
     * 
     * @param typeDeVoie
     *            la nouvelle valeur de typeDeVoie
     */
    void setTypeDeVoie(String typeDeVoie);

    /**
     * Getter de l'attribut libelleDeVoie.
     * 
     * @return la valeur de libelleDeVoie
     */
    @FieldXPath("libelleDeVoie")
    String getLibelleDeVoie();

    /**
     * Setter de l'attribut libelleDeVoie.
     * 
     * @param libelleDeVoie
     *            la nouvelle valeur de libelleDeVoie
     */
    void setLibelleDeVoie(String libelleDeVoie);

    /**
     * Getter de l'attribut localite.
     * 
     * @return la valeur de localite
     */
    @FieldXPath("localite")
    String getLocalite();

    /**
     * Setter de l'attribut localite.
     * 
     * @param localite
     *            la nouvelle valeur de localite
     */
    void setLocalite(String localite);

    /**
     * Getter de l'attribut complementLocalisation.
     * 
     * @return la valeur de complementLocalisation
     */
    @FieldXPath("complementDeLocalisation")
    String getComplementLocalisation();

    /**
     * Setter de l'attribut complementLocalisation.
     * 
     * @param complementLocalisation
     *            la nouvelle valeur de complementLocalisation
     */
    void setComplementLocalisation(String complementLocalisation);

    /**
     * Getter de l'attribut codePostal.
     * 
     * @return la valeur de codePostal
     */
    @FieldXPath("codePostal")
    String getCodePostal();

    /**
     * Setter de l'attribut codePostal.
     * 
     * @param codePostal
     *            la nouvelle valeur de codePostal
     */
    void setCodePostal(String codePostal);

}
