/**
 * 
 */
package fr.ge.common.tc.ws.v1.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xmlfield.core.XmlField;
import org.xmlfield.core.XmlFieldFactory;
import org.xmlfield.core.types.XmlString;

import fr.ge.common.regent.constante.enumeration.InfosXmlTcEnum;
import fr.ge.common.regent.service.ServiceGeneationRegentFromDB;
import fr.ge.common.regent.service.impl.ServiceValidationXmlTcImpl;
import fr.ge.common.regent.ws.v1.bean.DestinataireDossierCfe;
import fr.ge.common.regent.ws.v1.bean.DestinataireDossierUnique;
import fr.ge.common.regent.ws.v1.bean.DossierCfe;
import fr.ge.common.regent.ws.v1.bean.IdentificationDossierUnique;
import fr.ge.common.regent.ws.v1.bean.PieceJointe;
import fr.ge.common.regent.ws.v1.bean.TcBean;
import fr.ge.common.tc.ws.v1.model.IAdressePayeur;
import fr.ge.common.tc.ws.v1.model.IDestinataireDossierCfe;
import fr.ge.common.tc.ws.v1.model.IDestinataireDossierUnique;
import fr.ge.common.tc.ws.v1.model.IDossierCfe;
import fr.ge.common.tc.ws.v1.model.IPaiementDossier;
import fr.ge.common.tc.ws.v1.model.IPieceJointe;
import fr.ge.common.tc.ws.v1.model.IXmltc;
import fr.ge.common.tc.ws.v1.service.contract.IXmlTcGenerator;

/**
 * 
 * Implementation de la génération du XML TC pour les authtorités de type PGUEN
 * notamment les Greffes
 * 
 * @author $Author: traimbau $
 * @version $Revision: 0 $
 */

@Service("xmlTcGuenGenerator")
public class XmlTcGuenGeneratorImpl implements IXmlTcGenerator {

    /** The logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(XmlTcGuenGeneratorImpl.class);

    @Autowired
    private XmlFieldFactory xmlFieldFactory;

    @Autowired
    protected ServiceGeneationRegentFromDB serviceGenerationRegentFromDB;

    @Autowired
    protected ServiceValidationXmlTcImpl ServiceValidationXmlTc;

    private String XMLTC_VERSION = "V2012.02";
    private IXmltc xml;

    /**
     * Constructeur de la classe.
     *
     */
    public XmlTcGuenGeneratorImpl() {
        // TODO Auto-generated constructor stub
    }

    /**
     * {@inheritDoc}
     * 
     *
     */
    @Override
    public String generate(TcBean xmltcBean, InfosXmlTcEnum infos) throws Exception {

        LOGGER.info("Generation d'un XML-TC PGUEN");

        XmlField xf = this.xmlFieldFactory.getXmlField();
        xml = xf.newObject(IXmltc.class);

        if (xmltcBean != null) {
            LOGGER.debug("Ajout donnés racine xmltc");
            xml.setVersion(infos.getVersionXmlTc());
            xml.setEmetteur(infos.getEmetteurCode());
            xml.setDestinataire(infos.getDestinataireCode());
            // xml.setCodePartenaireEmetteur(xmltcBean.getCodePartenaireEmetteur());

            // xmlTcContent
            xml.setDateHeureGenerationXml(getDateGeneration());
            if (xmltcBean.getCommentaire() != null && !xmltcBean.getCommentaire().isEmpty()) {
                xml.setCommentaire(xmltcBean.getCommentaire());
            }
        }

        // Dossier unique
        if (xmltcBean.getDossierUnique() != null) {
            // if (xmltcBean.getDossierUnique().getIdentificationDossierUnique()
            // != null) {
            // LOGGER.debug("Ajout dossier unique");
            // ajoutDossierUnique(xmltcBean.getDossierUnique());
            // }

            // Identification dossier unique
            if (xmltcBean.getDossierUnique().getIdentificationDossierUnique() != null) {
                LOGGER.debug("Ajout correspondant");
                ajoutIdentificationDossierUnique(xmltcBean.getDossierUnique().getIdentificationDossierUnique());
            }

            // Destinataires dossier unique
            if (xmltcBean.getDossierUnique().getDestinataireDossierUnique() != null) {
                LOGGER.debug("Ajout des destinataires dossier unique");
                ajoutDestinataireDossierUnique(xmltcBean.getDossierUnique().getDestinataireDossierUnique());
            }

            // Pieces jointes
            if (xmltcBean.getDossierUnique().getPieceJointe() != null) {
                LOGGER.debug("Ajout des pièces jointes");
                ajoutPiecesJointes(xmltcBean.getDossierUnique().getPieceJointe());
            }

            // Dossier CFE
            if (xmltcBean.getDossierUnique().getDossierCfe() != null) {
                LOGGER.debug("Ajout des destinataires dossier CFE");
                ajoutDossierCfe(xmltcBean.getDossierUnique().getDossierCfe());
            }

        }

        String retour = xf.objectToXml(xml);

        // Validation du XML TC
        this.ServiceValidationXmlTc.validateXmlTc(XMLTC_VERSION, retour);

        return retour;
    }

    /**
     * Get the generation date of XML-TC with the right format
     * 
     * @return generation date
     */
    private String getDateGeneration() {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date dateGenerationXml = new Date();

        return simpleDateFormat.format(dateGenerationXml);
    }

    private void ajoutIdentificationDossierUnique(IdentificationDossierUnique identificationDossierUnique) {
        // Identifiant Dossier Unique
        if (identificationDossierUnique.getIdentifiantDossierUnique().getCodePartenaireEmetteur() != null
                && !identificationDossierUnique.getIdentifiantDossierUnique().getCodePartenaireEmetteur().isEmpty()) {
            xml.setCodePartenaireEmetteur(identificationDossierUnique.getIdentifiantDossierUnique().getCodePartenaireEmetteur());
        }
        if (identificationDossierUnique.getIdentifiantDossierUnique().getNumeroDossierUnique() != null
                && !identificationDossierUnique.getIdentifiantDossierUnique().getNumeroDossierUnique().isEmpty()) {
            xml.setNumeroDossierUnique(identificationDossierUnique.getIdentifiantDossierUnique().getNumeroDossierUnique());
        }
        // Type dossier unique
        if (identificationDossierUnique.getTypeDossierUnique() != null && !identificationDossierUnique.getTypeDossierUnique().isEmpty()) {
            xml.setTypeDossierUnique(identificationDossierUnique.getTypeDossierUnique());
        }
        // Nom dossier
        if (identificationDossierUnique.getNomDossier() != null && !identificationDossierUnique.getNomDossier().isEmpty()) {
            xml.setNomDossier(identificationDossierUnique.getNomDossier());
        }
        // Correspondant
        if (identificationDossierUnique.getCorrespondant().getIdentiteCorrespondant().getNomCorrespondant() != null
                && !identificationDossierUnique.getCorrespondant().getIdentiteCorrespondant().getNomCorrespondant().isEmpty()) {
            xml.setNomCorrespondant(identificationDossierUnique.getCorrespondant().getIdentiteCorrespondant().getNomCorrespondant());
        }
        // Adresse correspondant
        if (identificationDossierUnique.getCorrespondant().getAdresseCorrespondant().getNumeroDeVoie() != null
                && !identificationDossierUnique.getCorrespondant().getAdresseCorrespondant().getNumeroDeVoie().isEmpty()) {
            xml.setNumeroDeVoie(identificationDossierUnique.getCorrespondant().getAdresseCorrespondant().getNumeroDeVoie());
        }
        if (identificationDossierUnique.getCorrespondant().getAdresseCorrespondant().getIndiceDeRepetition() != null
                && !identificationDossierUnique.getCorrespondant().getAdresseCorrespondant().getIndiceDeRepetition().isEmpty()) {
            xml.setIndiceDeRepetition(identificationDossierUnique.getCorrespondant().getAdresseCorrespondant().getIndiceDeRepetition());
        }
        if (StringUtils.isNotEmpty(identificationDossierUnique.getCorrespondant().getAdresseCorrespondant().getLibelleDeVoie())) {
            final String typeDeVoie = Optional.ofNullable(identificationDossierUnique.getCorrespondant().getAdresseCorrespondant().getTypeDeVoie()) //
                    .filter(StringUtils::isNotEmpty) //
                    .orElse(StringUtils.EMPTY) //
            ;
            final String libelleDeVoie = Optional.of(identificationDossierUnique.getCorrespondant().getAdresseCorrespondant().getLibelleDeVoie()) //
                    .filter(StringUtils::isNotEmpty) //
                    .orElse(StringUtils.EMPTY) //
            ;
            xml.setTypeDeVoie(null);
            xml.setLibelleDeVoie(String.join(" ", typeDeVoie, libelleDeVoie).trim());
        }
        if (identificationDossierUnique.getCorrespondant().getAdresseCorrespondant().getLocalite() != null
                && !identificationDossierUnique.getCorrespondant().getAdresseCorrespondant().getLocalite().isEmpty()) {
            xml.setLocalite(identificationDossierUnique.getCorrespondant().getAdresseCorrespondant().getLocalite());
        }
        if (identificationDossierUnique.getCorrespondant().getAdresseCorrespondant().getComplementDeLocalisation() != null
                && !identificationDossierUnique.getCorrespondant().getAdresseCorrespondant().getComplementDeLocalisation().isEmpty()) {
            xml.setComplementDeLocalisation(identificationDossierUnique.getCorrespondant().getAdresseCorrespondant().getComplementDeLocalisation());
        }
        if (identificationDossierUnique.getCorrespondant().getAdresseCorrespondant().getCodePostal() != null
                && !identificationDossierUnique.getCorrespondant().getAdresseCorrespondant().getCodePostal().isEmpty()) {
            xml.setCodePostal(identificationDossierUnique.getCorrespondant().getAdresseCorrespondant().getCodePostal());
        }
        if (identificationDossierUnique.getCorrespondant().getAdresseCorrespondant().getBureauDistributeur() != null
                && !identificationDossierUnique.getCorrespondant().getAdresseCorrespondant().getBureauDistributeur().isEmpty()) {
            xml.setBureauDistributeur(identificationDossierUnique.getCorrespondant().getAdresseCorrespondant().getBureauDistributeur());
        }
        if (identificationDossierUnique.getCorrespondant().getAdresseCorrespondant().getCodePays() != null
                && !identificationDossierUnique.getCorrespondant().getAdresseCorrespondant().getCodePays().isEmpty()) {
            xml.setCodePays(identificationDossierUnique.getCorrespondant().getAdresseCorrespondant().getCodePays());
        }
        if (identificationDossierUnique.getCorrespondant().getAdresseCorrespondant().getAdresseEmail() != null
                && !identificationDossierUnique.getCorrespondant().getAdresseCorrespondant().getAdresseEmail().isEmpty()) {
            xml.setAdresseEmail(identificationDossierUnique.getCorrespondant().getAdresseCorrespondant().getAdresseEmail());
        }
    }

    private void ajoutDestinataireDossierUnique(List<DestinataireDossierUnique> listeDestDossierUnique) {
        for (DestinataireDossierUnique destinatairesDossierUnique : listeDestDossierUnique) {
            IDestinataireDossierUnique dest = xml.addToDestinatairesDossierUnique();
            dest.setRoleDestinataire(destinatairesDossierUnique.getRoleDestinataire());
            dest.setCodePartenaire(destinatairesDossierUnique.getCodeDestinataire().getCodePartenaire());
            dest.setCodeEdi(destinatairesDossierUnique.getCodeDestinataire().getCodeEdi());
            dest.setLibelleIntervenant(destinatairesDossierUnique.getCodeDestinataire().getLibelleIntervenant());
        }
    }

    private void ajoutPiecesJointes(List<PieceJointe> listePj) {
        for (PieceJointe piecesJointes : listePj) {
            IPieceJointe pj = xml.addToPiecesJointes();
            pj.setIndicePieceJointe(piecesJointes.getIndicePieceJointe());
            pj.setTypePieceJointe(piecesJointes.getTypePieceJointe());
            pj.setFormatPieceJointe(piecesJointes.getFormatPieceJointe());
            pj.setFichierPieceJointe(piecesJointes.getFichierPieceJointe());
        }
    }

    private void ajoutDossierCfe(DossierCfe dossierCfe) {

        IDossierCfe destdossCfe = xml.addToDossierCfe();
        destdossCfe.setNumeroDeLiasse(dossierCfe.getNumeroDeLiasse());
        destdossCfe.setDateHeureDepot(getDateGeneration());
        destdossCfe.setReferenceLiasseFo(dossierCfe.getReferenceLiasseFo());
        ajoutDestinataireDossierCfe(dossierCfe.getDestinataireDossierCfe());
    }

    private void ajoutDestinataireDossierCfe(List<DestinataireDossierCfe> listeDestDossierCfe) {
        for (DestinataireDossierCfe DestinatairesDossierCfe : listeDestDossierCfe) {
            IDestinataireDossierCfe destdossCfe = xml.addToDestinatairesDossierCfe();

            // Role destinataire
            if (DestinatairesDossierCfe.getRoleDestinataire() != null && !DestinatairesDossierCfe.getRoleDestinataire().isEmpty()) {
                destdossCfe.setRoleDestinataire(DestinatairesDossierCfe.getRoleDestinataire());
            }

            // Code Edi destinataire
            if (DestinatairesDossierCfe.getCodeEdiDestinataire() != null && !DestinatairesDossierCfe.getCodeEdiDestinataire().isEmpty()) {
                destdossCfe.setCodeDestinataire(DestinatairesDossierCfe.getCodeEdiDestinataire());
            }
            // indices des piecesjointes
            XmlString[] piecesJointes = new XmlString[DestinatairesDossierCfe.getIndicePieceJointe().size()];
            int indicePieceJointe = 0;
            for (String pieceJointe : DestinatairesDossierCfe.getIndicePieceJointe()) {
                XmlString piece = destdossCfe.addToIndicesPiecesJointes();
                piece.setString(pieceJointe);
                piecesJointes[indicePieceJointe] = piece;
                indicePieceJointe++;
            }
            destdossCfe.setIndicesPiecesJointes(piecesJointes);

            // Paiement dossiers Cfe
            if (DestinatairesDossierCfe.getPaiementDossier() != null) {
                IPaiementDossier paiementDossierCfe = destdossCfe.newPaiementDossier();
                if (DestinatairesDossierCfe.getPaiementDossier().getIdentitePayeur().getNomPayeur() != null
                        && !DestinatairesDossierCfe.getPaiementDossier().getIdentitePayeur().getNomPayeur().isEmpty()) {
                    paiementDossierCfe.setNomPayeur(DestinatairesDossierCfe.getPaiementDossier().getIdentitePayeur().getNomPayeur());
                }

                IAdressePayeur adressePayDossierCfe = paiementDossierCfe.newAdressePayeur();
                if (DestinatairesDossierCfe.getPaiementDossier().getAdressePayeur().getNumeroDeVoie() != null
                        && !DestinatairesDossierCfe.getPaiementDossier().getAdressePayeur().getNumeroDeVoie().isEmpty()) {
                    adressePayDossierCfe.setNumeroDeVoie(DestinatairesDossierCfe.getPaiementDossier().getAdressePayeur().getNumeroDeVoie());
                }
                if (DestinatairesDossierCfe.getPaiementDossier().getAdressePayeur().getIndiceDeRepetition() != null
                        && !DestinatairesDossierCfe.getPaiementDossier().getAdressePayeur().getIndiceDeRepetition().isEmpty()) {
                    adressePayDossierCfe.setIndiceDeRepetition(DestinatairesDossierCfe.getPaiementDossier().getAdressePayeur().getIndiceDeRepetition());
                }
                if (StringUtils.isNotEmpty(DestinatairesDossierCfe.getPaiementDossier().getAdressePayeur().getLibelleDeVoie())) {

                    final String typeDeVoie = Optional.ofNullable(DestinatairesDossierCfe.getPaiementDossier().getAdressePayeur().getTypeDeVoie()) //
                            .filter(StringUtils::isNotEmpty) //
                            .orElse(StringUtils.EMPTY) //
                    ;
                    final String libelleDeVoie = Optional.of(DestinatairesDossierCfe.getPaiementDossier().getAdressePayeur().getLibelleDeVoie()) //
                            .filter(StringUtils::isNotEmpty) //
                            .orElse(StringUtils.EMPTY) //
                    ;
                    adressePayDossierCfe.setTypeDeVoie(null);
                    adressePayDossierCfe.setLibelleDeVoie(String.join(" ", typeDeVoie, libelleDeVoie).trim());
                }
                if (DestinatairesDossierCfe.getPaiementDossier().getAdressePayeur().getLocalite() != null && !DestinatairesDossierCfe.getPaiementDossier().getAdressePayeur().getLocalite().isEmpty()) {
                    adressePayDossierCfe.setLocalite(DestinatairesDossierCfe.getPaiementDossier().getAdressePayeur().getLocalite());
                }
                if (DestinatairesDossierCfe.getPaiementDossier().getAdressePayeur().getComplementDeLocalisation() != null
                        && !DestinatairesDossierCfe.getPaiementDossier().getAdressePayeur().getComplementDeLocalisation().isEmpty()) {
                    adressePayDossierCfe.setComplementLocalisation(DestinatairesDossierCfe.getPaiementDossier().getAdressePayeur().getComplementDeLocalisation());
                }
                if (DestinatairesDossierCfe.getPaiementDossier().getAdressePayeur().getCodePostal() != null
                        && !DestinatairesDossierCfe.getPaiementDossier().getAdressePayeur().getCodePostal().isEmpty()) {
                    adressePayDossierCfe.setCodePostal(DestinatairesDossierCfe.getPaiementDossier().getAdressePayeur().getCodePostal());
                }
                if (DestinatairesDossierCfe.getPaiementDossier().getAdressePayeur().getBureauDistributeur() != null
                        && !DestinatairesDossierCfe.getPaiementDossier().getAdressePayeur().getBureauDistributeur().isEmpty()) {
                    adressePayDossierCfe.setBureauDistributeur(DestinatairesDossierCfe.getPaiementDossier().getAdressePayeur().getBureauDistributeur());
                }
                if (DestinatairesDossierCfe.getPaiementDossier().getAdressePayeur().getCodePays() != null && !DestinatairesDossierCfe.getPaiementDossier().getAdressePayeur().getCodePays().isEmpty()) {
                    adressePayDossierCfe.setCodePays(DestinatairesDossierCfe.getPaiementDossier().getAdressePayeur().getCodePays());
                }
                if (DestinatairesDossierCfe.getPaiementDossier().getAdressePayeur().getAdresseEmail() != null
                        && !DestinatairesDossierCfe.getPaiementDossier().getAdressePayeur().getAdresseEmail().isEmpty()) {
                    adressePayDossierCfe.setAdresseEmail(DestinatairesDossierCfe.getPaiementDossier().getAdressePayeur().getAdresseEmail());
                }
                if (DestinatairesDossierCfe.getPaiementDossier().getMontantFormalite() != null && !DestinatairesDossierCfe.getPaiementDossier().getMontantFormalite().isEmpty()) {
                    paiementDossierCfe.setMontantFormalite(DestinatairesDossierCfe.getPaiementDossier().getMontantFormalite());
                }
                if (DestinatairesDossierCfe.getPaiementDossier().getReferencePaiement() != null && !DestinatairesDossierCfe.getPaiementDossier().getReferencePaiement().isEmpty()) {
                    paiementDossierCfe.setReferencePaiement(DestinatairesDossierCfe.getPaiementDossier().getReferencePaiement());
                }
                if (DestinatairesDossierCfe.getPaiementDossier().getDateHeurePaiement() != null && !DestinatairesDossierCfe.getPaiementDossier().getDateHeurePaiement().isEmpty()) {
                    paiementDossierCfe.setDateHeurePaiement(DestinatairesDossierCfe.getPaiementDossier().getDateHeurePaiement());
                }
            }
            // Numéro de liasse
            destdossCfe.setIndiceLiasseXml(DestinatairesDossierCfe.getIndiceLiasseXml());
        }
    }

}
