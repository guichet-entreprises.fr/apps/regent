package fr.ge.common.tc.ws.v1.model;

import org.joda.time.DateTime;
import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;
import org.xmlfield.validation.annotations.NotEmpty;
import org.xmlfield.validation.annotations.Values;

import fr.ge.common.regent.bean.xml.IXml;

/**
 * Contenu XML pour la table dossier_transfert.
 * 
 */
@ResourceXPath("/dossier")
public interface IDossier extends IXml {

    /**
     * Get le numero dossier.
     *
     * @return le numero dossier
     */
    @FieldXPath("numeroDossier")
    @NotEmpty(groups = { Init.class, Save.class })
    String getNumeroDossier();

    /**
     * Set le numero dossier.
     *
     * @param numeroDossier
     *            le nouveau numero dossier
     */
    void setNumeroDossier(String numeroDossier);

    /**
     * Get le nom fichier dmtdu.
     *
     * @return le nom fichier dmtdu
     */
    @FieldXPath("nomFichierDMTDU")
    @NotEmpty(groups = { Init.class, Save.class })
    String getNomFichierDMTDU();

    /**
     * Set le nom fichier dmtdu.
     *
     * @param nomFichierDMTDU
     *            le nouveau nom fichier dmtdu
     */
    void setNomFichierDMTDU(String nomFichierDMTDU);

    /**
     * Get le nom dossier.
     *
     * @return le nom dossier
     */
    @FieldXPath("nomDossier")
    @NotEmpty(groups = { Init.class, Save.class })
    String getNomDossier();

    /**
     * Set le nom dossier.
     *
     * @param nomDossier
     *            le nouveau nom dossier
     */
    void setNomDossier(String nomDossier);

    /**
     * Get le date maj.
     *
     * @return le date maj
     */
    @FieldXPath("dateMaj")
    @NotEmpty(groups = { Save.class })
    DateTime getDateMaj();

    /**
     * Set le date maj.
     *
     * @param dateMaj
     *            le nouveau date maj
     */
    void setDateMaj(DateTime dateMaj);

    /**
     * Get le date generation.
     *
     * @return le date generation
     */
    @FieldXPath("dateGeneration")
    @NotEmpty(groups = { Init.class, Save.class })
    String getDateGeneration();

    /**
     * Set le date generation.
     *
     * @param dateGeneration
     *            le nouveau date generation
     */
    void setDateGeneration(String dateGeneration);

    /**
     * Get le date validation.
     *
     * @return le date validation
     */
    @FieldXPath("dateValidation")
    String getDateValidation();

    /**
     * Set le date validation.
     *
     * @param datevalidation
     *            le nouveau date validation
     */
    void setDateValidation(String datevalidation);

    /**
     * Get le etat.
     *
     * @return le etat
     */
    @FieldXPath("etat")
    @NotEmpty(groups = { Save.class })
    @Values(string = { "NOUVEAU", "TRAITE", "A_REJOUER", "KO", "TRANSMIS_CFE", "VALIDE AH", "REJETE" }, groups = { Save.class })
    String getEtat();

    /**
     * Set le etat.
     *
     * @param etat
     *            le nouveau etat
     */
    void setEtat(String etat);

    /**
     * Get le evenements.
     *
     * @return le evenements
     */
    @FieldXPath("historiqueTech/evenement")
    @NotEmpty(groups = { Save.class })
    IEvenement[] getEvenements();

    /**
     * Ajoute le to evenements.
     *
     * @return le i evenement
     */
    IEvenement addToEvenements();

    /**
     * Set le evenements.
     *
     * @param evenements
     *            le nouveau evenements
     */
    void setEvenements(IEvenement[] evenements);

}
