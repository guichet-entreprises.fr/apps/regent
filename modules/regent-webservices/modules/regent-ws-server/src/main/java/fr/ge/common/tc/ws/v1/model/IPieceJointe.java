package fr.ge.common.tc.ws.v1.model;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IPieceJointe.
 */
@ResourceXPath("/pieceJointe")
public interface IPieceJointe {

  /**
   * Getter.
   * 
   * @return indicePieceJointe
   */
  @FieldXPath("indicePieceJointe")
  String getIndicePieceJointe();

  /**
   * Setter.
   *
   * @param indicePieceJointe
   *          le nouveau indice piece jointe
   */
  void setIndicePieceJointe(String indicePieceJointe);

  /**
   * Getter.
   * 
   * @return fichierPieceJointe
   */
  @FieldXPath("fichierPieceJointe")
  String getFichierPieceJointe();

  /**
   * Setter.
   *
   * @param fichierPieceJointe
   *          le nouveau fichier piece jointe
   */
  void setFichierPieceJointe(String fichierPieceJointe);

  /**
   * Getter.
   * 
   * @return typePieceJointe
   */
  @FieldXPath("typePieceJointe")
  String getTypePieceJointe();

  /**
   * Setter.
   *
   * @param typePieceJointe
   *          le nouveau type piece jointe
   */
  void setTypePieceJointe(String typePieceJointe);

  /**
   * Getter.
   * 
   * @return fomatPieceJointe
   */
  @FieldXPath("formatPieceJointe")
  String getFormatPieceJointe();

  /**
   * Setter.
   *
   * @param formatPieceJointe
   *          le nouveau format piece jointe
   */
  void setFormatPieceJointe(String formatPieceJointe);

}
