/**
 *
 */
package fr.ge.common.regent.ws.v1.service.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Optional;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.liasse.service.ILiasseGeneration;
import fr.ge.common.regent.ws.v1.service.IRegentRestPrivateService;
import io.swagger.annotations.Api;

/**
 * Rest Regent service implementation.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Api("Regent REST Private Services")
@Path("/v1")
public class RegentServicePrivateImpl extends RegentServiceImpl implements IRegentRestPrivateService {

    /** The logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(RegentServicePrivateImpl.class);

    @Autowired
    private ILiasseGeneration liasseGenerationService;

    /**
     * {@inheritDoc}
     */
    @Override
    public Response generateXmlRegentFromJs(String regentVersion, List<String> listTypeEvenement, String authorityRole, String authorityCode, LinkedHashMap<String, Object> mappedFileToRegent,
            String liasseNumber) {

        String returnedRegent = null;
        try {
            liasseNumber = Optional.ofNullable(liasseNumber) //
                    .map(l -> l) //
                    .orElse(this.liasseGenerationService.getNumeroLiasse());
            LOGGER.info("Numéro de liasse {}.", liasseNumber);

            LinkedHashMap<String, Object> listeBalisesObligatoires = this.serviceGenerationRegentFromDB.fillMandatoryTags(authorityCode, authorityRole, listTypeEvenement, regentVersion, liasseNumber,
                    mappedFileToRegent);
            LOGGER.info("génération de la liste des balises obligatoires pour les évènement {} et le destinataire {}.", listTypeEvenement.toString(), authorityCode);
            returnedRegent = xmlService.buildXml(listeBalisesObligatoires, regentVersion);
            LOGGER.info("construction du XML-REGENT pour les évènement {} et le destinataire {} à partir du fichier de mapping.", listTypeEvenement.toString(), authorityCode);
            this.serviceGenerationRegentFromDB.validateXmlRegent(regentVersion, returnedRegent);
            LOGGER.info("validation du XML-REGENT généré.");
            if (returnedRegent != null && !returnedRegent.isEmpty()) {
                return Response.ok(returnedRegent).build();
            }
        } catch (Exception e) {
            LOGGER.error("une erreur technique est survenue lors de la construction du XML-REGENT pour les évènement {} et le destinataire {} à partir du fichier de mapping.",
                    listTypeEvenement.toString(), authorityCode);
            HashMap<String, String> errorResponse = new HashMap<String, String>();
            errorResponse.put("regent", returnedRegent);
            errorResponse.put("errors", e.getCause().getMessage());
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity(errorResponse).build();
        }
        LOGGER.error("le XML-REGENT généré pour les évènement {} et le destinataire {} à partir du fichier de mapping est vide.", listTypeEvenement.toString(), authorityCode);
        return Response.status(Status.INTERNAL_SERVER_ERROR).build();
    }

}
