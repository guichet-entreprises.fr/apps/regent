package fr.ge.common.tc.ws.v1.model;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IErreurDossier.
 */
@ResourceXPath("/erreurDossier")
public interface IErreurDossier {

  /**
   * Getter de l'attribut codeErreur.
   * 
   * @return String
   */
  @FieldXPath("codeErreur")
  String getCodeErreur();

  /**
   * Setter de l'attribut codeErreur.
   * 
   * @param codeErreur
   *          String
   */
  void setCodeErreur(String codeErreur);

  /**
   * Getter de l'attribut libelleErreur.
   * 
   * @return String
   */
  @FieldXPath("libelleErreur")
  String getLibelleErreur();

  /**
   * Setter de l'attribut libelleErreur.
   * 
   * @param libelleErreur
   *          String
   */
  void setLibelleErreur(String libelleErreur);

  /**
   * Getter de l'attribut fichierEnErreur.
   * 
   * @return String
   */
  @FieldXPath("fichierEnErreur")
  String getFichierEnErreur();

  /**
   * Setter de l'attribut fichierEnErreur.
   * 
   * @param fichierEnErreur
   *          String
   */
  void setFichierEnErreur(String fichierEnErreur);

  /**
   * Getter de l'attribut rubriqueEnErreur.
   * 
   * @return String
   */
  @FieldXPath("rubriqueEnErreur")
  String getRubriqueEnErreur();

  /**
   * Setter de l'attribut rubriqueEnErreur.
   * 
   * @param rubriqueEnErreur
   *          String
   */
  void setRubriqueEnErreur(String rubriqueEnErreur);

  /**
   * Getter de l'attribut valeurEnErreur.
   * 
   * @return String
   */
  @FieldXPath("valeurEnErreur")
  String getValeurEnErreur();

  /**
   * Setter de l'attribut valeurEnErreur.
   * 
   * @param valeurEnErreur
   *          String
   */
  void setValeurEnErreur(String valeurEnErreur);

}
