package fr.ge.common.tc.ws.v1.model;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;
import org.xmlfield.core.types.XmlString;

/**
 * Le Interface IEtatDossier.
 */
@ResourceXPath("/etatDossier")
public interface IEtatDossier {

  /**
   * Getter de l'attribut indiceEtat.
   * 
   * @return String
   */
  @FieldXPath("indiceEtat")
  String getIndiceEtat();

  /**
   * Setter de l'attribut indiceEtat.
   * 
   * @param indiceEtat
   *          String
   */
  void setIndiceEtat(String indiceEtat);

  /**
   * Getter de l'attribut dateHeureEtat.
   * 
   * @return la valeur de dateHeureEtat
   */
  @FieldXPath("dateHeureEtat")
  String getDateEtat();

  /**
   * Setter de l'attribut dateHeureEtat.
   * 
   * @param dateHeureEtat
   *          la nouvelle valeur de dateHeureEtat
   */
  void setDateEtat(String dateHeureEtat);

  /**
   * Getter de l'attribut typeEtat.
   * 
   * @return String
   */
  @FieldXPath("typeEtat")
  String getTypeEtat();

  /**
   * Setter de l'attribut typeEtat.
   * 
   * @param typeEtat
   *          String
   */
  void setTypeEtat(String typeEtat);

  /**
   * Getter de l'attribut codeEtat.
   * 
   * @return la valeur de codeEtat
   */
  @FieldXPath("codeEtat")
  String getCodeEtat();

  /**
   * Setter de l'attribut codeEtat.
   * 
   * @param codeEtat
   *          la nouvelle valeur de codeEtat
   */
  void setCodeEtat(String codeEtat);

  /**
   * Getter de l'attribut commentaireEtat.
   * 
   * @return la valeur de commentaireEtat
   */
  @FieldXPath("commentaireEtat")
  XmlString[] getCommentairesEtat();

  /**
   * Setter de l'attribut commentaireEtat.
   * 
   * @param commentaireEtat
   *          la nouvelle valeur de commentaireEtat
   */
  void setCommentairesEtat(XmlString[] commentaireEtat);

  /**
   * Getter de l'attribut erreurDossier.
   * 
   * @return IErreurDossier[]
   */
  @FieldXPath("erreurDossier")
  IErreurDossier[] getErreursDossier();

  /**
   * Setter de l'attribut erreurDossier.
   * 
   * @param erreurDossier
   *          IErreurDossier[]
   */
  void setErreursDossier(IErreurDossier[] erreurDossier);

}
