/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 */

package fr.ge.common.liasse.ws.v1.service.impl;

import javax.ws.rs.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.liasse.service.ILiasseGeneration;
import fr.ge.common.liasse.ws.v1.service.ILiasseRestService;
import io.swagger.annotations.Api;

/**
 * Service of liasse generating
 * 
 * @author mtakerra
 *
 */
@Api("Liasse REST private Services")
@Path("/v1")
public class LiasseServiceImpl implements ILiasseRestService {

    /** Technical logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(LiasseServiceImpl.class);

    @Autowired
    private ILiasseGeneration liasseGenerationService;

    /**
     * {@inheritDoc}
     */
    @Override
    public String generateLiasseNumber() {
        final String liasse = this.liasseGenerationService.getNumeroLiasse();
        LOGGER.info("Liasse number generated : {}", liasse);
        return liasse;
    }

}
