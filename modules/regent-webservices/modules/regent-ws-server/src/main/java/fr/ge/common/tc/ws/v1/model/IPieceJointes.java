package fr.ge.common.tc.ws.v1.model;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;
import org.xmlfield.core.types.XmlString;
import org.xmlfield.validation.annotations.NotEmpty;

/**
 * Le Interface IPieceJointes.
 */
@ResourceXPath("/pieceJointes")
public interface IPieceJointes {

    /**
     * Get le nom piece jointes.
     *
     * @return le nom piece jointes
     */
    @FieldXPath("nomPieceJointe")
    @NotEmpty(groups = { Init.class, Save.class })
    XmlString[] getNomPieceJointes();

    /**
     * Ajoute le to nom piece jointes.
     *
     * @return le xml string
     */
    XmlString addToNomPieceJointes();

    /**
     * Set le nom piece jointes.
     *
     * @param nomPieceJointes
     *            le nouveau nom piece jointes
     */
    void setNomPieceJointes(XmlString[] nomPieceJointes);

}
