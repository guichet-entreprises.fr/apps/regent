package fr.ge.common.tc.ws.v1.model;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.validation.annotations.NotEmpty;

/**
 * Le Interface IDossierV2.
 */
public interface IDossierV2 extends IDossier {

    /** La constante MODEL_VERSION. */
    static final int MODEL_VERSION = 2;

    /**
     * Get le type.
     *
     * @return le type
     */
    @FieldXPath("type")
    @NotEmpty(groups = { Save.class })
    String getType();

    /**
     * Set le type.
     *
     * @param etat
     *            le nouveau type
     */
    void setType(String etat);

}
