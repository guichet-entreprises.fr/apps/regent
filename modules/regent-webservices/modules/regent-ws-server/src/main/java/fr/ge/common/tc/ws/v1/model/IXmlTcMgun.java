/**
 * 
 */
package fr.ge.common.tc.ws.v1.model;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * @author $Author: traimbau $
 * @version $Revision: 0 $
 */

@ResourceXPath("/MGUN_DEMAT")
public interface IXmlTcMgun {

	/**
	 * Getter de la version.
	 * 
	 * @return version
	 */
	@FieldXPath("version")
	String getVersion();

	/**
	 * Setter de la version.
	 *
	 * @param version
	 */
	void setVersion(String version);

	/**
	 * Getter de l'emetteur
	 * 
	 * @return emetteur
	 */
	@FieldXPath("emetteur")
	String getEmetteur();

	/**
	 * Setter de l'emetteur
	 * 
	 * @param emetteur
	 */
	void setEmetteur(String emetteur);

	/**
	 * Getter du destinataire
	 * 
	 * @return destinataire
	 */
	@FieldXPath("destinataire")
	String getDestinataire();

	/**
	 * Setter du destinataire
	 * 
	 * @param destinataire
	 */
	void setDestinataire(String destinataire);

	/**
	 * Getter pour l'heure de generation du xml
	 * 
	 * @return dateHeureGenerationXml
	 */
	@FieldXPath("dateHeureGenerationXml")
	String getDateHeureGenerationXml();

	/**
	 * Setter pour dateHeureGenerationXml
	 * 
	 * @param dateHeureGenerationXml
	 */
	void setDateHeureGenerationXml(String dateHeureGenerationXml);

	/**
	 * Getter pour commentaire
	 * 
	 * @return commentaire
	 */
	@FieldXPath("commentaire")
	String getCommentaire();

	/**
	 * Setter pour commentaire
	 * 
	 * @param commentaire
	 */
	void setCommentaire(String commentaire);

	/**
	 * Getter pour le numero de liasse
	 * 
	 * @return numero de Liasse
	 */
	@FieldXPath("formalite/numeroDeLiasse")
	String getNumeroDeLiasse();

	/**
	 * Setter pour le numéro de liasse
	 * 
	 * @param numeroDeLiasse
	 */
	void setNumeroDeLiasse(String numeroDeLiasse);

	/**
	 * Getter pour la date du depot
	 * 
	 * @return dateDepot
	 */
	@FieldXPath("formalite/dateDepot")
	String getDateDepot();

	/**
	 * Setter pour la date du depot
	 * 
	 * @param dateDepot
	 */
	void setDateDepot(String dateDepot);

	/**
	 * Getter pour destinataires
	 * 
	 * @return destinataires
	 */
	@FieldXPath("formalite/destinataires")
	IDestinataires[] getDestinataires();

	/**
	 * Setter pour destinataires
	 * 
	 * @param destinataires
	 */
	void setDestinataires(IDestinataires[] destinataires);

	/**
	 * Ajout d'un destinataire à destinataires
	 * 
	 * @return
	 */
	IDestinataires addToDestinataires();

	/**
	 * Supression d'un destinataire à destinataires
	 */
	void removeFromDestinataire(IDestinataires destinataires);

	/**
	 * Getter pour le nom du correspondant
	 * 
	 * @return nomCorrespondant
	 */
	@FieldXPath("formalite/correspondant/identiteCorrespondant/nomCorrespondant")
	String getNomCorrespondant();

	/**
	 * Setter pour nom correspondant
	 * 
	 * @param nomCorrespondant
	 */
	void setNomCorrespondant(String nomCorrespondant);

	/**
	 * Getter pour le libelleDeVoie
	 * 
	 * @return libelleDeVoie
	 */
	@FieldXPath("formalite/correspondant/adresseCorrespondant/libelleDeVoie")
	String getLibelleDeVoie();

	/**
	 * Setter pour libelleDeVoie
	 * 
	 * @param numeroDeVoie
	 */
	void setLibelleDeVoie(String libelleDeVoie);

	/**
	 * Getter pour localite
	 * 
	 * @return localite
	 */
	@FieldXPath("formalite/correspondant/adresseCorrespondant/localite")
	String getLocalite();

	/**
	 * Setter pour localite
	 * 
	 * @param localite
	 */
	void setLocalite(String localite);

	/**
	 * Getter pour le code postal
	 * 
	 * @return codePostal
	 */
	@FieldXPath("formalite/correspondant/adresseCorrespondant/codePostal")
	String getCodePostal();

	/**
	 * Setter pour le code postal
	 * 
	 * @param codePostal
	 */
	void setCodePostal(String codePostal);

	/**
	 * Getter pour bureauDistributeur
	 * 
	 * @return bureauDistributeur
	 */
	@FieldXPath("formalite/correspondant/adresseCorrespondant/bureauDistributeur")
	String getBureauDistributeur();

	/**
	 * Setter pour bureauDistributeur
	 * 
	 * @param bureauDistributeur
	 */
	void setBureauDistributeur(String bureauDistributeur);

	/**
	 * Getter pour codePays
	 * 
	 * @return codePays
	 */
	@FieldXPath("formalite/correspondant/adresseCorrespondant/codePays")
	String getCodePays();

	/**
	 * Setter pour codePays
	 * 
	 * @param codePays
	 */
	void setCodePays(String codePays);

	/**
	 * Getter pour adresseEmail
	 * 
	 * @return adresseEmail
	 */
	@FieldXPath("formalite/correspondant/adresseCorrespondant/adresseEmail")
	String getAdresseEmail();

	/**
	 * Setter pour adresseEmail
	 * 
	 * @param adresseEmail
	 */
	void setAdresseEmail(String adresseEmail);

	/**
	 * Getter pour fichierLiasseXml
	 * 
	 * @return fichierLiasseXml
	 */
	@FieldXPath("formalite/fichierLiasseXml")
	String getFichierLiasseXml();

	/**
	 * Setter pour fichierLiasseXml
	 * 
	 * @param fichierLiasseXml
	 */
	void setFichierLiasseXml(String fichierLiasseXml);

	/**
	 * Getter pour documentJustificatif
	 * 
	 * @return documentJustificatif
	 */
	@FieldXPath("formalite/documentJustificatif")
	IDocumentJustificatif[] getDocumentJustificatif();

	/**
	 * Setter pour documentJustificatif
	 * 
	 * @param documentJustificatif
	 */
	void setDocumentJustificatif(IDocumentJustificatif[] documentJustificatif);

	/**
	 * Ajout d'un document à documentJustificatif
	 */
	IDocumentJustificatif addToDocumentJustificatif();

	/**
	 * Suppression d'un document à documentJustificatif
	 */
	void removeFromDocumentJustificatif(IDocumentJustificatif documentJustificatif);
}
