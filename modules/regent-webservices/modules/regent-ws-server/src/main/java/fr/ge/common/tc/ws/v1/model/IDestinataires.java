/**
 * 
 */
package fr.ge.common.tc.ws.v1.model;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Interface de modélisation XML d'un destinataire du XMLTC MGUN
 * 
 * @author $Author: traimbau $
 * @version $Revision: 0 $
 */

@ResourceXPath("/destinataires")
public interface IDestinataires {
	/**
	 * Getter pour le code EDI du destinataire
	 * 
	 * @return codeEdiDestinataire
	 */
	@FieldXPath("codeEdiDestinataire")
	String getCodeEdiDestinataire();

	/**
	 * Setter pour le code EDI du destinataire
	 * 
	 * @param codeEdiDestinataire
	 */
	void setCodeEdiDestinataire(String codeEdiDestinataire);

	/**
	 * Getter pour le type du destinataire
	 * 
	 * @return typeDestinataire
	 */
	@FieldXPath("typeDestinataire")
	String getTypeDestinataire();

	/**
	 * Setter pour le type du destinataire
	 * 
	 * @param typeDestinataire
	 */
	void setTypeDestinataire(String typeDestinataire);

	/***
	 * Getter et setters pour les informations de paiement
	 * 
	 */

	@FieldXPath("paiement/paiementCarteBleue/identitePayeur/nomPayeur")
	String getNomPayeur();

	void setNomPayeur(String nomPayeur);

	@FieldXPath("paiement/paiementCarteBleue/adressePayeur/numeroDeVoie")
	String getNumeroDeVoie();

	void setNumeroDeVoie(String numeroDeVoiePaiement);

	@FieldXPath("paiement/paiementCarteBleue/adressePayeur/typeDeVoie")
	String getTypeDeVoie();

	void setTypeDeVoie(String typeDeVoiePaiement);

	@FieldXPath("paiement/paiementCarteBleue/adressePayeur/libelleDeVoie")
	String getLibelleDeVoie();

	void setLibelleDeVoie(String libelleDeVoiePaiement);

	@FieldXPath("paiement/paiementCarteBleue/adressePayeur/localite")
	String getLocalite();

	void setLocalite(String localitePaiement);

	@FieldXPath("paiement/paiementCarteBleue/adressePayeur/codePays")
	String getCodePays();

	void setCodePays(String codePaysPaiement);

	@FieldXPath("paiement/paiementCarteBleue/adressePayeur/bureauDistributeur")
	String getBureauDistributeur();

	void setBureauDistributeur(String bureauDistributeur);

	@FieldXPath("paiement/paiementCarteBleue/adressePayeur/adresseEmail")
	String getAdresseMail();

	void setAdresseMail(String adresseMailPaiement);

	@FieldXPath("paiement/paiementCarteBleue/montantFormalite")
	String getMontantFormalite();

	void setMontantFormalite(String montantFormalite);

	@FieldXPath("paiement/paiementCarteBleue/referencePaiement")
	String getReferencePaiement();

	void setReferencePaiement(String referencePaiement);

	@FieldXPath("paiement/paiementCarteBleue/dateHeurePaiement")
	String getDateHeurePaiement();

	void setDateHeurePaiement(String dateHeurePaiement);
}
