package fr.ge.common.tc.ws.v1.model;

import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IDossierAutorisation.
 */
@ResourceXPath("/dossierAutorisation")
public interface IDossierAutorisation {
  // utile au filtre XMLTC pour supprimer la balise
}
