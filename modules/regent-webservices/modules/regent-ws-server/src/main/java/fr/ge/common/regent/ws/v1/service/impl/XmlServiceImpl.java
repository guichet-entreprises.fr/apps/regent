/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.regent.ws.v1.service.impl;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import fr.ge.common.regent.ws.v1.service.IXmlService;
import fr.ge.common.regent.ws.v1.utils.XPathUtils;

/**
 * @author mtakerra
 *
 */
@Service
public class XmlServiceImpl implements IXmlService {
    private static final Logger LOGGER = LoggerFactory.getLogger(XmlServiceImpl.class);

    private static final String ROOT = "REGENT-XML";
    private static final String XMLNS_XSI = "xmlns:xsi";
    private static final String XSI_VERSION = "xsi:noNamespaceSchemaLocation";
    private static final String VERSION_2008 = "V2008.11";
    private static final String VERSION_2016 = "V2016.02";

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public String buildXml(LinkedHashMap<String, Object> receivedDateMappedToRegent, String version) {

        Element rootElement = DocumentHelper.createElement(ROOT);

        rootElement.add(DocumentHelper.createAttribute(rootElement, XMLNS_XSI, "http://www.w3.org/2001/XMLSchema-instance"));
        if (version.equals(VERSION_2008)) {
            rootElement.add(DocumentHelper.createAttribute(rootElement, XSI_VERSION, "Message_Regent_V2008-11.xsd"));

        } else if (version.equals(VERSION_2016)) {
            rootElement.add(DocumentHelper.createAttribute(rootElement, XSI_VERSION, "Message_Regent_V2016-02.xsd"));

        }
        Document document = DocumentHelper.createDocument(rootElement);
        for (String key : receivedDateMappedToRegent.keySet()) {
            addElementToParent(document, key, receivedDateMappedToRegent.get(key));
        }

        return printDoc(document);

    }

    /**
     * Converts the xml from Document to String
     * 
     * @param document
     *            an XML of type Document
     * @return the XML converted to String
     */
    private String printDoc(Document document) {
        OutputFormat format = OutputFormat.createPrettyPrint();
        format.setEncoding("UTF-8");
        StringWriter writer = new StringWriter();
        XMLWriter xmlwriter = new XMLWriter(writer, format);
        try {
            xmlwriter.write(document);
            LOGGER.debug("XML-REGENT généré \n", writer.getBuffer().toString());
            return writer.getBuffer().toString();
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

    /**
     * Recursive method to create an element and, if necessary, its parents and
     * siblings
     * 
     * @param document
     * @param xpath
     *            to single element
     * @param value
     *            if null an empty element will be created
     * @return the created Node
     */
    private Node addElementToParent(Document document, String xpath, Object value) {

        String elementName = XPathUtils.getChildElementName(xpath);
        String parentXPath = XPathUtils.getParentXPath(xpath);
        Node parentNode = document.selectSingleNode(parentXPath);
        if (parentNode == null) {
            parentNode = addElementToParent(document, parentXPath, null);
        }

        // create younger siblings if needed
        Integer childIndex = XPathUtils.getChildElementIndex(xpath);
        if (childIndex > 1) {
            List<?> nodelist = document.selectNodes(XPathUtils.createPositionXpath(xpath, childIndex));
            // how many to create = (index wanted - existing - 1 to account for
            // the new element we will create)
            int nodesToCreate = childIndex - nodelist.size() - 1;
            for (int i = 0; i < nodesToCreate; i++) {
                ((Element) parentNode).addElement(elementName);
            }
        }

        // create requested element
        // Element created = ((Element) parentNode).addElement(elementName);
        Element created = ((Element) parentNode).addElement(elementName);
        if (value != null) {
            created.setText(value.toString());
        }
        return created;
    }

    /**
     * Convertit une liste non modifiable présente dans la SFD du xml regent
     * exemple : {'.','.'} de type UnmodifiableRandomAccessList en List<String>.
     *
     * @param value
     *            value
     * @return list
     */
    private List<String> convertToList(final Object value) {

        List<String> result = new ArrayList<String>();
        if (value instanceof Iterable<?>) {
            for (final Object obj : (Iterable<?>) value) {
                result.add(obj.toString());
            }
        }
        return result;

    }

}
