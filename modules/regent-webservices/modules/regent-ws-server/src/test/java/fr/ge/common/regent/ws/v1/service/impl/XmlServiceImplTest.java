package fr.ge.common.regent.ws.v1.service.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.regent.ws.v1.service.IXmlService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/service-test-context.xml", "classpath:spring/test-context.xml" })
public class XmlServiceImplTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(XmlServiceImplTest.class);

    @Autowired
    IXmlService xmlService;

    @Test
    public void testBuildXml() throws Exception {

        LinkedHashMap<String, Object> mappedFileToRegent = new LinkedHashMap<String, Object>();
        mappedFileToRegent.put("REGENT-XML/Emetteur", "Z1611");
        mappedFileToRegent.put("REGENT-XML/Destinataire", "C7801");
        mappedFileToRegent.put("REGENT-XML/DateHeureEmission", "2018-03-15T19:05:00");
        mappedFileToRegent.put("REGENT-XML/VersionMessage", "V2008.11");
        mappedFileToRegent.put("REGENT-XML/VersionNorme", "V2008.11");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Specification/NomService", "ReceptionLiasseDéclarant");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Specification/VersionService", "V2008.11");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/IDF", null);
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C01", "2018-03-15");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02", null);
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C03", "0");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C04", "2018-03-15");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C05", "M");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C06", "O");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.1", "01P");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2", "2015-07-21");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[0]", "M7501");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[1]", "G7501");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C36", "Poutkline Igor");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3", "75104");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.5", "2");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8", "75004");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11", "RUE");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12", "de Bourg Tibourg");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13", "PARIS");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.3", "ip@oste.fr");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.1", "POUTKLINE");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.3", "75104");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.5", "A");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.8", "75004");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.11", "RUE");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.12", "Bourg Tibourg");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.13", "PARIS");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C41", "Paris");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C42", "2016-05-17");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C43", null);
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.1", "1");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.2", "POUTKLINE");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.3", "IGOR");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P02", "");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.1", "1955-05-12");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.2", "99123");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.3", "FEDERATION DE RUSSIE");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P06", "A");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/AIP/P11", null);
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/AIP/P14", null);
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U01", null);
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U01/U01.1", "G7501");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U02", "552100554");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U04", "99");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U06", "O");

        HashMap<String, Boolean> listeBalisesObligatoires = new HashMap<String, Boolean>();
        listeBalisesObligatoires.put("REGENT-XML/VersionNorme", true);
        String regentVersion = "V2008.11";

        String resultXml = this.xmlService.buildXml(mappedFileToRegent, regentVersion);

        Assert.assertTrue(resultXml.contains("<REGENT-XML xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"Message_Regent_V2008-11.xsd\">"));
        Assert.assertTrue(resultXml.contains("<Emetteur>Z1611</Emetteur>"));
        Assert.assertTrue(resultXml.contains("<Destinataire>C7801</Destinataire>"));
        Assert.assertTrue(resultXml.contains("<DateHeureEmission>2018-03-15T19:05:00</DateHeureEmission>"));
        Assert.assertTrue(resultXml.contains("<VersionMessage>V2008.11</VersionMessage>"));
        Assert.assertTrue(resultXml.contains("<C20>M7501</C20>"));
        Assert.assertTrue(resultXml.contains("<C20>G7501</C20>"));

    }

    @Test
    public void testBuildXmlWithMultiOccurrences() throws Exception {

        LinkedHashMap<String, Object> mappedFileToRegent = new LinkedHashMap<String, Object>();
        mappedFileToRegent.put("REGENT-XML/Emetteur", "Z1611");
        mappedFileToRegent.put("REGENT-XML/Destinataire", "C7801");
        mappedFileToRegent.put("REGENT-XML/DateHeureEmission", "2018-03-15T19:05:00");
        mappedFileToRegent.put("REGENT-XML/VersionMessage", "V2008.11");
        mappedFileToRegent.put("REGENT-XML/VersionNorme", "V2008.11");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Specification/NomService", "ReceptionLiasseDéclarant");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Specification/VersionService", "V2008.11");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/IDF", null);
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C01", "2018-03-15");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02", null);
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C03", "0");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C04", "2018-03-15");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C05", "M");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C06", "O");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/EDF[1]/C10/C10.1", "01P");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/EDF[1]/C10/C10.2", "2015-07-21");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/EDF[2]/C10/C10.1", "02P");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/EDF[2]/C10/C10.2", "2015-07-22");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[0]", "M7501");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[1]", "G7501");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C36", "Poutkline Igor");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3", "75104");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.5", "2");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8", "75004");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11", "RUE");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12", "de Bourg Tibourg");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13", "PARIS");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.3", "ip@oste.fr");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.1", "POUTKLINE");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.3", "75104");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.5", "A");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.8", "75004");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.11", "RUE");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.12", "Bourg Tibourg");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.13", "PARIS");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C41", "Paris");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C42", "2016-05-17");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C43", null);
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.1", "1");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.2", "POUTKLINE");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.3", "IGOR");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P02", "");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.1", "1955-05-12");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.2", "99123");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.3", "FEDERATION DE RUSSIE");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P06", "A");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/AIP/P11", null);
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/AIP/P14", null);
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U01", null);
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U01/U01.1", "G7501");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U02", "552100554");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U04", "99");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U06", "O");

        HashMap<String, Boolean> listeBalisesObligatoires = new HashMap<String, Boolean>();
        listeBalisesObligatoires.put("REGENT-XML/VersionNorme", true);
        String regentVersion = "V2008.11";

        String resultXml = this.xmlService.buildXml(mappedFileToRegent, regentVersion);

        Assert.assertTrue(resultXml.contains("<REGENT-XML xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"Message_Regent_V2008-11.xsd\">"));
        Assert.assertTrue(resultXml.contains("<Emetteur>Z1611</Emetteur>"));
        Assert.assertTrue(resultXml.contains("<Destinataire>C7801</Destinataire>"));
        Assert.assertTrue(resultXml.contains("<DateHeureEmission>2018-03-15T19:05:00</DateHeureEmission>"));
        Assert.assertTrue(resultXml.contains("<VersionMessage>V2008.11</VersionMessage>"));
        Assert.assertTrue(resultXml.contains("<C20>M7501</C20>"));
        Assert.assertTrue(resultXml.contains("<C20>G7501</C20>"));
        Assert.assertTrue(resultXml.contains("<C10.1>01P</C10.1>"));
        Assert.assertTrue(resultXml.contains("<C10.2>2015-07-21</C10.2>"));
        Assert.assertTrue(resultXml.contains("<C10.1>02P</C10.1>"));
        Assert.assertTrue(resultXml.contains("<C10.2>2015-07-22</C10.2>"));

    }
}
