package fr.ge.common.regent.ws.support.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.mockito.Mockito;

public class LogContextWsFilterTest {

    @Test
    public void testDoFilter() throws IOException, ServletException {

        HttpServletRequest httpServletRequest = Mockito.mock(HttpServletRequest.class);
        HttpServletResponse httpServletResponse = Mockito.mock(HttpServletResponse.class);
        FilterChain filterChain = Mockito.mock(FilterChain.class);

        Mockito.when(httpServletRequest.getHeader("X-Correlation-ID")).thenReturn("1");
        Mockito.when(httpServletRequest.getHeader("X-User-ID")).thenReturn("2");
        LogContextWsFilter logContextWsFilter = new LogContextWsFilter();
        logContextWsFilter.doFilter(httpServletRequest, httpServletResponse, filterChain);
        Mockito.verify(filterChain).doFilter(httpServletRequest, httpServletResponse);
    }

    @Test
    public void testDoFilterWithNullCorrelationId() throws IOException, ServletException {

        HttpServletRequest httpServletRequest = Mockito.mock(HttpServletRequest.class);
        HttpServletResponse httpServletResponse = Mockito.mock(HttpServletResponse.class);
        FilterChain filterChain = Mockito.mock(FilterChain.class);

        LogContextWsFilter logContextWsFilter = new LogContextWsFilter();

        // -->Generate random Correlation-ID
        Mockito.when(httpServletRequest.getHeader("X-Correlation-ID")).thenReturn(null);
        Mockito.when(httpServletRequest.getHeader("X-User-ID")).thenReturn("2");
        logContextWsFilter.doFilter(httpServletRequest, httpServletResponse, filterChain);
        Mockito.verify(filterChain).doFilter(httpServletRequest, httpServletResponse);
    }
}
