package fr.ge.common.liasse.ws.v1.service.impl;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.junit.Assert.assertEquals;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.liasse.service.ILiasseGeneration;
import fr.ge.common.utils.test.AbstractRestTest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/data-context.xml", "classpath:spring/test-context.xml", "classpath:spring/service-test-context.xml",
        "classpath*:spring/regent-common-ws-context.xml" })
public class LiasseWebServiceImplTest extends AbstractRestTest {

    @Autowired
    private ILiasseGeneration liasseGenerationService;

    @Autowired
    @Qualifier("restServerPrivate")
    private JAXRSServerFactoryBean restServerFactory;

    @Override
    protected JAXRSServerFactoryBean getRestServerFactory() {
        return this.restServerFactory;
    }

    /** Setup. */
    @Before
    public void setup() {
        Mockito.reset(this.liasseGenerationService);
    }

    @Test
    public void testGenerateLiasseNumber() throws Exception {
        when(this.liasseGenerationService.getNumeroLiasse()).thenReturn("H10000000001");

        final Response response = this.client().path("/v1/liasse/generate").get();

        verify(this.liasseGenerationService).getNumeroLiasse();

        assertEquals(response.getStatus(),HttpStatus.OK.value());
        assertEquals(response.readEntity(String.class),"H10000000001");
    }

}
