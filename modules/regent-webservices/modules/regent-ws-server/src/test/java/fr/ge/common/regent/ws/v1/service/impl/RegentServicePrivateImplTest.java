package fr.ge.common.regent.ws.v1.service.impl;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.liasse.service.ILiasseGeneration;
import fr.ge.common.regent.service.ServiceGeneationRegentFromDB;
import fr.ge.common.regent.service.exception.XmlInvalidException;
import fr.ge.common.regent.ws.v1.bean.RegentBean;
import fr.ge.common.utils.exception.TechnicalException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/service-test-context.xml", "classpath:spring/test-context.xml", "classpath*:spring/regent-common-ws-context.xml",
        "classpath*:spring/regent-service-context-config.xml" })
public class RegentServicePrivateImplTest extends AbstractRestTest {

    @Autowired
    private ServiceGeneationRegentFromDB serviceGeneationRegentFromDB;

    @Autowired
    private ILiasseGeneration liasseGenerationService;

    /** empty regent. */
    public static String EMPTY_REGENT = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n<REGENT-XML";

    /** Original server factory. */
    @Autowired
    private JAXRSServerFactoryBean restServerPrivate;

    /** {@inheritDoc} */
    @Override
    protected JAXRSServerFactoryBean getRestServer() {
        return restServerPrivate;
    }

    /** Setup. */
    @Before
    public void setup() {
        Mockito.reset(this.serviceGeneationRegentFromDB, liasseGenerationService);
    }

    /**
     * Formats a String to be at the JSON format.
     *
     * @param str
     *            the input string
     * @return the string with the good format
     */
    private String json(final String str) {
        return str.replaceAll("'", "\"");
    }

    @Test
    // @Ignore("Test lié à l'ancienne méthode, à refaire")
    public void testGenerateXmlRegent() throws TechnicalException, XmlInvalidException {
        final String numeroLiasse = "H100099999999";
        LinkedHashMap<String, Object> mappedFileToRegent = new LinkedHashMap<String, Object>();
        mappedFileToRegent.put("REGENT-XML/Emetteur", "Z1611");
        mappedFileToRegent.put("REGENT-XML/Destinataire", "C7801");
        mappedFileToRegent.put("REGENT-XML/DateHeureEmission", "2018-03-15T19:05:00");
        mappedFileToRegent.put("REGENT-XML/VersionMessage", "V2008.11");
        mappedFileToRegent.put("REGENT-XML/VersionNorme", "V2008.11");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Specification/NomService", "ReceptionLiasseDéclarant");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Specification/VersionService", "V2008.11");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/IDF", null);
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C01", "2018-03-15");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02", null);
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C03", "0");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C04", "2018-03-15");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C05", "M");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C06", "O");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/EDF[1]/C10/C10.1", "01P");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/EDF[1]/C10/C10.2", "2015-07-21");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/EDF[2]/C10/C10.1", "02P");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/EDF[2]/C10/C10.2", "2015-07-22");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[0]", "M7501");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[1]", "G7501");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C36", "Poutkline Igor");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3", "75104");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.5", "2");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8", "75004");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11", "RUE");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12", "de Bourg Tibourg");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13", "PARIS");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.3", "ip@oste.fr");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.1", "POUTKLINE");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.3", "75104");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.5", "A");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.8", "75004");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.11", "RUE");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.12", "Bourg Tibourg");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.13", "PARIS");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C41", "Paris");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C42", "2016-05-17");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C43", null);
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.1", "1");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.2", "POUTKLINE");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.3", "IGOR");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P02", "");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.1", "1955-05-12");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.2", "99123");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.3", "FEDERATION DE RUSSIE");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P06", "A");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/AIP/P11", null);
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/AIP/P14", null);
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U01", null);
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U01/U01.1", "G7501");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U02", "552100554");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U04", "99");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U06", "O");

        when(this.serviceGeneationRegentFromDB.fillMandatoryTags(anyString(), anyString(), anyList(), anyString(), anyString(), Mockito.any(LinkedHashMap.class))).thenReturn(mappedFileToRegent);
        when(this.liasseGenerationService.getNumeroLiasse()).thenReturn(numeroLiasse);

        // call
        final Response response = this.client().type(MediaType.APPLICATION_JSON).path("/v1/xml-regent/generate/V2008.11/CFE/C7501").query("listTypeEvenement", "10P").post(mappedFileToRegent);

        // verify
        assertEquals(response.getStatus(),Status.OK.getStatusCode());
    }

    @Test
    // @Ignore("Test lié à l'ancienne méthode, à refaire")
    public void testGenerateXmlRegentExestingLiasse() throws TechnicalException, XmlInvalidException {
        final String numeroLiasse = "H100099999999";
        LinkedHashMap<String, Object> mappedFileToRegent = new LinkedHashMap<String, Object>();
        mappedFileToRegent.put("REGENT-XML/Emetteur", "Z1611");
        mappedFileToRegent.put("REGENT-XML/Destinataire", "C7801");
        mappedFileToRegent.put("REGENT-XML/DateHeureEmission", "2018-03-15T19:05:00");
        mappedFileToRegent.put("REGENT-XML/VersionMessage", "V2008.11");
        mappedFileToRegent.put("REGENT-XML/VersionNorme", "V2008.11");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Specification/NomService", "ReceptionLiasseDéclarant");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Specification/VersionService", "V2008.11");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/IDF", null);
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C01", "2018-03-15");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02", null);
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C03", "0");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C04", "2018-03-15");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C05", "M");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C06", "O");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/EDF[1]/C10/C10.1", "01P");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/EDF[1]/C10/C10.2", "2015-07-21");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/EDF[2]/C10/C10.1", "02P");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/EDF[2]/C10/C10.2", "2015-07-22");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[0]", "M7501");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[1]", "G7501");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C36", "Poutkline Igor");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3", "75104");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.5", "2");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8", "75004");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11", "RUE");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12", "de Bourg Tibourg");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13", "PARIS");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.3", "ip@oste.fr");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.1", "POUTKLINE");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.3", "75104");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.5", "A");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.8", "75004");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.11", "RUE");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.12", "Bourg Tibourg");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.13", "PARIS");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C41", "Paris");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C42", "2016-05-17");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C43", null);
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.1", "1");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.2", "POUTKLINE");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.3", "IGOR");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P02", "");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.1", "1955-05-12");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.2", "99123");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.3", "FEDERATION DE RUSSIE");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P06", "A");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/AIP/P11", null);
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/AIP/P14", null);
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U01", null);
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U01/U01.1", "G7501");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U02", "552100554");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U04", "99");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U06", "O");

        when(this.serviceGeneationRegentFromDB.fillMandatoryTags(anyString(), anyString(), anyList(), anyString(), anyString(), Mockito.any(LinkedHashMap.class))).thenReturn(mappedFileToRegent);
        when(this.liasseGenerationService.getNumeroLiasse()).thenReturn(numeroLiasse);

        // call
        final Response response = this.client().type(MediaType.APPLICATION_JSON).path("/v1/xml-regent/generate/V2008.11/CFE/C7501").query("listTypeEvenement", "10P")
                .query("liasseNumber", numeroLiasse).post(mappedFileToRegent);

        // verify
        assertEquals(response.getStatus(),Status.OK.getStatusCode());
    }

    @Test
    @Ignore("Test lié à l'ancienne méthode, à refaire")
    public void testFailedGenerateXmlRegent() throws TechnicalException, XmlInvalidException {
        final String numeroLiasse = "H100099999999";
        when(this.serviceGeneationRegentFromDB.getRegentXmlCap(anyList(), anyString(), anyString(), anyString(), anyString(), anyList(), anyString(), anyString(), anyString()))
                .thenThrow(new NullPointerException());

        // call
        final RegentBean regentBean = new RegentBean();
        final String dialogue = this.json("");
        final String profil = this.json("{'formalite': { 'reseauCFE': 'C'}}");
        regentBean.setDialogue(dialogue);
        regentBean.setProfil(profil);
        final Response response = this.client().type(MediaType.APPLICATION_JSON).path("/v1/generate/V2008.11/C/01P/CFE/C7501").post(regentBean);

        // verify
        assertEquals(response.getStatus(),Status.INTERNAL_SERVER_ERROR.getStatusCode());
    }

    @Test
    @Ignore("Test lié à l'ancienne méthode, à refaire")
    public void testFailedTrackerPost() throws TechnicalException, XmlInvalidException {
        final String numeroLiasse = "H100099999999";
        when(this.serviceGeneationRegentFromDB.getRegentXmlCap(anyList(), anyString(), anyString(), anyString(), anyString(), anyList(), anyString(), anyString(), anyString())).thenReturn(null);

        // call
        final RegentBean regentBean = new RegentBean();
        final String dialogue = this.json("{'formalite': {'codeCommuneActivite': '05023'}}");
        final String profil = this.json("{'formalite': { 'reseauCFE': 'C'}}");
        regentBean.setDialogue(dialogue);
        regentBean.setProfil(profil);
        final Response response = this.client().type(MediaType.APPLICATION_JSON).path("/v1/generate/V2008.11/C/01P/CFE/C7501").post(regentBean);

        // verify
        assertEquals(response.getStatus(),Status.OK.getStatusCode());
    }

    @Test
    @Ignore("Test lié à l'ancienne méthode, à refaire")
    public void testFailedGenerateLiasse() throws TechnicalException, XmlInvalidException {
        when(this.serviceGeneationRegentFromDB.getRegentXmlCap(anyList(), anyString(), anyString(), anyString(), anyString(), anyList(), anyString(), anyString(), anyString())).thenReturn(null);

        // call
        final RegentBean regentBean = new RegentBean();
        final String dialogue = this.json("{'formalite': {'codeCommuneActivite': '05023'}}");
        final String profil = this.json("{'formalite': { 'reseauCFE': 'C'}}");
        regentBean.setDialogue(dialogue);
        regentBean.setProfil(profil);
        final Response response = this.client().type(MediaType.APPLICATION_JSON).path("/v1/generate/V2008.11/C/01P/CFE/C7501").post(regentBean);

        // verify
        assertEquals(response.getStatus(),Status.INTERNAL_SERVER_ERROR.getStatusCode());
    }

    private static void writeBytesToFile(byte[] bFile, String fileDest) {

        try (FileOutputStream fileOuputStream = new FileOutputStream(fileDest)) {
            fileOuputStream.write(bFile);
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGenerateXmlRegentFromJs() {
        final String numeroLiasse = "H100099999999";
        // when(this.xmlService.buildXml(Mockito.any(LinkedHashMap.class),
        // anyString())).thenReturn("<REGENT-XML/>");
        String regentVersion = "V2008.11";
        List<String> listTypeEvenement = new ArrayList<String>();
        listTypeEvenement.add("10P");
        String authorityRole = "CFE";
        String authorityCode = "C7501";
        LinkedHashMap<String, Object> mappedFileToRegent = new LinkedHashMap<String, Object>();
        mappedFileToRegent.put("REGENT-XML/Emetteur", "Z1611");
        mappedFileToRegent.put("REGENT-XML/Destinataire", "C7801");
        mappedFileToRegent.put("REGENT-XML/DateHeureEmission", "2018-03-15T19:05:00");
        mappedFileToRegent.put("REGENT-XML/VersionMessage", "V2008.11");
        mappedFileToRegent.put("REGENT-XML/VersionNorme", "V2008.11");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Specification/NomService", "ReceptionLiasseDéclarant");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Specification/VersionService", "V2008.11");

        when(this.liasseGenerationService.getNumeroLiasse()).thenReturn(numeroLiasse);
        when(this.serviceGeneationRegentFromDB.fillMandatoryTags(anyString(), anyString(), anyList(), anyString(), anyString(), Mockito.any(LinkedHashMap.class))).thenReturn(mappedFileToRegent);

        // call
        final Response response = this.client().type(MediaType.APPLICATION_JSON).path("/v1/xml-regent/generate").path(regentVersion).path(authorityRole).path(authorityCode)
                .query("listTypeEvenement", "10P").post(mappedFileToRegent);

        // verify
        assertEquals(response.getStatus(),Status.OK.getStatusCode());
        String generatedRegent = response.readEntity(String.class);
        assertTrue(generatedRegent.contains("<Emetteur>Z1611</Emetteur>"));
        assertTrue(generatedRegent.contains("<Destinataire>C7801</Destinataire>"));
        assertTrue(generatedRegent.contains("<DateHeureEmission>2018-03-15T19:05:00</DateHeureEmission>"));
        assertTrue(generatedRegent.contains("<VersionMessage>V2008.11</VersionMessage>"));
        assertTrue(generatedRegent.contains("<VersionNorme>V2008.11</VersionNorme>"));
        assertTrue(generatedRegent.contains("<NomService>ReceptionLiasseDéclarant</NomService>"));
        assertTrue(generatedRegent.contains("<VersionService>V2008.11</VersionService>"));
    }

    @Test
    public void testGenerateXmlRegentFromJsRegentNull() {
        final String numeroLiasse = "H100099999999";
        when(this.serviceGeneationRegentFromDB.getRegentXmlCap(anyList(), anyString(), anyString(), anyString(), anyString(), anyList(), anyString(), anyString(), anyString())).thenReturn(null);
        // when(this.xmlService.buildXml(Mockito.any(LinkedHashMap.class),
        // anyString())).thenReturn(null);
        when(this.liasseGenerationService.getNumeroLiasse()).thenReturn(numeroLiasse);
        String regentVersion = "V2008.11";
        List<String> listTypeEvenement = new ArrayList<String>();
        listTypeEvenement.add("10P");
        String authorityRole = "CFE";
        String authorityCode = "C7501";
        LinkedHashMap<String, Object> mappedFileToRegent = new LinkedHashMap<String, Object>();
        mappedFileToRegent.put("REGENT-XML/Emetteur", "Z1611");
        mappedFileToRegent.put("REGENT-XML/Destinataire", "C7801");
        mappedFileToRegent.put("REGENT-XML/DateHeureEmission", "2018-03-15T19:05:00");
        mappedFileToRegent.put("REGENT-XML/VersionMessage", "V2008.11");
        mappedFileToRegent.put("REGENT-XML/VersionNorme", "V2008.11");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Specification/NomService", "ReceptionLiasseDéclarant");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Specification/VersionService", "V2008.11");
        // call
        final Response response = this.client().type(MediaType.APPLICATION_JSON).path("/v1/xml-regent/generate").path(regentVersion).path(authorityRole).path(authorityCode)
                .query("listTypeEvenement", "10P").post(null);

        // verify
        assertEquals(response.getStatus(),Status.OK.getStatusCode());
        String generatedRegent = response.readEntity(String.class);
        assertTrue(generatedRegent.contains(EMPTY_REGENT));
    }

    @Test
    public void testGenerateXmlRegentFromJsRegentValidationKo() {
        final String numeroLiasse = "H100099999999";
        when(this.liasseGenerationService.getNumeroLiasse()).thenReturn(numeroLiasse);
        when(this.serviceGeneationRegentFromDB.getRegentXmlCap(anyList(), anyString(), anyString(), anyString(), anyString(), anyList(), anyString(), anyString(), anyString())).thenReturn(null);
        Mockito.doThrow(new TechnicalException("")).when(this.serviceGeneationRegentFromDB).validateXmlRegent(anyString(), anyString());
        // when(this.xmlService.buildXml(Mockito.any(LinkedHashMap.class),
        // anyString())).thenReturn(null);
        String regentVersion = "V2008.11";
        List<String> listTypeEvenement = new ArrayList<String>();
        listTypeEvenement.add("10P");
        String authorityRole = "CFE";
        String authorityCode = "C7501";
        LinkedHashMap<String, Object> mappedFileToRegent = new LinkedHashMap<String, Object>();
        mappedFileToRegent.put("REGENT-XML/Emetteur", "Z1611");
        mappedFileToRegent.put("REGENT-XML/Destinataire", "C7801");
        mappedFileToRegent.put("REGENT-XML/DateHeureEmission", "2018-03-15T19:05:00");
        mappedFileToRegent.put("REGENT-XML/VersionMessage", "V2008.11");
        mappedFileToRegent.put("REGENT-XML/VersionNorme", "V2008.11");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Specification/NomService", "ReceptionLiasseDéclarant");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Specification/VersionService", "V2008.11");
        mappedFileToRegent.put("/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[1]/C10.2", "1P");

        // call
        final Response response = this.client().type(MediaType.APPLICATION_JSON).path("/v1/xml-regent/generate").path(regentVersion).path(authorityRole).path(authorityCode)
                .query("listTypeEvenement", "10P").post(mappedFileToRegent);

        // verify
        assertEquals(response.getStatus(), Status.INTERNAL_SERVER_ERROR.getStatusCode());
    }

    @Test
    public void testGenerateXmlRegentWithList() throws TechnicalException, XmlInvalidException {
        final String numeroLiasse = "H100099999999";
        List<String> listValues = new ArrayList<String>();
        listValues.add("IGOR");
        listValues.add("IGOR1");
        listValues.add("IGOR2");
        listValues.add("IGOR3");
        LinkedHashMap<String, Object> mappedFileToRegent = new LinkedHashMap<String, Object>();
        mappedFileToRegent.put("REGENT-XML/Emetteur", "Z1611");
        mappedFileToRegent.put("REGENT-XML/Destinataire", "C7801");
        mappedFileToRegent.put("REGENT-XML/DateHeureEmission", "2018-03-15T19:05:00");
        mappedFileToRegent.put("REGENT-XML/VersionMessage", "V2008.11");
        mappedFileToRegent.put("REGENT-XML/VersionNorme", "V2008.11");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Specification/NomService", "ReceptionLiasseDéclarant");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Specification/VersionService", "V2008.11");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/IDF", null);
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C01", "2018-03-15");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02", null);
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C03", "0");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C04", "2018-03-15");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C05", "M");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C06", "O");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/EDF[1]/C10/C10.1", "01P");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/EDF[1]/C10/C10.2", "2015-07-21");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/EDF[2]/C10/C10.1", "02P");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/EDF[2]/C10/C10.2", "2015-07-22");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[0]", "M7501");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[1]", "G7501");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C36", "Poutkline Igor");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3", "75104");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.5", "2");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8", "75004");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11", "RUE");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12", "de Bourg Tibourg");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13", "PARIS");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.3", "ip@oste.fr");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.1", "POUTKLINE");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.3", "75104");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.5", "A");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.8", "75004");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.11", "RUE");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.12", "Bourg Tibourg");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.13", "PARIS");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C41", "Paris");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C42", "2016-05-17");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C43", null);
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.1", "1");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.2", "POUTKLINE");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.3", listValues);
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P02", "");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.1", "1955-05-12");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.2", "99123");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.3", "FEDERATION DE RUSSIE");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P06", "A");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/AIP/P11", null);
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/AIP/P14", null);
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U01", null);
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U01/U01.1", "G7501");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U02", "552100554");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U04", "99");
        mappedFileToRegent.put("REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U06", "O");

        when(this.serviceGeneationRegentFromDB.fillMandatoryTags(anyString(), anyString(), anyList(), anyString(), anyString(), Mockito.any(LinkedHashMap.class))).thenReturn(mappedFileToRegent);
        when(this.liasseGenerationService.getNumeroLiasse()).thenReturn(numeroLiasse);

        // call
        final Response response = this.client().type(MediaType.APPLICATION_JSON).path("/v1/xml-regent/generate/V2008.11/CFE/C7501").query("listTypeEvenement", "10P").post(mappedFileToRegent);

        // verify
        assertEquals(response.getStatus(),Status.OK.getStatusCode());
    }

}
