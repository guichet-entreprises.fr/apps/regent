/**
 * 
 */
package fr.ge.common.tc.ws.v1.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.text.ParseException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.StreamUtils;
import org.xmlfield.core.XmlFieldFactory;
import org.xmlfield.core.exception.XmlFieldParsingException;

import fr.ge.common.regent.service.exception.XmlInvalidException;
import fr.ge.common.regent.ws.v1.service.impl.AbstractRestTest;
import fr.ge.common.tc.ws.v1.model.IDocumentJustificatif;
import fr.ge.common.tc.ws.v1.model.IXmlTcMgun;
import fr.ge.common.tc.ws.v1.model.IXmltc;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * @author $Author: lhoffste $
 * @version $Revision: 0 $
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/data-context.xml", "classpath:spring/test-context.xml", "classpath:spring/service-test-context.xml",
        "classpath:spring/service-xmltc-test-context.xml", "classpath*:spring/regent-common-ws-context.xml" })
public class TcServicePrivateImplTest extends AbstractRestTest {

    @Autowired
    private XmlFieldFactory xmlFieldFactory;

    /** Original server factory. */
    @Autowired
    private JAXRSServerFactoryBean restServerPrivate;

    /** {@inheritDoc} */
    @Override
    protected JAXRSServerFactoryBean getRestServer() {
        return restServerPrivate;
    }

    @Test
    public void testGenerateXmlTcWithEmptyJson() throws TechnicalException, XmlInvalidException {

        String jsonTc = "{}";

        // call
        final Response response = this.client().type(MediaType.APPLICATION_JSON).path("/v1/xml-tc/generate").post(jsonTc);

        assertEquals(response.getStatus(),Status.OK.getStatusCode());
    }

    @Test
    public void testGenerateXmlTcwithFullJson() throws IOException, XmlFieldParsingException {

        InputStream resourceAsStream = this.getClass().getResourceAsStream("/xmltc/xmltcTest.json");

        String xmlTcTest = StreamUtils.copyToString(resourceAsStream, Charset.forName("UTF-8"));

        // call
        final Response response = this.client().type(MediaType.APPLICATION_JSON).path("/v1/xml-tc/generate").post(xmlTcTest);
        String xmlTcString = response.readEntity(String.class);

        IXmltc xmlTc = xmlFieldFactory.getXmlField().xmlToObject(xmlTcString, IXmltc.class);

        assertEquals(response.getStatus(), Status.OK.getStatusCode());
        // Version
        assertEquals(xmlTc.getVersion(), "V2012.02");
        // destinataire
        assertEquals(xmlTc.getDestinataire(),"10000001");
        // codePartenaireEmetteur
        assertEquals(xmlTc.getCodePartenaireEmetteur(),"FRONT-OFFICE");
        // commentaire
        assertNull(xmlTc.getCommentaire());
        assertEquals(xmlTc.getNumeroDossierUnique(),"DOSSIER_M60011111111");
        // numeroDossierUnique
        assertEquals(xmlTc.getTypeDossierUnique(),"D1");
        // typeDossierUnique
        assertEquals(xmlTc.getNomDossier(),"Service Service");
        // nomDossier
        assertEquals(xmlTc.getNomCorrespondant(),"MACHIN TRUC");
        // nomCorrespondant
        assertEquals(xmlTc.getNumeroDeVoie(),"5");
        // numeroDeVoie
        assertNull(xmlTc.getTypeDeVoie());
        // typeDeVoie
        //assertThat(xmlTc.getTypeDeVoie()).isEqualTo(null);
        // Libellé de la voie
        assertEquals(xmlTc.getLibelleDeVoie(),"RUE DES AMES PERDUES");
        // codePostal
        assertEquals(xmlTc.getCodePostal(),"60190");
        // bureauDistributeur
        //assertThat(xmlTc.getBureauDistributeur()).isEqualTo("GOURNAY-SUR-ARONDE");
        assertEquals(xmlTc.getBureauDistributeur(),"GOURNAY-SUR-ARONDE");
        // codePays
        assertEquals(xmlTc.getCodePays(),"FR");
        // adresseEmail
        assertEquals(xmlTc.getAdresseEmail(),"service.service@yopmail.com");
        // roleDestinataire
       // assertThat(xmlTc.getDestinatairesDossierUnique()[0].getRoleDestinataire()).isEqualTo("TDR");
        assertEquals(xmlTc.getDestinatairesDossierUnique()[0].getRoleDestinataire(),"TDR");
        // codePartenaire
        assertEquals(xmlTc.getDestinatairesDossierUnique()[0].getCodePartenaire(),"G6002");
        // codeEdi
        assertEquals(xmlTc.getDestinatairesDossierUnique()[0].getCodeEdi(),"G6002");
        // libelleIntervenant
        assertEquals(xmlTc.getDestinatairesDossierUnique()[0].getLibelleIntervenant(),"GREFFE DE COMPIEGNE");
        // formatPieceJointe
        assertEquals(xmlTc.getPiecesJointes()[0].getFormatPieceJointe(),"XML");
    }

    @Test
    public void testGenerateXmlTcwithFullJsonWithoutPayment() throws IOException, XmlFieldParsingException {

        InputStream resourceAsStream = this.getClass().getResourceAsStream("/xmltc/xmltcTestSansPaiement.json");

        String xmlTcTest = StreamUtils.copyToString(resourceAsStream, Charset.forName("UTF-8"));

        // call
        final Response response = this.client().type(MediaType.APPLICATION_JSON).path("/v1/xml-tc/generate").post(xmlTcTest);
        String xmlTcString = response.readEntity(String.class);

        IXmltc xmlTc = xmlFieldFactory.getXmlField().xmlToObject(xmlTcString, IXmltc.class);

        // Version
        assertEquals(xmlTc.getVersion(), "V2012.02");
        // destinataire
        assertEquals(xmlTc.getDestinataire(),"10000001");
        // codePartenaireEmetteur
        assertEquals(xmlTc.getCodePartenaireEmetteur(),"FRONT-OFFICE");
        assertEquals(xmlTc.getNumeroDossierUnique(),"DOSSIER_M60011111111");
        // numeroDossierUnique
        assertEquals(xmlTc.getTypeDossierUnique(),"D1");
        // typeDossierUnique
        assertEquals(xmlTc.getNomDossier(),"Service Service");
        // nomDossier
        assertEquals(xmlTc.getNomCorrespondant(),"MACHIN TRUC");
        // nomCorrespondant
        assertEquals(xmlTc.getNumeroDeVoie(),"5");
        // numeroDeVoie
        assertNull(xmlTc.getTypeDeVoie());
        // typeDeVoie
        // Libellé de la voie
        assertEquals(xmlTc.getLibelleDeVoie(),"RUE DES AMES PERDUES");
        // codePostal
        assertEquals(xmlTc.getCodePostal(),"60190");
        // bureauDistributeur
        assertEquals(xmlTc.getBureauDistributeur(),"GOURNAY-SUR-ARONDE");
        // codePays
        assertEquals(xmlTc.getCodePays(),"FR");
        // adresseEmail
        assertEquals(xmlTc.getAdresseEmail(),"service.service@yopmail.com");
        // roleDestinataire
       // assertThat(xmlTc.getDestinatairesDossierUnique()[0].getRoleDestinataire()).isEqualTo("TDR");
        assertEquals(xmlTc.getDestinatairesDossierUnique()[0].getRoleDestinataire(),"TDR");
        // codePartenaire
        assertEquals(xmlTc.getDestinatairesDossierUnique()[0].getCodePartenaire(),"G6002");
        // codeEdi
        assertEquals(xmlTc.getDestinatairesDossierUnique()[0].getCodeEdi(),"G6002");
        // libelleIntervenant
        assertEquals(xmlTc.getDestinatairesDossierUnique()[0].getLibelleIntervenant(),"GREFFE DE COMPIEGNE");
        // formatPieceJointe
        assertEquals(xmlTc.getPiecesJointes()[0].getFormatPieceJointe(),"XML");
    }

    @Test
    public void testGenerateXmlTcForMgun() throws IOException, XmlFieldParsingException {

        final String CERFA_CODE_TYPE = "LI";
        final String OTHER_CODE_TYPE = "PJ";

        InputStream resourceAsStream = this.getClass().getResourceAsStream("/xmltc/xmltcTestMgun.json");

        String xmlTcTest = StreamUtils.copyToString(resourceAsStream, Charset.forName("UTF-8"));

        // call
        final Response response = this.client().type(MediaType.APPLICATION_JSON).path("/v1/xml-tc/generate").post(xmlTcTest);
        String xmlTcString = response.readEntity(String.class);

        IXmlTcMgun xmlTc = xmlFieldFactory.getXmlField().xmlToObject(xmlTcString, IXmlTcMgun.class);

        // version
        assertEquals(xmlTc.getVersion(), "V2009.01");
        // emetteur
        assertEquals(xmlTc.getEmetteur(), "00161001");
        // destinataire
        assertEquals(xmlTc.getDestinataire(), "00161005");
        // commentaire
        assertNull(xmlTc.getCommentaire());
        // numéro de Liasse
        //assertThat(xmlTc.getNumeroDeLiasse()).isEqualTo("H10000000795");
        assertEquals(xmlTc.getNumeroDeLiasse(), "H10000000795");
        // destinataires/code EDI
       // assertThat().isEqualTo("U9201");
        assertEquals(xmlTc.getDestinataires()[0].getCodeEdiDestinataire(), "U9201");
        // destinataires/type du destinataire
        //assertThat(xmlTc.getDestinataires()[0].getTypeDestinataire()).isEqualTo("CFE+AC");
        assertEquals(xmlTc.getDestinataires()[0].getTypeDestinataire(), "CFE+AC");
        // Xml Regent name
        assertEquals(xmlTc.getFichierLiasseXml(), "C1000A1001L000795D20200221H090203TFORMAS002PMGUN.xml");
        // documents justificatifs
        IDocumentJustificatif cerfa = xmlTc.getDocumentJustificatif()[0];
        IDocumentJustificatif otherPj1 = xmlTc.getDocumentJustificatif()[1];
        IDocumentJustificatif otherPj2 = xmlTc.getDocumentJustificatif()[2];

        // Cerfa
       // assertThat(cerfa.getCodeTypeDocumentJustificatif()).isEqualTo(CERFA_CODE_TYPE);
        assertEquals(cerfa.getCodeTypeDocumentJustificatif(), CERFA_CODE_TYPE);
       // assertThat(cerfa.getFichierPieceJointe()).isEqualTo("C1000A1001L000795D20200221H090203TPIJTES003PMGUN.pdf");
        assertEquals(cerfa.getFichierPieceJointe(), "C1000A1001L000795D20200221H090203TPIJTES003PMGUN.pdf");

        // Autres pièces jointes
       // assertThat(otherPj1.getCodeTypeDocumentJustificatif()).isEqualTo(OTHER_CODE_TYPE);
        assertEquals(otherPj1.getCodeTypeDocumentJustificatif(), OTHER_CODE_TYPE);
        assertEquals(otherPj1.getFichierPieceJointe(), "C1000A1001L000795D20200221H090203TPIJTES004PMGUN.pdf");

        //assertThat(otherPj2.getCodeTypeDocumentJustificatif()).isEqualTo(OTHER_CODE_TYPE);
        assertEquals(otherPj2.getCodeTypeDocumentJustificatif(), OTHER_CODE_TYPE);
        assertEquals(otherPj2.getCodeTypeDocumentJustificatif(),OTHER_CODE_TYPE);
    }

    @Test
    public void testGenerateXmlTcForMgunWithPaiement() throws IOException, XmlFieldParsingException, ParseException {

        final String CERFA_CODE_TYPE = "LI";
        final String OTHER_CODE_TYPE = "PJ";

        InputStream resourceAsStream = this.getClass().getResourceAsStream("/xmltc/xmlTcTestMgunPaiement.json");

        String xmlTcTest = StreamUtils.copyToString(resourceAsStream, Charset.forName("UTF-8"));

        // call
        final Response response = this.client().type(MediaType.APPLICATION_JSON).path("/v1/xml-tc/generate").post(xmlTcTest);
        String xmlTcString = response.readEntity(String.class);

        IXmlTcMgun xmlTc = xmlFieldFactory.getXmlField().xmlToObject(xmlTcString, IXmlTcMgun.class);

        // version
        assertEquals(xmlTc.getVersion(), "V2009.01");
        // emetteur
        assertEquals(xmlTc.getEmetteur(), "00161001");
        // destinataire
        assertEquals(xmlTc.getDestinataire(), "00161005");
        
        // commentaire
        assertEquals(xmlTc.getCommentaire(),"Commentaire");
        // numéro de Liasse
        assertEquals(xmlTc.getNumeroDeLiasse(),"H10000000724");

        // destinataires/code EDI
        assertEquals(xmlTc.getDestinataires()[0].getCodeEdiDestinataire(),"U9201");
        // destinataires/type du destinataire
        assertEquals(xmlTc.getDestinataires()[0].getTypeDestinataire(),"CFE");
        // Xml Regent name
        assertEquals(xmlTc.getFichierLiasseXml(),"C1000A1001L000724D20200211H102302TFORMAS002PMGUN.xml");
        // documents justificatifs
        IDocumentJustificatif cerfa = xmlTc.getDocumentJustificatif()[0];
        IDocumentJustificatif otherPj1 = xmlTc.getDocumentJustificatif()[1];
        IDocumentJustificatif otherPj2 = xmlTc.getDocumentJustificatif()[2];

        // Cerfa
        assertEquals(cerfa.getCodeTypeDocumentJustificatif(),CERFA_CODE_TYPE);
        assertEquals(cerfa.getFichierPieceJointe(),"C1000A1001L000724D20200211H102302TPIJTES003PMGUN.pdf");

        // Autres pièces jointes
        assertEquals(otherPj1.getCodeTypeDocumentJustificatif(),OTHER_CODE_TYPE);
        assertEquals(otherPj1.getFichierPieceJointe(),"C1000A1001L000724D20200211H102305TPIJTES010PMGUN.png");

        assertEquals(otherPj2.getCodeTypeDocumentJustificatif(),OTHER_CODE_TYPE);
        assertEquals(otherPj2.getFichierPieceJointe(),"C1000A1001L000724D20200211H102302TPIJTES004PMGUN.png");
        // Correspondant
        assertEquals(xmlTc.getNomCorrespondant(),"Harry Potter");
        assertEquals(xmlTc.getLibelleDeVoie(),"avenue de test");
        assertEquals(xmlTc.getLocalite(),"NANTERRE");
        assertEquals(xmlTc.getCodePostal(),"92000");
        assertEquals(xmlTc.getBureauDistributeur(),"NANTERRE");
        assertEquals(xmlTc.getCodePays(),"FR");
        assertEquals(xmlTc.getAdresseEmail(),"no.mail@adresse.fr");
        // Paiement
        assertEquals(xmlTc.getDestinataires()[0].getNomPayeur(),"Harry Potter");
        // Default value
        assertEquals(xmlTc.getDestinataires()[0].getCodePays(),"FR");
        assertNull(xmlTc.getDestinataires()[0].getNumeroDeVoie());
        assertNull(xmlTc.getDestinataires()[0].getTypeDeVoie());
        assertEquals(xmlTc.getDestinataires()[0].getLibelleDeVoie(),"Voie du test");
        assertEquals(xmlTc.getDestinataires()[0].getLocalite(),"NANTERRE");
        // Default value
        assertEquals(xmlTc.getDestinataires()[0].getBureauDistributeur(),"BUREAUDISTR");
        assertEquals(xmlTc.getDestinataires()[0].getAdresseMail(),"no.mail@adresse.fr");

        assertEquals(xmlTc.getDestinataires()[0].getReferencePaiement(),"202002JRJFJN28");
        assertEquals(xmlTc.getDestinataires()[0].getMontantFormalite(),"70,39");
        assertEquals(xmlTc.getDestinataires()[0].getDateHeurePaiement(),"2020-02-11T09:23:28");

    }
}
