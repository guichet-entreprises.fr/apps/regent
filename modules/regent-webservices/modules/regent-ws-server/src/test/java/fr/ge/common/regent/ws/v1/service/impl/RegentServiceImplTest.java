package fr.ge.common.regent.ws.v1.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.mockito.Mockito.when;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.regent.service.ServiceGeneationRegentFromDB;
import fr.ge.common.regent.ws.v1.service.IXmlService;
import fr.ge.common.regent.xml.XmlFieldElement;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/data-context.xml", "classpath:spring/test-context.xml", "classpath*:spring/regent-common-ws-context.xml",
        "classpath*:spring/regent-service-context-config.xml" })
public class RegentServiceImplTest extends AbstractRestTest {

    @Autowired
    private ServiceGeneationRegentFromDB serviceGeneationRegentFromDB;

    @Autowired
    IXmlService xmlService;

    /** empty regent. */
    public static String EMPTY_REGENT = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><REGENT-XML/>";

    /** Original server factory. */
    @Autowired
    private JAXRSServerFactoryBean restServerPublic;

    /** {@inheritDoc} */
    @Override
    protected JAXRSServerFactoryBean getRestServer() {
        return restServerPublic;
    }

    /** Setup. */
    @Before
    public void setup() {
        Mockito.reset(this.serviceGeneationRegentFromDB);
    }

    @Test
    public void testGenerateMappingJs() throws Exception {
        // Build the list and the file js that needs to be present in the
        // generated file
        final List<XmlFieldElement> listTagsByEvenement = new ArrayList<>();
        final String versionNorme = "2008";
        final List<String> listeEvenements = new ArrayList<>();

        XmlFieldElement normRubrique = new XmlFieldElement(null, "10000", "G001", null, "/Regent/rubriqueA:Emplacement", 1, "RUBRIQUE A", true, ".", true, versionNorme);

        XmlFieldElement normRubrique2 = new XmlFieldElement(null, "20000", "G002", null, "/Regent/rubriqueB:Emplacement", 1, "RUBRIQUE B", true, ".", true, versionNorme);

        listTagsByEvenement.add(normRubrique);
        listTagsByEvenement.add(normRubrique2);

        listeEvenements.add("10P");
        when(this.serviceGeneationRegentFromDB.getTagsByEventsAndVersion(listeEvenements, versionNorme)).thenReturn(listTagsByEvenement);

        final Response response = this.client().path("/v1/xml-regent/mapping").query("regentVersion", versionNorme).query("listTypeEvenement", listeEvenements).get();
        assertEquals(response.getStatus(),HttpStatus.OK.value());
        final InputStream responseObject = (InputStream) response.getEntity();
        final byte[] generatedFile = IOUtils.toByteArray(responseObject);

        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));
        String generatedFileString = new String(generatedFile, "UTF-8");
        assertTrue(generatedFileString.toString().contains("//Version : 2008"));
        assertTrue(generatedFileString.toString().contains("var regentFields = {};"));
        assertTrue(generatedFileString.toString().contains("regentFields['/REGENT-XML/rubriqueA:Emplacement']= null;"));
        assertTrue(generatedFileString.toString().contains("regentFields['/REGENT-XML/rubriqueB:Emplacement']= null;"));

    }

    @Test
    public void testGenerateMultiMappingJs() throws Exception {
        // Build the list and the file js that needs to be present in the
        // generated file
        final List<XmlFieldElement> listTagsByEvenement = new ArrayList<>();
        final String versionNorme = "V2008.11";
        final List<String> listeEvenements = new ArrayList<>();

        XmlFieldElement normRubrique = new XmlFieldElement(null, "10000", "G001", null, "/Regent/rubriqueA:Emplacement", 1, "RUBRIQUE A", true, ".", true, versionNorme);

        XmlFieldElement normRubrique2 = new XmlFieldElement(null, "20000", "G002", null, "/Regent/rubriqueB:Emplacement", 1, "RUBRIQUE B", true, ".", true, versionNorme);

        listTagsByEvenement.add(normRubrique);
        listTagsByEvenement.add(normRubrique2);

        listeEvenements.add("10P");
        listeEvenements.add("01P");
        when(this.serviceGeneationRegentFromDB.getTagsByEventsAndVersion(listeEvenements, versionNorme)).thenReturn(listTagsByEvenement);
        final Response response = this.client().path("/v1/xml-regent/mapping").query("regentVersion", versionNorme).query("listTypeEvenement", listeEvenements).get();
        final InputStream responseObject = (InputStream) response.getEntity();
        final byte[] generatedFile = IOUtils.toByteArray(responseObject);

        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));
        String generatedFileString = new String(generatedFile, "UTF-8");
        assertTrue(generatedFileString.toString().contains("//Version : V2008.11"));
        assertTrue(generatedFileString.toString().contains("var regentFields = {};"));
        assertTrue(generatedFileString.toString().contains("regentFields['/REGENT-XML/rubriqueA:Emplacement']= null;"));
        assertTrue(generatedFileString.toString().contains("regentFields['/REGENT-XML/rubriqueB:Emplacement']= null;"));

    }

    private static void writeBytesToFile(byte[] bFile, String fileDest) {

        try (FileOutputStream fileOuputStream = new FileOutputStream(fileDest)) {
            fileOuputStream.write(bFile);
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }

}
