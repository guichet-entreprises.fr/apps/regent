// check inout information

var proxyResult = _INPUT_.proxyResult;

return spec.create({
    id : 'proxy',
    label : 'Résultat de l\'appel au proxy',
    groups : [ spec.createGroup({
        id : 'result',
        label : 'Résultat de l\'appel au proxy',
        description : 'Votre dossier va être transmis au service compétent.',
        data : []
    }) ]
});
