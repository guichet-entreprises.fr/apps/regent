var recordUid = nash.record.description().recordUid;
var testAuthList= []; //Liste des autorités qui sera calculé en tant  normal donc reçu déjà remplie
var choice = $choiceAuthNumber.choiceAuth.numberAuth;
var i;
_log.info("choice is  {}", choice);
_log.info("Value('id').of(£choiceAuthNumber.choiceAuth.numberAuth).contains('oneAuth') is  {}", Value('id').of(choice).contains('oneAuth'));
_log.info("Value('id').of($choiceAuthNumber.choiceAuth.numberAuth).contains('multiAc') is  {}", Value('id').of($choiceAuthNumber.choiceAuth.numberAuth).contains('multiAc'));

if(Value('id').of($choiceAuthNumber.choiceAuth.numberAuth).contains('oneAuth')){
	
		//On générera le TC-XML de la greffe de Marseille
		testAuthList.push("GREFFE42218");
}

if(Value('id').of($choiceAuthNumber.choiceAuth.numberAuth).contains('multiAc')){
	
		//On générera le TC-XML de la greffe de Marseille
		testAuthList.push("GREFFE42218");
		//On générera le TC-XML de la greffe de Pontoise
		testAuthList.push("GREFFE05061");
		//On nedevras pas générer le TC-XML de la CMA Haute Garonne
		testAuthList.push("CMA69386");
}
_log.info("testAuthList is {}", testAuthList);

for(i = 0; i < testAuthList.length; i++){
	_log.info("auth is {}", testAuthList[i]);
	
	// call directory with funcId to find all information of Authorithy

	var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', testAuthList[i]) //
				   .connectionTimeout(10000) //
				   .receiveTimeout(10000) //
				   .accept('json') //
				   .get();

	//result			   
				   
	var receiverInfo = response.asObject();

	// prepare all information of receiver 
	var authorityFuncId = !receiverInfo.funcId ? null : receiverInfo.funcId;
	var authorityLabel = !receiverInfo.label ? null :receiverInfo.label;
	var contactInfo = !receiverInfo.contactInfo ? null : JSON.parse(receiverInfo.contactInfo);
	var partnerID = !contactInfo.paymentPartnerId ? null : contactInfo.paymentPartnerId;

	//Make the payout for the current authority 
	
	var responsePayOut= nash.service.request('http://localhost:50080/proxy-payment/private/api/v1/payment/payouts/{partnerId}/{recordId}', partnerID, recordUid) //
						.dataType('application/json') //
						.accept('json') //
						.post(null);	
							
	_log.info("Payout WS called with status  {}", responsePayOut.status);
	//extract the proofs of the payment from the body of the responsePayOut
	var proofs;
	if (responsePayOut != null && responsePayOut.getStatus() == 200) {
		proofs = responsePayOut.asString();
		_log.info("status is "+ responsePayOut.status);
		_log.info("proofs json is "+JSON.stringify(proofs));

		var jsonProofs = JSON.parse(proofs) ;
		 for (key in jsonProofs){
			 _log.info("key is " + key);
			 _log.info("value is "+jsonProofs[key]);
			nash.record.saveFile("/payOutGenerationXml/generated/"+key,nash.util.convertBase64ToByte(jsonProofs[key]));

		 }
	}
	
	//Test if the authority is a "GREFFE" we generate its XML-TC else we don't
 	if(testAuthList[i].indexOf("GREFFE") !== -1){
		//Si l'autoritéest une Greffe
				// Générer le JSON		
	var xmlTcContent = {
						  "version": "V2012.02",
						  "emetteur": "10001001",
						  "destinataire": "10000001",
						  "codePartenaireEmetteur": "FRONT-OFFICE",
						  "typeAutorisation": "POST-IMMAT",
						  "indicePieceJointe": "1",
						  "typePieceJointe": "LIASSE_CFE_EDI",
						  "fichierPieceJointe": "fichierPieceJointe.zip",
						  "numeroDeLiasse": "H100001645304",
						  "indiceLiasseXml": "indiceLiasseXml",
						  "dateHeureGenerationXml": "2018-06-04T18:00:00",
						  "commentaire": "Fichier xml technique de communication avec le SES",
						  "dossierUnique": {
							"identificationDossierUnique": {
							  "numeroDossierUnique": recordUid
							},
							"typeDossierUnique": "D1+D2",
							"nomDossier": "Brocante PM",
							"correspondant": {
							  "identiteCorrespondant": {
								"nomCorrespondant": "moi"
							  },
							  "adresseCorrespondant": {
								"numeroDeVoie": "129",
								"typeDeVoie": "Rue",
								"libelleDeVoie": "Généralde Gaulle",
								"localite": "Libellé commune",
								"complementDeLocalisation": "complement d'Adresse",
								"codePostal": "95120",
								"bureauDistributeur": "distribution Speciale",
								"codePays": "FR",
								"adresseEmail": "kamal@yopmail.com"
							  }
							},
							"destinataireDossierUnique": [
							  {
								"roleDestinataire": "CFE+TDR",
								"codeDestinataire": {
								  "codePartenaire": "M8901",
								  "codeEdi": "M8901",
								  "libelleIntervenant": "CMA DE L'YONNE"
								}
							  }
							],
							"pieceJointe": [
							  {
								"formatPieceJointe": "XML"
							  }
							],
							"dossierCfe": {
							  "dateHeureDepot": "2018-06-04T18:00:00",
							  "referenceLiasseFo": recordUid,
							  "destinataireDossierCfe": [
								{
								  "roleDestinataire": "CFE+TDR",
								  "codeEdiDestinataire": "M8901",
								  "paiementDossier": {
									"identitePayeur": {
									  "nomPayeur": "EL MOURAHIB"
									},
									"adresseEmail": "kamal@yopmail.com",
									"montantFormalite": "83,96",
									"referencePaiement": recordUid+"G1",
									"dateHeurePaiement": "2013-12-18T11:49:43"
								  }
								}
							  ]
							},
							"dossierAutorisation": {
							  "numeroDossierAutorisation": "89010010488",
							  "dateHeureDepot": "2013-12-13T19:19:55",
							  "sousDossierAutorisation": [
								{
								  "informationDestinataireAutorisation": {
									"codePartenaireDestinataireAutorisation": "Z0000",
									"typeActivite": "BRO"
								  }
								}
							  ]
							}
						  }
						};
	
_log.info(" xmlTcContent :  {}", xmlTcContent);
	var response = nash.service.request('http://localhost:20080/regent-ws-server/api/private/v1/xml-tc/generate') //
		.accept('json') //
		.connectionTimeout(10000) //
		.receiveTimeout(10000) 
		.dataType('application/json')//
		.post(JSON.stringify(xmlTcContent));
		_log.info("response generate XML-TC :  {}", response);
		
		if (response != null && response.status == 200) {
			var xmlTcStr = response.asBytes();
			_log.info("status is "+ response.status);
			_log.info("xmlTcStr is "+ xmlTcStr);	
			//Sauvegarde du fichier recu  dans /payOutGenerationXml/generated
			nash.record.saveFile("/payOutGenerationXml/generated/C1000A1001L"+"1645304"+"D"+"20180608"+"H"+"111500"+"TPIJTES"+i+"CPGUEN.xml",xmlTcStr);
	
		}	
	} 
}

_log.info("SPLIT : xmlTcContent :  {}", xmlTcContent); 

// output to confirm
return spec.create({
	id : 'PayOutGenerationXmlConfirmationMessage',
	label : "payout et génération XML-TC confirmation message",
	groups : [ spec.createGroup({
		id : 'confirmationMessage',
		description : "PayOut et génération du XML-TC est finie",
		data : []
	}) ]
});
