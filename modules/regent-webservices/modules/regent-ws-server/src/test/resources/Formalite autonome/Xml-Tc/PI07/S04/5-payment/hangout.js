
var userFirstName = $p2cmbme.cadre1ObjetModificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiquePrenom1;
var userLastName = $p2cmbme.cadre1ObjetModificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiqueNomNaissance;
var userCivility = "Mr" // $p2cmbme.cadre2IdentiteGroup.cadre2Identite.civilite;
var description = nash.record.description().title; 
var recordUid = nash.record.description().recordUid; 

_log.info("userFirstName is {}", userFirstName);
_log.info("userLastName is {}", userLastName);
_log.info("userCivility is {}", userCivility);
_log.info("description is {}", description);
_log.info("recordUid is {}", recordUid);



var payment = {    
		"recordUid" : recordUid,  
		"userCivility": userCivility,
		"userLastName": userLastName,
		"userFirstName": userFirstName,
		"dossierId" : recordUid,
		"description": "Inscription au registre INPI",
		"lines": [
		               {
					   "description": "Frais d'inscription du dossier",
						"amount": "10000","state": "requested"
						}
				],
		                    
		"reference": recordUid
		};
		
var nfo = nash.hangout.pay({'payment' : JSON.stringify(payment)});
return nfo;