//Get the test case we are testing
var testAuthList=[];
var i;
var data = [];
var fakeEmailUser;
var fakeLastNameUser = "fakeLastNameUser";
var fakeFirstNameUser = "fakeFirstNameUser";
var choice = $choiceAuthNumber.choiceAuth.numberAuth;

_log.info("choice is  {}", choice);
_log.info("Value('id').of(£choiceAuthNumber.choiceAuth.numberAuth).contains('oneAuth') is  {}", Value('id').of(choice).contains('oneAuth'));
_log.info("Value('id').of($choiceAuthNumber.choiceAuth.numberAuth).contains('multiAc') is  {}", Value('id').of($choiceAuthNumber.choiceAuth.numberAuth).contains('multiAc'));

if(Value('id').of($choiceAuthNumber.choiceAuth.numberAuth).contains('oneAuth')){
	
		//On générera le TC-XML de la greffe de Marseille
		testAuthList.push("GREFFE42218");
}

if(Value('id').of($choiceAuthNumber.choiceAuth.numberAuth).contains('multiAc')){
	
		//On générera le TC-XML de la greffe de Marseille
		testAuthList.push("GREFFE42218");
		//On générera le TC-XML de la greffe de Pontoise
		testAuthList.push("GREFFE05061");
		//On nedevras pas générer le TC-XML de la CMA Haute Garonne
		testAuthList.push("CMA69386"); 
}
_log.info("testAuthList is {}", testAuthList);

for(i = 0; i < testAuthList.length; i++){
	_log.info("auth is {}", testAuthList[i]);
	
// call directory with funcId to find all information of Authorithy

var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', testAuthList[i]) //
	           .connectionTimeout(10000) //
	           .receiveTimeout(10000) //
	           .accept('json') //
	           .get();

//result			   
			   
var receiverInfo = response.asObject();

// prepare all information of receiver to create data.xml

var authorityFuncId = !receiverInfo.funcId ? null : receiverInfo.funcId;
var authorityLabel = !receiverInfo.label ? null :receiverInfo.label;
var contactInfo = !receiverInfo.contactInfo ? null : JSON.parse(receiverInfo.contactInfo);
var tel = !contactInfo.profile.tel ? null : contactInfo.profile.tel;
var nomAuth = !contactInfo.profile.address.recipientName ? null : contactInfo.profile.address.recipientName;
var authCP = !contactInfo.profile.address.cityNumber ? null : contactInfo.profile.address.cityNumber;
var authAddressName = !contactInfo.profile.address.addressName ? null : contactInfo.profile.address.addressName;
var authAddressNameCompl = !contactInfo.profile.address.addressNameCompl ? null : contactInfo.profile.adress.addressNameCompl;
var authSpecial = !contactInfo.profile.address.special ? null : contactInfo.profile.address.special;
var authCity = !contactInfo.profile.address.cityName ? null : contactInfo.profile.address.cityName;
var authAdress = authAddressName + " " + authAddressNameCompl + " " + authSpecial;
var partnerID = !contactInfo.paymentPartnerId ? null : contactInfo.paymentPartnerId;
var walletLabel =  testAuthList[i] + " wallet";
fakeEmailUser = testAuthList[i] +"@fake.com";


var accountCreation = {
	"reference": testAuthList[i],
	"address": {
		"city": authCity,
		"country": "FR",
		"line1": authAdress,
		"line2": null,
		"postal_code": authCP},
	"users": [{
		"email": fakeEmailUser,
		"first_name": fakeFirstNameUser,
		"last_name": fakeLastNameUser}]
	};
	
_log.info("accountCreation is  {}", accountCreation);

if(partnerID == null)
{
	//call web service to create a account_id in Nomo
	//TODO : remplacer la deuxieme partie de l'url par la propriété ${payment.proxy.api.account.uri} qui contient "/api/v1/payment/partners/accounts"
	//en vérifiant que la deuxième partie est bien remplacé
	var response = nash.service.request('http://localhost:50080/proxy-payment/private/api/v1/payment/partners/accounts') //
	.dataType('application/json') //
    .accept('json') //
    .post(JSON.stringify(accountCreation));
	
	partnerID = response.asString();
	
	var authorityObject = new Object();
	authorityObject.funcId = authorityFuncId;
	authorityObject.label = authorityLabel;
	var contact = {};
	contact["paymentPartnerId"] = partnerID;
	var contactInfo = JSON.stringify(contact);
	authorityObject.contactInfo = contactInfo;

	_log.info("contact is  {}",contact);
	_log.info("authorityObject is  {}",authorityObject);

	var response = nash.service.request('${directory.baseUrl}/v1/authority/merge') //
	           .connectionTimeout(10000) //
	           .receiveTimeout(10000) //
	           .dataType('application/json')//
	           .accept('json') //
	           .put(authorityObject);
	
	
}

_log.info("after if response create partner account id is  {}", partnerID);

	var numberAuth = i+1;
	var authInfo= spec.createGroup({
					'id': "auth"+numberAuth,
					'label': "autorité n° "+numberAuth+" :",
					'data': [ 
								spec.createData({
									'id': 'authorityLabel',
									'label': "Le nom de l'autorité n° "+numberAuth+" :",
									'type': 'StringReadOnly',
									'value':authorityLabel
								}),
								spec.createData({
									'id': 'authorityCode',
									'label': "Le code de l'autorité n° "+numberAuth+" :",
									'type': 'StringReadOnly',
									'value':authorityFuncId
								})
							]
				});
	
	data.push(authInfo)
}


return spec.create({
    id : 'reviewAuthorities',
    label : "Les autorités cible du test",
    groups : [ spec.createGroup({
	    id : 'authoritiesList',
		label: "Détail des autorités qui seront utilisés lors de ce test",
	    data : data
    }) ]
});