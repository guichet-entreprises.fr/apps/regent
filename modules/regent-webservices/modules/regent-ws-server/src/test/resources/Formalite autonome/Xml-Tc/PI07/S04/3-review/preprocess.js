var formFields = {};
var cerfaFields = {};
function pad(s) { return (s < 10) ? '0' + s : s; }

// Cadres 1 - Objet de la formalité

var objet= $p2cmbme.cadre1ObjetModificationGroup.cadre1ObjetModification;

formFields['modifSituationpersonnelle']                                              = Value('id').of(objet.objetModification).contains('10P') ? true : (Value('id').of(objet.objetModification).contains('15P') ? true : (Value('id').of(objet.objetModification).contains('17P') ? true : (Value('id').of(objet.objetModification).contains('16P') ? true : false)));
formFields['modifAutre']                                                             = Value('id').of(objet.objetModification).contains('30P') ? true : (Value('id').of(objet.objetModification).contains('declarationEIRL') ? true : (Value('id').of(objet.objetModification).contains('modificationEIRL') ? true : (Value('id').of(objet.objetModification).contains('finEIRL') ? true : false)));
formFields['modifTransfert']                                                         = Value('id').of(objet.objetModification).contains('11P') ? true : false;
formFields['modifEtablissement']                                                     = Value('id').of(objet.objetModification).contains('11P') ? true : (Value('id').of(objet.objetModification).contains('modifActivite') ? true : false);

// Cadres 2 - Rappel d'identification

var identite= $p2cmbme.cadre1ObjetModificationGroup.cadre1RappelIdentification;

formFields['siren']                                                                  = identite.siren.split(' ').join('');
formFields['immatRCS']                                                               = Value('id').of(identite.immatriculation).contains('immatRCS') ? true : false;
formFields['immatRCSGreffe']                                                         = identite.immatRCSGreffe;
formFields['immatRM']                                                                = Value('id').of(identite.immatriculation).contains('immatRM') ? true : false;
formFields['immatRMDept']                                                            = identite.immatRMDept;
formFields['greffeSecondaire']                                                       = identite.greffeSecondaire;
formFields['lieuDepotImpot']                                                         = identite.lieuDepotImpot;
formFields['microSocialOui']                                                         = true;
formFields['microSocialNon']                                                         = false;

// Cadres 3A

var modifIdentiteDeclarant = $p2cmbme.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle;
    
if ($p2cmbme.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifIdentite.modifDateIdentite !== null) {
    var dateTmp = new Date(parseInt($p2cmbme.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifIdentite.modifDateIdentite.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateIdentite']          = date;
}
formFields['personneLiee_personnePhysique_nomNaissance']                             = Value('id').of(objet.objetModification).contains('10P') ? (Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifNomNaissance') ? modifIdentiteDeclarant.modifIdentite.modifNewNomNaissance : identite.personneLieePersonnePhysiqueNomNaissance) : identite.personneLieePersonnePhysiqueNomNaissance;
formFields['personneLiee_personnePhysique_nomUsage']                                 = Value('id').of(objet.objetModification).contains('15P') ? (Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPseudonyme).contains('modifNomUsage') ? modifIdentiteDeclarant.modifIdentite.modifNewNomUsage : identite.personneLieePersonnePhysiqueNomUsage) : identite.personneLieePersonnePhysiqueNomUsage;
formFields['personneLiee_personnePhysique_prenom1']                                  = Value('id').of(objet.objetModification).contains('10P') ? (Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifPrenom') ? modifIdentiteDeclarant.modifIdentite.modifNewPrenoms : identite.personneLieePersonnePhysiquePrenom1) : identite.personneLieePersonnePhysiquePrenom1;
formFields['personneLiee_personnePhysique_pseudonyme']                               = Value('id').of(objet.objetModification).contains('15P') ? (Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPseudonyme).contains('modifPseudonyme') ? modifIdentiteDeclarant.modifIdentite.modifNewPseudonyme : identite.personneLieePersonnePhysiquePseudonyme) : identite.personneLieePersonnePhysiquePseudonyme;
if ($p2cmbme.cadre1ObjetModificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiqueDateNaissance !== null) {
    var dateTmp = new Date(parseInt($p2cmbme.cadre1ObjetModificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiqueDateNaissance.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['personneLiee_personnePhysique_dateNaissance']          = date;
}
formFields['personneLiee_personnePhysique_lieuNaissanceDepartement']                 = identite.personneLieePersonnePhysiqueLieuNaissanceDepartement;
formFields['personneLiee_personnePhysique_lieuNaissanceCommune']                     = identite.personneLieePersonnePhysiqueLieuNaissanceCommune + ' / ' + identite.personneLieePersonnePhysiqueLieuNaissancePays;

// Cadres 3B
formFields['modifAncienNomNaissance']                                                = (Value('id').of(objet.objetModification).contains('10P') or Value('id').of(objet.objetModification).contains('15P')) ? identite.personneLieePersonnePhysiqueNomNaissance : '';
formFields['modifAncienNomUsage']                                                    = (Value('id').of(objet.objetModification).contains('10P') or Value('id').of(objet.objetModification).contains('15P')) ? identite.personneLieePersonnePhysiqueNomUsage : '';
formFields['modifAncienPrenoms']                                                     = (Value('id').of(objet.objetModification).contains('10P') or Value('id').of(objet.objetModification).contains('15P')) ? identite.personneLieePersonnePhysiquePrenom1 : '';
formFields['modifAncienPseudonyme']                                                  = (Value('id').of(objet.objetModification).contains('10P') or Value('id').of(objet.objetModification).contains('15P')) ? identite.personneLieePersonnePhysiquePseudonyme : '';

// Cadres 4

if ($p2cmbme.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifDomicile.modifDateDomicile !== null) {
    var dateTmp = new Date(parseInt($p2cmbme.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifDomicile.modifDateDomicile.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateDomicile']          = date;
}
formFields['personneLiee_adresse_voie']                                              = modifIdentiteDeclarant.modifDomicile.newAdresseDomicile.personneLieeAdresseVoieNew;
formFields['personneLiee_adresse_complementAdresse']                                 = modifIdentiteDeclarant.modifDomicile.newAdresseDomicile.personneLieeAdresseComplementAdresseNew;
formFields['personneLiee_adresse_codePostal']                                        = modifIdentiteDeclarant.modifDomicile.newAdresseDomicile.personneLieeAdresseCodePostalNew;
formFields['personneLiee_adresse_commune']                                           = modifIdentiteDeclarant.modifDomicile.newAdresseDomicile.personneLieeAdresseCommuneNew;
formFields['deptAncienDomicile']                                                     = modifIdentiteDeclarant.modifDomicile.newAdresseDomicile.deptAncienDomicile;
if ($p2cmbme.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifNationalite.modifDateNationalite !== null) {
    var dateTmp = new Date(parseInt($p2cmbme.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifNationalite.modifDateNationalite.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateNationalite']          = date;
}
formFields['modifNouvelleNationalite']                                               = modifIdentiteDeclarant.modifNationalite.modifNewNationalite;

// Cadres 7 - EIRL

if ($p2cmbme.cadre4DeclarationAffectationPatrimoineGroup.cadreDeclarationAffectationPatrimoine.modifDateDeclarationEIRL !== null) {
    var dateTmp = new Date(parseInt($p2cmbme.cadre4DeclarationAffectationPatrimoineGroup.cadreDeclarationAffectationPatrimoine.modifDateDeclarationEIRL.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEIRL']          = date1;
} else if ($p2cmbme.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.modifDateEIRL !== null) {
    var dateTmp = new Date(parseInt($p2cmbme.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.modifDateEIRL.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEIRL']          = date2;
}  else if ($p2cmbme.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.poursuiteEIRLGroup.modifDatePoursuiteEIRL !== null) {
    var dateTmp = new Date(parseInt($p2cmbme.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.poursuiteEIRLGroup.modifDatePoursuiteEIRL.getTimeInMillis()));
    var date3 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date3 = date3.concat(pad(month.toString()));
	date3 = date3.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEIRL']          = date3;
}  else if ($p2cmbme.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.finEIRLGroup.finEIRLDeclaration.modifDateFinEIRL !== null) {
    var dateTmp = new Date(parseInt($p2cmbme.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.finEIRLGroup.finEIRLDeclaration.modifDateFinEIRL.getTimeInMillis()));
    var date4 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date4 = date4.concat(pad(month.toString()));
	date4 = date4.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEIRL']          = date4;
}
formFields['eirl_immatriculation']                                                   = Value('id').of(objet.objetModification).contains('declarationEIRL') ? true : false;
formFields['eirl_modification']                                                      = Value('id').of(objet.objetModification).contains('modificationEIRL') ? true : (Value('id').of(objet.objetModification).contains('finEIRL') ? true : false);

// Cadre 8 - Conjoint

var conjoint = $p2cmbme.cadre3ModificationConjointGroup.cadre3ModificationConjoint;

if ($p2cmbme.cadre3ModificationConjointGroup.cadre3ModificationConjoint.modifDateConjoint !== null) {
    var dateTmp = new Date(parseInt($p2cmbme.cadre3ModificationConjointGroup.cadre3ModificationConjoint.modifDateConjoint.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateConjoint']          = date;
}
formFields['modifMentionNouveauConjoint']                                            = Value ('id').of(conjoint.objetModifConjoint).eq('modifMentionNouveauConjoint') ? true : false;
formFields['suppressionMentionConjoint']                                             = Value ('id').of(conjoint.objetModifConjoint).eq('suppressionMentionConjoint') ? true : false;
formFields['modifNomNaissanceConjoint']                                              = conjoint.modifNomNaissanceConjoint;
formFields['modifNomUsageConjoint']                                                  = conjoint.modifNomUsageConjoint;
formFields['modifPrenomConjoint']                                                    = conjoint.modifPrenomConjoint;
if ($p2cmbme.cadre3ModificationConjointGroup.cadre3ModificationConjoint.modifDateNaissanceConjoint !== null) {
    var dateTmp = new Date(parseInt($p2cmbme.cadre3ModificationConjointGroup.cadre3ModificationConjoint.modifDateNaissanceConjoint.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateNaissanceConjoint']          = date;
}
formFields['modifLieuNaissanceDepartementConjoint']                                  = conjoint.modifLieuNaissanceDepartementConjoint;
formFields['modifLieuNaissanceCommuneConjoint']                                      = Value('id').of(objet.objetModification).contains('30P') ? (Value('id').of(conjoint.objetModifConjoint).contains('modifMentionNouveauConjoint') ? (conjoint.modifLieuNaissanceCommuneConjoint + ' / ' + conjoint.modifLieuNaissancePaysConjoint) : '') : '';
formFields['modifNationaliteConjoint']                                               = conjoint.modifNationaliteConjoint;

var nirConjoint = conjoint.modifNumeroSSConjoint;
if(nirConjoint != null) {
    nirConjoint = nirConjoint.replace(/ /g, "");
    formFields['modifNumeroSSConjoint']                	= nirConjoint.substring(0, 13);
    formFields['modifNumeroSSConjoint_cle']             = nirConjoint.substring(13, 15);
}
formFields['modifDomicileConjoint_voie']                                             = Value('id').of(objet.objetModification).contains('30P') ? (conjoint.adresseConjointDifferente ? (conjoint.adresseDomicileConjoint.modifDomicileConjointVoie + ' / ' + conjoint.adresseDomicileConjoint.modifDomicileConjointComplementAdresse) : '') : '';
formFields['modifDomicileConjoint_codePostal']                                       = conjoint.adresseDomicileConjoint.modifDomicileConjointCodePostal;
formFields['modifDomicileConjoint_commune']                                          = conjoint.adresseDomicileConjoint.modifDomicileConjointCommune;

// Cadre 10 - Déclaration relative à l'établissement

formFields['modifConcerneActivite']                                                  = Value('id').of(objet.objetModification).contains('modifActivite') ? true : false;
formFields['modifConcerneTransfert']                                                 = Value('id').of(objet.objetModification).contains('11P') ? true : false;
formFields['modifConcerneOuverture']                                                 = Value('id').of($p2cmbme.cadre6EtablissementGroup.modificationEtablissement.origineNouvelEtablissement).eq('etablissementNouveau') ? true : false;
formFields['modifConcerneFermeture']                                                 = $p2cmbme.cadre6EtablissementGroup.modificationEtablissement.destinationEtablissement != null ? (Value('id').of($p2cmbme.cadre6EtablissementGroup.modificationEtablissement.destinationEtablissement).eq('ModifDestinationEtablissementDevientSecondaire') ? false : true) : false;

// Cadre 11 - Etablissement transféré ou fermé

var etablissement = $p2cmbme.cadre6EtablissementGroup;

if (etablissement.modificationEtablissement.dateModificationEtablissement !== null) {
    var dateTmp = new Date(parseInt(etablissement.modificationEtablissement.dateModificationEtablissement.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementTransfererFermer']          = date;
}
formFields['modifCategarieEtablissementTransfererFermer_principal']                  = Value('id').of(objet.objetModification).contains('11P') ? true : false;
formFields['modifCategarieEtablissementTransfererFermer_secondaire']                 = false;
formFields['modifDestinationEtablissement_vendu']                                    = Value('id').of(etablissement.modificationEtablissement.destinationEtablissement).eq('ModifDestinationEtablissementVendu') ? true : false;
formFields['modifDestinationEtablissement_ferme1']                                   = Value('id').of(etablissement.modificationEtablissement.destinationEtablissement).eq('modifDestinationEtablissementFerme1') ? true : false;
formFields['modifDestinationEtablissement_autre1']                                   = Value('id').of(etablissement.modificationEtablissement.destinationEtablissement).eq('ModifDestinationEtablissementAutre1') ? true : false;
formFields['modifDestinationEtablissementAutre1_libelle']                            = etablissement.modificationEtablissement.modifDestinationEtablissementAutre1Libelle;
formFields['modifDestinationEtablissement_devientPrincipal']                         = false;
formFields['modifDestinationEtablissement_devientSecondaire']                        = Value('id').of(etablissement.modificationEtablissement.destinationEtablissement).eq('ModifDestinationEtablissementDevientSecondaire') ? true : false;
formFields['modifDestinationEtablissement_supprime']                                 = false;
formFields['modifDestinationEtablissement_vendu2']                                   = false;
formFields['modifDestinationEtablissement_autre2']                                   = false;
formFields['modifDestinationEtablissementAutre2_libelle']                            = '';
formFields['modifDestinationEtablissementDateFinEmploiSalarie']                      = '';
formFields['modifAncienneAdresseEtablissement_voie']                                 = Value('id').of(objet.objetModification).contains('11P') ? etablissement.cadre6Etablissement.adresseEtablissement.etablissementAdresseVoie : '';
formFields['modifAncienneAdresseEtablissement_complementAdresse']                    = Value('id').of(objet.objetModification).contains('11P') ? etablissement.cadre6Etablissement.adresseEtablissement.etablissementComplementAdresse : '';
formFields['modifAncienneAdresseEtablissement_codePostal']                           = Value('id').of(objet.objetModification).contains('11P') ? etablissement.cadre6Etablissement.adresseEtablissement.etablissementAdresseCodePostal : '';
formFields['modifAncienneAdresseEtablisseement_commune']                             = Value('id').of(objet.objetModification).contains('11P') ? etablissement.cadre6Etablissement.adresseEtablissement.etablissementAdresseCommune : '';

// Cadre 12 - Etablissement ouvert ou modifié

if (etablissement.modificationEtablissement.dateModificationEtablissement !== null) {
    var dateTmp = new Date(parseInt(etablissement.modificationEtablissement.dateModificationEtablissement.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementOuvertModifier']          = date1;
}  else if ($p2cmbme.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.etablissementDateModificationtActivite !== null) {
    var dateTmp = new Date(parseInt($p2cmbme.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.etablissementDateModificationtActivite.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementOuvertModifier']          = date2;
}
formFields['modifNouvelleAdresseEtablissement_voie']                                 = Value('id').of(objet.objetModification).contains('11P') ? etablissement.modificationEtablissement.adresseEtablissementNew.etablissementAdresseVoieNew : (Value('id').of(objet.objetModification).contains('modifActivite') ? etablissement.cadre6Etablissement.adresseEtablissement.etablissementAdresseVoie : '');
formFields['modifNouvelleAdresseEtablissement_complementAdresse']                    = Value('id').of(objet.objetModification).contains('11P') ? etablissement.modificationEtablissement.adresseEtablissementNew.etablissementComplementAdresseNew : (Value('id').of(objet.objetModification).contains('modifActivite') ? etablissement.cadre6Etablissement.adresseEtablissement.etablissementComplementAdresse : '');
formFields['modifNouvelleAdresseEtablissement_codePostal']                           = Value('id').of(objet.objetModification).contains('11P') ? etablissement.modificationEtablissement.adresseEtablissementNew.etablissementAdresseCodePostalNew : (Value('id').of(objet.objetModification).contains('modifActivite') ? etablissement.cadre6Etablissement.adresseEtablissement.etablissementAdresseCodePostal : '');
formFields['modifNouvelleAdresseEtablisseement_commune']                             = Value('id').of(objet.objetModification).contains('11P') ? etablissement.modificationEtablissement.adresseEtablissementNew.etablissementAdresseCommuneNew : (Value('id').of(objet.objetModification).contains('modifActivite') ? etablissement.cadre6Etablissement.adresseEtablissement.etablissementAdresseCommune : '');
formFields['modifCategarieEtablissementOuvert_effectifPresenceOui']                  = Value('id').of(objet.objetModification).contains('11P') ? (Value('id').of(etablissement.modificationEtablissement.origineNouvelEtablissement).eq('etablissementModifie') ? (etablissement.modificationEtablissement.presenceSalarieEtablissementCree ? true : false) : false) : (Value('id').of(objet.objetModification).contains('modifActivite') ? (etablissement.cadre6Etablissement.adresseEtablissement.presenceSalarieEtablissementModifie ? true : false) : false);
formFields['modifCategarieEtablissementOuvert_effectifPresenceNon']                  = Value('id').of(objet.objetModification).contains('11P') ? (Value('id').of(etablissement.modificationEtablissement.origineNouvelEtablissement).eq('etablissementModifie') ? (etablissement.modificationEtablissement.presenceSalarieEtablissementCree ? false : true) : false) : (Value('id').of(objet.objetModification).contains('modifActivite') ? (etablissement.cadre6Etablissement.adresseEtablissement.presenceSalarieEtablissementModifie ? false : true) : false);
formFields['modifCategarieEtablissementModifier_principal']                          = Value('id').of(objet.objetModification).contains('11P') ? (Value('id').of(etablissement.modificationEtablissement.origineNouvelEtablissement).eq('etablissementModifie') ? true : false) : false;
formFields['modifCategarieEtablissementModifier_secondaire']                         = false;
formFields['modifCategarieEtablissementOuvert_principal']                            = Value('id').of(objet.objetModification).contains('11P') ? (Value('id').of(etablissement.modificationEtablissement.origineNouvelEtablissement).contains('etablissementNouveau') ? true : false) : false;
formFields['modifCategarieEtablissementOuvert_secondaire']                           = false;
formFields['modifCategarieEtablissementOuvert_fondePouvoirOui']                      = false;
formFields['modifCategarieEtablissementOuvert_fondePouvoirNon']                      = false;
formFields['modifCategarieEtablissementOuvert_effectifCreer']                        = Value('id').of(etablissement.modificationEtablissement.origineNouvelEtablissement).eq('etablissementNouveau') ? (etablissement.modificationEtablissement.presenceSalarieEtablissementCree ? etablissement.modificationEtablissement.cadreEffectifSalarie.etablissementEffectifSalarieNombre : "0") : '';
formFields['modifCategarieEtablissementOuvert_effectifNombre']                       = Value('id').of(etablissement.modificationEtablissement.origineNouvelEtablissement).eq('etablissementNouveau') ? (etablissement.modificationEtablissement.presenceSalarieEtablissementCree ? etablissement.modificationEtablissement.cadreEffectifSalarie.etablissementEffectifSalarieTotal : "0") : '';
formFields['modifCategarieEtablissementOuvert_effectifApprentis']                    = Value('id').of(etablissement.modificationEtablissement.origineNouvelEtablissement).eq('etablissementNouveau') ? (etablissement.modificationEtablissement.presenceSalarieEtablissementCree ? etablissement.modificationEtablissement.cadreEffectifSalarie.etablissementEffectifSalarieApprentis : "0") : '';
formFields['modifCategarieEtablissementOuvert_effectifVRP']                          = Value('id').of(etablissement.modificationEtablissement.origineNouvelEtablissement).eq('etablissementNouveau') ? (etablissement.modificationEtablissement.presenceSalarieEtablissementCree ? etablissement.modificationEtablissement.cadreEffectifSalarie.etablissementEffectifSalarieVRP : "0") : '';

// Cadre 13 - Activité

var activite = $p2cmbme.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite;

if (activite.etablissementDateModificationtActivite !== null) {
    var dateTmp = new Date(parseInt(activite.etablissementDateModificationtActivite.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFields['modifDateActivite']          = date1;
}  else if ($p2cmbme.cadre6EtablissementGroup.modificationEtablissement.dateModificationEtablissement !== null) {
    var dateTmp = new Date(parseInt($p2cmbme.cadre6EtablissementGroup.modificationEtablissement.dateModificationEtablissement.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFields['modifDateActivite']          = date2;
}
formFields['entreprise_activitePermanenteSaisonniere_permanente']                    = Value('id').of(activite.activiteTemps).eq('EntrepriseActivitePermanenteSaisonnierePermanente') ? true : false;
formFields['entreprise_activitePermanenteSaisonniere_saisonniere']                   = Value('id').of(activite.activiteTemps).eq('EntrepriseActivitePermanenteSaisonniereSaisonniere') ? true : false;
formFields['etablissment_nonSedentariteQualite_nonSedentaire']                       = activite.etablissmentNonSedentariteQualiteNonSedentaire ? true : false;
formFields['etablissement_activites']                                                = Value('id').of(objet.objetModification).contains('modifActivite') ? activite.etablissementActivitesNew : (Value('id').of(objet.objetModification).contains('11P') ? activite.etablissementActivitesAutres : '');
formFields['etablissement_activitePlusImportante']                                   = (Value('id').of(objet.objetModification).contains('modifActivite') or Value('id').of(objet.objetModification).contains('11P')) ? activite.etablissementActivitesPrincipales : '';
formFields['etablissement_activiteLieuExercice_magasin']                             = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMagasin') ? true : false;
formFields['etablissement_activiteLieuExerciceMagasin']                              = activite.etablissementActiviteLieuExerciceMagasinSurface;
formFields['etablissement_activiteLieuExercice_marche']                              = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMarche') ? true : false;
formFields['etablissement_activiteNature_commerceDetail']                            = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCommerceDetail') ? true : false;
formFields['etablissement_activiteNature_commerceGros']                              = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCommerceGros') ? true : false;
formFields['etablissement_activiteNature_fabricationProduction']                     = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureFabricationProduction') ? true : false;
formFields['etablissement_activiteNature_batTravauxPublics']                         = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureBatTravauxPublics') ? true : false;
formFields['etablissement_activiteNature_cocheAutre']                                = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCocheAutre') ? true : false;
formFields['etablissement_activiteNature_autre']                                     = activite.etablissementActiviteNatureAutre;
formFields['activiteDevientPrincipale_oui']                                          = activite.activiteDevientPrincipaleEntreprise ? true : false;
formFields['activiteDevientPrincipale_non']                                          = (Value('id').of(objet.objetModification).contains('modifActivite') or Value('id').of(objet.objetModification).contains('11P')) ? (activite.activiteDevientPrincipaleEntreprise ? false : true) : false;
formFields['modifActiviteSuiteAdjonction']                                           = Value('id').of(activite.motifModificationActivite).contains('adjonctionActivite') ? true : false;
formFields['modifActiviteSuiteSuppression']                                          = Value('id').of(activite.motifModificationActivite).contains('suppressionActivite') ? true : false;
formFields['modifActiviteSuiteSuppression_disparition']                              = Value('id').of(activite.motifSuppressionPar).contains('suppressionParDisparition') ? true : false;
formFields['modifActiviteSuiteSuppression_vente']                                    = Value('id').of(activite.motifSuppressionPar).contains('suppressionParVente') ? true : false;
formFields['modifActiviteSuiteSuppression_reprise']                                  = Value('id').of(activite.motifSuppressionPar).contains('suppressionParReprise') ? true : false;
formFields['modifActiviteSuiteSuppression_autre']                                    = Value('id').of(activite.motifSuppressionPar).contains('suppressionParAutre') ? true : false;
formFields['modifActiviteSuiteSuppression_autreLibelle']                             = activite.suppressionParAutreLibelle;

// Cadre 14 - Nom établissement

if (activite.etablissementNomCommercialProfessionnel != null and activite.etablissementEnseigne != null and etablissement.modificationEtablissement.dateModificationEtablissement !== null) {
    var dateTmp = new Date(parseInt(etablissement.modificationEtablissement.dateModificationEtablissement.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateNomEtablissement']          = date;
}
formFields['etablissement_nomCommercialProfessionnel']                               = activite.etablissementNomCommercialProfessionnel;
formFields['etablissement_enseigne']                                                 = activite.etablissementEnseigne;

// Cadre 15 - Origine du fonds

var origineFonds = $p2cmbme.cadre6EtablissementGroup.modificationEtablissement.cadreOrigineFonds

formFields['etablissement_origineFonds_creation']                                    = (Value('id').of(etablissement.modificationEtablissement.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') or Value('id').of(etablissement.modificationEtablissement.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')) ? true : (Value('id').of(origineFonds.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation') ? true : false);
formFields['etablissement_origineFonds_achat']                                       = Value('id').of(origineFonds.etablisementOrigineFonds).eq('EtablissementOrigineFondsAchat') ? true : false;
formFields['etablissement_origineFonds_locationGerance']                             = Value('id').of(origineFonds.etablisementOrigineFonds).eq('EtablissementOrigineFondsLocationGerance') ? true : false;
formFields['etablissement_origineFonds_geranceMandat']                               = Value('id').of(origineFonds.etablisementOrigineFonds).eq('EtablissementOrigineFondsGeranceMandat') ? true : false;
formFields['etablissement_origineFonds_autre']                                       = Value('id').of(origineFonds.etablisementOrigineFonds).eq('EtablissementOrigineFondsCocheAutre') ? true : false;
formFields['etablissement_origineFonds_autreLibelle']                                = origineFonds.etablissementOrigineFondsAutre;
formFields['entrepriseLiee_siren_precedentExploitant']                               = origineFonds.precedentExploitant.entrepriseLieeSirenPrecedentExploitant != null ? origineFonds.precedentExploitant.entrepriseLieeSirenPrecedentExploitant.split(' ').join('') : '';
formFields['entrepriseLiee_entreprisePP_nom_precedentExploitant']                    = origineFonds.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant != null ? origineFonds.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant : origineFonds.precedentExploitant.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant;
formFields['entrepriseLiee_entreprisePP_nomUsage_precedentExploitant']               = origineFonds.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant;
formFields['entrepriseLiee_entreprisePP_prenom1_precedentExploitant']                = origineFonds.precedentExploitant.entrepriseLieeEntreprisePPPrenom1PrecedentExploitant;
if (origineFonds.geranceMandat.etablissementLoueurMandantDuFondsDebutContrat !== null) {
    var dateTmp = new Date(parseInt(origineFonds.geranceMandat.etablissementLoueurMandantDuFondsDebutContrat.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['etablissement_loueurMandantDuFondsDebutContrat']          = date;
}
if (origineFonds.geranceMandat.etablissementLoueurMandantDuFondsFinContrat !== null) {
    var dateTmp = new Date(parseInt(origineFonds.geranceMandat.etablissementLoueurMandantDuFondsFinContrat.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['etablissement_loueurMandantDuFondsFinContrat']          = date;
}
formFields['etablissement_loueurMandantDuFondsRenouvellementTaciteReconduction_oui'] = (Value('id').of(origineFonds.etablisementOrigineFonds).eq('EtablissementOrigineFondsLocationGerance') or Value('id').of(origineFonds.etablisementOrigineFonds).eq('EtablissementOrigineFondsGeranceMandat')) ? (origineFonds.geranceMandat.etablissementLoueurMandantDuFondsRenouvellementTaciteReconductionOui ? true : false) : false;
formFields['etablissement_loueurMandantDuFondsRenouvellementTaciteReconduction_non'] = (Value('id').of(origineFonds.etablisementOrigineFonds).eq('EtablissementOrigineFondsLocationGerance') or Value('id').of(origineFonds.etablisementOrigineFonds).eq('EtablissementOrigineFondsGeranceMandat')) ? (origineFonds.geranceMandat.etablissementLoueurMandantDuFondsRenouvellementTaciteReconductionOui ? false : true) : false;
formFields['entrepriseLiee_entreprisePM_nom_loueurMandantDuFonds']                   = origineFonds.geranceMandat.entrepriseLieeEntreprisePMDenominationLoueurMandantDuFonds != null ? origineFonds.geranceMandat.entrepriseLieeEntreprisePMDenominationLoueurMandantDuFonds : origineFonds.geranceMandat.entrepriseLieeEntreprisePMNomNaissanceLoueurMandantDuFonds;
formFields['entrepriseLiee_entreprisePP_nomUsage_loueurMandantDuFonds']              = origineFonds.geranceMandat.entrepriseLieeEntreprisePPNomUsageLoueurMandantDuFonds;
formFields['entrepriseLiee_entreprisePP_prenom1_loueurMandantDuFonds']               = origineFonds.geranceMandat.entrepriseLieeEntreprisePPPrenom1LoueurMandantDuFonds;
formFields['entrepriseLiee_adresse_nomVoie_loueurMandantDuFonds']                    = (Value('id').of(origineFonds.etablisementOrigineFonds).eq('EtablissementOrigineFondsLocationGerance') or Value('id').of(origineFonds.etablisementOrigineFonds).eq('EtablissementOrigineFondsGeranceMandat')) ? (origineFonds.geranceMandat.entrepriseLieeAdresseMandantDuFonds.numRueAdresseLoueurMandantDufonds + ' ' + (origineFonds.geranceMandat.entrepriseLieeAdresseMandantDuFonds.rueComplementAdresseLoueurMandantDuFonds != null ? origineFonds.geranceMandat.entrepriseLieeAdresseMandantDuFonds.rueComplementAdresseLoueurMandantDuFonds : "")) : '';
formFields['entrepriseLiee_adresse_codePostal_loueurMandantDuFonds']                 = origineFonds.geranceMandat.entrepriseLieeAdresseMandantDuFonds.entrepriseLieeAdresseCodePostalLoueurMandantDuFonds;
formFields['entrepriseLiee_adresse_commune_loueurMandantDuFonds']                    = origineFonds.geranceMandat.entrepriseLieeAdresseMandantDuFonds.entrepriseLieeAdresseCommuneLoueurMandantDuFonds;
formFields['entrepriseLiee_siren_geranceMandat']                                     = origineFonds.geranceMandat.entrepriseLieeSirenGeranceMandat != null ? origineFonds.geranceMandat.entrepriseLieeSirenGeranceMandat.split(' ').join('') : '';
formFields['entrepriseLiee_greffeImmatriculation']                                   = origineFonds.geranceMandat.entrepriseLieeGreffeImmatriculation;
if(origineFonds.precedentExploitant.etablissementJournalAnnoncesLegalesDateParution != null) {
	var dateTmp = new Date(parseInt(origineFonds.precedentExploitant.etablissementJournalAnnoncesLegalesDateParution.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['etablissement_journalAnnoncesLegalesDateParution'] = date;
}
formFields['etablissement_journalAnnoncesLegalesNom']                                = origineFonds.precedentExploitant.etablissementJournalAnnoncesLegalesNom;

// Cadre 17 - Observation

var correspondance = $p2cmbme.cadre8RensCompGroup.cadre8RensComp;

formFields['modifDateObservations']                                                  = '';
formFields['observations']                                                           = correspondance.formaliteObservations;

// Cadre 18 - Adresse de correspondance

formFields['adresseCorrespondanceCadre_coche']                                       = (Value('id').of(correspondance.adresseCorrespond).eq('domi') or Value('id').of(correspondance.adresseCorrespond).eq('prof')) ? true : false;
formFields['adresseCorrespondanceCadre_numero']                                      = Value('id').of(correspondance.adresseCorrespond).eq('domi') ? "4" : (Value('id').of(correspondance.adresseCorrespond).eq('prof') ? "12": '');
formFields['adressesCorrespondanceAutre']                                            = Value('id').of(correspondance.adresseCorrespond).eq('autre') ? true : false;
formFields['adresseCorrespondance_voie1']                                            = correspondance.adresseCorrespondance.numeroRueAdresseCorrespondance;
formFields['adresseCorrespondance_voie2']                                            = correspondance.adresseCorrespondance.rueComplementAdresseCorrespondance;
formFields['adresseCorrespondance_codePostal']                                       = correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal;
formFields['adresseCorrespondance_commune']                                          = correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune;
formFields['telephone1']                                                             = correspondance.infosSup.formaliteTelephone1;
formFields['telephone2']                                                             = correspondance.infosSup.formaliteTelephone2;
formFields['courriel']                                                               = correspondance.infosSup.formaliteFaxCourriel != null ? correspondance.infosSup.formaliteFaxCourriel : correspondance.infosSup.telecopie;

// Cadre 19 - Diffusion informations

var signataire1 = $p2cmbme.cadre9SignatureGroup.cadre9Signature;

formFields['diffusionInformation']                                                   = Value('id').of(signataire1.signataire).eq('formaliteNonDiffusionInformationOui') ? true : false;
formFields['nonDiffusionInformation']                                                = Value('id').of(signataire1.signataire).eq('formaliteNonDiffusionInformationNon') ? true : false;

// Cadre 20 - Signataire

formFields['signataireDeclarant']                                                    = Value('id').of(signataire1.soussigne).eq('FormaliteSignataireQualiteDeclarant') ? true : false;
formFields['signataireMandataire']                                                   = Value('id').of(signataire1.soussigne).eq('FormaliteSignataireQualiteMandataire') ? true : false;
formFields['signataireAutre']                                                        = Value('id').of(signataire1.soussigne).eq('FormaliteSignataireQualiteAutre') ? true : false
formFields['nomPrenomDenominationMandataire']										 = (Value('id').of(signataire1.soussigne).eq('FormaliteSignataireQualiteMandataire') or Value('id').of(signataire1.soussigne).eq('FormaliteSignataireQualiteAutre')) ? signataire1.adresseMandataire.nomPrenomDenominationMandataire : '';
formFields['signataireAdresse']                                                      = (Value('id').of(signataire1.soussigne).eq('FormaliteSignataireQualiteMandataire') or Value('id').of(signataire1.soussigne).eq('FormaliteSignataireQualiteAutre')) ? (signataire1.adresseMandataire.numeroNomRueMandataire + ' ' + (signataire1.adresseMandataire.dataCodePostalMandataire != null ? signataire1.adresseMandataire.dataCodePostalMandataire : '') + ' ' + signataire1.adresseMandataire.villeAdresseMandataire) : '';
formFields['signatureDate']                                                          = signataire1.formaliteSignatureDate;
formFields['signatureLieu']                                                          = signataire1.formaliteSignatureLieu;
formFields['estEIRL_oui']                                                            = (Value('id').of(objet.objetModification).contains('declarationEIRL') or Value('id').of(objet.objetModification).contains('modificationEIRL') or Value('id').of(objet.objetModification).contains('finEIRL')) ? true : false;
formFields['estEIRL_non']                                                            = (Value('id').of(objet.objetModification).contains('declarationEIRL') or Value('id').of(objet.objetModification).contains('modificationEIRL') or Value('id').of(objet.objetModification).contains('finEIRL')) ? false : true;
formFields['intercalaireNombre']                                                     = "0";
formFields['nombreActivitesSoumisQualification']                                     = "0";
formFields['estNDI_oui']                                                             = false;
formFields['estNDI_non']                                                             = true;
formFields['signature']                                                              = '';

								// Intercalaire PEIRL CMB
								
//Cadre 1 - Déclaration ou modification d'affectation de patrimoine

formFields['eirl_complete_p2cmb']                                                           = true;
formFields['eirl_complete_p4cmb']                                                           = false;
formFields['eirl_complete_p0cmb']                                                           = false;
formFields['eirl_complete_p0me']                                                            = false;
formFields['declaration_initiale']															= Value('id').of(objet.objetModification).contains('declarationEIRL') ? true : false
formFields['declaration_modification']                                                      = (Value('id').of(objet.objetModification).contains('modificationEIRL') or Value('id').of(objet.objetModification).contains('finEIRL')) ? true : false;


// Cadre 2 - Rappel d'identification

formFields['eirl_siren']                                                                    = identite.siren.split(' ').join('');
formFields['eirl_nomNaissance']                                                             = Value('id').of(objet.objetModification).contains('10P') ? (Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifNomNaissance') ? modifIdentiteDeclarant.modifIdentite.modifNewNomNaissance : identite.personneLieePersonnePhysiqueNomNaissance) : identite.personneLieePersonnePhysiqueNomNaissance;
formFields['eirl_nomUsage']                                                                 = Value('id').of(objet.objetModification).contains('15P') ? (Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPseudonyme).contains('modifNomUsage') ? modifIdentiteDeclarant.modifIdentite.modifNewNomUsage : identite.personneLieePersonnePhysiqueNomUsage) : identite.personneLieePersonnePhysiqueNomUsage;
formFields['eirl_prenom']                                                                   = Value('id').of(objet.objetModification).contains('10P') ? (Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifPrenom') ? modifIdentiteDeclarant.modifIdentite.modifNewPrenoms : identite.personneLieePersonnePhysiquePrenom1) : identite.personneLieePersonnePhysiquePrenom1;

// Cadre 3 - Déclaration d'affectation de patrimoine

var declarationAffectation = $p2cmbme.cadre4DeclarationAffectationPatrimoineGroup.cadreDeclarationAffectationPatrimoine;

formFields['eirl_statutEIRL_declarationInitiale']                                           = Value('id').of(declarationAffectation.eirlStatut).eq('EirlStatutEIRLDeclarationInitiale') ? true : false;
formFields['eirl_statutEIRL_reprise']                                                       = Value('id').of(declarationAffectation.eirlStatut).eq('EirlStatutEIRLReprise') ? true : false;
formFields['eirl_denomination']                                                             = declarationAffectation.declarationPatrimoine.eirlDenomination;
if (declarationAffectation.declarationPatrimoine.eirlDateClotureExerciceComptable !== null) {
    var dateTmp = new Date(parseInt(declarationAffectation.declarationPatrimoine.eirlDateClotureExerciceComptable.getTimeInMillis()));
    var dateClotureEc = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateClotureEc = dateClotureEc.concat(pad(month.toString()));
    formFields['eirl_dateClotureExerciceComptable']          = dateClotureEc;
}
formFields['eirl_objet']                                                                    = declarationAffectation.declarationPatrimoine.eirlObjet;
formFields['eirl_depotRCS']                                                                 = Value('id').of(declarationAffectation.declarationPatrimoine.eirlChoixDepotRegistre).eq('EirlDepotRCS') ? true : false;
formFields['eirl_depotRM']                                                                  = Value('id').of(declarationAffectation.declarationPatrimoine.eirlChoixDepotRegistre).eq('EirlDepotRM') ? true : false;
formFields['eirl_precedentEIRLDenomination']                                                = declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLDenomination;
formFields['eirl_precedentEIRLRegistre_rcs']                                                = Value('id').of(declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLRegistre).eq('EirlPrecedentEIRLRegistreRcs') ? true : false;
formFields['eirl_precedentEIRLRegistre_rm']                                                 = Value('id').of(declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLRegistre).eq('EirlPrecedentEIRLRegistreRm') ? true : false;
formFields['eirl_precedentEIRLRegistre_rseirl']                                             = Value('id').of(declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLRegistre).eq('EirlPrecedentEIRLRegistreRseirl') ? true : false;
formFields['eirl_precedentEIRLLieuImmatriculation']                                         = declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLLieuImmatriculation;
formFields['eirl_precedentEIRLSIREN']                                                       = declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLSIREN != null ? (declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLSIREN.split(' ').join('')) : '';

// Cadre 4 - Rappel d'identification reltif à l'EIRL

var rappelIdentification = $p2cmbme.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreRappelIdentificationEIRL;

formFields['eirl_rappelDenomination']                                                       = rappelIdentification.eirlRappelDenomination != null ? rappelIdentification.eirlRappelDenomination : '';
formFields['eirl_rappelAdresse']                                                            = (rappelIdentification.cadreEirlRappelAdresse.rueNumeroAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.rueNumeroAdresseEirl : '') 
																				            + ' ' + (rappelIdentification.cadreEirlRappelAdresse.rueComplementAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.rueComplementAdresseEirl : '')
																				            + ' ' + (rappelIdentification.cadreEirlRappelAdresse.personneLieeAdresseCodePostalEirl != null ? rappelIdentification.cadreEirlRappelAdresse.personneLieeAdresseCodePostalEirl : '')
																		                    + ' ' + (rappelIdentification.cadreEirlRappelAdresse.villeAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.villeAdresseEirl : '');
formFields['eirl_rappelDepotRCS']                                                           = Value('id').of(rappelIdentification.eirlRappelgreffeDepot).eq('eirlRappelDepotRCS') ? true : false;
formFields['eirl_rappelDepotRM']                                                            = Value('id').of(rappelIdentification.eirlRappelgreffeDepot).eq('eirlRappelDepotRM') ? true : false;
formFields['eirl_rappelDepotRSEIRL']                                                        = Value('id').of(rappelIdentification.eirlRappelgreffeDepot).eq('eirlRappelDepotRSEIRL') ? true : false;
formFields['eirl_rappelLieuImmatriculation']                                                = rappelIdentification.eirlRappelLieuImmatriculation;

// Cadre 5 et 6- Déclaration de modification

var modifEIRL = $p2cmbme.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL;                                         

if (modifEIRL.modifDateEIRL !== null and Value('id').of(modifEIRL.objetModificationEIRL).contains('modifDenoEIRL')) {
    var dateTmp = new Date(parseInt(modifEIRL.modifDateEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDateDenoEIRL']          = dateModif;
}
formFields['new_DenoEIRL']                                                                  = modifEIRL.newDenoEIRL;
if (modifEIRL.modifDateEIRL !== null and Value('id').of(modifEIRL.objetModificationEIRL).contains('modifDateCloture')) {
    var dateTmp = new Date(parseInt(modifEIRL.modifDateEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDateClotureExerciceComptableEIRL']          = dateModif;
}                                         
if (modifEIRL.eirlNewDateClotureExerciceComptable !== null) {
    var dateTmp = new Date(parseInt(modifEIRL.eirlNewDateClotureExerciceComptable.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    formFields['eirl_newDateClotureExerciceComptable']          = dateModif;
}
if (modifEIRL.modifDateEIRL !== null and Value('id').of(modifEIRL.objetModificationEIRL).contains('modifObjet')) {
    var dateTmp = new Date(parseInt(modifEIRL.modifDateEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDateObjetEIRL']          = dateModif;
}
formFields['new_objetEIRL']                                                                 = modifEIRL.newObjetEIRL;
if (modifEIRL.modifDateEIRL !== null and Value('id').of(modifEIRL.objetModificationEIRL).contains('modifDAP')) {
    var dateTmp = new Date(parseInt(modifEIRL.modifDateEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDateDAP']          = dateModif;
}
formFields['modif_DAP']                                                                     = Value('id').of(modifEIRL.objetModificationEIRL).contains('modifDAP') ? true : false;
if (modifEIRL.modifDateEIRL !== null and Value('id').of(modifEIRL.objetModificationEIRL).contains('modifAdresse')) {
    var dateTmp = new Date(parseInt(modifEIRL.modifDateEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDateAdresseEIRL']          = dateModif;
}	
formFields['new_adresseEIRL']                                                               = Value('id').of(modifEIRL.objetModificationEIRL).contains('modifAdresse') ? ((modifEIRL.newAdresseEIRL.rueNumeroAdresseEirlNew != null ? modifEIRL.newAdresseEIRL.rueNumeroAdresseEirlNew : '')
																				            + ' ' + (modifEIRL.newAdresseEIRL.rueComplementAdresseEirlNew != null ? modifEIRL.newAdresseEIRL.rueComplementAdresseEirlNew : '')) : '';
formFields['new_adresseEIRL2']                                                              = Value('id').of(modifEIRL.objetModificationEIRL).contains('modifAdresse') ? (modifEIRL.newAdresseEIRL.personneLieeAdresseCodePostalEirlNew
																				            + ' ' + modifEIRL.newAdresseEIRL.villeAdresseEirlNew) : '';

var finDAP = $p2cmbme.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.finEIRLGroup;
																							
if (finDAP.finEIRLDeclaration.modifDateFinEIRL !== null) {
    var dateTmp = new Date(parseInt(finDAP.finEIRLDeclaration.modifDateFinEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDateFinEIRL']          = dateModif;
}
formFields['finEIRL_renonciationAvecPoursuite']                                             = Value('id').of(objet.objetModification).contains('finEIRL') ? (Value('id').of(finDAP.finEIRLDeclaration.motifFinEIRL).eq('FinEIRLRenonciationAvecPoursuite') ? true : false) : '';
formFields['finEIRL_renonciationSansPoursuite']                                             = Value('id').of(objet.objetModification).contains('finEIRL') ? (Value('id').of(finDAP.finEIRLDeclaration.motifFinEIRL).eq('FinEIRLRenonciationSansPoursuite') ? true : false) : '';
formFields['finEIRL_cessionPP']                                                             = Value('id').of(objet.objetModification).contains('finEIRL') ? (Value('id').of(finDAP.finEIRLDeclaration.motifFinEIRL).eq('FinEIRLCessionPP') ? true : false) : '';
formFields['finEIRL_cessionPM']                                                             = Value('id').of(objet.objetModification).contains('finEIRL') ? (Value('id').of(finDAP.finEIRLDeclaration.motifFinEIRL).eq('FinEIRLCessionPM') ? true : false) : '';	
if (modifEIRL.poursuiteEIRLGroup.modifDatePoursuiteEIRL !== null) {
    var dateTmp = new Date(parseInt(modifEIRL.poursuiteEIRLGroup.modifDatePoursuiteEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDatePoursuiteEIRL']          = dateModif;
}
formFields['modif_pouruiteEIRL']                                                            = Value('id').of(modifEIRL.objetModificationEIRL).contains('poursuiteEIRL') ? true : false;
formFields['EIRL_poursuiteNomPrenom']                                                       = Value('id').of(modifEIRL.objetModificationEIRL).contains('poursuiteEIRL') ? modifEIRL.poursuiteEIRLGroup.eIRLPoursuiteNomPrenom : '';

// Cadre 7 - Options fiscales

formFields['eirl_regimeFiscal_regimeImpositionBeneficesVersementLiberatoire_oui']           = Value('id').of(objet.objetModification).contains('declarationEIRL') ? ($p2cmbme.cadre4DeclarationAffectationPatrimoineGroup.impositionBenefices.regimeFiscalRegimeImpositionBeneficesVersementLiberatoire ? true : false) : false;
formFields['eirl_regimeFiscal_regimeImpositionBeneficesVersementLiberatoire_non']           = Value('id').of(objet.objetModification).contains('declarationEIRL') ? ($p2cmbme.cadre4DeclarationAffectationPatrimoineGroup.impositionBenefices.regimeFiscalRegimeImpositionBeneficesVersementLiberatoire ? false : true) : false;

/*
 * Création du dossier avec volet social : ajout du cerfa avec volet social
 */
 
var cerfaDoc1 = nash.doc //
	.load('models/cerfa_11678-05_p2cmb_avec_volet_social.pdf') //
	.apply(formFields);

//finalDoc.append(cerfaDoc.save('cerfa.pdf'));

/*
 * Création du dossier sans volet social : ajout du cerfa sans volet social
 */
 
 var cerfaDoc2 = nash.doc //
	.load('models/cerfa_11678-05_p2cmb_sans_volet_social.pdf') //
	.apply (formFields);
	cerfaDoc1.append(cerfaDoc2.save('cerfa.pdf'));

/*
 * Ajout de l'intercalaire PEIRL ME avec option fiscale
 */
if (Value('id').of($p2cmbme.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).contains('declarationEIRL') or Value('id').of($p2cmbme.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).contains('modificationEIRL') or Value('id').of($p2cmbme.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).contains('finEIRL'))
{
	var peirlMEDoc1 = nash.doc //
		.load('models/cerfa_14215-03_peirlcmb_avec_volet_social.pdf') //
		.apply (formFields);
	cerfaDoc1.append(peirlMEDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire PEIRL ME sans option fiscale
 */
if (Value('id').of($p2cmbme.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).contains('declarationEIRL') or Value('id').of($p2cmbme.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).contains('modificationEIRL') or Value('id').of($p2cmbme.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).contains('finEIRL'))
{
	var peirlMEDoc2 = nash.doc //
		.load('models/cerfa_14215-03_peirlcmb_sans_volet_social.pdf') //
		.apply (formFields);
	cerfaDoc1.append(peirlMEDoc2.save('cerfa.pdf'));
}

function appendPj(fld) {
	fld.forEach(function (elm) {
        cerfaDoc1.append(elm);
    });
}

/*
 * Ajout des PJs
 */

// PJ Déclarant

var pj=$p2cmbme.cadre9SignatureGroup.cadre9Signature.soussigne;

if(Value('id').of(pj).contains('FormaliteSignataireQualiteDeclarant')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantSignataire);
}

if((Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire') or Value('id').of(pj).contains('FormaliteSignataireQualiteAutre')) and (Value('id').of($p2cmbme.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).contains('10P') or Value('id').of($p2cmbme.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).contains('15P') or Value('id').of($p2cmbme.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).contains('17P'))) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantNonSignataire);
} 

// PJ Mandataire

if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire') or Value('id').of(pj).contains('FormaliteSignataireQualiteAutre')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjPouvoir);
	appendPj($attachmentPreprocess.attachmentPreprocess.pjIDMandataireSignataire);
}

// PJ Conjoint

var pj=$p2cmbme.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification;
if(Value('id').of(pj).contains('30P') and Value('id').of($p2cmbme.cadre3ModificationConjointGroup.cadre3ModificationConjoint.objetModifConjoint).eq('modifMentionNouveauConjoint')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjIDConjoint);
}


// PJ Etablissement

var pj=$p2cmbme.cadre6EtablissementGroup.modificationEtablissement.activiteLieu;
if(Value('id').of(pj).contains('entrepriseAdresseEntrepriseDomicile')) {
    appendPj($attachmentPreprocess.attachmentPreprocess.pjDomicile);
}

var pj=$p2cmbme.cadre6EtablissementGroup.modificationEtablissement.activiteLieu;
if(Value('id').of(pj).contains('etablissementDomiciliationOui')) {
    appendPj($attachmentPreprocess.attachmentPreprocess.pjContratDomiciliation);
}

var pj=$p2cmbme.cadre6EtablissementGroup.modificationEtablissement.cadreOrigineFonds.etablisementOrigineFonds ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsCreation') or ((Value('id').of(pj).contains('EtablissementOrigineFondsAchat')) and $p2cmbme.cadre6EtablissementGroup.modificationEtablissement.cadreOrigineFonds.precedentExploitant.fondsArtisanal) or (Value('id').of(pj).contains('EtablissementOrigineFondsCocheAutre'))) {
    appendPj($attachmentPreprocess.attachmentPreprocess.pjEtablissementCreation);
}

var pj=$p2cmbme.cadre6EtablissementGroup.modificationEtablissement.cadreOrigineFonds.etablisementOrigineFonds ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsLocationGerance')) {
    appendPj($attachmentPreprocess.attachmentPreprocess.pjLocationGerance);
	appendPj($attachmentPreprocess.attachmentPreprocess.pjLocationGerancePublication);
}

var pj=$p2cmbme.cadre6EtablissementGroup.modificationEtablissement.cadreOrigineFonds.etablisementOrigineFonds ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsGeranceMandat')) {
    appendPj($attachmentPreprocess.attachmentPreprocess.pjGeranceMandat);
	appendPj($attachmentPreprocess.attachmentPreprocess.pjGeranceMandatPublication);
}

var pj=$p2cmbme.cadre6EtablissementGroup.modificationEtablissement.cadreOrigineFonds.etablisementOrigineFonds ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsAchat') and ($p2cmbme.cadre6EtablissementGroup.modificationEtablissement.cadreOrigineFonds.precedentExploitant.cessionFonds)) {
    appendPj($attachmentPreprocess.attachmentPreprocess.pjPlanCession);
}

var pj=$p2cmbme.cadre6EtablissementGroup.modificationEtablissement.cadreOrigineFonds.etablisementOrigineFonds ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsAchat') and ($p2cmbme.cadre6EtablissementGroup.modificationEtablissement.cadreOrigineFonds.precedentExploitant.etablissementJournalAnnoncesLegalesNom != null)) {
    appendPj($attachmentPreprocess.attachmentPreprocess.pjAchat);
	appendPj($attachmentPreprocess.attachmentPreprocess.pjAchatPublication);
}

// PJ Eirl

var pj=$p2cmbme.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification;
if(Value('id').of(pj).contains('declarationEIRL') or Value('id').of(pj).contains('modificationEIRL')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjDAP);
}

var pj=$p2cmbme.cadre4DeclarationAffectationPatrimoineGroup.informationsComplementaires.contenuDAP;
if(Value('id').of(pj).contains('bienImmo') or Value('id').of($p2cmbme.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.contenuDAP).contains('bienImmo')) {
    appendPj($attachmentPreprocess.attachmentPreprocess.pjDAPActeNotarie);
}

var pj=$p2cmbme.cadre4DeclarationAffectationPatrimoineGroup.informationsComplementaires.contenuDAP;
if(Value('id').of(pj).contains('bienRapportEvaluation') or Value('id').of($p2cmbme.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.contenuDAP).contains('bienRapportEvaluation')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjDAPRapportEvaluation);
}

var pj=$p2cmbme.cadre4DeclarationAffectationPatrimoineGroup.informationsComplementaires.contenuDAP;
if(Value('id').of(pj).contains('bienCommunIndivis') or Value('id').of($p2cmbme.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.contenuDAP).contains('bienCommunIndivis')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjDAPAccordTiers);
}

var pj=$p2cmbme.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification ;
if(Value('id').of(pj).contains('modifActivite')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjSPI);
}

/*
 * Enregistrement du fichier (en mémoire)
 */

var finalDoc = cerfaDoc1.save('PE_P2_CMB_ME_Modification.pdf');

var data = [ spec.createData({
    id : 'record',
    label : 'Déclaration de modification d\'un commerçant et/ou d\'un artisan en micro-entrepreneur',
    description : 'Voici le formulaire obtenu à partir des données saisies.',
    type : 'FileReadOnly',
    value : [ finalDoc ]
}) ];

var groups = [ spec.createGroup({
    id : 'generated',
    label : 'Génération du dossier',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Déclaration de modification d\'un commerçant et/ou d\'un artisan en micro-entrepreneur',
    groups : groups
});
