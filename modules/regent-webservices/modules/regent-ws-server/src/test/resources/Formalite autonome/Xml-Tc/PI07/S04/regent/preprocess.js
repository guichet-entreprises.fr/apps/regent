/* //Getting the info to be sent in parameter of the ws generate regent

var regentVersion = $forms1.regentInfos.regentVersion;
var eventRegent = $forms1.regentInfos.eventRegent;
var authorityType = $forms1.regentInfos.authorityType;
var authorityId = $forms1.regentInfos.authorityId;


 */

var regentFields = {};
 regentFields['/REGENT-XML/Emetteur']="Z1611";
 regentFields['/REGENT-XML/Destinataire']="C7801";
  regentFields['/REGENT-XML/DateHeureEmission']="2018-03-15T19:05:00";
  regentFields['/REGENT-XML/VersionMessage']="V2008.11";
 regentFields['/REGENT-XML/VersionNorme']="V2008.11";
 regentFields['/REGENT-XML/ServiceApplicatif/Specification/NomService']="ReceptionLiasseDéclarant";
 regentFields['/REGENT-XML/ServiceApplicatif/Specification/VersionService']="V2008.11";
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF']=null;
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C01']="2018-03-15";
  regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02']=null;
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C03']="0";
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C04']="2018-03-15";
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C05']="M";
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C06']="O";
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.1']="01P";
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2']="2015-07-21";
 var dest = [];
 dest.push["M7501"];
 dest.push["G7501"]
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20']=dest,
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C36']="Poutkline Igor";
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']="75104";
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.5']="2";
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8']="75004";
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']="RUE";
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12']="de Bourg Tibourg";
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']="PARIS";
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.3']="ip@oste.fr";
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.1']="POUTKLINE";
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.3']="75104";
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.5']="A";
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.8']="75004";
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.11']="RUE";
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.12']="Bourg Tibourg";
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.13']="PARIS";
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C41']="Paris";
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C42']="2016-05-17";
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C43']=null,
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.1']="1";
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.2']="POUTKLINE";
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.3']="IGOR";
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P02']="";
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.1']="1955-05-12";
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.2']="99123";
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.3']="FEDERATION DE RUSSIE";
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P06']="A";
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/AIP/P11']=null;
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/AIP/P14']=null;
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U01']=null;
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U01/U01.1']="G7501";
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U02']="552100554";
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U04']="99";
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U06']="O"


// Call to the WS payout

var response = nash.service.request('http://localhost:20080/regent-ws-server/api/private/v1/xml-regent/generate/V2008.11/CFE/CMA75?listTypeEvenement=10P')
		.accept('json') //
		.post(regentFields);

_log.info("Payout WS called with status  {}", response.status);
//extract the proofs of the payment from the body of the response

//Get xml regent value 

if (response != null && response.getStatus() == 200) {
	var xmlRegentStr = response.asBytes();
    _log.info("status is "+ response.status);
	_log.info("xmlRegentStr is "+ xmlRegentStr);
    nash.record.saveFile("2-regent/RegentTagGenerated.xml",xmlRegentStr);
     }
	 
// create spec data file to be shown to the user 
var dataList = [];
var data = spec.createData({
				    id: 'Status',
				    label: "Avec le statut",
				    type: 'Integer',
				    value: response.status
			});
dataList.push(data);

if (response.getStatus() == 500) {
	
data = spec.createData({
				    id: 'Error',
				    label: "Les erreurs retournées sont",
				    type: 'String',
				    value: response.asString()
			});
dataList.push(data);			
}
// output to confirm
return spec.create({
	id : 'splitPaymentconfirmation',
	label : "split confirmation message",
	groups : [ spec.createGroup({
		id : 'confirmationMessage',
		description : "L'appel au ws dé génération du regent a été effectué",
		data : dataList
	}) ]
});
