// prepare info to send to pushingBox
var recordUid 		= '<unknown>'
var formName 		= '<unknown>'
var formId 			= '<unknown>'
var dateTime 		= new Date()
var pushingBoxDevID	= _CONFIG_ ? _CONFIG_.get('pushingbox.devid') : null

if (!pushingBoxDevID) {
	pushingBoxDevID = 'v1E8F075A7DF1552';
}

if (nash.record && nash.record.description) {
	recordUid 	= nash.record.description().recordUid
	formName 	= nash.record.description().title
	formId		= nash.record.description().formUid
}

_log.info("recordUid is  {}", recordUid);
_log.info("formName is  {}", formName);
_log.info("formId is  {}", formId);
_log.info("dateTime is  {}", dateTime);

// call pushingBox
var response = nash.service.request('http://api.pushingbox.com/pushingbox')
	.param("recordId",	recordUid)
	.param("formName",	formName)
	.param("formId",	formId)
	.param("dateTime",	dateTime)
	.param("devid",		pushingBoxDevID)
	.get();



//prepare info to send 
var region = "idefr";
_log.info("region  {}",region);
var type_auth = "Direction Régionale Interdépartementale de l'Alimentation, de l'Agriculture et de la Forêt";
_log.info("type_auth  {}",type_auth);
var attachement= "/3-review/generated/generated.formulaire-1-PE_PO_PL_ME_Creation.pdf"

// output to confirm

return spec.create({
	id: 'authority',
	label: "functional id of authority",
	groups: [spec.createGroup({
			id: 'authority',
			label: "authority",
			data: [spec.createData({
					id: 'authorityType',
					label: "Code de l'autorité compétente",
					type: 'String',
					mandatory: true,
					value: type_auth
				}),
				spec.createData({
					id: 'zipCode',
					label: "code postal",
					type: 'String',
					mandatory: true,
					value: region
				}),
				spec.createData({
					id: 'attachment',
					label: "Pièce jointe",
					type: 'String',
					mandatory: true,
					value: attachement
				})
			]
		})]
});
