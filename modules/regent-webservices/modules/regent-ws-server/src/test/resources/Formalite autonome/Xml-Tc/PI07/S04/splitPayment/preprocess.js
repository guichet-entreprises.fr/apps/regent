var recordUid = nash.record.description().recordUid;
var testAuthList = [];

if(Value('id').of($choiceAuthNumber.choiceAuth.numberAuth).contains('oneAuth')){

		var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', "GREFFE05061") //
	           .connectionTimeout(10000) //
	           .receiveTimeout(10000) //
	           .accept('json') //
	           .get();

		//result			   
					   
		var receiverInfo = response.asObject();

		// prepare all information of receiver to create data.xml
		var contactInfo = !receiverInfo.contactInfo ? null : JSON.parse(receiverInfo.contactInfo);
		var partnerID = !contactInfo.paymentPartnerId ? null : contactInfo.paymentPartnerId;

		var repartitionPayment = {
									"dossierId" : recordUid,
									"lines" : [{
										"amount" : "10000",
										"description" : "Frais inscription du dossier",
										"partnerId" : partnerID
									}]
								};

}

if(Value('id').of($choiceAuthNumber.choiceAuth.numberAuth).contains('multiAc')){
		//On générera le TC-XML de la greffe de Marseille
		testAuthList.push("GREFFE42218");
				var response1 = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', "GREFFE42218") //
	           .connectionTimeout(10000) //
	           .receiveTimeout(10000) //
	           .accept('json') //
	           .get();

		//result			   
					   
		var receiverInfoMarseille = response1.asObject();

		// prepare all information of receiver to create data.xml
		var contactInfoMarseille = !receiverInfoMarseille.contactInfo ? null : JSON.parse(receiverInfoMarseille.contactInfo);
		var partnerIdGreffeMarseille = !contactInfoMarseille.paymentPartnerId ? null : contactInfoMarseille.paymentPartnerId;
		//On générera le TC-XML de la greffe de Pontoise
				var response2 = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', "GREFFE05061") //
	           .connectionTimeout(10000) //
	           .receiveTimeout(10000) //
	           .accept('json') //
	           .get();

		//result			   
					   
		var receiverInfoPontoise = response2.asObject();

		// prepare all information of receiver to create data.xml
		var contactInfoPontoise = !receiverInfoPontoise.contactInfo ? null : JSON.parse(receiverInfoPontoise.contactInfo);
		var partnerIdGreffePontoise = !contactInfoPontoise.paymentPartnerId ? null : contactInfoPontoise.paymentPartnerId;
		
		//On ne devras pas générer le TC-XML de la CMA Haute Garonne
 		
				var response3 = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', "CMA69386") //
	           .connectionTimeout(10000) //
	           .receiveTimeout(10000) //
	           .accept('json') //
	           .get();

		//result			   
					   
		var receiverInfoGaronne = response3.asObject(); 

 		// prepare all information of receiver to create data.xml
		var contactInfoGaronne = !receiverInfoGaronne.contactInfo ? null : JSON.parse(receiverInfoGaronne.contactInfo);
		var partnerIdGaronne = !contactInfoGaronne.paymentPartnerId ? null : contactInfoGaronne.paymentPartnerId;
		
		var repartitionPayment = {
									"dossierId" : recordUid,
									"lines" : [
										{
											"amount" : "4000",
											"description" : "Frais inscription du dossier Greffe Marseille",
											"partnerId" : partnerIdGreffeMarseille
										},
										{
											"amount" : "3000",
											"description" : "Frais inscription du dossier Greffe Pontoise",
											"partnerId" : partnerIdGreffePontoise
										},
										{
											"amount" : "3000",
											"description" : "Frais inscription du dossier CMA Haute Garonne",
											"partnerId" : partnerIdGaronne											
										}
									]
								};	
}


_log.info("SPLIT : repartitionPayment :  {}", repartitionPayment);

_log.info("SPLIT : JSON.stringify {}", JSON.stringify(repartitionPayment));

_log.info("SPLIT : Record UID {}", recordUid);
// Call to the WS splitpayment

var responseFinal = nash.service.request(
		'http://localhost:50080/proxy-payment/private//api/v1/payment/payments') //
.dataType('application/json') //
.accept('json') //
.post(JSON.stringify(repartitionPayment));

// extract the proofs of the payment from the body of the responseFinal
var proofs = responseFinal.asString();
var jsonProofs = JSON.parse(proofs) ;
for (key in jsonProofs){
	nash.record.saveFile("/splitPayment/generated/"+key,nash.util.convertBase64ToByte(jsonProofs[key]));
}
_log.info("SPLIT : proof saved successfull");

// output to confirm
return spec.create({
    id : 'splitPaymentconfirmation',
    label : 'Résultat du split',
    groups : [ spec.createGroup({
        id : 'result',
        label : 'Résultat du split',
        description : 'La répartition du montant à payer aux différentes autorités s\'est bien effectuée',
        data : []
    }) ]
});


