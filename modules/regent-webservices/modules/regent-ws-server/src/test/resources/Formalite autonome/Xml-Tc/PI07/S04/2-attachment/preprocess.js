// PJ Déclarant

var userDeclarant;
if ($p2cmbme.cadre1ObjetModificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiqueNomUsage != null) {
    var userDeclarant = $p2cmbme.cadre1ObjetModificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiqueNomUsage + '  ' + $p2cmbme.cadre1ObjetModificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiquePrenom1 ;
} else {
    var userDeclarant = $p2cmbme.cadre1ObjetModificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiqueNomNaissance + '  '+ $p2cmbme.cadre1ObjetModificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiquePrenom1 ;
}

var pj=$p2cmbme.cadre9SignatureGroup.cadre9Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteDeclarant')) {
    attachment('pjIDDeclarantSignataire', 'pjIDDeclarantSignataire', { label: userDeclarant, mandatory:"true"});
}

var pj=$p2cmbme.cadre9SignatureGroup.cadre9Signature.soussigne;
if((Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire') or Value('id').of(pj).contains('FormaliteSignataireQualiteAutre')) and (Value('id').of($p2cmbme.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).contains('10P') or Value('id').of($p2cmbme.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).contains('15P') or Value('id').of($p2cmbme.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).contains('17P'))) {
    attachment('pjIDDeclarantNonSignataire', 'pjIDDeclarantNonSignataire', { label: userDeclarant, mandatory:"true"});
}
// PJ Mandataire

var pj=$p2cmbme.cadre9SignatureGroup.cadre9Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire') or Value('id').of(pj).contains('FormaliteSignataireQualiteAutre')) {
    attachment('pjPouvoir', 'pjPouvoir', { mandatory:"true"});
}

var userMandataire=$p2cmbme.cadre9SignatureGroup.cadre9Signature.adresseMandataire

var pj=$p2cmbme.cadre9SignatureGroup.cadre9Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire') or Value('id').of(pj).contains('FormaliteSignataireQualiteAutre')) {
    attachment('pjIDMandataireSignataire', 'pjIDMandataireSignataire', {label: userMandataire.nomPrenomDenominationMandataire, mandatory:"true"});
}

// PJ Conjoint

var pj=$p2cmbme.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification;
if(Value('id').of(pj).contains('30P') and Value('id').of($p2cmbme.cadre3ModificationConjointGroup.cadre3ModificationConjoint.objetModifConjoint).eq('modifMentionNouveauConjoint')) {
    attachment('pjIDConjoint', 'pjIDConjoint', { mandatory:"true"});
}

// PJ Etablissement

var pj=$p2cmbme.cadre6EtablissementGroup.modificationEtablissement.activiteLieu;
if(Value('id').of(pj).contains('entrepriseAdresseEntrepriseDomicile')) {
    attachment('pjDomicile', 'pjDomicile', { mandatory:"true"});
}

var pj=$p2cmbme.cadre6EtablissementGroup.modificationEtablissement.activiteLieu ;
if(Value('id').of(pj).contains('etablissementDomiciliationOui')) {
    attachment('pjContratDomiciliation', 'pjContratDomiciliation', { mandatory:"true"});
}

var pj=$p2cmbme.cadre6EtablissementGroup.modificationEtablissement.cadreOrigineFonds.etablisementOrigineFonds ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsCreation') or ((Value('id').of(pj).contains('EtablissementOrigineFondsAchat')) and $p2cmbme.cadre6EtablissementGroup.modificationEtablissement.cadreOrigineFonds.precedentExploitant.fondsArtisanal) or (Value('id').of(pj).contains('EtablissementOrigineFondsCocheAutre'))) {
    attachment('pjEtablissementCreation', 'pjEtablissementCreation', { mandatory:"true"});
}

var pj=$p2cmbme.cadre6EtablissementGroup.modificationEtablissement.cadreOrigineFonds.etablisementOrigineFonds ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsLocationGerance')) {
    attachment('pjLocationGerance', 'pjLocationGerance', { mandatory:"true"});
}

var pj=$p2cmbme.cadre6EtablissementGroup.modificationEtablissement.cadreOrigineFonds.etablisementOrigineFonds ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsLocationGerance')) {
    attachment('pjLocationGerancePublication', 'pjLocationGerancePublication', { mandatory:"true"});
}

var pj=$p2cmbme.cadre6EtablissementGroup.modificationEtablissement.cadreOrigineFonds.etablisementOrigineFonds ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsGeranceMandat')) {
    attachment('pjGeranceMandat', 'pjGeranceMandat', { mandatory:"true"});
}

var pj=$p2cmbme.cadre6EtablissementGroup.modificationEtablissement.cadreOrigineFonds.etablisementOrigineFonds ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsGeranceMandat')) {
    attachment('pjGeranceMandatPublication', 'pjGeranceMandatPublication', { mandatory:"true"});
}

var pj=$p2cmbme.cadre6EtablissementGroup.modificationEtablissement.cadreOrigineFonds.etablisementOrigineFonds ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsAchat') and ($p2cmbme.cadre6EtablissementGroup.modificationEtablissement.cadreOrigineFonds.precedentExploitant.cessionFonds)) {
    attachment('pjPlanCession', 'pjPlanCession', { mandatory:"true"});
}

var pj=$p2cmbme.cadre6EtablissementGroup.modificationEtablissement.cadreOrigineFonds.etablisementOrigineFonds ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsAchat') and ($p2cmbme.cadre6EtablissementGroup.modificationEtablissement.cadreOrigineFonds.precedentExploitant.etablissementJournalAnnoncesLegalesNom != null)) {
    attachment('pjAchat', 'pjAchat', { mandatory:"true"});
}

var pj=$p2cmbme.cadre6EtablissementGroup.modificationEtablissement.cadreOrigineFonds.etablisementOrigineFonds ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsAchat') and ($p2cmbme.cadre6EtablissementGroup.modificationEtablissement.cadreOrigineFonds.precedentExploitant.etablissementJournalAnnoncesLegalesNom != null)) {
    attachment('pjAchatPublication', 'pjAchatPublication', { mandatory:"true"});
}

// PJ EIRL

var pj=$p2cmbme.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification;
if(Value('id').of(pj).contains('declarationEIRL') or Value('id').of(pj).contains('modificationEIRL')) {
    attachment('pjDAP', 'pjDAP', { mandatory:"true"});
}

var pj=$p2cmbme.cadre4DeclarationAffectationPatrimoineGroup.informationsComplementaires.contenuDAP;
if(Value('id').of(pj).contains('bienImmo') or Value('id').of($p2cmbme.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.contenuDAP).contains('bienImmo')) {
    attachment('pjDAPActeNotarie', 'pjDAPActeNotarie', { mandatory:"true"});
}

var pj=$p2cmbme.cadre4DeclarationAffectationPatrimoineGroup.informationsComplementaires.contenuDAP;
if(Value('id').of(pj).contains('bienRapportEvaluation') or Value('id').of($p2cmbme.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.contenuDAP).contains('bienRapportEvaluation')) {
    attachment('pjDAPRapportEvaluation', 'pjDAPRapportEvaluation', { mandatory:"true"});
}

var pj=$p2cmbme.cadre4DeclarationAffectationPatrimoineGroup.informationsComplementaires.contenuDAP;
if(Value('id').of(pj).contains('bienCommunIndivis') or Value('id').of($p2cmbme.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.contenuDAP).contains('bienCommunIndivis')) {
    attachment('pjDAPAccordTiers', 'pjDAPAccordTiers', { mandatory:"true"});
}

// SPI

var pj=$p2cmbme.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification ;
if(Value('id').of(pj).contains('modifActivite')) {
    attachment('pjSPI', 'pjSPI', { mandatory:"false"});
}

