//Récupération des champs saisis dans la première étape
var userEmail = $step01.regent.userEmail;
var typeEvenement = $step01.regent.eventCode;
var regentVersion = $step01.regent.regentVersion;

var response = nash.service.request('${regent.baseUrl}/v1/public/generateMappingFile') //
        .connectionTimeout(60000) //
        .receiveTimeout(60000) //
    	.dataType("form")//
    	.param('regentVersion',[regentVersion])//
    	.param('listTypeEvenement',[typeEvenement])//
		.get();

	
//Get xml regent value 

if (response != null && response.getStatus() == 200) {
	var xmlRegentStr = response.asBytes();
    _log.info("status is "+ response.status);
	_log.info("xmlRegentStr is "+ xmlRegentStr);
    nash.record.saveFile("/2-user-data/RegentTagGenerated.js",xmlRegentStr);
     }
     
return spec.create({
    id : 'regent',
    label : 'XML Regent',
    groups : [ spec.createGroup({
        id : 'datas',
        label : 'Mapping Regent',
        data : [ spec.createData({
            id : 'mapping_regent',
            label : 'Mapping Regent généré',
            type : 'TextReadOnly',
            value : 'Le fichier a été enregistré veuillez le trouver dans le dossier.'
        }) ]
    }) ]
});
