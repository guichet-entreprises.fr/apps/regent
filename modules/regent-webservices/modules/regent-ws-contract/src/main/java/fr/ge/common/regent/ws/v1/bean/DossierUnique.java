/**
 * 
 */
package fr.ge.common.regent.ws.v1.bean;

import java.util.List;

/**
 * Représente le bloc dossierUnique du XML-TC qui contient toutes les
 * informations du dossier.
 * 
 * @author $Author: lhoffste $
 * @version $Revision: 0 $
 */
public class DossierUnique {

    /** Information du dossier unique {@link IdentificationDossierUnique}. */
    private IdentificationDossierUnique identificationDossierUnique;
    /**
     * Listes de tous les destinataires du dossier
     * {@link DestinataireDossierUnique}.
     */
    private List<DestinataireDossierUnique> destinataireDossierUnique;
    /**
     * Liste de toutes les pièces justificatives du dossier {@link PieceJointe}.
     */
    private List<PieceJointe> pieceJointe;
    /** DossierCfe contenant les dossiers destinés aux CFE. */
    private DossierCfe dossierCfe;

    /**
     * Constructeur de la classe.
     * 
     * @param identificationDossierUnique
     *            {@link #identificationDossierUnique}.
     * @param destinataireDossierUnique
     *            {@link #destinataireDossierUnique}.
     * @param pieceJointe
     *            {@link #pieceJointe}.
     * @param dossierCfe
     *            {@link #dossierCfe}.
     */
    public DossierUnique(IdentificationDossierUnique identificationDossierUnique, List<DestinataireDossierUnique> destinataireDossierUnique, List<PieceJointe> pieceJointe, DossierCfe dossierCfe) {
        super();
        this.identificationDossierUnique = identificationDossierUnique;
        this.destinataireDossierUnique = destinataireDossierUnique;
        this.pieceJointe = pieceJointe;
        this.dossierCfe = dossierCfe;
    }

    /**
     * Constructeur de la classe.
     *
     */
    public DossierUnique() {
        super();
    }

    /**
     * Accesseur sur l'attribut {@link #identificationDossierUnique}.
     *
     * @return IdentificationDossierUnique identificationDossierUnique
     */
    public IdentificationDossierUnique getIdentificationDossierUnique() {
        return identificationDossierUnique;
    }

    /**
     * Mutateur sur l'attribut {@link #identificationDossierUnique}.
     *
     * @param identificationDossierUnique
     *            la nouvelle valeur de l'attribut identificationDossierUnique
     */
    public void setIdentificationDossierUnique(IdentificationDossierUnique identificationDossierUnique) {
        this.identificationDossierUnique = identificationDossierUnique;
    }

    /**
     * Accesseur sur l'attribut {@link #destinataireDossierUnique}.
     *
     * @return List<DestinataireDossierUnique> destinataireDossierUnique
     */
    public List<DestinataireDossierUnique> getDestinataireDossierUnique() {
        return destinataireDossierUnique;
    }

    /**
     * Mutateur sur l'attribut {@link #destinataireDossierUnique}.
     *
     * @param destinataireDossierUnique
     *            la nouvelle valeur de l'attribut destinataireDossierUnique
     */
    public void setDestinataireDossierUnique(List<DestinataireDossierUnique> destinataireDossierUnique) {
        this.destinataireDossierUnique = destinataireDossierUnique;
    }

    /**
     * Accesseur sur l'attribut {@link #pieceJointe}.
     *
     * @return List<PieceJointe> pieceJointe
     */
    public List<PieceJointe> getPieceJointe() {
        return pieceJointe;
    }

    /**
     * Mutateur sur l'attribut {@link #pieceJointe}.
     *
     * @param pieceJointe
     *            la nouvelle valeur de l'attribut pieceJointe
     */
    public void setPieceJointe(List<PieceJointe> pieceJointe) {
        this.pieceJointe = pieceJointe;
    }

    /**
     * Accesseur sur l'attribut {@link #dossierCfe}.
     *
     * @return DossierCfe dossierCfe
     */
    public DossierCfe getDossierCfe() {
        return dossierCfe;
    }

    /**
     * Mutateur sur l'attribut {@link #dossierCfe}.
     *
     * @param dossierCfe
     *            la nouvelle valeur de l'attribut dossierCfe
     */
    public void setDossierCfe(DossierCfe dossierCfe) {
        this.dossierCfe = dossierCfe;
    }

}
