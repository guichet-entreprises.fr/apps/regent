/**
 * 
 */
package fr.ge.common.regent.ws.v1.bean;

/**
 * Contient l'adresse de correspondance du déclarant.
 * 
 * @author $Author: lhoffste $
 * @version $Revision: 0 $
 */
public class AdresseCorrespondant {

    /** numéro de la voie. */
    private String numeroDeVoie;
    /** indécie de répétition */
    private String indiceDeRepetition;
    /** type de la voie. */
    private String typeDeVoie;
    /** libellé de la voie. */
    private String libelleDeVoie;
    /** nom de la localité. */
    private String localite;
    /** complément d'adresse. */
    private String complementDeLocalisation;
    /** code postal. */
    private String codePostal;
    /** Bureau distributeur. */
    private String bureauDistributeur;
    /** code pays. */
    private String codePays;
    /** adresse mail. */
    private String adresseEmail;

    /**
     * Constructeur de la classe.
     *
     * @param numeroDeVoie
     *            {@link #numeroDeVoie}
     * @param typeDeVoie
     *            {@link #typeDeVoie}
     * @param libelleDeVoie
     *            {@link #libelleDeVoie}
     * @param localite
     *            {@link #localite}
     * @param complementDeLocalisation
     *            {@link #complementDeLocalisation}
     * @param codePostal
     *            {@link #codePostal}
     * @param bureauDistributeur
     *            {@link #getBureauDistributeur()}
     * @param codePays
     *            {@link #codePays}
     * @param adresseEmail
     *            {@link #adresseEmail}
     */
    public AdresseCorrespondant(String numeroDeVoie, String typeDeVoie, String libelleDeVoie, String localite, String complementDeLocalisation, String codePostal, String bureauDistributeur,
            String codePays, String adresseEmail) {
        super();
        this.numeroDeVoie = numeroDeVoie;
        this.typeDeVoie = typeDeVoie;
        this.libelleDeVoie = libelleDeVoie;
        this.localite = localite;
        this.complementDeLocalisation = complementDeLocalisation;
        this.codePostal = codePostal;
        this.bureauDistributeur = bureauDistributeur;
        this.codePays = codePays;
        this.adresseEmail = adresseEmail;
    }

    /**
     * Constructeur de la classe.
     *
     */
    public AdresseCorrespondant() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * Accesseur sur l'attribut {@link #numeroDeVoie}.
     *
     * @return String numeroDeVoie
     */
    public String getNumeroDeVoie() {
        return numeroDeVoie;
    }

    /**
     * Mutateur sur l'attribut {@link #numeroDeVoie}.
     *
     * @param numeroDeVoie
     *            la nouvelle valeur de l'attribut numeroDeVoie
     */
    public void setNumeroDeVoie(String numeroDeVoie) {
        this.numeroDeVoie = numeroDeVoie;
    }

    /**
     * @return the indiceDeRepetition
     */
    public String getIndiceDeRepetition() {
        return indiceDeRepetition;
    }

    /**
     * @param indiceDeRepetition
     *            the indiceDeRepetition to set
     */
    public void setIndiceDeRepetition(String indiceDeRepetition) {
        this.indiceDeRepetition = indiceDeRepetition;
    }

    /**
     * Accesseur sur l'attribut {@link #typeDeVoie}.
     *
     * @return String typeDeVoie
     */
    public String getTypeDeVoie() {
        return typeDeVoie;
    }

    /**
     * Mutateur sur l'attribut {@link #typeDeVoie}.
     *
     * @param typeDeVoie
     *            la nouvelle valeur de l'attribut typeDeVoie
     */
    public void setTypeDeVoie(String typeDeVoie) {
        this.typeDeVoie = typeDeVoie;
    }

    /**
     * Accesseur sur l'attribut {@link #libelleDeVoie}.
     *
     * @return String libelleDeVoie
     */
    public String getLibelleDeVoie() {
        return libelleDeVoie;
    }

    /**
     * Mutateur sur l'attribut {@link #libelleDeVoie}.
     *
     * @param libelleDeVoie
     *            la nouvelle valeur de l'attribut libelleDeVoie
     */
    public void setLibelleDeVoie(String libelleDeVoie) {
        this.libelleDeVoie = libelleDeVoie;
    }

    /**
     * Accesseur sur l'attribut {@link #localite}.
     *
     * @return String localite
     */
    public String getLocalite() {
        return localite;
    }

    /**
     * Mutateur sur l'attribut {@link #localite}.
     *
     * @param localite
     *            la nouvelle valeur de l'attribut localite
     */
    public void setLocalite(String localite) {
        this.localite = localite;
    }

    /**
     * Accesseur sur l'attribut {@link #complementDeLocalisation}.
     *
     * @return String complementDeLocalisation
     */
    public String getComplementDeLocalisation() {
        return complementDeLocalisation;
    }

    /**
     * Mutateur sur l'attribut {@link #complementDeLocalisation}.
     *
     * @param complementDeLocalisation
     *            la nouvelle valeur de l'attribut complementDeLocalisation
     */
    public void setComplementDeLocalisation(String complementDeLocalisation) {
        this.complementDeLocalisation = complementDeLocalisation;
    }

    /**
     * Accesseur sur l'attribut {@link #codePostal}.
     *
     * @return String codePostal
     */
    public String getCodePostal() {
        return codePostal;
    }

    /**
     * Mutateur sur l'attribut {@link #codePostal}.
     *
     * @param codePostal
     *            la nouvelle valeur de l'attribut codePostal
     */
    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    /**
     * Accesseur sur l'attribut {@link #bureauDistributeur}.
     *
     * @return String bureauDistributeur
     */
    public String getBureauDistributeur() {
        return bureauDistributeur;
    }

    /**
     * Mutateur sur l'attribut {@link #bureauDistributeur}.
     *
     * @param bureauDistributeur
     *            la nouvelle valeur de l'attribut bureauDistributeur
     */
    public void setBureauDistributeur(String bureauDistributeur) {
        this.bureauDistributeur = bureauDistributeur;
    }

    /**
     * Accesseur sur l'attribut {@link #codePays}.
     *
     * @return String codePays
     */
    public String getCodePays() {
        return codePays;
    }

    /**
     * Mutateur sur l'attribut {@link #codePays}.
     *
     * @param codePays
     *            la nouvelle valeur de l'attribut codePays
     */
    public void setCodePays(String codePays) {
        this.codePays = codePays;
    }

    /**
     * Accesseur sur l'attribut {@link #adresseEmail}.
     *
     * @return String adresseEmail
     */
    public String getAdresseEmail() {
        return adresseEmail;
    }

    /**
     * Mutateur sur l'attribut {@link #adresseEmail}.
     *
     * @param adresseEmail
     *            la nouvelle valeur de l'attribut adresseEmail
     */
    public void setAdresseEmail(String adresseEmail) {
        this.adresseEmail = adresseEmail;
    }

}
