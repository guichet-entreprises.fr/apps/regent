/**
 * 
 */
package fr.ge.common.regent.ws.v1.bean;

/**
 * Objet représentant le XML-TC.
 * 
 * @author $Author: lhoffste $
 * @version $Revision: 0 $
 */
public class TcBean {

    /** Version du xml TC */
    private String version;
    /** Code emetteur du xml-tc */
    private String emetteur;
    /** Code destinataire */
    private String destinataire;
    /** code du partenaire emetteur */
    private String codePartenaireEmetteur;
    /** type de l'autorisation */
    private String typeAutorisation;
    /** indice de la pièce jointe */
    private String indicePieceJointe;
    /** type de la pièce jointe */
    private String typePieceJointe;
    /** nom du fichier dela pièce jointe */
    private String fichierPieceJointe;
    /** numéro de liasse */
    private String numeroDeLiasse;
    /** indice de la pièce jointe représentant le xml-regent */
    private String indiceLiasseXml;
    /** date et heure de génération du xml */
    private String dateHeureGenerationXml;
    /** commentaire */
    private String commentaire;
    /** dossier unique */
    private DossierUnique dossierUnique;
    /** informations correspendnat. */
    private Correspondant correspondant;

    /**
     * Constructeur de la classe.
     *
     * @param version
     *            {@link #version}
     * @param emetteur
     *            {@link #emetteur}
     * @param destinataire
     *            {@link #destinataire}
     * @param codePartenaireEmetteur
     *            {@link #codePartenaireEmetteur}
     * @param typeAutorisation
     *            {@link #typeAutorisation}
     * @param indicePieceJointe
     *            {@link #indicePieceJointe}
     * @param typePieceJointe
     *            {@link #typePieceJointe}
     * @param fichierPieceJointe
     *            {@link #fichierPieceJointe}
     * @param numeroDeLiasse
     *            {@link #numeroDeLiasse}
     * @param indiceLiasseXml
     *            {@link #indiceLiasseXml}
     * @param dateHeureGenerationXml
     *            {@link #dateHeureGenerationXml}
     * @param commentaire
     *            {@link #commentaire}
     * @param dossierUnique
     *            {@link #dossierUnique}
     * @param correspondant
     *            {@link #correspondant}
     */
    public TcBean(String version, String emetteur, String destinataire, String codePartenaireEmetteur, String typeAutorisation, String indicePieceJointe, String typePieceJointe,
            String fichierPieceJointe, String numeroDeLiasse, String indiceLiasseXml, String dateHeureGenerationXml, String commentaire, DossierUnique dossierUnique, Correspondant correspondant) {
        super();
        this.version = version;
        this.emetteur = emetteur;
        this.destinataire = destinataire;
        this.codePartenaireEmetteur = codePartenaireEmetteur;
        this.typeAutorisation = typeAutorisation;
        this.indicePieceJointe = indicePieceJointe;
        this.typePieceJointe = typePieceJointe;
        this.fichierPieceJointe = fichierPieceJointe;
        this.numeroDeLiasse = numeroDeLiasse;
        this.indiceLiasseXml = indiceLiasseXml;
        this.dateHeureGenerationXml = dateHeureGenerationXml;
        this.commentaire = commentaire;
        this.dossierUnique = dossierUnique;
        this.correspondant = correspondant;
    }

    /**
     * Constructeur de la classe.
     *
     */
    public TcBean() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * Accesseur sur l'attribut {@link #version}.
     *
     * @return String version
     */
    public String getVersion() {
        return version;
    }

    /**
     * Mutateur sur l'attribut {@link #version}.
     *
     * @param version
     *            la nouvelle valeur de l'attribut version
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * Accesseur sur l'attribut {@link #emetteur}.
     *
     * @return String emetteur
     */
    public String getEmetteur() {
        return emetteur;
    }

    /**
     * Mutateur sur l'attribut {@link #emetteur}.
     *
     * @param emetteur
     *            la nouvelle valeur de l'attribut emetteur
     */
    public void setEmetteur(String emetteur) {
        this.emetteur = emetteur;
    }

    /**
     * Accesseur sur l'attribut {@link #destinataire}.
     *
     * @return String destinataire
     */
    public String getDestinataire() {
        return destinataire;
    }

    /**
     * Mutateur sur l'attribut {@link #destinataire}.
     *
     * @param destinataire
     *            la nouvelle valeur de l'attribut destinataire
     */
    public void setDestinataire(String destinataire) {
        this.destinataire = destinataire;
    }

    /**
     * Accesseur sur l'attribut {@link #codePartenaireEmetteur}.
     *
     * @return String codePartenaireEmetteur
     */
    public String getCodePartenaireEmetteur() {
        return codePartenaireEmetteur;
    }

    /**
     * Mutateur sur l'attribut {@link #codePartenaireEmetteur}.
     *
     * @param codePartenaireEmetteur
     *            la nouvelle valeur de l'attribut codePartenaireEmetteur
     */
    public void setCodePartenaireEmetteur(String codePartenaireEmetteur) {
        this.codePartenaireEmetteur = codePartenaireEmetteur;
    }

    /**
     * Accesseur sur l'attribut {@link #typeAutorisation}.
     *
     * @return String typeAutorisation
     */
    public String getTypeAutorisation() {
        return typeAutorisation;
    }

    /**
     * Mutateur sur l'attribut {@link #typeAutorisation}.
     *
     * @param typeAutorisation
     *            la nouvelle valeur de l'attribut typeAutorisation
     */
    public void setTypeAutorisation(String typeAutorisation) {
        this.typeAutorisation = typeAutorisation;
    }

    /**
     * Accesseur sur l'attribut {@link #indicePieceJointe}.
     *
     * @return String indicePieceJointe
     */
    public String getIndicePieceJointe() {
        return indicePieceJointe;
    }

    /**
     * Mutateur sur l'attribut {@link #indicePieceJointe}.
     *
     * @param indicePieceJointe
     *            la nouvelle valeur de l'attribut indicePieceJointe
     */
    public void setIndicePieceJointe(String indicePieceJointe) {
        this.indicePieceJointe = indicePieceJointe;
    }

    /**
     * Accesseur sur l'attribut {@link #typePieceJointe}.
     *
     * @return String typePieceJointe
     */
    public String getTypePieceJointe() {
        return typePieceJointe;
    }

    /**
     * Mutateur sur l'attribut {@link #typePieceJointe}.
     *
     * @param typePieceJointe
     *            la nouvelle valeur de l'attribut typePieceJointe
     */
    public void setTypePieceJointe(String typePieceJointe) {
        this.typePieceJointe = typePieceJointe;
    }

    /**
     * Accesseur sur l'attribut {@link #fichierPieceJointe}.
     *
     * @return String fichierPieceJointe
     */
    public String getFichierPieceJointe() {
        return fichierPieceJointe;
    }

    /**
     * Mutateur sur l'attribut {@link #fichierPieceJointe}.
     *
     * @param fichierPieceJointe
     *            la nouvelle valeur de l'attribut fichierPieceJointe
     */
    public void setFichierPieceJointe(String fichierPieceJointe) {
        this.fichierPieceJointe = fichierPieceJointe;
    }

    /**
     * Accesseur sur l'attribut {@link #numeroDeLiasse}.
     *
     * @return String numeroDeLiasse
     */
    public String getNumeroDeLiasse() {
        return numeroDeLiasse;
    }

    /**
     * Mutateur sur l'attribut {@link #numeroDeLiasse}.
     *
     * @param numeroDeLiasse
     *            la nouvelle valeur de l'attribut numeroDeLiasse
     */
    public void setNumeroDeLiasse(String numeroDeLiasse) {
        this.numeroDeLiasse = numeroDeLiasse;
    }

    /**
     * Accesseur sur l'attribut {@link #indiceLiasseXml}.
     *
     * @return String indiceLiasseXml
     */
    public String getIndiceLiasseXml() {
        return indiceLiasseXml;
    }

    /**
     * Mutateur sur l'attribut {@link #indiceLiasseXml}.
     *
     * @param indiceLiasseXml
     *            la nouvelle valeur de l'attribut indiceLiasseXml
     */
    public void setIndiceLiasseXml(String indiceLiasseXml) {
        this.indiceLiasseXml = indiceLiasseXml;
    }

    /**
     * Accesseur sur l'attribut {@link #dateHeureGenerationXml}.
     *
     * @return String dateHeureGenerationXml
     */
    public String getDateHeureGenerationXml() {
        return dateHeureGenerationXml;
    }

    /**
     * Mutateur sur l'attribut {@link #dateHeureGenerationXml}.
     *
     * @param dateHeureGenerationXml
     *            la nouvelle valeur de l'attribut dateHeureGenerationXml
     */
    public void setDateHeureGenerationXml(String dateHeureGenerationXml) {
        this.dateHeureGenerationXml = dateHeureGenerationXml;
    }

    /**
     * Accesseur sur l'attribut {@link #commentaire}.
     *
     * @return String commentaire
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     * Mutateur sur l'attribut {@link #commentaire}.
     *
     * @param commentaire
     *            la nouvelle valeur de l'attribut commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    /**
     * Accesseur sur l'attribut {@link #dossierUnique}.
     *
     * @return DossierUnique dossierUnique
     */
    public DossierUnique getDossierUnique() {
        return dossierUnique;
    }

    /**
     * Mutateur sur l'attribut {@link #dossierUnique}.
     *
     * @param dossierUnique
     *            la nouvelle valeur de l'attribut dossierUnique
     */
    public void setDossierUnique(DossierUnique dossierUnique) {
        this.dossierUnique = dossierUnique;
    }

    /**
     * Accesseur sur l'attribut {@link #correspondant}.
     *
     * @return Correspondant correspondant
     */
    public Correspondant getCorrespondant() {
        return correspondant;
    }

    /**
     * Mutateur sur l'attribut {@link #correspondant}.
     *
     * @param correspondant
     *            la nouvelle valeur de l'attribut correspondant
     */
    public void setCorrespondant(Correspondant correspondant) {
        this.correspondant = correspondant;
    }

}
