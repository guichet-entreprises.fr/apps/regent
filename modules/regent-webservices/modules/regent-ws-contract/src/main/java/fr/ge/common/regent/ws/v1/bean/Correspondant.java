/**
 * 
 */
package fr.ge.common.regent.ws.v1.bean;

/**
 * Information de correspondance du déclarant.
 * 
 * @author $Author: lhoffste $
 * @version $Revision: 0 $
 */
public class Correspondant {

    /** Identité du déclarant. */
    private IdentiteCorrespondant identiteCorrespondant;
    /** Adresse de correspondance du déclarant. */
    private AdresseCorrespondant AdresseCorrespondant;

    /**
     * Constructeur de la classe.
     *
     * @param identiteCorrespondant
     *            {@link #identiteCorrespondant}
     * @param adresseCorrespondant
     *            {@link #AdresseCorrespondant}
     */
    public Correspondant(IdentiteCorrespondant identiteCorrespondant, fr.ge.common.regent.ws.v1.bean.AdresseCorrespondant adresseCorrespondant) {
        super();
        this.identiteCorrespondant = identiteCorrespondant;
        AdresseCorrespondant = adresseCorrespondant;
    }

    /**
     * Constructeur de la classe.
     *
     */
    public Correspondant() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * Accesseur sur l'attribut {@link #identiteCorrespondant}.
     *
     * @return IdentiteCorrespondant identiteCorrespondant
     */
    public IdentiteCorrespondant getIdentiteCorrespondant() {
        return identiteCorrespondant;
    }

    /**
     * Mutateur sur l'attribut {@link #identiteCorrespondant}.
     *
     * @param identiteCorrespondant
     *            la nouvelle valeur de l'attribut identiteCorrespondant
     */
    public void setIdentiteCorrespondant(IdentiteCorrespondant identiteCorrespondant) {
        this.identiteCorrespondant = identiteCorrespondant;
    }

    /**
     * Accesseur sur l'attribut {@link #adresseCorrespondant}.
     *
     * @return AdresseCorrespondant adresseCorrespondant
     */
    public AdresseCorrespondant getAdresseCorrespondant() {
        return AdresseCorrespondant;
    }

    /**
     * Mutateur sur l'attribut {@link #adresseCorrespondant}.
     *
     * @param adresseCorrespondant
     *            la nouvelle valeur de l'attribut adresseCorrespondant
     */
    public void setAdresseCorrespondant(AdresseCorrespondant adresseCorrespondant) {
        AdresseCorrespondant = adresseCorrespondant;
    }

}
