/**
 * 
 */
package fr.ge.common.regent.ws.v1.bean;

/**
 * Informations du destinataire.
 * 
 * @author $Author: lhoffste $
 * @version $Revision: 0 $
 */
public class CodeDestinataire {

    /** Code du partenaire. */
    private String codePartenaire;
    /** Code Edi du partenaire. */
    private String codeEdi;
    /** Libellé du partenaire. */
    private String libelleIntervenant;

    /**
     * Constructeur de la classe.
     *
     * @param codePartenaire
     *            {@link #codePartenaire}
     * @param codeEdi
     *            {@link #codeEdi}
     * @param libelleIntervenant
     *            {@link #libelleIntervenant}
     */
    public CodeDestinataire(String codePartenaire, String codeEdi, String libelleIntervenant) {
        super();
        this.codePartenaire = codePartenaire;
        this.codeEdi = codeEdi;
        this.libelleIntervenant = libelleIntervenant;
    }

    /**
     * Constructeur de la classe.
     *
     */
    public CodeDestinataire() {
        super();
    }

    /**
     * Accesseur sur l'attribut {@link #codePartenaire}.
     *
     * @return String codePartenaire
     */
    public String getCodePartenaire() {
        return codePartenaire;
    }

    /**
     * Mutateur sur l'attribut {@link #codePartenaire}.
     *
     * @param codePartenaire
     *            la nouvelle valeur de l'attribut codePartenaire
     */
    public void setCodePartenaire(String codePartenaire) {
        this.codePartenaire = codePartenaire;
    }

    /**
     * Accesseur sur l'attribut {@link #codeEdi}.
     *
     * @return String codeEdi
     */
    public String getCodeEdi() {
        return codeEdi;
    }

    /**
     * Mutateur sur l'attribut {@link #codeEdi}.
     *
     * @param codeEdi
     *            la nouvelle valeur de l'attribut codeEdi
     */
    public void setCodeEdi(String codeEdi) {
        this.codeEdi = codeEdi;
    }

    /**
     * Accesseur sur l'attribut {@link #libelleIntervenant}.
     *
     * @return String libelleIntervenant
     */
    public String getLibelleIntervenant() {
        return libelleIntervenant;
    }

    /**
     * Mutateur sur l'attribut {@link #libelleIntervenant}.
     *
     * @param libelleIntervenant
     *            la nouvelle valeur de l'attribut libelleIntervenant
     */
    public void setLibelleIntervenant(String libelleIntervenant) {
        this.libelleIntervenant = libelleIntervenant;
    }

}
