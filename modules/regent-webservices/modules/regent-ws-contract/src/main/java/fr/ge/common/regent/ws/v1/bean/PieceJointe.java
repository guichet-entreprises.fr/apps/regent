/**
 * 
 */
package fr.ge.common.regent.ws.v1.bean;

/**
 * Bloc pièce jointe, contient les informations sur une pièce jointe.
 * 
 * @author $Author: lhoffste $
 * @version $Revision: 0 $
 */
public class PieceJointe {

    /** Indice de la pièce jointe. */
    private String indicePieceJointe;
    /** Type de la pièce jointe. */
    private String typePieceJointe;
    /** Format de la pièce jointe. */
    private String formatPieceJointe;
    /** Nom du fichier de la pièce jointe. */
    private String fichierPieceJointe;

    /**
     * Constructeur de la classe.
     * 
     * @param indicePieceJointe
     *            {@link #indicePieceJointe}.
     * @param typePieceJointe
     *            {@link #typePieceJointe}.
     * @param formatPieceJointe
     *            {@link #formatPieceJointe}.
     * @param fichierPieceJointe
     *            {@link #fichierPieceJointe}.
     */
    public PieceJointe(String indicePieceJointe, String typePieceJointe, String formatPieceJointe, String fichierPieceJointe) {
        super();
        this.indicePieceJointe = indicePieceJointe;
        this.typePieceJointe = typePieceJointe;
        this.formatPieceJointe = formatPieceJointe;
        this.fichierPieceJointe = fichierPieceJointe;
    }

    /**
     * Constructeur de la classe.
     *
     */
    public PieceJointe() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @return the indicePieceJointe
     */
    public String getIndicePieceJointe() {
        return indicePieceJointe;
    }

    /**
     * @param indicePieceJointe
     *            the indicePieceJointe to set
     */
    public void setIndicePieceJointe(String indicePieceJointe) {
        this.indicePieceJointe = indicePieceJointe;
    }

    /**
     * @return the typePieceJointe
     */
    public String getTypePieceJointe() {
        return typePieceJointe;
    }

    /**
     * @param typePieceJointe
     *            the typePieceJointe to set
     */
    public void setTypePieceJointe(String typePieceJointe) {
        this.typePieceJointe = typePieceJointe;
    }

    /**
     * @return the fichierPieceJointe
     */
    public String getFichierPieceJointe() {
        return fichierPieceJointe;
    }

    /**
     * @param fichierPieceJointe
     *            the fichierPieceJointe to set
     */
    public void setFichierPieceJointe(String fichierPieceJointe) {
        this.fichierPieceJointe = fichierPieceJointe;
    }

    /**
     * Accesseur sur l'attribut {@link #formatPieceJointe}.
     *
     * @return String formatPieceJointe
     */
    public String getFormatPieceJointe() {
        return formatPieceJointe;
    }

    /**
     * Mutateur sur l'attribut {@link #formatPieceJointe}.
     *
     * @param formatPieceJointe
     *            la nouvelle valeur de l'attribut formatPieceJointe
     */
    public void setFormatPieceJointe(String formatPieceJointe) {
        this.formatPieceJointe = formatPieceJointe;
    }

}
