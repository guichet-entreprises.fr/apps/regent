/**
 * 
 */
package fr.ge.common.regent.ws.v1.bean;

/**
 * Bloc informationPartenaireDestinataireAutorisation.
 * 
 * @author $Author: lhoffste $
 * @version $Revision: 0 $
 */
public class InformationDestinataireAutorisation {

    private String codePartenaireDestinataireAutorisation;
    private String typeActivite;

    /**
     * Constructeur de la classe.
     *
     * @param codePartenaireDestinataireAutorisation
     * @param typeActivite
     */
    public InformationDestinataireAutorisation(String codePartenaireDestinataireAutorisation, String typeActivite) {
        super();
        this.codePartenaireDestinataireAutorisation = codePartenaireDestinataireAutorisation;
        this.typeActivite = typeActivite;
    }

    /**
     * Constructeur de la classe.
     *
     */
    public InformationDestinataireAutorisation() {
        super();
    }

    /**
     * Accesseur sur l'attribut {@link #codePartenaireDestinataireAutorisation}.
     *
     * @return String codePartenaireDestinataireAutorisation
     */
    public String getCodePartenaireDestinataireAutorisation() {
        return codePartenaireDestinataireAutorisation;
    }

    /**
     * Mutateur sur l'attribut {@link #codePartenaireDestinataireAutorisation}.
     *
     * @param codePartenaireDestinataireAutorisation
     *            la nouvelle valeur de l'attribut
     *            codePartenaireDestinataireAutorisation
     */
    public void setCodePartenaireDestinataireAutorisation(String codePartenaireDestinataireAutorisation) {
        this.codePartenaireDestinataireAutorisation = codePartenaireDestinataireAutorisation;
    }

    /**
     * Accesseur sur l'attribut {@link #typeActivite}.
     *
     * @return String typeActivite
     */
    public String getTypeActivite() {
        return typeActivite;
    }

    /**
     * Mutateur sur l'attribut {@link #typeActivite}.
     *
     * @param typeActivite
     *            la nouvelle valeur de l'attribut typeActivite
     */
    public void setTypeActivite(String typeActivite) {
        this.typeActivite = typeActivite;
    }

}
