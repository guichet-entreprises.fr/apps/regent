/**
 * 
 */
package fr.ge.common.regent.ws.v1.bean;

/**
 * Informations d'un destinataires.
 * 
 * @author $Author: lhoffste $
 * @version $Revision: 0 $
 */
public class DestinataireDossierUnique {

    /** Rôle du destinataire. */
    private String roleDestinataire;
    /** Codes identifiant le destinataire. */
    private CodeDestinataire codeDestinataire;

    /**
     * Constructeur de la classe.
     *
     * @param roleDestinataire
     *            {@link #roleDestinataire}
     * @param codeDestinataire
     *            {@link #codeDestinataire}
     */
    public DestinataireDossierUnique(String roleDestinataire, CodeDestinataire codeDestinataire) {
        super();
        this.roleDestinataire = roleDestinataire;
        this.codeDestinataire = codeDestinataire;
    }

    /**
     * Constructeur de la classe.
     *
     */
    public DestinataireDossierUnique() {
        super();
    }

    /**
     * Accesseur sur l'attribut {@link #roleDestinataire}.
     *
     * @return String roleDestinataire
     */
    public String getRoleDestinataire() {
        return roleDestinataire;
    }

    /**
     * Mutateur sur l'attribut {@link #roleDestinataire}.
     *
     * @param roleDestinataire
     *            la nouvelle valeur de l'attribut roleDestinataire
     */
    public void setRoleDestinataire(String roleDestinataire) {
        this.roleDestinataire = roleDestinataire;
    }

    /**
     * Accesseur sur l'attribut {@link #codeDestinataire}.
     *
     * @return CodeDestinataire codeDestinataire
     */
    public CodeDestinataire getCodeDestinataire() {
        return codeDestinataire;
    }

    /**
     * Mutateur sur l'attribut {@link #codeDestinataire}.
     *
     * @param codeDestinataire
     *            la nouvelle valeur de l'attribut codeDestinataire
     */
    public void setCodeDestinataire(CodeDestinataire codeDestinataire) {
        this.codeDestinataire = codeDestinataire;
    }

}
