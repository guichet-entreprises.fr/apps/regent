/**
 * 
 */
package fr.ge.common.regent.ws.v1.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Bloc dossierAutorisation.
 * 
 * @author $Author: lhoffste $
 * @version $Revision: 0 $
 */
public class DossierAutorisation {

    private String numeroDossierAutorisation;
    private String dateHeureDepot;
    private List<SousDossierAutorisation> sousDossierAutorisation = new ArrayList<SousDossierAutorisation>();

    /**
     * Constructeur de la classe.
     *
     * @param numeroDossierAutorisation
     * @param dateHeureDepot
     * @param sousDossierAutorisation
     */
    public DossierAutorisation(String numeroDossierAutorisation, String dateHeureDepot, List<SousDossierAutorisation> sousDossierAutorisation) {
        super();
        this.numeroDossierAutorisation = numeroDossierAutorisation;
        this.dateHeureDepot = dateHeureDepot;
        this.sousDossierAutorisation = sousDossierAutorisation;
    }

    /**
     * Constructeur de la classe.
     *
     */
    public DossierAutorisation() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * Accesseur sur l'attribut {@link #numeroDossierAutorisation}.
     *
     * @return String numeroDossierAutorisation
     */
    public String getNumeroDossierAutorisation() {
        return numeroDossierAutorisation;
    }

    /**
     * Mutateur sur l'attribut {@link #numeroDossierAutorisation}.
     *
     * @param numeroDossierAutorisation
     *            la nouvelle valeur de l'attribut numeroDossierAutorisation
     */
    public void setNumeroDossierAutorisation(String numeroDossierAutorisation) {
        this.numeroDossierAutorisation = numeroDossierAutorisation;
    }

    /**
     * Accesseur sur l'attribut {@link #dateHeureDepot}.
     *
     * @return String dateHeureDepot
     */
    public String getDateHeureDepot() {
        return dateHeureDepot;
    }

    /**
     * Mutateur sur l'attribut {@link #dateHeureDepot}.
     *
     * @param dateHeureDepot
     *            la nouvelle valeur de l'attribut dateHeureDepot
     */
    public void setDateHeureDepot(String dateHeureDepot) {
        this.dateHeureDepot = dateHeureDepot;
    }

    /**
     * Accesseur sur l'attribut {@link #sousDossierAutorisation}.
     *
     * @return List<SousDossierAutorisation> sousDossierAutorisation
     */
    public List<SousDossierAutorisation> getSousDossierAutorisation() {
        return sousDossierAutorisation;
    }

    /**
     * Mutateur sur l'attribut {@link #sousDossierAutorisation}.
     *
     * @param sousDossierAutorisation
     *            la nouvelle valeur de l'attribut sousDossierAutorisation
     */
    public void setSousDossierAutorisation(List<SousDossierAutorisation> sousDossierAutorisation) {
        this.sousDossierAutorisation = sousDossierAutorisation;
    }

}
