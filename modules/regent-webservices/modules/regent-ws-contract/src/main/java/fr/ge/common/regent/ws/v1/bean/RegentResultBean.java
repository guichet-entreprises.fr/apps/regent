/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.regent.ws.v1.bean;

import java.io.UnsupportedEncodingException;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RegentResultBean {

    /** Error validation. **/
    private String error;

    /** XML content. **/
    private byte[] content;

    /**
     * 
     * Constructeur de la classe.
     *
     */
    public RegentResultBean() {

    }

    /**
     * Accesseur sur l'attribut {@link #error}.
     *
     * @return String error
     */
    public String getError() {
        return error;
    }

    /**
     * Mutateur sur l'attribut {@link #error}.
     *
     * @param error
     *            la nouvelle valeur de l'attribut error
     */
    public void setError(String error) {
        this.error = error;
    }

    /**
     * Accesseur sur l'attribut {@link #content}.
     *
     * @return byte[] content
     */
    public byte[] getContent() {
        return content;
    }

    /**
     * Mutateur sur l'attribut {@link #content}.
     *
     * @param content
     *            la nouvelle valeur de l'attribut content
     */
    public void setContent(byte[] content) {
        this.content = content;
    }

    /**
     * Returns XML Regent as plain-text
     * 
     * @return the XML Regent as plain-text
     */
    @JsonProperty(value = "asXml")
    public String asXml() {
        try {
            return new String(this.getContent(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }
}
