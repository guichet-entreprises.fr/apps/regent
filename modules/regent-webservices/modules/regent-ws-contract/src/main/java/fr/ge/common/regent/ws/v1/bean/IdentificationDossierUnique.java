/**
 * 
 */
package fr.ge.common.regent.ws.v1.bean;

/**
 * Bloc identificationDossierUnique du XML-TC, contient les informations du
 * dossier unique (dossier utilisateur).
 * 
 * @author $Author: lhoffste $
 * @version $Revision: 0 $
 */
public class IdentificationDossierUnique {

    /** Bloc identifiantDossierUnique {@link IdentifiantDossierUnique}. */
    private IdentifiantDossierUnique identifiantDossierUnique;

    /** Type du dossier unique. */
    private String typeDossierUnique;

    /** Nom du dossier. */
    private String nomDossier;

    /**
     * Bloc correspondant, contient les informations du déclarant
     * {@link Correspondant}.
     */
    private Correspondant correspondant;

    /**
     * Constructeur.
     * 
     * @param identifiantDossierUnique
     *            {@link #identifiantDossierUnique}.
     * @param typeDossierUnique
     *            {@link #typeDossierUnique}.
     * @param nomDossier
     *            {@link #nomDossier}.
     * @param correspondant
     *            {@link #correspondant}.
     * @param numeroDossierUnique
     *            {@link #numeroDossierUnique}.
     */
    public IdentificationDossierUnique(IdentifiantDossierUnique identifiantDossierUnique, String typeDossierUnique, String nomDossier, Correspondant correspondant, String numeroDossierUnique) {
        super();
        this.identifiantDossierUnique = identifiantDossierUnique;
        this.typeDossierUnique = typeDossierUnique;
        this.nomDossier = nomDossier;
        this.correspondant = correspondant;
        this.numeroDossierUnique = numeroDossierUnique;
    }

    /**
     * @return the typeDossierUnique
     */
    public String getTypeDossierUnique() {
        return typeDossierUnique;
    }

    /**
     * @param typeDossierUnique
     *            the typeDossierUnique to set
     */
    public void setTypeDossierUnique(String typeDossierUnique) {
        this.typeDossierUnique = typeDossierUnique;
    }

    /**
     * @return the nomDossier
     */
    public String getNomDossier() {
        return nomDossier;
    }

    /**
     * @param nomDossier
     *            the nomDossier to set
     */
    public void setNomDossier(String nomDossier) {
        this.nomDossier = nomDossier;
    }

    /**
     * @return the correspondant
     */
    public Correspondant getCorrespondant() {
        return correspondant;
    }

    /**
     * @param correspondant
     *            the correspondant to set
     */
    public void setCorrespondant(Correspondant correspondant) {
        this.correspondant = correspondant;
    }

    /**
     * @return the identifiantDossierUnique
     */
    public IdentifiantDossierUnique getIdentifiantDossierUnique() {
        return identifiantDossierUnique;
    }

    /**
     * @param identifiantDossierUnique
     *            the identifiantDossierUnique to set
     */
    public void setIdentifiantDossierUnique(IdentifiantDossierUnique identifiantDossierUnique) {
        this.identifiantDossierUnique = identifiantDossierUnique;
    }

    String numeroDossierUnique;

    /**
     * Constructeur de la classe.
     *
     * @param numeroDossierUnique
     */
    public IdentificationDossierUnique(String numeroDossierUnique) {
        super();
        this.numeroDossierUnique = numeroDossierUnique;
    }

    /**
     * Constructeur de la classe.
     *
     */
    public IdentificationDossierUnique() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * Accesseur sur l'attribut {@link #numeroDossierUnique}.
     *
     * @return String numeroDossierUnique
     */
    public String getNumeroDossierUnique() {
        return numeroDossierUnique;
    }

    /**
     * Mutateur sur l'attribut {@link #numeroDossierUnique}.
     *
     * @param numeroDossierUnique
     *            la nouvelle valeur de l'attribut numeroDossierUnique
     */
    public void setNumeroDossierUnique(String numeroDossierUnique) {
        this.numeroDossierUnique = numeroDossierUnique;
    }

}
