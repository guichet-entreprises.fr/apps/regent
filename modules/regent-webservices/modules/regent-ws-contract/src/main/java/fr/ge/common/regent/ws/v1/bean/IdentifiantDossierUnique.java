/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 */

package fr.ge.common.regent.ws.v1.bean;

/**
 * Bloc identifiantDossierUnique du XML-TC; contient les informatiosn
 * identifiant le dossier utilisateur.
 * 
 * @author mtakerra
 *
 */
public class IdentifiantDossierUnique {

    /** Code de l'emetteur du dossier. */
    private String codePartenaireEmetteur;
    /** numéro du dossier unique. */
    private String numeroDossierUnique;

    /**
     * Constructeur.
     */
    public IdentifiantDossierUnique() {
        super();
    }

    /**
     * Constructeur.
     * 
     * @param codePartenaireEmetteur
     *            {@link #codePartenaireEmetteur}.
     * @param numeroDossierUnique
     *            {@link #numeroDossierUnique}.
     */
    public IdentifiantDossierUnique(String codePartenaireEmetteur, String numeroDossierUnique) {
        super();
        this.codePartenaireEmetteur = codePartenaireEmetteur;
        this.numeroDossierUnique = numeroDossierUnique;
    }

    /**
     * @return the codePartenaireEmetteur
     */
    public String getCodePartenaireEmetteur() {
        return codePartenaireEmetteur;
    }

    /**
     * @param codePartenaireEmetteur
     *            the codePartenaireEmetteur to set
     */
    public void setCodePartenaireEmetteur(String codePartenaireEmetteur) {
        this.codePartenaireEmetteur = codePartenaireEmetteur;
    }

    /**
     * @return the numeroDossierUnique
     */
    public String getNumeroDossierUnique() {
        return numeroDossierUnique;
    }

    /**
     * @param numeroDossierUnique
     *            the numeroDossierUnique to set
     */
    public void setNumeroDossierUnique(String numeroDossierUnique) {
        this.numeroDossierUnique = numeroDossierUnique;
    }
}
