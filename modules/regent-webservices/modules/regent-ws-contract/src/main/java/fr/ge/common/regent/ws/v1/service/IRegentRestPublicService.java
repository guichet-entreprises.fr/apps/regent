/**
 *
 */
package fr.ge.common.regent.ws.v1.service;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

/**
 * Regent Rest service contract, public methods.
 *
 * @author aolubi
 *
 */
@Path("/v1")
public interface IRegentRestPublicService {

    /**
     * WS that allows to get a list of the XML for a Regent version & event Tags
     * in a js file.
     *
     * @param the
     *            regent version, must not be null
     *
     * @param A
     *            list of the evenments that we want to get the list of
     *            tags,must not be null
     *
     * @return message URI
     */
    @GET
    @Path("/xml-regent/mapping")
    Response generateMappingJs(@QueryParam("regentVersion") String regentVersion, @QueryParam("listTypeEvenement") List<String> listTypeEvenement);

}
