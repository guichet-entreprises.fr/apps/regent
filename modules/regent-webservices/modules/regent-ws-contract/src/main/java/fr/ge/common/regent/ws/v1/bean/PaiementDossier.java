/**
 * 
 */
package fr.ge.common.regent.ws.v1.bean;

/**
 * Bloc paiementDossier , contient les informations du paiement effectué pour un
 * destinataire {@link DestinataireDossierCfe}.
 * 
 * @author $Author: lhoffste $
 * @version $Revision: 0 $
 */
public class PaiementDossier {

    /** Identité du payeur {@link IdentitePayeur}. */
    private IdentitePayeur identitePayeur;
    /** Adresse du payeur {@link AdressePayeur}. */
    private AdressePayeur adressePayeur;
    /** Montant payé pour ce partenaire. */
    private String montantFormalite;
    /** Référence de la transaction. */
    private String referencePaiement;
    /** Date et heure du paiement */
    private String dateHeurePaiement;

    /**
     * Constructeur de la classe.
     * 
     * @param identitePayeur
     *            {@link #identitePayeur}.
     * @param adressePayeur
     *            {@link #adressePayeur}.
     * @param montantFormalite
     *            {@link #montantFormalite}.
     * @param referencePaiement
     *            {@link #referencePaiement}.
     * @param dateHeurePaiement
     *            {@link #dateHeurePaiement}.
     */
    public PaiementDossier(IdentitePayeur identitePayeur, AdressePayeur adressePayeur, String montantFormalite, String referencePaiement, String dateHeurePaiement) {
        super();
        this.identitePayeur = identitePayeur;
        this.adressePayeur = adressePayeur;
        this.montantFormalite = montantFormalite;
        this.referencePaiement = referencePaiement;
        this.dateHeurePaiement = dateHeurePaiement;
    }

    /**
     * Constructeur de la classe.
     *
     */
    public PaiementDossier() {
        super();
    }

    /**
     * @return the adressePayeur
     */
    public AdressePayeur getAdressePayeur() {
        return adressePayeur;
    }

    /**
     * @param adressePayeur
     *            the adressePayeur to set
     */
    public void setAdressePayeur(AdressePayeur adressePayeur) {
        this.adressePayeur = adressePayeur;
    }

    /**
     * Accesseur sur l'attribut {@link #identitePayeur}.
     *
     * @return IdentitePayeur identitePayeur
     */
    public IdentitePayeur getIdentitePayeur() {
        return identitePayeur;
    }

    /**
     * Mutateur sur l'attribut {@link #identitePayeur}.
     *
     * @param identitePayeur
     *            la nouvelle valeur de l'attribut identitePayeur
     */
    public void setIdentitePayeur(IdentitePayeur identitePayeur) {
        this.identitePayeur = identitePayeur;
    }

    /**
     * Accesseur sur l'attribut {@link #montantFormalite}.
     *
     * @return String montantFormalite
     */
    public String getMontantFormalite() {
        return montantFormalite;
    }

    /**
     * Mutateur sur l'attribut {@link #montantFormalite}.
     *
     * @param montantFormalite
     *            la nouvelle valeur de l'attribut montantFormalite
     */
    public void setMontantFormalite(String montantFormalite) {
        this.montantFormalite = montantFormalite;
    }

    /**
     * Accesseur sur l'attribut {@link #referencePaiement}.
     *
     * @return String referencePaiement
     */
    public String getReferencePaiement() {
        return referencePaiement;
    }

    /**
     * Mutateur sur l'attribut {@link #referencePaiement}.
     *
     * @param referencePaiement
     *            la nouvelle valeur de l'attribut referencePaiement
     */
    public void setReferencePaiement(String referencePaiement) {
        this.referencePaiement = referencePaiement;
    }

    /**
     * Accesseur sur l'attribut {@link #dateHeurePaiement}.
     *
     * @return String dateHeurePaiement
     */
    public String getDateHeurePaiement() {
        return dateHeurePaiement;
    }

    /**
     * Mutateur sur l'attribut {@link #dateHeurePaiement}.
     *
     * @param dateHeurePaiement
     *            la nouvelle valeur de l'attribut dateHeurePaiement
     */
    public void setDateHeurePaiement(String dateHeurePaiement) {
        this.dateHeurePaiement = dateHeurePaiement;
    }

}
