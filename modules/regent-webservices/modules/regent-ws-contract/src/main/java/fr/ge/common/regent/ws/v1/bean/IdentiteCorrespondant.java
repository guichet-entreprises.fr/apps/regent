/**
 * 
 */
package fr.ge.common.regent.ws.v1.bean;

/**
 * Bloc identiteCorrespondant; contient l'identité du déclarant.
 * 
 * @author $Author: lhoffste $
 * @version $Revision: 0 $
 */
public class IdentiteCorrespondant {

    /** nom du déclarant. */
    private String nomCorrespondant;

    /**
     * Constructeur de la classe.
     *
     * @param nomCorrespondant
     *            {@link #nomCorrespondant}.
     */
    public IdentiteCorrespondant(String nomCorrespondant) {
        super();
        this.nomCorrespondant = nomCorrespondant;
    }

    /**
     * Constructeur de la classe.
     *
     */
    public IdentiteCorrespondant() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * Accesseur sur l'attribut {@link #nomCorrespondant}.
     *
     * @return String nomCorrespondant
     */
    public String getNomCorrespondant() {
        return nomCorrespondant;
    }

    /**
     * Mutateur sur l'attribut {@link #nomCorrespondant}.
     *
     * @param nomCorrespondant
     *            la nouvelle valeur de l'attribut nomCorrespondant
     */
    public void setNomCorrespondant(String nomCorrespondant) {
        this.nomCorrespondant = nomCorrespondant;
    }

}
