/**
 * 
 */
package fr.ge.common.regent.ws.v1.bean;

/**
 * Bloc identitePayeur; contient l'identité du payeur.
 * 
 * @author $Author: lhoffste $
 * @version $Revision: 0 $
 */
public class IdentitePayeur {

    /** nom du payeur. */
    private String nomPayeur;

    /**
     * Constructeur de la classe.
     *
     * @param nomPayeur
     *            {@link #nomPayeur}
     */
    public IdentitePayeur(String nomPayeur) {
        super();
        this.nomPayeur = nomPayeur;
    }

    /**
     * Constructeur de la classe.
     *
     */
    public IdentitePayeur() {
        super();
    }

    /**
     * Accesseur sur l'attribut {@link #nomPayeur}.
     *
     * @return String nomPayeur
     */
    public String getNomPayeur() {
        return nomPayeur;
    }

    /**
     * Mutateur sur l'attribut {@link #nomPayeur}.
     *
     * @param nomPayeur
     *            la nouvelle valeur de l'attribut nomPayeur
     */
    public void setNomPayeur(String nomPayeur) {
        this.nomPayeur = nomPayeur;
    }

}
