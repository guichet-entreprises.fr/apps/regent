/**
 * 
 */
package fr.ge.common.regent.ws.v1.bean;

import java.util.List;

/**
 * Bloc dossierCfe; contient les informations des dossiers partenaires à
 * transmettre aux CFEs.
 * 
 * @author $Author: lhoffste $
 * @version $Revision: 0 $
 */
public class DossierCfe {

    /** numéro de dossier. */
    private String numeroDeLiasse;
    /** date et heure de dépot du dossier. */
    private String dateHeureDepot;
    /** numéro de dossier. */
    private String referenceLiasseFo;
    /** liste des dossiers partenaires {@link DestinataireDossierCfe} */
    private List<DestinataireDossierCfe> destinataireDossierCfe;

    /**
     * Constructeur de la class.
     * 
     * @param numeroDeLiasse
     *            {@link #numeroDeLiasse}
     * @param dateHeureDepot
     *            {@link #dateHeureDepot}
     * @param referenceLiasseFo
     *            {@link #referenceLiasseFo}
     * @param destinataireDossierCfe
     *            {@link #destinataireDossierCfe}
     */
    public DossierCfe(String numeroDeLiasse, String dateHeureDepot, String referenceLiasseFo, List<DestinataireDossierCfe> destinataireDossierCfe) {
        super();
        this.numeroDeLiasse = numeroDeLiasse;
        this.dateHeureDepot = dateHeureDepot;
        this.referenceLiasseFo = referenceLiasseFo;
        this.destinataireDossierCfe = destinataireDossierCfe;
    }

    /**
     * Constructeur de la classe.
     *
     */
    public DossierCfe() {
        super();
    }

    /**
     * @return the numeroDeLiasse
     */
    public String getNumeroDeLiasse() {
        return numeroDeLiasse;
    }

    /**
     * @param numeroDeLiasse
     *            the numeroDeLiasse to set
     */
    public void setNumeroDeLiasse(String numeroDeLiasse) {
        this.numeroDeLiasse = numeroDeLiasse;
    }

    /**
     * Accesseur sur l'attribut {@link #dateHeureDepot}.
     *
     * @return String dateHeureDepot
     */
    public String getDateHeureDepot() {
        return dateHeureDepot;
    }

    /**
     * Mutateur sur l'attribut {@link #dateHeureDepot}.
     *
     * @param dateHeureDepot
     *            la nouvelle valeur de l'attribut dateHeureDepot
     */
    public void setDateHeureDepot(String dateHeureDepot) {
        this.dateHeureDepot = dateHeureDepot;
    }

    /**
     * Accesseur sur l'attribut {@link #referenceLiasseFo}.
     *
     * @return String referenceLiasseFo
     */
    public String getReferenceLiasseFo() {
        return referenceLiasseFo;
    }

    /**
     * Mutateur sur l'attribut {@link #referenceLiasseFo}.
     *
     * @param referenceLiasseFo
     *            la nouvelle valeur de l'attribut referenceLiasseFo
     */
    public void setReferenceLiasseFo(String referenceLiasseFo) {
        this.referenceLiasseFo = referenceLiasseFo;
    }

    /**
     * Accesseur sur l'attribut {@link #destinataireDossierCfe}.
     *
     * @return List<DestinataireDossierCfe> destinataireDossierCfe
     */
    public List<DestinataireDossierCfe> getDestinataireDossierCfe() {
        return destinataireDossierCfe;
    }

    /**
     * Mutateur sur l'attribut {@link #destinataireDossierCfe}.
     *
     * @param destinataireDossierCfe
     *            la nouvelle valeur de l'attribut destinataireDossierCfe
     */
    public void setDestinataireDossierCfe(List<DestinataireDossierCfe> destinataireDossierCfe) {
        this.destinataireDossierCfe = destinataireDossierCfe;
    }

}
