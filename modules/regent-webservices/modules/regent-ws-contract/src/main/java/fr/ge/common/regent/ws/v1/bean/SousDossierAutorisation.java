/**
 * 
 */
package fr.ge.common.regent.ws.v1.bean;

/**
 * @author $Author: lhoffste $
 * @version $Revision: 0 $
 */
public class SousDossierAutorisation {

    private InformationDestinataireAutorisation informationDestinataireAutorisation;

    /**
     * Constructeur de la classe.
     *
     * @param informationDestinataireAutorisation
     */
    public SousDossierAutorisation(InformationDestinataireAutorisation informationDestinataireAutorisation) {
        super();
        this.informationDestinataireAutorisation = informationDestinataireAutorisation;
    }

    /**
     * Constructeur de la classe.
     *
     */
    public SousDossierAutorisation() {
        super();
    }

    /**
     * Accesseur sur l'attribut {@link #informationDestinataireAutorisation}.
     *
     * @return InformationDestinataireAutorisation
     *         informationDestinataireAutorisation
     */
    public InformationDestinataireAutorisation getInformationDestinataireAutorisation() {
        return informationDestinataireAutorisation;
    }

    /**
     * Mutateur sur l'attribut {@link #informationDestinataireAutorisation}.
     *
     * @param informationDestinataireAutorisation
     *            la nouvelle valeur de l'attribut
     *            informationDestinataireAutorisation
     */
    public void setInformationDestinataireAutorisation(InformationDestinataireAutorisation informationDestinataireAutorisation) {
        this.informationDestinataireAutorisation = informationDestinataireAutorisation;
    }

}
