/**
 *
 */
package fr.ge.common.regent.ws.v1.service;

import java.util.LinkedHashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Regent Rest service contract, private methods.
 *
 * @author aolubi
 *
 */
public interface IRegentRestPrivateService {

    /**
     * Create and validate an XML regent from a mapped file.
     * 
     * @param regentVersion
     *            version of the regent to generate
     * @param listTypeEvenement
     *            list of events concerned by the formality
     * @param authorityRole
     *            role of the recipient authority
     * @param authorityCode
     *            code of the recipient authority
     * @param mappedFileToRegent
     *            a Map<tag, value> of the trags to generate in the xml-regent
     * @return generated anda validated XML-REGENT
     */
    @POST
    @Path("/xml-regent/generate/{regentVersion}/{authorityRole}/{authorityCode}")
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    Response generateXmlRegentFromJs(@PathParam("regentVersion") String regentVersion, @QueryParam("listTypeEvenement") List<String> listTypeEvenement,
            @PathParam("authorityRole") String authorityRole, @PathParam("authorityCode") String authorityCode, LinkedHashMap<String, Object> mappedFileToRegent,
            @QueryParam("liasseNumber") String liasseNumber);
}
