/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 */

package fr.ge.common.regent.ws.v1.bean;

/**
 * Adresse du payeur.
 * 
 * @author mtakerra
 *
 */
public class AdressePayeur {

    /** numéro de voie de l'adresse. */
    private String numeroDeVoie;
    /** indécie de répétition */
    private String indiceDeRepetition;
    /** type de voie de l'adresse. */
    private String typeDeVoie;
    /** libellé de la voie de l'adresse. */
    private String libelleDeVoie;
    /** nom de la localité de l'adresse. */
    private String localite;
    /** complément d'adresse. */
    private String complementDeLocalisation;
    /** code postal de l'adresse. */
    private String codePostal;
    /** bureau destributeur de l'adresse. */
    private String bureauDistributeur;
    /** code pays de l'adresse. */
    private String codePays;
    /** adresse mail du payeur. */
    private String adresseEmail;

    /**
     * Constructeur.
     * 
     * @param numeroDeVoie
     *            {@link #numeroDeVoie}
     * @param typeDeVoie
     *            {@link #typeDeVoie}
     * @param libelleDeVoie
     *            {@link #libelleDeVoie}
     * @param localite
     *            {@link #localite}
     * @param complementDeLocalisation
     *            {@link #complementDeLocalisation}
     * @param codePostal
     *            {@link #codePostal}
     * @param bureauDistributeur
     *            {@link #bureauDistributeur}
     * @param codePays
     *            {@link #codePays}
     * @param adresseEmail
     *            {@link #adresseEmail}
     */
    public AdressePayeur(String numeroDeVoie, String typeDeVoie, String libelleDeVoie, String localite, String complementDeLocalisation, String codePostal, String bureauDistributeur, String codePays,
            String adresseEmail) {
        super();
        this.numeroDeVoie = numeroDeVoie;
        this.typeDeVoie = typeDeVoie;
        this.libelleDeVoie = libelleDeVoie;
        this.localite = localite;
        this.complementDeLocalisation = complementDeLocalisation;
        this.codePostal = codePostal;
        this.bureauDistributeur = bureauDistributeur;
        this.codePays = codePays;
        this.adresseEmail = adresseEmail;
    }

    /**
     * 
     */
    public AdressePayeur() {
        super();
        // Nothing to do
    }

    /**
     * @return the numeroDeVoie
     */
    public String getNumeroDeVoie() {
        return numeroDeVoie;
    }

    /**
     * @param numeroDeVoie
     *            the numeroDeVoie to set
     */
    public void setNumeroDeVoie(String numeroDeVoie) {
        this.numeroDeVoie = numeroDeVoie;
    }

    /**
     * @return the indiceDeRepetition
     */
    public String getIndiceDeRepetition() {
        return indiceDeRepetition;
    }

    /**
     * @param indiceDeRepetition
     *            the indiceDeRepetition to set
     */
    public void setIndiceDeRepetition(String indiceDeRepetition) {
        this.indiceDeRepetition = indiceDeRepetition;
    }

    /**
     * @return the typeDeVoie
     */
    public String getTypeDeVoie() {
        return typeDeVoie;
    }

    /**
     * @param typeDeVoie
     *            the typeDeVoie to set
     */
    public void setTypeDeVoie(String typeDeVoie) {
        this.typeDeVoie = typeDeVoie;
    }

    /**
     * @return the libelleDeVoie
     */
    public String getLibelleDeVoie() {
        return libelleDeVoie;
    }

    /**
     * @param libelleDeVoie
     *            the libelleDeVoie to set
     */
    public void setLibelleDeVoie(String libelleDeVoie) {
        this.libelleDeVoie = libelleDeVoie;
    }

    /**
     * @return the localite
     */
    public String getLocalite() {
        return localite;
    }

    /**
     * @param localite
     *            the localite to set
     */
    public void setLocalite(String localite) {
        this.localite = localite;
    }

    /**
     * @return the complementDeLocalisation
     */
    public String getComplementDeLocalisation() {
        return complementDeLocalisation;
    }

    /**
     * @param complementDeLocalisation
     *            the complementDeLocalisation to set
     */
    public void setComplementDeLocalisation(String complementDeLocalisation) {
        this.complementDeLocalisation = complementDeLocalisation;
    }

    /**
     * @return the codePostal
     */
    public String getCodePostal() {
        return codePostal;
    }

    /**
     * @param codePostal
     *            the codePostal to set
     */
    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    /**
     * @return the bureauDistributeur
     */
    public String getBureauDistributeur() {
        return bureauDistributeur;
    }

    /**
     * @param bureauDistributeur
     *            the bureauDistributeur to set
     */
    public void setBureauDistributeur(String bureauDistributeur) {
        this.bureauDistributeur = bureauDistributeur;
    }

    /**
     * @return the codePays
     */
    public String getCodePays() {
        return codePays;
    }

    /**
     * @param codePays
     *            the codePays to set
     */
    public void setCodePays(String codePays) {
        this.codePays = codePays;
    }

    /**
     * @return the adresseEmail
     */
    public String getAdresseEmail() {
        return adresseEmail;
    }

    /**
     * @param adresseEmail
     *            the adresseEmail to set
     */
    public void setAdresseEmail(String adresseEmail) {
        this.adresseEmail = adresseEmail;
    }
}
