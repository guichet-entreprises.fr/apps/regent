/**
 * 
 */
package fr.ge.common.regent.ws.v1.bean;

import java.util.List;

/**
 * Bloc destinataireDossierCfe; contient les informations sur le dossier à
 * envoyer à un CFE.
 * 
 * @author $Author: lhoffste $
 * @version $Revision: 0 $
 */
public class DestinataireDossierCfe {

    /** Rôle du destinataire (CFE, TDR, CFE+TDR). */
    private String roleDestinataire;
    /** Code Edi du destinataire. */
    private String codeEdiDestinataire;
    /** Liste des pièces jointes pour ce destinataires */
    private List<String> indicePieceJointe;
    /** Information du paiement pour ce destinataire. */
    private PaiementDossier paiementDossier;
    /** Indice de la PJ représentant le XML regent pour ce destinataire. */
    private String indiceLiasseXml;

    /**
     * Constructeur de la class.
     * 
     * @param roleDestinataire
     *            {@link #roleDestinataire}
     * @param codeEdiDestinataire
     *            {@link #codeEdiDestinataire}
     * @param indicePieceJointe
     *            {@link #indicePieceJointe}
     * @param paiementDossier
     *            {@link #paiementDossier}
     * @param indiceLiasseXml
     *            {@link #indiceLiasseXml}
     */
    public DestinataireDossierCfe(String roleDestinataire, String codeEdiDestinataire, List<String> indicePieceJointe, PaiementDossier paiementDossier, String indiceLiasseXml) {
        super();
        this.roleDestinataire = roleDestinataire;
        this.codeEdiDestinataire = codeEdiDestinataire;
        this.indicePieceJointe = indicePieceJointe;
        this.paiementDossier = paiementDossier;
        this.indiceLiasseXml = indiceLiasseXml;
    }

    /**
     * Constructeur de la classe.
     *
     */
    public DestinataireDossierCfe() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @return the indicePieceJointe
     */
    public List<String> getIndicePieceJointe() {
        return indicePieceJointe;
    }

    /**
     * @param indicePieceJointe
     *            the indicePieceJointe to set
     */
    public void setIndicePieceJointe(List<String> indicePieceJointe) {
        this.indicePieceJointe = indicePieceJointe;
    }

    /**
     * @return the indiceLiasseXml
     */
    public String getIndiceLiasseXml() {
        return indiceLiasseXml;
    }

    /**
     * @param indiceLiasseXml
     *            the indiceLiasseXml to set
     */
    public void setIndiceLiasseXml(String indiceLiasseXml) {
        this.indiceLiasseXml = indiceLiasseXml;
    }

    /**
     * Accesseur sur l'attribut {@link #roleDestinataire}.
     *
     * @return String roleDestinataire
     */
    public String getRoleDestinataire() {
        return roleDestinataire;
    }

    /**
     * Mutateur sur l'attribut {@link #roleDestinataire}.
     *
     * @param roleDestinataire
     *            la nouvelle valeur de l'attribut roleDestinataire
     */
    public void setRoleDestinataire(String roleDestinataire) {
        this.roleDestinataire = roleDestinataire;
    }

    /**
     * Accesseur sur l'attribut {@link #codeEdiDestinataire}.
     *
     * @return String codeEdiDestinataire
     */
    public String getCodeEdiDestinataire() {
        return codeEdiDestinataire;
    }

    /**
     * Mutateur sur l'attribut {@link #codeEdiDestinataire}.
     *
     * @param codeEdiDestinataire
     *            la nouvelle valeur de l'attribut codeEdiDestinataire
     */
    public void setCodeEdiDestinataire(String codeEdiDestinataire) {
        this.codeEdiDestinataire = codeEdiDestinataire;
    }

    /**
     * Accesseur sur l'attribut {@link #paiementDossier}.
     *
     * @return PaiementDossier paiementDossier
     */
    public PaiementDossier getPaiementDossier() {
        return paiementDossier;
    }

    /**
     * Mutateur sur l'attribut {@link #paiementDossier}.
     *
     * @param paiementDossier
     *            la nouvelle valeur de l'attribut paiementDossier
     */
    public void setPaiementDossier(PaiementDossier paiementDossier) {
        this.paiementDossier = paiementDossier;
    }

}
