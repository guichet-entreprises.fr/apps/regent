/**
 * 
 */
package fr.ge.common.regent.ws.v1.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fr.ge.common.regent.ws.v1.bean.TcBean;

/**
 * Service de génération de XML-TC.
 * 
 * @author $Author: lhoffste $
 * @version $Revision: 0 $
 */

@Path("/v1")
public interface ITcRestService {

    /**
     * Génération et validation du XML-TC.
     * 
     * @param xmltcBean
     *            Objet représentant le xml-tc à générer.
     * @return le XML-TC généré et valdé.
     * @throws Exception
     */
    @POST
    @Path("/xml-tc/generate")
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    Response generateXmlTc(TcBean xmltcBean) throws Exception;
}
