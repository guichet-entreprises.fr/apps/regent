/**
 *
 */
package fr.ge.common.regent.ws.v1.service;

import java.util.LinkedHashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fr.ge.common.regent.ws.v1.bean.RegentBean;

/**
 * Regent Rest service contract.
 *
 * @author aolubi
 *
 */
@Path("/v1")
public interface IRegentRestService {

    /**
     * Create or update a pending message.
     *
     * @param bean
     *            message to persist
     * @return message URI
     * 
     * @deprecated Do not use this method! replaced with new method
     *             {@link #generateXmlRegentFromJs} to genrate xml-regent from a
     *             mapped JS file
     */
    @POST
    @Path("/private/generate/{regentVersion}/{typeFlux}/{typeEvenement}/{authorityRole}/{authorityCode}")
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    @Deprecated
    Response generate(@PathParam("regentVersion") String regentVersion, @PathParam("typeFlux") String typeFlux, @PathParam("typeEvenement") String typeEvenement,
            @PathParam("authorityRole") String authorityRole, @PathParam("authorityCode") String authorityCode, RegentBean regentBean);

    /**
     * WS that allows to get a list of the XML for a Regent version & evenment
     * Tags in a js file.
     *
     * @param the
     *            regent version, must not be null
     *
     * @param A
     *            list of the evenments that we want to get the list of
     *            tags,must not be null
     *
     * @return message URI
     */
    @GET
    @Path("/public/xml-regent/mapping")
    Response generateMappingJs(@QueryParam("regentVersion") String regentVersion, @QueryParam("listTypeEvenement") List<String> listTypeEvenement);

    /**
     * Create and validate an XML regent from a mapped file.
     * 
     * @param regentVersion
     *            version of the regent to generate
     * @param listTypeEvenement
     *            list of events concerned by the formality
     * @param authorityRole
     *            role of the recipient authority
     * @param authorityCode
     *            code of the recipient authority
     * @param mappedFileToRegent
     *            a Map<tag, value> of the trags to generate in the xml-regent
     * @return generated anda validated XML-REGENT
     */
    @POST
    @Path("/private/xml-regent/generate/{regentVersion}/{authorityRole}/{authorityCode}")
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    Response generateXmlRegentFromJs(@PathParam("regentVersion") String regentVersion, @QueryParam("listTypeEvenement") List<String> listTypeEvenement,
            @PathParam("authorityRole") String authorityRole, @PathParam("authorityCode") String authorityCode, LinkedHashMap<String, Object> mappedFileToRegent);
}
