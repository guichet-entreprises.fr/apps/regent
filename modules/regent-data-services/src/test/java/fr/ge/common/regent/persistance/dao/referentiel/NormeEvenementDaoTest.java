package fr.ge.common.regent.persistance.dao.referentiel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import fr.ge.common.regent.bean.modele.referentiel.ENormeEvenement;
import fr.ge.common.regent.util.test.AbstractSpringTest;

/**
 * Le Class NormeEvenementDaoTest.
 */
public class NormeEvenementDaoTest extends AbstractSpringTest {

    /** La constante VERSION. */
    private static final String VERSION = "1";

    /** Le norme evenement dao. */
    @Autowired
    private NormeEvenementDao normeEvenementDao;

    /**
     * Set le up.
     */
    @Before
    @Transactional(readOnly = false, value = "regent")
    public void setUp() {
        ENormeEvenement normEvt = new ENormeEvenement();
        normEvt.setId(1);
        normEvt.setEvenement("EVENEMENT_1");
        normEvt.setRubrique("RUBRIQUE A");
        normEvt.setVersion(VERSION);
        normEvt.setCondition(Boolean.TRUE);
        normeEvenementDao.save(normEvt);

        ENormeEvenement normEvt2 = new ENormeEvenement();
        normEvt2.setId(2);
        normEvt2.setEvenement("EVENEMENT_2");
        normEvt2.setRubrique("RUBRIQUE B");
        normEvt2.setVersion(VERSION);
        normEvt2.setCondition(Boolean.TRUE);
        normeEvenementDao.save(normEvt2);
    }

    /**
     * Test get rubrique by evenement and version.
     */
    @Test
    @Transactional(readOnly = false, value = "regent")
    public void testGetRubriqueByEvenementAndVersion() {
        List<String> listEvts = new ArrayList<String>();
        listEvts.add("EVENEMENT_1");
        listEvts.add("EVENEMENT_2");
        List<String> rubriqueByEvenementAndVersion = normeEvenementDao.getRubriqueByEvenementAndVersion(VERSION, listEvts);

        assertNotNull(rubriqueByEvenementAndVersion);
        assertEquals(2, rubriqueByEvenementAndVersion.size());

    }

}
