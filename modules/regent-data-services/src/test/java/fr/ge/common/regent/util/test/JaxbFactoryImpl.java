/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.regent.util.test;

import java.io.ByteArrayInputStream;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import fr.ge.common.utils.exception.TechnicalException;

/**
 * The Class JaxbFactoryImpl.
 *
 * @author Christian Cougourdan
 */
public class JaxbFactoryImpl {

    /** The Constant instance. */
    private static final JaxbFactoryImpl _INSTANCE = new JaxbFactoryImpl();

    /** Context cache. */
    private final Map<Class<?>, JAXBContext> contextCache = new ConcurrentHashMap<>();

    /**
     * Hidden constructor.
     */
    private JaxbFactoryImpl() {
        // Nothing to do
    }

    /**
     * Return factory instance singleton.
     *
     * @return factory instance
     */
    public static JaxbFactoryImpl instance() {
        return _INSTANCE;
    }

    /**
     * Retrieve JAXB context for specific class.
     *
     * @param clazz
     *            target class
     * @return JAXB context instance
     */
    public JAXBContext context(final Class<?> clazz) {
        JAXBContext jaxbContext = this.contextCache.get(clazz);

        if (null == jaxbContext) {
            try {
                jaxbContext = JAXBContext.newInstance(clazz);
                this.contextCache.put(clazz, jaxbContext);
            } catch (final JAXBException ex) {
                throw new TechnicalException("Unable to load JAXB context for class " + clazz.getName(), ex);
            }
        }

        return jaxbContext;
    }

    /**
     * Unmarshal xml bean from byte array content.
     *
     * @param <T>
     *            the generic type
     * @param beanAsBytes
     *            bean as byte array
     * @param clazz
     *            bean class
     * @return class instance
     */
    @SuppressWarnings("unchecked")
    public <T> T unmarshal(final byte[] beanAsBytes, final Class<T> clazz) {
        if (null == beanAsBytes) {
            return null;
        }

        final JAXBContext jaxbContext = this.context(clazz);

        final Unmarshaller unmarshaller;
        try {
            unmarshaller = jaxbContext.createUnmarshaller();
            unmarshaller.setListener(new Unmarshaller.Listener() {
            });

        } catch (final JAXBException e) {
            throw new TechnicalException("Unable to create unmarshaller for class " + clazz.getName(), e);
        }

        final T bean;
        try {
            bean = (T) unmarshaller.unmarshal(new ByteArrayInputStream(beanAsBytes));
        } catch (final JAXBException e) {
            throw new TechnicalException("Unable to unmarshall bean " + clazz.getName(), e);
        }

        return bean;
    }

    /**
     * Marshaller.
     *
     * @param <T>
     *            type générique
     * @param bean
     *            bean
     * @return marshaller
     */
    private <T> Marshaller marshaller(final T bean) {
        final JAXBContext jaxbContext = this.context(bean.getClass());

        final Marshaller marshaller;
        try {
            marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
        } catch (final JAXBException ex) {
            throw new TechnicalException("Unable to create marshaller for class " + bean.getClass().getName(), ex);
        }
        return marshaller;
    }

}
