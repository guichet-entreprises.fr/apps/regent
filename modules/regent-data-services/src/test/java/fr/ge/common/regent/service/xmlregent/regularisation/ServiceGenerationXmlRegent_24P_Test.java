/**
 * 
 */
package fr.ge.common.regent.service.xmlregent.regularisation;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import fr.ge.common.regent.service.xmlregent.AbstractServiceGenerationXmlRegentTest;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * Classe de tests d'XML regent pour les évènement de type 24P en
 * régularisation.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
@Ignore
public class ServiceGenerationXmlRegent_24P_Test extends AbstractServiceGenerationXmlRegentTest {

    private static final String TYPE_FORMALITE = "regularisation";
    private static final String TYPE_EVENEMENT = "24P";

    /**
     * JIRA 2789 / Selenium 10.
     *
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseE02Sel10() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "M93010004820", "M9301", "TDR", Arrays.asList("M9301", "G9301"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.5";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise E02.5 ", "1", result);
    }

    /**
     * SONIC-3561 [SUPPORT][PROD][GUB-58] - Régul pb Xml Regent balise E92.1 -
     * C77011942464.
     *
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseE92GUB58V2008() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C77011942464", "C7701", "CFE", Arrays.asList("C7701"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE/E92/E92.1";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise E92.1 ", "N", result);
    }

    /**
     * SONIC-3561 [SUPPORT][PROD][GUB-58] - Régul pb Xml Regent balise E92.1 -
     * C77011942464.
     *
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseE92GUB58V2026() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C77011942464", "C7701", "CFE", Arrays.asList("C7701"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE/E92/E92.1";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise E92.1 ", "N", result);
    }

    @Test
    public void testCheckBaliseC10_2_V2008_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "M79010002219", "M7901", "TDR", Arrays.asList("M7901"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2";

        String result = evaluteXpathFromXml(xpath, xml);

        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());

        Assert.assertEquals("La balise C10.2", date, result);
    }

    @Test
    public void testCheckBaliseC10_2_V2016_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "M79010002219", "M7901", "TDR", Arrays.asList("M7901"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2";

        String result = evaluteXpathFromXml(xpath, xml);

        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());

        Assert.assertEquals("La balise C10.2", date, result);
    }

    @Test
    public void testCheckBaliseC10_2_V2008_EV_24P() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "M74010001951", "M7401", "TDR", Arrays.asList("M7401"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2";

        String result = evaluteXpathFromXml(xpath, xml);

        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());

        Assert.assertEquals("La balise C10.2", date, result);
    }

    @Test
    public void testCheckBaliseC10_2_V2016_EV_24P() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "M74010001951", "M7401", "TDR", Arrays.asList("M7401"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2";

        String result = evaluteXpathFromXml(xpath, xml);

        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());

        Assert.assertEquals("La balise C10.2", date, result);
    }

}
