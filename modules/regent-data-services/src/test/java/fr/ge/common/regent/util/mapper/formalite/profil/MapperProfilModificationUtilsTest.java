/**
 * 
 */
package fr.ge.common.regent.util.mapper.formalite.profil;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.xmlfield.core.XmlField;
import org.xmlfield.core.exception.XmlFieldParsingException;

import fr.ge.common.regent.bean.formalite.dialogue.creation.PostalCommuneBean;
import fr.ge.common.regent.bean.formalite.profil.modification.ProfilModificationBean;
import fr.ge.common.regent.bean.xml.formalite.profil.modification.IProfilModification;
import fr.ge.common.regent.util.test.AbstractSpringTest;

/**
 * MapperProfilModificationUtilsTest.
 * 
 * @author roveda
 * 
 */
public class MapperProfilModificationUtilsTest extends AbstractSpringTest {

    /**
     * 
     */
    private static final String URSSAF = "URSSAF";

    /**
     * MapperProfilModificationUtils.
     */
    private MapperProfilModificationUtils mapper;

    /** La constante XF. */
    private static final XmlField XF = new XmlField();

    /**
     * mapper dozer profilEntrepriseModifMapper.
     */
    @Autowired
    private Mapper profilEntrepriseModifMapper;

    /**
     * setUp.
     */
    @Before
    public void setUp() {
        mapper = new MapperProfilModificationUtils();
        mapper.setProfilEntrepriseModifMapper(profilEntrepriseModifMapper);
    }

    /**
     * Test mapping interface(bdd) -> bean.
     * 
     * @throws XmlFieldParsingException
     *             XmlFieldParsingException
     * @throws FileNotFoundException
     *             FileNotFoundException
     */
    @SuppressWarnings("deprecation")
    @Test
    public final void testMapperInterfaceToBean() throws XmlFieldParsingException, FileNotFoundException {
        ProfilModificationBean profilEntrepriseModificationBean = new ProfilModificationBean();

        FileInputStream fileInputStream = new FileInputStream("src/test/resources/formalitesXml/profilModification.xml");
        IProfilModification xml = XF.xmlToObject(fileInputStream, IProfilModification.class);

        mapper.mapper(xml, profilEntrepriseModificationBean);

        assertEquals("commune_TU_@postalCommune", profilEntrepriseModificationBean.getPostalCommune().getCommune());
        assertEquals("commune_TU_@postalCommune1", profilEntrepriseModificationBean.getPostalCommune1().getCommune());
        assertEquals("commune_TU_@postalCommune2", profilEntrepriseModificationBean.getPostalCommune2().getCommune());

        assertNotNull(profilEntrepriseModificationBean.getEvenement());
        assertEquals(2, profilEntrepriseModificationBean.getEvenement().size());
        assertEquals("01P", profilEntrepriseModificationBean.getEvenement().get(0));
        assertEquals("02P", profilEntrepriseModificationBean.getEvenement().get(1));

        assertNotNull(profilEntrepriseModificationBean.getModifEvenements2());
        assertEquals(2, profilEntrepriseModificationBean.getModifEvenements2().size());
        assertEquals("TU_A", profilEntrepriseModificationBean.getModifEvenements2().get(0));
        assertEquals("TU_B", profilEntrepriseModificationBean.getModifEvenements2().get(1));

        assertNotNull(profilEntrepriseModificationBean.getModifEvenements());
        assertEquals(2, profilEntrepriseModificationBean.getModifEvenements().size());
        assertEquals("TU_C", profilEntrepriseModificationBean.getModifEvenements().get(0));
        assertEquals("TU_D", profilEntrepriseModificationBean.getModifEvenements().get(1));

    }

    /**
     * 
     * Test mapping bean -> interface(bdd).
     * 
     * @throws XmlFieldParsingException
     *             XmlFieldParsingException
     */
    @SuppressWarnings("deprecation")
    @Test
    public final void testMapperBeanToInterface() throws XmlFieldParsingException {
        ProfilModificationBean profilEntrepriseModificationBean = new ProfilModificationBean();

        PostalCommuneBean postalCommune = new PostalCommuneBean();
        postalCommune.setCommune("commune_TU_@postalCommune");
        profilEntrepriseModificationBean.setPostalCommune(postalCommune);
        PostalCommuneBean postalCommune1 = new PostalCommuneBean();
        postalCommune1.setCommune("commune_TU_@postalCommune_1");
        profilEntrepriseModificationBean.setPostalCommune1(postalCommune1);
        PostalCommuneBean postalCommune2 = new PostalCommuneBean();
        postalCommune2.setCommune("commune_TU_@postalCommune_2");
        profilEntrepriseModificationBean.setPostalCommune2(postalCommune2);

        List<String> evenement = new ArrayList<String>();
        evenement.add("01P");
        evenement.add("02P");
        profilEntrepriseModificationBean.setEvenement(evenement);

        List<String> modifEvenements2 = new ArrayList<String>();
        modifEvenements2.add("TU_A");
        modifEvenements2.add("TU_B");
        profilEntrepriseModificationBean.setModifEvenements2(modifEvenements2);

        List<String> modifEvenements = new ArrayList<String>();
        modifEvenements.add("TU_C");
        modifEvenements.add("TU_D");
        profilEntrepriseModificationBean.setModifEvenements(modifEvenements);

        // Init Interface
        IProfilModification xml = XF.newObject(IProfilModification.class);

        mapper.mapper(profilEntrepriseModificationBean, xml);

        assertEquals("commune_TU_@postalCommune", xml.getPostalCommune().getCommune());
        assertEquals("commune_TU_@postalCommune_1", xml.getPostalCommune1().getCommune());
        assertEquals("commune_TU_@postalCommune_2", xml.getPostalCommune2().getCommune());

        assertNotNull(profilEntrepriseModificationBean.getEvenement());
        assertEquals(2, xml.getEvenement().length);
        assertEquals("01P", xml.getEvenement()[0].getString());
        assertEquals("02P", xml.getEvenement()[1].getString());

        assertNotNull(profilEntrepriseModificationBean.getModifEvenements2());
        assertEquals(2, xml.getModifEvenements2().length);
        assertEquals("TU_A", xml.getModifEvenements2()[0].getString());
        assertEquals("TU_B", xml.getModifEvenements2()[1].getString());

        assertNotNull(profilEntrepriseModificationBean.getModifEvenements());
        assertEquals(2, xml.getModifEvenements().length);
        assertEquals("TU_C", xml.getModifEvenements()[0].getString());
        assertEquals("TU_D", xml.getModifEvenements()[1].getString());
    }

    @Test
    public final void testGUB358() throws FileNotFoundException, XmlFieldParsingException {
        ProfilModificationBean profilEntrepriseModificationBean = new ProfilModificationBean();

        String commune = "29232";
        String codePostal = "29000";

        PostalCommuneBean postalCommune = new PostalCommuneBean();
        postalCommune.setCommune(commune);
        postalCommune.setCodePostal(codePostal);
        profilEntrepriseModificationBean.setPostalCommune(postalCommune);

        PostalCommuneBean postalCommune1 = new PostalCommuneBean();
        postalCommune1.setCommune(commune);
        postalCommune1.setCodePostal(codePostal);
        profilEntrepriseModificationBean.setPostalCommune1(postalCommune1);

        PostalCommuneBean postalCommune2 = new PostalCommuneBean();
        profilEntrepriseModificationBean.setPostalCommune2(postalCommune2);

        List<String> evenement = new ArrayList<String>();
        evenement.add("01P");
        evenement.add("02P");
        profilEntrepriseModificationBean.setEvenement(evenement);

        List<String> modifEvenements2 = new ArrayList<String>();
        modifEvenements2.add("TU_A");
        modifEvenements2.add("TU_B");
        profilEntrepriseModificationBean.setModifEvenements2(modifEvenements2);

        List<String> modifEvenements = new ArrayList<String>();
        modifEvenements.add("TU_C");
        modifEvenements.add("TU_D");
        profilEntrepriseModificationBean.setModifEvenements(modifEvenements);

        // Init Interface
        IProfilModification iProfilEntrepriseModification = XF.newObject(IProfilModification.class);

        this.mapper.mapper(profilEntrepriseModificationBean, iProfilEntrepriseModification);

        assertEquals(iProfilEntrepriseModification.getPostalCommune().getCommune(), commune);
    }

    @Test
    public final void testGUB327() throws FileNotFoundException, XmlFieldParsingException {
        ProfilModificationBean profilEntrepriseModificationBean = new ProfilModificationBean();

        profilEntrepriseModificationBean.setSecteurCfe(URSSAF);

        // Init Interface
        IProfilModification iProfilEntrepriseModification = XF.newObject(IProfilModification.class);

        this.mapper.mapper(profilEntrepriseModificationBean, iProfilEntrepriseModification);

        assertEquals(URSSAF, iProfilEntrepriseModification.getSecteurCfe());
    }
}
