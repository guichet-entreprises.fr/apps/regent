package fr.ge.common.regent.persistance.dao.referentiel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import fr.ge.common.regent.bean.modele.referentiel.ENormeFlux;
import fr.ge.common.regent.constante.enumeration.DestinataireXmlRegentEnum;
import fr.ge.common.regent.util.test.AbstractSpringTest;

/**
 * Le Class NormeFluxDaoTest.
 */
public class NormeFluxDaoTest extends AbstractSpringTest {

    /** Le norme flux dao. */
    @Autowired
    private NormeFluxDao normeFluxDao;

    /**
     * Test get flux by evenement and destinataire.
     */
    @Test
    @Transactional(readOnly = true, value = "regent")
    public void testGetFluxByEvenementAndDestinataire() {
        List<String> evenement = new ArrayList<String>();
        evenement.add("24P");
        evenement.add("16P");
        List<ENormeFlux> fluxByEvenementAndDestinataire = normeFluxDao.getFluxByEvenementAndDestinataire(DestinataireXmlRegentEnum.GREFFE, evenement);
        assertNotNull(fluxByEvenementAndDestinataire);
        assertEquals(3, fluxByEvenementAndDestinataire.size());
    }

}
