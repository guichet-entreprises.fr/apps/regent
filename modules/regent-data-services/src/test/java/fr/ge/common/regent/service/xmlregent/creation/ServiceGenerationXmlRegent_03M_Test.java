/**
 * 
 */
package fr.ge.common.regent.service.xmlregent.creation;

import static org.fest.assertions.Assertions.assertThat;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import fr.ge.common.regent.bean.xml.v2008.IXmlGlobal2008;
import fr.ge.common.regent.bean.xml.v2016.IXmlGlobal2016;
import fr.ge.common.regent.service.xmlregent.AbstractServiceGenerationXmlRegentTest;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * Test événement 03M.
 * 
 * @author $Author: hhichri $
 * @version $Revision: 0 $
 */
public class ServiceGenerationXmlRegent_03M_Test extends AbstractServiceGenerationXmlRegentTest {

    private static final String TYPE_FORMALITE = "creation";
    private static final String TYPE_EVENEMENT = "03M";

    /**
     * Test Scénario 21 : Balise U31.
     * 
     * @throws TechnicalException
     */
    @Test
    public void testCheckBaliseU31Scenario21() throws TechnicalException {
        IXmlGlobal2008 result = getXmlRegent2008(TYPE_EVENEMENT, TYPE_FORMALITE, "M28010001251", "M2801", "CFE+TDR", Arrays.asList("M2801", "G2801"),
                AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        Assert.assertEquals("La balise U31 ", "114", result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getRFU()[0].getU31()[0].getString());
    }

    /**
     * Test Scénario 26 : Balise U31.
     * 
     * @throws TechnicalException
     */
    @Test
    public void testCheckBaliseU31Scenario26() throws TechnicalException {
        IXmlGlobal2008 result = getXmlRegent2008(TYPE_EVENEMENT, TYPE_FORMALITE, "C22010001264", "C2201", "CFE", Arrays.asList("C2201", "G2203"),
                AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        Assert.assertEquals("La balise U31 ", "115", result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getRFU()[0].getU31()[0].getString());
    }

    /**
     * Test Scénario 58 : Balise U31.
     * 
     * @throws TechnicalException
     */
    @Test
    public void testCheckBaliseU31Scenario58() throws TechnicalException {
        IXmlGlobal2008 result = getXmlRegent2008(TYPE_EVENEMENT, TYPE_FORMALITE, "C13100001032", "C1310", "CFE", Arrays.asList("G1303", "C1310"),
                AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        Assert.assertEquals("La balise U31 ", "115", result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getRFU()[0].getU31()[0].getString());
    }

    /**
     * Test Scénario 105 : Balise U31.
     * 
     * @throws TechnicalException
     */
    @Test
    public void testCheckBaliseU31Scenario105() throws TechnicalException {
        IXmlGlobal2008 result = getXmlRegent2008(TYPE_EVENEMENT, TYPE_FORMALITE, "G94010001286", "G9401", "CFE+TDR", Arrays.asList("G9401"),
                AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        Assert.assertEquals("La balise U31 ", "111", result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getRFU()[0].getU31()[0].getString());
    }

    /**
     * Test Scénario 105 : Balise U32.
     * 
     * @throws TechnicalException
     */
    @Test
    public void testCheckBaliseU32Scenario105() throws TechnicalException {
        IXmlGlobal2008 result = getXmlRegent2008(TYPE_EVENEMENT, TYPE_FORMALITE, "G94010001286", "G9401", "CFE+TDR", Arrays.asList("G9401"),
                AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        Assert.assertEquals("La balise U32 ", "212", result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getRFU()[0].getU32()[0].getString());
    }

    /**
     * Test Scénario 21 : Balise E01.
     * 
     * @throws TechnicalException
     */
    @Test
    public void testCheckBaliseU21Scenario21() throws TechnicalException {
        IXmlGlobal2008 result = getXmlRegent2008(TYPE_EVENEMENT, TYPE_FORMALITE, "M28010001251", "M2801", "CFE+TDR", Arrays.asList("M2801", "G2801"),
                AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        Assert.assertEquals("La balise E01 ", "1", result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getEtablissement()[0].getICE()[0].getE01());
    }

    /**
     * Test Scénario 60 : Balise E01.
     * 
     * @throws TechnicalException
     */
    @Test
    public void testCheckBaliseU21Scenario60() throws TechnicalException {
        IXmlGlobal2008 result = getXmlRegent2008(TYPE_EVENEMENT, TYPE_FORMALITE, "M13010001276", "M1301", "CFE+TDR", Arrays.asList("M1301", "G1303"),
                AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        Assert.assertEquals("La balise E01 ", "1", result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getEtablissement()[0].getICE()[0].getE01());
    }

    /**
     * Test Scénario 105 : Balise E01.
     * 
     * @throws TechnicalException
     */
    @Test
    public void testCheckBaliseU21Scenario105() throws TechnicalException {
        IXmlGlobal2008 result = getXmlRegent2008(TYPE_EVENEMENT, TYPE_FORMALITE, "G94010001286", "G9401", "CFE+TDR", Arrays.asList("G9401"),
                AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        Assert.assertEquals("La balise E01 ", "1", result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getEtablissement()[0].getICE()[0].getE01());
    }

    /**
     * JIRA montant minimum en centime : selenium 58, balise 24.4.
     *
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseM244Sel58() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C13100001032", "C1310", "CFE", Arrays.asList("C1310", "G1303"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CSM/M24/M24.4";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise M24.4", "500000", result);
    }

    /**
     * Test balise E77 qui doit pas être présente dans la version 2008 mais pas
     * 2016
     * 
     * @throws TechnicalException
     */
    @Test
    public void testCheckBaliseE77() throws TechnicalException {
        // version 2008
        IXmlGlobal2008 result2008 = getXmlRegent2008(TYPE_EVENEMENT, TYPE_FORMALITE, "C22010001264", "C2201", "CFE", Arrays.asList("C2201", "G2203"),
                AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        Assert.assertEquals("La balise 77 ", "09", result2008.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getEtablissement()[0].getCAE()[0].getE77()[0].getE771());

        // version 2016 : balise CAE
        IXmlGlobal2016 result2016 = getXmlRegent2016(TYPE_EVENEMENT, TYPE_FORMALITE, "C22010001264", "C2201", "CFE", Arrays.asList("C2201", "G2203"),
                AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);
        Assert.assertNotNull(result2016.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getEtablissement()[0].getCAE()[0]);
    }

    /**
     * Test Balise U75.
     * 
     * @throws TechnicalException
     *             : TechnicalException
     */
    @Test
    public void testCheckBaliseU75() throws TechnicalException {

        // test ou la regle spel est respecté on attend 212
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "G94010001286", "G9401", "CFE+TDR", Arrays.asList("G9401"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);

        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OIU/U75";
        String result = evaluteXpathFromXml(xpath, xml);

        assertThat(result).isNotNull();
        assertThat(result).isEqualTo("212");

        // test ou la regle spel n'est pas respecté on attend vide car la balise
        // mere OIU n'est pas géré
        String xml2 = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C13100001032", "C1310", "CFE", Arrays.asList("G1303", "C1310"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);

        String result2 = evaluteXpathFromXml(xpath, xml2);

        assertThat(result2).isNotNull();
        assertThat(result2).isEqualTo("");
    }

    /**
     * Test balise E73 obligatoire sur la version 2016 de la norme
     * 
     * @throws TechnicalException
     */
    @Test
    public void testCheckBaliseE73() throws TechnicalException {
        // version 2016 : E 73 obligatoire
        IXmlGlobal2016 result2016 = getXmlRegent2016(TYPE_EVENEMENT, TYPE_FORMALITE, "C75010000292", "C7501", "CFE", Arrays.asList("C7501", "G7501"),
                AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);
        Assert.assertNotNull(result2016.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getEtablissement()[0].getCAE()[0].getE73());
    }

    @Test
    public void testCheckBaliseC10_2_V2008_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C13100001032", "C1310", "CFE", Arrays.asList("C1310"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise C10.2", "2008-01-01", result);
    }

    @Test
    public void testCheckBaliseC10_2_V2016_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C13100001032", "C1310", "CFE", Arrays.asList("C1310"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise C10.2", "2008-01-01", result);
    }
}
