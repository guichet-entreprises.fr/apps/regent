package fr.ge.common.regent.data;

import javax.sql.DataSource;

import org.dbunit.DataSourceDatabaseTester;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.ReplacementDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * Initialise la données de la base regent.
 * 
 * @author aolubi
 * 
 */
public class ReferentielRegentDataInit {

    /** La constante LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ReferentielRegentDataInit.class);

    /** Le referentiel data source. */
    @Autowired
    private DataSource regentDataSource;

    private boolean init = false;

    /**
     * Initialise le.
     *
     * @throws Exception
     *             le exception
     */
    @Transactional(readOnly = false, value = "regent")
    public void init() throws Exception {

        // if (!TestUnitaireContext.isReferentielCharge()){
        if (!init) {
            LOGGER.warn("Starting Regent init....");
            DataSourceDatabaseTester databaseTester = new DataSourceDatabaseTester(this.regentDataSource);

            // regent
            ReplacementDataSet[] dataSets = new ReplacementDataSet[] {

                    new ReplacementDataSet(new FlatXmlDataSetBuilder().setColumnSensing(true).build(this.getClass().getResourceAsStream("/data/referentiel_regent/norme_destinataire.xml"))),
                    new ReplacementDataSet(new FlatXmlDataSetBuilder().setColumnSensing(true).build(this.getClass().getResourceAsStream("/data/referentiel_regent/norme_evenement.xml"))),
                    new ReplacementDataSet(new FlatXmlDataSetBuilder().setColumnSensing(true).build(this.getClass().getResourceAsStream("/data/referentiel_regent/normeflux.xml"))),
                    new ReplacementDataSet(new FlatXmlDataSetBuilder().setColumnSensing(true).build(this.getClass().getResourceAsStream("/data/referentiel_regent/regles_regent.xml"))), };

            for (ReplacementDataSet dataset : dataSets) {
                dataset.addReplacementObject("[NULL]", null);
                dataset.addReplacementObject("", null);
            }
            databaseTester.setDataSet(new CompositeDataSet(dataSets));
            databaseTester.onSetup();

            LOGGER.info("Regent init done.");
            // TestUnitaireContext.setReferentielCharge(true);
            init = true;
        }
    }

    /**
     * Set le regent data source.
     *
     * @param regentDataSource
     *            the regentDataSource to set
     */
    public void setRegentDataSource(final DataSource regentDataSource) {
        this.regentDataSource = regentDataSource;
    }

}
