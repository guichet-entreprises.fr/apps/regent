package fr.ge.common.regent.persistance.dao.referentiel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import fr.ge.common.referentiel.bean.IRefBean;
import fr.ge.common.referentiel.dao.SupraNationaliteDao;
import fr.ge.common.regent.util.test.AbstractSpringTest;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * Le Class SupraNationaliteDaoTest.
 */
public class SupraNationaliteDaoTest extends AbstractSpringTest {

    /** Le supra nationalite dao. */
    @Autowired
    private SupraNationaliteDao supraNationaliteDao;

    /**
     * Test get nationalites.
     */
    @Test
    @Transactional(readOnly = true, value = "regent")
    public void testGetNationalites() {
        List<IRefBean> nationalites = supraNationaliteDao.getNationalites();
        assertNotNull(nationalites);
        assertTrue(nationalites.size() > 0);
        assertEquals(219, nationalites.size());
    }

    /**
     * Test recuperer nationalite by nom pays and by code pays tronque.
     * 
     * @throws TechnicalException
     */
    @Test
    @Transactional(readOnly = true, value = "regent")
    public void testRecupererNationaliteByNomPaysAndByCodePaysTronque() throws TechnicalException {
        String codePaysTronque = "203";
        String nomPays = "IRAQ";
        String recupererNationalite = supraNationaliteDao.recupererNationalite(nomPays, codePaysTronque);

        assertNotNull(recupererNationalite);
        assertEquals("IRAKIENNE", recupererNationalite);
    }

    /**
     * Test recuperer nationalite by code pays tronque.
     * 
     * @throws TechnicalException
     */
    @Test
    @Transactional(readOnly = true, value = "regent")
    public void testRecupererNationaliteByCodePaysTronque() throws TechnicalException {
        String codePaysTronque = "203";
        String recupererNationalite = supraNationaliteDao.recupererNationalite(codePaysTronque);

        assertNotNull(recupererNationalite);
        assertEquals("IRAKIENNE", recupererNationalite);
    }

    /**
     * Test de vérification de levée d'exception
     * 
     * @throws TechnicalException
     */
    @Test(expected = TechnicalException.class)
    @Transactional(readOnly = true, value = "regent")
    public void testRecupererNationalite() throws TechnicalException {
        String codePaysTronque = "432";
        supraNationaliteDao.recupererNationalite(null, codePaysTronque);
    }
}
