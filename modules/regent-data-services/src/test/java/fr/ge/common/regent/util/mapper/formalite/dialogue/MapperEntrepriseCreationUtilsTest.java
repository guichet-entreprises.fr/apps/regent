package fr.ge.common.regent.util.mapper.formalite.dialogue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.xmlfield.core.XmlField;
import org.xmlfield.core.exception.XmlFieldParsingException;

import fr.ge.common.regent.bean.formalite.dialogue.creation.DialogueCreationBean;
import fr.ge.common.regent.bean.xml.formalite.dialogue.creation.IDialogueCreation;
import fr.ge.common.regent.util.test.AbstractSpringTest;

/**
 * Classe de Test sur Entreprise Creation.
 * 
 * @author $Author: hhichri $
 * @version $Revision: 0 $
 */
public class MapperEntrepriseCreationUtilsTest extends AbstractSpringTest {

    /**
     * mapperEntrepriseCreationUtils.
     */
    @Autowired
    private MapperEntrepriseCreationUtils mapperEntrepriseCreationUtils;

    /**
     * Test Mapper Entreprise.
     * 
     * @throws XmlFieldParsingException
     *             : {@link XmlFieldParsingException}
     * @throws FileNotFoundException
     *             : {@link FileNotFoundException}
     */
    @Test
    public void mapperEntrepriseXmlToBeanTest() throws XmlFieldParsingException, FileNotFoundException {
        String associeUnique = "oui";
        String associeUniqueEstPresident = "non";
        String autreEtablissementUE = "oui";
        String capitalDevise = "EUR";

        FileInputStream fileInputStream = new FileInputStream("src/test/resources/formalitesXml/formalitesCreation.xml");
        IDialogueCreation xml = new XmlField().xmlToObject(fileInputStream, IDialogueCreation.class);
        // Init Bean
        DialogueCreationBean formaliteBean = new DialogueCreationBean();
        assertNull(formaliteBean.getNumeroDossier());
        this.mapperEntrepriseCreationUtils.mapperEntreprise(xml, formaliteBean);

        assertEquals(associeUnique, formaliteBean.getEntreprise().getAssocieUnique());
        assertEquals(associeUniqueEstPresident, formaliteBean.getEntreprise().getAssocieUniqueEstPresident());
        assertEquals(autreEtablissementUE, formaliteBean.getEntreprise().getAutreEtablissementUE());
        assertEquals(capitalDevise, formaliteBean.getEntreprise().getCapitalDevise());
        assertNotNull(formaliteBean.getEntreprise().getEirl());
        assertNotNull(formaliteBean.getEntreprise().getEirl().getDefinitionPatrimoine());
        assertEquals(2, formaliteBean.getEntreprise().getEirl().getDefinitionPatrimoine().size());
    }

}
