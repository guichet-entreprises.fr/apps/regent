package fr.ge.common.regent.util.mapper.formalite.dialogue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.xmlfield.core.XmlField;
import org.xmlfield.core.exception.XmlFieldParsingException;

import fr.ge.common.regent.bean.formalite.dialogue.cessation.DialogueCessationBean;
import fr.ge.common.regent.bean.formalite.dialogue.cessation.DirigeantCessationBean;
import fr.ge.common.regent.bean.formalite.dialogue.cessation.EntrepriseCessationBean;
import fr.ge.common.regent.bean.formalite.dialogue.cessation.EtablissementCessationBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.AdresseBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.PostalCommuneBean;
import fr.ge.common.regent.bean.xml.formalite.dialogue.cessation.IDialogueCessation;
import fr.ge.common.regent.bean.xml.formalite.dialogue.cessation.IEtablissementCessation;
import fr.ge.common.regent.util.test.AbstractSpringTest;

/**
 * Le Class MapperFormaliteCessationUtilsTest.
 */
public class MapperDialogueCessationUtilsTest extends AbstractSpringTest {

    /** La constante NUM_DOSSIER_TU. */
    private static final String NUM_DOSSIER_TU = "NUM_DOSSIER_TU";

    /** La constante XF. */
    private static final XmlField XF = new XmlField();

    /** Le mapper. */
    @Autowired
    private MapperDialogueCessationUtils mapper;

    /**
     * Test mapper formalite cessation bean i formalite cessation bean to
     * interface.
     *
     * @throws XmlFieldParsingException
     *             le xml field parsing exception
     */
    @Test
    public void testMapperFormaliteCessationBeanIFormaliteCessationBeanToInterface() throws XmlFieldParsingException {
        // Init Bean
        DialogueCessationBean bean = new DialogueCessationBean();
        bean.setNumeroDossier(NUM_DOSSIER_TU);
        EntrepriseCessationBean entreprise = new EntrepriseCessationBean();
        List<DirigeantCessationBean> dirigeants = new ArrayList<DirigeantCessationBean>();
        entreprise.setDirigeants(dirigeants);
        DirigeantCessationBean dirigeant = new DirigeantCessationBean();
        dirigeant.setNumeroDossier(NUM_DOSSIER_TU);
        dirigeants.add(dirigeant);
        bean.setEntreprise(entreprise);
        List<EtablissementCessationBean> etablissementsBean = new ArrayList<EtablissementCessationBean>();
        EtablissementCessationBean e1 = new EtablissementCessationBean();
        AdresseBean adresseE1 = new AdresseBean();
        PostalCommuneBean codePostalCommuneE1 = new PostalCommuneBean();
        codePostalCommuneE1.setCommune("commune_TU_@1");
        adresseE1.setCodePostalCommune(codePostalCommuneE1);
        e1.setAdresse(adresseE1);
        etablissementsBean.add(e1);

        EtablissementCessationBean e2 = new EtablissementCessationBean();
        AdresseBean adresseE2 = new AdresseBean();
        PostalCommuneBean codePostalCommuneE2 = new PostalCommuneBean();
        codePostalCommuneE2.setCommune("commune_TU_@2");
        adresseE2.setCodePostalCommune(codePostalCommuneE2);
        e2.setAdresse(adresseE2);
        etablissementsBean.add(e2);

        entreprise.setEtablissements(etablissementsBean);

        // Init Interface
        IDialogueCessation xml = XF.newObject(IDialogueCessation.class);

        this.mapper.mapper(bean, xml);

        assertNotNull(xml);
        assertEquals(NUM_DOSSIER_TU, xml.getNumeroDossier());
        assertEquals(NUM_DOSSIER_TU, xml.getEntreprise().getDirigeants()[0].getNumeroDossier());

        IEtablissementCessation[] etablissements = xml.getEntreprise().getEtablissements();
        Assert.assertNotNull(etablissements);
        Assert.assertEquals(2, etablissements.length);
        assertEquals("commune_TU_@1", etablissements[0].getAdresse().getCodePostalCommune().getCommune());

        assertEquals("commune_TU_@2", etablissements[1].getAdresse().getCodePostalCommune().getCommune());

    }

    /**
     * Test mapper i formalite cessation formalite cessation interface to bean.
     *
     * @throws XmlFieldParsingException
     *             le xml field parsing exception
     * @throws FileNotFoundException
     *             le file not found exception
     */
    @Test
    public void testMapperIFormaliteCessationFormaliteCessationInterfaceToBean() throws XmlFieldParsingException, FileNotFoundException {

        FileInputStream fileInputStream = new FileInputStream("src/test/resources/formalitesXml/formalitesCessation.xml");
        IDialogueCessation xml = XF.xmlToObject(fileInputStream, IDialogueCessation.class);

        // Init Bean
        DialogueCessationBean bean = new DialogueCessationBean();
        assertNull(bean.getNumeroDossier());
        this.mapper.mapper(xml, bean);

        assertEquals("DOSSIER_M93010025216", bean.getNumeroDossier());
        assertEquals("nomDirigeant1", bean.getEntreprise().getDirigeants().get(0).getPpNomNaissance());
        assertEquals("nomDirigeant2", bean.getEntreprise().getDirigeants().get(1).getPpNomNaissance());

        List<EtablissementCessationBean> etablissements = bean.getEntreprise().getEtablissements();
        Assert.assertNotNull(etablissements);
        Assert.assertEquals(2, etablissements.size());
        assertEquals("commune_TU_@1", etablissements.get(0).getAdresse().getCodePostalCommune().getCommune());
        assertEquals("commune_TU_@2", etablissements.get(1).getAdresse().getCodePostalCommune().getCommune());
    }

}
