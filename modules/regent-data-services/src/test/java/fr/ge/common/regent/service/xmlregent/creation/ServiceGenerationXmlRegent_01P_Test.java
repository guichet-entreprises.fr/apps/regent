/**
 * 
 */
package fr.ge.common.regent.service.xmlregent.creation;

import static org.fest.assertions.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import fr.ge.common.regent.bean.xml.v2008.IXmlGlobal2008;
import fr.ge.common.regent.bean.xml.v2016.IXmlGlobal2016;
import fr.ge.common.regent.bean.xml.v2016.JQU;
import fr.ge.common.regent.service.xmlregent.AbstractServiceGenerationXmlRegentTest;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * Classe de tests d'XML regent pour les évènement de type 01P en création.
 * 
 * @author $Author: HZITOUNI $
 * @version $Revision: 0 $
 */
public class ServiceGenerationXmlRegent_01P_Test extends AbstractServiceGenerationXmlRegentTest {

    private static final String TYPE_FORMALITE = "creation";
    private static final String TYPE_EVENEMENT = "01P";

    /**
     * Test check balise c37.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseC37() throws TechnicalException {
        IXmlGlobal2008 result = getXmlRegent2008(TYPE_EVENEMENT, TYPE_FORMALITE, "M75010002740", "G7501", "TDR", Arrays.asList("M7501", "G7501"),
                AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        Assert.assertEquals("La balise C37.13 ", "PARIS", result.getServiceApplicatif()[0].getLiasse()[0].getService()[0].getADF()[0].getC37()[0].getC3713());
    }

    /**
     * Test check balise c37_ x path.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseC37_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "M75010002740", "G7501", "TDR", Arrays.asList("M7501", "G7501"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise C37.13", "PARIS", result);
    }

    /**
     * JIRA 2711 / Selenium 115.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseE70AgentCom() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "G25010000372", "G2501", "CFE+TDR", Arrays.asList("G2501"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E70";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise E70", "mercerie, fil aiguilles, immobilier", result);
    }

    /**
     * JIRA 2711 / Selenium 115.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseE71AgentCom() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "G25010000372", "G2501", "CFE+TDR", Arrays.asList("G2501"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E71";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise E71", "mercerie", result);
    }

    /**
     * JIRA 2711 / Selenium 115.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseU21AgentCom() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "G25010000372", "G2501", "CFE+TDR", Arrays.asList("G2501"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U21";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise U21", "mercerie, fil aiguilles, immobilier", result);
    }

    /**
     * JIRA 2711 / Selenium 115.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseE70SansAgentCom() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "M75010002740", "G7501", "TDR", Arrays.asList("M7501", "G7501"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E70";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise E70", "Ventes d'article à Paris, Fabrication de bijoux artisanaux", result);
    }

    @Test
    public void testCheckBaliseU31RegimeFiscal() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "X89010000101", "X8901", "CFE", Arrays.asList("X8901"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU";

        String result = evaluteXpathFromXml(xpath, xml);
        Assert.assertEquals("La premiere balse  U31", "118", result.substring(0, 3));
        Assert.assertEquals("La seconde  balise  U31", "116", result.substring(3, 6));

    }

    @Test
    public void testCheckBaliseU331RegimeFiscalEtablissement1() throws TechnicalException {

        IXmlGlobal2008 result = getXmlRegent2008(TYPE_EVENEMENT, TYPE_FORMALITE, "X89010000101", "X8901", "CFE", Arrays.asList("X8901"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        Assert.assertEquals("La balise U33.1 ", "315", result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getRFU()[0].getU33()[0].getU331());

        Assert.assertEquals("La balise U33.1 ", "310", result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getRFU()[0].getU33()[1].getU331());

    }

    /**
     * Test check balise u70_ x path.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseU70_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C78010003976", "C7801", "CFE", Arrays.asList("C7801", "G7801"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U70";

        String result = evaluteXpathFromXml(xpath, xml);
        Assert.assertEquals("La balise U70", "116", result);
    }

    /**
     * Test check balise u72_ x path.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseU72_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C78010003976", "C7801", "CFE", Arrays.asList("C7801", "G7801"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U72/U72.1";

        String result = evaluteXpathFromXml(xpath, xml);
        Assert.assertEquals("La balise U72", "310", result);
    }

    /**
     * Test check balise u71_ x path.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseU71_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C78010003976", "C7801", "CFE", Arrays.asList("C7801", "G7801"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U71";

        String result = evaluteXpathFromXml(xpath, xml);
        Assert.assertEquals("La balise U71", "211", result);
    }

    /**
     * Test check balise A30.2.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseA30_2() throws TechnicalException {
        IXmlGlobal2008 result = getXmlRegent2008(TYPE_EVENEMENT, TYPE_FORMALITE, "C65010005148", "C6501", "CFE", Arrays.asList("G6502", "C6501"),
                AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        Assert.assertEquals("La balise A30.2 ", "PARIS", result.getServiceApplicatif()[0].getLiasse()[0].getPersonnePhysique()[0].getGCS()[0].getJES()[0].getA30()[0].getA302());
    }

    /**
     * Test check balise P03.3.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseP03_3() throws TechnicalException {
        IXmlGlobal2008 result = getXmlRegent2008(TYPE_EVENEMENT, TYPE_FORMALITE, "M75010004605", "G7501", "TDR", Arrays.asList("G7501", "M7501"),
                AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        Assert.assertEquals("La balise P03.3 ", "FEDERATION DE RUSSIE", result.getServiceApplicatif()[0].getLiasse()[0].getPersonnePhysique()[0].getICP()[0].getP03()[0].getP033());
        Assert.assertNotEquals("La balise P03.3 ", "RUSSIE", result.getServiceApplicatif()[0].getLiasse()[0].getPersonnePhysique()[0].getICP()[0].getP03()[0].getP033());
    }

    /**
     * Test check balise C40.3.13.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseC40_3_13() throws TechnicalException {
        IXmlGlobal2008 result = getXmlRegent2008(TYPE_EVENEMENT, TYPE_FORMALITE, "C65010005148", "G6502", "TDR", Arrays.asList("C6501", "G6502"),
                AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        Assert.assertEquals("La balise C40.3.13 ", "Los Angeles", result.getServiceApplicatif()[0].getLiasse()[0].getService()[0].getSIF()[0].getC40()[0].getC403()[0].getC40313());

        Assert.assertNotEquals("La balise C40.3.13 ", ".", result.getServiceApplicatif()[0].getLiasse()[0].getService()[0].getSIF()[0].getC40()[0].getC403()[0].getC40313());
    }

    /**
     * Test check balise u71_ x path.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseP01_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C05010004173", "C0501", "CFE", Arrays.asList("C0501", "G0501"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.13";

        String result = evaluteXpathFromXml(xpath, xml);
        Assert.assertEquals("La balise P04.13", "BRIANCON", result);
    }

    /**
     * Test check balise P76.3.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseP763_XPath() throws TechnicalException {
        IXmlGlobal2008 result = getXmlRegent2008(TYPE_EVENEMENT, TYPE_FORMALITE, "U95010000242", "G9501", "CFE", Arrays.asList("G9501", "U9501"),
                AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        Assert.assertEquals("La balise P76.3 ", "lieu immat", result.getServiceApplicatif()[0].getLiasse()[0].getPersonnePhysique()[0].getDAP()[0].getP76()[0].getP763());
    }

    /**
     * Test check balise P76.4.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseP764_XPath() throws TechnicalException {
        IXmlGlobal2008 result = getXmlRegent2008(TYPE_EVENEMENT, TYPE_FORMALITE, "U95010000242", "U9501", "CFE", Arrays.asList("U9501", "G9501"),
                AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        Assert.assertEquals("La balise P76.4 ", "676767676", result.getServiceApplicatif()[0].getLiasse()[0].getPersonnePhysique()[0].getDAP()[0].getP76()[0].getP764());
    }

    /**
     * Test check balise C10.2 x path.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseC102XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C75010000039", "C7501", "CFE", Arrays.asList("C7501", "G7501"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2";
        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise C10.2", "2016-07-14", result);
    }

    /**
     * Test check balise E73.111 Mantis 1481.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseE73Mantis1481() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "M28010000022", "M2801", "CFE+TDR", Arrays.asList("M2801"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E73/E73.11/E73.111";
        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise E73.111", "", result);
    }

    /**
     * Test check balise A30.1 Mantis 1481.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseA30Mantis1479() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C07010002999", "C0701", "CFE", Arrays.asList("G0702", "C0701"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/JES/A30/A30.1";
        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertNotNull(result);
        Assert.assertEquals("La balise A30.1", "01053", result);
    }

    /**
     * Vérifie que la balise A62.1 n'est pas renseignée (pas de date de
     * naissance enregistrée si le numéro de sécurité sociale est renseigné).
     * JIRA SONIC-3131.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseA621Jira3131() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "X89010000101", "X8901", "CFE", Arrays.asList("X8901"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/Regent/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ADS/A62/A62.1";
        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertNotNull(result);
        Assert.assertEquals("La balise A62.1", "", result);
    }

    /**
     * Test check balise c45.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseC45() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C78010000211", "C7801", "CFE", Arrays.asList("C7801", "G7801"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);

        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C45";
        String result = evaluteXpathFromXml(xpath, xml);

        assertThat(result).isNotNull();
        assertThat(result).isEqualTo("O");

        String xml2 = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C78010000212", "G7801", "TDR", Arrays.asList("C7801", "G7801"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);

        String result2 = evaluteXpathFromXml(xpath, xml2);

        assertThat(result2).isNotNull();
        assertThat(result2).isEqualTo("N");
    }

    /**
     * Test check balise P07.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseP07() throws TechnicalException {

        // cas de test O
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C78010000211", "C7801", "CFE", Arrays.asList("C7801", "G7801"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);

        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P07";
        String result = evaluteXpathFromXml(xpath, xml);

        assertThat(result).isNotNull();
        assertThat(result).isEqualTo("O");

        // cas de test N
        String xml2 = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C05010004173", "C0501", "CFE", Arrays.asList("C0501", "G0501"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);

        String result2 = evaluteXpathFromXml(xpath, xml2);

        assertThat(result2).isNotNull();
        assertThat(result2).isEqualTo("N");
    }

    /**
     * Test check balise C00 x path.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseC00_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "U34020000253", "U3402", "CFE", Arrays.asList("U3402"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C00";

        String result = evaluteXpathFromXml(xpath, xml);
        Assert.assertEquals("La balise C00", "", result);
    }

    /**
     * Test check balise C07 x path.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseC07_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "U34020000253", "U3402", "CFE", Arrays.asList("U3402"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C07";

        String result = evaluteXpathFromXml(xpath, xml);
        Assert.assertEquals("La balise C07", "O", result);
    }

    /**
     * Test check balise C08 x path.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseC08_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "U34020000253", "U3402", "CFE", Arrays.asList("U3402"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C08";

        String result = evaluteXpathFromXml(xpath, xml);
        Assert.assertEquals("La balise C08", "", result);
    }

    /**
     * Test check balise C09 x path.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseC09_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "U34020000253", "U3402", "CFE", Arrays.asList("U3402"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C09";

        String result = evaluteXpathFromXml(xpath, xml);
        Assert.assertEquals("La balise C09", "", result);
    }

    /**
     * SONIC-3469 : [REGENT-2016] Supprimer la balise A71 de la rubrique SAS.
     * Test check balise A71 supprimée. Regent 2016.02.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseA71() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "M29010000227", "M2901", "CFE+TDR", Arrays.asList("M2901", "G2903"),
                AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SAS/A71";

        String result = evaluteXpathFromXml(xpath, xml);
        Assert.assertEquals("La balise A71", "", result);
    }

    /**
     * Test check balise U42 x path.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseU42_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "M28010000275", "M2801", "CFE", Arrays.asList("M2801"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OVU/U42";

        String result = evaluteXpathFromXml(xpath, xml);
        Assert.assertEquals("La balise U42", "O", result);

        xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C78010000211", "C7801", "CFE", Arrays.asList("C7801", "G7801"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);
        xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OVU/U42";
        result = evaluteXpathFromXml(xpath, xml);
        Assert.assertEquals("La balise U42", "", result);
    }

    /**
     * Test check Balise A24 regent 2008.<br/>
     * SONIC-3474 :[REGENT-2016] Modifier la balise A24 de la rubrique SNS
     * (A24.1, A24.5, A24.6).
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseA24Version2008() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C05010004173", "C0501", "CFE", Arrays.asList("C0501", "G0501"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A24/A24.1";

        String result = evaluteXpathFromXml(xpath, xml);
        Assert.assertEquals("La balise A24.1", "", result);

        String xpath2 = "/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A24/A24.5";

        String result2 = evaluteXpathFromXml(xpath2, xml);
        Assert.assertEquals("La balise A24.5", "", result2);

        String xpath3 = "/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A24/A24.6";

        String result3 = evaluteXpathFromXml(xpath3, xml);
        Assert.assertEquals("La balise A24.6", "", result3);
    }

    /**
     * Test check Balise A24 regent 2016.<br/>
     * SONIC-3474 :[REGENT-2016] Modifier la balise A24 de la rubrique SNS
     * (A24.1, A24.5, A24.6).
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseA24Version2016() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C05010004173", "C0501", "CFE", Arrays.asList("C0501", "G0501"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A24/A24.1";

        String result = evaluteXpathFromXml(xpath, xml);
        Assert.assertEquals("La balise A24.1", "N", result);

        String xpath2 = "/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A24/A24.5";

        String result2 = evaluteXpathFromXml(xpath2, xml);
        Assert.assertEquals("La balise A24.5", "", result2);

        String xpath3 = "/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A24/A24.6";

        String result3 = evaluteXpathFromXml(xpath3, xml);
        Assert.assertEquals("La balise A24.6", "", result3);
    }

    /**
     * Test check balise A67 x path.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseA67V2008XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "M28010000284", "M2801", "CFE", Arrays.asList("M2801", "G2801"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);

        List<String> xPaths = new ArrayList<String>();
        xPaths.add("/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ADS/A67/A67.1");
        xPaths.add("/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ADS/A67/A67.2");
        xPaths.add("/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ADS/A67/A67.3");
        xPaths.add("/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ADS/A67/A67.4");

        for (final String xPath : xPaths) {
            final String tagName = xPath.substring(xPath.lastIndexOf("/") + 1, xPath.length());
            final String result = evaluteXpathFromXml(xPath, xml);
            Assert.assertNotEquals("La balise " + tagName, "", result);
        }
    }

    /**
     * Test check balise A67 x path.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseA67V2016XPath() throws TechnicalException {

        List<String> xPaths = new ArrayList<String>();
        xPaths.add("/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ADS/A67/A67.1");
        xPaths.add("/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ADS/A67/A67.2");
        xPaths.add("/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ADS/A67/A67.3");
        xPaths.add("/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ADS/A67/A67.4");

        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "M28010000285", "M2801", "CFE", Arrays.asList("M2801", "G2801"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);

        for (final String xPath : xPaths) {
            final String tagName = xPath.substring(xPath.lastIndexOf("/") + 1, xPath.length());
            final String result = evaluteXpathFromXml(xPath, xml);
            Assert.assertEquals("La balise " + tagName, "", result);
        }
    }

    /**
     * Test balise ISP.
     * 
     * @throws TechnicalException
     */
    @Test
    public void testCheckBaliseISPVersion2016() throws TechnicalException {
        IXmlGlobal2016 result = getXmlRegent2016(TYPE_EVENEMENT, TYPE_FORMALITE, "M95010000316", "M9501", "CFE+TDR", Arrays.asList("M9501"),
                AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);
        Assert.assertEquals("La balise P90.1.1", "A", result.getServiceApplicatif()[0].getLiasse()[0].getPersonnePhysique()[0].getISP()[0].getP90()[0].getP901()[0].getP9011());
        Assert.assertEquals("La balise P90.1.2", "renonciation RP", result.getServiceApplicatif()[0].getLiasse()[0].getPersonnePhysique()[0].getISP()[0].getP90()[0].getP901()[0].getP9012());
        Assert.assertEquals("La balise P90.2.1", "C", result.getServiceApplicatif()[0].getLiasse()[0].getPersonnePhysique()[0].getISP()[0].getP90()[0].getP902()[0].getP9021());
        Assert.assertEquals("La balise P90.2.2", "déclarationautre", result.getServiceApplicatif()[0].getLiasse()[0].getPersonnePhysique()[0].getISP()[0].getP90()[0].getP902()[0].getP9022());
    }

    /**
     * Test balise JQU.
     * 
     * @throws TechnicalException
     */
    @Test
    public void testCheckBaliseJQUVersion2016() throws TechnicalException {
        IXmlGlobal2016 result = getXmlRegent2016(TYPE_EVENEMENT, TYPE_FORMALITE, "M95010000316", "M9501", "CFE+TDR", Arrays.asList("M9501"),
                AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);

        JQU jqu0 = result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getJQU()[0];
        JQU jqu1 = result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getJQU()[1];
        JQU jqu2 = result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getJQU()[2];
        JQU jqu3 = result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getJQU()[3];
        JQU jqu4 = result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getJQU()[4];

        // Vérifier la balise U60
        Assert.assertEquals("La balise U60 ", "2", jqu0.getU60());
        Assert.assertEquals("La balise U60 ", "1", jqu1.getU60());
        Assert.assertEquals("La balise U60 ", "1", jqu2.getU60());
        Assert.assertEquals("La balise U60 ", "1", jqu3.getU60());
        Assert.assertEquals("La balise U60 ", "1", jqu4.getU60());

        // Vérifier la balise U61
        Assert.assertEquals("La balise U61.1 ", "O", jqu0.getU61()[0].getU611());
        Assert.assertEquals("La balise U61.2 ", "Activité soumise à qualification 1/5", jqu0.getU61()[0].getU612());

        Assert.assertEquals("La balise U61.1 ", "O", jqu1.getU61()[0].getU611());
        Assert.assertEquals("La balise U61.2 ", "Activité soumise à qualification 2/5", jqu1.getU61()[0].getU612());

        Assert.assertEquals("La balise U61.1 ", "O", jqu2.getU61()[0].getU611());
        Assert.assertEquals("La balise U61.2 ", "Activité soumise à qualification 3/5", jqu2.getU61()[0].getU612());

        Assert.assertEquals("La balise U61.1 ", "O", jqu3.getU61()[0].getU611());
        Assert.assertEquals("La balise U61.2 ", "Activité soumise à qualification 4/5", jqu3.getU61()[0].getU612());

        Assert.assertEquals("La balise U61.1 ", "O", jqu4.getU61()[0].getU611());
        Assert.assertEquals("La balise U61.2 ", "Activité soumise à qualification 5/5", jqu4.getU61()[0].getU612());

        // Vérifier la balise U62
        Assert.assertTrue(jqu0.getU62().length == 0);

        Assert.assertEquals("La balise U62.1 ", "1", jqu1.getU62()[0].getU621());
        Assert.assertEquals("La balise U62.2 ", null, jqu1.getU62()[0].getU622());

        Assert.assertEquals("La balise U62.1 ", "3", jqu2.getU62()[0].getU621());
        Assert.assertEquals("La balise U62.2 ", null, jqu2.getU62()[0].getU622());

        Assert.assertEquals("La balise U62.1 ", "4", jqu3.getU62()[0].getU621());
        Assert.assertEquals("La balise U62.2 ", null, jqu3.getU62()[0].getU622());

        Assert.assertEquals("La balise U62.1 ", "5", jqu4.getU62()[0].getU621());
        Assert.assertEquals("La balise U62.2 ", "qualité autre", jqu4.getU62()[0].getU622());

        // Vérifier la balise U62.3
        Assert.assertEquals("La balise U62.3.1 ", "nom", jqu3.getU62()[0].getU623()[0].getU6231());
        Assert.assertEquals("La balise U62.3.2 ", "usage", jqu3.getU62()[0].getU623()[0].getU6232());
        Assert.assertEquals("La balise U62.3.4 ", "2017-02-01", jqu3.getU62()[0].getU623()[0].getU6234());
        Assert.assertEquals("La balise U62.3.5 ", "01001", jqu3.getU62()[0].getU623()[0].getU6235());
        Assert.assertEquals("La balise U62.3.6 ", null, jqu3.getU62()[0].getU623()[0].getU6236());
        Assert.assertEquals("La balise U62.3.7 ", "L' ABERGEMENT-CLEMENCIAT", jqu3.getU62()[0].getU623()[0].getU6237());

        Assert.assertEquals("La balise U62.3.1 ", "nomAutre", jqu4.getU62()[0].getU623()[0].getU6231());
        Assert.assertEquals("La balise U62.3.2 ", "usageAutre", jqu4.getU62()[0].getU623()[0].getU6232());
        Assert.assertEquals("La balise U62.3.4 ", "2017-02-01", jqu4.getU62()[0].getU623()[0].getU6234());
        Assert.assertEquals("La balise U62.3.5 ", "99247", jqu4.getU62()[0].getU623()[0].getU6235());
        Assert.assertEquals("La balise U62.3.6 ", "EMIRATS ARABES UNIS", jqu4.getU62()[0].getU623()[0].getU6236());
        Assert.assertEquals("La balise U62.3.7 ", "ville", jqu4.getU62()[0].getU623()[0].getU6237());
    }

    @Test
    public void testCheckBaliseC40_V2008_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C07022597292", "C0702", "CFE", Arrays.asList("C0702", "G0702"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.13";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise C40.3.13", ".", result);
    }

    @Test
    public void testCheckBaliseC40_V2016_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C07022597292", "C0702", "CFE", Arrays.asList("C0702", "G0702"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.13";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise C40.3.13", ".", result);
    }

    /**
     * [GUB-67] - Génération d'un XML Regent vide pour un agent commercial en
     * secteur artisanal
     * 
     * @throws TechnicalException
     */
    @Test
    public void testCheckGUB67_2008() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C07022597292", "M7501", "CFE+TDR", Arrays.asList("M7501", "G7501"),
                AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.13";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise C40.3.13", ".", result);
    }

    /**
     * [GUB-67] - Génération d'un XML Regent vide pour un agent commercial en
     * secteur artisanal
     * 
     * @throws TechnicalException
     */
    @Test
    public void testCheckGUB67_2016() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C07022597292", "M7501", "CFE+TDR", Arrays.asList("M7501", "G7501"),
                AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.13";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise C40.3.13", ".", result);
    }

    /**
     * [GUB-186] dossier en erreur : ayant-droit né à l'étranger
     * 
     * @throws TechnicalException
     */
    @Test
    public void testCheckGUB186_2008() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "U01010750894", "U2401", "CFE", Arrays.asList("U2401"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);

        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ADS/A62/A62.2";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise A62.2", "99328", result);
    }

    @Test
    public void testCheckBaliseC10_2_V2008_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C05010004173", "C0501", "CFE", Arrays.asList("C0501"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise C10.2", "2007-08-01", result);
    }

    @Test
    public void testCheckBaliseC10_2_V2016_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C05010004173", "C0501", "CFE", Arrays.asList("C0501"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise C10.2", "2007-08-01", result);
    }

    /**
     * SONIC-3741
     * 
     * [SUPPORT][PROD][GUB-264][xml regent] [prod] [création] le xml regent ne
     * prévoit pas le cas des dossiers agricoles et agent co pour la balise
     * P76.2
     * 
     * Cas Agricole: precedentEIRLRegistre = ra.
     * 
     * V2008
     * 
     * @throws TechnicalException
     */
    @Test
    public void testCheckBaliseP76_2_GUB264_V2008_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "X19010000238", "X1901", "CFE", Arrays.asList("X1901"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P76/P76.2";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise P76.2", "5", result);
    }

    /**
     * SONIC-3741
     * 
     * [SUPPORT][PROD][GUB-264][xml regent] [prod] [création] le xml regent ne
     * prévoit pas le cas des dossiers agricoles et agent co pour la balise
     * P76.2
     * 
     * Cas Agricole: precedentEIRLRegistre = ra.
     * 
     * V2016
     * 
     * @throws TechnicalException
     */
    @Test
    public void testCheckBaliseP76_2_GUB264_V2016_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "X19010000238", "X1901", "CFE", Arrays.asList("X1901"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P76/P76.2";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise P76.2", "5", result);
    }

    /**
     * SONIC-3741
     * 
     * [SUPPORT][PROD][GUB-264][xml regent] [prod] [création] le xml regent ne
     * prévoit pas le cas des dossiers agricoles et agent co pour la balise
     * P76.2
     * 
     * Cas Agent Co : precedentEIRLRegistre = rsac.
     * 
     * V2008
     * 
     * @throws TechnicalException
     */
    @Test
    public void testCheckBaliseP76_2_GUB264_V2008_XPath2() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "G25010000283", "G2501", "CFE+TDR", Arrays.asList("G2501"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P76/P76.2";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise P76.2", "4", result);
    }

    /**
     * SONIC-3741
     * 
     * [SUPPORT][PROD][GUB-264][xml regent] [prod] [création] le xml regent ne
     * prévoit pas le cas des dossiers agricoles et agent co pour la balise
     * P76.2
     * 
     * Cas Agent Co : precedentEIRLRegistre = rsac.
     * 
     * V2016.
     * 
     * @throws TechnicalException
     */
    @Test
    public void testCheckBaliseP76_2_GUB264_V2016_XPath2() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "G25010000283", "G2501", "CFE+TDR", Arrays.asList("G2501"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P76/P76.2";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise P76.2", "4", result);
    }
}
