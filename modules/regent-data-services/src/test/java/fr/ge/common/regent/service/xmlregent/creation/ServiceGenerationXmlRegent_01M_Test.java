/**
 * 
 */
package fr.ge.common.regent.service.xmlregent.creation;

import static org.fest.assertions.Assertions.assertThat;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import fr.ge.common.regent.service.xmlregent.AbstractServiceGenerationXmlRegentTest;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * Classe de tests d'XML regent pour les évènement de type 01P en création.
 *
 * @author $Author: HZITOUNI $
 * @version $Revision: 0 $
 */

public class ServiceGenerationXmlRegent_01M_Test extends AbstractServiceGenerationXmlRegentTest {

    private static final String TYPE_FORMALITE = "creation";
    private static final String TYPE_EVENEMENT = "01M";

    /**
     * Test check balise M55.4_ x path.
     *
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseC37_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C78010001301", "G7801", "TDR", Arrays.asList("C7801", "G7801"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55/M55.4";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise M55.4", "G7801", result);
    }

    /**
     * Scénario 20 : Balise ICD ne doit pas être présente.
     *
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseICDScenario20() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "M28010004115", "G2801", "TDR", Arrays.asList("M2801", "G2801"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/Regent/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/ICD";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise ICD", "", result);
    }

    /**
     * Test check balise M21.1_ x path.
     *
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseM211() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C75010005376", "C7501", "CFE", Arrays.asList("C7501", "G7501"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FJM/M21/M21.1";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise M21.1", "5720", result);
    }

    /**
     * Test check balise M55.4_ x path.
     *
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseA426XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "X89010004500", "X8901", "CFE", Arrays.asList("X8901", "G8901"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A24/A24.6";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise A24.6", "89", result);
    }

    /**
     * Test check balise E80.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseE80() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C78010001301", "G7801", "TDR", Arrays.asList("C7801", "G7801"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);

        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E80";
        String result = evaluteXpathFromXml(xpath, xml);

        assertThat(result).isNotNull();
        assertThat(result).isEqualTo("");

        String xml2 = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C78010001301", "G7801", "TDR", Arrays.asList("C7801", "G7801"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);

        String result2 = evaluteXpathFromXml(xpath, xml2);

        assertThat(result2).isNotNull();
        assertThat(result2).isEqualTo("Société de domiciliation;Galerie d'art");
    }

    /**
     * Test check balise E76.2.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBalisE76_2() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C78010001301", "G7801", "TDR", Arrays.asList("C7801", "G7801"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);

        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E76/E76.2";
        String result = evaluteXpathFromXml(xpath, xml);

        assertThat(result).isNotNull();
        assertThat(result).isEqualTo("");

        String xml2 = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C78010001301", "G7801", "TDR", Arrays.asList("C7801", "G7801"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);

        String result2 = evaluteXpathFromXml(xpath, xml2);

        assertThat(result2).isNotNull();
        assertThat(result2).isEqualTo(".");
    }

    /**
     * Test check Balise A24 regent 2016.<br/>
     * SONIC-3474 :[REGENT-2016] Modifier la balise A24 de la rubrique SNS
     * (A24.1, A24.5, A24.6).
     * 
     * @throws TechnicalException
     */
    @Test
    public void testCheckBaliseA24Version2016() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "X89010004500", "X8901", "CFE", Arrays.asList("X8901", "G8901"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A24/A24.1";

        String result = evaluteXpathFromXml(xpath, xml);
        Assert.assertEquals("La balise A24.1", "O", result);

        String xpath2 = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A24/A24.5";

        String result2 = evaluteXpathFromXml(xpath2, xml);
        Assert.assertEquals("La balise A24.5", "", result2);

        String xpath3 = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A24/A24.6";

        String result3 = evaluteXpathFromXml(xpath3, xml);
        Assert.assertEquals("La balise A24.6", "", result3);
    }

    /**
     * Test check Balise A24 regent 2008.<br/>
     * SONIC-3474 :[REGENT-2016] Modifier la balise A24 de la rubrique SNS
     * (A24.1, A24.5, A24.6).
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseA24Version2008() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "X89010004500", "X8901", "CFE", Arrays.asList("X8901", "G8901"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A24/A24.1";

        String result = evaluteXpathFromXml(xpath, xml);
        Assert.assertEquals("La balise A24.1", "", result);

        String xpath2 = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A24/A24.5";

        String result2 = evaluteXpathFromXml(xpath2, xml);
        Assert.assertEquals("La balise A24.5", "", result2);

        String xpath3 = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A24/A24.6";

        String result3 = evaluteXpathFromXml(xpath3, xml);
        Assert.assertEquals("La balise A24.6", "89", result3);
    }

    @Test
    public void testGub121() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "M59012291882", "M5901", "CFE+TDR", Arrays.asList("M5901", "G5910"),
                AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/DIU/U50/U50.1";
        String result = evaluteXpathFromXml(xpath, xml);
        Assert.assertEquals("La balise U50.1", "1", result);

    }

    /**
     * GUB 50 . Test check balise R02 path.
     *
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseR02_V2008_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C75012212577", "X4701", "CFE", Arrays.asList("C7501", "C7501"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante/ICR/R02/R02.8";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise R02.8", ".", result);
    }

    @Test
    public void testCheckBaliseR02_V2016_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C75012212577", "X4701", "CFE", Arrays.asList("C7501", "C7501"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante/ICR/R02/R02.8";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise R02.8", ".", result);
    }

    @Test
    public void testCheckBaliseD04_V2008_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C78012357485", "C7801", "CFE", Arrays.asList("C7801", "G7801"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/IPD/D04/D04.8";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise D04.8", ".", result);
    }

    @Test
    public void testCheckBaliseD04_V2016_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C78012357485", "C7801", "CFE", Arrays.asList("C7801", "G7801"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/IPD/D04/D04.8";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise D04.8", ".", result);
    }

    @Test
    public void testCheckBaliseC10_2_V2008_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C05010004173", "C0501", "CFE", Arrays.asList("C0501"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise C10.2", "2007-08-01", result);
    }

    @Test
    public void testCheckBaliseC10_2_V2016_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C05010004173", "C0501", "CFE", Arrays.asList("C0501"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise C10.2", "2007-08-01", result);
    }

    @Test
    public void testCheck_REGENT_GUB336_V2008_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C93013035401", "C9301", "CFE", Arrays.asList("C9301"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/Emetteur";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise Emetteur", "Z1611", result);
    }
}