package fr.ge.common.regent.persistance.dao.referentiel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import fr.ge.common.referentiel.bean.modele.ESupraDepartements;
import fr.ge.common.referentiel.dao.SupraDepartementsDao;
import fr.ge.common.regent.util.test.AbstractSpringTest;

/**
 * Le Class SupraDepartementsDaoTest.
 */
public class SupraDepartementsDaoTest extends AbstractSpringTest {

    /** Le supra departements dao. */
    @Autowired
    private SupraDepartementsDao supraDepartementsDao;

    /**
     * Test chercher prefecture par code commune.
     */
    @Test
    @Transactional(readOnly = false, value = "regent")
    public void testChercherPrefectureParCodeCommune() {
        String codeCommune = "49007";
        ESupraDepartements chercherPrefectureParCodeCommune = supraDepartementsDao.chercherPrefectureParCodeCommune(codeCommune);

        assertNotNull(chercherPrefectureParCodeCommune);
        assertEquals("49", chercherPrefectureParCodeCommune.getCodeDepartement());
    }

}
