package fr.ge.common.regent.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import fr.ge.common.regent.bean.modele.referentiel.ENormeRubrique;
import fr.ge.common.regent.persistance.dao.referentiel.NormeRubriqueDao;
import fr.ge.common.regent.service.ServiceRubriquesManagement;
import fr.ge.common.regent.util.test.AbstractSpringTest;
import fr.ge.common.utils.exception.TechnicalException;

@Transactional(readOnly = true, value = "regent")
@ContextConfiguration(locations = { "classpath:spring/service-mock.xml" })
public class ServiceRubriquesManagementImplTest extends AbstractSpringTest {

    @Autowired
    private NormeRubriqueDao normeRubriqueDao;

    @Autowired
    private ServiceRubriquesManagement serviceRubriqueManagmentImpl;

    @Test
    public void testGetRubriquesPathByEvenementAndVersion() throws Exception {
        final List<ENormeRubrique> listTagsByEvenement = new ArrayList<>();
        final List<String> listeEvenements = new ArrayList<>();
        listeEvenements.add("10P");
        ENormeRubrique normRubrique = new ENormeRubrique();
        normRubrique.setId(10000);
        normRubrique.setCondition("VRAI");
        normRubrique.setLibelle("RUBRIQUE A");
        normRubrique.setPath("/path/rubriqueA:Emplacement");
        normRubrique.setRubrique("RUBRIQUE A");
        normRubrique.setGroupe("G001");
        normeRubriqueDao.save(normRubrique);

        ENormeRubrique normRubrique2 = new ENormeRubrique();
        normRubrique2.setId(20000);
        normRubrique2.setCondition("VRAI");
        normRubrique2.setLibelle("RUBRIQUE B");
        normRubrique2.setPath("/path/rubriqueB:Emplacement");
        normRubrique2.setRubrique("RUBRIQUE B");
        normRubrique2.setGroupe("G002");
        normeRubriqueDao.save(normRubrique2);

        listTagsByEvenement.add(normRubrique);
        listTagsByEvenement.add(normRubrique2);

        when(this.normeRubriqueDao.getRubriquesPathByEvenementAndVersion(anyString(), anyList())).thenReturn(listTagsByEvenement);
        final List<ENormeRubrique> listTags = this.serviceRubriqueManagmentImpl.getRubriquesPathByEvenementAndVersion("V2008.11", listeEvenements);

        assertNotNull(listTags);
        assertEquals(2, listTags.size());
        assertThat(listTags.get(0).getId()).isEqualTo(10000);
        assertThat(listTags.get(0).getCondition()).isEqualTo("VRAI");
        assertThat(listTags.get(0).getLibelle()).isEqualTo("RUBRIQUE A");
        assertThat(listTags.get(0).getPath()).isEqualTo("/path/rubriqueA:Emplacement");
        assertThat(listTags.get(0).getRubrique()).isEqualTo("RUBRIQUE A");
        assertThat(listTags.get(0).getGroupe()).isEqualTo("G001");

        assertThat(listTags.get(1).getId()).isEqualTo(20000);
        assertThat(listTags.get(1).getCondition()).isEqualTo("VRAI");
        assertThat(listTags.get(1).getLibelle()).isEqualTo("RUBRIQUE B");
        assertThat(listTags.get(1).getPath()).isEqualTo("/path/rubriqueB:Emplacement");
        assertThat(listTags.get(1).getRubrique()).isEqualTo("RUBRIQUE B");
        assertThat(listTags.get(1).getGroupe()).isEqualTo("G002");
    }

    @Test(expected = TechnicalException.class)
    public void testGetRubriquesPathByEvenementAndVersionKO() throws Exception {

        final List<ENormeRubrique> listTags = this.serviceRubriqueManagmentImpl.getRubriquesPathByEvenementAndVersion(null, null);
    }

}
