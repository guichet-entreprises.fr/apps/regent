package fr.ge.common.regent.persistance.dao.referentiel;

import java.util.List;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import fr.ge.common.referentiel.dao.SupraCFECommuneDao;
import fr.ge.common.regent.util.test.AbstractSpringTest;

/**
 * Le Class SupraCommunesDaoTest.
 */
public class SupraCFECommunesDaoTest extends AbstractSpringTest {

    /** Le dao à tester. */
    @Autowired
    private SupraCFECommuneDao dao;

    /**
     * Test getListeCodeCommuneByCodeCfeAndTypeCFE OK.
     */
    @Test
    @Transactional(readOnly = true, value = "regent")
    public void test_getListeCodeCommuneByCodeCfeAndTypeCFE_OK() {
        String codeCFE = "M3501";
        String typeCFE = "CMA";
        List<String> codeCommunes = dao.getListeCodeCommuneByCodeCfeAndTypeCFE(codeCFE, typeCFE);
        Assertions.assertThat(codeCommunes).isNotNull();
        Assertions.assertThat(!codeCommunes.isEmpty()).isTrue();

        codeCFE = "U3501";
        typeCFE = "URSSAF";
        codeCommunes = dao.getListeCodeCommuneByCodeCfeAndTypeCFE(codeCFE, typeCFE);
        Assertions.assertThat(codeCommunes).isNotNull();
        Assertions.assertThat(!codeCommunes.isEmpty()).isTrue();
    }

    /**
     * Test getListeCodeCommuneByCodeCfeAndTypeCFE Empty result.
     */
    @Test
    @Transactional(readOnly = true, value = "regent")
    public void test_getListeCodeCommuneByCodeCfeAndTypeCFE_Empty() {
        String codeCFE = "C14011";
        String typeCFE = "CCI";
        List<String> codeCommunes = dao.getListeCodeCommuneByCodeCfeAndTypeCFE(codeCFE, typeCFE);
        Assertions.assertThat(codeCommunes).isNotNull();
        Assertions.assertThat(codeCommunes.isEmpty()).isTrue();
    }
}
