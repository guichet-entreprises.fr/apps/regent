/**
 * 
 */
package fr.ge.common.regent.service.xmlregent.modification;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import fr.ge.common.regent.bean.xml.v2016.IXmlGlobal2016;
import fr.ge.common.regent.service.xmlregent.AbstractServiceGenerationXmlRegentTest;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * Classe de tests d'XML regent pour les évènement de type 67X en modification.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
public class ServiceGenerationXmlRegent_67X_Test extends AbstractServiceGenerationXmlRegentTest {

    private static final String TYPE_FORMALITE = "modification";
    private static final String TYPE_EVENEMENT = "67X";

    /**
     * JIRA 1417 / Selenium 75.
     *
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseE70Sel75() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "U33010002911", "M9301", "CFE", Arrays.asList("U3301"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E70";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise E70 ", "Moniteur de surf", result);
    }

    /**
     * JIRA 1417 / Selenium 79.
     *
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseE70Sel79() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "M93010000402", "M9301", "CFE+TDR", Arrays.asList("M9301"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E70";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise E70 ", "Réparation de raquettes et vente d'articles de sport sur stands", result);
    }

    /**
     * JIRA 1417 / Selenium 80.
     *
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseE70Sel80() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "M93010003061", "G9301", "TDR", Arrays.asList("G9301"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E70";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise E70 ", "Réparation de raquettes et vente d'articles de sport sur stands lors d'évènements sportifs", result);
    }

    /**
     * JIRA 2789 / Selenium 75.
     *
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseE02Sel75() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "U33010005088", "U3301", "CFE", Arrays.asList("U3301", "G3303"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.5";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise E02.5 ", "1", result);
    }

    /**
     * JIRA 2789 / Selenium 79.
     *
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseE02Sel79() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "M93010005157", "M9301", "CFE", Arrays.asList("M9301", "G9301"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.5";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise E02.5 ", "32", result);
    }

    /**
     * JIRA 2789 / Selenium 82.
     *
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseE02Sel82() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C33010005202", "C3301", "CFE", Arrays.asList("C3301", "G3302"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.5";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise E02.5 ", "22", result);
    }

    /**
     * JIRA 2789 / Selenium 87.
     *
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseE02Sel87() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C33020004956", "M9301", "CFE", Arrays.asList("M9301", "G9301"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.5";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise E02.5 ", "1", result);
    }

    /**
     * JIRA 2789 / Selenium 89.
     *
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseE02Sel89() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "M93010004994", "M9301", "CFE", Arrays.asList("M9301"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.5";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise E02.5 ", "1", result);
    }

    /**
     * JIRA 2789 / Selenium 90.
     *
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseE02Sel90() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "U93010005006", "U9301", "CFE", Arrays.asList("U9301", "G9301"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.5";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise E02.5 ", "1", result);
    }

    /**
     * JIRA 2840
     *
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckModuleSAEE84() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "M10010000020", "M1001", "CFE+TDR", Arrays.asList("M1001"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E84";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise E84 ", "1", result);
    }

    /**
     * JIRA 2840
     *
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckModuleSAEE858() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "M10010000020", "M1001", "CFE+TDR", Arrays.asList("M1001"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E85/E85.8";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise E85.8 ", "2", result);
    }

    /**
     * JIRA 2840
     *
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckModuleSAEE86O() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C10010000034", "C1001", "CFE", Arrays.asList("M1001", "G1001"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E86";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise E86 ", "N", result);
    }

    /**
     * JIRA 2840
     *
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckModuleSAENONTansmis() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C10010000034", "G1001", "TDR", Arrays.asList("M1001", "G1001"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("Le module SAE ", "", result);
    }

    /**
     * Rubrique JQU. Regent 2016.02
     *
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckRubriqueJQU() throws TechnicalException {
        IXmlGlobal2016 result = getXmlRegent2016(TYPE_EVENEMENT, TYPE_FORMALITE, "M75010000367", "M7501", "CFE", Arrays.asList("M7501"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);

        Assert.assertTrue(result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getJQU() != null);
        Assert.assertTrue(result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getJQU().length == 4);

        // Vérifier la balise U60
        Assert.assertEquals("La balise U60 ", "1", result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getJQU()[0].getU60());
        Assert.assertEquals("La balise U60 ", "2", result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getJQU()[1].getU60());
        Assert.assertEquals("La balise U60 ", "1", result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getJQU()[2].getU60());
        Assert.assertEquals("La balise U60 ", "1", result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getJQU()[3].getU60());

        // Vérifier la balise U61
        Assert.assertEquals("La balise U61.1 ", "O", result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getJQU()[0].getU61()[0].getU611());
        Assert.assertEquals("La balise U61.2 ", "Activité soumise à qualification 1/4", result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getJQU()[0].getU61()[0].getU612());

        Assert.assertEquals("La balise U61.1 ", "O", result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getJQU()[1].getU61()[0].getU611());
        Assert.assertEquals("La balise U61.2 ", "Activité soumise à qualification 2/4", result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getJQU()[1].getU61()[0].getU612());

        Assert.assertEquals("La balise U61.1 ", "O", result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getJQU()[2].getU61()[0].getU611());
        Assert.assertEquals("La balise U61.2 ", "Activité soumise à qualification 3/4", result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getJQU()[2].getU61()[0].getU612());

        Assert.assertEquals("La balise U61.1 ", "O", result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getJQU()[3].getU61()[0].getU611());
        Assert.assertEquals("La balise U61.2 ", "Activité soumise à qualification 4/4", result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getJQU()[3].getU61()[0].getU612());

        // Vérifier la balise U62
        Assert.assertEquals("La balise U62.1 ", "1", result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getJQU()[0].getU62()[0].getU621());
        Assert.assertEquals("La balise U62.2 ", null, result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getJQU()[0].getU62()[0].getU622());

        Assert.assertTrue(result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getJQU()[1].getU62().length == 0);

        Assert.assertEquals("La balise U62.1 ", "3", result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getJQU()[2].getU62()[0].getU621());
        Assert.assertEquals("La balise U62.2 ", null, result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getJQU()[2].getU62()[0].getU622());

        Assert.assertEquals("La balise U62.1 ", "5", result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getJQU()[3].getU62()[0].getU621());
        Assert.assertEquals("La balise U62.2 ", "AUTRE", result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getJQU()[3].getU62()[0].getU622());

        // Vérifier la balise U62.3
        Assert.assertEquals("La balise U62.3.1 ", "NOM", result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getJQU()[3].getU62()[0].getU623()[0].getU6231());
        Assert.assertEquals("La balise U62.3.2 ", "NOMusage", result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getJQU()[3].getU62()[0].getU623()[0].getU6232());
        Assert.assertEquals("La balise U62.3.4 ", "2017-02-01", result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getJQU()[3].getU62()[0].getU623()[0].getU6234());
        Assert.assertEquals("La balise U62.3.5 ", "02001", result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getJQU()[3].getU62()[0].getU623()[0].getU6235());
        Assert.assertEquals("La balise U62.3.6 ", null, result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getJQU()[3].getU62()[0].getU623()[0].getU6236());
        Assert.assertEquals("La balise U62.3.7 ", "ABBECOURT", result.getServiceApplicatif()[0].getLiasse()[0].getEntreprise()[0].getJQU()[3].getU62()[0].getU623()[0].getU6237());

    }

}
