/**
 * 
 */
package fr.ge.common.regent.service.xmlregent.creation;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import fr.ge.common.regent.bean.xml.v2008.IXmlGlobal2008;
import fr.ge.common.regent.service.xmlregent.AbstractServiceGenerationXmlRegentTest;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * Test événement 05P.
 * 
 * @author $Author: hhichri $
 * @version $Revision: 0 $
 */
public class ServiceGenerationXmlRegent_05P_Test extends AbstractServiceGenerationXmlRegentTest {

    private static final String TYPE_FORMALITE = "creation";
    private static final String TYPE_EVENEMENT = "05P";

    /**
     * Scénario voir Jira 2630 : Balise A64.2 : NIR ayant Droit.
     * 
     * @throws TechnicalException
     */
    @Test
    public void testCheckBaliseA64() throws TechnicalException {
        IXmlGlobal2008 result = getXmlRegent2008(TYPE_EVENEMENT, TYPE_FORMALITE, "X75010024171", "X7501", "CFE", Arrays.asList("X7501"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        Assert.assertEquals("La balise A64.2 ", "00", result.getServiceApplicatif()[0].getLiasse()[0].getPersonnePhysique()[0].getGCS()[0].getADS()[0].getA64()[0].getA642());
    }

    /**
     * Scénario 19 : Balise A53.4 : Situation du Conjoint vis-à-vis de la
     * sécurité Sociale ne doit pas être présente.
     *
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseSCSScenario19() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "M29010004104", "M2901", "CFE+TDR", Arrays.asList("M2901"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/Regent/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise SCS", "", result);
    }

    /**
     * Test check balise u50_ x path.
     *
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseU501_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "M42011812130", "M4201", "CFE", Arrays.asList("M4201"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/DIU/U50/U50.1";
        String result = evaluteXpathFromXml(xpath, xml);
        Assert.assertEquals("La balise U50.1", "", result);
    }

    /**
     * Test check balise P14 x path. la balise ne doit pas etre présente pour
     * les évenements de création
     *
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseP14_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "M95010000052", "M9501", "CFE", Arrays.asList("M9501"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/AIP/P14";
        String result = evaluteXpathFromXml(xpath, xml);
        Assert.assertEquals("La balise P14", "", result);
    }

    /**
     * [GUB-206].
     * 
     * @throws TechnicalException
     */
    @Test
    public void testCheckGUB206V2008() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "U23010000839", "U2301", "CFE", Arrays.asList("U2301"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/CAS/A42/A42.1";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise A42.1", "N2433", result);
    }

    /**
     * [GUB-206].
     * 
     * @throws TechnicalException
     */
    @Test
    public void testCheckGUB206V2016() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "U23010000839", "U2301", "CFE", Arrays.asList("U2301"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/CAS/A42/A42.1";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise A42.1", "N2433", result);
    }

    @Test
    public void testCheckBaliseC10_2_V2008_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C25012269511", "C2501", "CFE", Arrays.asList("C2501"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise C10.2", "2016-12-01", result);
    }

    @Test
    public void testCheckBaliseC10_2_V2016_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C25012269511", "C2501", "CFE", Arrays.asList("C2501"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise C10.2", "2016-12-01", result);
    }

}
