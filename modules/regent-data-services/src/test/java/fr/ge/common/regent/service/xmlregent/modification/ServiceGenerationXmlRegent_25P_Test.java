/**
 * 
 */
package fr.ge.common.regent.service.xmlregent.modification;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import fr.ge.common.regent.service.xmlregent.AbstractServiceGenerationXmlRegentTest;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * Test 10P.
 * 
 * @author $Author: hhichri $
 * @version $Revision: 0 $
 */
public class ServiceGenerationXmlRegent_25P_Test extends AbstractServiceGenerationXmlRegentTest {

    private static final String TYPE_FORMALITE = "modification";
    private static final String TYPE_EVENEMENT = "25P";

    /**
     * Test Mantis 1477:[MODIFICATION] : One of '{P76.2}' is expected.<br/>
     * Scénario : <br/>
     * C91011653929 P76.2 : Nom du registre Le XML ne respecte pas les XSD
     * définies pour les raisons suivantes : cvc-complex-type.2.4.b: The content
     * of element 'P76' is not complete. One of '{P76.2}' is expected.
     * 
     * @throws TechnicalException
     *             :
     * @{@link TechnicalException}
     */
    @Test
    public void testCheckRubriqueP76() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C91011653929", "C9101", "CFE", Arrays.asList("C9101"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P76/P76.2";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertNotNull(result);

        Assert.assertEquals("La balise P67.2", "3", result);
    }

    @Test
    public void testCheckBaliseC10_2_V2008_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C91011653929", "C9101", "CFE", Arrays.asList("C9101"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise C10.2", "2016-04-28", result);
    }

    @Test
    public void testCheckBaliseC10_2_V2016_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C91011653929", "C9101", "CFE", Arrays.asList("C9101"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise C10.2", "2016-04-28", result);
    }

}
