/**
 * 
 */
package fr.ge.common.regent.service.referentiel;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.google.common.collect.Lists;

import fr.ge.common.referentiel.dao.SupraCFECommuneDao;
import fr.ge.common.referentiel.service.impl.ReferentielSupraServiceImpl;
import fr.ge.common.regent.bean.modele.referentiel.ENormeEvenement;
import fr.ge.common.regent.bean.modele.referentiel.ENormeFlux;
import fr.ge.common.regent.bean.xml.TypeFluxXml;
import fr.ge.common.regent.constante.TypeDossierEnum;
import fr.ge.common.regent.constante.enumeration.DestinataireXmlRegentEnum;
import fr.ge.common.regent.persistance.dao.referentiel.NormeEvenementDao;
import fr.ge.common.regent.persistance.dao.referentiel.NormeFluxDao;
import fr.ge.common.regent.service.referentiel.impl.ReferentielServiceImpl;
import fr.ge.common.regent.util.test.AbstractSpringTest;

/**
 * Classe de test du service {@link ReferentielService}.
 * 
 * @author $Author: fbeaurai $
 * @version $Revision: 0 $
 */
public class ReferentielServiceImplTest extends AbstractSpringTest {

    /** Service referentiel. */
    @InjectMocks
    private ReferentielServiceImpl referentielService = new ReferentielServiceImpl();

    /** Service referentiel. */
    @Mock
    private ReferentielSupraServiceImpl referentielSupraService;

    /** Le norme flux dao. */
    @Mock
    private NormeFluxDao normeFluxDao;

    /** normeEvenement dao *. */
    @Mock
    private NormeEvenementDao normeEvenementDao;

    /** Le supra cfe commune dao. */
    @Mock
    private SupraCFECommuneDao supraCFECommuneDao;

    /**
     * Initialisation du test
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Test de la méthode
     * {@link ReferentielService#listerFluxParDestinataireEtEvenements(fr.guichetentreprises.constante.enumeration.DestinataireXmlRegentEnum, List, fr.ge.referentiel.bean.constante.TypeDossierEnum)}
     * .
     */
    @Test
    public void test_listerFluxParDestinataireEtEvenements() {
        // méthode getFLUXCA
        DestinataireXmlRegentEnum destinataire = DestinataireXmlRegentEnum.CA;
        String evenementCourant = "evt";
        TypeDossierEnum typeDossier = TypeDossierEnum.CREATION;
        String typeFlux = "fluxCa";

        List<ENormeFlux> normesFlux = Lists.<ENormeFlux>newArrayList();
        ENormeFlux normeFlux = new ENormeFlux();
        normeFlux.setType(TypeDossierEnum.CREATION.getTypeTechnique());
        normeFlux.setFluxCa(typeFlux);
        normeFlux.setEvenement(evenementCourant);
        normesFlux.add(normeFlux);
        normeFlux = new ENormeFlux();
        normeFlux.setType(TypeDossierEnum.MODIFICATION.getTypeTechnique());
        normesFlux.add(normeFlux);
        normeFlux = new ENormeFlux();
        normeFlux.setType(TypeDossierEnum.REGULARISATION.getTypeTechnique());
        normesFlux.add(normeFlux);
        normeFlux = new ENormeFlux();
        // Type composé FC
        normeFlux.setType(TypeDossierEnum.CESSATION.getTypeTechnique().concat(TypeDossierEnum.CREATION.getTypeTechnique()));
        normesFlux.add(normeFlux);

        when(this.normeFluxDao.getFluxByEvenementAndDestinataire(Mockito.any(DestinataireXmlRegentEnum.class), Mockito.anyListOf(String.class))).thenReturn(normesFlux);

        String keyMapFlux = "FLUXCA" + "-" + "C";

        List<String> evenements = Lists.<String>newArrayList(evenementCourant);
        TypeFluxXml typeFluxXml = new TypeFluxXml(new HashMap<String, List<String>>(), normeFlux.getPoid(), typeFlux, typeDossier.getTypeTechnique(), normeFlux.getRole(), evenements);
        typeFluxXml.getMapRoleEvenement().put(normeFlux.getRole(), evenements);
        Map<String, TypeFluxXml> mapFlux = new HashMap<String, TypeFluxXml>();
        mapFlux.put(keyMapFlux, typeFluxXml);

        List<TypeFluxXml> resultatAttendu = Lists.<TypeFluxXml>newArrayList(typeFluxXml);

        // Appel du mock de la couche service
        List<TypeFluxXml> resultat = this.referentielService.listerFluxParDestinataireEtEvenements(destinataire, evenements, typeDossier);
        assertThat(resultat).isEqualTo(resultatAttendu);

        // Verification des arguments lors de l'appel de la couche persistance
        verify(this.normeFluxDao).getFluxByEvenementAndDestinataire(Mockito.any(DestinataireXmlRegentEnum.class), Mockito.anyListOf(String.class));
    }

    /**
     * Test de la méthode
     * {@link ReferentielService#getRubriqueOblogatoireOuConditionnelle(String, List)}.
     */
    @Test
    public void test_getRubriqueOblogatoireOuConditionnelle() {
        String version = "version";
        List<String> evts = Lists.<String>newArrayList();
        List<ENormeEvenement> listRubrique = Lists.<ENormeEvenement>newArrayList();
        String rubrique = "Rubrique";
        ENormeEvenement normeEvt = new ENormeEvenement();
        normeEvt.setCondition(Boolean.TRUE);
        normeEvt.setRubrique(rubrique);
        listRubrique.add(normeEvt);

        when(normeEvenementDao.getRubriqueOblogatoireOuConditionnelle(Mockito.anyString(), Mockito.anyListOf(String.class))).thenReturn(listRubrique);

        Map<String, Boolean> resultatAttendu = new HashMap<String, Boolean>();
        resultatAttendu.put(rubrique, Boolean.TRUE);

        // Appel du mock de la couche service
        Map<String, Boolean> resultat = this.referentielService.getRubriqueOblogatoireOuConditionnelle(version, evts);
        assertThat(resultat).isEqualTo(resultatAttendu);

        // Verification des arguments lors de l'appel de la couche persistance
        verify(this.normeEvenementDao).getRubriqueOblogatoireOuConditionnelle(Mockito.anyString(), Mockito.anyListOf(String.class));
    }

}
