/**
 *
 */
package fr.ge.common.regent.service.xmlregent;

import static org.apache.commons.lang.CharEncoding.UTF_8;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xmlfield.core.XmlFieldFactory;
import org.xmlfield.core.exception.XmlFieldParsingException;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.common.regent.bean.xml.TypeFluxXml;
import fr.ge.common.regent.bean.xml.v2008.IXmlGlobal2008;
import fr.ge.common.regent.bean.xml.v2016.IXmlGlobal2016;
import fr.ge.common.regent.constante.FormaliteXmlIdTechniquesEnum;
import fr.ge.common.regent.constante.TypeDossierEnum;
import fr.ge.common.regent.constante.enumeration.DestinataireXmlRegentEnum;
import fr.ge.common.regent.context.function.FonctionPdf;
import fr.ge.common.regent.context.function.FonctionRegent;
import fr.ge.common.regent.service.exception.XmlInvalidException;
import fr.ge.common.regent.service.impl.ServiceGenerationXmlRegentFromDBImpl;
import fr.ge.common.regent.service.referentiel.ReferentielService;
import fr.ge.common.regent.util.test.AbstractSpringTest;
import fr.ge.common.regent.util.test.CoreUtil;
import fr.ge.common.regent.ws.v1.bean.RegentResultBean;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * Class AbstractServiceGenerationXmlRegentTest.
 *
 * @author $Author: HZITOUNI $
 * @version $Revision: 0 $
 */
@Ignore
public class AbstractServiceGenerationXmlRegentTest extends AbstractSpringTest {

    /** data files rep. */
    public static String DATA_FILES_REP = "src/test/resources/data/xmlregent/";

    /** empty regent. */
    public static String EMPTY_REGENT = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><REGENT-XML/>";

    /** formalite prefixe. */
    public static String FORMALITE_PREFIXE = "form_";

    /** input suffixe. */
    public static String INPUT_SUFFIXE = ".json";

    /** profil prefixe. */
    public static String PROFIL_PREFIXE = "profil_";

    /** La constante VERSION_2008_XMLREGENT. */
    public static final String VERSION_2008_XMLREGENT = "V2008.11";

    /** La constante VERSION_2016_XMLREGENT. */
    public static final String VERSION_2016_XMLREGENT = "V2016.02";

    /** La constante XSD_REGENT_PATH_2008. */
    private static final String XSD_REGENT_PATH_2008 = DATA_FILES_REP + "xsd/Message_Regent_V2008-11.xsd";

    /** La constante XSD_REGENT_PATH_2016. */
    private static final String XSD_REGENT_PATH_2016 = DATA_FILES_REP + "xsd/Message_Regent_V2016-02.xsd";

    /**
     * permet de determiner le repertoire contenant les données du test.
     *
     * @param fullClassName
     *            full class name
     * @return path from class
     */
    protected static String getPathFromClass(final String fullClassName) {
        // Decoupage du nom de la classe
        final String message = "La classe " + fullClassName + " ne respecte pas la règle de nommage "
                + "fr.ge.common.regent.service.xmlregent.[FORMALITE].ServiceGenerationXmlRegent_[EVENEMENT]_Test.java"
                + " / Exemple : fr.ge.common.regent.service.xmlregent.creation.ServiceGenerationXmlRegent_01P_Test.java";
        // fr.guichetentreprises.service.xmlregent.creation.ServiceGenerationXmlRegent_01P_Test
        String[] tabStr = fullClassName.split("\\.");
        if (tabStr.length < 3) {
            throw new IllegalArgumentException(message);
        }
        // Récupération du type d'evenement
        final String className = tabStr[tabStr.length - 1];

        // Récupération de la formalité : création
        final String formalite = tabStr[tabStr.length - 2];

        tabStr = className.split("_");

        if (tabStr.length < 3) {
            throw new IllegalArgumentException(message);
        }

        // Récupération de l'evenement : 01P
        final String evenement = tabStr[tabStr.length - 2];

        return DATA_FILES_REP + formalite + File.separator + evenement;
    }

    /** fonction regent. */
    @Autowired
    @InjectMocks
    private FonctionRegent fonctionRegent;

    /** ref service. */
    @Autowired
    private ReferentielService refService;

    /** service generation xml regent from DB. */
    @Autowired
    @InjectMocks
    ServiceGenerationXmlRegentFromDBImpl serviceGenerationXmlRegentFromDB;

    /** Le xml field factory. */
    @Autowired
    private XmlFieldFactory xmlFieldFactory;

    /** ref service. */
    @Autowired
    private FonctionPdf fonctionPdf;

    /**
     * Evalute xpath from xml.
     *
     * @param xpath
     *            xpath
     * @param xml
     *            xml
     * @return string
     * @throws TechnicalException
     *             technique exception
     */
    public String evaluteXpathFromXml(final String xpath, final String xml) throws TechnicalException {
        String result;
        final XPath xPath = XPathFactory.newInstance().newXPath();
        try {
            result = xPath.evaluate(xpath, new InputSource(new StringReader(xml)));
        } catch (final XPathExpressionException e) {
            throw new TechnicalException("Erreur lors de la récupération du XPATH " + xpath + " sur le xml ");
        }

        return result;
    }

    /**
     * Accesseur sur l'attribut {@link #evenement pour destinataire}.
     *
     * @param typeFlux
     *            type flux
     * @param roleDestinataire
     *            role destinataire
     * @return evenement pour destinataire
     */
    private List<String> getEvenementPourDestinataire(final TypeFluxXml typeFlux, final String roleDestinataire) {
        final List<String> listEvenement = new ArrayList<String>();
        for (final String role : typeFlux.getMapRoleEvenement().keySet()) {
            if (StringUtils.equals(roleDestinataire, role) || StringUtils.contains(role, roleDestinataire) || StringUtils.contains(roleDestinataire, role)) {
                listEvenement.addAll(typeFlux.getMapRoleEvenement().get(role));
            }
        }
        Collections.sort(listEvenement);
        return listEvenement;
    }

    /**
     * Methode permettant de récupérer la liste des evenement depuis la
     * représentation XML du profil en entrée du test. - Recupération de
     * l'identifiant de Formalité (XPATH : /formalite/identifiantFormalite) -
     * Détermination du type de formalité par l'enum
     * FormaliteXmlIdTechniquesEnum - Si création : <evenement>01P</evenement>
     * <evenement>02M</evenement> - Sinon : <evenements>
     * <evenement>01P</evenement> <evenement>02M</evenement> </evenements>
     *
     * @param numDossier
     *            num dossier
     * @return list evenement
     * @throws TechnicalException
     *             technique exception
     */
    private List<String> getListEvenement(final String typeEvenement, final String numDossier) throws TechnicalException {
        // Recupération de l'identifiant de Formalité (XPATH :
        // /formalite/identifiantFormalite)

        ObjectMapper mapper = new ObjectMapper();
        String profilJson = getProfil(typeEvenement, numDossier);

        JsonNode rootProfil = null;
        try {
            rootProfil = mapper.readTree(profilJson);
        } catch (Exception e) {
            throw new TechnicalException(e);
        }

        final String identifiantFormalite = rootProfil.at("/formalite/identifiantFormalite").asText();

        if (StringUtils.isBlank(identifiantFormalite)) {
            throw new TechnicalException("Valeur NULL ou Vide lors de la récupération du XPATH " + identifiantFormalite + " sur le xml profil du dossier : " + numDossier);
        }

        // Détermination du type de formalité par l'enum
        // FormaliteXmlIdTechniquesEnum

        final FormaliteXmlIdTechniquesEnum formaliteType = FormaliteXmlIdTechniquesEnum.fromIdTechnique(identifiantFormalite);

        if (StringUtils.isBlank(identifiantFormalite)) {
            throw new TechnicalException("Le type de profil n'a pas pu être determiné pour la valeur  " + identifiantFormalite + " dans l'énumération FormaliteXmlIdTechniquesEnum");
        }

        return this.getListEvtFromTypeProf(rootProfil, formaliteType);

    }

    /**
     * Accesseur sur l'attribut {@link #list evt from type prof}.
     *
     * @param xmlProfil
     *            xml profil
     * @param formaliteType
     *            formalite type
     * @return list evt from type prof
     * @throws TechnicalException
     *             technique exception
     */
    private List<String> getListEvtFromTypeProf(final JsonNode rootProfil, final FormaliteXmlIdTechniquesEnum formaliteType) throws TechnicalException {
        /**
         * - Si création : <evenement>01P</evenement> <evenement>02M</evenement>
         * - Sinon : <evenements> <evenement>01P</evenement>
         * <evenement>02M</evenement> </evenements>
         */

        List<String> results = new ArrayList<String>();
        if (formaliteType == FormaliteXmlIdTechniquesEnum.PROFIL_CREATION || formaliteType == FormaliteXmlIdTechniquesEnum.PROFIL) {
            final String evenement = rootProfil.at("/formalite/evenement").asText();
            results.add(evenement);
        } else {
            JsonNode valuesNode = rootProfil.at("/formalite/evenements/evenement");
            if (null != valuesNode.asText()) {
                results.add(valuesNode.asText());
            }
            for (JsonNode node : valuesNode) {
                results.add(node.asText());
            }
        }
        return results;
    }

    /**
     * Accesseur sur l'attribut {@link #type dossier from identifiant}.
     *
     * @param identifiantFormalite
     *            identifiant formalite
     * @return type dossier from identifiant
     * @throws TechnicalException
     *             technique exception
     */
    private TypeDossierEnum getTypeDossierFromIdentifiant(final String identifiantFormalite) throws TechnicalException {
        // Détermination du type de formalité par l'enum
        // FormaliteXmlIdTechniquesEnum

        final FormaliteXmlIdTechniquesEnum formaliteType = FormaliteXmlIdTechniquesEnum.fromIdTechnique(identifiantFormalite);

        if (StringUtils.isBlank(identifiantFormalite)) {
            throw new TechnicalException("Le type de profil n'a pas pu être determiné pour la valeur  " + identifiantFormalite + " dans l'énumération FormaliteXmlIdTechniquesEnum");
        }
        switch (formaliteType) {
        case PROFIL_CREATION:
            return TypeDossierEnum.CREATION;
        case PROFIL:
            return TypeDossierEnum.CREATION;
        case PROFIL_REGULARISATION:
            return TypeDossierEnum.REGULARISATION;
        case PROFIL_CESSATION:
            return TypeDossierEnum.CESSATION;
        case PROFIL_MODIFICATION:
            return TypeDossierEnum.MODIFICATION;

        default:
            throw new TechnicalException("Le type de dossier n'a pas pu être determiné pour la valeur  " + formaliteType + " dans l'énumération FormaliteXmlIdTechniquesEnum");
        }

    }

    /**
     * Récupération du type de flux à partir du numéro de dossier et du role du
     * destinataire.
     *
     * @param numDossier
     *            num dossier
     * @param role
     *            role
     * @param evenements
     *            evenements
     * @return type flux
     * @throws TechnicalException
     *             technique exception
     */
    private TypeFluxXml getTypeFlux(final String typeEvenement, final String numDossier, final String role, final List<String> evenements) throws TechnicalException {
        ObjectMapper mapper = new ObjectMapper();
        String profilXml = getProfil(typeEvenement, numDossier);
        String dialogueXml = getDialogue(typeEvenement, numDossier);

        JsonNode rootProfil = null;
        try {
            rootProfil = mapper.readTree(profilXml);
        } catch (Exception e) {
            throw new TechnicalException(e);
        }
        JsonNode rootDialogue = null;
        try {
            rootDialogue = mapper.readTree(dialogueXml);
        } catch (Exception e) {
            throw new TechnicalException(e);
        }

        final String estAgentCommercial = rootDialogue.at("/formalite/estAgentCommercial").asText();

        String secteurCfe = rootProfil.at("/formalite/secteurCfe").asText();

        if (StringUtils.isBlank(secteurCfe)) {
            secteurCfe = rootProfil.at("/formalite/secteurCFE").asText();
        }

        final String identifiantFormalite = rootProfil.at("/formalite/identifiantFormalite").asText();

        DestinataireXmlRegentEnum reseauDestinataire = null;

        if (StringUtils.equals(estAgentCommercial, "true") && StringUtils.equals("GREFFE", secteurCfe)) {
            reseauDestinataire = DestinataireXmlRegentEnum.getDestinataireParNom("AGTCOM");
        } else {
            reseauDestinataire = DestinataireXmlRegentEnum.getDestinataireParNom(secteurCfe);
        }
        final TypeDossierEnum typeDossier = this.getTypeDossierFromIdentifiant(identifiantFormalite);
        final List<TypeFluxXml> listTypeFlux = this.refService.listerFluxParDestinataireEtEvenements(reseauDestinataire, evenements, typeDossier);
        for (final TypeFluxXml typeFluxXml : listTypeFlux) {
            if (typeFluxXml.getRoleDeFlux().contains(role)) {
                return typeFluxXml;
            }
        }

        return null;
    }

    /**
     * Accesseur sur l'attribut {@link #xml from file}.
     *
     * @param prefixe
     *            prefixe
     * @param numeroDossier
     *            numero dossier
     * @return xml from file
     * @throws TechnicalException
     *             technique exception
     */
    public String getJsonFromFile(final String prefixe, final String numeroDossier, final String extension) throws TechnicalException {
        try {
            final String path = getPathFromClass(this.getClass().getName());
            return FileUtils.readFileToString(new File(path, prefixe + numeroDossier + extension), UTF_8);
        } catch (final IOException e) {
            throw new TechnicalException("Le fichier " + prefixe + numeroDossier + extension + " est introuvable sous le répertoire " + DATA_FILES_REP, e);
        }
    }

    /**
     * Accesseur sur l'attribut {@link #xml regent} pour la version 2008.
     *
     * @param numDossier
     *            num dossier
     * @param codeEdi
     *            code edi
     * @param role
     *            role
     * @param listCodeEdi
     *            list code edi
     * @param version
     *            la version du XML regent
     * @return xml regent
     * @throws TechnicalException
     *             technique exception
     */
    public IXmlGlobal2008 getXmlRegent2008(final String typeEvenement, final String typeFormalite, final String numDossier, final String codeEdi, final String role, final List<String> listCodeEdi,
            final String version) throws TechnicalException {

        final String xmlRegentStr = this.getXmlRegentStr(typeEvenement, typeFormalite, numDossier, codeEdi, role, listCodeEdi, version);
        IXmlGlobal2008 ixmlRegent;
        final String erreurXmlField = "Erreur lors de la transformation XMLField pour le dossier : Numero dossier " + numDossier + "/  codeEDI : " + codeEdi + " / role : " + role;
        try {
            ixmlRegent = this.xmlFieldFactory.getXmlField().xmlToObject(xmlRegentStr, IXmlGlobal2008.class);
        } catch (final XmlFieldParsingException e) {
            throw new TechnicalException(erreurXmlField, e);
        }

        return ixmlRegent;

    }

    /**
     * Accesseur sur l'attribut {@link #xml regent} pour la version 2008.
     *
     * @param numDossier
     *            num dossier
     * @param codeEdi
     *            code edi
     * @param role
     *            role
     * @param listCodeEdi
     *            list code edi
     * @param version
     *            la version du XML regent
     * @return xml regent
     * @throws TechnicalException
     *             technique exception
     */
    public IXmlGlobal2016 getXmlRegent2016(final String typeEvenement, final String typeFormalite, final String numDossier, final String codeEdi, final String role, final List<String> listCodeEdi,
            final String version) throws TechnicalException {

        final String xmlRegentStr = this.getXmlRegentStr(typeEvenement, typeFormalite, numDossier, codeEdi, role, listCodeEdi, version);

        IXmlGlobal2016 ixmlRegent;
        final String erreurXmlField = "Erreur lors de la transformation XMLField pour le dossier : Numero dossier " + numDossier + "/  codeEDI : " + codeEdi + " / role : " + role;
        try {
            ixmlRegent = this.xmlFieldFactory.getXmlField().xmlToObject(xmlRegentStr, IXmlGlobal2016.class);
        } catch (final XmlFieldParsingException e) {
            throw new TechnicalException(erreurXmlField, e);
        }

        return ixmlRegent;

    }

    /**
     * Accesseur sur l'attribut {@link #xml regent str}.
     *
     * @param numDossier
     *            num dossier
     * @param codeEdi
     *            code edi
     * @param role
     *            role
     * @param listCodeEdi
     *            list code edi
     * @param version
     *            la version du XML Regent
     * @return xml regent str
     * @throws TechnicalException
     *             technique exception
     * @throws UnsupportedEncodingException
     * @throws XmlInvalidException
     */
    public String getXmlRegentStr(final String typeEvenement, final String typeFormalite, final String numDossier, final String codeEdi, final String role, final List<String> listCodeEdi,
            final String version) throws TechnicalException {
        final String strMessErreur = "Erreur lors du traitement du dossier : Numero dossier " + numDossier + "/  codeEDI : " + codeEdi + " / role : " + role;

        final List<String> evenements = this.getListEvenement(typeEvenement, numDossier);
        final TypeFluxXml typeFlux = this.getTypeFlux(typeEvenement, numDossier, role, evenements);
        String xmlRegentStr = null;

        try {
            final List<String> listEvenementsByDestinataire = this.getEvenementPourDestinataire(typeFlux, role);

            String profil = getProfil(typeEvenement, numDossier);
            String cfe = getDialogue(typeEvenement, numDossier);

            final RegentResultBean regentResult = this.serviceGenerationXmlRegentFromDB.getRegentXmlCap(listCodeEdi, profil, cfe, codeEdi, role, listEvenementsByDestinataire, version, numDossier,
                    typeFlux.getTypeDeFlux());
            xmlRegentStr = new String(regentResult.getContent(), "UTF-8");
        } catch (final TechnicalException e1) {
            throw new TechnicalException(strMessErreur, e1);
        } catch (final UnsupportedEncodingException e2) {
            throw new TechnicalException(strMessErreur, e2);
        }
        Assert.assertNotNull("Le xmlRegent produit pour le dossier " + numDossier + " est null", xmlRegentStr);
        Assert.assertNotEquals("Le xmlRegent produit pour le dossier " + numDossier + " est vide", xmlRegentStr, EMPTY_REGENT);

        Assert.assertTrue(this.validateOnRegentXsd(xmlRegentStr, version));
        return xmlRegentStr;

    }

    /**
     * Sets the up.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Validate on regent xsd.
     *
     * @param xml
     *            xml
     * @param version
     *            version
     * @return true, si ça fonctionne
     * @throws TechnicalException
     *             technique exception
     */
    boolean validateOnRegentXsd(final String xml, final String version) throws TechnicalException {
        final String messageErreur = "La validation XSD du document Regent n'a pas pu être effectuée.";
        final SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema;
        String cheminXsd = null;
        if (VERSION_2008_XMLREGENT.equals(version)) {
            cheminXsd = XSD_REGENT_PATH_2008;
        } else if (VERSION_2016_XMLREGENT.equals(version)) {
            cheminXsd = XSD_REGENT_PATH_2016;
        }
        try {
            schema = factory.newSchema(new File(cheminXsd));

            final Validator validator = schema.newValidator();

            validator.validate(new StreamSource(new ByteArrayInputStream(xml.getBytes())));
        } catch (final SAXException e) {
            throw new TechnicalException(messageErreur, e);
        } catch (IOException e) {
            throw new TechnicalException(messageErreur, e);
        }
        return true;

    }

    /**
     * Resource name.
     *
     * @param name
     *            the name
     * @return the string
     */
    public String resourceName(final String name) {
        if (name.startsWith("/")) {
            return name;
        } else {
            return String.format("%s-%s", this.getClass().getSimpleName(), name);
        }
    }

    public byte[] resourceAsBytes(final String name) {
        byte[] asBytes = null;
        if (null == asBytes) {
            final String fullName = this.resourceName(name);
            asBytes = CoreUtil.resourceAsBytes(fullName, this.getClass());
        }

        return asBytes;
    }

    /**
     * Get a JSON dialogue.
     * 
     * @param numeroDossier
     * @return
     * @throws TechnicalException
     */
    public String getDialogue(final String typeEvenement, final String numeroDossier) throws TechnicalException {
        final String fileName = typeEvenement + "/form_" + numeroDossier + ".json";
        final byte[] beanAsBytes = resourceAsBytes(fileName);
        try {
            return new String(beanAsBytes, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new TechnicalException(e);
        }
    }

    /**
     * Get a JSON profil.
     * 
     * @param numeroDossier
     * @return
     * @throws TechnicalException
     */
    public String getProfil(final String typeEvenement, final String numeroDossier) throws TechnicalException {
        final String fileName = typeEvenement + "/profil_" + numeroDossier + ".json";

        final byte[] beanAsBytes = resourceAsBytes(fileName);
        try {
            return new String(beanAsBytes, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new TechnicalException(e);
        }
    }
}
