package fr.ge.common.regent.service.xmlregent;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import fr.ge.common.regent.service.impl.ServiceGenerationXmlRegentFromDBImpl;
import fr.ge.common.regent.services.declenchement.IDeclenchementRubriqueXmlRegentService;
import fr.ge.common.regent.xml.XmlFieldElement;
import fr.ge.common.regent.xml.impl.XmlWriter;
import fr.ge.common.utils.exception.TechnicalException;

public class ServiceGenerationXmlRegentFromDBImplTest {
    @Mock
    private IDeclenchementRubriqueXmlRegentService declenchementRubriqueXmlRegent;
    @Mock
    private XmlWriter xmlWriter;

    @InjectMocks
    ServiceGenerationXmlRegentFromDBImpl classTest = new ServiceGenerationXmlRegentFromDBImpl() {
    };

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCheck_REGEN() throws TechnicalException {

        LinkedHashMap<String, Object> mappedFileToRegent = new LinkedHashMap<String, Object>();
        mappedFileToRegent.put("/REGENT-XML/rubriqueA:Emplacement", "toto");
        mappedFileToRegent.put("/REGENT-XML/rubriqueB:Emplacement", null);
        final String versionNorme = "2008";
        XmlFieldElement normRubrique = new XmlFieldElement(null, "10000", "G001", null, "/Regent/rubriqueA:Emplacement", 1, "RUBRIQUE A", true, ".", true, versionNorme);
        XmlFieldElement normRubrique2 = new XmlFieldElement(null, "20000", "G002", null, "/Regent/rubriqueB:Emplacement", 1, "RUBRIQUE B", true, ".", true, versionNorme);
        List<XmlFieldElement> listTagsByEvenement = new ArrayList<>();
        listTagsByEvenement.add(normRubrique);
        listTagsByEvenement.add(normRubrique2);
        List<XmlFieldElement> listUn = new ArrayList<>();
        List<XmlFieldElement> listRubrique1 = new ArrayList<>();
        listRubrique1.add(normRubrique);
        List<XmlFieldElement> listRubrique2 = new ArrayList<>();
        listRubrique2.add(normRubrique2);
        List<String> list = new ArrayList<String>();
        List<String> listNomRubrique = new ArrayList<String>();
        listNomRubrique.add("RUBRIQUE A");
        listNomRubrique.add("RUBRIQUE B");
        Mockito.when(declenchementRubriqueXmlRegent.getListRubriqueForXmlRegent(Mockito.eq(versionNorme), Mockito.eq(list), Mockito.anyString(), Mockito.eq("G"))).thenReturn(listNomRubrique);
        Mockito.when(xmlWriter.getXmlFieldElement(Mockito.eq("regent"), Mockito.eq("RUBRIQUE A"), Mockito.eq(versionNorme))).thenReturn(listRubrique1);
        Mockito.when(xmlWriter.getXmlFieldElement(Mockito.eq("regent"), Mockito.eq("RUBRIQUE B"), Mockito.eq(versionNorme))).thenReturn(listRubrique2);
        Mockito.when(xmlWriter.getXmlFieldElement(Mockito.eq("regent"), Mockito.eq(versionNorme))).thenReturn(listUn);
        Mockito.when(xmlWriter.getListRubriqueOrdonne(Mockito.anyList(), Mockito.anyList())).thenReturn(listTagsByEvenement);

        List<String> listEvents = new ArrayList<>();
        LinkedHashMap<String, Object> retour = classTest.fillMandatoryTags("G7501", "", listEvents, versionNorme, "H100099999999", mappedFileToRegent);

    }

    @Test
    public void testCheck_REGEN_ListValues() throws TechnicalException {

        LinkedHashMap<String, Object> mappedFileToRegent = new LinkedHashMap<String, Object>();
        List<String> listValuesRubriqueA = new ArrayList<String>();
        List<String> listValuesRubriqueB = new ArrayList<String>();

        listValuesRubriqueA.add("prenomA");
        listValuesRubriqueA.add("prenomB");
        listValuesRubriqueA.add("prenomC");
        listValuesRubriqueA.add("prenomD");
        listValuesRubriqueB.add("prenomE");
        listValuesRubriqueB.add("prenomF");
        listValuesRubriqueB.add("prenomG");

        mappedFileToRegent.put("/REGENT-XML/rubriqueA:Emplacement", "toto");
        mappedFileToRegent.put("/REGENT-XML/rubriqueB:Emplacement", null);
        mappedFileToRegent.put("/REGENT-XML/rubriqueA:Emplacement", listValuesRubriqueA);
        mappedFileToRegent.put("/REGENT-XML/rubriqueC:Emplacement", listValuesRubriqueB);
        final String versionNorme = "2008";
        XmlFieldElement normRubrique = new XmlFieldElement(null, "10000", "G001", null, "/Regent/rubriqueA:Emplacement", 1, "RUBRIQUE A", true, ".", true, versionNorme);
        XmlFieldElement normRubrique2 = new XmlFieldElement(null, "20000", "G002", null, "/Regent/rubriqueB:Emplacement", 1, "RUBRIQUE B", true, ".", true, versionNorme);
        List<XmlFieldElement> listTagsByEvenement = new ArrayList<>();
        listTagsByEvenement.add(normRubrique);
        listTagsByEvenement.add(normRubrique2);
        List<XmlFieldElement> listUn = new ArrayList<>();
        List<XmlFieldElement> listRubrique1 = new ArrayList<>();
        listRubrique1.add(normRubrique);
        List<XmlFieldElement> listRubrique2 = new ArrayList<>();
        listRubrique2.add(normRubrique2);
        List<String> list = new ArrayList<String>();
        List<String> listNomRubrique = new ArrayList<String>();
        listNomRubrique.add("RUBRIQUE A");
        listNomRubrique.add("RUBRIQUE B");
        Mockito.when(declenchementRubriqueXmlRegent.getListRubriqueForXmlRegent(Mockito.eq(versionNorme), Mockito.eq(list), Mockito.anyString(), Mockito.eq("G"))).thenReturn(listNomRubrique);
        Mockito.when(xmlWriter.getXmlFieldElement(Mockito.eq("regent"), Mockito.eq("RUBRIQUE A"), Mockito.eq(versionNorme))).thenReturn(listRubrique1);
        Mockito.when(xmlWriter.getXmlFieldElement(Mockito.eq("regent"), Mockito.eq("RUBRIQUE B"), Mockito.eq(versionNorme))).thenReturn(listRubrique2);
        Mockito.when(xmlWriter.getXmlFieldElement(Mockito.eq("regent"), Mockito.eq(versionNorme))).thenReturn(listUn);
        Mockito.when(xmlWriter.getListRubriqueOrdonne(Mockito.anyList(), Mockito.anyList())).thenReturn(listTagsByEvenement);

        List<String> listEvents = new ArrayList<>();
        LinkedHashMap<String, Object> retour = classTest.fillMandatoryTags("G7501", "", listEvents, versionNorme, "H100099999999", mappedFileToRegent);

    }

    @Test
    public void getTagsByEventsAndVersionTest() throws TechnicalException {

        final String versionNorme = "2008.11";
        List<XmlFieldElement> listTagsByEvenement = new ArrayList<>();
        List<XmlFieldElement> listTagsByEvenementt = new ArrayList<>();
        List<XmlFieldElement> listTagsByEvenementtt = new ArrayList<>();
        List<XmlFieldElement> listUn = new ArrayList<>();
        List<XmlFieldElement> listDeux = new ArrayList<>();
        XmlFieldElement normRubrique = new XmlFieldElement(null, "10000", "G001", null, "/Regent/rubriqueA:Emplacement", 1, "RUBRIQUE A", true, ".", true, versionNorme);
        XmlFieldElement normRubrique2 = new XmlFieldElement(null, "20000", "G002", null, "/Regent/rubriqueB:Emplacement", 1, "RUBRIQUE B", true, ".", true, versionNorme);
        listTagsByEvenement.add(normRubrique);
        listTagsByEvenementt.add(normRubrique2);
        listTagsByEvenementtt.add(normRubrique);
        listTagsByEvenementtt.add(normRubrique2);

        List<String> listeven = new ArrayList<>();
        listeven.add("even1");
        when(declenchementRubriqueXmlRegent.getListRubriqueByEventAndVersion(listeven, versionNorme)).thenReturn(Mockito.anyList());
        when(xmlWriter.getXmlFieldElement(Mockito.eq("regent"), versionNorme)).thenReturn(listTagsByEvenement);
        when(xmlWriter.getListRubriqueOrdonne(Mockito.anyList(), Mockito.anyList())).thenReturn(listTagsByEvenementtt);
        List<XmlFieldElement> expectedsList = classTest.getTagsByEventsAndVersion(listeven, versionNorme);

        assertEquals(expectedsList, listTagsByEvenementtt);

    }

}
