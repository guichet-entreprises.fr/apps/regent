/**
 * 
 */
package fr.ge.common.regent.service.xmlregent.modification;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import fr.ge.common.regent.service.xmlregent.AbstractServiceGenerationXmlRegentTest;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * Test 10P.
 * 
 * @author $Author: hhichri $
 * @version $Revision: 0 $
 */
public class ServiceGenerationXmlRegent_10P_Test extends AbstractServiceGenerationXmlRegentTest {

    private static final String TYPE_FORMALITE = "modification";
    private static final String TYPE_EVENEMENT = "10P";

    /**
     * Test Mantis 1278:[MODIFICATION][URSSAF] : L'ancien nom ou prénom est
     * obligatoire dans le cas d'un 10P <br/>
     * Scénario : <br/>
     * Exécuter les scénarios 135, 136.<br/>
     * - 10P <br/>
     * - Modification nom de naissance ==> sélénium 135. <br/>
     * - Modification prénom ==> sélénium 136.
     * 
     * Résultat obtenu : <br/>
     * 135 - Absence de l'ancien prénom alors que la donnée est obligatoire
     * <br/>
     * 136 - Absence de l'ancien nom de naissance alors que la donnée est
     * obligatoire
     * 
     * Résultat attendu : <br/>
     * En cas de modification du seul nom ou du seul prénom, l'ancien nom ET
     * l'ancien prénom doivent être restitués.
     * 
     * @throws TechnicalException
     *             :
     * @{@link TechnicalException}
     */
    @Test
    public void testCheckRubriqueAIPTest136() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "U44010000034", "U4401", "CFE", Arrays.asList("U4401"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/AIP/P11";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise P11", "MARTINEAU", result);
    }

    /**
     * Test Mantis 1278:[MODIFICATION][URSSAF] : L'ancien nom ou prénom est
     * obligatoire dans le cas d'un 10P <br/>
     * Scénario : <br/>
     * Exécuter les scénarios 135, 136.<br/>
     * - 10P <br/>
     * - Modification nom de naissance ==> sélénium 135. <br/>
     * - Modification prénom ==> sélénium 136.
     * 
     * Résultat obtenu : <br/>
     * 135 - Absence de l'ancien prénom alors que la donnée est obligatoire
     * <br/>
     * 136 - Absence de l'ancien nom de naissance alors que la donnée est
     * obligatoire
     * 
     * Résultat attendu : <br/>
     * En cas de modification du seul nom ou du seul prénom, l'ancien nom ET
     * l'ancien prénom doivent être restitués.
     * 
     * @throws TechnicalException
     *             :
     * @{@link TechnicalException}
     */
    @Test
    public void testCheckRubriqueAIPTest135() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "U44010000026", "U4401", "CFE", Arrays.asList("U4401"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/AIP/P14";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise P14", "ALICIA", result);
    }

    @Test
    public void testCheckBaliseC10_2_V2008_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "U44010000026", "U4401", "CFE", Arrays.asList("U4401"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise C10.2", "1998-07-12", result);
    }

    @Test
    public void testCheckBaliseC10_2_V2016_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "U44010000026", "U4401", "CFE", Arrays.asList("U4401"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise C10.2", "1998-07-12", result);
    }

}
