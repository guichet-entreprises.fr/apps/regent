package fr.ge.common.regent.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Initialise les données dans l'ensemble des bases de l'application.
 * <p>
 * Liste des référentiels :
 * <ul>
 * <li>Ge données</li>
 * <li>Référentiel</li>
 * <li>SES
 * </ul>
 * 
 * @author Nicolas Richeton
 * 
 */
public class DevDataInit {

    /** La constante LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(DevDataInit.class);

    /** Le referentiel regent data init. */
    private ReferentielRegentDataInit referentielRegentDataInit = null;

    /**
     * Initialise le.
     *
     * @throws Exception
     *             le exception
     */
    public void init() throws Exception {
        LOGGER.warn("Starting DB init....");

        // Liste des référentiels
        if (this.referentielRegentDataInit != null) {
            this.referentielRegentDataInit.init();
        }
        LOGGER.info("DB init done.");
    }

    /**
     * Get le referentiel data init.
     *
     * @return the referentielDataInit
     */
    public ReferentielRegentDataInit getReferentielDataInit() {
        return this.referentielRegentDataInit;
    }

    /**
     * Set le referentiel data init.
     *
     * @param referentielDataInit
     *            the referentielDataInit to set
     */
    public void setReferentielRegentDataInit(ReferentielRegentDataInit referentielRegentDataInit) {
        this.referentielRegentDataInit = referentielRegentDataInit;
    }

}
