/**
 * 
 */
package fr.ge.common.regent.service.xmlregent.modification;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

import fr.ge.common.regent.service.xmlregent.AbstractServiceGenerationXmlRegentTest;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * Test 10P.
 * 
 * @author $Author: hhichri $
 * @version $Revision: 0 $
 */
public class ServiceGenerationXmlRegent_17P_Test extends AbstractServiceGenerationXmlRegentTest {

    private static final String TYPE_FORMALITE = "modification";
    private static final String TYPE_EVENEMENT = "17P";

    /**
     * Test SONIC-3837
     * 
     * [RECR][FORMS][GUB-300] dossier régularisation : erreur XSD balise C10.2
     * 
     * 
     * 
     * L'utilisateur commence un dossier de régularisation Le dossier est
     * finalisé et envoyé aux partenaires pour traitement
     * 
     * Résultat attendu : la balise C10.2 est valorisée avec la date du jour
     * 
     * Résultat obtenu la balise C10.2 est remplie par un "." [point] cela
     * génère une erreur XSD sur le dossier, qui n'est pas transmis au
     * partenaire
     * 
     * Informations complémentaires :
     * 
     * 2 dossiers de régularisation concernés :<br/>
     * scénario PPEI5 dossier C01010760116 >> évennements : 11P, 54P, 80P <br/>
     * scénario PPEI8 : dossier C01010760207 >> évennements : 11P, 16P, 54P, 80P
     * 
     * 1 dossier de modification également scénario PPEIRL4 : dossier
     * C29030760964 >> évennements : 17P
     * 
     * 
     * 
     * @throws TechnicalException
     * @{@link TechnicalException}
     */
    @Test
    public void testCheckRubriqueC10_2_Test_2008() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C30020001795", "C3002", "CFE", Arrays.asList("C3002", "G3002"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2";

        String result = evaluteXpathFromXml(xpath, xml);

        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());

        Assert.assertEquals("La balise C10.2", date, result);
    }

    /**
     * Test SONIC-3837
     * 
     * [RECR][FORMS][GUB-300] dossier régularisation : erreur XSD balise C10.2
     * 
     * 
     * 
     * L'utilisateur commence un dossier de régularisation Le dossier est
     * finalisé et envoyé aux partenaires pour traitement
     * 
     * Résultat attendu : la balise C10.2 est valorisée avec la date du jour
     * 
     * Résultat obtenu la balise C10.2 est remplie par un "." [point] cela
     * génère une erreur XSD sur le dossier, qui n'est pas transmis au
     * partenaire
     * 
     * Informations complémentaires :
     * 
     * 2 dossiers de régularisation concernés :<br/>
     * scénario PPEI5 dossier C01010760116 >> évennements : 11P, 54P, 80P <br/>
     * scénario PPEI8 : dossier C01010760207 >> évennements : 11P, 16P, 54P, 80P
     * 
     * 1 dossier de modification également scénario PPEIRL4 : dossier
     * C29030760964 >> évennements : 17P
     * 
     * 
     * 
     * @throws TechnicalException
     * @{@link TechnicalException}
     */
    @Test
    public void testCheckRubriqueC10_2_Test_2016() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C30020001795", "C3002", "CFE", Arrays.asList("C3002", "G3002"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2";

        String result = evaluteXpathFromXml(xpath, xml);

        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());

        Assert.assertEquals("La balise C10.2", date, result);
    }

}
