package fr.ge.common.regent.util.mapper.formalite.dialogue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.xmlfield.core.XmlField;
import org.xmlfield.core.XmlFieldFactory;
import org.xmlfield.core.exception.XmlFieldParsingException;

import fr.ge.common.regent.bean.formalite.dialogue.creation.AdresseBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.AutreEtablissementUEBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.DialogueCreationBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.PostalCommuneBean;
import fr.ge.common.regent.bean.formalite.dialogue.regularisation.ActiviteSoumiseQualificationBean;
import fr.ge.common.regent.bean.formalite.dialogue.regularisation.ConjointBean;
import fr.ge.common.regent.bean.formalite.dialogue.regularisation.DialogueRegularisationBean;
import fr.ge.common.regent.bean.formalite.dialogue.regularisation.DirigeantBean;
import fr.ge.common.regent.bean.formalite.dialogue.regularisation.EntrepriseBean;
import fr.ge.common.regent.bean.formalite.dialogue.regularisation.EntrepriseLieeBean;
import fr.ge.common.regent.bean.formalite.dialogue.regularisation.EtablissementBean;
import fr.ge.common.regent.bean.formalite.dialogue.regularisation.PersonneLieeBean;
import fr.ge.common.regent.bean.xml.formalite.dialogue.creation.IDialogueCreation;
import fr.ge.common.regent.bean.xml.formalite.dialogue.regularisation.IDialogueRegularisation;
import fr.ge.common.regent.bean.xml.formalite.dialogue.regularisation.IDirigeantRegularisation;
import fr.ge.common.regent.util.test.AbstractSpringTest;

/**
 * Le Class MapperFormaliteRegularisationUtilsTest.
 */
public class MapperDialogueRegularisationUtilsTest extends AbstractSpringTest {

    /** La constante XF. */
    private static final XmlFieldFactory XF = new XmlFieldFactory(true);

    /** Le mapper formalite regularisation utils. */
    @Autowired
    private MapperDialogueRegularisationUtils mapperFormaliteRegularisationUtils;

    /** mapper formalite creation utils. */
    @Autowired
    private MapperDialogueCreationUtils mapperFormaliteCreationUtils;

    /**
     * Test mapper interface vers bean.
     *
     * @throws XmlFieldParsingException
     *             le xml field parsing exception
     */
    @Test
    public void testMapperInterfaceVersBean() throws XmlFieldParsingException {
        IDialogueRegularisation regul = XF.getXmlField().newObject(IDialogueRegularisation.class);
        regul.setDossierId("1234");

        DialogueRegularisationBean regulBean = new DialogueRegularisationBean();
        this.mapperFormaliteRegularisationUtils.mapper(regul, regulBean);

        assertEquals(Long.valueOf(1234), regulBean.getDossierId());
    }

    /**
     * Test mapper interface vers beanq.
     *
     * @throws XmlFieldParsingException
     *             xml field parsing exception
     */
    @Test
    public void testMapperInterfaceVersBeanq() throws XmlFieldParsingException {
        IDialogueCreation iFormaliteCreation = XF.getXmlField().newObject(IDialogueCreation.class);
        iFormaliteCreation.setDossierId("4321");

        DialogueCreationBean creationBean = new DialogueCreationBean();
        this.mapperFormaliteCreationUtils.mapper(iFormaliteCreation, creationBean);

        assertEquals(Long.valueOf(4321), creationBean.getDossierId());
    }

    /**
     * Test mapper bean vers interface.
     *
     * @throws XmlFieldParsingException
     *             le xml field parsing exception
     */
    @Test
    public void testMapperBeanVersInterface() throws XmlFieldParsingException {
        DialogueRegularisationBean regulBean = new DialogueRegularisationBean();
        regulBean.setDossierId(Long.valueOf(1234));

        IDialogueRegularisation regul = XF.getXmlField().newObject(IDialogueRegularisation.class);
        this.mapperFormaliteRegularisationUtils.mapper(regulBean, regul);

        assertEquals("1234", regul.getDossierId());
    }

    /**
     * Test mapper interface vers bean ko.
     *
     * @throws XmlFieldParsingException
     *             le xml field parsing exception
     */
    @Test(expected = Exception.class)
    public void testMapperInterfaceVersBeanKO() throws XmlFieldParsingException {
        IDialogueRegularisation regul = XF.getXmlField().newObject(IDialogueRegularisation.class);
        regul.setDossierId("toto");

        DialogueRegularisationBean regulBean = new DialogueRegularisationBean();
        this.mapperFormaliteRegularisationUtils.mapper(regul, regulBean);
    }

    /**
     * Test mapper i formalite cessation formalite cessation interface to bean.
     *
     * @throws XmlFieldParsingException
     *             le xml field parsing exception
     * @throws FileNotFoundException
     *             le file not found exception
     */
    @Test
    public void testMapperIFormaliteCessationFormaliteCessationInterfaceToBean() throws XmlFieldParsingException, FileNotFoundException {

        FileInputStream fileInputStream = new FileInputStream("src/test/resources/formalitesXml/formalitesRegul.xml");
        IDialogueRegularisation xml = new XmlField().xmlToObject(fileInputStream, IDialogueRegularisation.class);

        // Init Bean
        DialogueRegularisationBean bean = new DialogueRegularisationBean();
        assertNull(bean.getNumeroDossier());
        this.mapperFormaliteRegularisationUtils.mapper(xml, bean);

        assertEquals("DOSSIER_M93010025216", bean.getNumeroDossier());

        assertEquals("commune_TU_@Signature", bean.getAdresseSignature().getCodePostalCommune().getCommune());
        assertEquals("commune_TU_@Correspondance", bean.getCorrespondanceAdresse().getCodePostalCommune().getCommune());

        assertEquals("commune_TU_@Etablissement", bean.getEntreprise().getEtablissement().getAdresse().getCodePostalCommune().getCommune());

        // entrepriseLieeDomiciliation
        assertEquals("commune_TU_@EntrepriseLieeDom", bean.getEntreprise().getEntrepriseLieeDomiciliation().getAdresse().getCodePostalCommune().getCommune());
        assertEquals("commune_TU_@EntrepriseLieeDomModifAncienneAdresse2", bean.getEntreprise().getEntrepriseLieeDomiciliation().getModifAncienneAdresse2().getCodePostalCommune().getCommune());

        // entrepriseLieeLoueurMandant
        assertEquals("commune_TU_@EntrepriseLieeLoueurMandant", bean.getEntreprise().getEntrepriseLieeLoueurMandant().getAdresse().getCodePostalCommune().getCommune());
        assertEquals("commune_TU_@EntrepriseLieeLoueurMandantModifAncienneAdresse2",
                bean.getEntreprise().getEntrepriseLieeLoueurMandant().getModifAncienneAdresse2().getCodePostalCommune().getCommune());
        // liste -> entrepriseLieesPrecedentsExploitants

        // entrepriseLieesPrecedentsExploitants[0]
        assertEquals("commune_TU_@EntrepriseLieePrecExploit1", bean.getEntreprise().getEntreprisesLieesPrecedentsExploitants().get(0).getAdresse().getCodePostalCommune().getCommune());
        assertEquals("commune_TU_@EntrepriseLieePrecExploit1ModifAncienneAdresse1",
                bean.getEntreprise().getEntreprisesLieesPrecedentsExploitants().get(0).getModifAncienneAdresse2().getCodePostalCommune().getCommune());

        // entrepriseLieesPrecedentsExploitants[1]
        assertEquals("commune_TU_@EntrepriseLieePrecExploit2", bean.getEntreprise().getEntreprisesLieesPrecedentsExploitants().get(1).getAdresse().getCodePostalCommune().getCommune());
        assertEquals("commune_TU_@EntrepriseLieePrecExploit1ModifAncienneAdresse2",
                bean.getEntreprise().getEntreprisesLieesPrecedentsExploitants().get(1).getModifAncienneAdresse2().getCodePostalCommune().getCommune());

        assertEquals("nomDirigeant1", bean.getEntreprise().getDirigeants().get(0).getPpNomNaissance());
        assertEquals("commune_TU_@Dirigeant1_ppAdresseForain", bean.getEntreprise().getDirigeants().get(0).getPpAdresseForain().getCommune());
        assertEquals("commune_TU_@Dirigeant1_ppAdresseAmbulant", bean.getEntreprise().getDirigeants().get(0).getPpAdresseAmbulant().getCommune());
        assertEquals("commune_TU_@Dirigeant1_ppAdresse", bean.getEntreprise().getDirigeants().get(0).getPpAdresse().getCodePostalCommune().getCommune());

        assertEquals("nomDirigeant2", bean.getEntreprise().getDirigeants().get(1).getPpNomNaissance());

    }

    /**
     * Test mapper formalite regul bean i formalite regul bean to interface.
     *
     * @throws XmlFieldParsingException
     *             le xml field parsing exception
     */
    @Test
    public void testMapperFormaliteRegulBeanIFormaliteRegulBeanToInterface() throws XmlFieldParsingException {

        // Init Bean
        DialogueRegularisationBean bean = new DialogueRegularisationBean();
        String numDossier = "NUM_DOSSIER_TU";
        bean.setNumeroDossier(numDossier);

        EntrepriseBean entreprise = new EntrepriseBean();

        // entrepriseLieeDomiciliation

        AdresseBean adresseDom = new AdresseBean();
        PostalCommuneBean codePostalCommune = new PostalCommuneBean();
        codePostalCommune.setCommune("TU_dom");
        adresseDom.setCodePostalCommune(codePostalCommune);

        EntrepriseLieeBean entrepriseLieeDomiciliation = new EntrepriseLieeBean();
        entrepriseLieeDomiciliation.setAdresse(adresseDom);
        entrepriseLieeDomiciliation.setModifAncienneAdresse2(adresseDom);
        entreprise.setEntrepriseLieeDomiciliation(entrepriseLieeDomiciliation);

        EtablissementBean etablissement = new EtablissementBean();
        List<ActiviteSoumiseQualificationBean> activitesSoumisesQualification = new ArrayList<>();
        ActiviteSoumiseQualificationBean activite = new ActiviteSoumiseQualificationBean();
        activite.setNumeroDossier(numDossier);
        activite.setTypePersonne("TU_type_personne");
        activitesSoumisesQualification.add(activite);
        etablissement.setActivitesSoumisesQualification(activitesSoumisesQualification);

        entreprise.setEtablissement(etablissement);

        List<AutreEtablissementUEBean> etablissementUE = new ArrayList<>();
        etablissementUE.add(new AutreEtablissementUEBean());
        entreprise.setEtablissementsUE(etablissementUE);

        List<PersonneLieeBean> personnesLiees = new ArrayList<>();
        personnesLiees.add(new PersonneLieeBean());
        entreprise.setPersonneLiee(personnesLiees);

        // entrepriseLieeLoueurMandant
        AdresseBean adresseLoueur = new AdresseBean();
        PostalCommuneBean codePostalCommuneLoueur = new PostalCommuneBean();
        codePostalCommuneLoueur.setCommune("TU_loueur");
        adresseLoueur.setCodePostalCommune(codePostalCommuneLoueur);

        EntrepriseLieeBean entrepriseLieeLoueur = new EntrepriseLieeBean();
        entrepriseLieeLoueur.setAdresse(adresseLoueur);
        entrepriseLieeLoueur.setModifAncienneAdresse2(adresseLoueur);
        entreprise.setEntrepriseLieeLoueurMandant(entrepriseLieeLoueur);

        // entrepriseLieesPrecedentsExploitants
        List<EntrepriseLieeBean> entrepriseLieesPrecedentsExploitants = new ArrayList<EntrepriseLieeBean>();

        AdresseBean adressePrecExploit = new AdresseBean();
        PostalCommuneBean codePostalCommunePrecExploit = new PostalCommuneBean();
        codePostalCommunePrecExploit.setCommune("TU_PREC");
        adressePrecExploit.setCodePostalCommune(codePostalCommunePrecExploit);

        EntrepriseLieeBean e = new EntrepriseLieeBean();
        e.setAdresse(adressePrecExploit);
        e.setModifAncienneAdresse2(adressePrecExploit);
        entrepriseLieesPrecedentsExploitants.add(e);
        entreprise.setEntreprisesLieesPrecedentsExploitants(entrepriseLieesPrecedentsExploitants);

        List<DirigeantBean> dirigeants = new ArrayList<DirigeantBean>();
        DirigeantBean dirigeant = new DirigeantBean();
        dirigeant.setNumeroDossier(numDossier);

        ConjointBean conjoint = new ConjointBean();
        conjoint.setNumeroDossier(numDossier);
        dirigeant.setConjoint(conjoint);

        AdresseBean modifAncienDomicile = new AdresseBean();
        PostalCommuneBean cpModifAncDom = new PostalCommuneBean();
        cpModifAncDom.setCommune("TU_cpModifAncDom");

        modifAncienDomicile.setCodePostalCommune(cpModifAncDom);

        dirigeant.setModifAncienDomicile(modifAncienDomicile);
        AdresseBean ppAdresse = new AdresseBean();
        PostalCommuneBean ppAdresseCodPost = new PostalCommuneBean();
        ppAdresseCodPost.setCommune("TU_ppAdresse");
        ppAdresse.setCodePostalCommune(ppAdresseCodPost);
        dirigeant.setPpAdresse(ppAdresse);

        PostalCommuneBean ppAdresseAmbulant = new PostalCommuneBean();
        ppAdresseAmbulant.setCommune("TU_ambulant");
        dirigeant.setPpAdresseAmbulant(ppAdresseAmbulant);

        PostalCommuneBean ppAdresseForain = new PostalCommuneBean();
        ppAdresseForain.setCommune("TU_forain");
        dirigeant.setPpAdresseForain(ppAdresseForain);

        dirigeants.add(dirigeant);

        entreprise.setDirigeants(dirigeants);

        bean.setEntreprise(entreprise);

        // Init Interface
        IDialogueRegularisation xml = new XmlField().newObject(IDialogueRegularisation.class);
        this.mapperFormaliteRegularisationUtils.mapper(bean, xml);

        assertNotNull(xml);
        assertEquals(numDossier, xml.getNumeroDossier());

        // TU EntrepriseLieeLoueurMandant
        assertEquals("TU_dom", xml.getEntreprise().getEntrepriseLieeDomiciliation().getAdresse().getCodePostalCommune().getCommune());
        assertEquals("TU_dom", xml.getEntreprise().getEntrepriseLieeDomiciliation().getModifAncienneAdresse2().getCodePostalCommune().getCommune());

        // TU EntrepriseLieeLoueurMandant
        assertEquals("TU_loueur", xml.getEntreprise().getEntrepriseLieeLoueurMandant().getAdresse().getCodePostalCommune().getCommune());
        assertEquals("TU_loueur", xml.getEntreprise().getEntrepriseLieeLoueurMandant().getModifAncienneAdresse2().getCodePostalCommune().getCommune());

        // TU EntrepriseLieesPrecedentsExploitant
        assertEquals("TU_PREC", xml.getEntreprise().getEntreprisesLieesPrecedentsExploitants()[0].getAdresse().getCodePostalCommune().getCommune());
        assertEquals("TU_PREC", xml.getEntreprise().getEntreprisesLieesPrecedentsExploitants()[0].getModifAncienneAdresse2().getCodePostalCommune().getCommune());

        // TU Dirigeants
        IDirigeantRegularisation iDirigeantRegularisation = xml.getEntreprise().getDirigeants()[0];
        assertEquals(numDossier, iDirigeantRegularisation.getNumeroDossier());

        // TU Dirigeants[0]->Conjoint
        assertEquals(numDossier, iDirigeantRegularisation.getConjoint().getNumeroDossier());

        assertEquals("TU_cpModifAncDom", iDirigeantRegularisation.getModifAncienDomicile().getCodePostalCommune().getCommune());

        assertEquals("TU_ppAdresse", iDirigeantRegularisation.getPpAdresse().getCodePostalCommune().getCommune());
        assertEquals("TU_ambulant", iDirigeantRegularisation.getPpAdresseAmbulant().getCommune());
        assertEquals("TU_forain", iDirigeantRegularisation.getPpAdresseForain().getCommune());

    }

}
