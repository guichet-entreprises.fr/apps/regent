package fr.ge.common.regent.persistance.dao.referentiel;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import fr.ge.common.regent.bean.modele.referentiel.ENormeEvenement;
import fr.ge.common.regent.bean.modele.referentiel.ENormeRubrique;
import fr.ge.common.regent.util.test.AbstractSpringTest;

public class NormeRubriqueDaoTest extends AbstractSpringTest {

    /** La constante VERSION. */
    private static final String VERSION = "3";

    /** Le norme evenement dao. */
    @Autowired
    private NormeEvenementDao normeEvenementDao;

    /** Le norme Rubrique DAO */
    @Autowired
    private NormeRubriqueDao normeRubrique;

    /**
     * Set le up.
     */
    @Before
    @Transactional(readOnly = false, value = "regent")
    public void setUp() {
        ENormeEvenement normEvt = new ENormeEvenement();
        normEvt.setId(10);
        normEvt.setEvenement("EVENEMENT_10");
        normEvt.setRubrique("RUBRIQUE A");
        normEvt.setVersion(VERSION);
        normEvt.setCondition(Boolean.TRUE);
        normeEvenementDao.save(normEvt);

        ENormeEvenement normEvt2 = new ENormeEvenement();
        normEvt2.setId(20);
        normEvt2.setEvenement("EVENEMENT_20");
        normEvt2.setRubrique("RUBRIQUE B");
        normEvt2.setVersion(VERSION);
        normEvt2.setCondition(Boolean.TRUE);
        normeEvenementDao.save(normEvt2);

        ENormeRubrique normRubrique = new ENormeRubrique();
        normRubrique.setId(10000);
        normRubrique.setCondition("VRAI");
        normRubrique.setLibelle("RUBRIQUE A");
        normRubrique.setPath("/path/rubriqueA:Emplacement");
        normRubrique.setRubrique("RUBRIQUE A");
        normRubrique.setGroupe("G001");
        normeRubrique.save(normRubrique);

        ENormeRubrique normRubrique2 = new ENormeRubrique();
        normRubrique2.setId(20000);
        normRubrique2.setCondition("VRAI");
        normRubrique2.setLibelle("RUBRIQUE B");
        normRubrique2.setPath("/path/rubriqueB:Emplacement");
        normRubrique2.setRubrique("RUBRIQUE B");
        normRubrique2.setGroupe("G002");
        normeRubrique.save(normRubrique2);
    }

    @Test
    @Transactional(readOnly = false, value = "regent")
    public void testGetRubriquesPathByEvenementAndVersion() throws Exception {

        List<String> listEvts = new ArrayList<String>();
        listEvts.add("EVENEMENT_10");
        listEvts.add("EVENEMENT_20");
        List<ENormeRubrique> rubriqueByEvenementAndVersion = this.normeRubrique.getRubriquesPathByEvenementAndVersion(VERSION, listEvts);

        assertNotNull(rubriqueByEvenementAndVersion);
        assertEquals(2, rubriqueByEvenementAndVersion.size());
        assertThat(rubriqueByEvenementAndVersion.get(0).getId()).isEqualTo(10000);
        assertThat(rubriqueByEvenementAndVersion.get(0).getCondition()).isEqualTo("VRAI");
        assertThat(rubriqueByEvenementAndVersion.get(0).getLibelle()).isEqualTo("RUBRIQUE A");
        assertThat(rubriqueByEvenementAndVersion.get(0).getPath()).isEqualTo("/path/rubriqueA:Emplacement");
        assertThat(rubriqueByEvenementAndVersion.get(0).getRubrique()).isEqualTo("RUBRIQUE A");
        assertThat(rubriqueByEvenementAndVersion.get(0).getGroupe()).isEqualTo("G001");

        assertThat(rubriqueByEvenementAndVersion.get(1).getId()).isEqualTo(20000);
        assertThat(rubriqueByEvenementAndVersion.get(1).getCondition()).isEqualTo("VRAI");
        assertThat(rubriqueByEvenementAndVersion.get(1).getLibelle()).isEqualTo("RUBRIQUE B");
        assertThat(rubriqueByEvenementAndVersion.get(1).getPath()).isEqualTo("/path/rubriqueB:Emplacement");
        assertThat(rubriqueByEvenementAndVersion.get(1).getRubrique()).isEqualTo("RUBRIQUE B");
        assertThat(rubriqueByEvenementAndVersion.get(1).getGroupe()).isEqualTo("G002");
    }

    @Test
    @Transactional(readOnly = false, value = "regent")
    public void testGetRubriquesPathByEvenementAndVersionReturnEmptyList() throws Exception {

        List<String> listEvts = new ArrayList<String>();
        listEvts.add("EVENEMENT_10");
        listEvts.add("EVENEMENT_20");
        List<ENormeRubrique> rubriqueByEvenementAndVersion = this.normeRubrique.getRubriquesPathByEvenementAndVersion("2", listEvts);

        assertNotNull(rubriqueByEvenementAndVersion);
        assertEquals(0, rubriqueByEvenementAndVersion.size());
    }

}
