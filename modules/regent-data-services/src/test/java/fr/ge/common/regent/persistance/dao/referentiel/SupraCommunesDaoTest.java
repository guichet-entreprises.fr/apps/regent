package fr.ge.common.regent.persistance.dao.referentiel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import fr.ge.common.referentiel.bean.IRefBean;
import fr.ge.common.referentiel.dao.SupraCommunesDao;
import fr.ge.common.regent.util.test.AbstractSpringTest;

/**
 * Le Class SupraCommunesDaoTest.
 */
public class SupraCommunesDaoTest extends AbstractSpringTest {

    /** Le supra communes dao. */
    @Autowired
    private SupraCommunesDao supraCommunesDao;

    /**
     * Test chercher commune par departement.
     */
    @Test
    @Transactional(readOnly = true, value = "regent")
    public void testChercherCommuneParDepartement() {
        String codeDepartement = "29";
        List<IRefBean> chercherCommuneParDepartement = supraCommunesDao.chercherCommuneParDepartement(codeDepartement);

        assertNotNull(chercherCommuneParDepartement);
        assertEquals(302, chercherCommuneParDepartement.size());
        assertEquals("ARGOL", chercherCommuneParDepartement.get(0).getElementLabel());
    }

    /**
     * Test chercher commune par code postal.
     */
    @Test
    @Transactional(readOnly = true, value = "regent")
    public void testChercherCommuneParCodePostal() {
        String codePostal = "49007";
        List<IRefBean> chercherCommuneParCodePostal = supraCommunesDao.chercherCommuneParCodePostal(codePostal);
        assertNotNull(chercherCommuneParCodePostal);
        assertEquals(2, chercherCommuneParCodePostal.size());
        assertEquals("ANGERS", chercherCommuneParCodePostal.get(0).getElementLabel());
        assertEquals("SAINT-BARTHELEMY-D'ANJOU", chercherCommuneParCodePostal.get(1).getElementLabel());

    }

}
