package fr.ge.common.regent.util.mapper.formalite.dialogue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.xmlfield.core.XmlField;
import org.xmlfield.core.exception.XmlFieldParsingException;
import org.xmlfield.core.types.XmlString;

import fr.ge.common.regent.bean.formalite.dialogue.creation.AdresseBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.PostalCommuneBean;
import fr.ge.common.regent.bean.formalite.dialogue.modification.ActiviteSoumiseQualificationBean;
import fr.ge.common.regent.bean.formalite.dialogue.modification.ConjointBean;
import fr.ge.common.regent.bean.formalite.dialogue.modification.DialogueModificationBean;
import fr.ge.common.regent.bean.formalite.dialogue.modification.DirigeantBean;
import fr.ge.common.regent.bean.formalite.dialogue.modification.EirlBean;
import fr.ge.common.regent.bean.formalite.dialogue.modification.EntrepriseBean;
import fr.ge.common.regent.bean.formalite.dialogue.modification.EntrepriseLieeBean;
import fr.ge.common.regent.bean.formalite.dialogue.modification.EtablissementBean;
import fr.ge.common.regent.bean.xml.formalite.dialogue.modification.IDialogueModification;
import fr.ge.common.regent.bean.xml.formalite.dialogue.modification.IEirlModification;
import fr.ge.common.regent.util.test.AbstractSpringTest;

/**
 * Le Class MapperFormaliteModificationUtilsTest.
 */
public class MapperDialogueModificationUtilsTest extends AbstractSpringTest {

    /** La constante XF. */
    private static final XmlField XF = new XmlField();

    /** Le mapper. */
    @Autowired
    private MapperDialogueModificationUtils mapper;

    /**
     * Test mapper i formalite modification formalite modification bean.
     * 
     * @throws FileNotFoundException
     *             le file not found exception
     * @throws XmlFieldParsingException
     *             le xml field parsing exception
     */
    @Test
    public void testMapperIFormaliteModificationFormaliteModificationBean() throws FileNotFoundException, XmlFieldParsingException {
        FileInputStream fileInputStream = new FileInputStream("src/test/resources/formalitesXml/formalitesModification.xml");
        ;
        IDialogueModification xml = XF.xmlToObject(fileInputStream, IDialogueModification.class);

        DialogueModificationBean bean = new DialogueModificationBean();
        assertNull(bean.getNumeroDossier());

        this.mapper.mapper(xml, bean);

        assertEquals("DOSSIER_M93010025216", bean.getNumeroDossier());
        assertEquals("commune_TU_@1", bean.getEntreprise().getEtablissement().getAdresse().getCodePostalCommune().getCommune());
        assertEquals("commune_TU_@2", bean.getEntreprise().getEtablissement().getModifAncienneAdresse1().getCodePostalCommune().getCommune());
        assertEquals("commune_TU_@LieeDom", bean.getEntreprise().getEntrepriseLieeDomiciliation().getAdresse().getCodePostalCommune().getCommune());
        assertEquals("commune_TU_@LoueurMandant", bean.getEntreprise().getEntrepriseLieeLoueurMandant().getAdresse().getCodePostalCommune().getCommune());

        // liste EntrePrecExploit
        assertEquals(2, bean.getEntreprise().getEntreprisesLieesPrecedentsExploitants().size());

        // EntrePrecExploit 1
        assertEquals("commune_TU_@PrecExploit1", bean.getEntreprise().getEntreprisesLieesPrecedentsExploitants().get(0).getAdresse().getCodePostalCommune().getCommune());
        // EntrePrecExploit 2
        assertEquals("commune_TU_@PrecExploit2", bean.getEntreprise().getEntreprisesLieesPrecedentsExploitants().get(1).getAdresse().getCodePostalCommune().getCommune());

        // liste dirigeants
        assertEquals(2, bean.getEntreprise().getDirigeants().size());

        // dirigeant 1
        assertEquals("nomDirigeant1", bean.getEntreprise().getDirigeants().get(0).getPpNomNaissance());
        assertEquals("commune_TU_@Dirigeant1_ppAdresse", bean.getEntreprise().getDirigeants().get(0).getPpAdresse().getCodePostalCommune().getCommune());
        assertEquals("commune_TU_@Dirigeant1_modifAncienDomicile", bean.getEntreprise().getDirigeants().get(0).getModifAncienDomicile().getCodePostalCommune().getCommune());

        // dirigeant 1 -> modifsIdentitePrenoms[]
        assertEquals(2, bean.getEntreprise().getDirigeants().get(0).getModifIdentiteNomPrenom().size());
        assertEquals("modifPrenomA", bean.getEntreprise().getDirigeants().get(0).getModifIdentiteNomPrenom().get(0));
        assertEquals("modifPrenomB", bean.getEntreprise().getDirigeants().get(0).getModifIdentiteNomPrenom().get(1));

        assertEquals("commune_TU_@Conjoint1_ppAdresse", bean.getEntreprise().getDirigeants().get(0).getConjoint().getPpAdresse().getCodePostalCommune().getCommune());

        // dirigeant 2
        assertEquals("nomDirigeant2", bean.getEntreprise().getDirigeants().get(1).getPpNomNaissance());

    }

    /**
     * Test mapper i formalite modification formalite modification bean.
     * 
     * @throws FileNotFoundException
     *             le file not found exception
     * @throws XmlFieldParsingException
     *             le xml field parsing exception
     */
    @Test
    public void testMapperIFormaliteModificationFormaliteModificationBeanEirl() throws FileNotFoundException, XmlFieldParsingException {
        FileInputStream fileInputStream = new FileInputStream("src/test/resources/formalitesXml/formalitesModificationAvecEirl.xml");
        IDialogueModification xml = XF.xmlToObject(fileInputStream, IDialogueModification.class);

        DialogueModificationBean bean = new DialogueModificationBean();
        assertNull(bean.getNumeroDossier());

        this.mapper.mapper(xml, bean);
        assertEquals("denomination", bean.getEntreprise().getEirl().getDenomination());
        assertEquals("modifNouvelleDenominationEIRL", bean.getEntreprise().getEirl().getModifNouvelleDenominationEIRL());
        assertEquals("10/10/2014", bean.getEntreprise().getEirl().getDateClotureExerciceComptable());
        assertEquals("modifDateClotureExerciceComptable", bean.getEntreprise().getEirl().getModifDateClotureExerciceComptable());
        assertEquals("registreDepot", bean.getEntreprise().getEirl().getRegistreDepot());
        assertEquals("boulevard de bouvets", bean.getEntreprise().getEirl().getModifAdresseEIrl().getNomVoie());
        assertEquals("SOUSSE", bean.getEntreprise().getEirl().getModifAdresseEIrl().getVille());
        assertEquals("bienImmo", bean.getEntreprise().getEirl().getDefinitionPatrimoine().get(0));
        assertEquals("bienRapportEvaluation", bean.getEntreprise().getEirl().getDefinitionPatrimoine().get(1));
        assertEquals("bienIndivis", bean.getEntreprise().getEirl().getDefinitionPatrimoine().get(2));
    }

    /**
     * Test mapper formalite modification bean i formalite modification.
     * 
     * @throws XmlFieldParsingException
     *             le xml field parsing exception
     */
    @Test
    public void testMapperFormaliteModificationBeanIFormaliteModification() throws XmlFieldParsingException {

        // Init bean
        DialogueModificationBean b = new DialogueModificationBean();
        b.setNumeroDossier("DOSSIER_M93010025216");
        // bean entreprise
        EntrepriseBean entreprise = new EntrepriseBean();
        // entreprise->etablissement
        EtablissementBean etablissement = new EtablissementBean();

        ActiviteSoumiseQualificationBean activite = new ActiviteSoumiseQualificationBean();
        activite.setNumeroDossier("DOSSIER_M93010025216");
        activite.setTypePersonne("TU_type_personne");
        List<ActiviteSoumiseQualificationBean> activites = new ArrayList<>();
        activites.add(activite);
        etablissement.setActivitesSoumisesQualification(activites);

        AdresseBean a1 = new AdresseBean();
        PostalCommuneBean codePostalCommune1 = new PostalCommuneBean();
        codePostalCommune1.setCommune("commune_TU_@1");
        a1.setCodePostalCommune(codePostalCommune1);
        etablissement.setAdresse(a1);

        AdresseBean a2 = new AdresseBean();
        PostalCommuneBean codePostalCommune2 = new PostalCommuneBean();
        codePostalCommune2.setCommune("commune_TU_@2");
        a2.setCodePostalCommune(codePostalCommune2);
        etablissement.setModifAncienneAdresse1(a2);

        entreprise.setEtablissement(etablissement);

        // entreprise->etablissement->eLieeLieeDom
        EntrepriseLieeBean eLieeDom = new EntrepriseLieeBean();
        AdresseBean aLieeDom = new AdresseBean();
        PostalCommuneBean codePostalCommuneLieeDom = new PostalCommuneBean();
        codePostalCommuneLieeDom.setCommune("commune_TU_@LieeDom");
        aLieeDom.setCodePostalCommune(codePostalCommuneLieeDom);
        eLieeDom.setAdresse(aLieeDom);

        entreprise.setEntrepriseLieeDomiciliation(eLieeDom);

        // entreprise->etablissement->eLieeLoueurMandant
        EntrepriseLieeBean eLieeLoueurMandant = new EntrepriseLieeBean();
        AdresseBean aLoueurMandant = new AdresseBean();
        PostalCommuneBean codePostalCommuneLoueurMandant = new PostalCommuneBean();
        codePostalCommuneLoueurMandant.setCommune("commune_TU_@LoueurMandant");
        aLoueurMandant.setCodePostalCommune(codePostalCommuneLoueurMandant);
        eLieeLoueurMandant.setAdresse(aLoueurMandant);

        entreprise.setEntrepriseLieeLoueurMandant(eLieeLoueurMandant);

        // entreprise->etablissement->EntreprisesLieesPrecedentsExploitants[]
        EntrepriseLieeBean eLieePrecExploit = new EntrepriseLieeBean();
        AdresseBean aPrecExploit = new AdresseBean();
        PostalCommuneBean codePostalCommunePrecExploit = new PostalCommuneBean();
        codePostalCommunePrecExploit.setCommune("commune_TU_@PrecExploit1");
        aPrecExploit.setCodePostalCommune(codePostalCommunePrecExploit);
        eLieePrecExploit.setAdresse(aPrecExploit);

        List<EntrepriseLieeBean> listEntrLieePrecExploit = new ArrayList<EntrepriseLieeBean>();
        listEntrLieePrecExploit.add(eLieePrecExploit);
        entreprise.setEntreprisesLieesPrecedentsExploitants(listEntrLieePrecExploit);

        // entreprise->dirigeants
        List<DirigeantBean> dirigeants = new ArrayList<DirigeantBean>();
        DirigeantBean d1 = new DirigeantBean();
        d1.setPpNomNaissance("nomDirigeant1");

        AdresseBean aD1PpAdresse = new AdresseBean();
        PostalCommuneBean codePostalCommunePpNomNaissance = new PostalCommuneBean();
        codePostalCommunePpNomNaissance.setCommune("commune_TU_@Dirigeant1_ppAdresse");
        aD1PpAdresse.setCodePostalCommune(codePostalCommunePpNomNaissance);
        d1.setPpAdresse(aD1PpAdresse);

        AdresseBean aD1ModifAncAdresse = new AdresseBean();
        PostalCommuneBean codePostalCommunesD1ModifAncAdr = new PostalCommuneBean();
        codePostalCommunesD1ModifAncAdr.setCommune("commune_TU_@Dirigeant1_modifAncienDomicile");
        aD1ModifAncAdresse.setCodePostalCommune(codePostalCommunesD1ModifAncAdr);
        d1.setModifAncienDomicile(aD1ModifAncAdresse);

        List<String> modifIdentiteNomPrenoms = new ArrayList<String>();
        modifIdentiteNomPrenoms.add("modifPrenomA");
        modifIdentiteNomPrenoms.add("modifPrenomB");
        d1.setModifIdentiteNomPrenom(modifIdentiteNomPrenoms);

        AdresseBean adresseConjoint = new AdresseBean();
        PostalCommuneBean codePostalCommunesConjointAdresse = new PostalCommuneBean();
        codePostalCommunesConjointAdresse.setCommune("commune_TU_@Conjoint1_ppAdresse");
        adresseConjoint.setCodePostalCommune(codePostalCommunesConjointAdresse);
        ConjointBean conjoint = new ConjointBean();
        conjoint.setPpAdresse(adresseConjoint);
        d1.setConjoint(conjoint);

        d1.setDeptAncienDomicile("93");
        dirigeants.add(d1);

        entreprise.setDirigeants(dirigeants);

        b.setEntreprise(entreprise);
        //

        // Init Interface
        IDialogueModification iFormaliteModification = XF.newObject(IDialogueModification.class);

        this.mapper.mapper(b, iFormaliteModification);

        assertEquals("DOSSIER_M93010025216", iFormaliteModification.getNumeroDossier());
        assertEquals("commune_TU_@1", iFormaliteModification.getEntreprise().getEtablissement().getAdresse().getCodePostalCommune().getCommune());
        assertEquals("commune_TU_@2", iFormaliteModification.getEntreprise().getEtablissement().getModifAncienneAdresse1().getCodePostalCommune().getCommune());
        assertEquals("commune_TU_@LieeDom", iFormaliteModification.getEntreprise().getEntrepriseLieeDomiciliation().getAdresse().getCodePostalCommune().getCommune());
        assertEquals("commune_TU_@LoueurMandant", iFormaliteModification.getEntreprise().getEntrepriseLieeLoueurMandant().getAdresse().getCodePostalCommune().getCommune());

        // liste EntrePrecExploit
        assertEquals(1, iFormaliteModification.getEntreprise().getEntreprisesLieesPrecedentsExploitants().length);

        // EntrePrecExploit 1
        assertEquals("commune_TU_@PrecExploit1", iFormaliteModification.getEntreprise().getEntreprisesLieesPrecedentsExploitants()[0].getAdresse().getCodePostalCommune().getCommune());

        // liste dirigeants
        assertEquals(1, iFormaliteModification.getEntreprise().getDirigeants().length);

        // dirigeant 1
        assertEquals("nomDirigeant1", iFormaliteModification.getEntreprise().getDirigeants()[0].getPpNomNaissance());
        assertEquals("commune_TU_@Dirigeant1_ppAdresse", iFormaliteModification.getEntreprise().getDirigeants()[0].getPpAdresse().getCodePostalCommune().getCommune());
        assertEquals("commune_TU_@Dirigeant1_modifAncienDomicile", iFormaliteModification.getEntreprise().getDirigeants()[0].getModifAncienDomicile().getCodePostalCommune().getCommune());

        // dirigeant 1 -> modifsIdentitePrenoms[]
        assertEquals(2, iFormaliteModification.getEntreprise().getDirigeants()[0].getModifIdentiteNomPrenom().length);
        XmlString prenomA = iFormaliteModification.getEntreprise().getDirigeants()[0].getModifIdentiteNomPrenom()[0];
        assertEquals("modifPrenomA", prenomA.getString());
        XmlString prenomB = iFormaliteModification.getEntreprise().getDirigeants()[0].getModifIdentiteNomPrenom()[1];
        assertEquals("modifPrenomB", prenomB.getString());

        assertEquals("commune_TU_@Conjoint1_ppAdresse", iFormaliteModification.getEntreprise().getDirigeants()[0].getConjoint().getPpAdresse().getCodePostalCommune().getCommune());

        assertEquals("93", iFormaliteModification.getEntreprise().getDirigeants()[0].getDeptAncienDomicile());

        Assert.assertTrue(XF.objectToXml(iFormaliteModification).contains("<deptAncienDomicile>93</deptAncienDomicile>"));

    }

    /**
     * Test mapper formalite modification bean i formalite modification eirl.
     * 
     * @throws XmlFieldParsingException
     *             xml field parsing exception
     */
    @Test
    public void testMapperFormaliteModificationBeanIFormaliteModificationEirl() throws XmlFieldParsingException {

        // Init bean
        DialogueModificationBean b = new DialogueModificationBean();
        b.setNumeroDossier("DOSSIER_M93010025216");
        // bean entreprise
        EntrepriseBean entreprise = new EntrepriseBean();
        // entreprise->etablissement
        EtablissementBean etablissement = new EtablissementBean();
        AdresseBean a1 = new AdresseBean();
        PostalCommuneBean codePostalCommune1 = new PostalCommuneBean();
        codePostalCommune1.setCommune("commune_TU_@1");
        a1.setCodePostalCommune(codePostalCommune1);
        etablissement.setAdresse(a1);

        AdresseBean a2 = new AdresseBean();
        PostalCommuneBean codePostalCommune2 = new PostalCommuneBean();
        codePostalCommune2.setCommune("commune_TU_@2");
        a2.setCodePostalCommune(codePostalCommune2);
        etablissement.setModifAncienneAdresse1(a2);

        entreprise.setEtablissement(etablissement);

        // entreprise->etablissement->eLieeLieeDom
        EntrepriseLieeBean eLieeDom = new EntrepriseLieeBean();
        AdresseBean aLieeDom = new AdresseBean();
        PostalCommuneBean codePostalCommuneLieeDom = new PostalCommuneBean();
        codePostalCommuneLieeDom.setCommune("commune_TU_@LieeDom");
        aLieeDom.setCodePostalCommune(codePostalCommuneLieeDom);
        eLieeDom.setAdresse(aLieeDom);

        entreprise.setEntrepriseLieeDomiciliation(eLieeDom);

        // entreprise->etablissement->eLieeLoueurMandant
        EntrepriseLieeBean eLieeLoueurMandant = new EntrepriseLieeBean();
        AdresseBean aLoueurMandant = new AdresseBean();
        PostalCommuneBean codePostalCommuneLoueurMandant = new PostalCommuneBean();
        codePostalCommuneLoueurMandant.setCommune("commune_TU_@LoueurMandant");
        aLoueurMandant.setCodePostalCommune(codePostalCommuneLoueurMandant);
        eLieeLoueurMandant.setAdresse(aLoueurMandant);

        entreprise.setEntrepriseLieeLoueurMandant(eLieeLoueurMandant);

        // entreprise->etablissement->EntreprisesLieesPrecedentsExploitants[]
        EntrepriseLieeBean eLieePrecExploit = new EntrepriseLieeBean();
        AdresseBean aPrecExploit = new AdresseBean();
        PostalCommuneBean codePostalCommunePrecExploit = new PostalCommuneBean();
        codePostalCommunePrecExploit.setCommune("commune_TU_@PrecExploit1");
        aPrecExploit.setCodePostalCommune(codePostalCommunePrecExploit);
        eLieePrecExploit.setAdresse(aPrecExploit);

        List<EntrepriseLieeBean> listEntrLieePrecExploit = new ArrayList<EntrepriseLieeBean>();
        listEntrLieePrecExploit.add(eLieePrecExploit);
        entreprise.setEntreprisesLieesPrecedentsExploitants(listEntrLieePrecExploit);

        // entreprise->dirigeants
        List<DirigeantBean> dirigeants = new ArrayList<DirigeantBean>();
        DirigeantBean d1 = new DirigeantBean();
        d1.setPpNomNaissance("nomDirigeant1");

        AdresseBean aD1PpAdresse = new AdresseBean();
        PostalCommuneBean codePostalCommunePpNomNaissance = new PostalCommuneBean();
        codePostalCommunePpNomNaissance.setCommune("commune_TU_@Dirigeant1_ppAdresse");
        aD1PpAdresse.setCodePostalCommune(codePostalCommunePpNomNaissance);
        d1.setPpAdresse(aD1PpAdresse);

        AdresseBean aD1ModifAncAdresse = new AdresseBean();
        PostalCommuneBean codePostalCommunesD1ModifAncAdr = new PostalCommuneBean();
        codePostalCommunesD1ModifAncAdr.setCommune("commune_TU_@Dirigeant1_modifAncienDomicile");
        aD1ModifAncAdresse.setCodePostalCommune(codePostalCommunesD1ModifAncAdr);
        d1.setModifAncienDomicile(aD1ModifAncAdresse);

        List<String> modifIdentiteNomPrenoms = new ArrayList<String>();
        modifIdentiteNomPrenoms.add("modifPrenomA");
        modifIdentiteNomPrenoms.add("modifPrenomB");
        d1.setModifIdentiteNomPrenom(modifIdentiteNomPrenoms);

        AdresseBean adresseConjoint = new AdresseBean();
        PostalCommuneBean codePostalCommunesConjointAdresse = new PostalCommuneBean();
        codePostalCommunesConjointAdresse.setCommune("commune_TU_@Conjoint1_ppAdresse");
        adresseConjoint.setCodePostalCommune(codePostalCommunesConjointAdresse);
        ConjointBean conjoint = new ConjointBean();
        conjoint.setPpAdresse(adresseConjoint);
        d1.setConjoint(conjoint);

        dirigeants.add(d1);

        entreprise.setDirigeants(dirigeants);

        EirlBean eirl = new EirlBean();
        eirl.setActivitesAccessoiresBICBNC("activitesAccessoiresBICBNC");
        eirl.setDateClotureExerciceComptable("10/10/2015");
        List<String> definitionPatriomoine = new ArrayList<String>();
        definitionPatriomoine.add("def1");
        definitionPatriomoine.add("def2");
        eirl.setDefinitionPatrimoine(definitionPatriomoine);
        AdresseBean modifAdresseEIrl = new AdresseBean();
        modifAdresseEIrl.setCodePays("500");
        modifAdresseEIrl.setComplementAdresse("complementAdresse");
        modifAdresseEIrl.setNomVoie("nomVoie");
        eirl.setModifAdresseEIrl(modifAdresseEIrl);
        eirl.setDenomination("denomination");
        eirl.setLieuRegistreDepot("lieuRegistreDepot");
        entreprise.setEirl(eirl);

        b.setEntreprise(entreprise);
        //

        // Init Interface
        IDialogueModification iFormaliteModification = XF.newObject(IDialogueModification.class);

        this.mapper.mapper(b, iFormaliteModification);

        assertEquals("DOSSIER_M93010025216", iFormaliteModification.getNumeroDossier());
        assertEquals("commune_TU_@1", iFormaliteModification.getEntreprise().getEtablissement().getAdresse().getCodePostalCommune().getCommune());
        assertEquals("commune_TU_@2", iFormaliteModification.getEntreprise().getEtablissement().getModifAncienneAdresse1().getCodePostalCommune().getCommune());
        assertEquals("commune_TU_@LieeDom", iFormaliteModification.getEntreprise().getEntrepriseLieeDomiciliation().getAdresse().getCodePostalCommune().getCommune());
        assertEquals("commune_TU_@LoueurMandant", iFormaliteModification.getEntreprise().getEntrepriseLieeLoueurMandant().getAdresse().getCodePostalCommune().getCommune());

        // liste EntrePrecExploit
        assertEquals(1, iFormaliteModification.getEntreprise().getEntreprisesLieesPrecedentsExploitants().length);

        // EntrePrecExploit 1
        assertEquals("commune_TU_@PrecExploit1", iFormaliteModification.getEntreprise().getEntreprisesLieesPrecedentsExploitants()[0].getAdresse().getCodePostalCommune().getCommune());

        // liste dirigeants
        assertEquals(1, iFormaliteModification.getEntreprise().getDirigeants().length);

        // dirigeant 1
        assertEquals("nomDirigeant1", iFormaliteModification.getEntreprise().getDirigeants()[0].getPpNomNaissance());
        assertEquals("commune_TU_@Dirigeant1_ppAdresse", iFormaliteModification.getEntreprise().getDirigeants()[0].getPpAdresse().getCodePostalCommune().getCommune());
        assertEquals("commune_TU_@Dirigeant1_modifAncienDomicile", iFormaliteModification.getEntreprise().getDirigeants()[0].getModifAncienDomicile().getCodePostalCommune().getCommune());

        // dirigeant 1 -> modifsIdentitePrenoms[]
        assertEquals(2, iFormaliteModification.getEntreprise().getDirigeants()[0].getModifIdentiteNomPrenom().length);
        XmlString prenomA = iFormaliteModification.getEntreprise().getDirigeants()[0].getModifIdentiteNomPrenom()[0];
        assertEquals("modifPrenomA", prenomA.getString());
        XmlString prenomB = iFormaliteModification.getEntreprise().getDirigeants()[0].getModifIdentiteNomPrenom()[1];
        assertEquals("modifPrenomB", prenomB.getString());

        assertEquals("commune_TU_@Conjoint1_ppAdresse", iFormaliteModification.getEntreprise().getDirigeants()[0].getConjoint().getPpAdresse().getCodePostalCommune().getCommune());

        // EIRL
        IEirlModification iEirl = iFormaliteModification.getEntreprise().getEirl();
        assertEquals("10/10/2015", iEirl.getDateClotureExerciceComptable());
        assertEquals("500", iEirl.getModifAdresseEIrl().getCodePays());
        assertEquals("complementAdresse", iEirl.getModifAdresseEIrl().getComplementAdresse());
        assertEquals("nomVoie", iEirl.getModifAdresseEIrl().getNomVoie());
        assertEquals("denomination", iEirl.getDenomination());
        assertEquals("lieuRegistreDepot", iEirl.getLieuRegistreDepot());

    }

}
