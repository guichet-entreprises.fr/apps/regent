package fr.ge.common.regent.util.mapper.formalite.profil;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.xmlfield.core.XmlFieldFactory;
import org.xmlfield.core.exception.XmlFieldParsingException;
import org.xmlfield.core.types.XmlString;

import fr.ge.common.regent.bean.formalite.profil.regularisation.ProfilRegularisationBean;
import fr.ge.common.regent.bean.xml.formalite.profil.creation.ICfe;
import fr.ge.common.regent.bean.xml.formalite.profil.regularisation.IProfilRegularisation;
import fr.ge.common.regent.util.test.AbstractSpringTest;

/**
 * Le Class MapperProfilRegularisationUtilsTest.
 */
public class MapperProfilRegularisationUtilsTest extends AbstractSpringTest {

    /** La constante XF. */
    private static final XmlFieldFactory XF = new XmlFieldFactory(true);

    /** Le mapper profil regularisation utils. */
    @Autowired
    private MapperProfilRegularisationUtils mapperProfilRegularisationUtils;

    /**
     * Test mapper.
     *
     * @throws XmlFieldParsingException
     *             le xml field parsing exception
     */
    @Test
    public void testMapper() throws XmlFieldParsingException {
        IProfilRegularisation iProfilR = XF.getXmlField().newObject(IProfilRegularisation.class);
        iProfilR.setActivitePrincipaleCodeActivite("activitePrincipaleCodeActivite");
        iProfilR.setAqpa(true);
        XmlString str = iProfilR.addToEvenement();
        str.setString("testStr");
        ICfe cfe = iProfilR.newCfe();
        cfe.setCode("codeTestCfe");

        ProfilRegularisationBean profilR = new ProfilRegularisationBean();
        this.mapperProfilRegularisationUtils.mapper(iProfilR, profilR);

        assertEquals("activitePrincipaleCodeActivite", profilR.getActivitePrincipaleCodeActivite());
        assertTrue(profilR.isAqpa());
        assertEquals(1, profilR.getEvenement().size());
        assertEquals("testStr", profilR.getEvenement().get(0));
        assertNotNull(profilR.getCfe());
        assertEquals("codeTestCfe", profilR.getCfe().getCode());
    }

    /**
     * Test mapper activite secondaire interface vers bean.
     *
     * @throws XmlFieldParsingException
     *             xml field parsing exception
     */
    @Test
    public void testMapperActiviteSecondaireInterfaceVersBean() throws XmlFieldParsingException {
        IProfilRegularisation iProfilR = XF.getXmlField().newObject(IProfilRegularisation.class);
        iProfilR.setActivitePrincipaleCodeActivite("activitePrincipaleCodeActivite");

        iProfilR.setActiviteSecondaireCodeActivite("activiteSecondaireCodeActivite");
        iProfilR.setActiviteSecondaireDomaine("activiteSecondaireDomaine");
        iProfilR.setActiviteSecondaireSecteur("activiteSecondaireSecteur");

        ProfilRegularisationBean profilR = new ProfilRegularisationBean();
        this.mapperProfilRegularisationUtils.mapper(iProfilR, profilR);
        // assert ACT PRINCIPALE
        assertEquals("activitePrincipaleCodeActivite", profilR.getActivitePrincipaleCodeActivite());
        // assert ACT SECONDAIRE
        assertEquals("activiteSecondaireCodeActivite", profilR.getActiviteSecondaireCodeActivite());
        assertEquals("activiteSecondaireDomaine", profilR.getActiviteSecondaireDomaine());
        assertEquals("activiteSecondaireSecteur", profilR.getActiviteSecondaireSecteur());

    }

    /**
     * Test mapper activite secondaire bean vers interface.
     *
     * @throws XmlFieldParsingException
     *             xml field parsing exception
     */
    @Test
    public void testMapperActiviteSecondaireBeanVersInterface() throws XmlFieldParsingException {
        ProfilRegularisationBean profilR = new ProfilRegularisationBean();
        profilR.setActivitePrincipaleCodeActivite("activitePrincipaleCodeActivite");

        profilR.setActiviteSecondaireCodeActivite("activiteSecondaireCodeActivite");
        profilR.setActiviteSecondaireDomaine("activiteSecondaireDomaine");
        profilR.setActiviteSecondaireSecteur("activiteSecondaireSecteur");

        IProfilRegularisation iProfilR = XF.getXmlField().newObject(IProfilRegularisation.class);
        this.mapperProfilRegularisationUtils.mapper(profilR, iProfilR);

        // assert ACT PRINCIPALE
        assertEquals("activitePrincipaleCodeActivite", iProfilR.getActivitePrincipaleCodeActivite());
        // assert ACT SECONDAIRE
        assertEquals("activiteSecondaireCodeActivite", iProfilR.getActiviteSecondaireCodeActivite());
        assertEquals("activiteSecondaireDomaine", iProfilR.getActiviteSecondaireDomaine());
        assertEquals("activiteSecondaireSecteur", iProfilR.getActiviteSecondaireSecteur());

    }

}
