/**
 * 
 */
package fr.ge.common.regent.service.referentiel;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import fr.ge.common.regent.persistance.dao.referentiel.ReglesRegentDao;
import fr.ge.common.regent.service.referentiel.impl.ReferentielReglesServiceImpl;

/**
 * @author $Author: FADUBOIS $
 * @version $Revision: 0 $
 */
public class ReferentielReglesServiceImplTest {

    /** Service d'accès aux référentiels des regles. */
    @InjectMocks
    private ReferentielReglesServiceImpl referentielReglesService;

    /** Le regles regent dao. */
    @Mock
    private ReglesRegentDao reglesRegentDao;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void test_recupererToutesLesReglesRegent() {
        referentielReglesService.recupererToutesLesReglesRegent();
        Mockito.verify(reglesRegentDao).getAll("id");
    }

}
