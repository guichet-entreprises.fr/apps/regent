/**
 * 
 */
package fr.ge.common.regent.service.xmlregent.modification;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import fr.ge.common.regent.service.xmlregent.AbstractServiceGenerationXmlRegentTest;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * Test 610P.
 * 
 * @author $Author: mtakerra $
 * @version $Revision: 0 $
 */
public class ServiceGenerationXmlRegent_61P_Test extends AbstractServiceGenerationXmlRegentTest {

    private static final String TYPE_FORMALITE = "modification";
    private static final String TYPE_EVENEMENT = "61P";

    @Test
    public void testCheckBalise61P_C10_2_V2008_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C48010002226", "C4801", "CFE", Arrays.asList("C4801"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise C10.2", "2015-10-15", result);
    }

    @Test
    public void testCheckBalise61_PC10_2_V2016_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C48010002226", "C4801", "CFE", Arrays.asList("C4801"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2";

        String result = evaluteXpathFromXml(xpath, xml);

        Assert.assertEquals("La balise C10.2", "2015-10-15", result);
    }
}
