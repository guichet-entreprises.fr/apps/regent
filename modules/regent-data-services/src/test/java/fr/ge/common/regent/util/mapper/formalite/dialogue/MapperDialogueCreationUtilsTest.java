package fr.ge.common.regent.util.mapper.formalite.dialogue;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.xmlfield.core.XmlField;
import org.xmlfield.core.XmlFieldFactory;
import org.xmlfield.core.exception.XmlFieldParsingException;

import fr.ge.common.regent.bean.formalite.dialogue.creation.ActiviteSoumiseQualificationBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.AdresseBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.AutreEtablissementUEBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.AyantDroitBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.ConjointBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.DeclarationSocialeBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.DialogueCreationBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.DirigeantBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.EirlBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.EntrepriseBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.EntrepriseLieeBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.EtablissementBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.PersonneLieeBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.PostalCommuneBean;
import fr.ge.common.regent.bean.xml.formalite.dialogue.creation.IDialogueCreation;
import fr.ge.common.regent.bean.xml.formalite.dialogue.creation.IDirigeantCreation;
import fr.ge.common.regent.util.test.AbstractSpringTest;

/**
 * Le Class MapperFormaliteRegularisationUtilsTest.
 */
public class MapperDialogueCreationUtilsTest extends AbstractSpringTest {

    /** La constante XF. */
    private static final XmlFieldFactory XF = new XmlFieldFactory(true);

    /** mapper formalite creation utils. */
    @Autowired
    private MapperDialogueCreationUtils mapperFormaliteCreationUtils;

    /**
     * Test mapper interface vers bean.
     *
     * @throws XmlFieldParsingException
     *             le xml field parsing exception
     */
    @Test
    public void testMapperInterfaceVersBean() throws XmlFieldParsingException {
        IDialogueCreation iFormaliteCreation = XF.getXmlField().newObject(IDialogueCreation.class);
        iFormaliteCreation.setDossierId("4321");

        DialogueCreationBean creationBean = new DialogueCreationBean();
        this.mapperFormaliteCreationUtils.mapper(iFormaliteCreation, creationBean);

        assertEquals(Long.valueOf(4321), creationBean.getDossierId());
    }

    /**
     * Test mapper i formalite cessation formalite cessation interface to bean.
     *
     * @throws XmlFieldParsingException
     *             le xml field parsing exception
     * @throws FileNotFoundException
     *             le file not found exception
     */
    @Test
    public void testMapperIFormaliteCessationFormaliteCreationInterfaceToBean() throws XmlFieldParsingException, FileNotFoundException {
        FileInputStream fileInputStream = new FileInputStream("src/test/resources/formalitesXml/formalitesCreation.xml");
        IDialogueCreation xml = new XmlField().xmlToObject(fileInputStream, IDialogueCreation.class);
        // Init Bean
        DialogueCreationBean bean = new DialogueCreationBean();
        assertNull(bean.getNumeroDossier());
        this.mapperFormaliteCreationUtils.mapper(xml, bean);
        assertEquals("DOSSIER_X75010022805", bean.getNumeroDossier());
        assertEquals("commune_TU_@86194", bean.getEntreprise().getAdresse().getCodePostalCommune().getCommune());
        assertEquals("commune_TU_@92073", bean.getAdresseSignature().getCodePostalCommune().getCommune());
        assertEquals("complement d'adresse", bean.getCorrespondanceAdresse().getComplementAdresse());
        assertEquals("commune_TU_@Etablissement", bean.getEntreprise().getEtablissement().getAdresse().getCodePostalCommune().getCommune());
        // entrepriseLieeDomiciliation
        assertEquals("commune_TU_@EntrepriseLieeDom", bean.getEntreprise().getEntrepriseLieeDomiciliation().getAdresse().getCodePostalCommune().getCommune());
        // entrepriseLieeLoueurMandant
        assertEquals("commune_TU_@EntrepriseLieeLoueurMandant", bean.getEntreprise().getEntrepriseLieeLoueurMandant().getAdresse().getCodePostalCommune().getCommune());
        assertEquals("commune_TU_@EntrepriseLieeContratAppui", bean.getEntreprise().getEntrepriseLieeContratAppui().getAdresse().getCodePostalCommune().getCommune());
        assertEquals("commune_TU_@CORRESPANDANT", bean.getCorrespondanceDestinataire());
        assertEquals("CCC@GMAIL.COM", bean.getCourriel());
        assertEquals("DOSSIER_X75010022805", bean.getNumeroDossier());
        assertEquals("X75010022805", bean.getNumeroFormalite());
        assertEquals("des informations complémentaires", bean.getObservations());
        assertEquals("A", bean.getReseauCFE());
        // assertEquals("mandataire", bean.getEntreprise().ge);
        assertEquals("mandataire", bean.getSignataireNom());
        assertEquals("M", bean.getTypePersonne());
        assertEquals("commune_TU_@86194", bean.getEntreprise().getAdresse().getCodePostalCommune().getCommune());
        assertEquals("oui", bean.getEntreprise().getAssocieUnique());
        assertEquals("oui", bean.getEntreprise().getAutreEtablissementUE());
        assertEquals("EUR", bean.getEntreprise().getCapitalDevise());
        assertEquals("sa dénomination ou raison sociale", bean.getEntreprise().getDenomination());
        assertEquals(Integer.valueOf(6), bean.getEntreprise().getNombreDirigeants());
        assertEquals("l'enseigne de votre entreprise", bean.getEntreprise().getEtablissement().getEnseigne());
        assertEquals("le nom commercial", bean.getEntreprise().getEtablissement().getNomCommercialProfessionnel());

    }

    /**
     * Test mapper i formalite cessation formalite cessation interface to bean.
     *
     * @throws XmlFieldParsingException
     *             le xml field parsing exception
     * @throws FileNotFoundException
     *             le file not found exception
     */
    @Test
    public void testMapperIFormaliteCreationPrecedantExploitant() throws XmlFieldParsingException, FileNotFoundException {

        FileInputStream fileInputStream = new FileInputStream("src/test/resources/formalitesXml/formalitesCreationExploitant.xml");
        IDialogueCreation xml = new XmlField().xmlToObject(fileInputStream, IDialogueCreation.class);

        // Init Bean
        DialogueCreationBean bean = new DialogueCreationBean();
        assertNull(bean.getNumeroDossier());
        this.mapperFormaliteCreationUtils.mapper(xml, bean);

        assertEquals("commune_TU_@entreprisesLieesPrecedentsExploitants1", bean.getEntreprise().getEntreprisesLieesPrecedentsExploitants().get(0).getAdresse().getCodePostalCommune().getCommune());

        assertEquals("commune_TU_@entreprisesLieesPrecedentsExploitants2", bean.getEntreprise().getEntreprisesLieesPrecedentsExploitants().get(1).getAdresse().getCodePostalCommune().getCommune());

        assertEquals("commune_TU_@33522", bean.getEntreprise().getPersonneLiee().get(0).getPpAdresse().getCodePostalCommune().getCommune());

    }

    /**
     * Test mapper i formalite création formalite création interface to bean.
     *
     * @throws XmlFieldParsingException
     *             le xml field parsing exception
     * @throws FileNotFoundException
     *             le file not found exception
     */
    @Test
    public void testMapperIFormaliteCreationActivitesSoumisQualification() throws XmlFieldParsingException, FileNotFoundException {

        FileInputStream fileInputStream = new FileInputStream("src/test/resources/formalitesXml/formalitesCreationActiviteSoumiseQualification.xml");
        IDialogueCreation xml = new XmlField().xmlToObject(fileInputStream, IDialogueCreation.class);

        // Init Bean
        DialogueCreationBean bean = new DialogueCreationBean();
        assertNull(bean.getNumeroDossier());
        this.mapperFormaliteCreationUtils.mapper(xml, bean);

        assertEquals(2, bean.getEntreprise().getEtablissement().getActivitesSoumisesQualification().size());
        assertEquals("activite1", bean.getEntreprise().getEtablissement().getActivitesSoumisesQualification().get(0).getActiviteJQPA());

        assertEquals("activité2", bean.getEntreprise().getEtablissement().getActivitesSoumisesQualification().get(1).getActiviteJQPA());

    }

    /**
     * Test mapper i formalite cessation formalite cessation interface to bean.
     *
     * @throws XmlFieldParsingException
     *             le xml field parsing exception
     * @throws FileNotFoundException
     *             le file not found exception
     */
    @Test
    public void testMapperIFormaliteCreationDirigeantBean() throws XmlFieldParsingException, FileNotFoundException {

        FileInputStream fileInputStream = new FileInputStream("src/test/resources/formalitesXml/formalitesCreationDirigeant.xml");
        IDialogueCreation xml = new XmlField().xmlToObject(fileInputStream, IDialogueCreation.class);

        // Init Bean
        DialogueCreationBean bean = new DialogueCreationBean();
        assertNull(bean.getNumeroDossier());
        this.mapperFormaliteCreationUtils.mapper(xml, bean);

        assertEquals("non", bean.getEntreprise().getDirigeants().get(0).getConjointPresence());
        // assertEquals("050",
        // bean.getEntreprise().getDirigeants().get(0).getPpAdresse().getCodePays());
        assertEquals("commune_TU_@86194", bean.getEntreprise().getDirigeants().get(0).getPpAdresse().getCodePostalCommune().getCommune());
        assertEquals("commune_TU_@Dirigeant1_ppAdresseAmbulant", bean.getEntreprise().getDirigeants().get(0).getPpAdresseAmbulant().getCommune());
        assertEquals("commune_TU_@Dirigeant1_ppAdresseForain", bean.getEntreprise().getDirigeants().get(0).getPpAdresseForain().getCommune());
        assertEquals("212", bean.getEntreprise().getDirigeants().get(0).getPpLieuNaissancePays());
        assertEquals("34172", bean.getEntreprise().getDirigeants().get(1).getPmAdresse().getCodePostalCommune().getCommune());

        assertEquals("commune_TU_@86194", bean.getEntreprise().getDirigeants().get(4).getConjoint().getPpAdresse().getCodePostalCommune().getCommune());

        assertEquals("non", bean.getEntreprise().getDirigeants().get(4).getDeclarationSociale().getActiviteAutreQueDeclareePresence().toString());
        assertEquals("commune_TU_@86194", bean.getEntreprise().getDirigeants().get(4).getDeclarationSociale().getAyantDroits().get(0).getAdresse().getCodePostalCommune().getCommune());

        assertEquals("commune_TU_@86194", bean.getEntreprise().getDirigeants().get(4).getDeclarationSociale().getAyantDroits().get(0).getAdresse().getCodePostalCommune().getCommune());

    }

    @Test
    public void testMapperIFormaliteCreationConjointBean() throws Exception {
        FileInputStream fileInputStream = new FileInputStream("src/test/resources/formalitesXml/formalitesCreationConjoint.xml");
        IDialogueCreation xml = new XmlField().xmlToObject(fileInputStream, IDialogueCreation.class);

        // Mapping
        DialogueCreationBean bean = new DialogueCreationBean();
        mapperFormaliteCreationUtils.mapper(xml, bean);

        // Vérifications
        assertThat(bean.getNumeroDossier()).isNull();
        assertThat(bean.getEntreprise()).isNotNull();
        assertThat(bean.getEntreprise().getDirigeants()).hasSize(1);
        assertThat(bean.getEntreprise().getDirigeants().get(0)).isNotNull();
        ConjointBean conjoint = bean.getEntreprise().getDirigeants().get(0).getConjoint();
        assertThat(conjoint).isNotNull();
        assertThat(conjoint.getPpNomNaissance()).isEqualTo("Nom-de-naissance-conjoint");
        assertThat(conjoint.getPpPrenom1()).isEqualTo("Prénom-conjoint");
        assertThat(conjoint.getPpAdresseDifferentGerant()).isEqualTo("050");
        assertThat(conjoint.getPpDateNaissance()).isNullOrEmpty();
        assertThat(conjoint.getPpLieuNaissanceCommune()).isNullOrEmpty();
        assertThat(conjoint.getPpLieuNaissanceDepartement()).isNullOrEmpty();
        assertThat(conjoint.getPpLieuNaissancePays()).isNullOrEmpty();
        assertThat(conjoint.getPpLieuNaissanceVille()).isNullOrEmpty();
        assertThat(conjoint.getPpNationalite()).isNullOrEmpty();
    }

    /**
     * Test mapper i formalite creation dirigeant ayantayant droit.
     *
     * @throws XmlFieldParsingException
     *             xml field parsing exception
     * @throws FileNotFoundException
     *             file not found exception
     */
    @Test
    public void testMapperIFormaliteCreationDirigeantAyantayantDroit() throws XmlFieldParsingException, FileNotFoundException {

        FileInputStream fileInputStream = new FileInputStream("src/test/resources/formalitesXml/formalitesCreationDirigeantAyantDroit.xml");
        IDialogueCreation xml = new XmlField().xmlToObject(fileInputStream, IDialogueCreation.class);

        // Init Bean
        DialogueCreationBean bean = new DialogueCreationBean();
        assertNull(bean.getNumeroDossier());
        this.mapperFormaliteCreationUtils.mapper(xml, bean);

        assertEquals("commune_TU_@86194",
                bean.getEntreprise().getDirigeants().get(0).getDeclarationSociale().getAyantDroits().get(0).getAyantDroits().get(0).getAdresse().getCodePostalCommune().getCommune());

    }

    /**
     * Test mapper formalite creation beanto interface.
     *
     * @throws XmlFieldParsingException
     *             xml field parsing exception
     */
    @Test
    public void testMapperFormaliteCreationBeantoInterface() throws XmlFieldParsingException {

        // Init Bean
        DialogueCreationBean bean = new DialogueCreationBean();
        String numDossier = "NUM_DOSSIER_TU";
        bean.setNumeroDossier(numDossier);

        EntrepriseBean entreprise = new EntrepriseBean();
        entreprise.setEirl(new EirlBean());

        List<String> capitalApportsNature = new ArrayList<>();
        capitalApportsNature.add("Apport nature 1");
        entreprise.setCapitalApportsNature(capitalApportsNature);

        // entrepriseLieeDomiciliation

        AdresseBean adresseDom = new AdresseBean();
        PostalCommuneBean codePostalCommune = new PostalCommuneBean();
        codePostalCommune.setCommune("TU_dom");
        adresseDom.setCodePostalCommune(codePostalCommune);

        EntrepriseLieeBean entrepriseLieeDomiciliation = new EntrepriseLieeBean();
        entrepriseLieeDomiciliation.setAdresse(adresseDom);
        entreprise.setEntrepriseLieeDomiciliation(entrepriseLieeDomiciliation);

        EtablissementBean etablissement = new EtablissementBean();
        List<String> activitesExerceesAgricole = new ArrayList<>();
        activitesExerceesAgricole.add("Activite agricole 1");
        etablissement.setActivitesExerceesAgricole(activitesExerceesAgricole);

        List<ActiviteSoumiseQualificationBean> activitesSoumisesQualification = new ArrayList<>();
        ActiviteSoumiseQualificationBean activite = new ActiviteSoumiseQualificationBean();
        activite.setNumeroDossier(numDossier);
        activite.setTypePersonne("TU_type_personne");
        activitesSoumisesQualification.add(activite);
        etablissement.setActivitesSoumisesQualification(activitesSoumisesQualification);

        entreprise.setEtablissement(etablissement);

        // entrepriseLieeLoueurMandant
        AdresseBean adresseLoueur = new AdresseBean();
        PostalCommuneBean codePostalCommuneLoueur = new PostalCommuneBean();
        codePostalCommuneLoueur.setCommune("TU_loueur");
        adresseLoueur.setCodePostalCommune(codePostalCommuneLoueur);

        EntrepriseLieeBean entrepriseLieeLoueur = new EntrepriseLieeBean();
        entrepriseLieeLoueur.setAdresse(adresseLoueur);
        entreprise.setEntrepriseLieeLoueurMandant(entrepriseLieeLoueur);

        // entrepriseLieesPrecedentsExploitants
        List<EntrepriseLieeBean> entreprisesLieesPrecedentsExploitants = new ArrayList<EntrepriseLieeBean>();

        AdresseBean adressePrecExploit = new AdresseBean();
        PostalCommuneBean codePostalCommunePrecExploit = new PostalCommuneBean();
        codePostalCommunePrecExploit.setCommune("TU_PREC");
        adressePrecExploit.setCodePostalCommune(codePostalCommunePrecExploit);

        EntrepriseLieeBean e = new EntrepriseLieeBean();
        e.setAdresse(adressePrecExploit);
        entreprisesLieesPrecedentsExploitants.add(e);
        entreprise.setEntreprisesLieesPrecedentsExploitants(entreprisesLieesPrecedentsExploitants);

        List<DirigeantBean> dirigeants = new ArrayList<DirigeantBean>();
        DirigeantBean dirigeant = new DirigeantBean();
        dirigeant.setNumeroDossier(numDossier);

        ConjointBean conjoint = new ConjointBean();
        conjoint.setNumeroDossier(numDossier);
        conjoint.setPpAdresseDifferentGerant("050");
        conjoint.setPpDateNaissance("01/01/1980");
        dirigeant.setConjoint(conjoint);

        PostalCommuneBean cpModifAncDom = new PostalCommuneBean();
        cpModifAncDom.setCommune("TU_cpModifAncDom");
        ;
        AdresseBean ppAdresse = new AdresseBean();
        PostalCommuneBean ppAdresseCodPost = new PostalCommuneBean();
        ppAdresseCodPost.setCommune("TU_ppAdresse");
        ppAdresse.setCodePostalCommune(ppAdresseCodPost);
        dirigeant.setPpAdresse(ppAdresse);

        PostalCommuneBean ppAdresseAmbulant = new PostalCommuneBean();
        ppAdresseAmbulant.setCommune("TU_ambulant");
        dirigeant.setPpAdresseAmbulant(ppAdresseAmbulant);

        PostalCommuneBean ppAdresseForain = new PostalCommuneBean();
        ppAdresseForain.setCommune("TU_forain");
        dirigeant.setPpAdresseForain(ppAdresseForain);

        DeclarationSocialeBean socialeBeande = new DeclarationSocialeBean();

        List<AyantDroitBean> ayantDroits = new ArrayList<AyantDroitBean>();

        AyantDroitBean ayantDroit = new AyantDroitBean();
        ayantDroit.setAdresse(ppAdresse);

        ayantDroits.add(ayantDroit);

        socialeBeande.setAyantDroits(ayantDroits);
        dirigeant.setDeclarationSociale(socialeBeande);

        dirigeants.add(dirigeant);

        entreprise.setDirigeants(dirigeants);

        // Autre etablissement UE
        List<AutreEtablissementUEBean> etablissementsUE = new ArrayList<AutreEtablissementUEBean>();

        AutreEtablissementUEBean autre = new AutreEtablissementUEBean();
        AdresseBean adresse = new AdresseBean();
        PostalCommuneBean adresseCodPost = new PostalCommuneBean();
        adresseCodPost.setCommune("TU_adresse");
        adresse.setCodePostalCommune(adresseCodPost);
        autre.setAdresse(adresse);
        etablissementsUE.add(autre);

        entreprise.setEtablissementsUE(etablissementsUE);

        List<PersonneLieeBean> personneLiee = new ArrayList<PersonneLieeBean>();
        AdresseBean adressePersonneLiee = new AdresseBean();
        PostalCommuneBean ppAdresseCodPostPersonneLiee = new PostalCommuneBean();
        ppAdresseCodPost.setCommune("TU_ppAdresse");
        adressePersonneLiee.setCodePostalCommune(ppAdresseCodPostPersonneLiee);
        PersonneLieeBean perso = new PersonneLieeBean();

        perso.setPpAdresse(ppAdresse);
        personneLiee.add(perso);
        entreprise.setPersonneLiee(personneLiee);

        // entrepriseLieesPrecedentsExploitants
        List<EntrepriseLieeBean> entreprisesLieesFusionScission = new ArrayList<EntrepriseLieeBean>();

        AdresseBean adresseFusionScission = new AdresseBean();
        PostalCommuneBean codePostalCommuneFusionScission = new PostalCommuneBean();
        codePostalCommuneFusionScission.setCommune("TU_PREC");
        adresseFusionScission.setCodePostalCommune(codePostalCommuneFusionScission);

        EntrepriseLieeBean d = new EntrepriseLieeBean();
        d.setAdresse(adresseFusionScission);
        entreprisesLieesFusionScission.add(d);
        entreprise.setEntreprisesLieesFusionScission(entreprisesLieesFusionScission);

        bean.setEntreprise(entreprise);

        // Init Interface
        IDialogueCreation xml = new XmlField().newObject(IDialogueCreation.class);

        this.mapperFormaliteCreationUtils.mapper(bean, xml);
        //
        assertNotNull(xml);
        assertEquals(numDossier, xml.getNumeroDossier());
        assertEquals("TU_adresse", xml.getEntreprise().getEtablissementsUE()[0].getAdresse().getCodePostalCommune().getCommune());

        assertEquals("TU_ppAdresse", xml.getEntreprise().getPersonneLiee()[0].getPpAdresse().getCodePostalCommune().getCommune());
        assertEquals("TU_ppAdresse", xml.getEntreprise().getDirigeants()[0].getDeclarationSociale().getAyantDroits()[0].getAdresse().getCodePostalCommune().getCommune());

        // TU EntrepriseLieeLoueurMandant
        assertEquals("TU_dom", xml.getEntreprise().getEntrepriseLieeDomiciliation().getAdresse().getCodePostalCommune().getCommune());

        // TU EntrepriseLieeLoueurMandant
        assertEquals("TU_loueur", xml.getEntreprise().getEntrepriseLieeLoueurMandant().getAdresse().getCodePostalCommune().getCommune());

        // TU EntrepriseLieesPrecedentsExploitant
        assertEquals("TU_PREC", xml.getEntreprise().getEntreprisesLieesPrecedentsExploitants()[0].getAdresse().getCodePostalCommune().getCommune());

        // TU EntreprisesLieesFusionScission
        assertEquals("TU_PREC", xml.getEntreprise().getEntreprisesLieesFusionScission()[0].getAdresse().getCodePostalCommune().getCommune());

        // TU Dirigeants
        IDirigeantCreation iDirigeantRegularisation = xml.getEntreprise().getDirigeants()[0];
        assertEquals(numDossier, iDirigeantRegularisation.getNumeroDossier());

        // TU Dirigeants[0]->Conjoint
        assertEquals(numDossier, iDirigeantRegularisation.getConjoint().getNumeroDossier());
        assertEquals("050", iDirigeantRegularisation.getConjoint().getPpAdresseDifferentGerant());
        assertEquals("01/01/1980", iDirigeantRegularisation.getConjoint().getPpDateNaissance());

        assertEquals("TU_ppAdresse", iDirigeantRegularisation.getPpAdresse().getCodePostalCommune().getCommune());
        assertEquals("TU_ambulant", iDirigeantRegularisation.getPpAdresseAmbulant().getCommune());
        assertEquals("TU_forain", iDirigeantRegularisation.getPpAdresseForain().getCommune());

    }
}
