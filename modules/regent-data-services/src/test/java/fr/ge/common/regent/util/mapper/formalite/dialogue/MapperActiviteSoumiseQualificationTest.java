package fr.ge.common.regent.util.mapper.formalite.dialogue;

import org.dozer.Mapper;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.xmlfield.core.XmlField;
import org.xmlfield.core.exception.XmlFieldParsingException;

import fr.ge.common.regent.bean.formalite.dialogue.creation.ActiviteSoumiseQualificationBean;
import fr.ge.common.regent.bean.xml.formalite.dialogue.creation.IActiviteSoumiseQualificationCreation;
import fr.ge.common.regent.util.test.AbstractSpringTest;

/**
 * Le Class MapperFormaliteRegularisationUtilsTest.
 */
public class MapperActiviteSoumiseQualificationTest extends AbstractSpringTest {

    /** La constante XF. */
    private static final XmlField xmlField = new XmlField();

    /** QUALITE_PERSONNE_QUALIFIEE_PM_AUTRE. **/
    private static final String QUALITE_PERSONNE_QUALIFIEE_PM_AUTRE = "qualitePersonneQualifieePMAutre";

    /** QUALITE_PERSONNE_QUALIFIEE_PM. **/
    private static final String QUALITE_PERSONNE_QUALIFIEE_PM = "qualitePersonneQualifieePM";

    /** IDENTITE_PM_PRENOM3. **/
    private static final String IDENTITE_PM_PRENOM3 = "identitePMPrenom3";

    /** IDENTITE_PM_PRENOM2. **/
    private static final String IDENTITE_PM_PRENOM2 = "identitePMPrenom2";

    /** IDENTITE_PM_PRENOM1. **/
    private static final String IDENTITE_PM_PRENOM1 = "identitePMPrenom1";

    /** IDENTITE_PM_NOM_NAISSANCE. **/
    private static final String IDENTITE_PM_NOM_NAISSANCE = "identitePMNomNaissance";

    /** IDENTITE_PM_LIEU_NAISSANCE_VILLE. **/
    private static final String IDENTITE_PM_LIEU_NAISSANCE_VILLE = "identitePMLieuNaissanceVille";

    /** IDENTITE_PM_LIEU_NAISSANCE_PAYS. **/
    private static final String IDENTITE_PM_LIEU_NAISSANCE_PAYS = "identitePMLieuNaissancePays";

    /** IDENTITE_PM_LIEU_NAISSANCE_DEPARTEMENT. **/
    private static final String IDENTITE_PM_LIEU_NAISSANCE_DEPARTEMENT = "identitePMLieuNaissanceDepartement";

    /** IDENTITE_PM_LIEU_NAISSANCE_COMMUNE. **/
    private static final String IDENTITE_PM_LIEU_NAISSANCE_COMMUNE = "identitePMLieuNaissanceCommune";

    /** IDENTITE_PM_DATE_NAISSANCE. **/
    private static final String IDENTITE_PM_DATE_NAISSANCE = "identitePMDateNaissance";

    /** QUALITE_PERSONNE_QUALIFIEE_PP_AUTRE. **/
    private static final String QUALITE_PERSONNE_QUALIFIEE_PP_AUTRE = "qualitePersonneQualifieePPAutre";

    /** QUALITE_PERSONNE_QUALIFIEE_PP. **/
    private static final String QUALITE_PERSONNE_QUALIFIEE_PP = "qualitePersonneQualifieePP";

    /** IDENTITE_PP_PRENOM3. **/
    private static final String IDENTITE_PP_PRENOM3 = "identitePPPrenom3";

    /** IDENTITE_PP_PRENOM2. **/
    private static final String IDENTITE_PP_PRENOM2 = "identitePPPrenom2";

    /** IDENTITE_PP_PRENOM1. **/
    private static final String IDENTITE_PP_PRENOM1 = "identitePPPrenom1";

    /** IDENTITE_PP_NOM_NAISSANCE. **/
    private static final String IDENTITE_PP_NOM_NAISSANCE = "identitePPNomNaissance";

    /** IDENTITE_PP_LIEU_NAISSANCE_VILLE. **/
    private static final String IDENTITE_PP_LIEU_NAISSANCE_VILLE = "identitePPLieuNaissanceVille";

    /** IDENTITE_PP_LIEU_NAISSANCE_PAYS. **/
    private static final String IDENTITE_PP_LIEU_NAISSANCE_PAYS = "identitePPLieuNaissancePays";

    /** IDENTITE_PP_LIEU_NAISSANCE_DEPARTEMENT. **/
    private static final String IDENTITE_PP_LIEU_NAISSANCE_DEPARTEMENT = "identitePPLieuNaissanceDepartement";

    /** IDENTITE_PP_LIEU_NAISSANCE_COMMUNE. **/
    private static final String IDENTITE_PP_LIEU_NAISSANCE_COMMUNE = "identitePPLieuNaissanceCommune";

    /** IDENTITE_PP_DATE_NAISSANCE. **/
    private static final String IDENTITE_PP_DATE_NAISSANCE = "identitePPDateNaissance";

    /** AQPA_SITUATION. **/
    private static final String AQPA_SITUATION = "aqpaSituation";

    /** ACTIVITE_JQPA. **/
    private static final String ACTIVITE_JQPA = "activiteJQPA";

    /**
     * Méthode de création d'une activité soumise à qualification.
     * 
     * @return {@link ActiviteSoumiseQualificationBean}
     */
    private ActiviteSoumiseQualificationBean createActiviteSoumiseQualification() {
        ActiviteSoumiseQualificationBean res = new ActiviteSoumiseQualificationBean();
        res.setActiviteJQPA(ACTIVITE_JQPA);
        res.setAqpaSituation(AQPA_SITUATION);
        res.setId(1L);
        res.setIdentitePMDateNaissance(IDENTITE_PM_DATE_NAISSANCE);
        res.setIdentitePMLieuNaissanceCommune(IDENTITE_PM_LIEU_NAISSANCE_COMMUNE);
        res.setIdentitePMLieuNaissanceDepartement(IDENTITE_PM_LIEU_NAISSANCE_DEPARTEMENT);
        res.setIdentitePMLieuNaissancePays(IDENTITE_PM_LIEU_NAISSANCE_PAYS);
        res.setIdentitePMLieuNaissanceVille(IDENTITE_PM_LIEU_NAISSANCE_VILLE);
        res.setIdentitePMNomNaissance(IDENTITE_PM_NOM_NAISSANCE);
        res.setIdentitePMPrenom1(IDENTITE_PM_PRENOM1);
        res.setIdentitePMPrenom2(IDENTITE_PM_PRENOM2);
        res.setIdentitePMPrenom3(IDENTITE_PM_PRENOM3);
        res.setQualitePersonneQualifieePM(QUALITE_PERSONNE_QUALIFIEE_PM);
        res.setQualitePersonneQualifieePMAutre(QUALITE_PERSONNE_QUALIFIEE_PM_AUTRE);

        res.setIdentitePPDateNaissance(IDENTITE_PP_DATE_NAISSANCE);
        res.setIdentitePPLieuNaissanceCommune(IDENTITE_PP_LIEU_NAISSANCE_COMMUNE);
        res.setIdentitePPLieuNaissanceDepartement(IDENTITE_PP_LIEU_NAISSANCE_DEPARTEMENT);
        res.setIdentitePPLieuNaissancePays(IDENTITE_PP_LIEU_NAISSANCE_PAYS);
        res.setIdentitePPLieuNaissanceVille(IDENTITE_PP_LIEU_NAISSANCE_VILLE);
        res.setIdentitePPNomNaissance(IDENTITE_PP_NOM_NAISSANCE);
        res.setIdentitePPPrenom1(IDENTITE_PP_PRENOM1);
        res.setIdentitePPPrenom2(IDENTITE_PP_PRENOM2);
        res.setIdentitePPPrenom3(IDENTITE_PP_PRENOM3);
        res.setQualitePersonneQualifieePP(QUALITE_PERSONNE_QUALIFIEE_PP);
        res.setQualitePersonneQualifieePPAutre(QUALITE_PERSONNE_QUALIFIEE_PP_AUTRE);
        return res;

    }

    /** Mapper de l'activité soumise à qualification. **/
    @Autowired
    private Mapper activiteSoumiseQualificationMapper;

    /**
     * Tester le mapping bean interface.
     * 
     * @throws XmlFieldParsingException
     *             exception
     */
    @Test
    public void testMapperBeanEnInterface() throws XmlFieldParsingException {
        ActiviteSoumiseQualificationBean bean = createActiviteSoumiseQualification();

        IActiviteSoumiseQualificationCreation interfaceXml = xmlField.newObject(IActiviteSoumiseQualificationCreation.class);
        activiteSoumiseQualificationMapper.map(bean, interfaceXml);

        Assert.assertEquals(ACTIVITE_JQPA, interfaceXml.getActiviteJQPA());
        Assert.assertEquals(AQPA_SITUATION, interfaceXml.getAqpaSituation());
        Assert.assertEquals(IDENTITE_PM_DATE_NAISSANCE, interfaceXml.getIdentitePMDateNaissance());
        Assert.assertEquals(IDENTITE_PM_LIEU_NAISSANCE_COMMUNE, interfaceXml.getIdentitePMLieuNaissanceCommune());
        Assert.assertEquals(IDENTITE_PM_LIEU_NAISSANCE_DEPARTEMENT, interfaceXml.getIdentitePMLieuNaissanceDepartement());
    }

    /**
     * Tester le mapping bean interface.
     * 
     * @throws XmlFieldParsingException
     *             exception
     */
    @Test
    public void testMapperInterfaceEnBean() throws XmlFieldParsingException {

        IActiviteSoumiseQualificationCreation interfaceXml = xmlField.newObject(IActiviteSoumiseQualificationCreation.class);

        interfaceXml.setActiviteJQPA(ACTIVITE_JQPA);
        interfaceXml.setAqpaSituation(AQPA_SITUATION);
        interfaceXml.setIdentitePMDateNaissance(IDENTITE_PM_DATE_NAISSANCE);
        interfaceXml.setIdentitePMLieuNaissanceCommune(IDENTITE_PM_LIEU_NAISSANCE_COMMUNE);
        interfaceXml.setIdentitePMLieuNaissanceDepartement(IDENTITE_PM_LIEU_NAISSANCE_DEPARTEMENT);

        ActiviteSoumiseQualificationBean bean = new ActiviteSoumiseQualificationBean();
        activiteSoumiseQualificationMapper.map(interfaceXml, bean);

        Assert.assertEquals(ACTIVITE_JQPA, bean.getActiviteJQPA());
        Assert.assertEquals(AQPA_SITUATION, bean.getAqpaSituation());
        Assert.assertEquals(IDENTITE_PM_DATE_NAISSANCE, bean.getIdentitePMDateNaissance());
        Assert.assertEquals(IDENTITE_PM_LIEU_NAISSANCE_COMMUNE, bean.getIdentitePMLieuNaissanceCommune());
        Assert.assertEquals(IDENTITE_PM_LIEU_NAISSANCE_DEPARTEMENT, bean.getIdentitePMLieuNaissanceDepartement());
    }

    /**
     * Mutateur sur l'attribut {@link #activiteSoumiseQualificationMapper}.
     *
     * @param activiteSoumiseQualificationMapper
     *            la nouvelle valeur de l'attribut
     *            activiteSoumiseQualificationMapper
     */
    public void setActiviteSoumiseQualificationMapper(final Mapper activiteSoumiseQualificationMapper) {
        this.activiteSoumiseQualificationMapper = activiteSoumiseQualificationMapper;
    }

}
