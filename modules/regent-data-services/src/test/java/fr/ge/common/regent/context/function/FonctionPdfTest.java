package fr.ge.common.regent.context.function;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.referentiel.service.ReferentielSupraService;
import fr.ge.common.regent.bean.formalite.dialogue.creation.DirigeantBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.EntrepriseBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.EntrepriseLieeBean;
import fr.ge.common.regent.util.test.AbstractSpringTest;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * Le Class FonctionsPdfTest.
 */
public class FonctionPdfTest extends AbstractSpringTest {

    /** Le ref supra service. */
    @Autowired
    private ReferentielSupraService referentielSupraService;

    /**
     * Cas chaine vide pour un type ref pour la commune
     */
    @Test
    public void testRefToStringCasNormalTypeRefCommune() {
        FonctionPdf fonctionsPdf = new FonctionPdf();
        fonctionsPdf.setReferentielSupraService(referentielSupraService);
        assertEquals("MORLAIX", fonctionsPdf.refToString("commune", "29151"));
    }

    /**
     * Cas chaine vide pour un type ref pour l activite.
     */
    @Test
    public void testRefToStringActivite() {
        FonctionPdf fonctionsPdf = new FonctionPdf();
        fonctionsPdf.setReferentielSupraService(referentielSupraService);
        assertEquals("Société de domiciliation", fonctionsPdf.refToString("activite", "101"));
    }

    /**
     * Paris 1er arrondissement
     */
    @Test
    public void testRefToStringCommuneARRParis1er() {
        FonctionPdf fonctionsPdf = new FonctionPdf();
        fonctionsPdf.setReferentielSupraService(referentielSupraService);
        assertEquals("PARIS 1ER ARRONDISSEMENT", fonctionsPdf.refToString("commune-arr", "75101"));
    }

    /**
     * Paris 20eme arrondissement
     */
    @Test
    public void testRefToStringCommuneARRParis20() {
        FonctionPdf fonctionsPdf = new FonctionPdf();
        fonctionsPdf.setReferentielSupraService(referentielSupraService);
        assertEquals("PARIS 20E  ARRONDISSEMENT", fonctionsPdf.refToString("commune-arr", "75120"));
    }

    /**
     * Cas chaine vide pour un type ref pour la commune
     */
    @Test
    public void testRefToStringChaineVidePourTypeRefCommune() {
        FonctionPdf fonctionsPdf = new FonctionPdf();
        fonctionsPdf.setReferentielSupraService(referentielSupraService);
        assertEquals(StringUtils.EMPTY, fonctionsPdf.refToString("commune", StringUtils.EMPTY));
    }

    /**
     * Cas code inexistant en base pour un type ref pour la commune
     */
    @Test
    public void testRefToStringCodeInexistantPourTypeRefCommune() {
        FonctionPdf fonctionsPdf = new FonctionPdf();
        fonctionsPdf.setReferentielSupraService(referentielSupraService);

        assertEquals(StringUtils.EMPTY, fonctionsPdf.refToString("commune", "CodeInexistant"));
    }

    /**
     * cas particulier code étranger =>codePays != 050 (France)
     */
    @Test
    public void testRefToStringCodeEtrangerNonConnuPourTypeRefCommune() {
        FonctionPdf fonctionsPdf = new FonctionPdf();
        fonctionsPdf.setReferentielSupraService(referentielSupraService);

        // Ethiopie non présent dans le référentiel.xml
        String idRefCodeEtranger = "99" + "131";
        // on s'assure qu'auncun libellé du pays étranger n'est remonté
        assertEquals(StringUtils.EMPTY, fonctionsPdf.refToString("commune", idRefCodeEtranger));
    }

    /**
     * @param dirigeants
     * @param entreprise
     * 
     *            setup testCalculerNbOccurrenceM0PrimeAgricole
     */
    public void setup(List<DirigeantBean> dirigeants, EntrepriseBean entreprise) {
        DirigeantBean dirigeant1 = new DirigeantBean();
        dirigeant1.setQualite("associeIndefinimentResponsable");
        dirigeant1.setNature("2");
        dirigeant1.setPmRepresentantQualite("test1");
        dirigeants.add(dirigeant1);

        DirigeantBean dirigeant2 = new DirigeantBean();
        dirigeant2.setQualite("commissaireComptesSuppleant");
        dirigeant2.setNature("2");
        dirigeant2.setPmRepresentantQualite("test1");
        dirigeants.add(dirigeant2);

        DirigeantBean dirigeant3 = new DirigeantBean();
        dirigeant3.setQualite("commissaireComptes");
        dirigeant3.setNature("2");
        dirigeant3.setPmRepresentantQualite("nonConcerne");
        dirigeants.add(dirigeant3);

        DirigeantBean dirigeant4 = new DirigeantBean();
        dirigeant4.setQualite("associeIndefinimentResponsable");
        dirigeant4.setNature("2");
        dirigeant4.setPmRepresentantQualite("nonConcerne");
        dirigeants.add(dirigeant4);

        DirigeantBean dirigeant5 = new DirigeantBean();
        dirigeants.add(dirigeant5);

        List<EntrepriseLieeBean> entreprisesLiee = new ArrayList<EntrepriseLieeBean>();
        EntrepriseLieeBean entrepriseLiee1 = new EntrepriseLieeBean();
        entreprisesLiee.add(entrepriseLiee1);
        EntrepriseLieeBean entrepriseLiee2 = new EntrepriseLieeBean();
        entreprisesLiee.add(entrepriseLiee2);
        EntrepriseLieeBean entrepriseLiee3 = new EntrepriseLieeBean();
        entreprisesLiee.add(entrepriseLiee3);
        EntrepriseLieeBean entrepriseLiee4 = new EntrepriseLieeBean();
        entreprisesLiee.add(entrepriseLiee4);

        entreprise.setEntreprisesLieesFusionScission(entreprisesLiee);
    }

    /**
     * cas particulier code étranger =>codePays != 050 (France)
     */
    @Test
    public void testRefToStringCodeEtrangerPourTypeRefCommune() {
        FonctionPdf fonctionsPdf = new FonctionPdf();
        fonctionsPdf.setReferentielSupraService(referentielSupraService);

        // IRAQ présent dans le référentiel.xml
        String idRefCodeEtranger = "99" + "203";
        // on s'assure qu'auncun libellé du pays étranger n'est remonté
        assertEquals(StringUtils.EMPTY, fonctionsPdf.refToString("commune", idRefCodeEtranger));
    }

    /**
     * Test de récupération de la nationalité depuis le code Nationalité
     * tronqué.
     * 
     * @throws TechniqueException
     */
    @Test
    public void testRecupererNationalite() throws TechnicalException {
        String codeNationaliteTronque = "050";
        FonctionPdf fonctionsPdf = new FonctionPdf();
        fonctionsPdf.setReferentielSupraService(referentielSupraService);
        String nationalite = FonctionPdf.recupererNationalite(codeNationaliteTronque);

        assertEquals("FRANCAISE", nationalite);
    }

    /**
     * Test de la fonction qui permet de faire une nouvelle chaîne de caractère
     * contenu dans l'objet null bean.
     */
    @Test
    public void testSubstringNull() {
        Object bean = null;
        int startIndex = 0;
        int endIndex = 0;

        String substring = FonctionPdf.substring(bean, startIndex, endIndex);
        assertEquals("", substring);
    }

    /**
     * Test de la fonction qui permet de faire une nouvelle chaîne de caractère
     * contenu dans l'objet bean.
     */
    @Test
    public void testSubstring() {
        Object bean = new String("exemple");
        int startIndex = 0;
        int endIndex = 10;

        String substring = FonctionPdf.substring(bean, startIndex, endIndex);
        assertEquals("exemple", substring);
    }

    /**
     * Test de la fonction qui permet de faire une nouvelle chaîne de caractère
     * contenu dans l'objet bean.
     */
    @Test
    public void testSubstringEndIndex() {
        Object bean = new String("exemple");
        int startIndex = 0;
        int endIndex = 3;

        String substring = FonctionPdf.substring(bean, startIndex, endIndex);
        assertEquals("exe", substring);
    }

    /**
     * Test de récupération du code département à l'aide du code commune.
     */
    @Test
    public void testRefDepartementBlank() {
        String codeCommune = "";
        FonctionPdf fonctionsPdf = new FonctionPdf();
        fonctionsPdf.setReferentielSupraService(referentielSupraService);

        String refDepartement = FonctionPdf.refDepartement(codeCommune);

        assertEquals(null, refDepartement);
    }

    /**
     * Test de récupération du code département à l'aide du code commune.
     */
    @Test
    public void testRefDepartement() {
        String codeCommune = "95607";
        FonctionPdf fonctionsPdf = new FonctionPdf();
        fonctionsPdf.setReferentielSupraService(referentielSupraService);

        String refDepartement = FonctionPdf.refDepartement(codeCommune);

        assertEquals("95", refDepartement);
    }

    /**
     * Test de récupération du libellé de la commune à l'aide de la commune.
     */
    @Test
    public void testRecupererLibelleCommuneNull() {
        String commune = null;
        FonctionPdf fonctionsPdf = new FonctionPdf();
        fonctionsPdf.setReferentielSupraService(referentielSupraService);

        String libelleCommune = FonctionPdf.recupererLibelleCommune(commune);

        assertEquals("", libelleCommune);
    }

    /**
     * Test de récupération du libellé de la commune à l'aide de la commune.
     */
    @Test
    public void testRecupererLibelleCommune() {
        String commune = "95607";
        FonctionPdf fonctionsPdf = new FonctionPdf();
        fonctionsPdf.setReferentielSupraService(referentielSupraService);

        String libelleCommune = FonctionPdf.recupererLibelleCommune(commune);

        assertEquals("TAVERNY", libelleCommune);
    }

    /**
     * Test de récupération du libellé Edi à l'aide du codeEdi.
     */
    @Test
    public void testGetLibelleEdiNull() {
        String codeEdi = null;
        FonctionPdf fonctionsPdf = new FonctionPdf();
        fonctionsPdf.setReferentielSupraService(referentielSupraService);

        String libelleEdi = FonctionPdf.getLibelleEdi(codeEdi);

        assertEquals("", libelleEdi);
    }

    /**
     * Test de récupération du libellé Edi à l'aide du codeEdi.
     */
    @Test
    public void testGetLibelleEdi() {
        String codeEdi = "M2902";
        FonctionPdf fonctionsPdf = new FonctionPdf();
        fonctionsPdf.setReferentielSupraService(referentielSupraService);

        String libelleEdi = FonctionPdf.getLibelleEdi(codeEdi);

        assertEquals("CMA DU FINISTÈRE", libelleEdi);
    }

    /**
     * Test du retour à la ligne.
     */
    @Test
    public void testRetourLigne() {
        String retourLigne = FonctionPdf.retourLigne();

        assertEquals("\n", retourLigne);
    }

}
