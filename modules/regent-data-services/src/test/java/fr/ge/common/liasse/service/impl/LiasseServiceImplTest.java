package fr.ge.common.liasse.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import fr.ge.common.liasse.dao.LiasseNumberDao;

public class LiasseServiceImplTest {

    @InjectMocks
    private LiasseGenerationImpl service;

    @Mock
    private LiasseNumberDao dao;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGenerateLiasseNumber() throws Exception {
        Long nextNumber = new Long(1);
        when(this.dao.nextVal()).thenReturn(nextNumber);
        assertThat(service.getNumeroLiasse()).isEqualTo("H10000000001");

        nextNumber = new Long(11111111);
        when(this.dao.nextVal()).thenReturn(nextNumber);
        assertThat(service.getNumeroLiasse()).isEqualTo("H10001111111");
    }
}
