/**
 * 
 */
/**
 * @author mkebaili
 *
 */
package fr.ge.common.regent.util.mapper.formalite.profil;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.xmlfield.core.XmlFieldFactory;
import org.xmlfield.core.exception.XmlFieldParsingException;
import org.xmlfield.core.types.XmlString;

import fr.ge.common.regent.bean.formalite.profil.cessation.ProfilCessationBean;
import fr.ge.common.regent.bean.xml.formalite.profil.cessation.IProfilCessation;
import fr.ge.common.regent.bean.xml.formalite.profil.creation.ICfe;
import fr.ge.common.regent.util.test.AbstractSpringTest;

/**
 * Le Class MapperProfilCessationUtilsTest.
 */
public class MapperProfilCessationUtilsTest extends AbstractSpringTest {

    /** Le xml field factory. */
    private final XmlFieldFactory xmlFieldFactory;

    /**
     * Instancie un nouveau mapper profil cessation utils test.
     */
    public MapperProfilCessationUtilsTest() {
        this.xmlFieldFactory = new XmlFieldFactory(true);
    }

    /** Le mapper profil cessation utils. */
    @Autowired
    private MapperProfilCessationUtils mapperProfilCessationUtils;

    /**
     * Test mapper.
     * 
     * @throws XmlFieldParsingException
     *             le xml field parsing exception
     */
    @Test
    public void testMapper() throws XmlFieldParsingException {

        IProfilCessation iProfilC = xmlFieldFactory.getXmlField().newObject(IProfilCessation.class);
        iProfilC.setMicroSocialOuiNon("microSocialOuiNon");

        XmlString str = iProfilC.addToEvenement();
        str.setString("testStr");
        ICfe cfe = iProfilC.newCfe();
        cfe.setCode("codeTestCfe");

        ProfilCessationBean profilC = new ProfilCessationBean();
        List<String> evenements = new ArrayList<String>();
        profilC.setEvenement(evenements);
        this.mapperProfilCessationUtils.mapper(iProfilC, profilC);

        assertEquals("microSocialOuiNon", profilC.getMicroSocialOuiNon());

        assertEquals(1, profilC.getEvenement().size());
        assertEquals("testStr", profilC.getEvenement().get(0));
        assertNotNull(profilC.getCfe());
        assertEquals("codeTestCfe", profilC.getCfe().getCode());
    }

    /**
     * Test mapper pour la récupération d'un code APE erroné.
     * 
     * @throws XmlFieldParsingException
     *             le xml field parsing exception
     */
    @Test
    public void testCodeAPEFromDBMapper() throws XmlFieldParsingException {

        IProfilCessation iProfilC = xmlFieldFactory.getXmlField().newObject(IProfilCessation.class);
        iProfilC.setActivitePrincipaleCodeAPE("1122z");

        ProfilCessationBean profilC = new ProfilCessationBean();

        this.mapperProfilCessationUtils.mapper(iProfilC, profilC);

        assertEquals("11.22Z", profilC.getActivitePrincipaleCodeAPE());

    }

    /**
     * Test mapper pour la récupération d'un code APE erroné.
     * 
     * @throws XmlFieldParsingException
     *             le xml field parsing exception
     */
    @Test
    public void testCodeAPEFromBeanMapper() throws XmlFieldParsingException {

        IProfilCessation iProfilC = xmlFieldFactory.getXmlField().newObject(IProfilCessation.class);

        ProfilCessationBean profilC = new ProfilCessationBean();
        profilC.setActivitePrincipaleCodeAPE("1122z");

        this.mapperProfilCessationUtils.mapper(profilC, iProfilC);

        assertEquals("11.22Z", iProfilC.getActivitePrincipaleCodeAPE());

    }

}
