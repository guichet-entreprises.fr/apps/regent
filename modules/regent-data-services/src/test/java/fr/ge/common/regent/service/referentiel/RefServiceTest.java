package fr.ge.common.regent.service.referentiel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import fr.ge.common.regent.bean.modele.referentiel.ENormeEvenement;
import fr.ge.common.regent.bean.xml.TypeFluxXml;
import fr.ge.common.regent.constante.TypeDossierEnum;
import fr.ge.common.regent.constante.enumeration.DestinataireXmlRegentEnum;
import fr.ge.common.regent.persistance.dao.referentiel.NormeEvenementDao;
import fr.ge.common.regent.util.test.AbstractSpringTest;

/**
 * Le Class RefServiceTest.
 */
public class RefServiceTest extends AbstractSpringTest {

    /** La constante CODE_EVT_11P. */
    private final static String CODE_EVT_11P = "11P";

    /** La constante CODE_EVT_24P. */
    private final static String CODE_EVT_24P = "24P";

    /** La constante CODE_EVT_25P. */
    private final static String CODE_EVT_25P = "25P";

    /** La constante CODE_EVT_30P. */
    private final static String CODE_EVT_30P = "30P";

    /** La constante CODE_EVT_16P. */
    private final static String CODE_EVT_16P = "16P";

    /** La constante CODE_EVT_17P. */
    private final static String CODE_EVT_17P = "17P";

    /** La constante CODE_EVT_41P. */
    private final static String CODE_EVT_41P = "41P";

    /** La constante CARTE_IDENTITE. */
    private final static String CARTE_IDENTITE = "CARTE_IDENTITE";

    /** La constante ATT_DOMICILE. */
    private final static String ATT_DOMICILE = "ATT_DOMICILE";

    /** La constante STATUTS. */
    private final static String STATUTS = "STATUTS";

    /** La constante PJ049. */
    private final static String PJ049 = "PJ049";

    /** La constante PJ053. */
    private final static String PJ053 = "PJ053";

    /** La constante PJ041. */
    private static final String PJ041 = "PJ041";

    /** La constante VERSION. */
    private static final String VERSION = "1";

    /** Le ref service. */
    @Autowired
    private ReferentielService refService;

    /** Le norme evenement dao. */
    @Autowired
    private NormeEvenementDao normeEvenementDao;

    /**
     * Test get rubrique by evenement and version.
     */
    @Test
    @Transactional(readOnly = false, value = "regent")
    public void testGetRubriqueObligaoireOrConditionnelle() {
        // prépare données
        ENormeEvenement normEvt = new ENormeEvenement();
        normEvt.setId(101);
        normEvt.setEvenement("EVENEMENT_1");
        normEvt.setRubrique("RUBRIQUE A");
        normEvt.setVersion(VERSION);
        normEvt.setCondition(Boolean.TRUE);
        normeEvenementDao.save(normEvt);

        ENormeEvenement normEvt2 = new ENormeEvenement();
        normEvt2.setId(102);
        normEvt2.setEvenement("EVENEMENT_2");
        normEvt2.setRubrique("RUBRIQUE B");
        normEvt2.setVersion(VERSION);
        normEvt2.setCondition(Boolean.TRUE);
        normeEvenementDao.save(normEvt2);

        ENormeEvenement normEvt3 = new ENormeEvenement();
        normEvt3.setId(103);
        normEvt3.setEvenement("EVENEMENT_3");
        normEvt3.setRubrique("RUBRIQUE B");
        normEvt3.setVersion(VERSION);
        normEvt3.setCondition(Boolean.FALSE);
        normeEvenementDao.save(normEvt3);

        List<String> listEvts = new ArrayList<String>();
        listEvts.add("EVENEMENT_1");
        listEvts.add("EVENEMENT_2");
        listEvts.add("EVENEMENT_3");
        List<ENormeEvenement> rubriqueByEvenementAndVersion = normeEvenementDao.getRubriqueOblogatoireOuConditionnelle(VERSION, listEvts);

        assertNotNull(rubriqueByEvenementAndVersion);
        assertEquals(3, rubriqueByEvenementAndVersion.size());
        Map<String, Boolean> rubriqueObligatoireOrConditionnelle = this.refService.getRubriqueOblogatoireOuConditionnelle(VERSION, listEvts);

        assertNotNull(rubriqueObligatoireOrConditionnelle);
        assertEquals(2, rubriqueObligatoireOrConditionnelle.keySet().size());
        assertEquals(true, rubriqueObligatoireOrConditionnelle.keySet().contains("RUBRIQUE B"));
        assertEquals(true, rubriqueObligatoireOrConditionnelle.keySet().contains("RUBRIQUE A"));
        assertEquals(false, rubriqueObligatoireOrConditionnelle.get("RUBRIQUE B"));
        assertEquals(true, rubriqueObligatoireOrConditionnelle.get("RUBRIQUE A"));
    }

    /**
     * Cas : Entrant (cf referentiel.xml) - plusieurs evenements (16P,27P,24P) -
     * destinataire CMA [ M & Z] - UN SEUL type d'opération en paramètre
     * (Regul|Modif|Cess) différent => ici R(Régul.) Attendu : 2 flux. - Flux 1
     * : 16P,17P poids 0 [typeFLUX Z] - Flux 2 : 24P poids 1 [typeFLUX M]
     */
    @Test
    public void testListerFluxParDestinataireEtEvenementsCasNominal() {
        List<TypeFluxXml> resultat = refService.listerFluxParDestinataireEtEvenements(DestinataireXmlRegentEnum.CMA, Arrays.asList(CODE_EVT_24P, CODE_EVT_16P, CODE_EVT_17P),
                TypeDossierEnum.REGULARISATION);
        Assert.assertNotNull(resultat);
        Assert.assertEquals("Le nombre de flux retournés est incorrecte", 2, resultat.size());
        /**
         * Le premier flux doit etre de type Z, avec un poids de 0, et concernce
         * les evts 16P et 17P
         */
        Assert.assertEquals("Le type du 1er flux est incorrecte", "M", resultat.get(0).getTypeDeFlux());
        Assert.assertEquals("Le poids du 1er flux est incorrecte", 0, resultat.get(0).getPoid().intValue());
        Assert.assertEquals("Les evts du 1er flux sont incorrectes", Arrays.asList(CODE_EVT_24P), resultat.get(0).getEvenements());

        /**
         * Le second flux doit etre de type M, et type opération (Regul) avec un
         * poids de 1 , et concernce l'evenement 24P
         */
        TypeFluxXml typeFluxXml2 = resultat.get(1);
        Assert.assertEquals("Le type du 2eme flux est incorrecte", "Z", typeFluxXml2.getTypeDeFlux());
        Assert.assertEquals("Les evts du 1er flux sont incorrectes", Arrays.asList(CODE_EVT_16P, CODE_EVT_17P), resultat.get(1).getEvenements());

    }

    /**
     * MT 994. Cas : Entrant (cf referentiel.xml) - plusieurs evenements
     * (11P,24P,25P,30P) - partenaire CCI -> "C" - UN SEUL type d'opération en
     * paramètre (Regul|Modif|Cess) différent => ici R(Régul.) pour 11P,25P,30P
     * et 24P Attendu : 1 flux. - Flux 1 : 11P,24P,25P & 30P
     */
    @Test
    public void testListerFluxParDestinataireEtEvenementsCasTypeRegul() {
        List<TypeFluxXml> resultat = refService.listerFluxParDestinataireEtEvenements(DestinataireXmlRegentEnum.CCI, Arrays.asList(CODE_EVT_11P, CODE_EVT_24P, CODE_EVT_25P, CODE_EVT_30P),
                TypeDossierEnum.REGULARISATION);
        Assert.assertNotNull(resultat);
        Assert.assertEquals("Le nombre de flux retournés est incorrecte", 1, resultat.size());

        /**
         * Le premier flux doit etre de type C, et type opération (Regul) avec
         * un poids de 0, et concernce l'evenement 11P
         */
        TypeFluxXml typeFluxXml1 = resultat.get(0);
        Assert.assertEquals("Le type du 1er flux est incorrecte", "C", typeFluxXml1.getTypeDeFlux());
        Assert.assertEquals("Le poids du 1er flux est incorrecte", 0, typeFluxXml1.getPoid().intValue());
        Assert.assertEquals("Les evts du 1er flux sont incorrectes", Arrays.asList(CODE_EVT_11P), typeFluxXml1.getEvenements());

    }

    /**
     * MT 994. Cas : Entrant (cf referentiel.xml) - plusieurs evenements
     * (11P,24P,25P,30P) - même type Flux pour un partenaire (type flux C) - UN
     * SEUL type d'opération (Regul|Modif|Cess) => ici R(Régul.) TypeFlux Z :
     * R(Régul.) pour 11P TypeFlux M : R(Régul.) pour 24P, 25P et 30P Attendu :
     * 2 flux. - Flux 1 : 11P - Flux 2 : 24P, 25P & 30P
     */
    @Test
    public void testListerFluxParDestinataireEtEvenementsCasTypeRegulETTypeFluxDifferentZetM() {
        List<TypeFluxXml> resultat = refService.listerFluxParDestinataireEtEvenements(DestinataireXmlRegentEnum.CMA, Arrays.asList(CODE_EVT_11P, CODE_EVT_24P, CODE_EVT_25P, CODE_EVT_30P),
                TypeDossierEnum.REGULARISATION);
        Assert.assertNotNull(resultat);
        Assert.assertEquals("Le nombre de flux retournés est incorrecte", 2, resultat.size());

        /**
         * Le premier flux doit etre de type Z, et type opération (Modif) avec
         * un poids de 0, et concernce l'evenement 11P
         */
        TypeFluxXml typeFluxXml1 = resultat.get(0);
        Assert.assertEquals("Le type du 1er flux est incorrecte", "M", typeFluxXml1.getTypeDeFlux());
        Assert.assertEquals("Le poids du 1er flux est incorrecte", 0, typeFluxXml1.getPoid().intValue());
        Assert.assertEquals("Les evts du 1er flux sont incorrectes", Arrays.asList(CODE_EVT_24P), typeFluxXml1.getEvenements());

        /**
         * Le second flux doit etre de type M, et type opération (Regul) avec un
         * poids de 1, et concernce l'evenement 24P & 25P & 30P
         */
        TypeFluxXml typeFluxXml2 = resultat.get(1);
        Assert.assertEquals("Le type du 2eme flux est incorrecte", "Z", typeFluxXml2.getTypeDeFlux());
        Assert.assertEquals("Le poids du 2eme flux est incorrecte", 1, typeFluxXml2.getPoid().intValue());
        Assert.assertEquals("Les evts du 2eme flux sont incorrectes", Arrays.asList(CODE_EVT_11P), typeFluxXml2.getEvenements());

    }

    /**
     * MT 994. --> JIRA 2105 CAS PARTICULIER! Cas : Entrant (cf referentiel.xml)
     * - plusieurs evenements (41P,25P) - partenaire Greffe -> "GREFFE" - UN
     * SEUL type d'opération en paramètre (Regul|Modif|Cess) différent => ici
     * F(Cessation.) pour 25P et 41P Attendu : 2 flux. - Flux 1 : 25P [MODIF] -
     * Flux 2 : 41P
     */
    @Ignore
    @Test
    public void testListerFluxParDestinataireEtEvenementsCasTypeCessationQuiRenvois25PdeModif() {
        List<TypeFluxXml> resultat = refService.listerFluxParDestinataireEtEvenements(DestinataireXmlRegentEnum.GREFFE, Arrays.asList(CODE_EVT_25P, CODE_EVT_41P), TypeDossierEnum.CESSATION);
        Assert.assertNotNull(resultat);
        Assert.assertEquals("Le nombre de flux retournés est incorrecte", 2, resultat.size());

        /**
         * Le premier flux doit etre de type L (CH_GREFFE), et type opération
         * (Cessation) avec un poids de 0 DONC PRIORITAIRE SUR 41P, et concernce
         * l'evenement 25P qui doit être "transformer" en type MODIF
         */
        TypeFluxXml typeFluxXml1 = resultat.get(0);
        Assert.assertEquals("Le type du 1er flux est incorrecte", "L", typeFluxXml1.getTypeDeFlux());
        Assert.assertEquals("Le poids du 1er flux est incorrecte", 0, typeFluxXml1.getPoid().intValue());
        Assert.assertEquals("Les evts du 1er flux sont incorrectes", Arrays.asList(CODE_EVT_25P), typeFluxXml1.getEvenements());
        Assert.assertEquals("Type opération/dossier est incorrecte", "M", typeFluxXml1.getTypeOperation());

        /**
         * Le second flux doit etre de type L (CH_GREFFE), et type opération
         * (Cessation) avec un poids de 1, et concernce l'evenement 41P
         */
        TypeFluxXml typeFluxXml2 = resultat.get(1);
        Assert.assertEquals("Le type du 2nd flux est incorrecte", "L", typeFluxXml2.getTypeDeFlux());
        Assert.assertEquals("Le poids du 2nd flux est incorrecte", 1, typeFluxXml2.getPoid().intValue());
        Assert.assertEquals("Les evts du 2nd flux sont incorrectes", Arrays.asList(CODE_EVT_41P), typeFluxXml2.getEvenements());
        Assert.assertEquals("Type opération/dossier est incorrecte", "F", typeFluxXml2.getTypeOperation());

    }

    /**
     * MT 994. --> JIRA 2105 CAS PARTICULIER! => test le rollback Cas : Entrant
     * (cf referentiel.xml) - plusieurs evenements (41P) - partenaire Greffe ->
     * "GREFFE" - UN SEUL type d'opération en paramètre (Regul|Modif|Cess)
     * différent => ici F(Cessation.) pour 41P Attendu : 1 flux. - Flux 1 : 41P
     */
    @Test
    public void testListerFluxParDestinataireEtEvenementsCasTypeCessationQuiRenvois25PdeModifRollback() {
        List<TypeFluxXml> resultat = refService.listerFluxParDestinataireEtEvenements(DestinataireXmlRegentEnum.GREFFE, Arrays.asList(CODE_EVT_25P, CODE_EVT_41P), TypeDossierEnum.CESSATION);
        Assert.assertNotNull(resultat);
        Assert.assertEquals("Le nombre de flux retournés est incorrecte", 1, resultat.size());

        /**
         * Le premier flux doit etre de type L (CH_GREFFE), et type opération
         * (Cessation) avec un poids de 1, et concernce l'evenement 41P
         */
        TypeFluxXml typeFluxXml1 = resultat.get(0);
        Assert.assertEquals("Le type du 1er flux est incorrecte", "L", typeFluxXml1.getTypeDeFlux());
        Assert.assertEquals("Le poids du 1er flux est incorrecte", 1, typeFluxXml1.getPoid().intValue());
        Assert.assertEquals("Les evts du 1er flux sont incorrectes", Arrays.asList(CODE_EVT_41P), typeFluxXml1.getEvenements());
        Assert.assertEquals("Type opération/dossier est incorrecte", "F", typeFluxXml1.getTypeOperation());

    }
}
