/**
 * 
 */
package fr.ge.common.regent.service.xmlregent.modification;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import fr.ge.common.regent.bean.xml.v2008.IXmlGlobal2008;
import fr.ge.common.regent.service.xmlregent.AbstractServiceGenerationXmlRegentTest;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * Test 30P.
 * 
 * @author $Author: hhichri $
 * @version $Revision: 0 $
 */
public class ServiceGenerationXmlRegent_30P_Test extends AbstractServiceGenerationXmlRegentTest {

    private static final String TYPE_FORMALITE = "modification";
    private static final String TYPE_EVENEMENT = "30P";

    /**
     * Test Mantis 1418:[URSSAF][MODIFICATION] : Conjoint collaborateur
     * l'adresse n'est pas complète. <br/>
     * Scénario : Test 92
     * <ppAdresseDifferentGerant>oui</ppAdresseDifferentGerant>
     * 
     * Résultat obtenu : Conjoint collaborateur l'adresse n'est pas complète.
     * 
     * Résultat attendu : Partie conjoint collaborateur -> Domicile : Code
     * postal (P43.8) et commune (P43.13) sont obligatoires voir norme (Si P40 =
     * O et si déclarée)
     * 
     * @throws TechnicalException
     *             :
     * @{@link TechnicalException}
     */
    @Test
    public void testCheckBaliseP43() throws TechnicalException {
        IXmlGlobal2008 result = getXmlRegent2008(TYPE_EVENEMENT, TYPE_FORMALITE, "U93010024161", "U9301", "CFE", Arrays.asList("U9301"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        Assert.assertEquals("La balise P43.3 ", "93055", result.getServiceApplicatif()[0].getLiasse()[0].getPersonnePhysique()[0].getSMP()[0].getP43()[0].getP433());
        Assert.assertEquals("La balise P43.5 ", "29", result.getServiceApplicatif()[0].getLiasse()[0].getPersonnePhysique()[0].getSMP()[0].getP43()[0].getP435());
        Assert.assertEquals("La balise P43.6 ", "B", result.getServiceApplicatif()[0].getLiasse()[0].getPersonnePhysique()[0].getSMP()[0].getP43()[0].getP436());
        Assert.assertEquals("La balise P43.7 ", "ds", result.getServiceApplicatif()[0].getLiasse()[0].getPersonnePhysique()[0].getSMP()[0].getP43()[0].getP437());
        Assert.assertEquals("La balise P43.8 ", "93500", result.getServiceApplicatif()[0].getLiasse()[0].getPersonnePhysique()[0].getSMP()[0].getP43()[0].getP438());
        Assert.assertEquals("La balise P43.10 ", "ca", result.getServiceApplicatif()[0].getLiasse()[0].getPersonnePhysique()[0].getSMP()[0].getP43()[0].getP4310());
        Assert.assertEquals("La balise P43.11 ", "RTE", result.getServiceApplicatif()[0].getLiasse()[0].getPersonnePhysique()[0].getSMP()[0].getP43()[0].getP4311());
        Assert.assertEquals("La balise P43.12 ", "honoré d'estienne d'orves", result.getServiceApplicatif()[0].getLiasse()[0].getPersonnePhysique()[0].getSMP()[0].getP43()[0].getP4312());
        Assert.assertEquals("La balise P43.13 ", "PANTIN", result.getServiceApplicatif()[0].getLiasse()[0].getPersonnePhysique()[0].getSMP()[0].getP43()[0].getP4313());
    }

    /**
     * Test check balise A12 x path.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseA12_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "U93010024161", "U9301", "CFE", Arrays.asList("U9301"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);

        List<String> xPathA12 = new ArrayList<String>();
        xPathA12.add("/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ISS/A12/A12.3");
        xPathA12.add("/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ISS/A12/A12.5");
        xPathA12.add("/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ISS/A12/A12.6");
        xPathA12.add("/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ISS/A12/A12.7");
        xPathA12.add("/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ISS/A12/A12.8");
        xPathA12.add("/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ISS/A12/A12.10");
        xPathA12.add("/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ISS/A12/A12.11");
        xPathA12.add("/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ISS/A12/A12.12");
        for (final String xPath : xPathA12) {
            String tagName = xPath.substring(xPath.lastIndexOf("/") + 1, xPath.length());
            String result = evaluteXpathFromXml(xPath, xml);
            Assert.assertNotEquals("La balise " + tagName, "", result);
        }

        xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "U93010024161", "U9301", "CFE", Arrays.asList("U9301"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);
        for (final String xPath : xPathA12) {
            String tagName = xPath.substring(xPath.lastIndexOf("/") + 1, xPath.length());
            String result = evaluteXpathFromXml(xPath, xml);
            Assert.assertEquals("La balise " + tagName, "", result);
        }

    }

    /**
     * Test check balise GUB 327.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckGUB327() throws TechnicalException {
        IXmlGlobal2008 result = getXmlRegent2008(TYPE_EVENEMENT, TYPE_FORMALITE, "M01010762454", "G7501", "TDR", Arrays.asList("M7501", "G7501"),
                AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        Assert.assertEquals("La balise C37.13 ", "BOURG-EN-BRESSE", result.getServiceApplicatif()[0].getLiasse()[0].getService()[0].getADF()[0].getC37()[0].getC3713());
    }
}
