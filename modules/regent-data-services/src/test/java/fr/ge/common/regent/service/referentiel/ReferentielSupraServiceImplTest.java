/**
 * 
 */
package fr.ge.common.regent.service.referentiel;

import java.util.ArrayList;
import java.util.List;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import fr.ge.common.referentiel.bean.IRefBean;
import fr.ge.common.referentiel.bean.RefBean;
import fr.ge.common.referentiel.bean.modele.ESupraCFECommune;
import fr.ge.common.referentiel.bean.modele.ESupraCommunes;
import fr.ge.common.referentiel.bean.modele.ESupraDepartements;
import fr.ge.common.referentiel.bean.modele.ESupraPays;
import fr.ge.common.referentiel.bean.modele.ESupraTypeVoie;
import fr.ge.common.referentiel.dao.SupraActivitesDao;
import fr.ge.common.referentiel.dao.SupraCFECommuneDao;
import fr.ge.common.referentiel.dao.SupraCFEDao;
import fr.ge.common.referentiel.dao.SupraCommunesDao;
import fr.ge.common.referentiel.dao.SupraDepartementsDao;
import fr.ge.common.referentiel.dao.SupraNationaliteDao;
import fr.ge.common.referentiel.dao.SupraPaysDao;
import fr.ge.common.referentiel.dao.SupraTypeVoieDao;
import fr.ge.common.referentiel.service.impl.ReferentielSupraServiceImpl;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * @author $Author: FADUBOIS $
 * @version $Revision: 0 $
 */
public class ReferentielSupraServiceImplTest {

    /** Le referentiel supra service */
    @InjectMocks
    private ReferentielSupraServiceImpl referentielSupraService;

    /** Le supra communes dao. */
    @Mock
    private SupraCommunesDao supraCommunesDao;

    /** Le supra departements dao. */
    @Mock
    private SupraDepartementsDao supraDepartementsDao;

    /** Le supra nationalite dao. */
    @Mock
    private SupraNationaliteDao supraNationaliteDao;

    /** Le supra pays dao. */
    @Mock
    private SupraPaysDao supraPaysDao;

    /** Le supra type voie dao. */
    @Mock
    private SupraTypeVoieDao supraTypeVoieDao;

    /** Le supra activites dao. */
    @Mock
    private SupraActivitesDao supraActivitesDao;

    /** Le supra cfe dao. */
    @Mock
    private SupraCFEDao supraCFEDao;

    /** Le supra cfe commune dao. */
    @Mock
    private SupraCFECommuneDao supraCFECommuneDao;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void test_chercherDepartementParCodeCommune() {
        ESupraCommunes commune = new ESupraCommunes();
        commune.setCodeCommune("codeCommune");
        commune.setCodeDepartement("codeDepartement");

        Mockito.when(supraCommunesDao.get("codeCommune")).thenReturn(commune);

        // appel du service
        String result = referentielSupraService.chercherDepartementParCodeCommune("codeCommune");

        // verification des appels
        Mockito.verify(supraCommunesDao).get("codeCommune");
        Assertions.assertThat(result).isEqualTo("codeDepartement");
    }

    @Test
    public void test_chercherPrefectureParCodeCommune() {
        referentielSupraService.chercherPrefectureParCodeCommune("codeCommune");
        Mockito.verify(supraDepartementsDao).chercherPrefectureParCodeCommune("codeCommune");
    }

    @Test
    public void test_getCFE() {
        referentielSupraService.getCFE("codeCfe");
        Mockito.verify(supraCFEDao).get("codeCfe");
    }

    @Test
    public void test_getCFECommune() {
        referentielSupraService.getCFECommune("codeCommune");
        Mockito.verify(supraCFECommuneDao).get("codeCommune");
    }

    @Test
    public void test_getCommune() {
        referentielSupraService.getCommune("codeCommune");
        Mockito.verify(supraCommunesDao).get("codeCommune");
    }

    @Test
    public void test_getDepartement() {
        referentielSupraService.getDepartement("codeDepartement");
        Mockito.verify(supraDepartementsDao).get("codeDepartement");
    }

    @Test
    public void test_getDepartements() {
        referentielSupraService.getDepartements();
        Mockito.verify(supraDepartementsDao).getAll("libelle");
    }

    @Test
    public void test_getListePays() {
        referentielSupraService.getListePays();
        Mockito.verify(supraPaysDao).getAll("libelle");
    }

    @Test
    public void test_getListePaysEuropeens() {
        referentielSupraService.getListePaysEuropeens();
        Mockito.verify(supraPaysDao).getAllEuropeens("libelle");
    }

    @Test
    public void test_getNationalites() {
        referentielSupraService.getNationalites();
        Mockito.verify(supraNationaliteDao).getNationalites();
    }

    @Test
    public void test_getPays() {
        referentielSupraService.getPays("codePays");
        Mockito.verify(supraPaysDao).getByCodeInsee("99" + "codePays");
    }

    @Test
    public void test_getPrefectures() {

        // initialisation des objets pour les mocks
        ESupraDepartements eSupraDepartements = new ESupraDepartements();
        eSupraDepartements.setTncc(0);
        eSupraDepartements.setCodeDepartement("codeDepartement");
        eSupraDepartements.setLibelle("libelle");
        List<ESupraDepartements> listSupraDepartements = new ArrayList<ESupraDepartements>();
        listSupraDepartements.add(eSupraDepartements);

        IRefBean refBean = new RefBean("codeDepartement", "PRÉFECTURE DE libelle");

        // definition du mock
        Mockito.when(supraDepartementsDao.getAll("libelle")).thenReturn(listSupraDepartements);

        List<IRefBean> listResultRefBean = referentielSupraService.getPrefectures();
        Mockito.verify(supraDepartementsDao).getAll("libelle");

        Assertions.assertThat(listResultRefBean.get(0).getElementId()).isEqualTo(refBean.getElementId());
        Assertions.assertThat(listResultRefBean.get(0).getElementLabel()).isEqualTo(refBean.getElementLabel());
    }

    @Test
    public void test_getTypesVoie() {
        referentielSupraService.getTypesVoie();
        Mockito.verify(supraTypeVoieDao).getAll("libelle");
    }

    @Test
    public void test_getTypesVoieParCode() {
        // initialisation des objets pour les mocks
        ESupraTypeVoie refBean = new ESupraTypeVoie();
        refBean.setCode("code");
        refBean.setLibelle("libelle");

        // definition du mock
        Mockito.when(supraTypeVoieDao.get("code")).thenReturn(refBean);

        referentielSupraService.getTypesVoieParCode("code");
        Mockito.verify(supraTypeVoieDao).get("code");
    }

    @Test
    public void test_rechercherCommuneParCodePostal() {
        referentielSupraService.rechercherCommuneParCodePostal("codePostal");
        Mockito.verify(supraCommunesDao).chercherCommuneParCodePostal("codePostal");
    }

    @Test
    public void test_rechercherCommuneParDepartement() {
        referentielSupraService.rechercherCommuneParDepartement("dpt");
        Mockito.verify(supraCommunesDao).chercherCommuneParDepartement("dpt");
    }

    @Test
    public void test_recupererNationalite() throws TechnicalException {
        referentielSupraService.recupererNationalite("codePaysTronque");
        Mockito.verify(supraNationaliteDao).recupererNationalite("codePaysTronque");
    }

    @Test
    public void test_recupererCfe() {
        // initialisation des objets pour les mocks
        ESupraCFECommune commune = new ESupraCFECommune();
        commune.setCommune("commune");

        // definition du mock
        Mockito.when(supraCFECommuneDao.get("codeCommune")).thenReturn(commune);

        referentielSupraService.recupererCfe("secteurCfe", "codeCommune");
        Mockito.verify(supraCFECommuneDao).get("codeCommune");
    }

    @Test
    public void test_isPaysUE() {
        // initialisation des objets pour les mocks
        ESupraPays pays = new ESupraPays();
        pays.setUnionEuropeenne(true);

        // definition du mock
        Mockito.when(supraPaysDao.getByCodeInsee("codePays")).thenReturn(pays);

        referentielSupraService.isPaysUE("codePays");
        Mockito.verify(supraPaysDao).getByCodeInsee("99" + "codePays");

        Boolean result = referentielSupraService.isPaysUE("codePa");
        Assertions.assertThat(result).isFalse();
    }

    @Test
    public void test_getListeCodeCommuneByCodeCfeAndTypeCFE() {
        String codeCFE = "C7501";
        String typeCFE = "CCI";

        List<String> communes = new ArrayList<String>();
        communes.add("750001");
        communes.add("750002");
        communes.add("750003");
        // definition du mock
        Mockito.when(supraCFECommuneDao.getListeCodeCommuneByCodeCfeAndTypeCFE(codeCFE, typeCFE)).thenReturn(communes);

        List<String> codeCommunes = referentielSupraService.getListeCommuneParCodeCfeAndTypeCfE(codeCFE, typeCFE);
        Mockito.verify(supraCFECommuneDao).getListeCodeCommuneByCodeCfeAndTypeCFE(codeCFE, typeCFE);
        Assertions.assertThat(codeCommunes).isNotNull();
        Assertions.assertThat(!codeCommunes.isEmpty()).isTrue();

        typeCFE = "CMA";
        communes = new ArrayList<String>();
        // definition du mock
        Mockito.when(supraCFECommuneDao.getListeCodeCommuneByCodeCfeAndTypeCFE(codeCFE, typeCFE)).thenReturn(communes);
        codeCommunes = referentielSupraService.getListeCommuneParCodeCfeAndTypeCfE(codeCFE, typeCFE);
        Mockito.verify(supraCFECommuneDao).getListeCodeCommuneByCodeCfeAndTypeCFE(codeCFE, typeCFE);
        Assertions.assertThat(codeCommunes).isNotNull();
        Assertions.assertThat(codeCommunes.isEmpty()).isTrue();
    }

}
