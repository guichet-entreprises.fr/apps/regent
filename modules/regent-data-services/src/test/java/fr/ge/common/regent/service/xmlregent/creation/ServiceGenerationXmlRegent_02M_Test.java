/**
 * 
 */
package fr.ge.common.regent.service.xmlregent.creation;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

import fr.ge.common.regent.service.xmlregent.AbstractServiceGenerationXmlRegentTest;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * Classe de tests d'XML regent pour les évènement de type 01P en création.
 * 
 * @author $Author: HZITOUNI $
 * @version $Revision: 0 $
 */

public class ServiceGenerationXmlRegent_02M_Test extends AbstractServiceGenerationXmlRegentTest {

    private static final String TYPE_FORMALITE = "creation";
    private static final String TYPE_EVENEMENT = "02M";

    /**
     * Test check balise C10.2 x path.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseC102XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "G34040000052", "G3404", "CFE+TDR", Arrays.asList("G3404"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2";
        String result = evaluteXpathFromXml(xpath, xml);
        Date aujourdhui = new Date();
        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
        Assert.assertEquals("La balise C10.2", formater.format(aujourdhui), result);
    }

    /**
     * Test check balise R05 XMLregent V_2008.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseR05V_2008() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C33010001433", "C3301", "CFE", Arrays.asList("C3301"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante/ICR/R05/R05.8";
        String result = evaluteXpathFromXml(xpath, xml);
        Assert.assertEquals("La balise R05.8", ".", result);
    }

    /**
     * Test check balise R05 XMLregent V_2016.
     * 
     * @throws TechnicalException
     *             erreur technique exception
     */
    @Test
    public void testCheckBaliseR05V_2016() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C33010001433", "C3301", "CFE", Arrays.asList("C3301"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante/ICR/R05/R05.8";
        String result = evaluteXpathFromXml(xpath, xml);
        Assert.assertEquals("La balise R05.8", ".", result);
    }

    @Test
    public void testCheckBaliseC10_2_V2008_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C33010001433", "C3301", "CFE", Arrays.asList("C3301"), AbstractServiceGenerationXmlRegentTest.VERSION_2008_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2";

        String result = evaluteXpathFromXml(xpath, xml);

        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());

        Assert.assertEquals("La balise C10.2", date, result);
    }

    @Test
    public void testCheckBaliseC10_2_V2016_XPath() throws TechnicalException {
        String xml = getXmlRegentStr(TYPE_EVENEMENT, TYPE_FORMALITE, "C33010001433", "C3301", "CFE", Arrays.asList("C3301"), AbstractServiceGenerationXmlRegentTest.VERSION_2016_XMLREGENT);
        String xpath = "/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2";

        String result = evaluteXpathFromXml(xpath, xml);

        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());

        Assert.assertEquals("La balise C10.2", date, result);
    }

}