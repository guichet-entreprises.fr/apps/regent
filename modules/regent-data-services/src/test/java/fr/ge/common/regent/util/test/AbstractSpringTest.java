package fr.ge.common.regent.util.test;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Le Class AbstractSpringTest.
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:spring/applicationContext-referentiel-config-test.xml", "classpath:spring/applicationContext-common-regent-config-test.xml" })
public abstract class AbstractSpringTest {
    // configuration du contexte Spring
}
