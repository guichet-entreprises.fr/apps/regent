package fr.ge.common.regent.persistance.dao.referentiel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import fr.ge.common.referentiel.bean.modele.ESupraPays;
import fr.ge.common.referentiel.dao.SupraPaysDao;
import fr.ge.common.regent.util.test.AbstractSpringTest;

/**
 * Le Class SupraPaysDaoTest.
 */
public class SupraPaysDaoTest extends AbstractSpringTest {

    /** Le supra pays dao. */
    @Autowired
    private SupraPaysDao supraPaysDao;

    /**
     * Test get all europeens.
     */
    @Test
    @Transactional(readOnly = true, value = "regent")
    public void testGetAllEuropeens() {
        List<ESupraPays> allEuropeens = supraPaysDao.getAllEuropeens("codePays");
        assertNotNull(allEuropeens);
        assertEquals(32, allEuropeens.size());
        assertEquals("FR", allEuropeens.get(0).getCodeIso());
    }

    /**
     * Test get by code insee.
     */
    @Test
    @Transactional(readOnly = true, value = "regent")
    public void testGetByCodeInsee() {
        String codeInsee = "99050";
        ESupraPays byCodeInsee = supraPaysDao.getByCodeInsee(codeInsee);
        assertNotNull(byCodeInsee);
        assertEquals("FR", byCodeInsee.getCodeIso());
    }

}
