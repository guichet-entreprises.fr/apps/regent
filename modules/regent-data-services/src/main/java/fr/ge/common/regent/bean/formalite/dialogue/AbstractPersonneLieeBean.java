package fr.ge.common.regent.bean.formalite.dialogue;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import fr.ge.common.regent.bean.formalite.FormaliteVueBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.AdresseBean;

/**
 * Le Class AbstractPersonneLieeBean.
 */
public abstract class AbstractPersonneLieeBean extends FormaliteVueBean {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 6539284808238674120L;

    /** Le personne liee etablissement qualite. */
    private String personneLieeEtablissementQualite;

    /** Le pp nom naissance. */
    private String ppNomNaissance;

    /** Le pp nom usage. */
    private String ppNomUsage;

    /** Le pp prenom1. */
    private String ppPrenom1;

    /** Le pp prenom2. */
    private String ppPrenom2;

    /** Le pp prenom3. */
    private String ppPrenom3;

    /** Le pp prenom4. */
    private String ppPrenom4;

    /** Le pp nationalite. */
    private String ppNationalite;

    /** Le pp date naissance. */
    private String ppDateNaissance;

    /** Le pp lieu naissance pays. */
    private String ppLieuNaissancePays;

    /** Le pp lieu naissance departement. */
    private String ppLieuNaissanceDepartement;

    /** Le pp lieu naissance commune. */
    private String ppLieuNaissanceCommune;

    /** Le pp lieu naissance ville. */
    private String ppLieuNaissanceVille;

    /** Le pp adresse. */
    private AdresseBean ppAdresse = new AdresseBean();

    /**
     * Getter de l'attribut personneLieeEtablissementQualite.
     * 
     * @return la valeur de personneLieeEtablissementQualite
     */
    public String getPersonneLieeEtablissementQualite() {
        return this.personneLieeEtablissementQualite;
    }

    /**
     * Getter de l'attribut ppAdresse.
     * 
     * @return la valeur de ppAdresse
     */
    public AdresseBean getPpAdresse() {
        return this.ppAdresse;
    }

    /**
     * Getter de l'attribut ppDateNaissance.
     * 
     * @return la valeur de ppDateNaissance
     */
    public String getPpDateNaissance() {
        return this.ppDateNaissance;
    }

    /**
     * Getter de l'attribut ppLieuNaissanceCommune.
     * 
     * @return la valeur de ppLieuNaissanceCommune
     */
    public String getPpLieuNaissanceCommune() {
        return this.ppLieuNaissanceCommune;
    }

    /**
     * Getter de l'attribut ppLieuNaissanceDepartement.
     * 
     * @return la valeur de ppLieuNaissanceDepartement
     */
    public String getPpLieuNaissanceDepartement() {
        return this.ppLieuNaissanceDepartement;
    }

    /**
     * Getter de l'attribut ppLieuNaissancePays.
     * 
     * @return la valeur de ppLieuNaissancePays
     */
    public String getPpLieuNaissancePays() {
        return this.ppLieuNaissancePays;
    }

    /**
     * Getter de l'attribut ppLieuNaissanceVille.
     * 
     * @return la valeur de ppLieuNaissanceVille
     */
    public String getPpLieuNaissanceVille() {
        return this.ppLieuNaissanceVille;
    }

    /**
     * Getter de l'attribut ppNationalite.
     * 
     * @return la valeur de ppNationalite
     */
    public String getPpNationalite() {
        return this.ppNationalite;
    }

    /**
     * Getter de l'attribut ppNomNaissance.
     * 
     * @return la valeur de ppNomNaissance
     */
    public String getPpNomNaissance() {
        return this.ppNomNaissance;
    }

    /**
     * Getter de l'attribut ppNomUsage.
     * 
     * @return la valeur de ppNomUsage
     */
    public String getPpNomUsage() {
        return this.ppNomUsage;
    }

    /**
     * Getter de l'attribut ppPrenom1.
     * 
     * @return la valeur de ppPrenom1
     */
    public String getPpPrenom1() {
        return this.ppPrenom1;
    }

    /**
     * Getter de l'attribut ppPrenom2.
     * 
     * @return la valeur de ppPrenom2
     */
    public String getPpPrenom2() {
        return this.ppPrenom2;
    }

    /**
     * Getter de l'attribut ppPrenom3.
     * 
     * @return la valeur de ppPrenom3
     */
    public String getPpPrenom3() {
        return this.ppPrenom3;
    }

    /**
     * Getter de l'attribut ppPrenom4.
     * 
     * @return la valeur de ppPrenom4
     */
    public String getPpPrenom4() {
        return this.ppPrenom4;
    }

    /**
     * Setter de l'attribut personneLieeEtablissementQualite.
     * 
     * @param personneLieeEtablissementQualite
     *            la nouvelle valeur de personneLieeEtablissementQualite
     */
    public void setPersonneLieeEtablissementQualite(String personneLieeEtablissementQualite) {
        this.personneLieeEtablissementQualite = personneLieeEtablissementQualite;
    }

    /**
     * Setter de l'attribut ppAdresse.
     * 
     * @param ppAdresse
     *            la nouvelle valeur de ppAdresse
     */
    public void setPpAdresse(AdresseBean ppAdresse) {
        this.ppAdresse = ppAdresse;
    }

    /**
     * Setter de l'attribut ppDateNaissance.
     * 
     * @param ppDateNaissance
     *            la nouvelle valeur de ppDateNaissance
     */
    public void setPpDateNaissance(String ppDateNaissance) {
        this.ppDateNaissance = ppDateNaissance;
    }

    /**
     * Setter de l'attribut ppLieuNaissanceCommune.
     * 
     * @param ppLieuNaissanceCommune
     *            la nouvelle valeur de ppLieuNaissanceCommune
     */
    public void setPpLieuNaissanceCommune(String ppLieuNaissanceCommune) {
        this.ppLieuNaissanceCommune = ppLieuNaissanceCommune;
    }

    /**
     * Setter de l'attribut ppLieuNaissanceDepartement.
     * 
     * @param ppLieuNaissanceDepartement
     *            la nouvelle valeur de ppLieuNaissanceDepartement
     */
    public void setPpLieuNaissanceDepartement(String ppLieuNaissanceDepartement) {
        this.ppLieuNaissanceDepartement = ppLieuNaissanceDepartement;
    }

    /**
     * Setter de l'attribut ppLieuNaissancePays.
     * 
     * @param ppLieuNaissancePays
     *            la nouvelle valeur de ppLieuNaissancePays
     */
    public void setPpLieuNaissancePays(String ppLieuNaissancePays) {
        this.ppLieuNaissancePays = ppLieuNaissancePays;
    }

    /**
     * Setter de l'attribut ppLieuNaissanceVille.
     * 
     * @param ppLieuNaissanceVille
     *            la nouvelle valeur de ppLieuNaissanceVille
     */
    public void setPpLieuNaissanceVille(String ppLieuNaissanceVille) {
        this.ppLieuNaissanceVille = ppLieuNaissanceVille;
    }

    /**
     * Setter de l'attribut ppNationalite.
     * 
     * @param ppNationalite
     *            la nouvelle valeur de ppNationalite
     */
    public void setPpNationalite(String ppNationalite) {
        this.ppNationalite = ppNationalite;
    }

    /**
     * Setter de l'attribut ppNomNaissance.
     * 
     * @param ppNomNaissance
     *            la nouvelle valeur de ppNomNaissance
     */
    public void setPpNomNaissance(String ppNomNaissance) {
        this.ppNomNaissance = ppNomNaissance;
    }

    /**
     * Setter de l'attribut ppNomUsage.
     * 
     * @param ppNomUsage
     *            la nouvelle valeur de ppNomUsage
     */
    public void setPpNomUsage(String ppNomUsage) {
        this.ppNomUsage = ppNomUsage;
    }

    /**
     * Setter de l'attribut ppPrenom1.
     * 
     * @param ppPrenom1
     *            la nouvelle valeur de ppPrenom1
     */
    public void setPpPrenom1(String ppPrenom1) {
        this.ppPrenom1 = ppPrenom1;
    }

    /**
     * Setter de l'attribut ppPrenom2.
     * 
     * @param ppPrenom2
     *            la nouvelle valeur de ppPrenom2
     */
    public void setPpPrenom2(String ppPrenom2) {
        this.ppPrenom2 = ppPrenom2;
    }

    /**
     * Setter de l'attribut ppPrenom3.
     * 
     * @param ppPrenom3
     *            la nouvelle valeur de ppPrenom3
     */
    public void setPpPrenom3(String ppPrenom3) {
        this.ppPrenom3 = ppPrenom3;
    }

    /**
     * Setter de l'attribut ppPrenom4.
     * 
     * @param ppPrenom4
     *            la nouvelle valeur de ppPrenom4
     */
    public void setPpPrenom4(String ppPrenom4) {
        this.ppPrenom4 = ppPrenom4;
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

}
