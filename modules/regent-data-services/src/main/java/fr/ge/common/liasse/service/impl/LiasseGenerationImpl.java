/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 */

package fr.ge.common.liasse.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.ge.common.liasse.dao.LiasseNumberDao;
import fr.ge.common.liasse.service.ILiasseGeneration;

/**
 * @author mtakerra
 *
 */
@Service
public class LiasseGenerationImpl implements ILiasseGeneration {

    /** La constante NUM_LIASSE_COLUMN. */
    private static final String NUM_LIASSE_COLUMN = "numLiasse";

    /** La constante NUM_LIASSE_PREFIXE. */
    private static final String NUM_LIASSE_PREFIXE = "H1000";

    /** La constante NUM_LIASSE_FORMAT. */
    private static final String NUM_LIASSE_FORMAT = "%07d";

    /** La constante NUM_LIASSE_SIZE. */
    private static final Integer NUM_LIASSE_SIZE = 7;

    /** Le regles regent dao. */
    @Autowired
    private LiasseNumberDao liasseNumberDao;

    /**
     * Récupère la valeur courante formatée du numero de liasse du prochain
     * dossier.
     * 
     * @return numero de liasse a assigner à un dossier au format
     *         "H1000<numero7chiffres>"
     */
    @Override
    public String getNumeroLiasse() {
        final Long numLiasse = this.getNumero();
        return String.format("%s%s", NUM_LIASSE_PREFIXE, this.transformeNum(numLiasse, NUM_LIASSE_FORMAT, NUM_LIASSE_SIZE));
    }

    /**
     * Transforme l'entier <code>num</code> en String avec le format
     * <code>format</code>. Si <code>num</code> contient plus de
     * <code>maxSize</code> digit, il est tronqué. Sinon il est compléter avec
     * des <code>0</code>. <br>
     * Exemples :<br>
     * <code>transformeNumero("1234567", "%07d", 7)</code> retourne
     * <code>"1234567"</code><br>
     * <code>transformeNumero("123456789", "%07d", 7)</code> retourne
     * <code>"3456789"</code><br>
     * <code>transformeNumero("1234", "%07d", 7)</code> retourne
     * <code>"0001234"</code><br>
     *
     * @param num
     *            le num
     * @param format
     *            le format
     * @param maxSize
     *            le max size
     * @return le string
     */
    private String transformeNum(Long num, String format, int maxSize) {
        String numString = num.toString();
        if (numString.length() > maxSize) {
            numString = numString.substring(numString.length() - maxSize, numString.length());
        }
        return String.format(format, Long.parseLong(numString));
    }

    /**
     * Récupère les numéro en base.
     *
     * @return le numero
     */
    @Transactional(readOnly = false)
    private Long getNumero() {
        return this.liasseNumberDao.nextVal();
    }

}
