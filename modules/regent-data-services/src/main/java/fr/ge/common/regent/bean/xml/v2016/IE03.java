package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IE03.
 */
@ResourceXPath("/IE03")
public interface IE03 {

  /**
   * Get le e031.
   *
   * @return le e031
   */
  @FieldXPath("E03.1")
  String getE031();

  /**
   * Set le e031.
   *
   * @param E031
   *          le nouveau e031
   */
  void setE031(String E031);

  /**
   * Get le e032.
   *
   * @return le e032
   */
  @FieldXPath("E03.2")
  String getE032();

  /**
   * Set le e032.
   *
   * @param E032
   *          le nouveau e032
   */
  void setE032(String E032);

}
