package fr.ge.common.regent.bean.formalite.dialogue.creation;

import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.format.annotation.NumberFormat;

import fr.ge.common.regent.bean.formalite.dialogue.AbstractEtablissementBean;

/**
 * Le Class EtablissementBean.
 */
public class EtablissementBean extends AbstractEtablissementBean<ActiviteSoumiseQualificationBean> implements IFormaliteVue {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 2091654817820855275L;

    /** Le activite elevage. */
    private String activiteElevage;

    /** Le activite immobiliere. */
    private String activiteImmobiliere;

    /** Le activite lieu exercice. */
    private String activiteLieuExercice;

    /** Le activite lieu exercice autre. */
    private String activiteLieuExerciceAutre;

    /** Le activite plus importante agricole1. */
    private String activitePlusImportanteAgricole1;

    /** Le activite plus importante agricole2. */
    private String activitePlusImportanteAgricole2;

    /** Le activites accessoires bicbnc. */
    private String activitesAccessoiresBICBNC;

    /** Le activites exercees agricole. */
    private List<String> activitesExerceesAgricole;

    /** Le activite viticole. */
    private String activiteViticole;

    /** Le effectif salarie agricole nombre. */
    @NumberFormat
    private Integer effectifSalarieAgricoleNombre;

    /** Le est activite liberale. */
    private String estActiviteLiberale;

    /** Le periode activite saisonniere1 date debut. */
    private String periodeActiviteSaisonniere1DateDebut;

    /** Le periode activite saisonniere1 date fin. */
    private String periodeActiviteSaisonniere1DateFin;

    /** Le periode activite saisonniere2 date debut. */
    private String periodeActiviteSaisonniere2DateDebut;

    /** Le periode activite saisonniere2 date fin. */
    private String periodeActiviteSaisonniere2DateFin;

    /** Le precedent exploitant nombre. */
    @NumberFormat
    private Integer precedentExploitantNombre;

    /** Le secteur activite plus important. */
    private String secteurActivitePlusImportant;

    /** Le secteurs activites. */
    private String secteursActivites;

    /**
     * Getter de l'attribut activiteElevage.
     * 
     * @return la valeur de activiteElevage
     */
    public String getActiviteElevage() {
        return this.activiteElevage;
    }

    /**
     * Getter de l'attribut activiteImmobiliere.
     * 
     * @return la valeur de activiteImmobiliere
     */
    public String getActiviteImmobiliere() {
        return this.activiteImmobiliere;
    }

    /**
     * Getter de l'attribut activiteLieuExercice.
     * 
     * @return la valeur de activiteLieuExercice
     */
    public String getActiviteLieuExercice() {
        return this.activiteLieuExercice;
    }

    /**
     * Getter de l'attribut activiteLieuExerciceAutre.
     * 
     * @return la valeur de activiteLieuExerciceAutre
     */
    public String getActiviteLieuExerciceAutre() {
        return this.activiteLieuExerciceAutre;
    }

    /**
     * Getter de l'attribut activitePlusImportanteAgricole1.
     * 
     * @return la valeur de activitePlusImportanteAgricole1
     */
    public String getActivitePlusImportanteAgricole1() {
        return this.activitePlusImportanteAgricole1;
    }

    /**
     * Getter de l'attribut activitePlusImportanteAgricole2.
     * 
     * @return la valeur de activitePlusImportanteAgricole2
     */
    public String getActivitePlusImportanteAgricole2() {
        return this.activitePlusImportanteAgricole2;
    }

    /**
     * Getter de l'attribut activitesAccessoiresBICBNC.
     * 
     * @return la valeur de activitesAccessoiresBICBNC
     */
    public String getActivitesAccessoiresBICBNC() {
        return this.activitesAccessoiresBICBNC;
    }

    /**
     * Getter de l'attribut activitesExerceesAgricole.
     * 
     * @return la valeur de activitesExerceesAgricole
     */
    public List<String> getActivitesExerceesAgricole() {
        return this.activitesExerceesAgricole;
    }

    /**
     * Getter de l'attribut activiteViticole.
     * 
     * @return la valeur de activiteViticole
     */
    public String getActiviteViticole() {
        return this.activiteViticole;
    }

    /**
     * Getter de l'attribut effectifSalarieAgricoleNombre.
     * 
     * @return la valeur de effectifSalarieAgricoleNombre
     */
    public Integer getEffectifSalarieAgricoleNombre() {
        return this.effectifSalarieAgricoleNombre;
    }

    /**
     * Getter de l'attribut estActiviteLiberale.
     * 
     * @return la valeur de estActiviteLiberale
     */
    public String getEstActiviteLiberale() {
        return this.estActiviteLiberale;
    }

    /**
     * Getter de l'attribut periodeActiviteSaisonniere1DateDebut.
     * 
     * @return la valeur de periodeActiviteSaisonniere1DateDebut
     */
    public String getPeriodeActiviteSaisonniere1DateDebut() {
        return this.periodeActiviteSaisonniere1DateDebut;
    }

    /**
     * Getter de l'attribut periodeActiviteSaisonniere1DateFin.
     * 
     * @return la valeur de periodeActiviteSaisonniere1DateFin
     */
    public String getPeriodeActiviteSaisonniere1DateFin() {
        return this.periodeActiviteSaisonniere1DateFin;
    }

    /**
     * Getter de l'attribut periodeActiviteSaisonniere2DateDebut.
     * 
     * @return la valeur de periodeActiviteSaisonniere2DateDebut
     */
    public String getPeriodeActiviteSaisonniere2DateDebut() {
        return this.periodeActiviteSaisonniere2DateDebut;
    }

    /**
     * Getter de l'attribut periodeActiviteSaisonniere2DateFin.
     * 
     * @return la valeur de periodeActiviteSaisonniere2DateFin
     */
    public String getPeriodeActiviteSaisonniere2DateFin() {
        return this.periodeActiviteSaisonniere2DateFin;
    }

    /**
     * Getter de l'attribut precedentExploitantNombre.
     * 
     * @return la valeur de precedentExploitantNombre
     */
    public Integer getPrecedentExploitantNombre() {
        return this.precedentExploitantNombre;
    }

    /**
     * Getter de l'attribut secteurActivitePlusImportant.
     * 
     * @return la valeur de secteurActivitePlusImportant
     */
    public String getSecteurActivitePlusImportant() {
        return this.secteurActivitePlusImportant;
    }

    /**
     * Getter de l'attribut secteursActivites.
     * 
     * @return la valeur de secteursActivites
     */
    public String getSecteursActivites() {
        return this.secteursActivites;
    }

    /**
     * Setter de l'attribut activiteElevage.
     * 
     * @param activiteElevage
     *            la nouvelle valeur de activiteElevage
     */
    public void setActiviteElevage(String activiteElevage) {
        this.activiteElevage = activiteElevage;
    }

    /**
     * Setter de l'attribut activiteImmobiliere.
     * 
     * @param activiteImmobiliere
     *            la nouvelle valeur de activiteImmobiliere
     */
    public void setActiviteImmobiliere(String activiteImmobiliere) {
        this.activiteImmobiliere = activiteImmobiliere;
    }

    /**
     * Setter de l'attribut activiteLieuExercice.
     * 
     * @param activiteLieuExercice
     *            la nouvelle valeur de activiteLieuExercice
     */
    public void setActiviteLieuExercice(String activiteLieuExercice) {
        this.activiteLieuExercice = activiteLieuExercice;
    }

    /**
     * Setter de l'attribut activiteLieuExerciceAutre.
     * 
     * @param activiteLieuExerciceAutre
     *            la nouvelle valeur de activiteLieuExerciceAutre
     */
    public void setActiviteLieuExerciceAutre(String activiteLieuExerciceAutre) {
        this.activiteLieuExerciceAutre = activiteLieuExerciceAutre;
    }

    /**
     * Setter de l'attribut activitePlusImportante.
     * 
     * @param activitePlusImportante
     *            la nouvelle valeur de activitePlusImportante
     */
    @Override
    public void setActivitePlusImportante(String activitePlusImportante) {
        // Tronque la string
        // XSD Type :
        // AnonType_E71ACEEtablissementEntrepriseLiasseServiceApplicatifREGENT-XML;
        // Balise
        // <E71>
        int sizeMaxActivitePlusImportante = 140;
        String activite = activitePlusImportante;
        if (activite != null && activite.length() > sizeMaxActivitePlusImportante) {
            activite = activite.substring(0, sizeMaxActivitePlusImportante);
        }
        this.activitePlusImportante = activite;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setActivites(String activites) {
        // Tronque la string
        // XSD Type : Activite420_Type; Balise <E70>
        int sizeMaxActivites = 420;
        String activite = activites;
        if (activite != null && activite.length() > sizeMaxActivites) {
            activite = activite.substring(0, sizeMaxActivites);
        }
        // ce champ est renseigné par la fonction
        // FonctionsProfilEntrepriseV2.recapitulerActivite(String,
        // descrLibreActivite, String)
        // aujourd'hui la SFD ProfilV2 limite le descrLibreActivite à 300carac,
        // on tronquera à 420 par mesure de sécurité si le retour de cette
        // fonction dépasse 420 carac.
        this.activites = activite;

    }

    /**
     * Setter de l'attribut activitePlusImportanteAgricole1.
     * 
     * @param activitePlusImportanteAgricole1
     *            la nouvelle valeur de activitePlusImportanteAgricole1
     */
    public void setActivitePlusImportanteAgricole1(String activitePlusImportanteAgricole1) {
        this.activitePlusImportanteAgricole1 = activitePlusImportanteAgricole1;
    }

    /**
     * Setter de l'attribut activitePlusImportanteAgricole2.
     * 
     * @param activitePlusImportanteAgricole2
     *            la nouvelle valeur de activitePlusImportanteAgricole2
     */
    public void setActivitePlusImportanteAgricole2(String activitePlusImportanteAgricole2) {
        this.activitePlusImportanteAgricole2 = activitePlusImportanteAgricole2;
    }

    /**
     * Setter de l'attribut activitesAccessoiresBICBNC.
     * 
     * @param activitesAccessoiresBICBNC
     *            la nouvelle valeur de activitesAccessoiresBICBNC
     */
    public void setActivitesAccessoiresBICBNC(String activitesAccessoiresBICBNC) {
        this.activitesAccessoiresBICBNC = activitesAccessoiresBICBNC;
    }

    /**
     * Setter de l'attribut activitesExerceesAgricole.
     * 
     * @param activitesExerceesAgricole
     *            la nouvelle valeur de activitesExerceesAgricole
     */
    public void setActivitesExerceesAgricole(List<String> activitesExerceesAgricole) {
        this.activitesExerceesAgricole = activitesExerceesAgricole;
    }

    /**
     * Setter de l'attribut activiteViticole.
     * 
     * @param activiteViticole
     *            la nouvelle valeur de activiteViticole
     */
    public void setActiviteViticole(String activiteViticole) {
        this.activiteViticole = activiteViticole;
    }

    /**
     * Setter de l'attribut effectifSalarieAgricoleNombre.
     * 
     * @param effectifSalarieAgricoleNombre
     *            la nouvelle valeur de effectifSalarieAgricoleNombre
     */
    public void setEffectifSalarieAgricoleNombre(Integer effectifSalarieAgricoleNombre) {
        this.effectifSalarieAgricoleNombre = effectifSalarieAgricoleNombre;
    }

    /**
     * Setter de l'attribut estActiviteLiberale.
     * 
     * @param estActiviteLiberale
     *            la nouvelle valeur de estActiviteLiberale
     */
    public void setEstActiviteLiberale(String estActiviteLiberale) {
        this.estActiviteLiberale = estActiviteLiberale;
    }

    /**
     * Setter de l'attribut periodeActiviteSaisonniere1DateDebut.
     * 
     * @param periodeActiviteSaisonniere1DateDebut
     *            la nouvelle valeur de periodeActiviteSaisonniere1DateDebut
     */
    public void setPeriodeActiviteSaisonniere1DateDebut(String periodeActiviteSaisonniere1DateDebut) {
        this.periodeActiviteSaisonniere1DateDebut = periodeActiviteSaisonniere1DateDebut;
    }

    /**
     * Setter de l'attribut periodeActiviteSaisonniere1DateFin.
     * 
     * @param periodeActiviteSaisonniere1DateFin
     *            la nouvelle valeur de periodeActiviteSaisonniere1DateFin
     */
    public void setPeriodeActiviteSaisonniere1DateFin(String periodeActiviteSaisonniere1DateFin) {
        this.periodeActiviteSaisonniere1DateFin = periodeActiviteSaisonniere1DateFin;
    }

    /**
     * Setter de l'attribut periodeActiviteSaisonniere2DateDebut.
     * 
     * @param periodeActiviteSaisonniere2DateDebut
     *            la nouvelle valeur de periodeActiviteSaisonniere2DateDebut
     */
    public void setPeriodeActiviteSaisonniere2DateDebut(String periodeActiviteSaisonniere2DateDebut) {
        this.periodeActiviteSaisonniere2DateDebut = periodeActiviteSaisonniere2DateDebut;
    }

    /**
     * Setter de l'attribut periodeActiviteSaisonniere2DateFin.
     * 
     * @param periodeActiviteSaisonniere2DateFin
     *            la nouvelle valeur de periodeActiviteSaisonniere2DateFin
     */
    public void setPeriodeActiviteSaisonniere2DateFin(String periodeActiviteSaisonniere2DateFin) {
        this.periodeActiviteSaisonniere2DateFin = periodeActiviteSaisonniere2DateFin;
    }

    /**
     * Setter de l'attribut precedentExploitantNombre.
     * 
     * @param precedentExploitantNombre
     *            la nouvelle valeur de precedentExploitantNombre
     */
    public void setPrecedentExploitantNombre(Integer precedentExploitantNombre) {
        this.precedentExploitantNombre = precedentExploitantNombre;
    }

    /**
     * Setter de l'attribut secteurActivitePlusImportant.
     * 
     * @param secteurActivitePlusImportant
     *            la nouvelle valeur de secteurActivitePlusImportant
     */
    public void setSecteurActivitePlusImportant(String secteurActivitePlusImportant) {
        int sizeMaxSecteur = 100;
        String secteur = secteurActivitePlusImportant;
        if (secteur != null && secteur.length() > sizeMaxSecteur) {
            secteur = secteur.substring(0, sizeMaxSecteur);
        }
        this.secteurActivitePlusImportant = secteur;
    }

    /**
     * Setter de l'attribut secteursActivites.
     * 
     * @param secteursActivites
     *            la nouvelle valeur de secteursActivites
     */
    public void setSecteursActivites(String secteursActivites) {
        this.secteursActivites = secteursActivites;
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see fr.guichetentreprises.vue.AbstractEtablissementBean#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
