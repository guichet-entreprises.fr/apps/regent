package fr.ge.common.regent.service.exception;

/**
 * Exception signifiant que le XML contient des erreurs non fatales.
 * 
 */
public class AbstractXmlException extends Exception {

  /** La constante serialVersionUID. */
  private static final long serialVersionUID = 5074192888369496375L;

  /**
   * Constructeur.
   * 
   * @param message
   *          message d'erreur
   */
  public AbstractXmlException(String message) {
    super(message);
  }

  /**
   * Constructeur.
   * 
   * @param message
   *          message d'erreur
   * @param throwable
   *          l'erreur fatale
   */
  public AbstractXmlException(String message, Throwable throwable) {
    super(message, throwable);
  }

}
