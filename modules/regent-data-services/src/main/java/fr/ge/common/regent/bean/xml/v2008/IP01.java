package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;
import org.xmlfield.core.types.XmlString;

/**
 * Le Interface IP01.
 */
@ResourceXPath("/P01")
public interface IP01 {

  /**
   * Get le p011.
   *
   * @return le p011
   */
  @FieldXPath("P01.1")
  String getP011();

  /**
   * Set le p011.
   *
   * @param P011
   *          le nouveau p011
   */
  void setP011(String P011);

  /**
   * Get le p012.
   *
   * @return le p012
   */
  @FieldXPath("P01.2")
  String getP012();

  /**
   * Set le p012.
   *
   * @param P012
   *          le nouveau p012
   */
  void setP012(String P012);

  /**
   * Get le p013.
   *
   * @return le p013
   */
  @FieldXPath("P01.3")
  XmlString[] getP013();

  /**
   * Ajoute le to p013.
   *
   * @return le xml string
   */
  XmlString addToP013();

  /**
   * Set le p013.
   *
   * @param P013
   *          le nouveau p013
   */
  void setP013(XmlString[] P013);

}
