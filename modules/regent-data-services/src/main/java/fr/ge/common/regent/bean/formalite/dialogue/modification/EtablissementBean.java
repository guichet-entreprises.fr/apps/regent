package fr.ge.common.regent.bean.formalite.dialogue.modification;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.format.annotation.NumberFormat;

import fr.ge.common.regent.bean.formalite.dialogue.AbstractEtablissementBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.AdresseBean;

/**
 * Le Class EtablissementBean.
 */
public class EtablissementBean extends AbstractEtablissementBean<ActiviteSoumiseQualificationBean> implements IFormaliteModificationVue {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 2911077573445327336L;

    /** Le modif date adresse1. */
    private String modifDateAdresse1;

    /** Le modif date adresse. */
    private String modifDateAdresse;

    /** Le modif ancienne adresse1. */
    private AdresseBean modifAncienneAdresse1 = new AdresseBean();

    /** Le modif date activite. */
    private String modifDateActivite;

    /** Le modif evenements3. */
    private String modifEvenements3;

    /** Le destination etablissement principal. */
    private String destinationEtablissementPrincipal;

    /** Le destination autre1. */
    private String destinationAutre1;

    /** Le activites1. */
    private String activites1;

    /** Le activites2. */
    private String activites2;

    /** Le suppression activite par. */
    private String suppressionActivitePar;

    /** Le suppression activite autre. */
    private String suppressionActiviteAutre;

    /** Le effectif salarie vrp. */
    @NumberFormat
    private Integer effectifSalarieVrp;

    /** effectifsalariePresence. **/
    private String effectifsalariePresence;

    /** aqpaSalarieQualifie. */
    private String aqpaSalarieQualifie;

    /** periodeActiviteSaisonniere1DateDebut. */
    private String periodeActiviteSaisonniere1DateDebut;
    /** periodeActiviteSaisonniere1DateFin. */
    private String periodeActiviteSaisonniere1DateFin;
    /** periodeActiviteSaisonniere2DateDebut. */
    private String periodeActiviteSaisonniere2DateDebut;
    /** periodeActiviteSaisonniere2DateFin. */
    private String periodeActiviteSaisonniere2DateFin;

    /** les activites agricole exercées . */
    private String activitesExerceesAgricole;

    /** les secteurs activites. */
    private String secteursActivites;

    /**
     * Est-ce que le transfert s'effectue dans le même département ? 'oui' ou
     * 'non'.
     */
    private String transfertMemeDepartement;

    /**
     * Get le modif date adresse1.
     *
     * @return the modifDateAdresse1
     */
    public String getModifDateAdresse1() {
        return this.modifDateAdresse1;
    }

    /**
     * Set le modif date adresse1.
     *
     * @param modifDateAdresse1
     *            the modifDateAdresse1 to set
     */
    public void setModifDateAdresse1(String modifDateAdresse1) {
        this.modifDateAdresse1 = modifDateAdresse1;
    }

    /**
     * Get le modif ancienne adresse1.
     *
     * @return the modifAncienneAdresse1
     */
    public AdresseBean getModifAncienneAdresse1() {
        return this.modifAncienneAdresse1;
    }

    /**
     * Set le modif ancienne adresse1.
     *
     * @param modifAncienneAdresse1
     *            the modifAncienneAdresse1 to set
     */
    public void setModifAncienneAdresse1(AdresseBean modifAncienneAdresse1) {
        this.modifAncienneAdresse1 = modifAncienneAdresse1;
    }

    /**
     * Get le modif date activite.
     *
     * @return the modifDateActivite
     */
    public String getModifDateActivite() {
        return this.modifDateActivite;
    }

    /**
     * Set le modif date activite.
     *
     * @param modifDateActivite
     *            the modifDateActivite to set
     */
    public void setModifDateActivite(String modifDateActivite) {
        this.modifDateActivite = modifDateActivite;
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see fr.guichetentreprises.vue.AbstractEtablissementBean#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    /**
     * Get le modif date adresse.
     *
     * @return the modifDateAdresse
     */
    public String getModifDateAdresse() {
        return modifDateAdresse;
    }

    /**
     * Set le modif date adresse.
     *
     * @param modifDateAdresse
     *            the modifDateAdresse to set
     */
    public void setModifDateAdresse(String modifDateAdresse) {
        this.modifDateAdresse = modifDateAdresse;
    }

    /**
     * Get le modif evenements3.
     *
     * @return the modifEvenements3
     */
    public String getModifEvenements3() {
        return modifEvenements3;
    }

    /**
     * Set le modif evenements3.
     *
     * @param modifEvenements3
     *            the modifEvenements3 to set
     */
    public void setModifEvenements3(String modifEvenements3) {
        this.modifEvenements3 = modifEvenements3;
    }

    /**
     * Get le destination etablissement principal.
     *
     * @return the destinationEtablissementPrincipal
     */
    public String getDestinationEtablissementPrincipal() {
        return destinationEtablissementPrincipal;
    }

    /**
     * Set le destination etablissement principal.
     *
     * @param destinationEtablissementPrincipal
     *            the destinationEtablissementPrincipal to set
     */
    public void setDestinationEtablissementPrincipal(String destinationEtablissementPrincipal) {
        this.destinationEtablissementPrincipal = destinationEtablissementPrincipal;
    }

    /**
     * Get le destination autre1.
     *
     * @return the destinationAutre1
     */
    public String getDestinationAutre1() {
        return destinationAutre1;
    }

    /**
     * Set le destination autre1.
     *
     * @param destinationAutre1
     *            the destinationAutre1 to set
     */
    public void setDestinationAutre1(String destinationAutre1) {
        this.destinationAutre1 = destinationAutre1;
    }

    /**
     * Get le activites1.
     *
     * @return the activites1
     */
    public String getActivites1() {
        return activites1;
    }

    /**
     * Set le activites1.
     *
     * @param activites1
     *            the activites1 to set
     */
    public void setActivites1(String activites1) {
        this.activites1 = activites1;
    }

    /**
     * Get le activites2.
     *
     * @return the activites2
     */
    public String getActivites2() {
        return activites2;
    }

    /**
     * Set le activites2.
     *
     * @param activites2
     *            the activites2 to set
     */
    public void setActivites2(String activites2) {
        this.activites2 = activites2;
    }

    /**
     * Get le suppression activite par.
     *
     * @return the suppressionActivitePar
     */
    public String getSuppressionActivitePar() {
        return suppressionActivitePar;
    }

    /**
     * Set le suppression activite par.
     *
     * @param suppressionActivitePar
     *            the suppressionActivitePar to set
     */
    public void setSuppressionActivitePar(String suppressionActivitePar) {
        this.suppressionActivitePar = suppressionActivitePar;
    }

    /**
     * Get le suppression activite autre.
     *
     * @return the suppressionActiviteAutre
     */
    public String getSuppressionActiviteAutre() {
        return suppressionActiviteAutre;
    }

    /**
     * Set le suppression activite autre.
     *
     * @param suppressionActiviteAutre
     *            the suppressionActiviteAutre to set
     */
    public void setSuppressionActiviteAutre(String suppressionActiviteAutre) {
        this.suppressionActiviteAutre = suppressionActiviteAutre;
    }

    /**
     * Get le effectif salarie vrp.
     *
     * @return the effectifSalarieVrp
     */
    public Integer getEffectifSalarieVrp() {
        return effectifSalarieVrp;
    }

    /**
     * Set le effectif salarie vrp.
     *
     * @param effectifSalarieVrp
     *            the effectifSalarieVrp to set
     */
    public void setEffectifSalarieVrp(Integer effectifSalarieVrp) {
        this.effectifSalarieVrp = effectifSalarieVrp;
    }

    /**
     * @return the effectifsalariePresence
     */
    public String getEffectifsalariePresence() {
        return effectifsalariePresence;
    }

    /**
     * @param effectifsalariePresence
     *            the effectifsalariePresence to set
     */
    public void setEffectifsalariePresence(String effectifsalariePresence) {
        this.effectifsalariePresence = effectifsalariePresence;
    }

    /**
     * @return the aqpaSalarieQualifie
     */
    public String getAqpaSalarieQualifie() {
        return aqpaSalarieQualifie;
    }

    /**
     * @param aqpaSalarieQualifie
     *            the aqpaSalarieQualifie to set
     */
    public void setAqpaSalarieQualifie(final String aqpaSalarieQualifie) {
        this.aqpaSalarieQualifie = aqpaSalarieQualifie;
    }

    /**
     * @return the effectifsalariePresence
     */
    public String getPeriodeActiviteSaisonniere1DateDebut() {
        return periodeActiviteSaisonniere1DateDebut;
    }

    /**
     * @param periodeActiviteSaisonniere1DateDebut
     *            the periodeActiviteSaisonniere1DateDebut to set
     */
    public void setPeriodeActiviteSaisonniere1DateDebut(String periodeActiviteSaisonniere1DateDebut) {
        this.periodeActiviteSaisonniere1DateDebut = periodeActiviteSaisonniere1DateDebut;
    }

    /**
     * @return the periodeActiviteSaisonniere1DateFin
     */
    public String getPeriodeActiviteSaisonniere1DateFin() {
        return periodeActiviteSaisonniere1DateFin;
    }

    /**
     * @param periodeActiviteSaisonniere2DateDebut
     *            the periodeActiviteSaisonniere2DateDebut to set
     */

    public void setPeriodeActiviteSaisonniere1DateFin(String periodeActiviteSaisonniere1DateFin) {
        this.periodeActiviteSaisonniere1DateFin = periodeActiviteSaisonniere1DateFin;
    }

    /**
     * @return the periodeActiviteSaisonniere2DateDebut
     */
    public String getPeriodeActiviteSaisonniere2DateDebut() {
        return periodeActiviteSaisonniere2DateDebut;
    }

    /**
     * @param periodeActiviteSaisonniere2DateDebut
     *            the periodeActiviteSaisonniere2DateDebut to set
     */
    public void setPeriodeActiviteSaisonniere2DateDebut(String periodeActiviteSaisonniere2DateDebut) {
        this.periodeActiviteSaisonniere2DateDebut = periodeActiviteSaisonniere2DateDebut;
    }

    /**
     * @return the periodeActiviteSaisonniere2DateFin
     */
    public String getPeriodeActiviteSaisonniere2DateFin() {
        return periodeActiviteSaisonniere2DateFin;
    }

    /**
     * @param periodeActiviteSaisonniere2DateFin
     *            the periodeActiviteSaisonniere2DateFin to set
     */
    public void setPeriodeActiviteSaisonniere2DateFin(String periodeActiviteSaisonniere2DateFin) {
        this.periodeActiviteSaisonniere2DateFin = periodeActiviteSaisonniere2DateFin;
    }

    /**
     * Accesseur sur l'attribut {@link #activitesExerceesAgricole}.
     *
     * @return String activitesExerceesAgricole
     */
    public String getActivitesExerceesAgricole() {
        return activitesExerceesAgricole;
    }

    /**
     * Mutateur sur l'attribut {@link #activitesExerceesAgricole}.
     *
     * @param activitesExerceesAgricole
     *            la nouvelle valeur de l'attribut activitesExerceesAgricole
     */
    public void setActivitesExerceesAgricole(String activitesExerceesAgricole) {
        this.activitesExerceesAgricole = activitesExerceesAgricole;
    }

    /**
     * Accesseur sur l'attribut {@link #secteursActivites}.
     *
     * @return String secteursActivites
     */
    public String getSecteursActivites() {
        return secteursActivites;
    }

    /**
     * Mutateur sur l'attribut {@link #secteursActivites}.
     *
     * @param secteursActivites
     *            la nouvelle valeur de l'attribut secteursActivites
     */
    public void setSecteursActivites(final String secteursActivites) {
        this.secteursActivites = secteursActivites;
    }

    /**
     * Accesseur sur l'attribut {@link #transfertMemeDepartement}.
     *
     * @return String transfertMemeDepartement
     */
    public String getTransfertMemeDepartement() {
        return transfertMemeDepartement;
    }

    /**
     * Mutateur sur l'attribut {@link #transfertMemeDepartement}.
     *
     * @param transfertMemeDepartement
     *            la nouvelle valeur de l'attribut transfertMemeDepartement
     */
    public void setTransfertMemeDepartement(String transfertMemeDepartement) {
        this.transfertMemeDepartement = transfertMemeDepartement;
    }

}
