package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Interface ISS.
 */
@ResourceXPath("/ISS")
public interface ISS {

  /**
   * Get le A10.
   *
   * @return le A10
   */
  @FieldXPath("A10")
  IA10[] getA10();

  /**
   * Ajoute le to A10.
   *
   * @return le i A10
   */
  IA10 addToA10();

  /**
   * Set le.
   *
   * @param A10
   *          le A10
   */
  void set(IA10[] A10);

  /**
   * Get le A11.
   *
   * @return le A11
   */
  @FieldXPath("A11")
  IA11[] getA11();

  /**
   * Ajoute le to A10.
   *
   * @return le i A10
   */
  IA11 addToA11();

  /**
   * Set le.
   *
   * @param A11
   *          le A11
   */
  void set(IA11[] A11);

  /**
   * Get le A12.
   *
   * @return le A12
   */
  @FieldXPath("A12")
  IA12[] getA12();

  /**
   * Ajoute le to A12.
   *
   * @return le i A12
   */
  IA12 addToA12();

  /**
   * Set le.
   *
   * @param A12
   *          le A12
   */
  void set(IA12[] A12);

}
