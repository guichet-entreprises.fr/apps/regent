package fr.ge.common.regent.bean.formalite.profil;

import java.util.ArrayList;
import java.util.List;

import fr.ge.common.regent.bean.formalite.FormaliteVueBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.PostalCommuneBean;
import fr.ge.common.regent.bean.formalite.profil.creation.CfeBean;

/**
 * Classe Absrtaite pour regrouper les champs à enregistrer dans la table
 * Formalité XML pour tous les profils (regul : A3,A2, modification A1 et A4).
 * 
 * @author hhichri
 */
public abstract class AbstractProfilEntreprise extends FormaliteVueBean {

    /** serialVersionUID *. */
    private static final long serialVersionUID = 3808487604138484845L;

    /** Le cfe. */
    /* CFE */
    private CfeBean cfe = new CfeBean();

    /** Le code departement. */
    /* Le code département */
    private String codeDepartement;

    /** Le nom dossier. */
    /* Le nom du dossier */
    private String nomDossier;

    /** Le activite principale code activite. */
    /* Activite Principale Code Activite */
    private String activitePrincipaleCodeActivite;

    /** Le activite principale libelle. */
    /* Activite Principale Libelle */
    private String activitePrincipaleLibelle;

    /** Le activite secondaire code activite. */
    /* activiteSecondaireCodeActivite */
    private String activiteSecondaireCodeActivite;

    /** Le postal commune. */
    /* postal Commune */
    private PostalCommuneBean postalCommune = new PostalCommuneBean();

    /** Le secteur cfe. */
    /* Sercteur CFE */
    private String secteurCfe;

    /** Le forme juridique. */
    private String formeJuridique;

    /** Le activite agent commercial. */
    private String activiteAgentCommercial;

    /** Le aqpa. */
    private boolean aqpa;

    /** Evenements. */
    private List<String> evenement = new ArrayList<String>();

    /**
     * Get le cfe.
     * 
     * @return the cfe
     */
    public CfeBean getCfe() {
        return this.cfe;
    }

    /**
     * Set le cfe.
     * 
     * @param cfe
     *            the cfe to set
     */
    public void setCfe(CfeBean cfe) {
        this.cfe = cfe;
    }

    /**
     * Get le code departement.
     * 
     * @return the codeDepartement
     */
    public String getCodeDepartement() {
        return this.codeDepartement;
    }

    /**
     * Set le code departement.
     * 
     * @param codeDepartement
     *            the codeDepartement to set
     */
    public void setCodeDepartement(String codeDepartement) {
        this.codeDepartement = codeDepartement;
    }

    /**
     * Get le nom dossier.
     * 
     * @return the nomDossier
     */
    public String getNomDossier() {
        return this.nomDossier;
    }

    /**
     * Set le nom dossier.
     * 
     * @param nomDossier
     *            the nomDossier to set
     */
    public void setNomDossier(String nomDossier) {
        this.nomDossier = nomDossier;
    }

    /**
     * Get le activite principale code activite.
     * 
     * @return the activitePrincipaleCodeActivite
     */
    public String getActivitePrincipaleCodeActivite() {
        return this.activitePrincipaleCodeActivite;
    }

    /**
     * Set le activite principale code activite.
     * 
     * @param activitePrincipaleCodeActivite
     *            the activitePrincipaleCodeActivite to set
     */
    public void setActivitePrincipaleCodeActivite(String activitePrincipaleCodeActivite) {
        this.activitePrincipaleCodeActivite = activitePrincipaleCodeActivite;
    }

    /**
     * Get le activite principale libelle.
     * 
     * @return the activitePrincipaleLibelle
     */
    public String getActivitePrincipaleLibelle() {
        return this.activitePrincipaleLibelle;
    }

    /**
     * Set le activite principale libelle.
     * 
     * @param activitePrincipaleLibelle
     *            the activitePrincipaleLibelle to set
     */
    public void setActivitePrincipaleLibelle(String activitePrincipaleLibelle) {
        this.activitePrincipaleLibelle = activitePrincipaleLibelle;
    }

    /**
     * Get le activite secondaire code activite.
     * 
     * @return the activiteSecondaireCodeActivite
     */
    public String getActiviteSecondaireCodeActivite() {
        return this.activiteSecondaireCodeActivite;
    }

    /**
     * Set le activite secondaire code activite.
     * 
     * @param activiteSecondaireCodeActivite
     *            the activiteSecondaireCodeActivite to set
     */
    public void setActiviteSecondaireCodeActivite(String activiteSecondaireCodeActivite) {
        this.activiteSecondaireCodeActivite = activiteSecondaireCodeActivite;
    }

    /**
     * Get le postal commune.
     * 
     * @return the postalCommune
     */
    public PostalCommuneBean getPostalCommune() {
        return this.postalCommune;
    }

    /**
     * Set le postal commune.
     * 
     * @param postalCommune
     *            the postalCommune to set
     */
    public void setPostalCommune(PostalCommuneBean postalCommune) {
        this.postalCommune = postalCommune;
    }

    /**
     * Get le secteur cfe.
     * 
     * @return the secteurCfe
     */
    public String getSecteurCfe() {
        return this.secteurCfe;
    }

    /**
     * Set le secteur cfe.
     * 
     * @param secteurCfe
     *            the secteurCfe to set
     */
    public void setSecteurCfe(String secteurCfe) {
        this.secteurCfe = secteurCfe;
    }

    /**
     * Getter de l'attribut formeJuridique.
     * 
     * @return la valeur de formeJuridique
     */
    public String getFormeJuridique() {
        return this.formeJuridique;
    }

    /**
     * Setter de l'attribut formeJuridique.
     * 
     * @param formeJuridique
     *            la nouvelle valeur de formeJuridique
     */
    public void setFormeJuridique(String formeJuridique) {
        this.formeJuridique = formeJuridique;
    }

    /**
     * Getter de l'attribut activiteAgentCommercial.
     * 
     * @return la valeur de activiteAgentCommercial
     */
    public String getActiviteAgentCommercial() {
        return this.activiteAgentCommercial;
    }

    /**
     * Setter de l'attribut activiteAgentCommercial.
     * 
     * @param activiteAgentCommercial
     *            la nouvelle valeur de activiteAgentCommercial
     */
    public void setActiviteAgentCommercial(String activiteAgentCommercial) {
        this.activiteAgentCommercial = activiteAgentCommercial;
    }

    /**
     * @return the aqpa
     */
    public boolean isAqpa() {
        return aqpa;
    }

    /**
     * @param aqpa
     *            the aqpa to set
     */
    public void setAqpa(boolean aqpa) {
        this.aqpa = aqpa;
    }

    /**
     * Getter de l'attribut evenement.
     * 
     * @return la valeur de evenement
     */
    public List<String> getEvenement() {
        return evenement;
    }

    /**
     * Setter de l'attribut evenement.
     * 
     * @param evenement
     *            la nouvelle valeur de evenement
     */
    public void setEvenement(List<String> evenement) {
        this.evenement = evenement;
    }

}
