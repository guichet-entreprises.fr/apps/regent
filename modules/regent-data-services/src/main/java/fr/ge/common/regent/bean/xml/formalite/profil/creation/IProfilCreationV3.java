/**
 * 
 */
package fr.ge.common.regent.bean.xml.formalite.profil.creation;

/**
 * Interface IProfilCreationV3.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
public interface IProfilCreationV3 extends IProfilCreationV2 {

  /** La constante MODEL_VERSION 3. */
  int MODEL_VERSION = 3;
}
