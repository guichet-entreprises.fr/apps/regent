package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface GCS.
 */
@ResourceXPath("/GCS")
public interface GCS {

  /**
   * Get le A30.
   *
   * @return le A30
   */
  @FieldXPath("JES")
  IJES[] getJES();

  /**
   * Ajoute le to JES.
   *
   * @return le JES
   */
  IJES addToJES();

  /**
   * Set le JES.
   *
   * @param JES
   *          le nouveau JES
   */
  void setJES(IJES[] JES);

  /**
   * Get le cas.
   *
   * @return le cas
   */
  @FieldXPath("CAS")
  CAS[] getCAS();

  /**
   * Ajoute le to cas.
   *
   * @return le cas
   */
  CAS addToCAS();

  /**
   * Set le cas.
   *
   * @param CAS
   *          le nouveau cas
   */
  void setCAS(CAS[] CAS);

  /**
   * Get le scs.
   *
   * @return le scs
   */
  @FieldXPath("SCS")
  SCS[] getSCS();

  /**
   * Ajoute le to scs.
   *
   * @return le scs
   */
  SCS addToSCS();

  /**
   * Set le scs.
   *
   * @param SCS
   *          le nouveau scs
   */
  void setSCS(SCS[] SCS);

  /**
   * Get le ISS.
   *
   * @return le ISS
   */
  @FieldXPath("ISS")
  ISS[] getISS();

  /**
   * Ajoute le to ISS.
   *
   * @return le ISS
   */
  ISS addToISS();

  /**
   * Set le ISS.
   *
   * @param ISS
   *          le nouveau ISS
   */
  void setISS(ISS[] ISS);

  /**
   * Get le ISS.
   *
   * @return le ISS
   */
  @FieldXPath("SNS")
  ISNS[] getSNS();

  /**
   * Ajoute le to ISS.
   *
   * @return le ISS
   */
  ISNS addToSNS();

  /**
   * Set le SNS.
   *
   * @param SNS
   *          le nouveau SNS
   */
  void setSNS(ISNS[] SNS);

  /********************************* ADS ************************************/
  /**
   * Get le ADS.
   *
   * @return le ADS
   */
  @FieldXPath("ADS")
  ADS[] getADS();

  /**
   * Ajoute le to ADS.
   *
   * @return le ADS
   */
  ADS addToADS();

  /**
   * Set le ADS.
   *
   * @param ADS
   *          le nouveau ADS
   */
  void setADS(ADS[] ADS);

  /********************************* SAS ************************************/
  /**
   * Get le SAS.
   *
   * @return le SAS
   */
  @FieldXPath("SAS")
  SAS[] getSAS();

  /**
   * Ajoute le to SAS.
   *
   * @return le SAS
   */
  SAS addToSAS();

  /**
   * Set le SAS.
   *
   * @param SAS
   *          le nouveau SAS
   */
  void setSAS(SAS[] SAS);

  /********************************* MSS ************************************/
  /**
   * Get le MSS.
   *
   * @return le MSS
   */
  @FieldXPath("MSS")
  MSS[] getMSS();

  /**
   * Ajoute le to MSS.
   *
   * @return le MSS
   */
  MSS addToMSS();

  /**
   * Set le MSS.
   *
   * @param MSS
   *          le nouveau MSS
   */
  void setMSS(MSS[] MSS);

}
