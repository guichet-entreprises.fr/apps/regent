package fr.ge.common.regent.bean.formalite.dialogue.creation;

/**
 * Le Class PostalCommuneBean.
 */
public class PostalCommuneBean implements IFormaliteVue {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** Le code postal. */
    private String codePostal;

    /** Le commune. */
    private String commune;

    /**
     * Getter de l'attribut codePostal.
     * 
     * @return la valeur de codePostal
     */
    public String getCodePostal() {
        return this.codePostal;
    }

    /**
     * Setter de l'attribut codePostal.
     * 
     * @param codePostal
     *            la nouvelle valeur de codePostal
     */
    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    /**
     * Getter de l'attribut commune.
     * 
     * @return la valeur de commune
     */
    public String getCommune() {
        return this.commune;
    }

    /**
     * Setter de l'attribut commune.
     * 
     * @param commune
     *            la nouvelle valeur de commune
     */
    public void setCommune(String commune) {
        this.commune = commune;
    }

}
