/**
 * 
 */
package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Interface R01.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
@ResourceXPath("/R01")
public interface R01 {

  /**
   * Accesseur sur l'attribut {@link #r011}.
   *
   * @return r011
   */
  @FieldXPath("R01.1")
  String getR011();

  /**
   * Mutateur sur l'attribut {@link #r011}.
   *
   * @param R011
   *          la nouvelle valeur de l'attribut r011
   */
  void setR011(String R011);

  /**
   * Accesseur sur l'attribut {@link #r012}.
   *
   * @return r012
   */
  @FieldXPath("R01.2")
  String getR012();

  /**
   * Mutateur sur l'attribut {@link #r012}.
   *
   * @param R012
   *          la nouvelle valeur de l'attribut r012
   */
  void setR012(String R012);

}
