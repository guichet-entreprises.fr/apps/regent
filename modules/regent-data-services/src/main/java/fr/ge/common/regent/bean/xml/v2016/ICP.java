package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface ICP.
 */
@ResourceXPath("/ICP")
public interface ICP {

  /**
   * Get le p01.
   *
   * @return le p01
   */
  @FieldXPath("P01")
  IP01[] getP01();

  /**
   * Ajoute le to p01.
   *
   * @return le i p01
   */
  IP01 addToP01();

  /**
   * Set le p01.
   *
   * @param P01
   *          le nouveau p01
   */
  void setP01(IP01[] P01);

  /**
   * Get le p02.
   *
   * @return le p02
   */
  @FieldXPath("P02")
  IP02[] getP02();

  /**
   * Ajoute le to p02.
   *
   * @return le i p02
   */
  IP02 addToP02();

  /**
   * Set le p02.
   *
   * @param P02
   *          le nouveau p02
   */
  void setP02(IP02[] P02);

  /**
   * Get le p03.
   *
   * @return le p03
   */
  @FieldXPath("P03")
  IP03[] getP03();

  /**
   * Ajoute le to p03.
   *
   * @return le i p03
   */
  IP03 addToP03();

  /**
   * Set le p03.
   *
   * @param P03
   *          le nouveau p03
   */
  void setP03(IP03[] P03);

  /**
   * Get le p04.
   *
   * @return le p04
   */
  @FieldXPath("P04")
  IP04[] getP04();

  /**
   * Ajoute le to p04.
   *
   * @return le i p04
   */
  IP04 addToP04();

  /**
   * Set le p04.
   *
   * @param P04
   *          le nouveau p04
   */
  void setP04(IP04[] P04);

  /**
   * Get le P95.
   *
   * @return le P95
   */
  @FieldXPath("P95")
  IP95[] getP95();

  /**
   * Ajoute le to P95.
   *
   * @return le i P95
   */
  IP95 addToP95();

  /**
   * Set le P95.
   *
   * @param P95
   *          le nouveau P95
   */
  void setP95(IP95[] P95);

  /**
   * Get le p05.
   *
   * @return le p05
   */
  @FieldXPath("P05")
  String getP05();

  /**
   * Set le p05.
   *
   * @param P05
   *          le nouveau p05
   */
  void setP05(String P05);

  /**
   * Get le P10.
   *
   * @return le P10
   */
  @FieldXPath("P10")
  String getP10();

  /**
   * Set le P10.
   *
   * @param P10
   *          le nouveau P10
   */
  void setP10(String P10);

  /**
   * Get le p06.
   *
   * @return le p06
   */
  @FieldXPath("P06")
  String getP06();

  /**
   * Set le p06.
   *
   * @param P06
   *          le nouveau p06
   */
  void setP06(String P06);

  /**
   * Get le P07.
   *
   * @return le P07
   */
  @FieldXPath("P07")
  String getP07();

  /**
   * Set le P07.
   *
   * @param P07
   *          le nouveau P07
   */
  void setP07(String P07);

}
