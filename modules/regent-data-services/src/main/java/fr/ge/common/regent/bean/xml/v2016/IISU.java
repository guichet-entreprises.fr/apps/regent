package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Interface IISU.
 */
@ResourceXPath("/ISU")
public interface IISU {
  /**
   * Get le u41.
   *
   * @return le u41
   */
  @FieldXPath("U41")
  String getU41();

  /**
   * Set le u41.
   *
   * @param U41
   *          le nouveau u41
   */
  void setU41(String U41);
}
