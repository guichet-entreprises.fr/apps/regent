package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface EtablissementFerme.
 */
@ResourceXPath("/EtablissementFerme")
public interface EtablissementFerme {

  /**
   * Get le iae.
   *
   * @return le iae
   */
  @FieldXPath("IAE")
  IAE[] getIAE();

  /**
   * Ajoute le to iae.
   *
   * @return le iae
   */
  IAE addToIAE();

  /**
   * Set le iae.
   *
   * @param IAE
   *          le nouveau iae
   */
  void setIAE(IAE[] IAE);

  /**
   * Get le dee.
   *
   * @return le dee
   */
  @FieldXPath("DEE")
  DEE[] getDEE();

  /**
   * Ajoute le to dee.
   *
   * @return le dee
   */
  DEE addToDEE();

  /**
   * Set le dee.
   *
   * @param DEE
   *          le nouveau dee
   */
  void setDEE(DEE[] DEE);
}
