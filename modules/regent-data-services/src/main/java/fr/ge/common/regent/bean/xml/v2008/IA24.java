package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface A24.
 */
@ResourceXPath("/A24")
public interface IA24 {

  /**
   * Get le A242.
   *
   * @return le A242
   */
  @FieldXPath("A24.2")
  String getA242();

  /**
   * Set le A242.
   *
   * @param A242
   *          le nouveau A242
   */
  void setA242(String A242);

  /**
   * Get le A243.
   *
   * @return le A243
   */
  @FieldXPath("A24.3")
  String getA243();

  /**
   * Set le A243.
   *
   * @param A243
   *          le nouveau A243
   */
  void setA243(String A243);

  /**
   * Get le A245.
   *
   * @return le A245
   */
  @FieldXPath("A24.5")
  String getA245();

  /**
   * Set le A245.
   *
   * @param A245
   *          le nouveau A245
   */
  void setA245(String A245);

  /**
   * Get le A246.
   *
   * @return le A246
   */
  @FieldXPath("A24.6")
  String getA246();

  /**
   * Set le A246.
   *
   * @param A246
   *          le nouveau A246
   */
  void setA246(String A246);
}
