package fr.ge.common.regent.bean.formalite.dialogue.regularisation;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import fr.ge.common.regent.bean.formalite.dialogue.AbstractEtablissementBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.AdresseBean;

/**
 * Le Class EtablissementBean.
 */
public class EtablissementBean extends AbstractEtablissementBean<ActiviteSoumiseQualificationBean> implements IFormaliteRegularisationVue {

    /** serialVersionUID. **/
    private static final long serialVersionUID = 8184992583521425834L;

    /** Le modif date adresse1. */
    private String modifDateAdresse1;

    /** Le modif date adresse. */
    private String modifDateAdresse;

    /** Le modif ancienne adresse1. */
    private AdresseBean modifAncienneAdresse1 = new AdresseBean();

    /** Le modif date activite. */
    private String modifDateActivite;

    /** Le modif ancienne activite. */
    private String modifAncienneActivite;

    /**
     * Get le modif date adresse1.
     *
     * @return the modifDateAdresse1
     */
    public String getModifDateAdresse1() {
        return this.modifDateAdresse1;
    }

    /**
     * Set le modif date adresse1.
     *
     * @param modifDateAdresse1
     *            the modifDateAdresse1 to set
     */
    public void setModifDateAdresse1(String modifDateAdresse1) {
        this.modifDateAdresse1 = modifDateAdresse1;
    }

    /**
     * Get le modif ancienne adresse1.
     *
     * @return the modifAncienneAdresse1
     */
    public AdresseBean getModifAncienneAdresse1() {
        return this.modifAncienneAdresse1;
    }

    /**
     * Set le modif ancienne adresse1.
     *
     * @param modifAncienneAdresse1
     *            the modifAncienneAdresse1 to set
     */
    public void setModifAncienneAdresse1(AdresseBean modifAncienneAdresse1) {
        this.modifAncienneAdresse1 = modifAncienneAdresse1;
    }

    /**
     * Get le modif date activite.
     *
     * @return the modifDateActivite
     */
    public String getModifDateActivite() {
        return this.modifDateActivite;
    }

    /**
     * Set le modif date activite.
     *
     * @param modifDateActivite
     *            the modifDateActivite to set
     */
    public void setModifDateActivite(String modifDateActivite) {
        this.modifDateActivite = modifDateActivite;
    }

    /**
     * Get le modif ancienne activite.
     *
     * @return the modifAncienneActivite
     */
    public String getModifAncienneActivite() {
        return this.modifAncienneActivite;
    }

    /**
     * Set le modif ancienne activite.
     *
     * @param modifAncienneActivite
     *            the modifAncienneActivite to set
     */
    public void setModifAncienneActivite(String modifAncienneActivite) {
        this.modifAncienneActivite = modifAncienneActivite;
    }

    /**
     * Get le modif date adresse.
     *
     * @return the modifDateAdresse
     */
    public String getModifDateAdresse() {
        return modifDateAdresse;
    }

    /**
     * Set le modif date adresse.
     *
     * @param modifDateAdresse
     *            the modifDateAdresse to set
     */
    public void setModifDateAdresse(String modifDateAdresse) {
        this.modifDateAdresse = modifDateAdresse;
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see fr.guichetentreprises.vue.AbstractEtablissementBean#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
