package fr.ge.common.regent.service;

/**
 * Le Interface ServiceGenerationXmlRegent.
 */
public interface ServiceGenerationXmlRegent {

  /** Le regent emetteur. */
  String REGENT_EMETTEUR = "Z1611";

  /** Le regent file encoding. */
  String REGENT_FILE_ENCODING = "UTF-8";

  /** Le regent file method. */
  String REGENT_FILE_METHOD = "xml";

  /** Le regent file omit xml declaration. */
  String REGENT_FILE_OMIT_XML_DECLARATION = "no";

  /** Le regent file indent. */
  String REGENT_FILE_INDENT = "yes";

  /** C05 Agent Commercial. */
  String C05AGENTCOMMERCIAL = "R";

}
