package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;
import org.xmlfield.core.types.XmlString;

/**
 * Le Interface OFU.
 */
@ResourceXPath("/OFU")
public interface OFU {
  
  /**
   * Get le U70.
   *
   * @return le U70
   */
  @FieldXPath("U70")
  XmlString[] getU70();

  /**
   * Ajoute le to U70.
   *
   * @return le xml string
   */
  XmlString addToU70();

  /**
   * Set le U70.
   *
   * @param U70
   *          le nouveau U70
   */
  void setU70(XmlString[] U70);
  
  /**
   * Get le U71.
   *
   * @return le U71
   */
  @FieldXPath("U71")
  XmlString[] getU71();

  /**
   * Ajoute le to U71.
   *
   * @return le xml string
   */
  XmlString addToU71();

  /**
   * Set le U71.
   *
   * @param U71
   *          le nouveau U71
   */
  void setU71(XmlString[] U71);

  /**
   * Get le U74.
   *
   * @return le U74
   */
  @FieldXPath("U74")
  String getU74();

  /**
   * Set le U74.
   *
   * @param U74
   *          le nouveau U74
   */
  void setU74(String U74);

  /**
   * Get le IU72.
   *
   * @return le IU72
   */
  @FieldXPath("U72")
  IU72[] getU72();

  /**
   * Ajoute le to IU72.
   *
   * @return le IU72
   */
  IU72 addToU72();

  /**
   * Set le IU72.
   *
   * @param IU72
   *          le nouveau IU72
   */
  void setU72(IU72[] IU72);

}
