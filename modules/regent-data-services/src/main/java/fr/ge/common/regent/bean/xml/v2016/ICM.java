package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * L'interface ICM.
 */
@ResourceXPath("/ICM")
public interface ICM {

  /**
   * Get le M01.
   *
   * @return le M01
   */
  @FieldXPath("M01")
  IM01[] getM01();

  /**
   * Ajoute le to M01.
   *
   * @return le M01
   */
  IM01 addToM01();

  /**
   * Set le m01.
   *
   * @param m01
   *          le m01
   */
  void set(IM01[] m01);

  /**
   * Get le m02.
   *
   * @return le m02
   */
  @FieldXPath("M02")
  String getM02();

  /**
   * Set le m02.
   *
   * @param m02
   *          le nouveau m02
   */
  void setM02(String m02);

}
