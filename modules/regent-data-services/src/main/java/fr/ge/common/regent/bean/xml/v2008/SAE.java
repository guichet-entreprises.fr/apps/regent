package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface SAE.
 */
@ResourceXPath("SAE")
public interface SAE {

  /**
   * Get le e86.
   *
   * @return le e86
   */
  @FieldXPath("E86")
  String getE86();

  /**
   * Set le e86.
   *
   * @param E86
   *          le nouveau e86
   */
  void setE86(String E86);

  /**
   * Get le E84.
   *
   * @return le E84
   */
  @FieldXPath("E84")
  String getE84();

  /**
   * Set le E84.
   *
   * @param E84
   *          le nouveau E84
   */
  void setE84(String E84);

  /**
   * Get le E83.
   *
   * @return le E83
   */
  @FieldXPath("E83")
  IE83[] getE83();

  /**
   * Ajoute le to E83.
   *
   * @return le E83
   */
  IE83 addToE83();

  /**
   * Set le E83.
   *
   * @param E83
   *          le nouveau E83
   */
  void setE83(IE83[] E83);

  /**
   * Get le E85.
   *
   * @return le E85
   */
  @FieldXPath("E85")
  IE85[] getE85();

  /**
   * Ajoute le to E85.
   *
   * @return le E85
   */
  IE85 addToE85();

  /**
   * Set le E85.
   *
   * @param E85
   *          le nouveau E85
   */
  void setE85(IE85[] E85);
}
