package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Interface IP95.
 */
@ResourceXPath("/E85")
public interface IE85 {

  /**
   * Get le E85.8.
   *
   * @return le E85.8
   */
  @FieldXPath("E85.8")
  String getE858();

  /**
   * Set le E85.8.
   *
   * @param E858
   *          .8 .1 .1 le nouveau E85.8
   */
  void setE858(String E858);

}
