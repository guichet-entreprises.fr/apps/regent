package fr.ge.common.regent.bean.formalite.dialogue;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import fr.ge.common.regent.bean.formalite.dialogue.creation.AdresseBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.PostalCommuneBean;

/**
 * Le Class AbstractDirigeantBean.
 *
 * @author roveda
 * @param <T>
 *            le type generique
 */
public abstract class AbstractDirigeantBean<T extends AbstractConjointBean> extends AbstractDirigeantCommunBean implements IConjointFactory<T> {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -7854891332644736396L;

    /** Le pp pseudonyme. */
    private String ppPseudonyme;

    /** Le pp nationalite. */
    private String ppNationalite;

    /** Le pp civilite. */
    private String ppCivilite;

    /** Le pp mineur emancipe. */
    private String ppMineurEmancipe;

    /** Le pp adresse forain. */
    private PostalCommuneBean ppAdresseForain = new PostalCommuneBean();

    /** Le pp adresse ambulant. */
    private PostalCommuneBean ppAdresseAmbulant = new PostalCommuneBean();

    /** Le pp adresse. */
    private AdresseBean ppAdresse = new AdresseBean();

    /** Le conjoint presence. */
    private String conjointPresence;

    /** Le conjoint regime. */
    private String conjointRegime;

    /** Le conjoint statut. */
    private String conjointStatut;

    /** Le conjoint. */
    private T conjoint;

    /**
     * Instancie un nouveau abstract dirigeant bean.
     */
    public AbstractDirigeantBean() {
        super();
    }

    /**
     * Get le conjoint.
     *
     * @return the conjoint
     */
    public T getConjoint() {
        return conjoint;
    }

    /**
     * Set le conjoint.
     *
     * @param conjoint
     *            the conjoint to set
     */
    public void setConjoint(T conjoint) {
        this.conjoint = conjoint;
    }

    /**
     * Get le pp pseudonyme.
     *
     * @return the ppPseudonyme
     */
    public String getPpPseudonyme() {
        return ppPseudonyme;
    }

    /**
     * Set le pp pseudonyme.
     *
     * @param ppPseudonyme
     *            the ppPseudonyme to set
     */
    public void setPpPseudonyme(String ppPseudonyme) {
        this.ppPseudonyme = ppPseudonyme;
    }

    /**
     * Get le pp nationalite.
     *
     * @return the ppNationalite
     */
    public String getPpNationalite() {
        return ppNationalite;
    }

    /**
     * Set le pp nationalite.
     *
     * @param ppNationalite
     *            the ppNationalite to set
     */
    public void setPpNationalite(String ppNationalite) {
        this.ppNationalite = ppNationalite;
    }

    /**
     * Get le pp civilite.
     *
     * @return the ppCivilite
     */
    public String getPpCivilite() {
        return ppCivilite;
    }

    /**
     * Set le pp civilite.
     *
     * @param ppCivilite
     *            the ppCivilite to set
     */
    public void setPpCivilite(String ppCivilite) {
        this.ppCivilite = ppCivilite;
    }

    /**
     * Get le pp mineur emancipe.
     *
     * @return the ppMineurEmancipe
     */
    public String getPpMineurEmancipe() {
        return ppMineurEmancipe;
    }

    /**
     * Set le pp mineur emancipe.
     *
     * @param ppMineurEmancipe
     *            the ppMineurEmancipe to set
     */
    public void setPpMineurEmancipe(String ppMineurEmancipe) {
        this.ppMineurEmancipe = ppMineurEmancipe;
    }

    /**
     * Get le pp adresse forain.
     *
     * @return the ppAdresseForain
     */
    public PostalCommuneBean getPpAdresseForain() {
        return ppAdresseForain;
    }

    /**
     * Set le pp adresse forain.
     *
     * @param ppAdresseForain
     *            the ppAdresseForain to set
     */
    public void setPpAdresseForain(PostalCommuneBean ppAdresseForain) {
        this.ppAdresseForain = ppAdresseForain;
    }

    /**
     * Get le pp adresse ambulant.
     *
     * @return the ppAdresseAmbulant
     */
    public PostalCommuneBean getPpAdresseAmbulant() {
        return ppAdresseAmbulant;
    }

    /**
     * Set le pp adresse ambulant.
     *
     * @param ppAdresseAmbulant
     *            the ppAdresseAmbulant to set
     */
    public void setPpAdresseAmbulant(PostalCommuneBean ppAdresseAmbulant) {
        this.ppAdresseAmbulant = ppAdresseAmbulant;
    }

    /**
     * Get le pp adresse.
     *
     * @return the ppAdresse
     */
    public AdresseBean getPpAdresse() {
        return ppAdresse;
    }

    /**
     * Set le pp adresse.
     *
     * @param ppAdresse
     *            the ppAdresse to set
     */
    public void setPpAdresse(AdresseBean ppAdresse) {
        this.ppAdresse = ppAdresse;
    }

    /**
     * Get le conjoint presence.
     *
     * @return the conjointPresence
     */
    public String getConjointPresence() {
        return conjointPresence;
    }

    /**
     * Set le conjoint presence.
     *
     * @param conjointPresence
     *            the conjointPresence to set
     */
    public void setConjointPresence(String conjointPresence) {
        this.conjointPresence = conjointPresence;
    }

    /**
     * Get le conjoint regime.
     *
     * @return the conjointRegime
     */
    public String getConjointRegime() {
        return conjointRegime;
    }

    /**
     * Set le conjoint regime.
     *
     * @param conjointRegime
     *            the conjointRegime to set
     */
    public void setConjointRegime(String conjointRegime) {
        this.conjointRegime = conjointRegime;
    }

    /**
     * Get le conjoint statut.
     *
     * @return the conjointStatut
     */
    public String getConjointStatut() {
        return conjointStatut;
    }

    /**
     * Set le conjoint statut.
     *
     * @param conjointStatut
     *            the conjointStatut to set
     */
    public void setConjointStatut(String conjointStatut) {
        this.conjointStatut = conjointStatut;
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see fr.guichetentreprises.vue.AbstractDirigeantCommunBean#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

}
