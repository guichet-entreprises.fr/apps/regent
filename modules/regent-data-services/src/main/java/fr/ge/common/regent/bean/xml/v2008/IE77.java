package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IE77.
 */
@ResourceXPath("E77")
public interface IE77 {

  /**
   * Get le e771.
   * 
   * @return le e771
   */
  @FieldXPath("E77.1")
  String getE771();

  /**
   * Set le e771.
   * 
   * @param E771
   *          le nouveau e771
   */
  void setE771(String E771);

  /**
   * Get le e772.
   * 
   * @return le e772
   */
  @FieldXPath("E77.2")
  String getE772();

  /**
   * Set le e772.
   * 
   * @param E772
   *          le nouveau e772
   */
  void setE772(String E772);

  /**
   * Get le e773.
   * 
   * @return le e773
   */
  @FieldXPath("E77.3")
  String getE773();

  /**
   * Set le e773.
   * 
   * @param E773
   *          le nouveau e773
   */
  void setE773(String E773);

}
