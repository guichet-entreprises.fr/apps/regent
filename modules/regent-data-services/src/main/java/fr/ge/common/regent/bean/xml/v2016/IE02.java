package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IE02.
 */
@ResourceXPath("/E02")
public interface IE02 {

  /**
   * Get le e023.
   *
   * @return le e023
   */
  @FieldXPath("E02.3")
  String getE023();

  /**
   * Set le e023.
   *
   * @param E023
   *          le nouveau e023
   */
  void setE023(String E023);

  /**
   * Get le e025.
   *
   * @return le e025
   */
  @FieldXPath("E02.5")
  String getE025();

  /**
   * Set le e025.
   *
   * @param E025
   *          le nouveau e025
   */
  void setE025(String E025);

  /**
   * Get le e026.
   *
   * @return le e026
   */
  @FieldXPath("E02.6")
  String getE026();

  /**
   * Set le e026.
   *
   * @param E026
   *          le nouveau e026
   */
  void setE026(String E026);

  /**
   * Get le e027.
   *
   * @return le e027
   */
  @FieldXPath("E02.7")
  String getE027();

  /**
   * Set le e027.
   *
   * @param E027
   *          le nouveau e027
   */
  void setE027(String E027);

  /**
   * Get le e028.
   *
   * @return le e028
   */
  @FieldXPath("E02.8")
  String getE028();

  /**
   * Set le e028.
   *
   * @param E028
   *          le nouveau e028
   */
  void setE028(String E028);

  /**
   * Get le e0210.
   *
   * @return le e0210
   */
  @FieldXPath("E02.10")
  String getE0210();

  /**
   * Set le e0210.
   *
   * @param E0210
   *          le nouveau e0210
   */
  void setE0210(String E0210);

  /**
   * Get le e0211.
   *
   * @return le e0211
   */
  @FieldXPath("E02.11")
  String getE0211();

  /**
   * Set le e0211.
   *
   * @param E0211
   *          le nouveau e0211
   */
  void setE0211(String E0211);

  /**
   * Get le e0212.
   *
   * @return le e0212
   */
  @FieldXPath("E02.12")
  String getE0212();

  /**
   * Set le e0212.
   *
   * @param E0212
   *          le nouveau e0212
   */
  void setE0212(String E0212);

  /**
   * Get le e0213.
   *
   * @return le e0213
   */
  @FieldXPath("E02.13")
  String getE0213();

  /**
   * Set le e0213.
   *
   * @param E0213
   *          le nouveau e0213
   */
  void setE0213(String E0213);

  /**
   * Get le e0214.
   *
   * @return le e0214
   */
  @FieldXPath("E02.14")
  String getE0214();

  /**
   * Set le e0214.
   *
   * @param E0214
   *          le nouveau e0214
   */
  void setE0214(String E0214);

}
