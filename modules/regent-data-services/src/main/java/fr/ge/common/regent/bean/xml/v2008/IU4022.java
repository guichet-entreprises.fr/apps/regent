package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

@ResourceXPath("/U40.2.2")
public interface IU4022 {
		
		
	/**
	   * Get le U40223.
	   *
	   * @return le U40223
	   */
	  @FieldXPath("U40.2.2.3")
	  String getU40223();

	  /**
	   * Set le U40223.
	   *
	   * @param C401
	   *          le nouveau U40223
	   */
	  void setU40223(String U40223);
		/**
	   * Get le 40225.
	   *
	   * @return le 40225
	   */
	  @FieldXPath("U40.2.2.5")
	  String getU40225();

	  /**
	   * Set le U40225.
	   *
	   * @param U40225
	   *          le nouveau U40225
	   */
	  void setU40225(String U40225);
		/**
	   * Get le U40226.
	   *
	   * @return le U40226
	   */
	  @FieldXPath("U40.2.2.6")
	  String getU40226();

	  /**
	   * Set le U40226.
	   *
	   * @param U40226
	   *          le nouveau U40226
	   */
	  void setU40226(String U40226);
	  
		/**
	   * Get le c401.
	   *
	   * @return le c401
	   */
	  @FieldXPath("U40.2.2.7")
	  String getU40227();

	  /**
	   * Set le U40227.
	   *
	   * @param U40227
	   *          le nouveau U40227
	   */
	  void setU40227(String U40227);
	  
		/**
	   * Get le c401.
	   *
	   * @return le c401
	   */
	  @FieldXPath("U40.2.2.8")
	  String getU40228();

	  /**
	   * Set le U40228.
	   *
	   * @param U40228
	   *          le nouveau U40228
	   */
	  void setU40228(String U40228);
	  
		/**
	   * Get le c401.
	   *
	   * @return le c401
	   */
	  @FieldXPath("U40.2.2.10")
	  String getU402210();

	  /**
	   * Set le U402210.
	   *
	   * @param U402210
	   *          le nouveau U402210
	   */
	  void setU402210(String U402210);
	  
		/**
	   * Get le c401.
	   *
	   * @return le c401
	   */
	  @FieldXPath("U40.2.2.11")
	  String getU402211();

	  /**
	   * Set le U402211.
	   *
	   * @param U402211
	   *          le nouveau U402211
	   */
	  void setU402211(String U402211);
	  
		/**
	   * Get le c401.
	   *
	   * @return le c401
	   */
	  @FieldXPath("U40.2.2.12")
	  String getU402212();

	  /**
	   * Set le U402212.
	   *
	   * @param U402212
	   *          le nouveau U402212
	   */
	  void setU402212(String U402212);
	
	  
	  
		/**
	   * Get le c401.
	   *
	   * @return le c401
	   */
	  @FieldXPath("U40.2.2.13")
	  String getU402213();

	  /**
	   * Set le U402213.
	   *
	   * @param U402213
	   *          le nouveau U402213
	   */
	  void setU402213(String U402213);
	  
	  
		/**
	   * Get le c401.
	   *
	   * @return le c401
	   */
	  @FieldXPath("U40.2.2.14")
	  String getU402214();

	  /**
	   * Set le U402214.
	   *
	   * @param U402214
	   *          le nouveau U402214
	   */
	  void setU402214(String U402214);
	

}
