package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IDF.
 */
@ResourceXPath("/IDF")
public interface IDF {

  /**
   * Get le c01.
   *
   * @return le c01
   */
  @FieldXPath("C01")
  String getC01();

  /**
   * Set le c01.
   *
   * @param CO1
   *          le nouveau c01
   */
  void setC01(String CO1);

  /**
   * Get le c02.
   *
   * @return le c02
   */
  @FieldXPath("C02")
  String getC02();

  /**
   * Set le c02.
   *
   * @param CO2
   *          le nouveau c02
   */
  void setC02(String CO2);

  /**
   * Get le c03.
   *
   * @return le c03
   */
  @FieldXPath("C03")
  String getC03();

  /**
   * Set le c03.
   *
   * @param CO3
   *          le nouveau c03
   */
  void setC03(String CO3);

  /**
   * Get le c04.
   *
   * @return le c04
   */
  @FieldXPath("C04")
  String getC04();

  /**
   * Set le c04.
   *
   * @param CO4
   *          le nouveau c04
   */
  void setC04(String CO4);

  /**
   * Get le c05.
   *
   * @return le c05
   */
  @FieldXPath("C05")
  String getC05();

  /**
   * Set le c05.
   *
   * @param CO5
   *          le nouveau c05
   */
  void setC05(String CO5);

  /**
   * Get le c06.
   *
   * @return le c06
   */
  @FieldXPath("C06")
  String getC06();

  /**
   * Set le c06.
   *
   * @param CO6
   *          le nouveau c06
   */
  void setC06(String CO6);

}
