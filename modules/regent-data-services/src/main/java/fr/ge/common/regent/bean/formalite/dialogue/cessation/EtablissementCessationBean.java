package fr.ge.common.regent.bean.formalite.dialogue.cessation;

import fr.ge.common.regent.bean.formalite.FormaliteVueBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.AdresseBean;

/**
 * Le Class EtablissementCessationBean.
 */
public class EtablissementCessationBean extends FormaliteVueBean {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 8050173819195413647L;

    /** Le id technique. */
    private String idTechnique;

    /**
     * Get le id technique.
     *
     * @return the idTechnique
     */
    public String getIdTechnique() {
        return idTechnique;
    }

    /**
     * Set le id technique.
     *
     * @param idTechnique
     *            the idTechnique to set
     */
    public void setIdTechnique(String idTechnique) {
        this.idTechnique = idTechnique;
    }

    /** Le adresse. */
    private AdresseBean adresse = new AdresseBean();

    /** Le destination destinataire etablissement . */
    private String destinationEtablissement;

    /** Le destination autre etablissement. */
    private String destinationAutre;

    /**
     * return the destinationEtablissement.
     *
     * @return le destination etablissement
     */
    public String getDestinationEtablissement() {
        return this.destinationEtablissement;
    }

    /**
     * Set le destination etablissement principal.
     *
     * @param destinationEtablissement
     *            the destinationEtablissement to set
     */
    public void setDestinationEtablissement(final String destinationEtablissement) {
        this.destinationEtablissement = destinationEtablissement;
    }

    /**
     * return the existenceAutreEtablissement.
     *
     * @return destinationAutre le nouveau destination autre1
     */

    public String getDestinationAutre() {
        return this.destinationAutre;
    }

    /**
     * 
     * Set le destination autre.
     * 
     * @param destinationAutre
     */

    public void setDestinationAutre(final String destinationAutre) {
        this.destinationAutre = destinationAutre;
    }

    /**
     * Get le adresse.
     *
     * @return the adresse
     */
    public AdresseBean getAdresse() {
        return adresse;
    }

    /**
     * Set le adresse.
     *
     * @param adresse
     *            the adresse to set
     */
    public void setAdresse(final AdresseBean adresse) {
        this.adresse = adresse;
    }
}
