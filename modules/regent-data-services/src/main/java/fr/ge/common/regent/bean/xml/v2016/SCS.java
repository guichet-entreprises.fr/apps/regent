package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface SCS.
 */
@ResourceXPath("/SCS")
public interface SCS {

  /**
   * Get le a53.
   *
   * @return le a53
   */
  @FieldXPath("A53")
  IA53[] getA53();

  /**
   * Ajoute le to a53.
   *
   * @return le i a53
   */
  IA53 addToA53();

  /**
   * Set le a53.
   *
   * @param IA53
   *          le nouveau a53
   */
  void setA53(IA53[] IA53);

  /**
   * Get le a55.
   *
   * @return le a55
   */
  @FieldXPath("A55")
  IA55[] getA55();

  /**
   * Ajoute le to a55.
   *
   * @return le i a55
   */
  IA55 addToA55();

  /**
   * Set le a55.
   *
   * @param IA55
   *          le nouveau a55
   */
  void setA55(IA55[] IA55);

  /**
   * Get le A54.
   *
   * @return le A54
   */
  @FieldXPath("A54")
  String getA54();

  /**
   * Set le A54.
   *
   * @param A54
   *          le nouveau A54
   */
  void setA54(String A54);

}
