// Profil V2 demo : NE PAS SUPPR
package fr.ge.common.regent.bean.xml.formalite.profil.creation;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Interface IProfilEntrepriseV2.
 */
@ResourceXPath("/formalite")
public interface IProfilCreationV2 extends IProfilCreation {

  /** La constante MODEL_VERSION. */
  static final int MODEL_VERSION = 2;

  /**
   * Accesseur sur l'attribut descr libre activite principale.
   *
   * @return descr libre activite principale
   */
  @FieldXPath(value = "descrLibreActivitePrincipale")
  public String getDescrLibreActivitePrincipale();

  /**
   * Mutateur sur l'attribut descr libre activite principale.
   *
   * @param descrLibreActivitePrincipale
   *          le nouveau descr libre activite principale
   */
  public void setDescrLibreActivitePrincipale(String descrLibreActivitePrincipale);

  /**
   * Accesseur sur l'attribut activite principale non trouve.
   *
   * @return activite principale non trouve
   */
  @FieldXPath(value = "activitePrincipaleNonTrouve")
  public String getActivitePrincipaleNonTrouve();

  /**
   * Mutateur sur l'attribut activite principale non trouve.
   *
   * @param activitePrincipaleNonTrouve
   *          le nouveau activite principale non trouve
   */
  public void setActivitePrincipaleNonTrouve(String activitePrincipaleNonTrouve);

  /**
   * Accesseur sur l'attribut code cfe.
   *
   * @return code cfe
   */
  @FieldXPath(value = "codeCfe")
  public String getCodeCfe();

  /**
   * Mutateur sur l'attribut code cfe.
   *
   * @param codeCfe
   *          le nouveau code cfe
   */
  public void setCodeCfe(String codeCfe);

  /**
   * Accesseur sur l'attribut code cfe2.
   *
   * @return code cfe2
   */
  @FieldXPath(value = "codeCfe2")
  public String getCodeCfe2();

  /**
   * Mutateur sur l'attribut code cfe2.
   *
   * @param codeCfe2
   *          le nouveau code cfe2
   */
  public void setCodeCfe2(String codeCfe2);

  /**
   * Accesseur sur l'attribut aqpa str.
   *
   * @return aqpa str
   */
  @FieldXPath(value = "aqpaStr")
  public String getAqpaStr();

  /**
   * Mutateur sur l'attribut aqpa str.
   *
   * @param aqpaStr
   *          le nouveau aqpa str
   */
  public void setAqpaStr(String aqpaStr);

  /**
   * Accesseur sur l'attribut descr libre activite secondaire.
   *
   * @return descr libre activite secondaire
   */
  @FieldXPath(value = "descrLibreActiviteSecondaire")
  public String getDescrLibreActiviteSecondaire();

  /**
   * Mutateur sur l'attribut descr libre activite secondaire.
   *
   * @param descrLibreActiviteSecondaire
   *          le nouveau descr libre activite secondaire
   */
  public void setDescrLibreActiviteSecondaire(String descrLibreActiviteSecondaire);

  /**
   * Accesseur sur l'attribut activite secondaire non trouve.
   *
   * @return activite secondaire non trouve
   */
  @FieldXPath(value = "activiteSecondaireNonTrouve")
  public String getActiviteSecondaireNonTrouve();

  /**
   * Mutateur sur l'attribut activite secondaire non trouve.
   *
   * @param activiteSecondaireNonTrouve
   *          le nouveau activite secondaire non trouve
   */
  public void setActiviteSecondaireNonTrouve(String activiteSecondaireNonTrouve);

  /**
   * Accesseur sur l'attribut {@link #precision activite principale}.
   *
   * @return precision activite principale
   */
  @FieldXPath(value = "precisionActivitePrincipale")
  public String getPrecisionActivitePrincipale();

  /**
   * Sets the precision activite principale.
   *
   * @return string
   */
  public void setPrecisionActivitePrincipale(String precisionActivitePrincipale);

  /**
   * Accesseur sur l'attribut {@link #precision activite secondaire}.
   *
   * @return precision activite secondaire
   */
  @FieldXPath(value = "precisionActiviteSecondaire")
  public String getPrecisionActiviteSecondaire();

  /**
   * Sets the precision activite secondaire.
   *
   * @return string
   */
  public void setPrecisionActiviteSecondaire(String precisionActiviteSecondaire);

}
// FIN Profil V2 demo : NE PAS SUPPR
