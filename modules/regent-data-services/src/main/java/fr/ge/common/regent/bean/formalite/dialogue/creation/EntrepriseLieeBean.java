package fr.ge.common.regent.bean.formalite.dialogue.creation;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import fr.ge.common.regent.bean.formalite.dialogue.AbstractEntrepriseLieeBean;

/**
 * Le Class EntrepriseLieeBean.
 */
public class EntrepriseLieeBean extends AbstractEntrepriseLieeBean implements IFormaliteVue {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 8332543591975597233L;

    /** Le numero detenteur. */
    private String numeroDetenteur;

    /** Le numero exploitation. */
    private String numeroExploitation;

    /** Le id technique. */
    private String idTechnique;

    /**
     * Get le id technique.
     * 
     * @return the idTechnique
     */
    public String getIdTechnique() {
        return this.idTechnique;
    }

    /**
     * Set le id technique.
     * 
     * @param idTechnique
     *            the idTechnique to set
     */
    public void setIdTechnique(String idTechnique) {
        this.idTechnique = idTechnique;
    }

    /**
     * Getter de l'attribut numeroDetenteur.
     * 
     * @return la valeur de numeroDetenteur
     */
    public String getNumeroDetenteur() {
        return this.numeroDetenteur;
    }

    /**
     * Getter de l'attribut numeroExploitation.
     * 
     * @return la valeur de numeroExploitation
     */
    public String getNumeroExploitation() {
        return this.numeroExploitation;
    }

    /**
     * Setter de l'attribut numeroDetenteur.
     * 
     * @param numeroDetenteur
     *            la nouvelle valeur de numeroDetenteur
     */
    public void setNumeroDetenteur(String numeroDetenteur) {
        this.numeroDetenteur = numeroDetenteur;
    }

    /**
     * Setter de l'attribut numeroExploitation.
     * 
     * @param numeroExploitation
     *            la nouvelle valeur de numeroExploitation
     */
    public void setNumeroExploitation(String numeroExploitation) {
        this.numeroExploitation = numeroExploitation;
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see fr.guichetentreprises.vue.AbstractEntrepriseLieeBean#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

}
