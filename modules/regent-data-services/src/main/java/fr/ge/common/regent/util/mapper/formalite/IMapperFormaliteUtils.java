package fr.ge.common.regent.util.mapper.formalite;

import fr.ge.common.regent.bean.formalite.FormaliteVueBean;
import fr.ge.common.regent.bean.xml.formalite.IFormalite;

/**
 * Classe utilitaire de recopie des données entre un bean XmlField et un bean
 * affichable.
 * 
 * @author jpauchet
 *
 * @param <X>
 *            le bean XmlField pour le stockage
 * @param <V>
 *            le bean affichable
 */
public interface IMapperFormaliteUtils<X extends IFormalite, V extends FormaliteVueBean> {

    /**
     * Recopie les données à partir d'un bean XmlField vers un bean affichable.
     *
     * @param iFormalite
     *            le bean XmlField
     * @param formalite
     *            le bean affichable
     */
    void mapper(X iFormalite, V formalite);

    /**
     * Recopie les données à partir d'un bean affichable vers un bean XmlField.
     *
     * @param formalite
     *            le bean affichable
     * @param iFormalite
     *            le bean XmlField
     */
    void mapper(V formalite, X iFormalite);

}
