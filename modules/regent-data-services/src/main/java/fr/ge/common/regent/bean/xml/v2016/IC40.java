package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IC40.
 */
@ResourceXPath("/C40")
public interface IC40 {

  /**
   * Get le c401.
   *
   * @return le c401
   */
  @FieldXPath("C40.1")
  String getC401();

  /**
   * Set le c401.
   *
   * @param C401
   *          le nouveau c401
   */
  void setC401(String C401);

  /**
   * Get le c402.
   *
   * @return le c402
   */
  @FieldXPath("C40.2")
  String getC402();

  /**
   * Set le c402.
   *
   * @param C402
   *          le nouveau c402
   */
  void setC402(String C402);

  /**
   * Get le c403.
   *
   * @return le c403
   */
  @FieldXPath("C40.3")
  IC403[] getC403();

  /**
   * Ajoute le to c403.
   *
   * @return le i c403
   */
  IC403 addToC403();

  /**
   * Set le c403.
   *
   * @param C403
   *          le nouveau c403
   */
  void setC403(IC403[] C403);

}
