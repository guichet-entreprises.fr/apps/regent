package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IC10.
 */
@ResourceXPath("/C10")
public interface IC10 {

  /**
   * Get le c101.
   *
   * @return le c101
   */
  @FieldXPath("C10.1")
  String getC101();

  /**
   * Set le c101.
   *
   * @param C101
   *          le nouveau c101
   */
  void setC101(String C101);

  /**
   * Get le c102.
   *
   * @return le c102
   */
  @FieldXPath("C10.2")
  String getC102();

  /**
   * Set le c102.
   *
   * @param C1O2
   *          le nouveau c102
   */
  void setC102(String C1O2);

  /**
   * Get le c103.
   *
   * @return le c103
   */
  @FieldXPath("C10.3")
  String getC103();

  /**
   * Set le c103.
   *
   * @param C1O3
   *          le nouveau c103
   */
  void setC103(String C1O3);

}
