package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface AQU.
 */
@ResourceXPath("/AQU")
public interface AQU {

  /**
   * Get le u60.
   *
   * @return le u60
   */
  @FieldXPath("U60")
  String getU60();

  /**
   * Set le u60.
   *
   * @param U60
   *          le nouveau u60
   */
  void setU60(String U60);

  /**
   * Get le u61.
   *
   * @return le u61
   */
  @FieldXPath("U61")
  String getU61();

  /**
   * Set le u61.
   *
   * @param U61
   *          le nouveau u61
   */
  void setU61(String U61);

  /**
   * Get le u62.
   *
   * @return le u62
   */
  @FieldXPath("U62")
  String getU62();

  /**
   * Set le u62.
   *
   * @param U62
   *          le nouveau u62
   */
  void setU62(String U62);
}
