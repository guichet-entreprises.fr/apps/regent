package fr.ge.common.regent.util.mapper.formalite.profil;

import java.util.ArrayList;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xmlfield.core.types.XmlString;

import fr.ge.common.regent.bean.formalite.dialogue.creation.PostalCommuneBean;
import fr.ge.common.regent.bean.formalite.profil.creation.CfeBean;
import fr.ge.common.regent.bean.formalite.profil.modification.ProfilModificationBean;
import fr.ge.common.regent.bean.xml.formalite.profil.modification.IProfilModification;
import fr.ge.common.regent.bean.xml.transverse.IPostalCommune;
import fr.ge.common.regent.util.mapper.formalite.IMapperFormaliteUtils;

/**
 * Le Class MapperProfilModificationUtils.
 */
@Component
public class MapperProfilModificationUtils implements IMapperFormaliteUtils<IProfilModification, ProfilModificationBean> {

    /** Le profil entreprise modif mapper. */
    @Autowired
    private Mapper profilEntrepriseModifMapper;

    /**
     * Mapper.
     *
     * @param iProfilEntrepriseModification
     *            le i profil entreprise modification
     * @param profilEntrepriseModificationBean
     *            le profil entreprise modification bean
     */
    public void mapper(IProfilModification iProfilEntrepriseModification, ProfilModificationBean profilEntrepriseModificationBean) {
        // Instanciation des attributs de type complexe
        profilEntrepriseModificationBean.setCfe(new CfeBean());
        profilEntrepriseModificationBean.setPostalCommune(new PostalCommuneBean());
        profilEntrepriseModificationBean.setPostalCommune1(new PostalCommuneBean());
        profilEntrepriseModificationBean.setPostalCommune2(new PostalCommuneBean());

        profilEntrepriseModificationBean.setModifEvenements2(new ArrayList<String>());

        // Mapping des champs simples
        this.getProfilEntrepriseModifMapper().map(iProfilEntrepriseModification, profilEntrepriseModificationBean);

        // Traitement des Listes (arrays)
        if (iProfilEntrepriseModification.getEvenement() != null && iProfilEntrepriseModification.getEvenement().length > 0) {
            for (XmlString evenement : iProfilEntrepriseModification.getEvenement()) {
                profilEntrepriseModificationBean.getEvenement().add(evenement.getString());
            }
        }

        if (iProfilEntrepriseModification.getModifEvenements2() != null && iProfilEntrepriseModification.getModifEvenements2().length > 0) {
            for (XmlString evenement : iProfilEntrepriseModification.getModifEvenements2()) {
                profilEntrepriseModificationBean.getModifEvenements2().add(evenement.getString());
            }
        }

        if (iProfilEntrepriseModification.getModifEvenements() != null && iProfilEntrepriseModification.getModifEvenements().length > 0) {
            for (XmlString evenement : iProfilEntrepriseModification.getModifEvenements()) {
                profilEntrepriseModificationBean.getModifEvenements().add(evenement.getString());
            }
        }
    }

    /**
     * Mapper.
     *
     * @param profilEntrepriseModificationBean
     *            le profil entreprise modification bean
     * @param iProfilEntrepriseModification
     *            le i profil entreprise modification
     */
    public void mapper(ProfilModificationBean profilEntrepriseModificationBean, IProfilModification iProfilEntrepriseModification) {
        /* Ajouter les objets */
        iProfilEntrepriseModification.newCfe();
        iProfilEntrepriseModification.newPostalCommune();
        iProfilEntrepriseModification.newPostalCommune1();
        iProfilEntrepriseModification.newPostalCommune2();

        /* Mapper les champs simples. */
        this.getProfilEntrepriseModifMapper().map(profilEntrepriseModificationBean, iProfilEntrepriseModification);

        /* GUB 327 */
        // iProfilEntrepriseModification.setSecteurCFE(profilEntrepriseModificationBean.getSecteurCfe());
        /* Fin GUB 327 */

        /* GUB 358 : postalCommune non correctement mappée */
        IPostalCommune postalCommune = iProfilEntrepriseModification.getPostalCommune();
        IPostalCommune postalCommune1 = iProfilEntrepriseModification.getPostalCommune1();
        IPostalCommune postalCommune2 = iProfilEntrepriseModification.getPostalCommune2();
        String codePostal1 = postalCommune1.getCodePostal();
        String codePostal2 = postalCommune2.getCodePostal();
        if (StringUtils.isNoneEmpty(codePostal1)) {
            postalCommune.setCodePostal(codePostal1);
            postalCommune.setCommune(postalCommune1.getCommune());
        } else if (StringUtils.isNoneEmpty(codePostal2)) {
            postalCommune.setCodePostal(codePostal2);
            postalCommune.setCommune(postalCommune2.getCommune());
        }
        /* Fin GUB 358 */

        /* Ajouter les évenemnts */
        if (!CollectionUtils.isEmpty(profilEntrepriseModificationBean.getEvenement())) {
            this.addEvenements(profilEntrepriseModificationBean, iProfilEntrepriseModification);
        }

        if (!CollectionUtils.isEmpty(profilEntrepriseModificationBean.getModifEvenements2())) {
            this.addModifEvenemnts2(profilEntrepriseModificationBean, iProfilEntrepriseModification);
        }

        if (!CollectionUtils.isEmpty(profilEntrepriseModificationBean.getModifEvenements())) {
            this.addModifEvenemnts(profilEntrepriseModificationBean, iProfilEntrepriseModification);
        }

    }

    /**
     * Ajoute le evenements.
     *
     * @param profilEntrepriseModificationBean
     *            le profil entreprise modification bean
     * @param iProfilEntrepriseModification
     *            le i profil entreprise modification
     */
    private void addEvenements(ProfilModificationBean profilEntrepriseModificationBean, IProfilModification iProfilEntrepriseModification) {
        /*
         * parcourir la liste des évenements existants sur l'interface XML et
         * les supprimer
         */
        for (XmlString evenement : iProfilEntrepriseModification.getEvenement()) {
            iProfilEntrepriseModification.removeFromEvenement(evenement);
        }
        /*
         * parcourir la liste des évenements dans le Bean pour les rajouter à
         * l'interface
         */
        for (String evenement : profilEntrepriseModificationBean.getEvenement()) {
            XmlString evenemntNew = iProfilEntrepriseModification.addToEvenement();
            evenemntNew.setString(evenement);
        }

    }

    /**
     * Ajoute le modif evenemnts2.
     *
     * @param profilEntrepriseModificationBean
     *            le profil entreprise modification bean
     * @param iProfilEntrepriseModification
     *            le i profil entreprise modification
     */
    private void addModifEvenemnts2(ProfilModificationBean profilEntrepriseModificationBean, IProfilModification iProfilEntrepriseModification) {
        /*
         * parcourir la liste des évenements existants sur l'interface XML et
         * les supprimer
         */
        for (XmlString evenement : iProfilEntrepriseModification.getModifEvenements2()) {
            iProfilEntrepriseModification.removeFromModifEvenements2(evenement);
        }
        /*
         * parcourir la liste des évenements dans le Bean pour les rajouter à
         * l'interface
         */
        for (String evenement : profilEntrepriseModificationBean.getModifEvenements2()) {
            XmlString evenemnt = iProfilEntrepriseModification.addToModifEvenements2();
            evenemnt.setString(evenement);
        }

    }

    /**
     * Ajoute le modif evenemnts.
     *
     * @param profilEntrepriseModificationBean
     *            le profil entreprise modification bean
     * @param iProfilEntrepriseModification
     *            le i profil entreprise modification
     */
    private void addModifEvenemnts(ProfilModificationBean profilEntrepriseModificationBean, IProfilModification iProfilEntrepriseModification) {
        /*
         * parcourir la liste des évenements existants sur l'interface XML et
         * les supprimer
         */
        for (XmlString evenement : iProfilEntrepriseModification.getModifEvenements()) {
            iProfilEntrepriseModification.removeFromModifEvenements(evenement);
        }
        /*
         * parcourir la liste des évenements dans le Bean pour les rajouter à
         * l'interface
         */
        for (String evenement : profilEntrepriseModificationBean.getModifEvenements()) {
            XmlString evenemnt = iProfilEntrepriseModification.addToModifEvenements();
            evenemnt.setString(evenement);
        }

    }

    /**
     * Get le profil entreprise modif mapper.
     *
     * @return the profilEntrepriseModifMapper
     */
    public Mapper getProfilEntrepriseModifMapper() {
        return profilEntrepriseModifMapper;
    }

    /**
     * Set le profil entreprise modif mapper.
     *
     * @param profilEntrepriseModifMapper
     *            the profilEntrepriseModifMapper to set
     */
    public void setProfilEntrepriseModifMapper(Mapper profilEntrepriseModifMapper) {
        this.profilEntrepriseModifMapper = profilEntrepriseModifMapper;
    }

}
