package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Interface IP74.
 */
@ResourceXPath("/P74")
public interface IP74 {
	
	 /**
	   * Get le P743.
	   *
	   * @return le P743
	   */
	  @FieldXPath("P74.3")
	  String getP743();

	  /**
	   * Set le P743.
	   *
	   * @param P743
	   *          le nouveau P743
	   */
	  void setP743(String P743);

	  /**
	   * Get le P745.
	   *
	   * @return le P745
	   */
	  @FieldXPath("P74.5")
	  String getP745();

	  /**
	   * Set le P745.
	   *
	   * @param P745
	   *          le nouveau P745
	   */
	  void setP745(String P745);

	  /**
	   * Get le P746.
	   *
	   * @return le P746
	   */
	  @FieldXPath("P74.6")
	  String getP746();

	  /**
	   * Set le P746.
	   *
	   * @param P746
	   *          le nouveau P746
	   */
	  void setP746(String P746);

	  /**
	   * Get le P747.
	   *
	   * @return le P747
	   */
	  @FieldXPath("P74.7")
	  String getP747();

	  /**
	   * Set le P747.
	   *
	   * @param P747
	   *          le nouveau P747
	   */
	  void setP747(String P747);

	  /**
	   * Get le P748.
	   *
	   * @return le P748
	   */
	  @FieldXPath("P74.8")
	  String getP748();

	  /**
	   * Set le P748.
	   *
	   * @param P748
	   *          le nouveau P748
	   */
	  void setP748(String P748);

	  /**
	   * Get le P7410.
	   *
	   * @return le P7410
	   */
	  @FieldXPath("P74.10")
	  String getP7410();

	  /**
	   * Set le P7410.
	   *
	   * @param P7410
	   *          le nouveau P7410
	   */
	  void setP7410(String P7410);

	  /**
	   * Get le P7411.
	   *
	   * @return le P7411
	   */
	  @FieldXPath("P74.11")
	  String getP7411();

	  /**
	   * Set le P7411.
	   *
	   * @param P7411
	   *          le nouveau P7411
	   */
	  void setP7411(String P7411);

	  /**
	   * Get le P7412.
	   *
	   * @return le P7412
	   */
	  @FieldXPath("P74.12")
	  String getP7412();

	  /**
	   * Set le P7412.
	   *
	   * @param P7412
	   *          le nouveau P7412
	   */
	  void setP7412(String P7412);

	  /**
	   * Get le P7413.
	   *
	   * @return le P7413
	   */
	  @FieldXPath("P74.13")
	  String getP7413();

	  /**
	   * Set le P7413.
	   *
	   * @param P7413
	   *          le nouveau P7413
	   */
	  void setP7413(String P7413);

	  /**
	   * Get le P7414.
	   *
	   * @return le P7414
	   */
	  @FieldXPath("P74.14")
	  String getP7414();

	  /**
	   * Set le P7414.
	   *
	   * @param P7414
	   *          le nouveau P7414
	   */
	  void setP7414(String P7414);


}
