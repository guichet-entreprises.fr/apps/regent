package fr.ge.common.regent.bean.formalite.dialogue;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import fr.ge.common.regent.bean.formalite.FormaliteVueBean;

/**
 * Le Class AbstractDirigeantCommunBean.
 */
public abstract class AbstractDirigeantCommunBean extends FormaliteVueBean {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -4084941752289890022L;

    /** Le pp nom naissance. */
    private String ppNomNaissance;

    /** Le pp nom usage. */
    private String ppNomUsage;

    /** Le pp prenom1. */
    private String ppPrenom1;

    /** Le pp prenom2. */
    private String ppPrenom2;

    /** Le pp prenom3. */
    private String ppPrenom3;

    /** Le pp prenom4. */
    private String ppPrenom4;

    /** Le pp date naissance. */
    private String ppDateNaissance;

    /** Le pp lieu naissance pays. */
    private String ppLieuNaissancePays;

    /** Le pp lieu naissance departement. */
    private String ppLieuNaissanceDepartement;

    /** Le pp lieu naissance commune. */
    private String ppLieuNaissanceCommune;

    /** Le pp lieu naissance ville. */
    private String ppLieuNaissanceVille;

    /**
     * Get le pp nom naissance.
     *
     * @return the ppNomNaissance
     */
    public String getPpNomNaissance() {
        return ppNomNaissance;
    }

    /**
     * Set le pp nom naissance.
     *
     * @param ppNomNaissance
     *            the ppNomNaissance to set
     */
    public void setPpNomNaissance(String ppNomNaissance) {
        this.ppNomNaissance = ppNomNaissance;
    }

    /**
     * Get le pp nom usage.
     *
     * @return the ppNomUsage
     */
    public String getPpNomUsage() {
        return ppNomUsage;
    }

    /**
     * Set le pp nom usage.
     *
     * @param ppNomUsage
     *            the ppNomUsage to set
     */
    public void setPpNomUsage(String ppNomUsage) {
        this.ppNomUsage = ppNomUsage;
    }

    /**
     * Get le pp prenom1.
     *
     * @return the ppPrenom1
     */
    public String getPpPrenom1() {
        return ppPrenom1;
    }

    /**
     * Set le pp prenom1.
     *
     * @param ppPrenom1
     *            the ppPrenom1 to set
     */
    public void setPpPrenom1(String ppPrenom1) {
        this.ppPrenom1 = ppPrenom1;
    }

    /**
     * Get le pp prenom2.
     *
     * @return the ppPrenom2
     */
    public String getPpPrenom2() {
        return ppPrenom2;
    }

    /**
     * Set le pp prenom2.
     *
     * @param ppPrenom2
     *            the ppPrenom2 to set
     */
    public void setPpPrenom2(String ppPrenom2) {
        this.ppPrenom2 = ppPrenom2;
    }

    /**
     * Get le pp prenom3.
     *
     * @return the ppPrenom3
     */
    public String getPpPrenom3() {
        return ppPrenom3;
    }

    /**
     * Set le pp prenom3.
     *
     * @param ppPrenom3
     *            the ppPrenom3 to set
     */
    public void setPpPrenom3(String ppPrenom3) {
        this.ppPrenom3 = ppPrenom3;
    }

    /**
     * Get le pp prenom4.
     *
     * @return the ppPrenom4
     */
    public String getPpPrenom4() {
        return ppPrenom4;
    }

    /**
     * Set le pp prenom4.
     *
     * @param ppPrenom4
     *            the ppPrenom4 to set
     */
    public void setPpPrenom4(String ppPrenom4) {
        this.ppPrenom4 = ppPrenom4;
    }

    /**
     * Get le pp date naissance.
     *
     * @return the ppDateNaissance
     */
    public String getPpDateNaissance() {
        return ppDateNaissance;
    }

    /**
     * Set le pp date naissance.
     *
     * @param ppDateNaissance
     *            the ppDateNaissance to set
     */
    public void setPpDateNaissance(String ppDateNaissance) {
        this.ppDateNaissance = ppDateNaissance;
    }

    /**
     * Get le pp lieu naissance pays.
     *
     * @return the ppLieuNaissancePays
     */
    public String getPpLieuNaissancePays() {
        return ppLieuNaissancePays;
    }

    /**
     * Set le pp lieu naissance pays.
     *
     * @param ppLieuNaissancePays
     *            the ppLieuNaissancePays to set
     */
    public void setPpLieuNaissancePays(String ppLieuNaissancePays) {
        this.ppLieuNaissancePays = ppLieuNaissancePays;
    }

    /**
     * Get le pp lieu naissance departement.
     *
     * @return the ppLieuNaissanceDepartement
     */
    public String getPpLieuNaissanceDepartement() {
        return ppLieuNaissanceDepartement;
    }

    /**
     * Set le pp lieu naissance departement.
     *
     * @param ppLieuNaissanceDepartement
     *            the ppLieuNaissanceDepartement to set
     */
    public void setPpLieuNaissanceDepartement(String ppLieuNaissanceDepartement) {
        this.ppLieuNaissanceDepartement = ppLieuNaissanceDepartement;
    }

    /**
     * Get le pp lieu naissance commune.
     *
     * @return the ppLieuNaissanceCommune
     */
    public String getPpLieuNaissanceCommune() {
        return ppLieuNaissanceCommune;
    }

    /**
     * Set le pp lieu naissance commune.
     *
     * @param ppLieuNaissanceCommune
     *            the ppLieuNaissanceCommune to set
     */
    public void setPpLieuNaissanceCommune(String ppLieuNaissanceCommune) {
        this.ppLieuNaissanceCommune = ppLieuNaissanceCommune;
    }

    /**
     * Get le pp lieu naissance ville.
     *
     * @return the ppLieuNaissanceVille
     */
    public String getPpLieuNaissanceVille() {
        return ppLieuNaissanceVille;
    }

    /**
     * Set le pp lieu naissance ville.
     *
     * @param ppLieuNaissanceVille
     *            the ppLieuNaissanceVille to set
     */
    public void setPpLieuNaissanceVille(String ppLieuNaissanceVille) {
        this.ppLieuNaissanceVille = ppLieuNaissanceVille;
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

}
