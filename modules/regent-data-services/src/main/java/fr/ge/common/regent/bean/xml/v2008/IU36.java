/**
 * 
 */
package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Interface U36.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
@ResourceXPath("/U36")
public interface IU36 {

  /**
   * Accesseur sur l'attribut {@link #U361}.
   *
   * @return U361
   */
  @FieldXPath("U36.2")
  String getU362();

  /**
   * Mutateur sur l'attribut {@link #U361}.
   *
   * @param U361
   *          la nouvelle valeur de l'attribut U361
   */
  void setU362(String U361);

  /**
   * Accesseur sur l'attribut {@link #U362}.
   *
   * @return U362
   */
  @FieldXPath("U36.3")
  String getU363();

  /**
   * Mutateur sur l'attribut {@link #U362}.
   *
   * @param U362
   *          la nouvelle valeur de l'attribut U362
   */
  void setU363(String U362);

}
