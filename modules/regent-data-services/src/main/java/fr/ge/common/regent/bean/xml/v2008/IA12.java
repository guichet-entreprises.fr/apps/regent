package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Interface IA12.
 */
@ResourceXPath("/A12")
public interface IA12 {
	
	/**
	   * Get le A123.
	   *
	   * @return le A123
	   */
	  @FieldXPath("A12.3")
	  String getA123();

	  /**
	   * Set le A123.
	   *
	   * @param A123
	   *          le nouveau A123
	   */
	  void setA123(String A123);

	  /**
	   * Get le A125.
	   *
	   * @return le A125
	   */
	  @FieldXPath("A12.5")
	  String getA125();

	  /**
	   * Set le A125.
	   *
	   * @param A125
	   *          le nouveau A125
	   */
	  void setA125(String A125);

	  /**
	   * Get le A126.
	   *
	   * @return le A126
	   */
	  @FieldXPath("A12.6")
	  String getA126();

	  /**
	   * Set le A126.
	   *
	   * @param A126
	   *          le nouveau A126
	   */
	  void setA126(String A126);

	  /**
	   * Get le A127.
	   *
	   * @return le A127
	   */
	  @FieldXPath("A12.7")
	  String getA127();

	  /**
	   * Set le A127.
	   *
	   * @param A127
	   *          le nouveau A127
	   */
	  void setA127(String A127);

	  /**
	   * Get le A128.
	   *
	   * @return le A128
	   */
	  @FieldXPath("A12.8")
	  String getA128();

	  /**
	   * Set le A128.
	   *
	   * @param A128
	   *          le nouveau A128
	   */
	  void setA128(String A128);

	  /**
	   * Get le A1210.
	   *
	   * @return le A1210
	   */
	  @FieldXPath("A12.10")
	  String getA1210();

	  /**
	   * Set le A1210.
	   *
	   * @param A1210
	   *          le nouveau A1210
	   */
	  void setA1210(String A1210);

	  /**
	   * Get le A1211.
	   *
	   * @return le A1211
	   */
	  @FieldXPath("A12.11")
	  String getA1211();

	  /**
	   * Set le A1211.
	   *
	   * @param A1211
	   *          le nouveau A1211
	   */
	  void setA1211(String A1211);

	  /**
	   * Get le A1212.
	   *
	   * @return le A1212
	   */
	  @FieldXPath("A12.12")
	  String getA1212();

	  /**
	   * Set le A1212.
	   *
	   * @param A1212
	   *          le nouveau A1212
	   */
	  void setA1212(String A1212);

	  /**
	   * Get le A1213.
	   *
	   * @return le A1213
	   */
	  @FieldXPath("A12.13")
	  String getA1213();

	  /**
	   * Set le A1213.
	   *
	   * @param A1213
	   *          le nouveau A1213
	   */
	  void setA1213(String A1213);

	  /**
	   * Get le A1214.
	   *
	   * @return le A1214
	   */
	  @FieldXPath("A12.14")
	  String getA1214();

	  /**
	   * Set le A1214.
	   *
	   * @param A1214
	   *          le nouveau A1214
	   */
	  void setA1214(String A1214);


}
