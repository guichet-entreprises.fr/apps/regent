package fr.ge.common.regent.bean.modele.referentiel;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import fr.ge.common.regent.bean.IXmlRegent;

/**
 * Le Class ERegleRegent.
 */
@Entity
@Proxy(lazy = false)
@Table(name = "regles_regent")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class ERegleRegent implements IXmlRegent, Serializable {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -6391217813023356782L;

    /** Le id. */
    @Id
    @Column(name = "CH_NUM", nullable = false, columnDefinition = "Integer")
    private String id;

    /** Le path. */
    @Column(name = "CH_Path", nullable = false)
    private String path;

    /** Le evaluation. */
    @Column(name = "CH_EVALUATION", nullable = true, length = 1048576)
    private String evaluation;

    /** Le occurences. */
    @Column(name = "CH_OCCURENCES", nullable = true)
    private Integer occurences;

    /** Le obligatoire. */
    @Column(name = "CH_OBLIGATOIRE", nullable = true)
    private boolean obligatoire;

    /** Le bourrage. */
    @Column(name = "CH_BOURRAGE", nullable = true)
    private String bourrage;

    /** Le version. */
    @Column(name = "CH_VERSION", nullable = false)
    private String version;

    /** Le rubrique. */
    @Column(name = "CH_RUBRIQUE", nullable = false)
    private String rubrique;

    /** Le end path. */
    @Column(name = "CH_ENDPATH", nullable = false)
    private boolean endPath;

    /**
     * {@inheritDoc}
     */
    @Override
    public String getPath() {
        return path;
    }

    /**
     * Set le path.
     *
     * @param path
     *            le nouveau path
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getEvaluation() {
        return evaluation;
    }

    /**
     * Set le evaluation.
     *
     * @param evaluation
     *            le nouveau evaluation
     */
    public void setEvaluation(String evaluation) {
        this.evaluation = evaluation;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer getOccurences() {
        return occurences;
    }

    /**
     * Set le occurences.
     *
     * @param occurences
     *            le nouveau occurences
     */
    public void setOccurences(Integer occurences) {
        this.occurences = occurences;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean getObligatoire() {
        return obligatoire;
    }

    /**
     * Set le obligatoire.
     *
     * @param obligatoire
     *            le nouveau obligatoire
     */
    public void setObligatoire(boolean obligatoire) {
        this.obligatoire = obligatoire;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getBourrage() {
        return bourrage;
    }

    /**
     * Set le bourrage.
     *
     * @param bourrage
     *            le nouveau bourrage
     */
    public void setBourrage(String bourrage) {
        this.bourrage = bourrage;
    }

    /**
     * Get le version.
     *
     * @return le version
     */
    public String getVersion() {
        return version;
    }

    /**
     * Set le version.
     *
     * @param version
     *            le nouveau version
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getRubrique() {
        return rubrique;
    }

    /**
     * Set le rubrique.
     *
     * @param rubrique
     *            le nouveau rubrique
     */
    public void setRubrique(String rubrique) {
        this.rubrique = rubrique;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean getEndPath() {
        return endPath;
    }

    /**
     * Set le end path.
     *
     * @param endPath
     *            le nouveau end path
     */
    public void setEndPath(boolean endPath) {
        this.endPath = endPath;
    }

    /**
     * Accesseur sur l'attribut {@link #id}.
     *
     * @return String id
     */
    public String getId() {
        return id;
    }

    /**
     * Mutateur sur l'attribut {@link #id}.
     *
     * @param id
     *            la nouvelle valeur de l'attribut id
     */
    public void setId(String id) {
        this.id = id;
    }

}
