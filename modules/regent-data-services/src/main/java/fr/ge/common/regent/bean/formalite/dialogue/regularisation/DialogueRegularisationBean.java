package fr.ge.common.regent.bean.formalite.dialogue.regularisation;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import fr.ge.common.regent.bean.formalite.dialogue.AbstractFormalite;

/**
 * Le Class FormaliteRegularisationBean.
 */
public class DialogueRegularisationBean extends AbstractFormalite<EntrepriseBean> implements IFormaliteRegularisationVue {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -2850877139634716057L;

    /** Le code commune activite. */
    private String codeCommuneActivite;

    /** L'attribut indiquant si une formalité de régul est terminee. */
    private boolean terminee;

    /**
     * Instantie un nouveau formalite regularisation bean.
     */
    public DialogueRegularisationBean() {

    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see fr.guichetentreprises.vue.AbstractFormalite#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    /**
     * Get le code commune activite.
     * 
     * @return the codeCommuneActivite
     */
    public String getCodeCommuneActivite() {
        return this.codeCommuneActivite;
    }

    /**
     * Set le code commune activite.
     * 
     * @param codeCommuneActivite
     *            the codeCommuneActivite to set
     */
    public void setCodeCommuneActivite(final String codeCommuneActivite) {
        this.codeCommuneActivite = codeCommuneActivite;
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see fr.guichetentreprises.vue.AbstractFormalite#createNewEntreprise()
     */
    @Override
    public EntrepriseBean createNewEntreprise() {
        return new EntrepriseBean();
    }

    /**
     * Accesseur sur l'attribut {@link #terminee}.
     *
     * @return boolean terminee
     */
    public boolean isTerminee() {
        return terminee;
    }

    /**
     * Mutateur sur l'attribut {@link #terminee}.
     *
     * @param terminee
     *            la nouvelle valeur de l'attribut terminee
     */
    public void setTerminee(final boolean terminee) {
        this.terminee = terminee;
    }
}
