package fr.ge.common.regent.services.declenchement.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import fr.ge.common.regent.persistance.dao.referentiel.NormeDestinataireDao;
import fr.ge.common.regent.persistance.dao.referentiel.NormeEvenementDao;
import fr.ge.common.regent.services.declenchement.IDeclenchementRubriqueXmlRegentService;

/**
 * Le Class DeclenchementRubriqueXmlRegentImpl.
 */
public class DeclenchementRubriqueXmlRegentServiceImpl implements IDeclenchementRubriqueXmlRegentService {

    /** Le norme destinataire dao. */
    @Autowired
    private NormeDestinataireDao normeDestinataireDao;

    /** Le norme evenement dao. */
    @Autowired
    private NormeEvenementDao normeEvenementDao;

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true, value = "regent")
    public List<String> getListRubriqueForXmlRegent(final String version, final List<String> evenements, final String role, final String destinataire) {

        List<String> listRubriqueByRole = this.normeDestinataireDao.getRubriqueByRoleAndDestinataire(role, destinataire);
        List<String> listRubriqueByEvenement = this.normeEvenementDao.getRubriqueByEvenementAndVersion(version, evenements);

        List<String> listRubriqueForXmlRegent = this.intersection(listRubriqueByEvenement, listRubriqueByRole);
        return listRubriqueForXmlRegent;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true, value = "regent")
    public List<String> getListRubriqueByEventAndVersion(List<String> evenements, String version) {

        List<String> listRubriqueByEvenement = this.normeEvenementDao.getRubriqueByEvenementAndVersion(version, evenements);

        return listRubriqueByEvenement;
    }

    /**
     * Intersection.
     *
     * @param <T>
     *            le type generique
     * @param list1
     *            le list1
     * @param list2
     *            le list2
     * @return le list
     */
    private <T> List<T> intersection(final List<T> list1, final List<T> list2) {
        List<T> list = new ArrayList<T>();

        for (T t : list1) {
            if (list2.contains(t)) {
                list.add(t);
            }
        }

        return list;
    }

}
