package fr.ge.common.regent.bean.formalite.dialogue;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.format.annotation.NumberFormat;

import fr.ge.common.regent.bean.formalite.FormaliteVueBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.AutreEtablissementUEBean;

/**
 * Le Class AbstractEntrepriseBean.
 *
 * @param <T>
 *            le type generique
 * @param <E>
 *            le type de l'element
 * @param <D>
 *            le type generique
 * @param <P>
 *            le type generique
 */
public abstract class AbstractEntrepriseBean<T extends AbstractEtablissementBean, E extends AbstractEntrepriseLieeBean, D extends AbstractDirigeantBean, P extends AbstractPersonneLieeBean>
        extends FormaliteVueBean implements IEntrepriseEtabFactory<T, E> {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -7998540450466394889L;

    /** Le exercice double activite. */
    private String exerciceDoubleActivite;

    /** Le insaisissabilite declaration. */
    private String insaisissabiliteDeclaration;

    /** Le insaisissabilite publication. */
    private String insaisissabilitePublication;

    /** Le insaisissabilite Renonciation RP. */
    protected String insaisissabiliteRenonciationRP;

    /** Le insaisissabilite Publication Renonciation RP. */
    protected String insaisissabilitePublicationRenonciationRP;

    /** Le insaisissabilite revocation RP. */
    protected String insaisissabiliteRevocationRP;

    /** Le insaisissabilite publication revocation RP. */
    protected String insaisissabilitePublicationRevocationRP;

    /** Le insaisissabilite Declaration Autres Biens. */
    private String insaisissabiliteDeclarationAutresBiens;

    /** Le insaisissabilite Publication Declaration Autres Biens. */
    private String insaisissabilitePublicationDeclarationAutresBiens;

    /** Le insaisissabilite Renonciation Declaration Autres Biens. */
    private String insaisissabiliteRenonciationDeclarationAutresBiens;

    /**
     * Le insaisissabilite Publication Renonciation Declaration Autres Biens.
     */
    private String insaisissabilitePublicationRenonciationDeclarationAutresBiens;

    /** Le autre etablissement ue. */
    private String autreEtablissementUE;

    /** Le autre etablissement ue nombre. */
    @NumberFormat
    private Integer autreEtablissementUENombre;

    /** Le adresse entreprise pp situation. */
    private String adresseEntreprisePPSituation;

    /** Le etablissement. */
    private T etablissement;

    /** Le entreprise liee domiciliation. */
    private E entrepriseLieeDomiciliation;

    /** Le entreprise liee loueur mandant. */
    private E entrepriseLieeLoueurMandant;

    /** Le nombre dirigeants. */
    @NumberFormat
    private Integer nombreDirigeants;

    /**
     * Setter de l'attribut nombreDirigeants.
     * 
     * @param nombreDirigeants
     *            la nouvelle valeur de nombreDirigeants
     */
    public void setNombreDirigeants(final Integer nombreDirigeants) {
        this.nombreDirigeants = nombreDirigeants;
    }

    /**
     * Getter de l'attribut nombreDirigeants.
     * 
     * @return la valeur de nombreDirigeants
     */
    public Integer getNombreDirigeants() {
        return this.nombreDirigeants;
    }

    /** Le etablissements ue. */
    private List<AutreEtablissementUEBean> etablissementsUE = new ArrayList<AutreEtablissementUEBean>();

    /** Le dirigeants. */
    private List<D> dirigeants;

    /** Le personne liee. */
    private List<P> personneLiee = new ArrayList<P>();

    /** Le forme juridique. */
    private String formeJuridique;

    /** Le forme juridique. */
    private String formeJuridiqueLibelle;

    /** Le associe unique. */
    private String associeUnique;

    /**
     * Instancie un nouveau abstract entreprise bean.
     */
    public AbstractEntrepriseBean() {
        super();

        if (this.nombreDirigeants == null) {

            dirigeants = new ArrayList<D>();
        } else {

            dirigeants = new ArrayList<D>(this.getNombreDirigeants());
        }

        if (this.etablissement == null) {
            this.etablissement = this.createNewEtablissement();
        }
        if (this.entrepriseLieeDomiciliation == null) {
            this.entrepriseLieeDomiciliation = this.createNewEntrepriseLiee();
        }

        if (this.entrepriseLieeLoueurMandant == null) {
            this.entrepriseLieeLoueurMandant = this.createNewEntrepriseLiee();
        }

        if (CollectionUtils.isEmpty(this.personneLiee)) {
            this.personneLiee = new ArrayList<P>();
        }
    }

    /**
     * Get le forme juridique.
     *
     * @return the formeJuridique
     */
    public String getFormeJuridique() {
        return this.formeJuridique;
    }

    /**
     * Set le forme juridique.
     *
     * @param formeJuridique
     *            the formeJuridique to set
     */
    public void setFormeJuridique(final String formeJuridique) {
        this.formeJuridique = formeJuridique;
    }

    /**
     * Get le associe unique.
     *
     * @return the associeUnique
     */
    public String getAssocieUnique() {
        return this.associeUnique;
    }

    /**
     * Set le associe unique.
     *
     * @param associeUnique
     *            the associeUnique to set
     */
    public void setAssocieUnique(final String associeUnique) {
        this.associeUnique = associeUnique;
    }

    /**
     * Get le personne liee.
     *
     * @return the personneLiee
     */
    public List<P> getPersonneLiee() {
        return this.personneLiee;
    }

    /**
     * Set le personne liee.
     *
     * @param personneLiee
     *            the personneLiee to set
     */
    public void setPersonneLiee(final List<P> personneLiee) {
        this.personneLiee = personneLiee;
    }

    /**
     * Get le etablissement.
     *
     * @return the etablissement
     */
    public T getEtablissement() {
        return this.etablissement;
    }

    /**
     * Set le etablissement.
     *
     * @param etablissement
     *            the etablissement to set
     */
    public void setEtablissement(final T etablissement) {
        this.etablissement = etablissement;
    }

    /**
     * Get le entreprise liee domiciliation.
     *
     * @return the entrepriseLieeDomiciliation
     */
    public E getEntrepriseLieeDomiciliation() {
        return this.entrepriseLieeDomiciliation;
    }

    /**
     * Set le entreprise liee domiciliation.
     *
     * @param entrepriseLieeDomiciliation
     *            the entrepriseLieeDomiciliation to set
     */
    public void setEntrepriseLieeDomiciliation(final E entrepriseLieeDomiciliation) {
        this.entrepriseLieeDomiciliation = entrepriseLieeDomiciliation;
    }

    /**
     * Get le entreprise liee loueur mandant.
     *
     * @return the entrepriseLieeLoueurMandant
     */
    public E getEntrepriseLieeLoueurMandant() {
        return this.entrepriseLieeLoueurMandant;
    }

    /**
     * Set le entreprise liee loueur mandant.
     *
     * @param entrepriseLieeLoueurMandant
     *            the entrepriseLieeLoueurMandant to set
     */
    public void setEntrepriseLieeLoueurMandant(final E entrepriseLieeLoueurMandant) {
        this.entrepriseLieeLoueurMandant = entrepriseLieeLoueurMandant;
    }

    /**
     * Get le etablissements ue.
     *
     * @return the etablissementsUE
     */
    public List<AutreEtablissementUEBean> getEtablissementsUE() {
        return this.etablissementsUE;
    }

    /**
     * Set le etablissements ue.
     *
     * @param etablissementsUE
     *            the etablissementsUE to set
     */
    public void setEtablissementsUE(final List<AutreEtablissementUEBean> etablissementsUE) {
        this.etablissementsUE = etablissementsUE;
    }

    /**
     * Get le dirigeants.
     *
     * @return the dirigeants
     */
    public List<D> getDirigeants() {
        return this.dirigeants;
    }

    /**
     * Set le dirigeants. h
     * 
     * @param dirigeants
     *            the dirigeants to set
     */
    public void setDirigeants(final List<D> dirigeants) {
        this.dirigeants = dirigeants;
    }

    /**
     * Get le exercice double activite.
     *
     * @return the exerciceDoubleActivite
     */
    public String getExerciceDoubleActivite() {
        return this.exerciceDoubleActivite;
    }

    /**
     * Set le exercice double activite.
     *
     * @param exerciceDoubleActivite
     *            the exerciceDoubleActivite to set
     */
    public void setExerciceDoubleActivite(final String exerciceDoubleActivite) {
        this.exerciceDoubleActivite = exerciceDoubleActivite;
    }

    /**
     * Get le insaisissabilite declaration.
     *
     * @return the insaisissabiliteDeclaration
     */
    public String getInsaisissabiliteDeclaration() {
        return this.insaisissabiliteDeclaration;
    }

    /**
     * Set le insaisissabilite declaration.
     *
     * @param insaisissabiliteDeclaration
     *            the insaisissabiliteDeclaration to set
     */
    public void setInsaisissabiliteDeclaration(final String insaisissabiliteDeclaration) {
        this.insaisissabiliteDeclaration = insaisissabiliteDeclaration;
    }

    /**
     * Get le insaisissabilite publication.
     *
     * @return the insaisissabilitePublication
     */
    public String getInsaisissabilitePublication() {
        return this.insaisissabilitePublication;
    }

    /**
     * Set le insaisissabilite publication.
     *
     * @param insaisissabilitePublication
     *            the insaisissabilitePublication to set
     */
    public void setInsaisissabilitePublication(final String insaisissabilitePublication) {
        this.insaisissabilitePublication = insaisissabilitePublication;
    }

    /**
     * Accesseur sur l'attribut {@link #insaisissabiliteRenonciationRP}.
     *
     * @return String insaisissabiliteRenonciationRP
     */
    public String getInsaisissabiliteRenonciationRP() {
        return insaisissabiliteRenonciationRP;
    }

    /**
     * Mutateur sur l'attribut {@link #insaisissabiliteRenonciationRP}.
     *
     * @param insaisissabiliteRenonciationRP
     *            la nouvelle valeur de l'attribut
     *            insaisissabiliteRenonciationRP
     */
    public void setInsaisissabiliteRenonciationRP(final String insaisissabiliteRenonciationRP) {
        this.insaisissabiliteRenonciationRP = insaisissabiliteRenonciationRP;
    }

    /**
     * Accesseur sur l'attribut
     * {@link #insaisissabilitePublicationRenonciationRP}.
     *
     * @return String insaisissabilitePublicationRenonciationRP
     */
    public String getInsaisissabilitePublicationRenonciationRP() {
        return insaisissabilitePublicationRenonciationRP;
    }

    /**
     * Mutateur sur l'attribut
     * {@link #insaisissabilitePublicationRenonciationRP}.
     *
     * @param insaisissabilitePublicationRenonciationRP
     *            la nouvelle valeur de l'attribut
     *            insaisissabilitePublicationRenonciationRP
     */
    public void setInsaisissabilitePublicationRenonciationRP(final String insaisissabilitePublicationRenonciationRP) {
        this.insaisissabilitePublicationRenonciationRP = insaisissabilitePublicationRenonciationRP;
    }

    /**
     * Accesseur sur l'attribut {@link #insaisissabiliteRevocationRP}.
     *
     * @return String insaisissabiliteRevocationRP
     */
    public String getInsaisissabiliteRevocationRP() {
        return insaisissabiliteRevocationRP;
    }

    /**
     * Mutateur sur l'attribut {@link #insaisissabiliteRevocationRP}.
     *
     * @param insaisissabiliteRevocationRP
     *            la nouvelle valeur de l'attribut insaisissabiliteRevocationRP
     */
    public void setInsaisissabiliteRevocationRP(final String insaisissabiliteRevocationRP) {
        this.insaisissabiliteRevocationRP = insaisissabiliteRevocationRP;
    }

    /**
     * Accesseur sur l'attribut
     * {@link #insaisissabilitePublicationRevocationRP}.
     *
     * @return String insaisissabilitePublicationRevocationRP
     */
    public String getInsaisissabilitePublicationRevocationRP() {
        return insaisissabilitePublicationRevocationRP;
    }

    /**
     * Mutateur sur l'attribut {@link #insaisissabilitePublicationRevocationRP}.
     *
     * @param insaisissabilitePublicationRevocationRP
     *            la nouvelle valeur de l'attribut
     *            insaisissabilitePublicationRevocationRP
     */
    public void setInsaisissabilitePublicationRevocationRP(final String insaisissabilitePublicationRevocationRP) {
        this.insaisissabilitePublicationRevocationRP = insaisissabilitePublicationRevocationRP;
    }

    /**
     * Accesseur sur l'attribut {@link #insaisissabiliteDeclarationAutresBiens}.
     *
     * @return String insaisissabiliteDeclarationAutresBiens
     */
    public String getInsaisissabiliteDeclarationAutresBiens() {
        return insaisissabiliteDeclarationAutresBiens;
    }

    /**
     * Mutateur sur l'attribut {@link #insaisissabiliteDeclarationAutresBiens}.
     *
     * @param insaisissabiliteDeclarationAutresBiens
     *            la nouvelle valeur de l'attribut
     *            insaisissabiliteDeclarationAutresBiens
     */
    public void setInsaisissabiliteDeclarationAutresBiens(final String insaisissabiliteDeclarationAutresBiens) {
        this.insaisissabiliteDeclarationAutresBiens = insaisissabiliteDeclarationAutresBiens;
    }

    /**
     * Accesseur sur l'attribut
     * {@link #insaisissabiliteRenonciationDeclarationAutresBiens}.
     *
     * @return String insaisissabiliteRenonciationDeclarationAutresBiens
     */
    public String getInsaisissabiliteRenonciationDeclarationAutresBiens() {
        return insaisissabiliteRenonciationDeclarationAutresBiens;
    }

    /**
     * Mutateur sur l'attribut
     * {@link #insaisissabiliteRenonciationDeclarationAutresBiens}.
     *
     * @param insaisissabiliteRenonciationDeclarationAutresBiens
     *            la nouvelle valeur de l'attribut
     *            insaisissabiliteRenonciationDeclarationAutresBiens
     */
    public void setInsaisissabiliteRenonciationDeclarationAutresBiens(final String insaisissabiliteRenonciationDeclarationAutresBiens) {
        this.insaisissabiliteRenonciationDeclarationAutresBiens = insaisissabiliteRenonciationDeclarationAutresBiens;
    }

    /**
     * Accesseur sur l'attribut
     * {@link #insaisissabilitePublicationRenonciationDeclarationAutresBiens}.
     *
     * @return String
     *         insaisissabilitePublicationRenonciationDeclarationAutresBiens
     */
    public String getInsaisissabilitePublicationRenonciationDeclarationAutresBiens() {
        return insaisissabilitePublicationRenonciationDeclarationAutresBiens;
    }

    /**
     * Mutateur sur l'attribut
     * {@link #insaisissabilitePublicationRenonciationDeclarationAutresBiens}.
     *
     * @param insaisissabilitePublicationRenonciationDeclarationAutresBiens
     *            la nouvelle valeur de l'attribut
     *            insaisissabilitePublicationRenonciationDeclarationAutresBiens
     */
    public void setInsaisissabilitePublicationRenonciationDeclarationAutresBiens(final String insaisissabilitePublicationRenonciationDeclarationAutresBiens) {
        this.insaisissabilitePublicationRenonciationDeclarationAutresBiens = insaisissabilitePublicationRenonciationDeclarationAutresBiens;
    }

    /**
     * Accesseur sur l'attribut
     * {@link #insaisissabilitePublicationDeclarationAutresBiens}.
     *
     * @return String insaisissabilitePublicationDeclarationAutresBiens
     */
    public String getInsaisissabilitePublicationDeclarationAutresBiens() {
        return insaisissabilitePublicationDeclarationAutresBiens;
    }

    /**
     * Mutateur sur l'attribut
     * {@link #insaisissabilitePublicationDeclarationAutresBiens}.
     *
     * @param insaisissabilitePublicationDeclarationAutresBiens
     *            la nouvelle valeur de l'attribut
     *            insaisissabilitePublicationDeclarationAutresBiens
     */
    public void setInsaisissabilitePublicationDeclarationAutresBiens(final String insaisissabilitePublicationDeclarationAutresBiens) {
        this.insaisissabilitePublicationDeclarationAutresBiens = insaisissabilitePublicationDeclarationAutresBiens;
    }

    /**
     * Get le autre etablissement ue.
     *
     * @return the autreEtablissementUE
     */
    public String getAutreEtablissementUE() {
        return this.autreEtablissementUE;
    }

    /**
     * Set le autre etablissement ue.
     *
     * @param autreEtablissementUE
     *            the autreEtablissementUE to set
     */
    public void setAutreEtablissementUE(final String autreEtablissementUE) {
        this.autreEtablissementUE = autreEtablissementUE;
    }

    /**
     * Get le autre etablissement ue nombre.
     *
     * @return the autreEtablissementUENombre
     */
    public Integer getAutreEtablissementUENombre() {
        return this.autreEtablissementUENombre;
    }

    /**
     * Set le autre etablissement ue nombre.
     *
     * @param autreEtablissementUENombre
     *            the autreEtablissementUENombre to set
     */
    public void setAutreEtablissementUENombre(final Integer autreEtablissementUENombre) {
        this.autreEtablissementUENombre = autreEtablissementUENombre;
    }

    /**
     * Get le adresse entreprise pp situation.
     *
     * @return the adresseEntreprisePPSituation
     */
    public String getAdresseEntreprisePPSituation() {
        return this.adresseEntreprisePPSituation;
    }

    /**
     * Set le adresse entreprise pp situation.
     *
     * @param adresseEntreprisePPSituation
     *            the adresseEntreprisePPSituation to set
     */
    public void setAdresseEntreprisePPSituation(final String adresseEntreprisePPSituation) {
        this.adresseEntreprisePPSituation = adresseEntreprisePPSituation;
    }

    /**
     * Accesseur sur l'attribut {@link #formeJuridiqueLibelle}.
     *
     * @return String formeJuridiqueLibelle
     */
    public String getFormeJuridiqueLibelle() {
        return formeJuridiqueLibelle;
    }

    /**
     * Mutateur sur l'attribut {@link #formeJuridiqueLibelle}.
     *
     * @param formeJuridiqueLibelle
     *            la nouvelle valeur de l'attribut formeJuridiqueLibelle
     */
    public void setFormeJuridiqueLibelle(final String formeJuridiqueLibelle) {
        this.formeJuridiqueLibelle = formeJuridiqueLibelle;
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

}
