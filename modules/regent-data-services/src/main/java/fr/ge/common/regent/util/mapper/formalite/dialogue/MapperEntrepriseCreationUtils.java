package fr.ge.common.regent.util.mapper.formalite.dialogue;

import java.util.ArrayList;

import org.apache.commons.collections.CollectionUtils;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xmlfield.core.types.XmlString;

import fr.ge.common.regent.bean.formalite.dialogue.creation.ActiviteSoumiseQualificationBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.AdresseBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.AutreEtablissementUEBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.AyantDroitBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.ConjointBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.DeclarationSocialeBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.DialogueCreationBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.DirigeantBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.EirlBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.EntrepriseBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.EntrepriseLieeBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.EtablissementBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.PersonneLieeBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.RegimeFiscalBean;
import fr.ge.common.regent.bean.xml.formalite.dialogue.creation.IActiviteSoumiseQualificationCreation;
import fr.ge.common.regent.bean.xml.formalite.dialogue.creation.IAutreEtablissementUECreation;
import fr.ge.common.regent.bean.xml.formalite.dialogue.creation.IAyantDroit;
import fr.ge.common.regent.bean.xml.formalite.dialogue.creation.IConjointCreation;
import fr.ge.common.regent.bean.xml.formalite.dialogue.creation.IDeclarationSociale;
import fr.ge.common.regent.bean.xml.formalite.dialogue.creation.IDialogueCreation;
import fr.ge.common.regent.bean.xml.formalite.dialogue.creation.IDirigeantCreation;
import fr.ge.common.regent.bean.xml.formalite.dialogue.creation.IEntrepriseCreation;
import fr.ge.common.regent.bean.xml.formalite.dialogue.creation.IEntrepriseLieeCreation;
import fr.ge.common.regent.bean.xml.formalite.dialogue.creation.IEtablissementCreation;
import fr.ge.common.regent.bean.xml.formalite.dialogue.creation.IPersonneLieeCreation;
import fr.ge.common.regent.bean.xml.formalite.dialogue.creation.IRegimeFiscal;
import fr.ge.common.regent.bean.xml.transverse.IAdresse;

/**
 * Class MapperEntrepriseCreationUtils.
 */
@Component
public class MapperEntrepriseCreationUtils {

    /** Le adresse création mapper. */
    @Autowired
    private Mapper adresseMapper;

    /** Le postal commune création mapper. */
    @Autowired
    private Mapper postalCommuneMapper;

    /** Le entreprise liee création mapper. */
    @Autowired
    private Mapper entrepriseLieeMapper;

    /** Le etablissement création mapper. */
    @Autowired
    private Mapper etablissementMapper;

    /** Le autre regimeFiscalMapper céation mapper. */
    @Autowired
    private Mapper regimeFiscalMapper;

    /** Le autre eirl création mapper. */
    @Autowired
    private Mapper eirlMapper;

    /** Le ayantsDroit création mapper. */
    @Autowired
    private Mapper ayantDroitMapper;

    /** Le declarationsociale création mapper. */
    @Autowired
    private Mapper declarationSocialeMapper;

    /** Le dirigeant création mapper. */
    @Autowired
    private Mapper dirigeantMapper;

    /** Le conjoint création mapper. */
    @Autowired
    private Mapper conjointMapper;

    /** Le personne liee création mapper. */
    @Autowired
    private Mapper personneLieeMapper;

    /** Le entreprise création mapper. */
    @Autowired
    private Mapper entrepriseMapper;

    /** Mapper de l'activité soumise à qualification. **/
    @Autowired
    private Mapper activiteSoumiseQualificationMapper;

    /**
     * Mapper Entreprise Interface Xml Field vers Bean.
     * 
     * @param iFormaliteCreation
     *            {@link IDialogueCreation}
     * @param formaliteCreationBean
     *            {@link DialogueCreationBean}
     */
    public void mapperEntreprise(IDialogueCreation iFormaliteCreation, DialogueCreationBean formaliteCreationBean) {
        IEntrepriseCreation iEntrepriseCreation = iFormaliteCreation.getEntreprise();

        if (iEntrepriseCreation != null) {

            EntrepriseBean entrepriseBean = formaliteCreationBean.getEntreprise();

            // Mapper Adresse Entreprise
            mapperAdresseEntreprise(iEntrepriseCreation, entrepriseBean);

            // Mapper Entreprise responsable du contrat d'appui
            mapperEntrepriseResponsableContratAppui(iEntrepriseCreation, entrepriseBean);

            // Mapper Entreprise de domiciliation
            mapperEntrepriseDomiciliation(iEntrepriseCreation, entrepriseBean);

            // Mapper Loueur-mandant du fonds
            mapperEntrepriseLieeLoueurMondant(iEntrepriseCreation, entrepriseBean);

            // Mapper Etablissement
            IEtablissementCreation iEtablissementCreation = iEntrepriseCreation.getEtablissement();
            if (iEtablissementCreation != null) {
                mapperEtablissementCreation(iEtablissementCreation, entrepriseBean);

            }
            /** Mapper les listes pour entreprise **/

            // Mapper Liste des Précédents exploitants
            mapperEntreprisesLieesPrecedentsExploitants(iEntrepriseCreation, entrepriseBean);

            // Mapper Liste des entreprises participantes à l'opération de
            // fusion/scission
            mapperEntreprisesLieesFusionScission(iEntrepriseCreation, entrepriseBean);

            // Mapper Autre établissement situé dans l'Union Européenne
            mapperAutreEtablissementsUE(iEntrepriseCreation, entrepriseBean);

            // Mapper regimeFiscalEirl
            mapperRegimeFiscalEirl(iEntrepriseCreation, entrepriseBean);

            // Mapper regimeFiscalEirlSecondaire
            mapperRegimeFiscalEirlSecondaire(iEntrepriseCreation, entrepriseBean);

            // Mapper regimeFiscalEtablissement
            mapperRegimeFiscalEtablissement(iEntrepriseCreation, entrepriseBean);

            // Mapper regimeFiscalEtablissementSecondaire
            mapperRegimeFiscalEtabSecond(iEntrepriseCreation, entrepriseBean);

            // Mapper Eirl
            mapperEirl(iEntrepriseCreation, entrepriseBean);

            // Mapper dirigeants
            mapperDirigeants(iEntrepriseCreation, entrepriseBean);

            // Mapper personneLiee
            mapperPersonnesLiees(iEntrepriseCreation, entrepriseBean);

            // Mapper la liste capitalApportsNature
            mapperCapitalApportsNature(iEntrepriseCreation, entrepriseBean);

            // mapping champs simples pour Entreprise
            this.entrepriseMapper.map(iEntrepriseCreation, entrepriseBean);
        }
    }

    /**
     * Mapper Adresse Interface Xml Field vers Bean
     * 
     * @param iEntrepriseCreation
     *            {@link IEntrepriseCreation}
     * @param entrepriseBean
     *            {@link EntrepriseBean}
     */
    private void mapperAdresseEntreprise(IEntrepriseCreation iEntrepriseCreation, EntrepriseBean entrepriseBean) {
        IAdresse iEntreprisedresse = iEntrepriseCreation.getAdresse();
        if (iEntreprisedresse != null) {
            AdresseBean CorrAdresseBean = entrepriseBean.getAdresse();
            this.adresseMapper.map(iEntreprisedresse, CorrAdresseBean);
            if (iEntreprisedresse.getCodePostalCommune() != null) {
                this.postalCommuneMapper.map(iEntreprisedresse.getCodePostalCommune(), entrepriseBean.getAdresse().getCodePostalCommune());
            }
        }
    }

    /**
     * Mapper Entreprise Responsable Contrat Appui Interface Xml Field vers Bean
     * 
     * @param iEntrepriseCreation
     *            {@link IEntrepriseCreation}
     * @param entrepriseBean
     *            {@link EntrepriseBean}
     */
    private void mapperEntrepriseResponsableContratAppui(IEntrepriseCreation iEntrepriseCreation, EntrepriseBean entrepriseBean) {
        IEntrepriseLieeCreation iEntrepriseLieeTypeEstResponsable = iEntrepriseCreation.getEntrepriseLieeContratAppui();
        if (iEntrepriseLieeTypeEstResponsable != null) {
            this.mapperEntrepriseLieeContratAppui(entrepriseBean, iEntrepriseLieeTypeEstResponsable);
        }
    }

    /**
     * Mapper Entreprise de domiciliation Inetrafce Xml Field to Bean
     * 
     * @param iEntrepriseCreation
     *            {@link IEntrepriseCreation}
     * @param entrepriseBean
     *            {@link EntrepriseBean}
     */
    private void mapperEntrepriseDomiciliation(IEntrepriseCreation iEntrepriseCreation, EntrepriseBean entrepriseBean) {
        IEntrepriseLieeCreation iEntrepriseLieeTypeDomiciliation = iEntrepriseCreation.getEntrepriseLieeDomiciliation();
        if (iEntrepriseLieeTypeDomiciliation != null) {
            mapperEntrepriseLieeDomiciliation(entrepriseBean, iEntrepriseLieeTypeDomiciliation);
        }
    }

    /**
     * Mapper Entreprise Liee Dom.
     * 
     * @param entrepriseBean
     *            entrepriseBean
     * @param iEntrLieeDom
     *            iEntrLieeDom
     */

    private void mapperEntrepriseLieeContratAppui(EntrepriseBean entrepriseBean, IEntrepriseLieeCreation iEntrLieeCreationContratAppui) {
        if (iEntrLieeCreationContratAppui != null) {
            if (entrepriseBean.getEntrepriseLieeContratAppui() == null) {
                entrepriseBean.setEntrepriseLieeContratAppui(new EntrepriseLieeBean());
            }
            this.entrepriseLieeMapper.map(iEntrLieeCreationContratAppui, entrepriseBean.getEntrepriseLieeContratAppui());
            IAdresse iAdresseLieeContratAppui = iEntrLieeCreationContratAppui.getAdresse();
            if (iAdresseLieeContratAppui != null) {
                AdresseBean adresseEntLieeContratAppuiBean = entrepriseBean.getEntrepriseLieeContratAppui().getAdresse();
                this.adresseMapper.map(iAdresseLieeContratAppui, adresseEntLieeContratAppuiBean);
                if (iAdresseLieeContratAppui.getCodePostalCommune() != null) {
                    this.postalCommuneMapper.map(iAdresseLieeContratAppui.getCodePostalCommune(), adresseEntLieeContratAppuiBean.getCodePostalCommune());
                }
            }
        }
    }

    /**
     * Mapper entreprise liee domiciliation.
     *
     * @param entrepriseBean
     *            entreprise bean
     * @param iEntrLieeCreationLieeDomiciliation
     *            i entr liee creation liee domiciliation
     */
    private void mapperEntrepriseLieeDomiciliation(EntrepriseBean entrepriseBean, IEntrepriseLieeCreation iEntrLieeCreationLieeDomiciliation) {
        if (iEntrLieeCreationLieeDomiciliation != null) {
            if (entrepriseBean.getEntrepriseLieeDomiciliation() == null) {
                entrepriseBean.setEntrepriseLieeDomiciliation(new EntrepriseLieeBean());
            }
            this.entrepriseLieeMapper.map(iEntrLieeCreationLieeDomiciliation, entrepriseBean.getEntrepriseLieeDomiciliation());
            IAdresse iAdresseLieeDomiciliation = iEntrLieeCreationLieeDomiciliation.getAdresse();
            if (iAdresseLieeDomiciliation != null) {
                AdresseBean adresseEntLieeDomBean = entrepriseBean.getEntrepriseLieeDomiciliation().getAdresse();
                this.adresseMapper.map(iAdresseLieeDomiciliation, adresseEntLieeDomBean);
                if (iAdresseLieeDomiciliation.getCodePostalCommune() != null) {
                    this.postalCommuneMapper.map(iAdresseLieeDomiciliation.getCodePostalCommune(), adresseEntLieeDomBean.getCodePostalCommune());
                }
            }
        }
    }

    /**
     * Mapper Loueur-mandant du fonds Inetrafce Xml Field to Bean
     * 
     * @param iEntrepriseCreation
     *            {@link IEntrepriseCreation}
     * @param entrepriseBean
     *            {@link EntrepriseBean}
     */
    private void mapperEntrepriseLieeLoueurMondant(IEntrepriseCreation iEntrepriseCreation, EntrepriseBean entrepriseBean) {
        IEntrepriseLieeCreation ientrepriseLieeLoueurMandant = iEntrepriseCreation.getEntrepriseLieeLoueurMandant();

        if (ientrepriseLieeLoueurMandant != null) {
            mapperEntrepriseLieeLoueurMandant(entrepriseBean, ientrepriseLieeLoueurMandant);
        }
    }

    /**
     * Mapper entreprise liee loueur mandant.
     *
     * @param entrepriseBean
     *            entreprise bean
     * @param iEntrLieeCreationLieeLoueurMandant
     *            i entr liee creation liee loueur mandant
     */
    private void mapperEntrepriseLieeLoueurMandant(EntrepriseBean entrepriseBean, IEntrepriseLieeCreation iEntrLieeCreationLieeLoueurMandant) {
        if (iEntrLieeCreationLieeLoueurMandant != null) {
            if (entrepriseBean.getEntrepriseLieeLoueurMandant() == null) {
                entrepriseBean.setEntrepriseLieeLoueurMandant(new EntrepriseLieeBean());
            }
            this.entrepriseLieeMapper.map(iEntrLieeCreationLieeLoueurMandant, entrepriseBean.getEntrepriseLieeLoueurMandant());
            IAdresse iAdresseLieeMandant = iEntrLieeCreationLieeLoueurMandant.getAdresse();
            if (iAdresseLieeMandant != null) {
                AdresseBean adresseEntLieeMandantBean = entrepriseBean.getEntrepriseLieeLoueurMandant().getAdresse();
                this.adresseMapper.map(iAdresseLieeMandant, adresseEntLieeMandantBean);
                if (iAdresseLieeMandant.getCodePostalCommune() != null) {
                    this.postalCommuneMapper.map(iAdresseLieeMandant.getCodePostalCommune(), adresseEntLieeMandantBean.getCodePostalCommune());
                }
            }
        }
    }

    /**
     * Mapper l'etablissement Interface Xml Field vers Bean.
     * 
     * @param entrepriseBean
     *            {@link EntrepriseBean}
     * @param iEtablissementCreation
     *            {@link IEntrepriseCreation}
     */
    private void mapperEtablissementCreation(IEtablissementCreation iEtablissementCreation, EntrepriseBean entrepriseBean) {
        if (iEtablissementCreation != null) {
            if (entrepriseBean.getEtablissement() == null) {
                entrepriseBean.setEtablissement(new EtablissementBean());
            }
            this.etablissementMapper.map(iEtablissementCreation, entrepriseBean.getEtablissement());
            IAdresse iAdresseEtablissement = iEtablissementCreation.getAdresse();
            if (iAdresseEtablissement != null) {
                AdresseBean adresseEtablissementBean = entrepriseBean.getEtablissement().getAdresse();
                this.adresseMapper.map(iAdresseEtablissement, adresseEtablissementBean);
                if (iAdresseEtablissement.getCodePostalCommune() != null) {
                    this.postalCommuneMapper.map(iAdresseEtablissement.getCodePostalCommune(), adresseEtablissementBean.getCodePostalCommune());
                }
            }
            /* mapper la liste ActivitesExerceesAgricole */
            mapperActivitesExerceesAgricole(iEtablissementCreation, entrepriseBean);

            /* mapper la liste des activités soumises à qualification */
            mapperActivitesSoumisesQualification(iEtablissementCreation, entrepriseBean);
        }
    }

    /**
     * mapper la liste ActivitesExerceesAgricole.
     * 
     * @param iEtablissementCreation
     *            {@link IEtablissementCreation}
     * @param entrepriseBean
     *            {@link EntrepriseBean}
     */
    private void mapperActivitesExerceesAgricole(IEtablissementCreation iEtablissementCreation, EntrepriseBean entrepriseBean) {
        if (iEtablissementCreation.getActivitesExerceesAgricole() != null) {
            if (entrepriseBean.getEtablissement().getActivitesExerceesAgricole() != null) {
                entrepriseBean.getEtablissement().getActivitesExerceesAgricole().clear();
            } else {
                entrepriseBean.getEtablissement().setActivitesExerceesAgricole(new ArrayList<String>());
            }
            XmlString[] activitesExerceesAgricoleI = iEtablissementCreation.getActivitesExerceesAgricole();
            for (XmlString activiteExerceesAgricoleI : activitesExerceesAgricoleI) {
                entrepriseBean.getEtablissement().getActivitesExerceesAgricole().add(activiteExerceesAgricoleI.getString());
            }
        }
    }

    /**
     * mapper la liste des activités soumises à qualification.
     * 
     * @param iEtablissementCreation
     *            {@link IEtablissementCreation}
     * @param entrepriseBean
     *            {@link EntrepriseBean}
     */
    private void mapperActivitesSoumisesQualification(IEtablissementCreation iEtablissementCreation, EntrepriseBean entrepriseBean) {
        if (iEtablissementCreation.getActivitesSoumisesQualification() != null) {
            if (CollectionUtils.isNotEmpty(entrepriseBean.getEtablissement().getActivitesSoumisesQualification())) {
                entrepriseBean.getEtablissement().getActivitesSoumisesQualification().clear();
            } else {
                entrepriseBean.getEtablissement().setActivitesSoumisesQualification(new ArrayList<ActiviteSoumiseQualificationBean>());
            }
            IActiviteSoumiseQualificationCreation[] iActivitesSoumisesQualification = iEtablissementCreation.getActivitesSoumisesQualification();
            for (IActiviteSoumiseQualificationCreation iActiviteSoumiseQualification : iActivitesSoumisesQualification) {
                ActiviteSoumiseQualificationBean activiteSoumiseQualificationBean = new ActiviteSoumiseQualificationBean();
                activiteSoumiseQualificationMapper.map(iActiviteSoumiseQualification, activiteSoumiseQualificationBean);
                entrepriseBean.getEtablissement().getActivitesSoumisesQualification().add(activiteSoumiseQualificationBean);
            }
        }
    }

    /**
     * Mapper Liste Entreprises Liees Precedents Exploitants Inetrafce Xml Field
     * to Bean.
     * 
     * @param iEntrepriseCreation
     *            {@link IEntrepriseCreation}
     * @param entrepriseBean
     *            {@link EntrepriseBean}
     */
    private void mapperEntreprisesLieesPrecedentsExploitants(IEntrepriseCreation iEntrepriseCreation, EntrepriseBean entrepriseBean) {
        entrepriseBean.getEntreprisesLieesPrecedentsExploitants().clear();
        for (IEntrepriseLieeCreation iEntrLieePrecExploitant : iEntrepriseCreation.getEntreprisesLieesPrecedentsExploitants()) {
            EntrepriseLieeBean entrLieePrecedentExploitantBean = new EntrepriseLieeBean();

            this.entrepriseLieeMapper.map(iEntrLieePrecExploitant, entrLieePrecedentExploitantBean);
            IAdresse iAdresseEntLieeDom = iEntrLieePrecExploitant.getAdresse();
            if (iAdresseEntLieeDom != null) {
                AdresseBean adresseEntLieeDomBean = entrLieePrecedentExploitantBean.getAdresse();
                this.adresseMapper.map(iAdresseEntLieeDom, adresseEntLieeDomBean);
                if (iAdresseEntLieeDom.getCodePostalCommune() != null) {
                    this.postalCommuneMapper.map(iAdresseEntLieeDom.getCodePostalCommune(), adresseEntLieeDomBean.getCodePostalCommune());
                }
            }
            IAdresse iAdresse = iEntrLieePrecExploitant.getAdresse();
            if (iAdresse != null) {
                AdresseBean adresseBean = entrLieePrecedentExploitantBean.getAdresse();
                this.adresseMapper.map(iAdresse, adresseBean);
                if (iAdresse.getCodePostalCommune() != null) {
                    this.postalCommuneMapper.map(iAdresse.getCodePostalCommune(), adresseBean.getCodePostalCommune());
                }
            }

            entrepriseBean.getEntreprisesLieesPrecedentsExploitants().add(entrLieePrecedentExploitantBean);

        }
    }

    /**
     * Mapper Liste des entreprises participantes à l'opération de
     * fusion/scission Inetrafce Xml Field to Bean
     * 
     * @param iEntrepriseCreation
     *            {@link IEntrepriseCreation}
     * @param entrepriseBean
     *            {@link EntrepriseBean}
     */
    private void mapperEntreprisesLieesFusionScission(IEntrepriseCreation iEntrepriseCreation, EntrepriseBean entrepriseBean) {
        entrepriseBean.getEntreprisesLieesFusionScission().clear();
        for (IEntrepriseLieeCreation iEntrLieeFusionScission : iEntrepriseCreation.getEntreprisesLieesFusionScission()) {
            EntrepriseLieeBean entrLieeFusionScissionBean = new EntrepriseLieeBean();

            this.entrepriseLieeMapper.map(iEntrLieeFusionScission, entrLieeFusionScissionBean);
            IAdresse iAdresseEntLieeDom = iEntrLieeFusionScission.getAdresse();
            if (iAdresseEntLieeDom != null) {
                AdresseBean adresseEntLieeFusionScissionBean = entrLieeFusionScissionBean.getAdresse();
                this.adresseMapper.map(iAdresseEntLieeDom, adresseEntLieeFusionScissionBean);
                if (iAdresseEntLieeDom.getCodePostalCommune() != null) {
                    this.postalCommuneMapper.map(iAdresseEntLieeDom.getCodePostalCommune(), adresseEntLieeFusionScissionBean.getCodePostalCommune());
                }
            }
            IAdresse iAdresse = iEntrLieeFusionScission.getAdresse();
            if (iAdresse != null) {
                AdresseBean adresseBean = entrLieeFusionScissionBean.getAdresse();
                this.adresseMapper.map(iAdresse, adresseBean);
                if (iAdresse.getCodePostalCommune() != null) {
                    this.postalCommuneMapper.map(iAdresse.getCodePostalCommune(), adresseBean.getCodePostalCommune());
                }
            }

            entrepriseBean.getEntreprisesLieesFusionScission().add(entrLieeFusionScissionBean);
        }
    }

    /**
     * Mapper Autre établissement situé dans l'Union Européenne Inetrafce Xml
     * Field to Bean
     * 
     * @param iEntrepriseCreation
     *            {@link IEntrepriseCreation}
     * @param entrepriseBean
     *            {@link EntrepriseBean}
     */
    private void mapperAutreEtablissementsUE(IEntrepriseCreation iEntrepriseCreation, EntrepriseBean entrepriseBean) {
        entrepriseBean.getEtablissementsUE().clear();
        for (IAutreEtablissementUECreation iEtablissementUE : iEntrepriseCreation.getEtablissementsUE()) {
            AutreEtablissementUEBean etablissementUEBean = new AutreEtablissementUEBean();
            this.entrepriseLieeMapper.map(iEtablissementUE, etablissementUEBean);
            entrepriseBean.getEtablissementsUE().add(etablissementUEBean);
        }
    }

    /**
     * Mapper regimeFiscalEirl Inetrafce Xml Field to Bean
     * 
     * @param iEntrepriseCreation
     *            {@link IEntrepriseCreation}
     * @param entrepriseBean
     *            {@link EntrepriseBean}
     */
    private void mapperRegimeFiscalEirl(IEntrepriseCreation iEntrepriseCreation, EntrepriseBean entrepriseBean) {
        IRegimeFiscal iRegimeFiscalEirl = iEntrepriseCreation.getRegimeFiscalEirl();
        if (iRegimeFiscalEirl != null) {
            RegimeFiscalBean regimeFiscalEirlBean = entrepriseBean.getRegimeFiscalEirl();
            if (regimeFiscalEirlBean == null) {
                entrepriseBean.setRegimeFiscalEirl(new RegimeFiscalBean());
            }
            this.regimeFiscalMapper.map(iRegimeFiscalEirl, entrepriseBean.getRegimeFiscalEirl());

        }
    }

    /**
     * Mapper regimeFiscalEirlSecondaire Inetrafce Xml Field to Bean
     * 
     * @param iEntrepriseCreation
     *            {@link IEntrepriseCreation}
     * @param entrepriseBean
     *            {@link EntrepriseBean}
     */
    private void mapperRegimeFiscalEirlSecondaire(IEntrepriseCreation iEntrepriseCreation, EntrepriseBean entrepriseBean) {
        IRegimeFiscal iRegimeFiscalEirlSecondaire = iEntrepriseCreation.getRegimeFiscalEirlSecondaire();
        if (iRegimeFiscalEirlSecondaire != null) {
            RegimeFiscalBean regimeFiscalEirlSecondaireBean = entrepriseBean.getRegimeFiscalEirlSecondaire();
            if (regimeFiscalEirlSecondaireBean == null) {
                entrepriseBean.setRegimeFiscalEirlSecondaire(new RegimeFiscalBean());
            }
            this.regimeFiscalMapper.map(iRegimeFiscalEirlSecondaire, entrepriseBean.getRegimeFiscalEirlSecondaire());
        }
    }

    /**
     * Mapper regimeFiscalEtablissement Inetrafce Xml Field to Bean
     * 
     * @param iEntrepriseCreation
     *            {@link IEntrepriseCreation}
     * @param entrepriseBean
     *            {@link EntrepriseBean}
     */
    private void mapperRegimeFiscalEtablissement(IEntrepriseCreation iEntrepriseCreation, EntrepriseBean entrepriseBean) {
        IRegimeFiscal iRegimeFiscalEtablissement = iEntrepriseCreation.getRegimeFiscalEtablissement();
        if (iRegimeFiscalEtablissement != null) {
            RegimeFiscalBean regimeFiscalEtablissementBean = entrepriseBean.getRegimeFiscalEtablissement();
            if (regimeFiscalEtablissementBean == null) {
                entrepriseBean.setRegimeFiscalEtablissement(new RegimeFiscalBean());
            }
            this.regimeFiscalMapper.map(iRegimeFiscalEtablissement, entrepriseBean.getRegimeFiscalEtablissement());
        }
    }

    /**
     * Mapper regimeFiscalEtablissementSecondaire Inetrafce Xml Field to Bean
     * 
     * @param iEntrepriseCreation
     *            {@link IEntrepriseCreation}
     * @param entrepriseBean
     *            {@link EntrepriseBean}
     */
    private void mapperRegimeFiscalEtabSecond(IEntrepriseCreation iEntrepriseCreation, EntrepriseBean entrepriseBean) {
        IRegimeFiscal iRegimeFiscalEirlEtablissementSecondaire = iEntrepriseCreation.getRegimeFiscalEtablissementSecondaire();
        if (iRegimeFiscalEirlEtablissementSecondaire != null) {
            RegimeFiscalBean regimeFiscalEtablissementSecondaireBean = entrepriseBean.getRegimeFiscalEtablissementSecondaire();

            if (regimeFiscalEtablissementSecondaireBean == null) {

                entrepriseBean.setRegimeFiscalEtablissementSecondaire(new RegimeFiscalBean());
            }

            this.regimeFiscalMapper.map(iRegimeFiscalEirlEtablissementSecondaire, entrepriseBean.getRegimeFiscalEtablissementSecondaire());
        }
    }

    /**
     * Mapper Eirl Interface Xml Field To Bean
     * 
     * @param iEntrepriseCreation
     *            {@link IEntrepriseCreation}
     * @param entrepriseBean
     *            {@link EntrepriseBean}
     */
    private void mapperEirl(IEntrepriseCreation iEntrepriseCreation, EntrepriseBean entrepriseBean) {
        if (entrepriseBean.getEirl() == null) {
            entrepriseBean.setEirl(new EirlBean());
        }
        if (iEntrepriseCreation.getEirl() != null) {
            this.eirlMapper.map(iEntrepriseCreation.getEirl(), entrepriseBean.getEirl());

            if (null != iEntrepriseCreation.getEirl().getDefinitionPatrimoine()) {
                entrepriseBean.getEirl().getDefinitionPatrimoine().clear();
                XmlString[] modifDefinitionPatrimoines = iEntrepriseCreation.getEirl().getDefinitionPatrimoine();
                for (XmlString modifDefinitionPatrimoine : modifDefinitionPatrimoines) {
                    entrepriseBean.getEirl().getDefinitionPatrimoine().add(modifDefinitionPatrimoine.getString());
                }
            }
        }
    }

    /**
     * Mapper dirigeants Interface Xml Field To Bean
     * 
     * @param iEntrepriseCreation
     *            {@link IEntrepriseCreation}
     * @param entrepriseBean
     *            {@link EntrepriseBean}
     */
    private void mapperDirigeants(IEntrepriseCreation iEntrepriseCreation, EntrepriseBean entrepriseBean) {
        entrepriseBean.getDirigeants().clear();
        for (IDirigeantCreation iDirigeantCreation : iEntrepriseCreation.getDirigeants()) {

            DirigeantBean dirigeantBean = new DirigeantBean();
            this.dirigeantMapper.map(iDirigeantCreation, dirigeantBean);

            IConjointCreation iConjointR = iDirigeantCreation.getConjoint();
            if (iConjointR != null) {
                if (dirigeantBean.getConjoint() == null) {
                    dirigeantBean.setConjoint(new ConjointBean());
                }
                this.conjointMapper.map(iConjointR, dirigeantBean.getConjoint());
            }
            IAdresse iPpAdresse = iDirigeantCreation.getPpAdresse();
            if (iPpAdresse != null) {
                AdresseBean ppAdresseBean = dirigeantBean.getPpAdresse();
                this.adresseMapper.map(iPpAdresse, ppAdresseBean);
                if (iPpAdresse.getCodePostalCommune() != null) {
                    this.postalCommuneMapper.map(iPpAdresse.getCodePostalCommune(), ppAdresseBean.getCodePostalCommune());
                }
            }
            if (iDirigeantCreation.getPpAdresseAmbulant() != null) {
                this.postalCommuneMapper.map(iDirigeantCreation.getPpAdresseAmbulant(), dirigeantBean.getPpAdresseAmbulant());
            }
            if (iDirigeantCreation.getPpAdresseForain() != null) {
                this.postalCommuneMapper.map(iDirigeantCreation.getPpAdresseForain(), dirigeantBean.getPpAdresseForain());
            }
            IAdresse iPmAdresse = iDirigeantCreation.getPmAdresse();
            if (iPmAdresse != null) {
                AdresseBean pmAdresseBean = dirigeantBean.getPmAdresse();
                this.adresseMapper.map(iPmAdresse, pmAdresseBean);
                if (iPmAdresse.getCodePostalCommune() != null) {
                    this.postalCommuneMapper.map(iPmAdresse.getCodePostalCommune(), pmAdresseBean.getCodePostalCommune());
                }
            }

            IAdresse iPmRepresentantAdresse = iDirigeantCreation.getPmRepresentantAdresse();
            if (iPmRepresentantAdresse != null) {
                AdresseBean pmRepresentantAdresse = dirigeantBean.getPmRepresentantAdresse();
                this.adresseMapper.map(iPmRepresentantAdresse, pmRepresentantAdresse);
                if (iPmRepresentantAdresse.getCodePostalCommune() != null) {
                    this.postalCommuneMapper.map(iPmRepresentantAdresse.getCodePostalCommune(), pmRepresentantAdresse.getCodePostalCommune());
                }
            }

            // Mapper de déclation sociale création
            IDeclarationSociale iDeclarationSociale = iDirigeantCreation.getDeclarationSociale();
            if (iDeclarationSociale != null) {
                if (dirigeantBean.getDeclarationSociale() == null) {
                    dirigeantBean.setDeclarationSociale(new DeclarationSocialeBean());
                }

                /* mapper la liste des ayants droit */

                if (dirigeantBean.getDeclarationSociale().getAyantDroits() != null) {

                    dirigeantBean.getDeclarationSociale().getAyantDroits().clear();
                    IAyantDroit[] iayantsDroit = iDirigeantCreation.getDeclarationSociale().getAyantDroits();

                    for (IAyantDroit iayantDroit : iayantsDroit) {

                        AyantDroitBean ayantDroitBean = new AyantDroitBean();

                        this.ayantDroitMapper.map(iayantDroit, ayantDroitBean);
                        ayantDroitBean.getAyantDroits().clear();
                        IAyantDroit[] iayantsDroitIayantDroit = iayantDroit.getAyantDroits();

                        for (IAyantDroit iayantDroitIyantDroit : iayantsDroitIayantDroit) {
                            AyantDroitBean ayantsDroitAyantsDroitBean = new AyantDroitBean();
                            this.ayantDroitMapper.map(iayantDroitIyantDroit, ayantsDroitAyantsDroitBean);

                            ayantDroitBean.getAyantDroits().add(ayantsDroitAyantsDroitBean);
                        }

                        dirigeantBean.getDeclarationSociale().getAyantDroits().add(ayantDroitBean);
                    }
                }

                this.declarationSocialeMapper.map(iDirigeantCreation.getDeclarationSociale(), dirigeantBean.getDeclarationSociale());
            }

            entrepriseBean.getDirigeants().add(dirigeantBean);

        }
    }

    /**
     * Mapper personneLiee Interface Xml Field To Bean
     * 
     * @param iEntrepriseCreation
     *            {@link IEntrepriseCreation}
     * @param entrepriseBean
     *            {@link EntrepriseBean}
     */
    private void mapperPersonnesLiees(IEntrepriseCreation iEntrepriseCreation, EntrepriseBean entrepriseBean) {
        entrepriseBean.getPersonneLiee().clear();
        for (IPersonneLieeCreation iPersLiee : iEntrepriseCreation.getPersonneLiee()) {
            PersonneLieeBean persLieeBean = new PersonneLieeBean();
            this.personneLieeMapper.map(iPersLiee, persLieeBean);
            entrepriseBean.getPersonneLiee().add(persLieeBean);
        }
    }

    /**
     * Mapper la lise de CapitalApportsNature de {@link IEntrepriseCreation} à
     * {@link EntrepriseBean}
     * 
     * @param iEntrepriseCreation
     *            {@link IEntrepriseCreation}
     * @param entrepriseBean
     *            {@link EntrepriseBean}
     */
    private void mapperCapitalApportsNature(IEntrepriseCreation iEntrepriseCreation, EntrepriseBean entrepriseBean) {
        if (iEntrepriseCreation.getCapitalApportsNature() != null) {
            if (entrepriseBean.getCapitalApportsNature() != null) {
                entrepriseBean.getCapitalApportsNature().clear();
            } else {
                entrepriseBean.setCapitalApportsNature(new ArrayList<String>());
            }
            XmlString[] capitalApportsNatureI = iEntrepriseCreation.getCapitalApportsNature();
            for (XmlString capitalApportNatureI : capitalApportsNatureI) {
                entrepriseBean.getCapitalApportsNature().add(capitalApportNatureI.getString());
            }
        }
    }

}
