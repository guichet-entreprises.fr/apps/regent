/**
 * 
 */
package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Interface P90.2.
 *
 * @author $Author: aboidard $
 * @version $Revision: 0 $
 */
@ResourceXPath("/P90.2")
public interface IP902 {

  /**
   * Accesseur sur l'attribut {@link #P9021}.
   *
   * @return P9021
   */
  @FieldXPath("P90.2.1")
  String getP9021();

  /**
   * Mutateur sur l'attribut {@link #P9021}.
   *
   * @param P9021
   *          la nouvelle valeur de l'attribut P9021
   */
  void setP9021(String P9021);

  /**
   * Accesseur sur l'attribut {@link #P9022}.
   *
   * @return P9022
   */
  @FieldXPath("P90.2.2")
  String getP9022();

  /**
   * Mutateur sur l'attribut {@link #P9022}.
   *
   * @param p9022
   *          la nouvelle valeur de l'attribut P9022
   */
  void setP9022(String p9022);

}
