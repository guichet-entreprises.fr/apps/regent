package fr.ge.common.regent.bean.formalite.dialogue.creation;

import fr.ge.common.regent.bean.formalite.FormaliteVueBean;

/**
 * Le Class RegimeFiscalBean.
 */
public class RegimeFiscalBean extends FormaliteVueBean implements IFormaliteVue {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 5187340394555234077L;

    /** Le date cloture exercice comptable. */
    private String dateClotureExerciceComptable;

    /** Le enregistrement prealable statuts. */
    private String enregistrementPrealableStatuts;

    /** Le enregistrement prealable statuts date. */
    private String enregistrementPrealableStatutsDate;

    /** Le enregistrement prealable statuts lieu. */
    private String enregistrementPrealableStatutsLieu;

    /** Le regime imposition benefices. */
    private String regimeImpositionBenefices;

    /** Le regime imposition benefices options particulieres. */
    private String regimeImpositionBeneficesOptionsParticulieres;

    /** Le regime imposition benefices options particulieres booleen. */
    private String regimeImpositionBeneficesOptionsParticulieresBooleen;

    /** Le regime imposition benefices versement liberatoire. */
    private String regimeImpositionBeneficesVersementLiberatoire;

    /** Le regime imposition tva. */
    private String regimeImpositionTVA;

    /** Le regime imposition tva condition versement. */
    private String regimeImpositionTVAConditionVersement;

    /** Le regime imposition tva options particulieres1. */
    private String regimeImpositionTVAOptionsParticulieres1;

    /** Le regime imposition tva options particulieres2. */
    private String regimeImpositionTVAOptionsParticulieres2;

    /** Le regime imposition tva options particulieres3. */
    private String regimeImpositionTVAOptionsParticulieres3;

    /** Le regime imposition tva options particulieres4. */
    private String regimeImpositionTVAOptionsParticulieres4;

    /** Le type regime. */
    private String typeRegime;

    /**
     * Getter de l'attribut dateClotureExerciceComptable.
     * 
     * @return la valeur de dateClotureExerciceComptable
     */
    public String getDateClotureExerciceComptable() {
        return this.dateClotureExerciceComptable;
    }

    /**
     * Getter de l'attribut enregistrementPrealableStatuts.
     * 
     * @return la valeur de enregistrementPrealableStatuts
     */
    public String getEnregistrementPrealableStatuts() {
        return this.enregistrementPrealableStatuts;
    }

    /**
     * Getter de l'attribut enregistrementPrealableStatutsDate.
     * 
     * @return la valeur de enregistrementPrealableStatutsDate
     */
    public String getEnregistrementPrealableStatutsDate() {
        return this.enregistrementPrealableStatutsDate;
    }

    /**
     * Getter de l'attribut enregistrementPrealableStatutsLieu.
     * 
     * @return la valeur de enregistrementPrealableStatutsLieu
     */
    public String getEnregistrementPrealableStatutsLieu() {
        return this.enregistrementPrealableStatutsLieu;
    }

    /**
     * Getter de l'attribut regimeImpositionBenefices.
     * 
     * @return la valeur de regimeImpositionBenefices
     */
    public String getRegimeImpositionBenefices() {
        return this.regimeImpositionBenefices;
    }

    /**
     * Getter de l'attribut regimeImpositionBeneficesOptionsParticulieres.
     * 
     * @return la valeur de regimeImpositionBeneficesOptionsParticulieres
     */
    public String getRegimeImpositionBeneficesOptionsParticulieres() {
        return this.regimeImpositionBeneficesOptionsParticulieres;
    }

    /**
     * Getter de l'attribut
     * regimeImpositionBeneficesOptionsParticulieresBooleen.
     * 
     * @return la valeur de regimeImpositionBeneficesOptionsParticulieresBooleen
     */
    public String getRegimeImpositionBeneficesOptionsParticulieresBooleen() {
        return this.regimeImpositionBeneficesOptionsParticulieresBooleen;
    }

    /**
     * Getter de l'attribut regimeImpositionBeneficesVersementLiberatoire.
     * 
     * @return la valeur de regimeImpositionBeneficesVersementLiberatoire
     */
    public String getRegimeImpositionBeneficesVersementLiberatoire() {
        return this.regimeImpositionBeneficesVersementLiberatoire;
    }

    /**
     * Getter de l'attribut regimeImpositionTVA.
     * 
     * @return la valeur de regimeImpositionTVA
     */
    public String getRegimeImpositionTVA() {
        return this.regimeImpositionTVA;
    }

    /**
     * Getter de l'attribut regimeImpositionTVAConditionVersement.
     * 
     * @return la valeur de regimeImpositionTVAConditionVersement
     */
    public String getRegimeImpositionTVAConditionVersement() {
        return this.regimeImpositionTVAConditionVersement;
    }

    /**
     * Getter de l'attribut regimeImpositionTVAOptionsParticulieres1.
     * 
     * @return la valeur de regimeImpositionTVAOptionsParticulieres1
     */
    public String getRegimeImpositionTVAOptionsParticulieres1() {
        return this.regimeImpositionTVAOptionsParticulieres1;
    }

    /**
     * Getter de l'attribut regimeImpositionTVAOptionsParticulieres2.
     * 
     * @return la valeur de regimeImpositionTVAOptionsParticulieres2
     */
    public String getRegimeImpositionTVAOptionsParticulieres2() {
        return this.regimeImpositionTVAOptionsParticulieres2;
    }

    /**
     * Getter de l'attribut regimeImpositionTVAOptionsParticulieres3.
     * 
     * @return la valeur de regimeImpositionTVAOptionsParticulieres3
     */
    public String getRegimeImpositionTVAOptionsParticulieres3() {
        return this.regimeImpositionTVAOptionsParticulieres3;
    }

    /**
     * Getter de l'attribut regimeImpositionTVAOptionsParticulieres4.
     * 
     * @return la valeur de regimeImpositionTVAOptionsParticulieres4
     */
    public String getRegimeImpositionTVAOptionsParticulieres4() {
        return this.regimeImpositionTVAOptionsParticulieres4;
    }

    /**
     * Getter de l'attribut typeRegime.
     * 
     * @return la valeur de typeRegime
     */
    public String getTypeRegime() {
        return this.typeRegime;
    }

    /**
     * Setter de l'attribut dateClotureExerciceComptable.
     * 
     * @param dateClotureExerciceComptable
     *            la nouvelle valeur de dateClotureExerciceComptable
     */
    public void setDateClotureExerciceComptable(String dateClotureExerciceComptable) {
        this.dateClotureExerciceComptable = dateClotureExerciceComptable;
    }

    /**
     * Setter de l'attribut enregistrementPrealableStatuts.
     * 
     * @param enregistrementPrealableStatuts
     *            la nouvelle valeur de enregistrementPrealableStatuts
     */
    public void setEnregistrementPrealableStatuts(String enregistrementPrealableStatuts) {
        this.enregistrementPrealableStatuts = enregistrementPrealableStatuts;
    }

    /**
     * Setter de l'attribut enregistrementPrealableStatutsDate.
     * 
     * @param enregistrementPrealableStatutsDate
     *            la nouvelle valeur de enregistrementPrealableStatutsDate
     */
    public void setEnregistrementPrealableStatutsDate(String enregistrementPrealableStatutsDate) {
        this.enregistrementPrealableStatutsDate = enregistrementPrealableStatutsDate;
    }

    /**
     * Setter de l'attribut enregistrementPrealableStatutsLieu.
     * 
     * @param enregistrementPrealableStatutsLieu
     *            la nouvelle valeur de enregistrementPrealableStatutsLieu
     */
    public void setEnregistrementPrealableStatutsLieu(String enregistrementPrealableStatutsLieu) {
        this.enregistrementPrealableStatutsLieu = enregistrementPrealableStatutsLieu;
    }

    /**
     * Setter de l'attribut regimeImpositionBenefices.
     * 
     * @param regimeImpositionBenefices
     *            la nouvelle valeur de regimeImpositionBenefices
     */
    public void setRegimeImpositionBenefices(String regimeImpositionBenefices) {
        this.regimeImpositionBenefices = regimeImpositionBenefices;
    }

    /**
     * Setter de l'attribut regimeImpositionBeneficesOptionsParticulieres.
     * 
     * @param regimeImpositionBeneficesOptionsParticulieres
     *            la nouvelle valeur de
     *            regimeImpositionBeneficesOptionsParticulieres
     */
    public void setRegimeImpositionBeneficesOptionsParticulieres(String regimeImpositionBeneficesOptionsParticulieres) {
        this.regimeImpositionBeneficesOptionsParticulieres = regimeImpositionBeneficesOptionsParticulieres;
    }

    /**
     * Setter de l'attribut
     * regimeImpositionBeneficesOptionsParticulieresBooleen.
     *
     * @param regimeImpositionBeneficesOptionsParticulieresBooleen
     *            le nouveau regime imposition benefices options particulieres
     *            booleen
     */
    public void setRegimeImpositionBeneficesOptionsParticulieresBooleen(String regimeImpositionBeneficesOptionsParticulieresBooleen) {
        this.regimeImpositionBeneficesOptionsParticulieresBooleen = regimeImpositionBeneficesOptionsParticulieresBooleen;
    }

    /**
     * Setter de l'attribut regimeImpositionBeneficesVersementLiberatoire.
     * 
     * @param regimeImpositionBeneficesVersementLiberatoire
     *            la nouvelle valeur de
     *            regimeImpositionBeneficesVersementLiberatoire
     */
    public void setRegimeImpositionBeneficesVersementLiberatoire(String regimeImpositionBeneficesVersementLiberatoire) {
        this.regimeImpositionBeneficesVersementLiberatoire = regimeImpositionBeneficesVersementLiberatoire;
    }

    /**
     * Setter de l'attribut regimeImpositionTVA.
     * 
     * @param regimeImpositionTVA
     *            la nouvelle valeur de regimeImpositionTVA
     */
    public void setRegimeImpositionTVA(String regimeImpositionTVA) {
        this.regimeImpositionTVA = regimeImpositionTVA;
    }

    /**
     * Setter de l'attribut regimeImpositionTVAConditionVersement.
     * 
     * @param regimeImpositionTVAConditionVersement
     *            la nouvelle valeur de regimeImpositionTVAConditionVersement
     */
    public void setRegimeImpositionTVAConditionVersement(String regimeImpositionTVAConditionVersement) {
        this.regimeImpositionTVAConditionVersement = regimeImpositionTVAConditionVersement;
    }

    /**
     * Setter de l'attribut regimeImpositionTVAOptionsParticulieres1.
     * 
     * @param regimeImpositionTVAOptionsParticulieres1
     *            la nouvelle valeur de regimeImpositionTVAOptionsParticulieres1
     */
    public void setRegimeImpositionTVAOptionsParticulieres1(String regimeImpositionTVAOptionsParticulieres1) {
        this.regimeImpositionTVAOptionsParticulieres1 = regimeImpositionTVAOptionsParticulieres1;
    }

    /**
     * Setter de l'attribut regimeImpositionTVAOptionsParticulieres2.
     * 
     * @param regimeImpositionTVAOptionsParticulieres2
     *            la nouvelle valeur de regimeImpositionTVAOptionsParticulieres2
     */
    public void setRegimeImpositionTVAOptionsParticulieres2(String regimeImpositionTVAOptionsParticulieres2) {
        this.regimeImpositionTVAOptionsParticulieres2 = regimeImpositionTVAOptionsParticulieres2;
    }

    /**
     * Setter de l'attribut regimeImpositionTVAOptionsParticulieres3.
     * 
     * @param regimeImpositionTVAOptionsParticulieres3
     *            la nouvelle valeur de regimeImpositionTVAOptionsParticulieres3
     */
    public void setRegimeImpositionTVAOptionsParticulieres3(String regimeImpositionTVAOptionsParticulieres3) {
        this.regimeImpositionTVAOptionsParticulieres3 = regimeImpositionTVAOptionsParticulieres3;
    }

    /**
     * Setter de l'attribut regimeImpositionTVAOptionsParticulieres4.
     * 
     * @param regimeImpositionTVAOptionsParticulieres4
     *            la nouvelle valeur de regimeImpositionTVAOptionsParticulieres4
     */
    public void setRegimeImpositionTVAOptionsParticulieres4(String regimeImpositionTVAOptionsParticulieres4) {
        this.regimeImpositionTVAOptionsParticulieres4 = regimeImpositionTVAOptionsParticulieres4;
    }

    /**
     * Setter de l'attribut typeRegime.
     * 
     * @param typeRegime
     *            la nouvelle valeur de typeRegime
     */
    public void setTypeRegime(String typeRegime) {
        this.typeRegime = typeRegime;
    }

}
