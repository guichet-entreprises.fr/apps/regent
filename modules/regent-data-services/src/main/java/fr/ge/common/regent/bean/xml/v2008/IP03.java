package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IP03.
 */
@ResourceXPath("/P03")
public interface IP03 {

  /**
   * Get le p031.
   *
   * @return le p031
   */
  @FieldXPath("P03.1")
  String getP031();

  /**
   * Set le p031.
   *
   * @param P031
   *          le nouveau p031
   */
  void setP031(String P031);

  /**
   * Get le p032.
   *
   * @return le p032
   */
  @FieldXPath("P03.2")
  String getP032();

  /**
   * Set le p032.
   *
   * @param P032
   *          le nouveau p032
   */
  void setP032(String P032);

  /**
   * Get le p033.
   *
   * @return le p033
   */
  @FieldXPath("P03.3")
  String getP033();

  /**
   * Set le p033.
   *
   * @param P033
   *          le nouveau p033
   */
  void setP033(String P033);

  /**
   * Get le p034.
   *
   * @return le p034
   */
  @FieldXPath("P03.4")
  String getP034();

  /**
   * Set le p034.
   *
   * @param P034
   *          le nouveau p034
   */
  void setP034(String P034);

}
