package fr.ge.common.regent.bean.formalite.dialogue.regularisation;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import fr.ge.common.regent.bean.formalite.dialogue.AbstractEntrepriseLieeBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.AdresseBean;

/**
 * Le Class EntrepriseLieeBean.
 *
 * @author roveda
 */
public class EntrepriseLieeBean extends AbstractEntrepriseLieeBean {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -8811514009462322002L;

    /** Le id technique. */
    private String idTechnique;

    /** Le modif date adresse2. */
    private String modifDateAdresse2;

    /** Le modif ancienne adresse2. */
    private AdresseBean modifAncienneAdresse2 = new AdresseBean();

    /**
     * Get le id technique.
     *
     * @return the idTechnique
     */
    public String getIdTechnique() {
        return idTechnique;
    }

    /**
     * Set le id technique.
     *
     * @param idTechnique
     *            the idTechnique to set
     */
    public void setIdTechnique(String idTechnique) {
        this.idTechnique = idTechnique;
    }

    /**
     * Get le modif date adresse2.
     *
     * @return the modifDateAdresse2
     */
    public String getModifDateAdresse2() {
        return modifDateAdresse2;
    }

    /**
     * Set le modif date adresse2.
     *
     * @param modifDateAdresse2
     *            the modifDateAdresse2 to set
     */
    public void setModifDateAdresse2(String modifDateAdresse2) {
        this.modifDateAdresse2 = modifDateAdresse2;
    }

    /**
     * Get le modif ancienne adresse2.
     *
     * @return the modifAncienneAdresse2
     */
    public AdresseBean getModifAncienneAdresse2() {
        return modifAncienneAdresse2;
    }

    /**
     * Set le modif ancienne adresse2.
     *
     * @param modifAncienneAdresse2
     *            the modifAncienneAdresse2 to set
     */
    public void setModifAncienneAdresse2(AdresseBean modifAncienneAdresse2) {
        this.modifAncienneAdresse2 = modifAncienneAdresse2;
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see fr.guichetentreprises.vue.AbstractEntrepriseLieeBean#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

}
