package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Interface IGDR.
 */
@ResourceXPath("/GDR")
public interface IGDR {

  /**
   * Get le diu.
   *
   * @return le diu
   */
  @FieldXPath("DIU")
  IDIU[] getDIU();

  /**
   * Ajoute le to diu.
   *
   * @return le diu
   */
  IDIU addToDIU();

  /**
   * Set le diu.
   *
   * @param DIU
   *          le nouveau diu
   */
  void setDIU(IDIU[] DIU);

  /**
   * Get le idu.
   *
   * @return le idu
   */
  @FieldXPath("IDU")
  IIDU[] getIDU();

  /**
   * Ajoute le to idu.
   *
   * @return le idu
   */
  IIDU addToIDU();

  /**
   * Set le idu.
   *
   * @param IDU
   *          le nouveau idu
   */
  void setIDU(IIDU[] IDU);
}
