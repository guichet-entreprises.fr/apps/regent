package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IE41.
 */
@ResourceXPath("/E41")
public interface IE41 {

  /**
   * Get le e411.
   *
   * @return le e411
   */
  @FieldXPath("E41.1")
  String getE411();

  /**
   * Set le e411.
   *
   * @param E411
   *          le nouveau e411
   */
  void setE411(String E411);

  /**
   * Get le e412.
   *
   * @return le e412
   */
  @FieldXPath("E41.2")
  String getE412();

  /**
   * Set le e412.
   *
   * @param E412
   *          le nouveau e412
   */
  void setE412(String E412);

}
