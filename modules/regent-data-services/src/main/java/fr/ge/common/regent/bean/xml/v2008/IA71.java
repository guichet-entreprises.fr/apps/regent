package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Interface IA10.
 */
@ResourceXPath("/A71")
public interface IA71 {

  /**
   * Get le A71.6.
   *
   * @return le A71.6
   */
  @FieldXPath("A71.6")
  String getAA716();

  /**
   * Set le A71.6.
   *
   * @param A71
   *          .6 le nouveau A71.6
   */
  void setA716(String A716);

  /**
   * Get le A71.7.
   *
   * @return le A71.7
   */
  @FieldXPath("A71.7")
  String getAA717();

  /**
   * Set le A71.7.
   *
   * @param A71
   *          .7 le nouveau A71.7
   */
  void setA717(String A717);

  /**
   * Get le A71.8.
   *
   * @return le A71.8
   */
  @FieldXPath("A71.8")
  String getAA718();

  /**
   * Set le A71.8.
   *
   * @param A71
   *          .8 le nouveau A71.8
   */
  void setA718(String A718);

  /**
   * Get le A71.10.
   *
   * @return le A71.10
   */
  @FieldXPath("A71.10")
  String getAA7110();

  /**
   * Set le A71.10.
   *
   * @param A71
   *          .10 le nouveau A71.10
   */
  void setA7110(String A7110);

  /**
   * Get le A71.11.
   *
   * @return le A71.11
   */
  @FieldXPath("A71.11")
  String getAA7111();

  /**
   * Set le A71.11.
   *
   * @param A71
   *          .11 le nouveau A71.11
   */
  void setA7111(String A7111);

  /**
   * Get le A71.11.
   *
   * @return le A71.11
   */
  @FieldXPath("A71.12")
  String getAA7112();

  /**
   * Set le A71.12.
   *
   * @param A71
   *          .12 le nouveau A71.12
   */
  void setA7112(String A7112);

  /**
   * Get le A71.13.
   *
   * @return le A71.13
   */
  @FieldXPath("A71.13")
  String getAA7113();

  /**
   * Set le A71.13.
   *
   * @param A7113
   *          .13 le nouveau A71.13
   */
  void setA7113(String A7113);

  /**
   * Get le A71.14.
   *
   * @return le A71.14
   */
  @FieldXPath("A71.14")
  String getAA7114();

  /**
   * Set le A71.14.
   *
   * @param A7114
   *          .14 le nouveau A71.14
   */
  void setA7114(String A7114);

}
