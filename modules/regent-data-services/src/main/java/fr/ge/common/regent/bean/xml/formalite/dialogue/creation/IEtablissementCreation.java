package fr.ge.common.regent.bean.xml.formalite.dialogue.creation;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;
import org.xmlfield.core.types.XmlString;

import fr.ge.common.regent.bean.xml.formalite.IFormalite;
import fr.ge.common.regent.bean.xml.transverse.IAdresse;

/**
 * Le Interface de l'entité Etablissement de l'entreprise
 * 
 * 
 * 
 */
@ResourceXPath("/formalite")
public interface IEtablissementCreation extends IFormalite {

    /**
     * Getter de l'attribut activiteElevage.
     * 
     * @return la valeur de activiteElevage
     */
    @FieldXPath(value = "activiteElevage")
    String getActiviteElevage();

    /**
     * Accesseur sur l'attribut activite plus importante.
     *
     * @return activite plus importante
     */
    @FieldXPath(value = "activitePlusImportante")
    String getActivitePlusImportante();

    /**
     * Getter de l'attribut activiteImmobiliere.
     * 
     * @return la valeur de activiteImmobiliere
     */
    @FieldXPath(value = "activiteImmobiliere")
    String getActiviteImmobiliere();

    /**
     * Getter de l'attribut activiteLieuExercice.
     * 
     * @return la valeur de activiteLieuExercice
     */
    @FieldXPath(value = "activiteLieuExercice")
    String getActiviteLieuExercice();

    /**
     * Getter de l'attribut activiteLieuExerciceAutre.
     * 
     * @return la valeur de activiteLieuExerciceAutre
     */
    @FieldXPath(value = "activiteLieuExerciceAutre")
    String getActiviteLieuExerciceAutre();

    /**
     * Getter de l'attribut activitePlusImportanteAgricole1.
     * 
     * @return la valeur de activitePlusImportanteAgricole1
     */
    @FieldXPath(value = "activitePlusImportanteAgricole1")
    String getActivitePlusImportanteAgricole1();

    /**
     * Getter de l'attribut activitePlusImportanteAgricole2.
     * 
     * @return la valeur de activitePlusImportanteAgricole2
     */
    @FieldXPath(value = "activitePlusImportanteAgricole2")
    String getActivitePlusImportanteAgricole2();

    /**
     * Getter de l'attribut activitesAccessoiresBICBNC.
     * 
     * @return la valeur de activitesAccessoiresBICBNC
     */
    @FieldXPath(value = "activitesAccessoiresBICBNC")
    String getActivitesAccessoiresBICBNC();

    /**
     * Getter de l'attribut activitesExerceesAgricole.
     * 
     * @return la valeur de activitesExerceesAgricole
     */
    @FieldXPath(value = "activitesExerceesAgricole")
    XmlString[] getActivitesExerceesAgricole();

    /**
     * Getter de l'attribut nombreActivitesSoumisesQualification.
     * 
     * @return la valeur de nombreActivitesSoumisesQualification
     */
    @FieldXPath(value = "nombreActivitesSoumisesQualification")
    Integer getNombreActivitesSoumisesQualification();

    /**
     * Setteur nombreActivitesSoumisesQualification.
     * 
     * @param nombreActivitesSoumisesQualification
     *            nombre des Activivites Soumises à Qualification
     */
    void setNombreActivitesSoumisesQualification(Integer nombreActivitesSoumisesQualification);

    /**
     * Retire le from definitionPatrimoine.
     *
     * @param activitesExerceesAgricole
     *            le definitionPatrimoine
     */
    void removeFromActivitesExerceesAgricole(XmlString activitesExerceesAgricole);

    /**
     * Ajoute un indice à la liste des ActivitesExerceesAgricole.
     *
     * @return XmlString
     */
    XmlString addToActivitesExerceesAgricole();

    /**
     * Getter de l'attribut activiteViticole.
     * 
     * @return la valeur de activiteViticole
     */
    @FieldXPath(value = "activiteViticole")
    String getActiviteViticole();

    /**
     * Getter de l'attribut effectifSalarieAgricoleNombre.
     * 
     * @return la valeur de effectifSalarieAgricoleNombre
     */
    @FieldXPath(value = "effectifSalarieAgricoleNombre")
    Integer getEffectifSalarieAgricoleNombre();

    /**
     * Getter de l'attribut effectifSalarieEmbauchePremierSalarie.
     * 
     * @return la valeur de effectifSalarieEmbauchePremierSalarie
     */
    @FieldXPath(value = "effectifSalarieEmbauchePremierSalarie")
    String getEffectifSalarieEmbauchePremierSalarie();

    /**
     * Getter de l'attribut estActiviteLiberale.
     * 
     * @return la valeur de estActiviteLiberale
     */
    @FieldXPath(value = "estActiviteLiberale")
    String getEstActiviteLiberale();

    /**
     * Getter de l'attribut periodeActiviteSaisonniere1DateDebut.
     * 
     * @return la valeur de periodeActiviteSaisonniere1DateDebut
     */
    @FieldXPath(value = "periodeActiviteSaisonniere1DateDebut")
    String getPeriodeActiviteSaisonniere1DateDebut();

    /**
     * Getter de l'attribut periodeActiviteSaisonniere1DateFin.
     * 
     * @return la valeur de periodeActiviteSaisonniere1DateFin
     */
    @FieldXPath(value = "periodeActiviteSaisonniere1DateFin")
    String getPeriodeActiviteSaisonniere1DateFin();

    /**
     * Getter de l'attribut periodeActiviteSaisonniere2DateDebut.
     * 
     * 
     * @return la valeur de periodeActiviteSaisonniere2DateDebut
     */
    @FieldXPath(value = "periodeActiviteSaisonniere2DateDebut")
    String getPeriodeActiviteSaisonniere2DateDebut();

    /**
     * Getter de l'attribut periodeActiviteSaisonniere2DateFin.
     * 
     * @return la valeur de periodeActiviteSaisonniere2DateFin
     */
    @FieldXPath(value = "periodeActiviteSaisonniere2DateFin")
    String getPeriodeActiviteSaisonniere2DateFin();

    /**
     * Getter de l'attribut precedentExploitantNombre.
     * 
     * @return la valeur de precedentExploitantNombre
     */
    @FieldXPath(value = "precedentExploitantNombre")
    Integer getPrecedentExploitantNombre();

    /**
     * Getter de l'attribut secteurActivitePlusImportant.
     * 
     * @return la valeur de secteurActivitePlusImportant
     */
    @FieldXPath(value = "secteurActivitePlusImportant")
    String getSecteurActivitePlusImportant();

    /**
     * Getter de l'attribut secteursActivites.
     * 
     * @return la valeur de secteursActivites
     */
    @FieldXPath(value = "secteursActivites")
    String getSecteursActivites();

    /**
     * Get le domiciliation.
     *
     * @return the domiciliation
     */
    @FieldXPath(value = "domiciliation")
    String getDomiciliation();

    /**
     * Get le activites.
     *
     * @return the activites
     */
    @FieldXPath(value = "activites")
    String getActivites();

    /**
     * Set le domiciliation.
     *
     * @param domiciliation
     *            the domiciliation to set
     */
    void setDomiciliation(String domiciliation);

    /**
     * Get le adresse.
     *
     * @return le adresse
     */
    @FieldXPath(value = "adresse")
    IAdresse getAdresse();

    /**
     * New adresse.
     *
     * @return le i adresse
     */
    IAdresse newAdresse();

    /**
     * Set le adresse.
     *
     * @param adresse
     *            le nouveau adresse
     */
    void setAdresse(IAdresse adresse);

    /**
     * Get le date debut activite.
     *
     * @return the dateDebutActivite
     */
    @FieldXPath(value = "dateDebutActivite")
    String getDateDebutActivite();

    /**
     * Set le date debut activite.
     *
     * @param dateDebutActivite
     *            the dateDebutActivite to set
     */
    void setDateDebutActivite(String dateDebutActivite);

    /**
     * Get le activite permanente saisonniere.
     *
     * @return the activitePermanenteSaisonniere
     */
    @FieldXPath(value = "activitePermanenteSaisonniere")
    String getActivitePermanenteSaisonniere();

    /**
     * Set le activite permanente saisonniere.
     *
     * @param activitePermanenteSaisonniere
     *            the activitePermanenteSaisonniere to set
     */
    void setActivitePermanenteSaisonniere(String activitePermanenteSaisonniere);

    /**
     * Get le non sedentarite qualite.
     *
     * @return the nonSedentariteQualite
     */
    @FieldXPath(value = "nonSedentariteQualite")
    String getNonSedentariteQualite();

    /**
     * Set le non sedentarite qualite.
     *
     * @param nonSedentariteQualite
     *            the nonSedentariteQualite to set
     */
    void setNonSedentariteQualite(String nonSedentariteQualite);

    /**
     * Get le est ambulant ue.
     *
     * @return the estAmbulantUE
     */
    @FieldXPath(value = "estAmbulantUE")
    String getEstAmbulantUE();

    /**
     * Set le est ambulant ue.
     *
     * @param estAmbulantUE
     *            the estAmbulantUE to set
     */
    void setEstAmbulantUE(String estAmbulantUE);

    /**
     * Get le aqpa situation.
     *
     * @return the aqpaSituation
     */
    @FieldXPath(value = "aqpaSituation")
    String getAqpaSituation();

    /**
     * Set le aqpa situation.
     *
     * @param aqpaSituation
     *            the aqpaSituation to set
     */
    void setAqpaSituation(String aqpaSituation);

    /**
     * Get le aqpa intitule diplome.
     *
     * @return the aqpaIntituleDiplome
     */
    @FieldXPath(value = "aqpaIntituleDiplome")
    String getAqpaIntituleDiplome();

    /**
     * Set le aqpa intitule diplome.
     *
     * @param aqpaIntituleDiplome
     *            the aqpaIntituleDiplome to set
     */
    void setAqpaIntituleDiplome(String aqpaIntituleDiplome);

    /**
     * Get le activite nature.
     *
     * @return the activiteNature
     */
    @FieldXPath(value = "activiteNature")
    String getActiviteNature();

    /**
     * Set le activite nature.
     *
     * @param activiteNature
     *            the activiteNature to set
     */

    void setActiviteNature(String activiteNature);

    /**
     * Get le activite nature autre.
     *
     * @return the activiteNatureAutre
     */
    @FieldXPath(value = "activiteNatureAutre")
    String getActiviteNatureAutre();

    /**
     * Set le activite nature autre.
     *
     * @param activiteNatureAutre
     *            the activiteNatureAutre to set
     */
    void setActiviteNatureAutre(String activiteNatureAutre);

    /**
     * Get le activite lieu exercice magasin.
     *
     * @return the activiteLieuExerciceMagasin
     */
    @FieldXPath(value = "activiteLieuExerciceMagasin")
    Integer getActiviteLieuExerciceMagasin();

    /**
     * Set le activite lieu exercice magasin.
     *
     * @param activiteLieuExerciceMagasin
     *            the activiteLieuExerciceMagasin to set
     */
    void setActiviteLieuExerciceMagasin(Integer activiteLieuExerciceMagasin);

    /**
     * Get le nom commercial professionnel.
     *
     * @return the nomCommercialProfessionnel
     */
    @FieldXPath(value = "nomCommercialProfessionnel")
    String getNomCommercialProfessionnel();

    /**
     * Set le nom commercial professionnel.
     *
     * @param nomCommercialProfessionnel
     *            the nomCommercialProfessionnel to set
     */
    void setNomCommercialProfessionnel(String nomCommercialProfessionnel);

    /**
     * Get le enseigne.
     *
     * @return the enseigne
     */
    @FieldXPath(value = "enseigne")
    String getEnseigne();

    /**
     * Set le enseigne.
     *
     * @param enseigne
     *            the enseigne to set
     */
    void setEnseigne(String enseigne);

    /**
     * Get le origine fonds.
     *
     * @return the origineFonds
     */
    @FieldXPath(value = "origineFonds")
    String getOrigineFonds();

    /**
     * Set le origine fonds.
     *
     * @param origineFonds
     *            the origineFonds to set
     */
    void setOrigineFonds(String origineFonds);

    /**
     * Get le loueur mandant du fonds debut contrat.
     *
     * @return the loueurMandantDuFondsDebutContrat
     */
    @FieldXPath(value = "loueurMandantDuFondsDebutContrat")
    String getLoueurMandantDuFondsDebutContrat();

    /**
     * Set le loueur mandant du fonds debut contrat.
     *
     * @param loueurMandantDuFondsDebutContrat
     *            the loueurMandantDuFondsDebutContrat to set
     */
    void setLoueurMandantDuFondsDebutContrat(String loueurMandantDuFondsDebutContrat);

    /**
     * Get le loueur mandant du fonds duree contrat indeterminee.
     *
     * @return the loueurMandantDuFondsDureeContratIndeterminee
     */
    @FieldXPath(value = "loueurMandantDuFondsDureeContratIndeterminee")
    String getLoueurMandantDuFondsDureeContratIndeterminee();

    /**
     * Set le loueur mandant du fonds duree contrat indeterminee.
     *
     * @param loueurMandantDuFondsDureeContratIndeterminee
     *            the loueurMandantDuFondsDureeContratIndeterminee to set
     */
    void setLoueurMandantDuFondsDureeContratIndeterminee(String loueurMandantDuFondsDureeContratIndeterminee);

    /**
     * Get le loueur mandant du fonds fin contrat.
     *
     * @return the loueurMandantDuFondsFinContrat
     */
    @FieldXPath(value = "loueurMandantDuFondsFinContrat")
    String getLoueurMandantDuFondsFinContrat();

    /**
     * Set le loueur mandant du fonds fin contrat.
     *
     * @param loueurMandantDuFondsFinContrat
     *            the loueurMandantDuFondsFinContrat to set
     */
    void setLoueurMandantDuFondsFinContrat(String loueurMandantDuFondsFinContrat);

    /**
     * Get le loueur mandant du fonds renouvellement tacite reconduction.
     *
     * @return the loueurMandantDuFondsRenouvellementTaciteReconduction
     */
    @FieldXPath(value = "loueurMandantDuFondsRenouvellementTaciteReconduction")
    String getLoueurMandantDuFondsRenouvellementTaciteReconduction();

    /**
     * Set le loueur mandant du fonds renouvellement tacite reconduction.
     *
     * @param loueurMandantDuFondsRenouvellementTaciteReconduction
     *            the loueurMandantDuFondsRenouvellementTaciteReconduction to
     *            set
     */
    void setLoueurMandantDuFondsRenouvellementTaciteReconduction(String loueurMandantDuFondsRenouvellementTaciteReconduction);

    /**
     * Get le plan cession.
     *
     * @return the planCession
     */
    @FieldXPath(value = "planCession")
    String getPlanCession();

    /**
     * Set le plan cession.
     *
     * @param planCession
     *            the planCession to set
     */
    void setPlanCession(String planCession);

    /**
     * Get le fonds uniquement artisanal.
     *
     * @return the fondsUniquementArtisanal
     */
    @FieldXPath(value = "fondsUniquementArtisanal")
    String getFondsUniquementArtisanal();

    /**
     * Set le fonds uniquement artisanal.
     *
     * @param fondsUniquementArtisanal
     *            the fondsUniquementArtisanal to set
     */
    void setFondsUniquementArtisanal(String fondsUniquementArtisanal);

    /**
     * Get le journal annonces legales nom.
     *
     * @return the journalAnnoncesLegalesNom
     */
    @FieldXPath(value = "journalAnnoncesLegalesNom")
    String getJournalAnnoncesLegalesNom();

    /**
     * Set le journal annonces legales nom.
     *
     * @param journalAnnoncesLegalesNom
     *            the journalAnnoncesLegalesNom to set
     */
    void setJournalAnnoncesLegalesNom(String journalAnnoncesLegalesNom);

    /**
     * Get le journal annonces legales date parution.
     *
     * @return the journalAnnoncesLegalesDateParution
     */
    @FieldXPath(value = "journalAnnoncesLegalesDateParution")
    String getJournalAnnoncesLegalesDateParution();

    /**
     * Set le journal annonces legales date parution.
     *
     * @param journalAnnoncesLegalesDateParution
     *            the journalAnnoncesLegalesDateParution to set
     */
    void setJournalAnnoncesLegalesDateParution(String journalAnnoncesLegalesDateParution);

    /**
     * Get le personne pouvoir presence.
     *
     * @return the personnePouvoirPresence
     */
    @FieldXPath(value = "personnePouvoirPresence")
    String getPersonnePouvoirPresence();

    /**
     * Set le personne pouvoir presence.
     *
     * @param personnePouvoirPresence
     *            the personnePouvoirPresence to set
     */
    void setPersonnePouvoirPresence(String personnePouvoirPresence);

    /**
     * Get le personne pouvoir nombre.
     *
     * @return the personnePouvoirNombre
     */
    @FieldXPath(value = "personnePouvoirNombre")
    Integer getPersonnePouvoirNombre();

    /**
     * Set le personne pouvoir nombre.
     *
     * @param personnePouvoirNombre
     *            the personnePouvoirNombre to set
     */
    void setPersonnePouvoirNombre(Integer personnePouvoirNombre);

    /**
     * Get le categorie.
     *
     * @return the categorie
     */
    @FieldXPath(value = "categorie")
    String getCategorie();

    /**
     * Set le categorie.
     *
     * @param categorie
     *            the categorie to set
     */
    void setCategorie(String categorie);

    /**
     * Get le situation.
     *
     * @return the situation
     */
    @FieldXPath(value = "situation")
    String getSituation();

    /**
     * Set le situation.
     *
     * @param situation
     *            the situation to set
     */
    void setSituation(String situation);

    /**
     * Get le effectif salarie presence.
     *
     * @return the effectifSalariePresence
     */
    @FieldXPath(value = "effectifSalariePresence")
    String getEffectifSalariePresence();

    /**
     * Set le effectif salarie presence.
     *
     * @param effectifSalariePresence
     *            the effectifSalariePresence to set
     */
    void setEffectifSalariePresence(String effectifSalariePresence);

    /**
     * Get le effectif salarie nombre.
     *
     * @return the effectifSalarieNombre
     */
    @FieldXPath(value = "effectifSalarieNombre")
    Integer getEffectifSalarieNombre();

    /**
     * Set le effectif salarie nombre.
     *
     * @param effectifSalarieNombre
     *            the effectifSalarieNombre to set
     */
    void setEffectifSalarieNombre(Integer effectifSalarieNombre);

    /**
     * Get le effectif salarie apprentis.
     *
     * @return the effectifSalarieApprentis
     */
    @FieldXPath(value = "effectifSalarieApprentis")
    Integer getEffectifSalarieApprentis();

    /**
     * Get le activiteSoumiseQualification.
     *
     * @return le activiteSoumiseQualification
     */
    @FieldXPath(value = "activitesSoumisesQualification/activiteSoumiseQualification")
    IActiviteSoumiseQualificationCreation[] getActivitesSoumisesQualification();

    /**
     * Ajoute le to activiteSoumiseQualification.
     *
     * @return le i activiteSoumiseQualification
     */
    IActiviteSoumiseQualificationCreation addToActivitesSoumisesQualification();

    /**
     * Set le ActivitesSoumisesQualification.
     *
     * @param ActivitesSoumisesQualification
     *            le nouveau ActivitesSoumisesQualification
     */
    void setActivitesSoumisesQualification(IActiviteSoumiseQualificationCreation[] activitesSoumisesQualification);

    /**
     * Retire le from dirigeants.
     *
     * @param dirigeants
     *            le dirigeants
     */
    void removeFromActivitesSoumisesQualification(IActiviteSoumiseQualificationCreation activitesSoumisesQualification);

    /**
     * Set le effectif salarie apprentis.
     *
     * @param effectifSalarieApprentis
     *            the effectifSalarieApprentis to set
     */
    void setEffectifSalarieApprentis(Integer effectifSalarieApprentis);

    /**
     * Setter de l'attribut activiteElevage.
     * 
     * @param activiteElevage
     *            la nouvelle valeur de activiteElevage
     */
    void setActiviteElevage(String activiteElevage);

    /**
     * Setter de l'attribut activiteImmobiliere.
     * 
     * @param activiteImmobiliere
     *            la nouvelle valeur de activiteImmobiliere
     */
    void setActiviteImmobiliere(String activiteImmobiliere);

    /**
     * Setter de l'attribut activiteLieuExercice.
     * 
     * @param activiteLieuExercice
     *            la nouvelle valeur de activiteLieuExercice
     */
    void setActiviteLieuExercice(String activiteLieuExercice);

    /**
     * Setter de l'attribut activiteLieuExerciceAutre.
     * 
     * @param activiteLieuExerciceAutre
     *            la nouvelle valeur de activiteLieuExerciceAutre
     */
    void setActiviteLieuExerciceAutre(String activiteLieuExerciceAutre);

    /**
     * Setter de l'attribut activitePlusImportante.
     * 
     * @param activitePlusImportante
     *            la nouvelle valeur de activitePlusImportante
     */

    void setActivitePlusImportante(String activitePlusImportante);

    /**
     * Mutateur sur l'attribut activites.
     *
     * @param activites
     *            le nouveau activites
     */
    void setActivites(String activites);

    /**
     * Setter de l'attribut activitePlusImportanteAgricole1.
     * 
     * @param activitePlusImportanteAgricole1
     *            la nouvelle valeur de activitePlusImportanteAgricole1
     */
    void setActivitePlusImportanteAgricole1(String activitePlusImportanteAgricole1);

    /**
     * Setter de l'attribut activitePlusImportanteAgricole2.
     * 
     * @param activitePlusImportanteAgricole2
     *            la nouvelle valeur de activitePlusImportanteAgricole2
     */
    void setActivitePlusImportanteAgricole2(String activitePlusImportanteAgricole2);

    /**
     * Setter de l'attribut activitesAccessoiresBICBNC.
     * 
     * @param activitesAccessoiresBICBNC
     *            la nouvelle valeur de activitesAccessoiresBICBNC
     */
    void setActivitesAccessoiresBICBNC(String activitesAccessoiresBICBNC);

    /**
     * Setter de l'attribut activitesExerceesAgricole.
     * 
     * @param activitesExerceesAgricole
     *            la nouvelle valeur de activitesExerceesAgricole
     */
    void setActivitesExerceesAgricole(XmlString[] activitesExerceesAgricole);

    /**
     * Setter de l'attribut activiteViticole.
     * 
     * @param activiteViticole
     *            la nouvelle valeur de activiteViticole
     */
    void setActiviteViticole(String activiteViticole);

    /**
     * Setter de l'attribut effectifSalarieAgricoleNombre.
     * 
     * @param effectifSalarieAgricoleNombre
     *            la nouvelle valeur de effectifSalarieAgricoleNombre
     */
    void setEffectifSalarieAgricoleNombre(Integer effectifSalarieAgricoleNombre);

    /**
     * Setter de l'attribut effectifSalarieEmbauchePremierSalarie.
     * 
     * @param effectifSalarieEmbauchePremierSalarie
     *            la nouvelle valeur de effectifSalarieEmbauchePremierSalarie
     */
    void setEffectifSalarieEmbauchePremierSalarie(String effectifSalarieEmbauchePremierSalarie);

    /**
     * Setter de l'attribut estActiviteLiberale.
     * 
     * @param estActiviteLiberale
     *            la nouvelle valeur de estActiviteLiberale
     */
    void setEstActiviteLiberale(String estActiviteLiberale);

    /**
     * Setter de l'attribut periodeActiviteSaisonniere1DateDebut.
     * 
     * @param periodeActiviteSaisonniere1DateDebut
     *            la nouvelle valeur de periodeActiviteSaisonniere1DateDebut
     */
    void setPeriodeActiviteSaisonniere1DateDebut(String periodeActiviteSaisonniere1DateDebut);

    /**
     * Setter de l'attribut periodeActiviteSaisonniere1DateFin.
     * 
     * @param periodeActiviteSaisonniere1DateFin
     *            la nouvelle valeur de periodeActiviteSaisonniere1DateFin
     */
    void setPeriodeActiviteSaisonniere1DateFin(String periodeActiviteSaisonniere1DateFin);

    /**
     * Setter de l'attribut periodeActiviteSaisonniere2DateDebut.
     * 
     * @param periodeActiviteSaisonniere2DateDebut
     *            la nouvelle valeur de periodeActiviteSaisonniere2DateDebut
     */
    void setPeriodeActiviteSaisonniere2DateDebut(String periodeActiviteSaisonniere2DateDebut);

    /**
     * Setter de l'attribut periodeActiviteSaisonniere2DateFin.
     * 
     * @param periodeActiviteSaisonniere2DateFin
     *            la nouvelle valeur de periodeActiviteSaisonniere2DateFin
     */
    void setPeriodeActiviteSaisonniere2DateFin(String periodeActiviteSaisonniere2DateFin);

    /**
     * Setter de l'attribut precedentExploitantNombre.
     * 
     * @param precedentExploitantNombre
     *            la nouvelle valeur de precedentExploitantNombre
     */
    void setPrecedentExploitantNombre(Integer precedentExploitantNombre);

    /**
     * Setter de l'attribut secteurActivitePlusImportant.
     * 
     * @param secteurActivitePlusImportant
     *            la nouvelle valeur de secteurActivitePlusImportant
     */
    void setSecteurActivitePlusImportant(String secteurActivitePlusImportant);

    /**
     * Setter de l'attribut secteursActivites.
     * 
     * @param secteursActivites
     *            la nouvelle valeur de secteursActivites
     */
    void setSecteursActivites(String secteursActivites);

}
