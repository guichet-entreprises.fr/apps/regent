package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface ICE.
 */
@ResourceXPath("ICE")
public interface ICE {

  /**
   * Get le e01.
   *
   * @return le e01
   */
  @FieldXPath("E01")
  String getE01();

  /**
   * Set le e01.
   *
   * @param E01
   *          le nouveau e01
   */
  void setE01(String E01);

  /**
   * Get le e02.
   *
   * @return le e02
   */
  @FieldXPath("E02")
  IE02[] getE02();

  /**
   * Ajoute le to e02.
   *
   * @return le i e02
   */
  IE02 addToE02();

  /**
   * Set le.
   *
   * @param E02
   *          le e02
   */
  void set(IE02[] E02);

  /**
   * Get le e03.
   *
   * @return le e03
   */
  @FieldXPath("E03")
  IE03[] getE03();

  /**
   * Ajoute le to e03.
   *
   * @return le i e03
   */
  IE03 addToE03();

  /**
   * Set le.
   *
   * @param E03
   *          le e03
   */
  void set(IE03[] E03);

  /**
   * Get le e04.
   *
   * @return le e04
   */
  @FieldXPath("E04")
  String getE04();

  /**
   * Set le e04.
   *
   * @param E04
   *          le nouveau e04
   */
  void setE04(String E04);

  /**
   * Get le e05.
   *
   * @return le e05
   */
  @FieldXPath("E05")
  String getE05();

  /**
   * Set le e05.
   *
   * @param E05
   *          le nouveau e05
   */
  void setE05(String E05);

  /**
   * Get le e09.
   *
   * @return le e09
   */
  @FieldXPath("E09")
  String getE09();

  /**
   * Set le e09.
   *
   * @param E09
   *          le nouveau e09
   */
  void setE09(String E09);

}
