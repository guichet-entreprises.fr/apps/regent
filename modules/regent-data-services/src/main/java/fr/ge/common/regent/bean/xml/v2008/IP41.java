package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IP41.
 */
@ResourceXPath("/P41")
public interface IP41 {

  /**
   * Get le p411.
   *
   * @return le p411
   */
  @FieldXPath("P41.1")
  String getP411();

  /**
   * Set le p411.
   *
   * @param P411
   *          le nouveau p411
   */
  void setP411(String P411);

  /**
   * Get le p412.
   *
   * @return le p412
   */
  @FieldXPath("P41.2")
  String getP412();

  /**
   * Set le p412.
   *
   * @param P412
   *          le nouveau p412
   */
  void setP412(String P412);

  /**
   * Get le p413.
   *
   * @return le p413
   */
  @FieldXPath("P41.3")
  String getP413();

  /**
   * Set le p413.
   *
   * @param P413
   *          le nouveau p413
   */
  void setP413(String P413);

  /**
   * Get le p414.
   *
   * @return le p414
   */
  @FieldXPath("P41.4")
  String getP414();

  /**
   * Set le p414.
   *
   * @param P414
   *          le nouveau p414
   */
  void setP414(String P414);

}
