package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface CAP.
 */
@ResourceXPath("/CAP")
public interface CAP {

  /**
   * Get le p51.
   *
   * @return le p51
   */
  @FieldXPath("P51")
  String getP51();

  /**
   * Set le p51.
   *
   * @param P51
   *          le nouveau p51
   */
  void setP51(String P51);

  /**
   * Get le p53.
   *
   * @return le p53
   */
  @FieldXPath("P53")
  String getP53();

  /**
   * Set le p53.
   *
   * @param P53
   *          le nouveau p53
   */
  void setP53(String P53);

  /**
   * Get le p54.
   *
   * @return le p54
   */
  @FieldXPath("P54")
  String getP54();

  /**
   * Set le p54.
   *
   * @param P54
   *          le nouveau p54
   */
  void setP54(String P54);

  /**
   * Get le p63.
   *
   * @return le p63
   */
  @FieldXPath("P63")
  String getP63();

  /**
   * Set le p63.
   *
   * @param P63
   *          le nouveau p63
   */
  void setP63(String P63);

}
