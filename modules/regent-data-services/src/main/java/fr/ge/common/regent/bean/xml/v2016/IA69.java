package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Interface IA10.
 */
@ResourceXPath("/A69")
public interface IA69 {

  /**
   * Get le A69.1.
   *
   * @return le A69.1
   */
  @FieldXPath("A69.1")
  String getA691();

  /**
   * Set le A69.1.
   *
   * @param A691
   *          /le nouveau A69.1
   */
  void setA691(String A691);

  /**
   * Get le A69.2.
   *
   * @return le A69.2
   */
  @FieldXPath("A69.2")
  String getA692();

  /**
   * Set le A69.2.
   *
   * @param A692
   *          .7 le nouveau A69.2
   */
  void setA692(String A692);

  /**
   * Get le A69.3.
   *
   * @return le A69.3
   */
  @FieldXPath("A69.3")
  String getA693();

  /**
   * Set le A69.3.
   *
   * @param A693
   *          le nouveau A69.3
   */
  void setA693(String A693);

  /**
   * Get le A69.4.
   *
   * @return le A69.4
   */
  @FieldXPath("A69.4")
  String getA694();

  /**
   * Set le A69.4.
   *
   * @param A694
   *          le nouveau A69.4
   */
  void setA694(String A694);

}
