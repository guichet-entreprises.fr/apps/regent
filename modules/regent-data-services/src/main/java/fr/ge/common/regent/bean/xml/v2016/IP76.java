package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IP76.
 */
@ResourceXPath("/P76")
public interface IP76 {

  /**
   * Get le p76.1.
   * 
   * @return le p76.1
   */
  @FieldXPath("P76.1")
  String getP761();

  /**
   * Set le p76.1.
   * 
   * @param P761
   *          .1 le nouveau p76.1
   */
  void setP761(String P761);

  /**
   * Get le p76.2.
   * 
   * @return le p76.2
   */
  @FieldXPath("P76.2")
  String getP762();

  /**
   * Set le p76.2.
   * 
   * @param P762
   *          .2 le nouveau p76.1
   */
  void setP762(String P762);

  /**
   * Get le p76.3.
   * 
   * @return le p76.3
   */
  @FieldXPath("P76.3")
  String getP763();

  /**
   * Set le p76.3.
   * 
   * @param P763
   *          .2 le nouveau p76.3
   */
  void setP763(String P763);

  /**
   * Get le p76.4.
   * 
   * @return le p76.4
   */
  @FieldXPath("P76.4")
  String getP764();

  /**
   * Set le p76.4.
   * 
   * @param P764
   *          .2 le nouveau p76.4
   */
  void setP764(String P764);
}
