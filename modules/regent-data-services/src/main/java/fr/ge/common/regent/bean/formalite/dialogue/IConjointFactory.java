package fr.ge.common.regent.bean.formalite.dialogue;

/**
 * Factory permettant de creer un objet simple/complexe/composite.
 *
 * @param <T>
 *            le type generique
 */
public interface IConjointFactory<T extends AbstractConjointBean> {

    /**
     * Creation d'un nouvel objet pour le conjoint.
     *
     * @return T
     */
    public T createNewConjoint();

}
