package fr.ge.common.regent.persistance.dao.referentiel;

import java.util.Collection;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import fr.ge.common.regent.bean.modele.referentiel.ENormeEvenement;
import fr.ge.common.regent.persistance.dao.AbstractDao;

/**
 * Le Class NormeEvenementDao.
 */
public class NormeEvenementDao extends AbstractDao < ENormeEvenement, String > {

  /**
   * Instancie un nouveau norme evenement dao.
   *
   * @param type
   *          le type
   */
  public NormeEvenementDao(Class < ENormeEvenement > type) {
    super(type);
  }

  /**
   * A partir de l’événement et de la version récupérer la liste des rubriques de la table
   * NORME_EVENEMENT. S'il y a plusieurs événements on récupère l'union de l'ensemble des rubriques
   * de chacun des événements.
   *
   * @param version
   *          le version
   * @param evenements
   *          le evenements
   * @return list de l'ensemble des rubriques de chacun des événements
   */
  public List < String > getRubriqueByEvenementAndVersion(final String version, final List < String > evenements) {

    Collection < String > values = evenements;
    Criteria criteria = this.getSession().createCriteria(this.getClazz());
    criteria.add(Restrictions.in("evenement", values));
    criteria.add(Restrictions.eq("version", version));
    criteria.setProjection(Projections.distinct(Projections.property("rubrique")));

    return criteria.list();
  }

  /**
   * A partir de l’événement et de la version récupérer la liste des rubriques de la table
   * NORME_EVENEMENT. S'il y a plusieurs événements on récupère l'union de l'ensemble des rubriques
   * de chacun des événements.
   *
   * @param version
   *          le version
   * @param evenements
   *          le evenements
   * @return list de l'ensemble des rubriques de chacun des événements
   */
  public List < ENormeEvenement > getRubriqueOblogatoireOuConditionnelle(String version, List < String > evenements) {

    Collection < String > values = evenements;
    Criteria criteria = this.getSession().createCriteria(this.getClazz());
    criteria.add(Restrictions.in("evenement", values));
    criteria.add(Restrictions.eq("version", version));

    return criteria.list();
  }

}
