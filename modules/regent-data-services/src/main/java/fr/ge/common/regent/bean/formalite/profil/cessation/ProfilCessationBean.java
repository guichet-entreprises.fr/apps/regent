package fr.ge.common.regent.bean.formalite.profil.cessation;

import static fr.ge.common.regent.util.CodeAPEUtils.transformeCodeAPE;

import fr.ge.common.regent.bean.formalite.profil.AbstractProfilEntreprise;

/**
 * Le Class ProfilCessationBean.
 */
public class ProfilCessationBean extends AbstractProfilEntreprise {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -7007760256996378955L;

    /** Le forme juridique. */
    private String formeJuridique;

    /** Le micro social oui non. */
    private String microSocialOuiNon;

    /** Le activite principale code ape. */
    private String activitePrincipaleCodeAPE;

    /** Le activite principale secteur. */
    private String activitePrincipaleSecteur;

    /** Le existe activite secondaire. */
    private String existeActiviteSecondaire;

    /** Le activite secondaire domaine. */
    private String activiteSecondaireDomaine;

    /** Le activite secondaire secteur. */
    private String activiteSecondaireSecteur;

    /** Le activite secondaire code activite. */
    private String activiteSecondaireCodeActivite;

    /** Le nom dossier. */
    private String nomDossier;

    /** Le option cmacc i1. */
    private String optionCMACCI1;

    /** Le option cmacci. */
    private String optionCMACCI;

    /** Le option cmacc i2. */
    private String optionCMACCI2;

    /** Le est inscrit registre public. */
    private String estInscritRegistrePublic;

    /** Le activite principale secteur2. */
    private String activitePrincipaleSecteur2;

    /** Type CFE. */
    private String typeCfe;

    /** Reseau CFE. */
    private String reseauCFE;

    /** Le numero de la formalite. */
    private String numeroFormalite;

    /**
     * Valeur technique, informe si l'utilisateur s'est connecté via dgfip ou
     * pas.
     */
    private String provenanceImpot;

    /** La description libre de l'activité secondaire. */
    private String descrLibreActiviteSecondaire;

    /**
     * Get provenanceImpot.
     * 
     * @return the provenanceImpot
     */
    public String getProvenanceImpot() {
        return provenanceImpot;
    }

    /**
     * Set provenanceImpot.
     * 
     * @param provenanceImpot
     *            the provenanceImpot to set
     */
    public void setProvenanceImpot(final String provenanceImpot) {
        this.provenanceImpot = provenanceImpot;
    }

    /**
     * Get le forme juridique.
     * 
     * @return the formeJuridique
     */
    public String getFormeJuridique() {
        return formeJuridique;
    }

    /**
     * Set le forme juridique.
     * 
     * @param formeJuridique
     *            the formeJuridique to set
     */
    public void setFormeJuridique(final String formeJuridique) {
        this.formeJuridique = formeJuridique;
    }

    /**
     * Get le micro social oui non.
     * 
     * @return the microSocialOuiNon
     */
    public String getMicroSocialOuiNon() {
        return this.microSocialOuiNon;
    }

    /**
     * Set le micro social oui non.
     * 
     * @param microSocialOuiNon
     *            the microSocialOuiNon to set
     */
    public void setMicroSocialOuiNon(final String microSocialOuiNon) {
        this.microSocialOuiNon = microSocialOuiNon;
    }

    /**
     * Get le activite principale code ape.
     * 
     * @return the activitePrincipaleCodeAPE
     */

    public String getActivitePrincipaleCodeAPE() {
        return transformeCodeAPE(this.activitePrincipaleCodeAPE);
    }

    /**
     * Set le activite principale code ape.
     * 
     * @param activitePrincipaleCodeAPE
     *            le nouveau activite principale code ape
     */
    public void setActivitePrincipaleCodeAPE(final String activitePrincipaleCodeAPE) {
        this.activitePrincipaleCodeAPE = transformeCodeAPE(activitePrincipaleCodeAPE);
    }

    /**
     * Get le activite principale secteur.
     * 
     * @return the activitePrincipaleSecteur
     */
    public String getActivitePrincipaleSecteur() {
        return this.activitePrincipaleSecteur;
    }

    /**
     * Set le activite principale secteur.
     * 
     * @param activitePrincipaleSecteur
     *            the activitePrincipaleSecteur to set
     */
    public void setActivitePrincipaleSecteur(final String activitePrincipaleSecteur) {
        this.activitePrincipaleSecteur = activitePrincipaleSecteur;
    }

    /**
     * Get le existe activite secondaire.
     * 
     * @return the existeActiviteSecondaire
     */

    public String getExisteActiviteSecondaire() {
        return this.existeActiviteSecondaire;
    }

    /**
     * Set le existe activite secondaire.
     * 
     * @param existeActiviteSecondaire
     *            le nouveau existe activite secondaire
     */
    public void setExisteActiviteSecondaire(final String existeActiviteSecondaire) {
        this.existeActiviteSecondaire = existeActiviteSecondaire;
    }

    /**
     * Get le activite secondaire domaine.
     * 
     * @return the activiteSecondaireDomaine
     */
    public String getActiviteSecondaireDomaine() {
        return this.activiteSecondaireDomaine;
    }

    /**
     * Set le activite secondaire domaine.
     * 
     * @param activiteSecondaireDomaine
     *            the activiteSecondaireDomaine to set
     */
    public void setActiviteSecondaireDomaine(final String activiteSecondaireDomaine) {
        this.activiteSecondaireDomaine = activiteSecondaireDomaine;
    }

    /**
     * Get le activite secondaire secteur.
     * 
     * @return the activiteSecondaireSecteur
     */
    public String getActiviteSecondaireSecteur() {
        return this.activiteSecondaireSecteur;
    }

    /**
     * Set le activite secondaire secteur.
     * 
     * @param activiteSecondaireSecteur
     *            the activiteSecondaireSecteur to set
     */
    public void setActiviteSecondaireSecteur(final String activiteSecondaireSecteur) {
        this.activiteSecondaireSecteur = activiteSecondaireSecteur;
    }

    /**
     * Get le activite secondaire code activite.
     * 
     * @return the activiteSecondaireCodeActivite
     */
    @Override
    public String getActiviteSecondaireCodeActivite() {
        return this.activiteSecondaireCodeActivite;
    }

    /**
     * Set le activite secondaire code activite.
     * 
     * @param activiteSecondaireCodeActivite
     *            the activiteSecondaireCodeActivite to set
     */
    @Override
    public void setActiviteSecondaireCodeActivite(final String activiteSecondaireCodeActivite) {
        this.activiteSecondaireCodeActivite = activiteSecondaireCodeActivite;
    }

    /**
     * Get le nom dossier.
     * 
     * @return the nomDossier
     */
    @Override
    public String getNomDossier() {
        return this.nomDossier;
    }

    /**
     * Set le nom dossier.
     * 
     * @param nomDossier
     *            the nomDossier to set
     */
    @Override
    public void setNomDossier(final String nomDossier) {
        this.nomDossier = nomDossier;
    }

    /**
     * Get le option cmacc i1.
     * 
     * @return the optionCMACCI1
     */
    public String getOptionCMACCI1() {
        return this.optionCMACCI1;
    }

    /**
     * Set le option cmacc i1.
     * 
     * @param optionCMACCI1
     *            the optionCMACCI1 to set
     */
    public void setOptionCMACCI1(final String optionCMACCI1) {
        this.optionCMACCI1 = optionCMACCI1;
    }

    /**
     * Get le option cmacc i2.
     * 
     * @return the optionCMACCI2
     */
    public String getOptionCMACCI2() {
        return this.optionCMACCI2;
    }

    /**
     * Set le option cmacc i2.
     * 
     * @param optionCMACCI2
     *            the optionCMACCI2 to set
     */
    public void setOptionCMACCI2(final String optionCMACCI2) {
        this.optionCMACCI2 = optionCMACCI2;
    }

    /**
     * Get le est inscrit registre public.
     * 
     * @return the estInscritRegistrePublic
     */
    public String getEstInscritRegistrePublic() {
        return this.estInscritRegistrePublic;
    }

    /**
     * Set le est inscrit registre public.
     * 
     * @param estInscritRegistrePublic
     *            the estInscritRegistrePublic to set
     */
    public void setEstInscritRegistrePublic(final String estInscritRegistrePublic) {
        this.estInscritRegistrePublic = estInscritRegistrePublic;
    }

    /**
     * Get le activite principale secteur2.
     * 
     * @return le activite principale secteur2
     */
    public String getActivitePrincipaleSecteur2() {
        return activitePrincipaleSecteur2;
    }

    /**
     * Set le activite principale secteur2.
     * 
     * @param activitePrincipaleSecteur2
     *            le nouveau activite principale secteur2
     */
    public void setActivitePrincipaleSecteur2(final String activitePrincipaleSecteur2) {
        this.activitePrincipaleSecteur2 = activitePrincipaleSecteur2;
    }

    /**
     * Get le type cfe.
     * 
     * @return the typeCfe
     */
    public String getTypeCfe() {
        return typeCfe;
    }

    /**
     * Set le type cfe.
     * 
     * @param typeCfe
     *            the typeCfe to set
     */
    public void setTypeCfe(final String typeCfe) {
        this.typeCfe = typeCfe;
    }

    /**
     * Get le reseau cfe.
     * 
     * @return the reseauCFE
     */
    public String getReseauCFE() {
        return reseauCFE;
    }

    /**
     * Set le reseau cfe.
     * 
     * @param reseauCFE
     *            the reseauCFE to set
     */
    public void setReseauCFE(final String reseauCFE) {
        this.reseauCFE = reseauCFE;
    }

    /**
     * Get le option cmacci.
     * 
     * @return le option cmacci
     */
    public String getOptionCMACCI() {
        return optionCMACCI;
    }

    /**
     * Set le option cmacci.
     * 
     * @param optionCMACCI
     *            le nouveau option cmacci
     */
    public void setOptionCMACCI(final String optionCMACCI) {
        this.optionCMACCI = optionCMACCI;
    }

    /**
     * Get le numero formalite.
     * 
     * @return the numeroFormalite
     */
    public String getNumeroFormalite() {
        return numeroFormalite;
    }

    /**
     * Set le numero formalite.
     * 
     * @param numeroFormalite
     *            the numeroFormalite to set
     */
    public void setNumeroFormalite(final String numeroFormalite) {
        this.numeroFormalite = numeroFormalite;
    }

    /**
     * Accesseur sur l'attribut {@link #descrLibreActiviteSecondaire}.
     * 
     * @return String descrLibreActiviteSecondaire
     */
    public String getDescrLibreActiviteSecondaire() {
        return descrLibreActiviteSecondaire;
    }

    /**
     * Mutateur sur l'attribut {@link #descrLibreActiviteSecondaire}.
     * 
     * @param descrLibreActiviteSecondaire
     *            la nouvelle valeur de l'attribut descrLibreActiviteSecondaire
     */
    public void setDescrLibreActiviteSecondaire(final String descrLibreActiviteSecondaire) {
        this.descrLibreActiviteSecondaire = descrLibreActiviteSecondaire;
    }
}
