package fr.ge.common.regent.constante.enumeration;

/**
 * Enum des destinataire du Flux Regent.
 * 
 */

public enum DestinataireXmlRegentEnum {

  /**
   * <p>
   * 1- nom de l'attribut du flux dans l'entité
   * <p>
   * 2- nom du champ dans la BDD.
   */

  CMA("CMA", "fluxCma"), /** Le cci. */
  CCI("CCI", "fluxCci"), /** Le urssaf. */
  URSSAF("URSSAF", "fluxUrssaf"), /** Le greffe. */
  GREFFE("GREFFE", "fluxGreffe"), /** Le cnba. */
  CNBA("CNBA", "fluxCnba"),
  /** Le ca. */
  CA("CA", "fluxCa"), AGTCOM("AGTCOM", "fluxAgtcom");

  /** nom . */
  private String nom;
  /** valeur. */
  private String nomAttribut;

  /**
   * Constructeur.
   * 
   * @param nom
   *          nom.
   * @param nomAttribut
   *          nom de l'attribut du flux dans l'entité.
   */
  private DestinataireXmlRegentEnum(final String nom, final String nomAttribut) {
    this.nom = nom;
    this.nomAttribut = nomAttribut;
  }

  /**
   * Accesseur en lecture de la classe <code>Destinataire</code> par Nom.
   * 
   * @param nom
   *          nom.
   * @return L'objet nom du destinataire.
   */
  public static DestinataireXmlRegentEnum getDestinataireParNom(final String nom) {
    for (DestinataireXmlRegentEnum destinataire : DestinataireXmlRegentEnum.values()) {
      if (destinataire.name().equals(nom)) {
        return destinataire;
      }
    }
    return null;
  }

  /**
   * Accesseur en lecture de l'attribut <code>nomAttribut</code>.
   * 
   * @return the nom de l'attribut du flux dans l'entité.
   */
  public String getNomAttribut() {
    return nomAttribut;
  }

  /**
   * Accesseur en écriture de l'attribut <code>nomAttribut</code>.
   * 
   * @param nomAttribut
   *          nom de l'attribut du flux dans l'entité à setter.
   */
  public void setNomAttribut(final String nomAttribut) {
    this.nomAttribut = nomAttribut;
  }

  /**
   * Accesseur en lecture de l'attribut <code>nom</code>.
   * 
   * @return the nom.
   */
  public String getnom() {
    return nom;
  }

  /**
   * Accesseur en écriture de l'attribut <code>nom</code>.
   * 
   * @param nom
   *          le nom à setter.
   */
  public void setnom(final String nom) {
    this.nom = nom;
  }

}
