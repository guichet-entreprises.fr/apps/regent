/**
 * 
 */
package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;
import org.xmlfield.core.types.XmlString;

/**
 * Interface R22.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
@ResourceXPath("/R22")
public interface R22 {

  /**
   * Accesseur sur l'attribut {@link #r222}.
   *
   * @return r222
   */
  @FieldXPath("R22.2")
  String getR222();

  /**
   * Mutateur sur l'attribut {@link #r222}.
   *
   * @param R222
   *          la nouvelle valeur de l'attribut r222
   */
  void setR222(String R222);

  /**
   * Accesseur sur l'attribut {@link #r223}.
   *
   * @return r223
   */
  @FieldXPath("R22.3")
  String getR223();

  /**
   * Mutateur sur l'attribut {@link #r223}.
   *
   * @param R223
   *          la nouvelle valeur de l'attribut r223
   */
  void setR223(String R223);

  /**
   * Ajoute le to R223.
   *
   * @return le xml string
   */
  XmlString addToR223();

}
