package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface PersonnePhysiqueDirigeante.
 */
@ResourceXPath("/PersonnePhysiqueDirigeante")
public interface PersonnePhysiqueDirigeante {

  /**
   * Get le gcs.
   *
   * @return le gcs
   */
  @FieldXPath("GCS")
  GCS[] getGCS();

  /**
   * Ajoute le to gcs.
   *
   * @return le gcs
   */
  GCS addToGCS();

  /**
   * Set le gcs.
   *
   * @param GCS
   *          le nouveau gcs
   */
  void setGCS(GCS[] GCS);

  /**
   * Get le IPD.
   *
   * @return le IPD
   */
  @FieldXPath("IPD")
  IIPD[] getIPD();

  /**
   * Ajoute le to IPD.
   *
   * @return le IPD
   */
  IIPD addToIPD();

  /**
   * Set le IPD.
   *
   * @param IPD
   *          le nouveau IPD
   */
  void setIPD(IIPD[] IPD);

  /**
   * Get le NPD.
   *
   * @return le NPD
   */
  @FieldXPath("NPD")
  INPD[] getNPD();

  /**
   * Ajoute le to NPD.
   *
   * @return le NPD
   */
  INPD addToNPD();

  /**
   * Set le NPD.
   *
   * @param NPD
   *          le nouveau NPD
   */
  void setIPD(INPD[] NPD);

  /**
   * Get le DIU.
   *
   * @return le DIU
   */
  @FieldXPath("DIU")
  IDIU[] getDIU();

  /**
   * Ajoute le to DIU.
   *
   * @return le DIU
   */
  IDIU addToDIU();

  /**
   * Set le DIU.
   *
   * @param DIU
   *          le nouveau DIU
   */
  void setDIU(IDIU[] DIU);

  /**
   * Get le IDU.
   *
   * @return le IDU
   */
  @FieldXPath("IDU")
  IIDU[] getIDU();

  /**
   * Ajoute le to IDU.
   *
   * @return le IDU
   */
  IIDU addToIDU();

  /**
   * Set le IDU.
   *
   * @param IDU
   *          le nouveau IDU
   */
  void setIDU(IIDU[] IDU);

  /***************** ICD ****************/
  /**
   * Get le ICD.
   *
   * @return le ICD
   */
  @FieldXPath("ICD")
  ICD[] getICD();

  /**
   * Ajoute le to ICD.
   *
   * @return le ICD
   */
  ICD addToICD();

  /**
   * Set le ICD.
   *
   * @param ICD
   *          le nouveau ICD
   */
  void setICD(ICD[] ICD);

  /***************** ICD ****************/
  /**
   * Get le SCD.
   *
   * @return le SCD
   */
  @FieldXPath("SCD")
  SCD[] getSCD();

  /**
   * Ajoute le to SCD.
   *
   * @return le SCD
   */
  SCD addToSCD();

  /**
   * Set le SCD.
   *
   * @param SCD
   *          le nouveau SCD
   */
  void setSCD(SCD[] SCD);

}
