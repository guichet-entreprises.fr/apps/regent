package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IE75.
 */
@ResourceXPath("/IE75")
public interface IE75 {

  /**
   * Get le e751.
   *
   * @return le e751
   */
  @FieldXPath("E75.1")
  String getE751();

  /**
   * Set le e751.
   *
   * @param E751
   *          le nouveau e751
   */
  void setE751(String E751);

  /**
   * Get le e752.
   *
   * @return le e752
   */
  @FieldXPath("E75.2")
  String getE752();

  /**
   * Set le e752.
   *
   * @param E752
   *          le nouveau e752
   */
  void setE752(String E752);

}
