package fr.ge.common.regent.bean.xml.formalite.profil.creation;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;
import org.xmlfield.core.types.XmlString;

import fr.ge.common.regent.bean.xml.formalite.IFormalite;
import fr.ge.common.regent.bean.xml.transverse.IPostalCommune;

/**
 * Le Interface IProfilEntreprise.
 */
@ResourceXPath("/formalite")
public interface IProfilCreation extends IFormalite {

    /** La constante MODEL_VERSION. */
    static final int MODEL_VERSION = 1;

    /**
     * Get le activite agent commercial.
     *
     * @return le activite agent commercial
     */
    @FieldXPath(value = "activiteAgentCommercial")
    String getActiviteAgentCommercial();

    /**
     * Get le activite des creation.
     *
     * @return le activite des creation
     */
    @FieldXPath(value = "activiteDesCreation")
    String getActiviteDesCreation();

    /**
     * Get le activite dpt siege.
     *
     * @return le activite dpt siege
     */
    @FieldXPath(value = "activiteDptSiege")
    String getActiviteDptSiege();

    /**
     * Get le activite lps.
     *
     * @return le activite lps
     */
    @FieldXPath(value = "activiteLPS")
    String getActiviteLPS();

    /**
     * Get le activite non salarie.
     *
     * @return le activite non salarie
     */
    @FieldXPath(value = "activiteNonSalarie")
    String getActiviteNonSalarie();

    /**
     * Get le activite principale code activite.
     *
     * @return le activite principale code activite
     */
    @FieldXPath(value = "activitePrincipaleCodeActivite")
    String getActivitePrincipaleCodeActivite();

    /**
     * Get le activite principale domaine.
     *
     * @return le activite principale domaine
     */
    @FieldXPath(value = "activitePrincipaleDomaine")
    String getActivitePrincipaleDomaine();

    /**
     * Get le activite principale domaine1.
     *
     * @return le activite principale domaine1
     */
    @FieldXPath(value = "activitePrincipaleDomaine1")
    String getActivitePrincipaleDomaine1();

    /**
     * Get le activite principale domaine2.
     *
     * @return le activite principale domaine2
     */
    @FieldXPath(value = "activitePrincipaleDomaine2")
    String getActivitePrincipaleDomaine2();

    /**
     * Get le activite principale libelle activite.
     *
     * @return le activite principale libelle activite
     */
    @FieldXPath(value = "activitePrincipaleLibelleActivite")
    String getActivitePrincipaleLibelleActivite();

    /**
     * Get le activite principale secteur.
     *
     * @return le activite principale secteur
     */
    @FieldXPath(value = "activitePrincipaleSecteur")
    String getActivitePrincipaleSecteur();

    /**
     * Get le activite principale secteur1.
     *
     * @return le activite principale secteur1
     */
    @FieldXPath(value = "activitePrincipaleSecteur1")
    String getActivitePrincipaleSecteur1();

    /**
     * Get le activite principale secteur2.
     *
     * @return le activite principale secteur2
     */
    @FieldXPath(value = "activitePrincipaleSecteur2")
    String getActivitePrincipaleSecteur2();

    /**
     * Get le activite principale secteur3.
     *
     * @return le activite principale secteur3
     */
    @FieldXPath(value = "activitePrincipaleSecteur3")
    String getActivitePrincipaleSecteur3();

    /**
     * Get le activite secondaire code activite.
     *
     * @return le activite secondaire code activite
     */
    @FieldXPath(value = "activiteSecondaireCodeActivite")
    String getActiviteSecondaireCodeActivite();

    /**
     * Get le activite secondaire domaine.
     *
     * @return le activite secondaire domaine
     */
    @FieldXPath(value = "activiteSecondaireDomaine")
    String getActiviteSecondaireDomaine();

    /**
     * Get le activite secondaire domaine1.
     *
     * @return le activite secondaire domaine1
     */
    @FieldXPath(value = "activiteSecondaireDomaine1")
    String getActiviteSecondaireDomaine1();

    /**
     * Get le activite secondaire domaine2.
     *
     * @return le activite secondaire domaine2
     */
    @FieldXPath(value = "activiteSecondaireDomaine2")
    String getActiviteSecondaireDomaine2();

    /**
     * Get le activite secondaire libelle activite.
     *
     * @return le activite secondaire libelle activite
     */
    @FieldXPath(value = "activiteSecondaireLibelleActivite")
    String getActiviteSecondaireLibelleActivite();

    /**
     * Get le activite secondaire secteur.
     *
     * @return le activite secondaire secteur
     */
    @FieldXPath(value = "activiteSecondaireSecteur")
    String getActiviteSecondaireSecteur();

    /**
     * Get le activite secondaire secteur1.
     *
     * @return le activite secondaire secteur1
     */
    @FieldXPath(value = "activiteSecondaireSecteur1")
    String getActiviteSecondaireSecteur1();

    /**
     * Get le activite secondaire secteur2.
     *
     * @return le activite secondaire secteur2
     */
    @FieldXPath(value = "activiteSecondaireSecteur2")
    String getActiviteSecondaireSecteur2();

    /**
     * Get le activite secondaire secteur3.
     *
     * @return le activite secondaire secteur3
     */
    @FieldXPath(value = "activiteSecondaireSecteur3")
    String getActiviteSecondaireSecteur3();

    /**
     * Get le activite siege.
     *
     * @return le activite siege
     */
    @FieldXPath(value = "activiteSiege")
    String getActiviteSiege();

    /**
     * Get le ambulant.
     *
     * @return le ambulant
     */
    @FieldXPath(value = "ambulant")
    String getAmbulant();

    /**
     * Get le aqpa.
     *
     * @return le aqpa
     */
    @Override
    @FieldXPath(value = "aqpa")
    boolean getAqpa();

    /**
     * Get le code departement.
     *
     * @return le code departement
     */
    @FieldXPath(value = "codeDepartement")
    String getCodeDepartement();

    /**
     * Getter de l'attribut evenement.
     * 
     * @return la valeur de evenement
     */
    @FieldXPath(value = "evenement")
    XmlString[] getEvenement();

    /**
     * Setter de l'attribut evenement.
     * 
     * @param evenement
     *            la nouvelle valeur de evenement
     */
    void setEvenement(XmlString[] evenement);

    /**
     * Retire le from evenement.
     *
     * @param evenement
     *            le evenement
     */
    void removeFromEvenement(XmlString evenement);

    /**
     * Ajoute un indice à la liste des evenement.
     *
     * @return XmlString
     */
    XmlString addToEvenement();

    /**
     * Get le existe activite secondaire.
     *
     * @return le existe activite secondaire
     */
    @FieldXPath(value = "existeActiviteSecondaire")
    String getExisteActiviteSecondaire();

    /**
     * Get le forme juridique.
     *
     * @return le forme juridique
     */
    @FieldXPath(value = "formeJuridique")
    String getFormeJuridique();

    /**
     * Get le nom dossier.
     *
     * @return le nom dossier
     */
    @FieldXPath(value = "nomDossier")
    String getNomDossier();

    /**
     * Get le non connaissance secteur principal.
     *
     * @return le non connaissance secteur principal
     */
    @FieldXPath(value = "nonConnaissanceSecteurPrincipal")
    String getNonConnaissanceSecteurPrincipal();

    /**
     * Get le non connaissance secteur secondaire.
     *
     * @return le non connaissance secteur secondaire
     */
    @FieldXPath(value = "nonConnaissanceSecteurSecondaire")
    String getNonConnaissanceSecteurSecondaire();

    /**
     * {@inheritDoc}
     */
    @Override
    /*
     * (non-Javadoc)
     * 
     * @see fr.guichetentreprises.objetsMetier.IFormalite#getNumeroFormalite()
     */
    @FieldXPath(value = "numeroFormalite")
    String getNumeroFormalite();

    /**
     * Get le option cmacci.
     *
     * @return le option cmacci
     */
    @FieldXPath(value = "optionCMACCI")
    String getOptionCMACCI();

    /**
     * Get le option cmacc i1.
     *
     * @return le option cmacc i1
     */
    @FieldXPath(value = "optionCMACCI1")
    String getOptionCMACCI1();

    /**
     * Get le option cmacc i2.
     *
     * @return le option cmacc i2
     */
    @FieldXPath(value = "optionCMACCI2")
    String getOptionCMACCI2();

    /**
     * Get le secteur cfe.
     *
     * @return le secteur cfe
     */
    @FieldXPath(value = "secteurCfe")
    String getSecteurCfe();

    /**
     * Get le type cfe.
     *
     * @return le type cfe
     */
    @FieldXPath(value = "typeCfe")
    String getTypeCfe();

    /**
     * {@inheritDoc}
     */
    @Override
    /*
     * (non-Javadoc)
     * 
     * @see fr.guichetentreprises.objetsMetier.IFormalite#getReseauCFE()
     */
    @FieldXPath(value = "reseauCFE")
    String getReseauCFE();

    /**
     * Get le type personne.
     *
     * @return le type personne
     */
    @Override
    @FieldXPath(value = "typePersonne")
    String getTypePersonne();

    /**
     * Get le recherche activite oui non.
     *
     * @return le recherche activite oui non
     */
    @FieldXPath(value = "rechercheActiviteOuiNon")
    String getRechercheActiviteOuiNon();

    /**
     * Get le recherche activite oui non libelle.
     *
     * @return le recherche activite oui non libelle
     */
    @FieldXPath(value = "rechercheActiviteOuiNonLibelle")
    String getRechercheActiviteOuiNonLibelle();

    /**
     * Get le recherche activite.
     *
     * @return le recherche activite
     */
    @FieldXPath(value = "rechercheActivite")
    String getRechercheActivite();

    /**
     * Get le recherche activite secondaire oui non.
     *
     * @return le recherche activite secondaire oui non
     */
    @FieldXPath(value = "rechercheActiviteSecondaireOuiNon")
    String getRechercheActiviteSecondaireOuiNon();

    /**
     * Get le recherche activite secondaire.
     *
     * @return le recherche activite secondaire
     */
    @FieldXPath(value = "rechercheActiviteSecondaire")
    String getRechercheActiviteSecondaire();

    /**
     * Get le code naf principal.
     *
     * @return le code naf principal
     */
    @FieldXPath(value = "codeNafPrincipal")
    String getCodeNafPrincipal();

    /**
     * Get le code naf secondaire.
     *
     * @return le code naf secondaire
     */
    @FieldXPath(value = "codeNafSecondaire")
    String getCodeNafSecondaire();

    /**
     * Get le aut debit de boisson.
     *
     * @return le aut debit de boisson
     */
    @FieldXPath(value = "autDebitDeBoisson")
    String getAutDebitDeBoisson();

    /**
     * Get le micro social oui non.
     *
     * @return le micro social oui non
     */
    @FieldXPath(value = "microSocialOuiNon")
    String getMicroSocialOuiNon();

    /**
     * Get le eirl oui non.
     *
     * @return le eirl oui non
     */
    @FieldXPath(value = "eirlOuiNon")
    String getEirlOuiNon();

    /**
     * Get le cfe.
     *
     * @return le cfe @
     */
    @FieldXPath(value = "cfe")
    ICfe getCfe();

    /**
     * New cfe.
     *
     * @return le i cfe
     */
    ICfe newCfe();

    /**
     * Get le postal commune.
     *
     * @return le postal commune
     */
    @FieldXPath("postalCommune")
    IPostalCommune getPostalCommune();

    /**
     * New postal commune.
     *
     * @return le i postal commune
     */
    IPostalCommune newPostalCommune();

    /**
     * Set le code naf principal.
     *
     * @param codeNafPrincipal
     *            the codeNafPrincipal to set
     */
    void setCodeNafPrincipal(String codeNafPrincipal);

    /**
     * Set le code naf secondaire.
     *
     * @param codeNafSecondaire
     *            the codeNafSecondaire to set
     */
    void setCodeNafSecondaire(String codeNafSecondaire);

    /**
     * Setter de l'attribut activiteAgentCommercial.
     * 
     * @param activiteAgentCommercial
     *            la nouvelle valeur de activiteAgentCommercial
     */
    void setActiviteAgentCommercial(String activiteAgentCommercial);

    /**
     * Setter de l'attribut activiteDesCreation.
     * 
     * @param activiteDesCreation
     *            la nouvelle valeur de activiteDesCreation
     */
    void setActiviteDesCreation(String activiteDesCreation);

    /**
     * Setter de l'attribut activiteDptSiege.
     * 
     * @param activiteDptSiege
     *            la nouvelle valeur de activiteDptSiege
     */
    void setActiviteDptSiege(String activiteDptSiege);

    /**
     * setActiviteLPS.
     * 
     * @param activiteLPS
     *            activiteLPS
     */
    void setActiviteLPS(String activiteLPS);

    /**
     * Setter de l'attribut activiteNonSalarie.
     * 
     * @param activiteNonSalarie
     *            la nouvelle valeur de activiteNonSalarie
     */
    void setActiviteNonSalarie(String activiteNonSalarie);

    /**
     * Setter de l'attribut activitePrincipaleCodeActivite.
     * 
     * @param activitePrincipaleCodeActivite
     *            la nouvelle valeur de activitePrincipaleCodeActivite
     */
    void setActivitePrincipaleCodeActivite(String activitePrincipaleCodeActivite);

    /**
     * Set le activite principale domaine.
     *
     * @param activitePrincipaleDomaine
     *            the activitePrincipaleDomaine to set
     */
    void setActivitePrincipaleDomaine(String activitePrincipaleDomaine);

    /**
     * Setter de l'attribut activitePrincipaleDomaine1.
     * 
     * @param activitePrincipaleDomaine1
     *            la nouvelle valeur de activitePrincipaleDomaine1
     */
    void setActivitePrincipaleDomaine1(String activitePrincipaleDomaine1);

    /**
     * Setter de l'attribut activitePrincipaleDomaine2.
     * 
     * @param activitePrincipaleDomaine2
     *            la nouvelle valeur de activitePrincipaleDomaine2
     */
    void setActivitePrincipaleDomaine2(String activitePrincipaleDomaine2);

    /**
     * Setter de l'attribut activitePrincipaleLibelleActivite.
     * 
     * @param activitePrincipaleLibelleActivite
     *            la nouvelle valeur de activitePrincipaleLibelleActivite
     */
    void setActivitePrincipaleLibelleActivite(String activitePrincipaleLibelleActivite);

    /**
     * Set le activite principale secteur.
     *
     * @param activitePrincipaleSecteur
     *            the activitePrincipaleSecteur to set
     */
    void setActivitePrincipaleSecteur(String activitePrincipaleSecteur);

    /**
     * Setter de l'attribut activitePrincipaleSecteur1.
     * 
     * @param activitePrincipaleSecteur1
     *            la nouvelle valeur de activitePrincipaleSecteur1
     */
    void setActivitePrincipaleSecteur1(String activitePrincipaleSecteur1);

    /**
     * Setter de l'attribut activitePrincipaleSecteur2.
     * 
     * @param activitePrincipaleSecteur2
     *            la nouvelle valeur de activitePrincipaleSecteur2
     */
    void setActivitePrincipaleSecteur2(String activitePrincipaleSecteur2);

    /**
     * Setter de l'attribut activiteSecondaireCodeActivite.
     * 
     * @param activiteSecondaireCodeActivite
     *            la nouvelle valeur de activiteSecondaireCodeActivite
     */
    void setActiviteSecondaireCodeActivite(String activiteSecondaireCodeActivite);

    /**
     * Set le activite secondaire domaine.
     *
     * @param activiteSecondaireDomaine
     *            the activiteSecondaireDomaine to set
     */
    void setActiviteSecondaireDomaine(String activiteSecondaireDomaine);

    /**
     * Setter de l'attribut activiteSecondaireDomaine1.
     * 
     * @param activiteSecondaireDomaine1
     *            la nouvelle valeur de activiteSecondaireDomaine1
     */
    void setActiviteSecondaireDomaine1(String activiteSecondaireDomaine1);

    /**
     * Setter de l'attribut activiteSecondaireDomaine2.
     * 
     * @param activiteSecondaireDomaine2
     *            la nouvelle valeur de activiteSecondaireDomaine2
     */
    void setActiviteSecondaireDomaine2(String activiteSecondaireDomaine2);

    /**
     * Setter de l'attribut activiteSecondaireLibelleActivite.
     * 
     * @param activiteSecondaireLibelleActivite
     *            la nouvelle valeur de activiteSecondaireLibelleActivite
     */
    void setActiviteSecondaireLibelleActivite(String activiteSecondaireLibelleActivite);

    /**
     * Set le activite secondaire secteur.
     *
     * @param activiteSecondaireSecteur
     *            the activiteSecondaireSecteur to set
     */
    void setActiviteSecondaireSecteur(String activiteSecondaireSecteur);

    /**
     * Setter de l'attribut activiteSecondaireSecteur1.
     * 
     * @param activiteSecondaireSecteur1
     *            la nouvelle valeur de activiteSecondaireSecteur1
     */
    void setActiviteSecondaireSecteur1(String activiteSecondaireSecteur1);

    /**
     * Setter de l'attribut activiteSecondaireSecteur2.
     * 
     * @param activiteSecondaireSecteur2
     *            la nouvelle valeur de activiteSecondaireSecteur2
     */
    void setActiviteSecondaireSecteur2(String activiteSecondaireSecteur2);

    /**
     * Setter de l'attribut activiteSiege.
     * 
     * @param activiteSiege
     *            la nouvelle valeur de activiteSiege
     */
    void setActiviteSiege(String activiteSiege);

    /**
     * Setter de l'attribut ambulant.
     * 
     * @param ambulant
     *            la nouvelle valeur de ambulant
     */
    void setAmbulant(String ambulant);

    /**
     * setAqpa.
     * 
     * @param aqpa
     *            aqpa
     */
    @Override
    void setAqpa(boolean aqpa);

    /**
     * Set le cfe.
     *
     * @param cfe
     *            the cfe to set
     */
    void setCfe(ICfe cfe);

    /**
     * Ajoute le to cfe.
     *
     * @param cfe
     *            le cfe
     */
    void addToCfe(ICfe cfe);

    /**
     * Set le code departement.
     *
     * @param codeDepartement
     *            the codeDepartement to set
     */
    void setCodeDepartement(String codeDepartement);

    /**
     * Set le evenement.
     *
     * @param evenement
     *            the evenement to set
     */
    void setEvenement(String evenement);

    /**
     * Setter de l'attribut existeActiviteSecondaire.
     * 
     * @param existeActiviteSecondaire
     *            la nouvelle valeur de existeActiviteSecondaire
     */
    void setExisteActiviteSecondaire(String existeActiviteSecondaire);

    /**
     * Setter de l'attribut formeJuridique.
     * 
     * @param formeJuridique
     *            la nouvelle valeur de formeJuridique
     */
    void setFormeJuridique(String formeJuridique);

    /**
     * Setter de l'attribut nomDossier.
     * 
     * @param nomDossier
     *            la nouvelle valeur de nomDossier
     */
    void setNomDossier(String nomDossier);

    /**
     * Setter de l'attribut nonConnaissanceSecteurPrincipal.
     * 
     * @param nonConnaissanceSecteurPrincipal
     *            la nouvelle valeur de nonConnaissanceSecteurPrincipal
     */
    void setNonConnaissanceSecteurPrincipal(String nonConnaissanceSecteurPrincipal);

    /**
     * Setter de l'attribut nonConnaissanceSecteurSecondaire.
     * 
     * @param nonConnaissanceSecteurSecondaire
     *            la nouvelle valeur de nonConnaissanceSecteurSecondaire
     */
    void setNonConnaissanceSecteurSecondaire(String nonConnaissanceSecteurSecondaire);

    /**
     * Setter de l'attribut numeroFormalite.
     * 
     * @param numeroFormalite
     *            la nouvelle valeur de numeroFormalite
     */
    @Override
    void setNumeroFormalite(String numeroFormalite);

    /**
     * Setter de l'attribut optionCMACCI.
     * 
     * @param optionCMACCI
     *            la nouvelle valeur de optionCMACCI
     */
    void setOptionCMACCI(String optionCMACCI);

    /**
     * setOptionCMACCI1.
     * 
     * @param optionCMACCI1
     *            optionCMACCI1
     */
    void setOptionCMACCI1(String optionCMACCI1);

    /**
     * setOptionCMACCI2.
     * 
     * @param optionCMACCI2
     *            optionCMACCI2
     */
    void setOptionCMACCI2(String optionCMACCI2);

    /**
     * Setter de l'attribut postalcommune.
     * 
     * @param postalcommune
     *            la nouvelle valeur de postalcommune
     */
    void setPostalCommune(IPostalCommune postalcommune);

    /**
     * Ajoute le to postal commune.
     *
     * @param postalCommune
     *            le postal commune
     */
    void addToPostalCommune(IPostalCommune postalCommune);

    /**
     * Set le secteur cfe.
     *
     * @param secteurCfe
     *            the secteurCfe to set
     */
    void setSecteurCfe(String secteurCfe);

    /**
     * Set le type cfe.
     *
     * @param typeCfe
     *            the typeCfe to set
     */
    void setTypeCfe(String typeCfe);

    /**
     * Set le reseau cfe.
     *
     * @param reseauCFE
     *            the reseauCFE to set
     */
    @Override
    void setReseauCFE(String reseauCFE);

    /**
     * Setter de l'attribut typePersonne.
     * 
     * @param typePersonne
     *            la nouvelle valeur de typePersonne
     */
    @Override
    void setTypePersonne(String typePersonne);

    /**
     * Setter de l'attribut setRechercheActiviteOuiNon.
     * 
     * @param rechercheActiviteOuiNon
     *            la nouvelle valeur de rechercheActiviteOuiNon
     */
    void setRechercheActiviteOuiNon(String rechercheActiviteOuiNon);

    /**
     * Setter de l'attribut rechercheActivite.
     * 
     * @param rechercheActivite
     *            la nouvelle valeur de rechercheActivite
     */
    void setRechercheActivite(String rechercheActivite);

    /**
     * Setter de l'attribut setRechercheActivitSecondaireeOuiNon.
     *
     * @param rechercheActiviteSecondaireOuiNon
     *            le nouveau recherche activite secondaire oui non
     */
    void setRechercheActiviteSecondaireOuiNon(String rechercheActiviteSecondaireOuiNon);

    /**
     * Setter de l'attribut rechercheActiviteSecondaire.
     *
     * @param rechercheActiviteSecondaire
     *            le nouveau recherche activite secondaire
     */
    void setRechercheActiviteSecondaire(String rechercheActiviteSecondaire);

    /**
     * Set le activite principale secteur3.
     *
     * @param activitePrincipaleSecteur3
     *            the activitePrincipaleSecteur3 to set
     */
    void setActivitePrincipaleSecteur3(String activitePrincipaleSecteur3);

    /**
     * Set le activite secondaire secteur3.
     *
     * @param activiteSecondaireSecteur3
     *            the activiteSecondaireSecteur3 to set
     */
    void setActiviteSecondaireSecteur3(String activiteSecondaireSecteur3);

    /**
     * Set le recherche activite oui non libelle.
     *
     * @param rechercheActiviteOuiNonLibelle
     *            the rechercheActiviteOuiNonLibelle to set
     */
    void setRechercheActiviteOuiNonLibelle(String rechercheActiviteOuiNonLibelle);

    /**
     * Set le aut debit de boisson.
     *
     * @param autDebitDeBoisson
     *            autDebitDeBoisson the autDebitDeBoisson to set
     */
    void setAutDebitDeBoisson(String autDebitDeBoisson);

    /**
     * Set le micro social oui non.
     *
     * @param microSocialOuiNon
     *            le nouveau micro social oui non
     */
    void setMicroSocialOuiNon(String microSocialOuiNon);

    /**
     * Set le eirl oui non.
     *
     * @param eirlOuiNon
     *            le nouveau eirl oui non
     */
    void setEirlOuiNon(String eirlOuiNon);

}
