package fr.ge.common.regent.bean.modele.referentiel;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

/**
 * Le Class ENormeDestinataire.
 */
@Entity
@Proxy(lazy = false)
@Table(name = "norme_destinataire")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class ENormeDestinataire implements Serializable {

  /** La constante serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** Le id. */
  @Id
  @Column(name = "CH_NUM", nullable = false)
  private Integer id;

  /** Le role. */
  @Column(name = "CH_ROLE", nullable = false)
  private String role;

  /** Le destinataire. */
  @Column(name = "CH_DESTINATAIRE", nullable = false)
  private String destinataire;

  /** Le rubrique. */
  @Column(name = "CH_RUBRIQUE", nullable = false)
  private String rubrique;

  /**
   * Get le id.
   *
   * @return le id
   */
  public Integer getId() {
    return id;
  }

  /**
   * Set le id.
   *
   * @param id
   *          le nouveau id
   */
  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * Get le role.
   *
   * @return le role
   */
  public String getRole() {
    return role;
  }

  /**
   * Set le role.
   *
   * @param role
   *          le nouveau role
   */
  public void setRole(String role) {
    this.role = role;
  }

  /**
   * Get le destinataire.
   *
   * @return le destinataire
   */
  public String getDestinataire() {
    return destinataire;
  }

  /**
   * Set le destinataire.
   *
   * @param destinataire
   *          le nouveau destinataire
   */
  public void setDestinataire(String destinataire) {
    this.destinataire = destinataire;
  }

  /**
   * Get le rubrique.
   *
   * @return le rubrique
   */
  public String getRubrique() {
    return rubrique;
  }

  /**
   * Set le rubrique.
   *
   * @param rubrique
   *          le nouveau rubrique
   */
  public void setRubrique(String rubrique) {
    this.rubrique = rubrique;
  }

}
