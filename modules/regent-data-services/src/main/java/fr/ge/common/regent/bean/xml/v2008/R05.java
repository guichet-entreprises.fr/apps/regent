/**
 * 
 */
package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Interface R05.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
@ResourceXPath("/R05")
public interface R05 {

  /**
   * Accesseur sur l'attribut {@link #r053}.
   *
   * @return r053
   */
  @FieldXPath("R05.3")
  String getR053();

  /**
   * Mutateur sur l'attribut {@link #r053}.
   *
   * @param R053
   *          la nouvelle valeur de l'attribut r053
   */
  void setR053(String R053);

  /**
   * Accesseur sur l'attribut {@link #r055}.
   *
   * @return r055
   */
  @FieldXPath("R05.5")
  String getR055();

  /**
   * Mutateur sur l'attribut {@link #r055}.
   *
   * @param R055
   *          la nouvelle valeur de l'attribut r055
   */
  void setR055(String R055);

  /**
   * Accesseur sur l'attribut {@link #r056}.
   *
   * @return r056
   */
  @FieldXPath("R05.6")
  String getR056();

  /**
   * Mutateur sur l'attribut {@link #r056}.
   *
   * @param R056
   *          la nouvelle valeur de l'attribut r056
   */
  void setR056(String R056);

  /**
   * Accesseur sur l'attribut {@link #r057}.
   *
   * @return r057
   */
  @FieldXPath("R05.7")
  String getR057();

  /**
   * Mutateur sur l'attribut {@link #r057}.
   *
   * @param R057
   *          la nouvelle valeur de l'attribut r057
   */
  void setR057(String R057);

  /**
   * Accesseur sur l'attribut {@link #r058}.
   *
   * @return r058
   */
  @FieldXPath("R05.8")
  String getR058();

  /**
   * Mutateur sur l'attribut {@link #r058}.
   *
   * @param R058
   *          la nouvelle valeur de l'attribut r058
   */
  void setR058(String R058);

  /**
   * Accesseur sur l'attribut {@link #r0510}.
   *
   * @return r0510
   */
  @FieldXPath("R05.10")
  String getR0510();

  /**
   * Mutateur sur l'attribut {@link #r0510}.
   *
   * @param R0510
   *          la nouvelle valeur de l'attribut r0510
   */
  void setR0510(String R0510);

  /**
   * Accesseur sur l'attribut {@link #r0511}.
   *
   * @return r0511
   */
  @FieldXPath("R05.11")
  String getR0511();

  /**
   * Mutateur sur l'attribut {@link #r0511}.
   *
   * @param R0511
   *          la nouvelle valeur de l'attribut r0511
   */
  void setR0511(String R0511);

  /**
   * Accesseur sur l'attribut {@link #r0512}.
   *
   * @return r0512
   */
  @FieldXPath("R05.12")
  String getR0512();

  /**
   * Mutateur sur l'attribut {@link #r0512}.
   *
   * @param R0512
   *          la nouvelle valeur de l'attribut r0512
   */
  void setR0512(String R0512);

  /**
   * Accesseur sur l'attribut {@link #r0513}.
   *
   * @return r0513
   */
  @FieldXPath("R05.13")
  String getR0513();

  /**
   * Mutateur sur l'attribut {@link #r0513}.
   *
   * @param R0513
   *          la nouvelle valeur de l'attribut r0513
   */
  void setR0513(String R0513);

  /**
   * Accesseur sur l'attribut {@link #r0514}.
   *
   * @return r0514
   */
  @FieldXPath("R05.14")
  String getR0514();

  /**
   * Mutateur sur l'attribut {@link #r0514}.
   *
   * @param R0514
   *          la nouvelle valeur de l'attribut r0514
   */
  void setR0514(String R0514);
}
