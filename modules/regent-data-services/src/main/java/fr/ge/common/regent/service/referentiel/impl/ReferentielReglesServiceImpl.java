/**
 * 
 */
package fr.ge.common.regent.service.referentiel.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import fr.ge.common.regent.bean.IXmlRegent;
import fr.ge.common.regent.persistance.dao.referentiel.ReglesRegentDao;
import fr.ge.common.regent.service.referentiel.ReferentielReglesService;

/**
 * Implémentation de l'interface {@link ReferentielReglesService}.
 * 
 * @author $Author: fbeaurai $
 * @version $Revision: 0 $
 */
public class ReferentielReglesServiceImpl implements ReferentielReglesService {

    /** String id. */
    private static final String ID = "id";

    /** Le regles regent dao. */
    @Autowired
    private ReglesRegentDao reglesRegentDao;

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true, value = "regent")
    public List<IXmlRegent> recupererToutesLesReglesRegent() {
        List<IXmlRegent> dest = new ArrayList<IXmlRegent>();
        dest.addAll(this.reglesRegentDao.getAll(ID));
        return dest;
    }

}
