package fr.ge.common.regent.bean.formalite.dialogue.creation;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import fr.ge.common.regent.bean.formalite.dialogue.AbstractConjointBean;

/**
 * Le Class ConjointBean.
 */
public class ConjointBean extends AbstractConjointBean implements IFormaliteVue {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 8907569932323750237L;

    /** Le pp adresse. */
    private AdresseBean ppAdresse = new AdresseBean();

    /** Le pp adresse different gerant. */
    private String ppAdresseDifferentGerant;

    /** Le pp civilite. */
    private String ppCivilite;

    /**
     * Instancie un nouveau conjoint bean.
     */
    public ConjointBean() {
        super();
    }

    /**
     * Getter de l'attribut ppAdresse.
     * 
     * @return la valeur de ppAdresse
     */
    public AdresseBean getPpAdresse() {
        return this.ppAdresse;
    }

    /**
     * getPpAdresseDifferentGerant.
     * 
     * @return ppAdresseDifferentGerant
     */
    public String getPpAdresseDifferentGerant() {
        return this.ppAdresseDifferentGerant;
    }

    /**
     * Getter de l'attribut ppCivilite.
     * 
     * @return la valeur de ppCivilite
     */
    public String getPpCivilite() {
        return this.ppCivilite;
    }

    /**
     * Setter de l'attribut ppAdresse.
     * 
     * @param ppAdresse
     *            la nouvelle valeur de ppAdresse
     */
    public void setPpAdresse(AdresseBean ppAdresse) {
        this.ppAdresse = ppAdresse;
    }

    /**
     * setPpAdresseDifferentGerant.
     * 
     * @param ppAdresseDifferentGerant
     *            ppAdresseDifferentGerant
     */
    public void setPpAdresseDifferentGerant(String ppAdresseDifferentGerant) {
        this.ppAdresseDifferentGerant = ppAdresseDifferentGerant;
    }

    /**
     * Setter de l'attribut ppCivilite.
     * 
     * @param ppCivilite
     *            la nouvelle valeur de ppCivilite
     */
    public void setPpCivilite(String ppCivilite) {
        this.ppCivilite = ppCivilite;
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see fr.guichetentreprises.vue.AbstractConjointBean#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

}
