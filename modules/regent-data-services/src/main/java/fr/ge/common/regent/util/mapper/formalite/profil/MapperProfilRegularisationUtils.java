package fr.ge.common.regent.util.mapper.formalite.profil;

import java.util.ArrayList;

import org.apache.commons.collections.CollectionUtils;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xmlfield.core.types.XmlString;

import fr.ge.common.regent.bean.formalite.dialogue.creation.PostalCommuneBean;
import fr.ge.common.regent.bean.formalite.profil.creation.CfeBean;
import fr.ge.common.regent.bean.formalite.profil.regularisation.ProfilRegularisationBean;
import fr.ge.common.regent.bean.xml.formalite.profil.regularisation.IProfilRegularisation;
import fr.ge.common.regent.util.mapper.formalite.IMapperFormaliteUtils;

/**
 * Le Class MapperProfilRegularisationUtils.
 */
@Component
public class MapperProfilRegularisationUtils implements IMapperFormaliteUtils<IProfilRegularisation, ProfilRegularisationBean> {

    /** Le profil entreprise r mapper. */
    @Autowired
    private Mapper profilEntrepriseRMapper;

    /**
     * Mapper.
     *
     * @param iProfilEntrepriseRegularisation
     *            le i profil entreprise regularisation
     * @param profilRegulBean
     *            le profil regul bean
     */
    public void mapper(IProfilRegularisation iProfilEntrepriseRegularisation, ProfilRegularisationBean profilRegulBean) {
        // Instanciation des attributs de type complexe
        profilRegulBean.setCfe(new CfeBean());
        profilRegulBean.setPostalCommune(new PostalCommuneBean());

        profilRegulBean.setModifEvenements(new ArrayList<String>());
        profilRegulBean.setModifEvenements2(new ArrayList<String>());
        profilRegulBean.setEvenement(new ArrayList<String>());

        // Mapping des champs simples
        this.getProfilEntrepriseRMapper().map(iProfilEntrepriseRegularisation, profilRegulBean);

        // Traitement des Listes (arrays)
        if (iProfilEntrepriseRegularisation.getEvenement() != null && iProfilEntrepriseRegularisation.getEvenement().length > 0) {
            for (XmlString evenement : iProfilEntrepriseRegularisation.getEvenement()) {
                profilRegulBean.getEvenement().add(evenement.getString());
            }
        }
        if (iProfilEntrepriseRegularisation.getModifEvenements() != null && iProfilEntrepriseRegularisation.getModifEvenements().length > 0) {
            for (XmlString evenement : iProfilEntrepriseRegularisation.getModifEvenements()) {
                profilRegulBean.getModifEvenements().add(evenement.getString());
            }
        }
        if (iProfilEntrepriseRegularisation.getModifEvenements2() != null && iProfilEntrepriseRegularisation.getModifEvenements2().length > 0) {
            for (XmlString evenement : iProfilEntrepriseRegularisation.getModifEvenements2()) {
                profilRegulBean.getModifEvenements2().add(evenement.getString());
            }
        }
    }

    /**
     * Mapper.
     *
     * @param profilRegulBean
     *            le profil regul bean
     * @param iProfilEntrepriseRegularisation
     *            le i profil entreprise regularisation
     */
    public void mapper(ProfilRegularisationBean profilRegulBean, IProfilRegularisation iProfilEntrepriseRegularisation) {
        /* Ajouter les objets */
        iProfilEntrepriseRegularisation.newCfe();
        iProfilEntrepriseRegularisation.newPostalCommune();

        /* Mapper les champs simples. */
        this.getProfilEntrepriseRMapper().map(profilRegulBean, iProfilEntrepriseRegularisation);

        /* Ajouter les évenemnts */
        if (!CollectionUtils.isEmpty(profilRegulBean.getEvenement())) {
            this.addEvenements(profilRegulBean, iProfilEntrepriseRegularisation);
        }
        if (!CollectionUtils.isEmpty(profilRegulBean.getModifEvenements())) {
            this.addModifEvenemnts(profilRegulBean, iProfilEntrepriseRegularisation);
        }
        if (!CollectionUtils.isEmpty(profilRegulBean.getModifEvenements2())) {
            this.addModifEvenemnts2(profilRegulBean, iProfilEntrepriseRegularisation);
        }

    }

    /**
     * Ajoute le modif evenemnts2.
     *
     * @param profilRegulBean
     *            le profil regul bean
     * @param iProfilEntrepriseRegularisation
     *            le i profil entreprise regularisation
     */
    private void addModifEvenemnts2(ProfilRegularisationBean profilRegulBean, IProfilRegularisation iProfilEntrepriseRegularisation) {
        /*
         * parcourir la liste des évenements existants sur l'interface XML et
         * les supprimer
         */
        for (XmlString evenement : iProfilEntrepriseRegularisation.getModifEvenements2()) {
            iProfilEntrepriseRegularisation.removeFromModifEvenements2(evenement);
        }
        /*
         * parcourir la liste des évenements dans le Bean pour les rajouter à
         * l'interface
         */
        for (String evenement : profilRegulBean.getModifEvenements2()) {
            XmlString evenemnt = iProfilEntrepriseRegularisation.addToModifEvenements2();
            evenemnt.setString(evenement);
        }

    }

    /**
     * Ajoute le modif evenemnts.
     *
     * @param profilRegularisationBean
     *            le profil regularisation bean
     * @param iProfilEntrepriseRegularisation
     *            le i profil entreprise regularisation
     */
    private void addModifEvenemnts(ProfilRegularisationBean profilRegularisationBean, IProfilRegularisation iProfilEntrepriseRegularisation) {
        /*
         * parcourir la liste des évenements existants sur l'interface XML et
         * les supprimer
         */
        for (XmlString evenement : iProfilEntrepriseRegularisation.getModifEvenements()) {
            iProfilEntrepriseRegularisation.removeFromModifEvenements(evenement);
        }
        /*
         * parcourir la liste des évenements dans le Bean pour les rajouter à
         * l'interface
         */
        for (String evenement : profilRegularisationBean.getModifEvenements()) {
            XmlString evenemnt = iProfilEntrepriseRegularisation.addToModifEvenements();
            evenemnt.setString(evenement);
        }

    }

    /**
     * Ajoute le evenements.
     *
     * @param profilRegularisationBean
     *            le profil regularisation bean
     * @param iProfilEntrepriseRegularisation
     *            le i profil entreprise regularisation
     */
    private void addEvenements(ProfilRegularisationBean profilRegularisationBean, IProfilRegularisation iProfilEntrepriseRegularisation) {
        /*
         * parcourir la liste des évenements existants sur l'interface XML et
         * les supprimer
         */
        for (XmlString evenement : iProfilEntrepriseRegularisation.getEvenement()) {
            iProfilEntrepriseRegularisation.removeFromEvenement(evenement);
        }
        /*
         * parcourir la liste des évenements dans le Bean pour les rajouter à
         * l'interface
         */
        for (String evenement : profilRegularisationBean.getEvenement()) {
            XmlString evenemntNew = iProfilEntrepriseRegularisation.addToEvenement();
            evenemntNew.setString(evenement);
        }

    }

    /**
     * Get le profil entreprise r mapper.
     *
     * @return the profilEntrepriseRMapper
     */
    public Mapper getProfilEntrepriseRMapper() {
        return this.profilEntrepriseRMapper;
    }

    /**
     * Set le profil entreprise r mapper.
     *
     * @param profilEntrepriseRMapper
     *            the profilEntrepriseRMapper to set
     */
    public void setProfilEntrepriseRMapper(Mapper profilEntrepriseRMapper) {
        this.profilEntrepriseRMapper = profilEntrepriseRMapper;
    }

}
