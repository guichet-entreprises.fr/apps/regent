package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IE13.
 */
@ResourceXPath("/E13")
public interface IE13 {

  /**
   * Get le e131.
   *
   * @return le e131
   */
  @FieldXPath("E13.1")
  String getE131();

  /**
   * Set le e131.
   *
   * @param E131
   *          le nouveau e131
   */
  void setE131(String E131);

  /**
   * Get le e132.
   *
   * @return le e132
   */
  @FieldXPath("E13.2")
  String getE132();

  /**
   * Set le e132.
   *
   * @param E132
   *          le nouveau e132
   */
  void setE132(String E132);

}
