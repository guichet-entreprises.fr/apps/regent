package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IA55.
 */
@ResourceXPath("/A55")
public interface IA55 {

  /**
   * Get le a551.
   *
   * @return le a551
   */
  @FieldXPath("A55.1")
  String getA551();

  /**
   * Set le a551.
   *
   * @param A551
   *          le nouveau a551
   */
  void setA551(String A551);

  /**
   * Get le a552.
   *
   * @return le a552
   */
  @FieldXPath("A55.2")
  String getA552();

  /**
   * Set le a a552.
   *
   * @param A552
   *          le nouveau a a552
   */
  void setA552(String A552);

}
