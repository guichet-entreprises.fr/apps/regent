package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface A21.
 */
@ResourceXPath("/A21")
public interface IA21 {

  /**
   * Get le A212.
   *
   * @return le A212
   */
  @FieldXPath("A21.2")
  String getA212();

  /**
   * Set le A212.
   *
   * @param A212
   *          le nouveau A212
   */
  void setA212(String A212);

  /**
   * Get le A213.
   *
   * @return le A213
   */
  @FieldXPath("A21.3")
  String getA213();

  /**
   * Set le A213.
   *
   * @param A213
   *          le nouveau A213
   */
  void setA213(String A213);

  /**
   * Get le A214.
   *
   * @return le A214
   */
  @FieldXPath("A21.4")
  String getA214();

  /**
   * Set le A214.
   *
   * @param A214
   *          le nouveau A214
   */
  void setA214(String A214);

  /**
   * Get le A215.
   *
   * @return le A215
   */
  @FieldXPath("A21.5")
  String getA215();

  /**
   * Set le A215.
   *
   * @param A215
   *          le nouveau A215
   */
  void setA215(String A215);

  /**
   * Get le A216.
   *
   * @return le A216
   */
  @FieldXPath("A21.6")
  String getA216();

  /**
   * Set le A216.
   *
   * @param A216
   *          le nouveau A216
   */
  void setA216(String A216);

}
