package fr.ge.common.regent.bean.formalite.dialogue;

/**
 * Factory permettant de creer un objet simple/complexe/composite.
 *
 * @param <T>
 *            le type generique
 * @param <E>
 *            le type de l'element
 */
public interface IEntrepriseEtabFactory<T extends AbstractEtablissementBean, E extends AbstractEntrepriseLieeBean> {

    /**
     * Creation d'un nouvel objet pour l'établissement.
     *
     * @return T
     */
    public T createNewEtablissement();

    /**
     * Creation d'un nouvel objet pour l'établissement.
     *
     * @return E
     */
    public E createNewEntrepriseLiee();

}
