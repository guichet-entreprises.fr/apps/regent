package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * L'interface NGM.
 */
@ResourceXPath("/NGM")
public interface INGM {

  /**
   * Get le m27.
   *
   * @return le m27
   */
  @FieldXPath("M27")
  String getM27();

  /**
   * Set le m27.
   *
   * @param m27
   *          le nouveau m27
   */
  void setM27(String m27);
}
