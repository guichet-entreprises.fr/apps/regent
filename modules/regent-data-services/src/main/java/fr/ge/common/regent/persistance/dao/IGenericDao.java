package fr.ge.common.regent.persistance.dao;

import java.io.Serializable;
import java.util.List;

/**
 * Classe générique d'accès aux données en base. Elle permet d'effectuer des
 * opération simple : ajouter, supprimer, mettre à jour, récupérer un ou tous
 * les éléments d'une table.
 * 
 * @param <T>
 *            Type de la table correspondante
 * @param <U>
 *            Type de la clé primaire de la table
 */
public interface IGenericDao<T, U extends Serializable> {

    /**
     * Récupère dans la table de T un élément en fonction de son identifiant.
     * 
     * @param uuid
     *            l'identifiant de l'élément à récupérer
     * @return l'objet
     */
    T get(U uuid);

    /**
     * Ajoute un élément en base.
     * 
     * @param o
     *            l'élément a ajouter
     * @return l'identifiant de l'élément enregistré en base
     */
    U save(T o);

    /**
     * Retourne la liste complete des élément de la table correspondante à la
     * classe T.
     * 
     * @param column
     *            la colonne a utiliser pour le tri
     * @return la liste avec tous les éléments de la table
     */
    List<T> getAll(String column);

}