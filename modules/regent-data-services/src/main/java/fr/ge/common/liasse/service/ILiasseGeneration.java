/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 */

package fr.ge.common.liasse.service;

/**
 * ILiasseNumberGeneration to get the next value of liasse number
 * 
 * @author mtakerra
 *
 */
public interface ILiasseGeneration {

    /**
     * Récupère la valeur courante formatée du numero de liasse du prochain
     * dossier.
     * 
     * @return numero de liasse a assigner à un dossier au format
     *         "H1000<numero7chiffres>"
     */
    public String getNumeroLiasse();

}
