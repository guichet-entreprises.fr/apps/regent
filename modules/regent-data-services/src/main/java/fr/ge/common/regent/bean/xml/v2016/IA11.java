package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Interface IA10.
 */
@ResourceXPath("/A11")
public interface IA11 {

  /**
   * Get le A11.1.
   *
   * @return le A11.1
   */
  @FieldXPath("A11.1")
  String getA111();

  /**
   * Set le A111.
   *
   * @param A111
   *          le nouveau A111
   */
  void setA111(String A111);

  /**
   * Get le A112.
   *
   * @return le A112
   */
  @FieldXPath("A11.2")
  String getA112();

  /**
   * Set le A102.
   *
   * @param A102
   *          le nouveau A102
   */
  void setA112(String A112);

}
