package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IA53.
 */
@ResourceXPath("/A53")
public interface IA53 {

  /**
   * Get le a534.
   *
   * @return le a534
   */
  @FieldXPath("A53.4")
  String getA534();

  /**
   * Set le a534.
   *
   * @param A534
   *          le nouveau a534
   */
  void setA534(String A534);

}
