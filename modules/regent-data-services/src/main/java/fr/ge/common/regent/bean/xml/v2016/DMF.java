package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;
import org.xmlfield.core.types.XmlString;

/**
 * Le Interface DMF.
 */
@ResourceXPath("/DMF")
public interface DMF {

  /**
   * Get le c20.
   *
   * @return le c20
   */
  @FieldXPath("C20")
  XmlString[] getC20();

  /**
   * Ajoute le to c20.
   *
   * @return le xml string
   */
  XmlString addToC20();

  /**
   * Set le c20.
   *
   * @param C20
   *          le nouveau c20
   */
  void setC20(XmlString[] C20);
}
