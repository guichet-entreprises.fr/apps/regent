package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Interface IP95.
 */
@ResourceXPath("/P95")
public interface IP95 {
	
	  /**
	   * Get le p95.1.
	   *
	   * @return le p95.1
	   */
	  @FieldXPath("P95.1")
	  String getP951();

	  /**
	   * Set le p95.1.
	   *
	   * @param P95.1
	   *          le nouveau p95.1
	   */
	  void setP951(String P951);
	  
	  /**
	   * Get le p95.2.
	   *
	   * @return le p95.2
	   */
	  @FieldXPath("P95.2")
	  String getP952();

	  /**
	   * Set le p95.2.
	   *
	   * @param P95.2
	   *          le nouveau p95.2
	   */
	  void setP952(String P952);
	  
	  /**
	   * Get le p95.4.
	   *
	   * @return le p95.4
	   */
	  @FieldXPath("P95.4")
	  String getP954();

	  /**
	   * Set le p95.4.
	   *
	   * @param P95.4
	   *          le nouveau p95.4
	   */
	  void setP954(String P954);
	  
	  /**
	   * Get le p95.5.
	   *
	   * @return le p95.5
	   */
	  @FieldXPath("P95.5")
	  String getP955();

	  /**
	   * Set le p95.5.
	   *
	   * @param P95.5
	   *          le nouveau p95.5
	   */
	  void setP955(String P955);
	  
	  /**
	   * Get le P95.3.
	   *
	   * @return le P95.3
	   */
	  @FieldXPath("P95.3")
	  IP953[] getP953();

	  /**
	   * Ajoute le to P953.
	   *
	   * @return le i P953
	   */
	  IP953 addToP953();

	  /**
	   * Set le u11.
	   *
	   * @param U11
	   *          le nouveau u11
	   */
	  void setP953(IP953[] P953);

}
