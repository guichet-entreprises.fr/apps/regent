package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * JQU.
 * 
 * @author $Author: hhichri $
 * @version $Revision: 0 $
 */
@ResourceXPath("/JQU")
public interface JQU {

  /**
   * Get le u60.
   *
   * @return le u60
   */
  @FieldXPath("U60")
  String getU60();

  /**
   * Set le u60.
   *
   * @param U60
   *          le nouveau u60
   */
  void setU60(String U60);

  /**
   * Get le U61.
   *
   * @return le U61
   */
  @FieldXPath("U61")
  IU61[] getU61();

  /**
   * Ajoute le to U61.
   *
   * @return le U61
   */
  IU61 addToU61();

  /**
   * Set le U61.
   *
   * @param U61
   *          le nouveau U61
   */
  void setU61(IU61[] U61);

  /**
   * Get le U62.
   *
   * @return le U62
   */
  @FieldXPath("U62")
  IU62[] getU62();

  /**
   * Ajoute le to U62.
   *
   * @return le U62
   */
  IU62 addToU62();

  /**
   * Set le U62.
   *
   * @param U62
   *          le nouveau U62
   */
  void setU62(IU62[] U62);
}
