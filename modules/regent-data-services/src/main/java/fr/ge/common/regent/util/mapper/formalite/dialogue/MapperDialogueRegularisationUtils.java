package fr.ge.common.regent.util.mapper.formalite.dialogue;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xmlfield.core.types.XmlString;

import fr.ge.common.regent.bean.formalite.dialogue.creation.AdresseBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.AutreEtablissementUEBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.PostalCommuneBean;
import fr.ge.common.regent.bean.formalite.dialogue.regularisation.ActiviteSoumiseQualificationBean;
import fr.ge.common.regent.bean.formalite.dialogue.regularisation.ConjointBean;
import fr.ge.common.regent.bean.formalite.dialogue.regularisation.DialogueRegularisationBean;
import fr.ge.common.regent.bean.formalite.dialogue.regularisation.DirigeantBean;
import fr.ge.common.regent.bean.formalite.dialogue.regularisation.EntrepriseBean;
import fr.ge.common.regent.bean.formalite.dialogue.regularisation.EntrepriseLieeBean;
import fr.ge.common.regent.bean.formalite.dialogue.regularisation.EtablissementBean;
import fr.ge.common.regent.bean.formalite.dialogue.regularisation.PersonneLieeBean;
import fr.ge.common.regent.bean.xml.formalite.dialogue.regularisation.IActiviteSoumiseQualificationRegularisation;
import fr.ge.common.regent.bean.xml.formalite.dialogue.regularisation.IAutreEtablissementUE;
import fr.ge.common.regent.bean.xml.formalite.dialogue.regularisation.IConjointRegularisation;
import fr.ge.common.regent.bean.xml.formalite.dialogue.regularisation.IDialogueRegularisation;
import fr.ge.common.regent.bean.xml.formalite.dialogue.regularisation.IDirigeantRegularisation;
import fr.ge.common.regent.bean.xml.formalite.dialogue.regularisation.IEntrepriseLieeRegularisation;
import fr.ge.common.regent.bean.xml.formalite.dialogue.regularisation.IEntrepriseRegularisation;
import fr.ge.common.regent.bean.xml.formalite.dialogue.regularisation.IEtablissementRegularisation;
import fr.ge.common.regent.bean.xml.formalite.dialogue.regularisation.IPersonneLieeRegularisation;
import fr.ge.common.regent.bean.xml.transverse.IAdresse;
import fr.ge.common.regent.util.mapper.formalite.IMapperFormaliteUtils;

/**
 * Le Class MapperFormaliteRegularisationUtils.
 */
@Component
public class MapperDialogueRegularisationUtils implements IMapperFormaliteUtils<IDialogueRegularisation, DialogueRegularisationBean> {

    /** Le formalite regularisation mapper. */
    @Autowired
    private Mapper formaliteRegularisationMapper;

    /** Le adresse mapper. */
    @Autowired
    private Mapper adresseMapper;

    /** Le autre etablissement ue regularisation mapper. */
    @Autowired
    private Mapper autreEtablissementUERegularisationMapper;

    /** Le personne liee regularisation mapper. */
    @Autowired
    private Mapper personneLieeRegularisationMapper;

    /** Le entreprise regularisation mapper. */
    @Autowired
    private Mapper entrepriseRegularisationMapper;

    /** Le dirigeant regularisation mapper. */
    @Autowired
    private Mapper dirigeantRegularisationMapper;

    /** Le conjoint regularisation mapper. */
    @Autowired
    private Mapper conjointRegularisationMapper;

    /** Le entreprise liee regularisation mapper. */
    @Autowired
    private Mapper entrepriseLieeRegularisationMapper;

    /** Le etablissement regularisation mapper. */
    @Autowired
    private Mapper etablissementRegularisationMapper;

    /** Le postal commune mapper. */
    @Autowired
    private Mapper postalCommuneMapper;

    /** activiteSoumiseQualificationRegularisationMapper. **/
    @Autowired
    private Mapper activiteSoumiseQualificationRegularisationMapper;

    /**
     * mapper pour préparer un bean depuis une interface (stocké en base).
     *
     * @param iFormaliteRegularisation
     *            le i formalite regularisation
     * @param formaliteRegulBean
     *            le formalite regul bean
     */
    @Override
    public void mapper(IDialogueRegularisation iFormaliteRegularisation, DialogueRegularisationBean formaliteRegulBean) {

        // Mapper Adresse signature
        IAdresse iAdresseSignature = iFormaliteRegularisation.getAdresseSignature();
        if (iAdresseSignature != null) {
            AdresseBean adresseSignatureBean = formaliteRegulBean.getAdresseSignature();
            this.adresseMapper.map(iAdresseSignature, adresseSignatureBean);
            if (iAdresseSignature.getCodePostalCommune() != null) {
                this.postalCommuneMapper.map(iAdresseSignature.getCodePostalCommune(), adresseSignatureBean.getCodePostalCommune());
            }
        }

        // Mapper Correspondance Adresse
        IAdresse iCorrespondanceAdresse = iFormaliteRegularisation.getCorrespondanceAdresse();
        if (iCorrespondanceAdresse != null) {
            AdresseBean iCorrAdresseBean = formaliteRegulBean.getCorrespondanceAdresse();
            this.adresseMapper.map(iCorrespondanceAdresse, iCorrAdresseBean);
            if (iCorrespondanceAdresse.getCodePostalCommune() != null) {
                this.postalCommuneMapper.map(iCorrespondanceAdresse.getCodePostalCommune(), iCorrAdresseBean.getCodePostalCommune());
            }
        }
        // Mapper Entreprise
        IEntrepriseRegularisation iEntrepriseR = iFormaliteRegularisation.getEntreprise();
        if (iEntrepriseR != null) {
            EntrepriseBean entrepriseBean = formaliteRegulBean.getEntreprise();
            // Mapper Entreprise Liee Dom
            IEntrepriseLieeRegularisation iEntrLieeDom = iEntrepriseR.getEntrepriseLieeDomiciliation();
            if (iEntrLieeDom != null) {
                if (entrepriseBean.getEntrepriseLieeDomiciliation() == null) {
                    entrepriseBean.setEntrepriseLieeDomiciliation(new EntrepriseLieeBean());
                }
                this.entrepriseLieeRegularisationMapper.map(iEntrLieeDom, entrepriseBean.getEntrepriseLieeDomiciliation());
                IAdresse iAdresseEntLieeDom = iEntrLieeDom.getAdresse();
                IAdresse iModifAncienneAdresse2 = iEntrLieeDom.getModifAncienneAdresse2();
                mapperAdresse(entrepriseBean, iAdresseEntLieeDom, iModifAncienneAdresse2);
            }
            // FIN Mapper Entreprise Liee Dom

            // Mapper Entreprise Liee Loueur Mandant
            IEntrepriseLieeRegularisation iEntrLieeLoueurMandant = iEntrepriseR.getEntrepriseLieeLoueurMandant();
            if (iEntrLieeLoueurMandant != null) {
                if (entrepriseBean.getEntrepriseLieeLoueurMandant() == null) {
                    entrepriseBean.setEntrepriseLieeLoueurMandant(new EntrepriseLieeBean());
                }
                this.entrepriseLieeRegularisationMapper.map(iEntrLieeLoueurMandant, entrepriseBean.getEntrepriseLieeLoueurMandant());
                IAdresse iAdresseLieeLoueur = iEntrLieeLoueurMandant.getAdresse();
                if (iAdresseLieeLoueur != null) {
                    AdresseBean adresseLieeLoueurBean = entrepriseBean.getEntrepriseLieeLoueurMandant().getAdresse();
                    this.adresseMapper.map(iAdresseLieeLoueur, adresseLieeLoueurBean);
                    if (iAdresseLieeLoueur.getCodePostalCommune() != null) {
                        this.postalCommuneMapper.map(iAdresseLieeLoueur.getCodePostalCommune(), adresseLieeLoueurBean.getCodePostalCommune());
                    }
                }
                IAdresse iModifAncienneAdresse2 = iEntrLieeLoueurMandant.getModifAncienneAdresse2();
                if (iModifAncienneAdresse2 != null) {
                    AdresseBean modifAncienneAdresse2Bean = entrepriseBean.getEntrepriseLieeLoueurMandant().getModifAncienneAdresse2();
                    this.adresseMapper.map(iModifAncienneAdresse2, modifAncienneAdresse2Bean);
                    if (iModifAncienneAdresse2.getCodePostalCommune() != null) {
                        this.postalCommuneMapper.map(iModifAncienneAdresse2.getCodePostalCommune(), modifAncienneAdresse2Bean.getCodePostalCommune());
                    }
                }
            }
            // FIN Mapper Entreprise Liee Loueur Mandant

            // Mapper Etablissement
            mapperEtablissement(iEntrepriseR, entrepriseBean);
            // FIN Mapper Etablissement

            /* Mapper les listes pour entreprise */

            entrepriseBean.getEntreprisesLieesPrecedentsExploitants().clear();
            for (IEntrepriseLieeRegularisation iEntrLieePrecExploitant : iEntrepriseR.getEntreprisesLieesPrecedentsExploitants()) {
                EntrepriseLieeBean entrLieePrecedentExploitantBean = new EntrepriseLieeBean();
                //
                this.entrepriseLieeRegularisationMapper.map(iEntrLieePrecExploitant, entrLieePrecedentExploitantBean);
                IAdresse iAdresseEntLieeDom = iEntrLieePrecExploitant.getAdresse();
                if (iAdresseEntLieeDom != null) {
                    AdresseBean adresseEntLieeDomBean = entrLieePrecedentExploitantBean.getAdresse();
                    this.adresseMapper.map(iAdresseEntLieeDom, adresseEntLieeDomBean);
                    if (iAdresseEntLieeDom.getCodePostalCommune() != null) {
                        this.postalCommuneMapper.map(iAdresseEntLieeDom.getCodePostalCommune(), adresseEntLieeDomBean.getCodePostalCommune());
                    }
                }
                IAdresse iModifAncienneAdresse2 = iEntrLieePrecExploitant.getModifAncienneAdresse2();
                if (iModifAncienneAdresse2 != null) {
                    AdresseBean modifAncienneAdresse2Bean = entrLieePrecedentExploitantBean.getModifAncienneAdresse2();
                    this.adresseMapper.map(iModifAncienneAdresse2, modifAncienneAdresse2Bean);
                    if (iModifAncienneAdresse2.getCodePostalCommune() != null) {
                        this.postalCommuneMapper.map(iModifAncienneAdresse2.getCodePostalCommune(), modifAncienneAdresse2Bean.getCodePostalCommune());
                    }
                }

                entrepriseBean.getEntreprisesLieesPrecedentsExploitants().add(entrLieePrecedentExploitantBean);
            }

            entrepriseBean.getDirigeants().clear();
            for (IDirigeantRegularisation iDirigeantR : iEntrepriseR.getDirigeants()) {
                DirigeantBean dirigeantBean = new DirigeantBean();
                this.personneLieeRegularisationMapper.map(iDirigeantR, dirigeantBean);

                this.dirigeantRegularisationMapper.map(iDirigeantR, dirigeantBean);
                IConjointRegularisation iConjointR = iDirigeantR.getConjoint();
                if (iConjointR != null) {
                    if (dirigeantBean.getConjoint() == null) {
                        dirigeantBean.setConjoint(new ConjointBean());
                    }
                    this.conjointRegularisationMapper.map(iConjointR, dirigeantBean.getConjoint());
                }
                IAdresse iPpAdresse = iDirigeantR.getPpAdresse();
                if (iPpAdresse != null) {
                    AdresseBean ppAdresseBean = dirigeantBean.getPpAdresse();
                    this.adresseMapper.map(iPpAdresse, ppAdresseBean);
                    if (iPpAdresse.getCodePostalCommune() != null) {
                        this.postalCommuneMapper.map(iPpAdresse.getCodePostalCommune(), ppAdresseBean.getCodePostalCommune());
                    }
                }
                if (iDirigeantR.getPpAdresseAmbulant() != null) {
                    this.postalCommuneMapper.map(iDirigeantR.getPpAdresseAmbulant(), dirigeantBean.getPpAdresseAmbulant());
                }
                if (iDirigeantR.getPpAdresseForain() != null) {
                    this.postalCommuneMapper.map(iDirigeantR.getPpAdresseForain(), dirigeantBean.getPpAdresseForain());
                }
                IAdresse iModifAncienDomicile = iDirigeantR.getModifAncienDomicile();
                if (iModifAncienDomicile != null) {
                    AdresseBean modifAncienDomicileBean = dirigeantBean.getModifAncienDomicile();
                    this.adresseMapper.map(iModifAncienDomicile, modifAncienDomicileBean);
                    if (iModifAncienDomicile.getCodePostalCommune() != null) {
                        this.postalCommuneMapper.map(iModifAncienDomicile.getCodePostalCommune(), modifAncienDomicileBean.getCodePostalCommune());
                    }
                }
                dirigeantBean.getModifIdentiteNomPrenom().clear();
                for (XmlString iModifIdenNomPrenom : iDirigeantR.getModifIdentiteNomPrenom()) {
                    dirigeantBean.getModifIdentiteNomPrenom().add(iModifIdenNomPrenom.getString());
                }
                entrepriseBean.getDirigeants().add(dirigeantBean);
            }

            entrepriseBean.getPersonneLiee().clear();
            for (IPersonneLieeRegularisation iPersLiee : iEntrepriseR.getPersonneLiee()) {
                PersonneLieeBean persLieeBean = new PersonneLieeBean();
                this.personneLieeRegularisationMapper.map(iPersLiee, persLieeBean);
                entrepriseBean.getPersonneLiee().add(persLieeBean);
            }

            entrepriseBean.getEtablissementsUE().clear();
            for (IAutreEtablissementUE iEtablissementUE : iEntrepriseR.getEtablissementsUE()) {
                AutreEtablissementUEBean etablissementUEBean = new AutreEtablissementUEBean();
                this.personneLieeRegularisationMapper.map(iEtablissementUE, etablissementUEBean);
                entrepriseBean.getEtablissementsUE().add(etablissementUEBean);
            }

            // mapping champs simples pour Entreprise
            this.entrepriseRegularisationMapper.map(iEntrepriseR, entrepriseBean);

        } // Fin mapping Entreprise

        formaliteRegulBean.setTerminee(iFormaliteRegularisation.getTerminee());
        /* mapping champs simples */
        this.formaliteRegularisationMapper.map(iFormaliteRegularisation, formaliteRegulBean);

    }

    /**
     * Mapper Etablissement Interface To Bean.
     * 
     * @param iEntrepriseR
     * @param entrepriseBean
     */
    private void mapperEtablissement(IEntrepriseRegularisation iEntrepriseR, EntrepriseBean entrepriseBean) {
        IEtablissementRegularisation iEtablissementR = iEntrepriseR.getEtablissement();
        if (iEtablissementR != null) {
            if (entrepriseBean.getEtablissement() == null) {
                entrepriseBean.setEtablissement(new EtablissementBean());
            }
            this.etablissementRegularisationMapper.map(iEtablissementR, entrepriseBean.getEtablissement());
            IAdresse iAdresseEtablissement = iEtablissementR.getAdresse();
            if (iAdresseEtablissement != null) {
                AdresseBean adresseEtablissementBean = entrepriseBean.getEtablissement().getAdresse();
                this.adresseMapper.map(iAdresseEtablissement, adresseEtablissementBean);
                if (iAdresseEtablissement.getCodePostalCommune() != null) {
                    this.postalCommuneMapper.map(iAdresseEtablissement.getCodePostalCommune(), adresseEtablissementBean.getCodePostalCommune());
                }
            }

            /* mapper la liste des activités soumises à qualification */
            mapperActivitesSoumisesQualification(iEtablissementR, entrepriseBean);
        }
    }

    /**
     * mapper la liste des activités soumises à qualification.
     * 
     * @param iEtablissementRegularisation
     *            {@link IEtablissementRegularisation}
     * @param entrepriseBean
     *            {@link EntrepriseBean}
     */
    private void mapperActivitesSoumisesQualification(IEtablissementRegularisation iEtablissementRegularisation, EntrepriseBean entrepriseBean) {
        if (iEtablissementRegularisation.getActivitesSoumisesQualification() != null) {
            if (CollectionUtils.isNotEmpty(entrepriseBean.getEtablissement().getActivitesSoumisesQualification())) {
                entrepriseBean.getEtablissement().getActivitesSoumisesQualification().clear();
            } else {
                entrepriseBean.getEtablissement().setActivitesSoumisesQualification(new ArrayList<ActiviteSoumiseQualificationBean>());
            }
            IActiviteSoumiseQualificationRegularisation[] iActivitesSoumisesQualification = iEtablissementRegularisation.getActivitesSoumisesQualification();
            for (IActiviteSoumiseQualificationRegularisation iActiviteSoumiseQualification : iActivitesSoumisesQualification) {
                ActiviteSoumiseQualificationBean activiteSoumiseQualificationBean = new ActiviteSoumiseQualificationBean();
                activiteSoumiseQualificationRegularisationMapper.map(iActiviteSoumiseQualification, activiteSoumiseQualificationBean);
                entrepriseBean.getEtablissement().getActivitesSoumisesQualification().add(activiteSoumiseQualificationBean);
            }
        }
    }

    /**
     * mapper adresse.
     * 
     * @param entrepriseBean
     *            entrepriseBean
     * @param iAdresseEntLieeDom
     *            iAdresseEntLieeDom
     * @param iModifAncienneAdresse2
     *            iModifAncienneAdresse2
     */
    private void mapperAdresse(EntrepriseBean entrepriseBean, IAdresse iAdresseEntLieeDom, IAdresse iModifAncienneAdresse2) {
        if (iAdresseEntLieeDom != null) {
            AdresseBean adresseEntLieeDomBean = entrepriseBean.getEntrepriseLieeDomiciliation().getAdresse();
            this.adresseMapper.map(iAdresseEntLieeDom, adresseEntLieeDomBean);
            if (iAdresseEntLieeDom.getCodePostalCommune() != null) {
                this.postalCommuneMapper.map(iAdresseEntLieeDom.getCodePostalCommune(), adresseEntLieeDomBean.getCodePostalCommune());
            }
        }

        if (iModifAncienneAdresse2 != null) {
            AdresseBean modifAncienneAdresse2Bean = entrepriseBean.getEntrepriseLieeDomiciliation().getModifAncienneAdresse2();
            this.adresseMapper.map(iModifAncienneAdresse2, modifAncienneAdresse2Bean);
            if (iModifAncienneAdresse2.getCodePostalCommune() != null) {
                this.postalCommuneMapper.map(iModifAncienneAdresse2.getCodePostalCommune(), modifAncienneAdresse2Bean.getCodePostalCommune());
            }
        }
    }

    /**
     * Mapper.
     *
     * @param formaliteRegulBean
     *            le formalite regul bean
     * @param iFormaliteRegularisation
     *            le i formalite regularisation
     */
    @Override
    public void mapper(DialogueRegularisationBean formaliteRegulBean, IDialogueRegularisation iFormaliteRegularisation) {

        // Mapper Adresse signature
        iFormaliteRegularisation.newAdresseSignature();
        IAdresse iAdresseSignature = iFormaliteRegularisation.getAdresseSignature();
        this.adresseMapper.map(formaliteRegulBean.getAdresseSignature(), iAdresseSignature);
        iAdresseSignature.newCodePostalCommune();
        this.postalCommuneMapper.map(formaliteRegulBean.getAdresseSignature().getCodePostalCommune(), iAdresseSignature.getCodePostalCommune());

        // Mapper Correspondance Adresse
        iFormaliteRegularisation.newCorrespondanceAdresse();
        IAdresse icorrespondanceAdresse = iFormaliteRegularisation.getCorrespondanceAdresse();
        this.adresseMapper.map(formaliteRegulBean.getCorrespondanceAdresse(), icorrespondanceAdresse);
        icorrespondanceAdresse.newCodePostalCommune();
        this.postalCommuneMapper.map(formaliteRegulBean.getCorrespondanceAdresse().getCodePostalCommune(), icorrespondanceAdresse.getCodePostalCommune());

        // DEBUT- Mapper Entreprise
        iFormaliteRegularisation.newEntreprise();
        IEntrepriseRegularisation iEntreprise = iFormaliteRegularisation.getEntreprise();

        EntrepriseBean entrepriseBean = formaliteRegulBean.getEntreprise();
        if (entrepriseBean != null) {
            // DEBUT- Mapper EntrepriseLieeDomiciliation
            iEntreprise.newEntrepriseLieeDomiciliation();
            IEntrepriseLieeRegularisation iEntrepriseLieeDomiciliation = iEntreprise.getEntrepriseLieeDomiciliation();
            iEntrepriseLieeDomiciliation.newAdresse();
            iEntrepriseLieeDomiciliation.getAdresse().newCodePostalCommune();
            EntrepriseLieeBean entrepriseLieeDomiciliationBean = entrepriseBean.getEntrepriseLieeDomiciliation();
            if (entrepriseLieeDomiciliationBean != null) {
                AdresseBean adresseEntrLieeDomBean = entrepriseLieeDomiciliationBean.getAdresse();
                if (adresseEntrLieeDomBean != null) {
                    this.adresseMapper.map(adresseEntrLieeDomBean, iEntrepriseLieeDomiciliation.getAdresse());
                    this.postalCommuneMapper.map(entrepriseLieeDomiciliationBean.getAdresse().getCodePostalCommune(), iEntrepriseLieeDomiciliation.getAdresse().getCodePostalCommune());
                }

                iEntrepriseLieeDomiciliation.newModifAncienneAdresse2();
                iEntrepriseLieeDomiciliation.getModifAncienneAdresse2().newCodePostalCommune();
                AdresseBean modifAncAdr2EntrLieeDomBean = entrepriseLieeDomiciliationBean.getModifAncienneAdresse2();
                if (modifAncAdr2EntrLieeDomBean != null) {
                    this.adresseMapper.map(modifAncAdr2EntrLieeDomBean, iEntrepriseLieeDomiciliation.getModifAncienneAdresse2());
                    this.postalCommuneMapper.map(entrepriseLieeDomiciliationBean.getModifAncienneAdresse2().getCodePostalCommune(),
                            iEntrepriseLieeDomiciliation.getModifAncienneAdresse2().getCodePostalCommune());
                }
            }
            // FIN - Mapper EntrepriseLieeDomiciliation

            // DEBUT- Mapper EntrepriseLieeLoueurMandant
            iEntreprise.newEntrepriseLieeLoueurMandant();
            IEntrepriseLieeRegularisation iEntrepriseLieeLoueurMandant = iEntreprise.newEntrepriseLieeLoueurMandant();
            iEntrepriseLieeLoueurMandant.newAdresse();
            iEntrepriseLieeLoueurMandant.getAdresse().newCodePostalCommune();

            EntrepriseLieeBean entrepriseLieeLoueurMandantBean = entrepriseBean.getEntrepriseLieeLoueurMandant();
            if (entrepriseLieeLoueurMandantBean != null) {
                AdresseBean adresseEntrLieeLoueurMandantBean = entrepriseLieeLoueurMandantBean.getAdresse();
                if (adresseEntrLieeLoueurMandantBean != null) {
                    this.adresseMapper.map(adresseEntrLieeLoueurMandantBean, iEntrepriseLieeLoueurMandant.getAdresse());
                    this.postalCommuneMapper.map(entrepriseLieeLoueurMandantBean.getAdresse().getCodePostalCommune(), iEntrepriseLieeLoueurMandant.getAdresse().getCodePostalCommune());
                }

                iEntrepriseLieeLoueurMandant.newModifAncienneAdresse2();
                iEntrepriseLieeLoueurMandant.getModifAncienneAdresse2().newCodePostalCommune();
                AdresseBean modifAncAdr2EntrLieeLoueurMandantBean = entrepriseLieeLoueurMandantBean.getModifAncienneAdresse2();
                if (modifAncAdr2EntrLieeLoueurMandantBean != null) {
                    this.adresseMapper.map(modifAncAdr2EntrLieeLoueurMandantBean, iEntrepriseLieeLoueurMandant.getModifAncienneAdresse2());
                    this.postalCommuneMapper.map(entrepriseLieeLoueurMandantBean.getModifAncienneAdresse2().getCodePostalCommune(),
                            iEntrepriseLieeLoueurMandant.getModifAncienneAdresse2().getCodePostalCommune());

                }
            }
            // FIN- Mapper EntrepriseLieeLoueurMandant

            // DEBUT- Mapper Etablissement

            mapperEtablissement(entrepriseBean, iEntreprise);
            // FIN - Mapper Etablissement

            this.entrepriseRegularisationMapper.map(entrepriseBean, iEntreprise);

            /* Mapper les listes pour entreprise */

            if (!CollectionUtils.isEmpty(entrepriseBean.getDirigeants())) {
                this.addDirigeant(entrepriseBean, iEntreprise);
            }

            if (!CollectionUtils.isEmpty(entrepriseBean.getEntreprisesLieesPrecedentsExploitants())) {
                this.addEntrepriseLieePrecedentiExploitant(entrepriseBean, iEntreprise);
            }

            if (!CollectionUtils.isEmpty(entrepriseBean.getPersonneLiee())) {
                this.addPersonneLiee(entrepriseBean, iEntreprise);
            }

            if (!CollectionUtils.isEmpty(entrepriseBean.getEtablissementsUE())) {
                this.addEtablissementsUE(entrepriseBean, iEntreprise);
            }
        }
        // FIN - Mapper Entreprise

        iFormaliteRegularisation.setTerminee(iFormaliteRegularisation.getTerminee());

        /* mapping champs simples FormaliteRegul. */
        this.formaliteRegularisationMapper.map(formaliteRegulBean, iFormaliteRegularisation);

    }

    /**
     * Mapper Etablissement Bean To Interface.
     * 
     * @param iEntreprise
     * @param entrepriseBean
     */
    private void mapperEtablissement(EntrepriseBean entrepriseBean, IEntrepriseRegularisation iEntreprise) {
        iEntreprise.newEtablissement();
        IEtablissementRegularisation iEtablissement = iEntreprise.getEtablissement();
        iEtablissement.newAdresse();
        iEtablissement.getAdresse().newCodePostalCommune();
        EtablissementBean etablissementBean = entrepriseBean.getEtablissement();
        if (etablissementBean != null) {
            AdresseBean adresseEtablissementBean = etablissementBean.getAdresse();
            if (adresseEtablissementBean != null) {
                this.adresseMapper.map(adresseEtablissementBean, iEtablissement.getAdresse());
                this.postalCommuneMapper.map(etablissementBean.getAdresse().getCodePostalCommune(), iEtablissement.getAdresse().getCodePostalCommune());
            }

            iEtablissement.newModifAncienneAdresse1();
            iEtablissement.getModifAncienneAdresse1().newCodePostalCommune();

            AdresseBean modifAncAdr1EtablissementBean = etablissementBean.getModifAncienneAdresse1();
            if (modifAncAdr1EtablissementBean != null) {
                this.adresseMapper.map(modifAncAdr1EtablissementBean, iEtablissement.getModifAncienneAdresse1());
                this.postalCommuneMapper.map(etablissementBean.getModifAncienneAdresse1().getCodePostalCommune(), iEtablissement.getModifAncienneAdresse1().getCodePostalCommune());
            }

        }
        this.etablissementRegularisationMapper.map(etablissementBean, iEtablissement);

        // Mapper liste des activités soumisies à qualification
        if (!CollectionUtils.isEmpty(etablissementBean.getActivitesSoumisesQualification())) {
            this.addActivitesSoumisesQualification(etablissementBean, iEtablissement);
        }
    }

    /**
     * Adds the activites soumises à qualification.
     * 
     * @param etablissementBean
     *            {@link EtablissementBean}
     * @param iEtablissement
     *            {@link IEtablissementRegularisation}
     */
    public void addActivitesSoumisesQualification(EtablissementBean etablissementBean, IEtablissementRegularisation iEtablissement) {
        /*
         * parcourir la liste existants sur l'interface XML et les supprimer
         */
        for (IActiviteSoumiseQualificationRegularisation iActiviteSoumiseQualification : iEtablissement.getActivitesSoumisesQualification()) {
            iEtablissement.removeFromActivitesSoumisesQualification(iActiviteSoumiseQualification);
        }
        /*
         * parcourir la liste des évenements dans le Bean pour les rajouter à
         * l'interface
         */

        for (ActiviteSoumiseQualificationBean activiteSoumiseQualificationBean : etablissementBean.getActivitesSoumisesQualification()) {
            IActiviteSoumiseQualificationRegularisation iActiviteSoumiseQualification = iEtablissement.addToActivitesSoumisesQualification();
            // DEBUT- Mapper iActiviteSoumiseQualification
            // Gestion idTechnique, nécessaire pour la gestion des impacts
            // (écran)
            if (activiteSoumiseQualificationBean.getIdTechnique() == null || activiteSoumiseQualificationBean.getIdTechnique().isEmpty()) {
                activiteSoumiseQualificationBean.setIdTechnique(UUID.randomUUID().toString());
            }

            if (activiteSoumiseQualificationBean != null) {
                this.activiteSoumiseQualificationRegularisationMapper.map(activiteSoumiseQualificationBean, iActiviteSoumiseQualification);
            }
        }
    }

    /**
     * Ajoute le entreprise liee precedenti exploitant.
     *
     * @param entrepriseBean
     *            le entreprise bean
     * @param iEntreprise
     *            le i entreprise
     */
    private void addEntrepriseLieePrecedentiExploitant(EntrepriseBean entrepriseBean, IEntrepriseRegularisation iEntreprise) {

        /*
         * parcourir la liste des EntrepriseLieePrecedentiExploitant existants
         * sur l'interface XML et les supprimer
         */
        IEntrepriseLieeRegularisation[] entrLieePrecedentiExploitantList = iEntreprise.getEntreprisesLieesPrecedentsExploitants();
        for (IEntrepriseLieeRegularisation iEntrLieePrecExploitant : entrLieePrecedentiExploitantList) {
            iEntreprise.removeFromEntrepriseLieesPrecedentsExploitants(iEntrLieePrecExploitant);
        }
        /*
         * parcourir la liste des EntrepriseLieePrecedentiExploitant dans le
         * Bean pour les rajouter à l'interface
         */
        List<EntrepriseLieeBean> entrLieePrecExploitListBean = entrepriseBean.getEntreprisesLieesPrecedentsExploitants();
        for (EntrepriseLieeBean entrepriseLieePrecedentExploitantBean : entrLieePrecExploitListBean) {
            IEntrepriseLieeRegularisation iEntrepriseLieePrecedentExploitant = iEntreprise.addToEntreprisesLieesPrecedentsExploitants();
            // DEBUT- Mapper iEntrepriseLieePrecedentiExploitant
            // Gestion idTechnique, nécessaire pour la gestion des impacts
            // (écran)
            if (entrepriseLieePrecedentExploitantBean.getIdTechnique() == null || entrepriseLieePrecedentExploitantBean.getIdTechnique().isEmpty()) {
                entrepriseLieePrecedentExploitantBean.setIdTechnique(UUID.randomUUID().toString());
            }

            iEntrepriseLieePrecedentExploitant.newAdresse();
            iEntrepriseLieePrecedentExploitant.getAdresse().newCodePostalCommune();
            iEntrepriseLieePrecedentExploitant.newModifAncienneAdresse2();
            iEntrepriseLieePrecedentExploitant.getModifAncienneAdresse2().newCodePostalCommune();
            if (entrepriseLieePrecedentExploitantBean != null) {
                if (entrepriseLieePrecedentExploitantBean.getAdresse() == null) {
                    entrepriseLieePrecedentExploitantBean.setAdresse(new AdresseBean());
                }
                this.adresseMapper.map(entrepriseLieePrecedentExploitantBean.getAdresse(), iEntrepriseLieePrecedentExploitant.getAdresse());
                this.postalCommuneMapper.map(entrepriseLieePrecedentExploitantBean.getAdresse().getCodePostalCommune(), iEntrepriseLieePrecedentExploitant.getAdresse().getCodePostalCommune());
                if (entrepriseLieePrecedentExploitantBean.getModifAncienneAdresse2() == null) {
                    entrepriseLieePrecedentExploitantBean.setModifAncienneAdresse2(new AdresseBean());
                }
                this.adresseMapper.map(entrepriseLieePrecedentExploitantBean.getModifAncienneAdresse2(), iEntrepriseLieePrecedentExploitant.getModifAncienneAdresse2());
                this.postalCommuneMapper.map(entrepriseLieePrecedentExploitantBean.getModifAncienneAdresse2().getCodePostalCommune(),
                        iEntrepriseLieePrecedentExploitant.getModifAncienneAdresse2().getCodePostalCommune());

                this.entrepriseLieeRegularisationMapper.map(entrepriseLieePrecedentExploitantBean, iEntrepriseLieePrecedentExploitant);
            }
            // FIN - Mapper iEntrepriseLieePrecedentiExploitant
        }

    }

    /**
     * Ajoute le dirigeant.
     *
     * @param entrepriseBean
     *            le entreprise bean
     * @param iEntreprise
     *            le i entreprise
     */
    private void addDirigeant(EntrepriseBean entrepriseBean, IEntrepriseRegularisation iEntreprise) {

        /*
         * parcourir la liste des Dirigeant existants sur l'interface XML et les
         * supprimer
         */
        IDirigeantRegularisation[] iDirigeantList = iEntreprise.getDirigeants();
        for (IDirigeantRegularisation iDirigeantCourant : iDirigeantList) {
            iEntreprise.removeFromDirigeants(iDirigeantCourant);
        }
        /*
         * parcourir la liste des Dirigeant dans le Bean pour les rajouter à
         * l'interface
         */
        List<DirigeantBean> dirigeantListBean = entrepriseBean.getDirigeants();
        for (DirigeantBean dirigeantBean : dirigeantListBean) {
            // Gestion idTechnique, nécessaire pour la gestion des impacts
            // (écran)
            if (StringUtils.isBlank(dirigeantBean.getIdTechnique())) {
                dirigeantBean.setIdTechnique(UUID.randomUUID().toString());
            }

            IDirigeantRegularisation iDirigeant = iEntreprise.addToDirigeants();
            // DEBUT - Mapper Dirigeant
            iDirigeant.newConjoint();
            if (dirigeantBean.getConjoint() == null) {
                dirigeantBean.setConjoint(new ConjointBean());
            }

            this.conjointRegularisationMapper.map(dirigeantBean.getConjoint(), iDirigeant.getConjoint());

            this.mappingDirigeantAdresse(dirigeantBean, iDirigeant);

            /* Mapper liste pour bean dirigeant (modifIdentiteNomPrenom) */
            if (!CollectionUtils.isEmpty(dirigeantBean.getModifIdentiteNomPrenom())) {
                this.addModifIdentiteNomPrenom(dirigeantBean, iDirigeant);
            }

            this.dirigeantRegularisationMapper.map(dirigeantBean, iDirigeant);
            // FIN - Mapper Dirigeant
        }

    }

    /**
     * Mapping des données de type Adresse
     * 
     * @param dirigeantBean
     * @param iDirigeant
     */
    private void mappingDirigeantAdresse(DirigeantBean dirigeantBean, IDirigeantRegularisation iDirigeant) {
        iDirigeant.newPpAdresse();
        iDirigeant.getPpAdresse().newCodePostalCommune();
        if (dirigeantBean.getPpAdresse() == null) {
            dirigeantBean.setPpAdresse(new AdresseBean());
        }
        this.adresseMapper.map(dirigeantBean.getPpAdresse(), iDirigeant.getPpAdresse());
        this.postalCommuneMapper.map(dirigeantBean.getPpAdresse().getCodePostalCommune(), iDirigeant.getPpAdresse().getCodePostalCommune());

        iDirigeant.newPpAdresseAmbulant();
        if (dirigeantBean.getPpAdresseAmbulant() == null) {
            dirigeantBean.setPpAdresseAmbulant(new PostalCommuneBean());
        }
        this.postalCommuneMapper.map(dirigeantBean.getPpAdresseAmbulant(), iDirigeant.getPpAdresseAmbulant());

        iDirigeant.newPpAdresseForain();
        if (dirigeantBean.getPpAdresseForain() == null) {
            dirigeantBean.setPpAdresseForain(new PostalCommuneBean());
        }
        this.postalCommuneMapper.map(dirigeantBean.getPpAdresseForain(), iDirigeant.getPpAdresseForain());

        iDirigeant.newModifAncienDomicile();
        iDirigeant.getModifAncienDomicile().newCodePostalCommune();
        if (dirigeantBean.getModifAncienDomicile() == null) {
            dirigeantBean.setModifAncienDomicile(new AdresseBean());
        }
        this.adresseMapper.map(dirigeantBean.getModifAncienDomicile(), iDirigeant.getModifAncienDomicile());
        this.postalCommuneMapper.map(dirigeantBean.getModifAncienDomicile().getCodePostalCommune(), iDirigeant.getModifAncienDomicile().getCodePostalCommune());
    }

    /**
     * Ajoute le modif identite nom prenom.
     *
     * @param dirigeantBean
     *            le dirigeant bean
     * @param iDirigeant
     *            le i dirigeant
     */
    private void addModifIdentiteNomPrenom(DirigeantBean dirigeantBean, IDirigeantRegularisation iDirigeant) {
        /*
         * parcourir la liste des modifIdentiteNomPrenom existants sur
         * l'interface XML et les supprimer
         */
        for (XmlString modifIdentiteNomPrenom : iDirigeant.getModifIdentiteNomPrenom()) {
            iDirigeant.removeFromModifIdentiteNomPrenom(modifIdentiteNomPrenom);
        }
        /*
         * parcourir la liste des évenements dans le Bean pour les rajouter à
         * l'interface
         */
        for (String modifIdentiteNomPrenomCourant : dirigeantBean.getModifIdentiteNomPrenom()) {
            XmlString modifIdentiteNomPrenom = iDirigeant.addToModifIdentiteNomPrenom();
            modifIdentiteNomPrenom.setString(modifIdentiteNomPrenomCourant);
        }

    }

    /**
     * Ajoute le personne liee.
     *
     * @param entrepriseBean
     *            le entreprise bean
     * @param iEntreprise
     *            le i entreprise
     */
    private void addPersonneLiee(EntrepriseBean entrepriseBean, IEntrepriseRegularisation iEntreprise) {
        /*
         * parcourir la liste des évenements existants sur l'interface XML et
         * les supprimer
         */
        IPersonneLieeRegularisation[] personnesLieesInterface = iEntreprise.getPersonneLiee();
        for (IPersonneLieeRegularisation iPersonneLieeRegularisation : personnesLieesInterface) {
            iEntreprise.removeFromPersonneLiee(iPersonneLieeRegularisation);
        }

        /*
         * parcourir la liste des évenements dans le Bean pour les rajouter à
         * l'interface
         */
        List<PersonneLieeBean> personnesLieesBean = entrepriseBean.getPersonneLiee();
        for (PersonneLieeBean personneLieeBean : personnesLieesBean) {
            IPersonneLieeRegularisation iPersonneLieeRegularisation = iEntreprise.addToPersonneLiee();
            iPersonneLieeRegularisation.newPpAdresse();
            iPersonneLieeRegularisation.getPpAdresse().newCodePostalCommune();
            if (personneLieeBean != null) {
                // Gestion idTechnique, nécessaire pour la gestion des impacts
                // (écran)
                if (personneLieeBean.getIdTechnique() == null || personneLieeBean.getIdTechnique().isEmpty()) {
                    personneLieeBean.setIdTechnique(UUID.randomUUID().toString());
                }
                if (personneLieeBean.getPpAdresse() == null) {
                    personneLieeBean.setPpAdresse(new AdresseBean());
                }
                this.adresseMapper.map(personneLieeBean.getPpAdresse(), iPersonneLieeRegularisation.getPpAdresse());
                this.postalCommuneMapper.map(personneLieeBean.getPpAdresse().getCodePostalCommune(), iPersonneLieeRegularisation.getPpAdresse().getCodePostalCommune());
                this.personneLieeRegularisationMapper.map(personneLieeBean, iPersonneLieeRegularisation);
            }
        }
    }

    /**
     * Ajoute le etablissements ue.
     *
     * @param entrepriseBean
     *            le entreprise bean
     * @param iEntreprise
     *            le i entreprise
     */
    private void addEtablissementsUE(EntrepriseBean entrepriseBean, IEntrepriseRegularisation iEntreprise) {
        /*
         * parcourir la liste des etablissementsUE existants sur l'interface XML
         * et les supprimer
         */
        IAutreEtablissementUE[] etablissementsUE = iEntreprise.getEtablissementsUE();
        for (IAutreEtablissementUE iAutreEtablissementUE : etablissementsUE) {
            iEntreprise.removeFromEtablissementsUE(iAutreEtablissementUE);
        }
        /*
         * parcourir la liste des évenements dans le Bean pour les rajouter à
         * l'interface
         */
        List<AutreEtablissementUEBean> etablissementsUEBean = entrepriseBean.getEtablissementsUE();
        for (AutreEtablissementUEBean autreEtablissementUEBean : etablissementsUEBean) {
            // Gestion idTechnique, nécessaire pour la gestion des impacts
            // (écran)
            if (autreEtablissementUEBean.getIdTechnique() == null || autreEtablissementUEBean.getIdTechnique().isEmpty()) {
                autreEtablissementUEBean.setIdTechnique(UUID.randomUUID().toString());
            }

            IAutreEtablissementUE iAutreEtablissementUE = iEntreprise.addToEtablissementsUE();
            iAutreEtablissementUE.newAdresse();
            iAutreEtablissementUE.getAdresse().newCodePostalCommune();
            if (autreEtablissementUEBean.getAdresse() == null) {
                autreEtablissementUEBean.setAdresse(new AdresseBean());
            }
            this.adresseMapper.map(autreEtablissementUEBean.getAdresse(), iAutreEtablissementUE.getAdresse());
            this.postalCommuneMapper.map(autreEtablissementUEBean.getAdresse().getCodePostalCommune(), iAutreEtablissementUE.getAdresse().getCodePostalCommune());
            this.autreEtablissementUERegularisationMapper.map(autreEtablissementUEBean, iAutreEtablissementUE);
        }

    }

    /**
     * Get le formalite regularisation mapper.
     *
     * @return the formaliteRegularisationMapper
     */
    public Mapper getFormaliteRegularisationMapper() {
        return this.formaliteRegularisationMapper;
    }

    /**
     * Set le formalite regularisation mapper.
     *
     * @param formaliteRegularisationMapper
     *            the formaliteRegularisationMapper to set
     */
    public void setFormaliteRegularisationMapper(Mapper formaliteRegularisationMapper) {
        this.formaliteRegularisationMapper = formaliteRegularisationMapper;
    }

    /**
     * Get le adresse mapper.
     *
     * @return the adresseMapper
     */
    public Mapper getAdresseMapper() {
        return this.adresseMapper;
    }

    /**
     * Set le adresse mapper.
     *
     * @param adresseMapper
     *            the adresseMapper to set
     */
    public void setAdresseMapper(Mapper adresseMapper) {
        this.adresseMapper = adresseMapper;
    }

    /**
     * Get le entreprise regularisation mapper.
     *
     * @return the entrepriseRegularisationMapper
     */
    public Mapper getEntrepriseRegularisationMapper() {
        return this.entrepriseRegularisationMapper;
    }

    /**
     * Set le entreprise regularisation mapper.
     *
     * @param entrepriseRegularisationMapper
     *            the entrepriseRegularisationMapper to set
     */
    public void setEntrepriseRegularisationMapper(Mapper entrepriseRegularisationMapper) {
        this.entrepriseRegularisationMapper = entrepriseRegularisationMapper;
    }

    /**
     * Get le personne liee regularisation mapper.
     *
     * @return the personneLieeRegularisationMapper
     */
    public Mapper getPersonneLieeRegularisationMapper() {
        return this.personneLieeRegularisationMapper;
    }

    /**
     * Set le personne liee regularisation mapper.
     *
     * @param personneLieeRegularisationMapper
     *            the personneLieeRegularisationMapper to set
     */
    public void setPersonneLieeRegularisationMapper(Mapper personneLieeRegularisationMapper) {
        this.personneLieeRegularisationMapper = personneLieeRegularisationMapper;
    }

    /**
     * Get le autre etablissement ue regularisation mapper.
     *
     * @return the autreEtablissementUERegularisationMapper
     */
    public Mapper getAutreEtablissementUERegularisationMapper() {
        return this.autreEtablissementUERegularisationMapper;
    }

    /**
     * Set le autre etablissement ue regularisation mapper.
     *
     * @param autreEtablissementUERegularisationMapper
     *            the autreEtablissementUERegularisationMapper to set
     */
    public void setAutreEtablissementUERegularisationMapper(Mapper autreEtablissementUERegularisationMapper) {
        this.autreEtablissementUERegularisationMapper = autreEtablissementUERegularisationMapper;
    }

    /**
     * Get le dirigeant regularisation mapper.
     *
     * @return the dirigeantRegularisationMapper
     */
    public Mapper getDirigeantRegularisationMapper() {
        return this.dirigeantRegularisationMapper;
    }

    /**
     * Set le dirigeant regularisation mapper.
     *
     * @param dirigeantRegularisationMapper
     *            the dirigeantRegularisationMapper to set
     */
    public void setDirigeantRegularisationMapper(Mapper dirigeantRegularisationMapper) {
        this.dirigeantRegularisationMapper = dirigeantRegularisationMapper;
    }

    /**
     * Get le conjoint regularisation mapper.
     *
     * @return the conjointRegularisationMapper
     */
    public Mapper getConjointRegularisationMapper() {
        return this.conjointRegularisationMapper;
    }

    /**
     * Set le conjoint regularisation mapper.
     *
     * @param conjointRegularisationMapper
     *            the conjointRegularisationMapper to set
     */
    public void setConjointRegularisationMapper(Mapper conjointRegularisationMapper) {
        this.conjointRegularisationMapper = conjointRegularisationMapper;
    }

    /**
     * Get le entreprise liee regularisation mapper.
     *
     * @return the entrepriseLieeRegularisationMapper
     */
    public Mapper getEntrepriseLieeRegularisationMapper() {
        return this.entrepriseLieeRegularisationMapper;
    }

    /**
     * Set le entreprise liee regularisation mapper.
     *
     * @param entrepriseLieeRegularisationMapper
     *            the entrepriseLieeRegularisationMapper to set
     */
    public void setEntrepriseLieeRegularisationMapper(Mapper entrepriseLieeRegularisationMapper) {
        this.entrepriseLieeRegularisationMapper = entrepriseLieeRegularisationMapper;
    }

    /**
     * Get le etablissement regularisation mapper.
     *
     * @return the etablissementRegularisationMapper
     */
    public Mapper getEtablissementRegularisationMapper() {
        return this.etablissementRegularisationMapper;
    }

    /**
     * Set le etablissement regularisation mapper.
     *
     * @param etablissementRegularisationMapper
     *            the etablissementRegularisationMapper to set
     */
    public void setEtablissementRegularisationMapper(Mapper etablissementRegularisationMapper) {
        this.etablissementRegularisationMapper = etablissementRegularisationMapper;
    }

    /**
     * Get le postal commune mapper.
     *
     * @return the postalCommuneMapper
     */
    public Mapper getPostalCommuneMapper() {
        return this.postalCommuneMapper;
    }

    /**
     * Set le postal commune mapper.
     *
     * @param postalCommuneMapper
     *            the postalCommuneMapper to set
     */
    public void setPostalCommuneMapper(Mapper postalCommuneMapper) {
        this.postalCommuneMapper = postalCommuneMapper;
    }

    /**
     * Accesseur sur l'attribut
     * {@link #activiteSoumiseQualificationRegularisationMapper}.
     *
     * @return Mapper activiteSoumiseQualificationRegularisationMapper
     */
    public Mapper getActiviteSoumiseQualificationRegularisationMapper() {
        return activiteSoumiseQualificationRegularisationMapper;
    }

    /**
     * Mutateur sur l'attribut
     * {@link #activiteSoumiseQualificationRegularisationMapper}.
     *
     * @param activiteSoumiseQualificationRegularisationMapper
     *            la nouvelle valeur de l'attribut
     *            activiteSoumiseQualificationRegularisationMapper
     */
    public void setActiviteSoumiseQualificationRegularisationMapper(Mapper activiteSoumiseQualificationRegularisationMapper) {
        this.activiteSoumiseQualificationRegularisationMapper = activiteSoumiseQualificationRegularisationMapper;
    }

}
