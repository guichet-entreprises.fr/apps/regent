package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;
import org.xmlfield.core.types.XmlString;

/**
 * Interface A61.
 */
@ResourceXPath("/A61")
public interface IA61 {

  /**
   * Get le A61.1.
   *
   * @return le A61.1
   */
  @FieldXPath("A61.1")
  String getA611();

  /**
   * Set le A611.
   *
   * @param A611
   *          le nouveau A611
   */
  void setA611(String A611);

  /**
   * Get le A612.
   *
   * @return le A612
   */
  @FieldXPath("A61.2")
  String getA612();

  /**
   * Set le A612.
   *
   * @param A612
   *          le nouveau A612
   */
  void setA612(String A612);

  /******/
  /**
   * Get le A61.3.
   *
   * @return le A61.3
   */
  @FieldXPath("A61.3")
  XmlString[] getA613();

  /**
   * Ajoute le to A613.
   *
   * @return le xml string
   */
  XmlString addToA613();

  /**
   * Set le A613.
   *
   * @param A613
   *          le nouveau A613
   */
  void setA613(XmlString[] A613);

}
