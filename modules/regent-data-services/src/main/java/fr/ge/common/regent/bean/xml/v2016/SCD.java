package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface SCD.
 */
@ResourceXPath("/SCD")
public interface SCD {

  /**
   * Get le D40.
   *
   * @return le D40
   */
  @FieldXPath("D40")
  String getD40();

  /**
   * Set le D40.
   *
   * @param D40
   *          le nouveau D40
   */
  void setD40(String D40);

  /**
   * Get le D41.
   *
   * @return le D41
   */
  @FieldXPath("D41")
  String getD41();

  /**
   * Set le D41.
   *
   * @param D41
   *          le nouveau D41
   */
  void setD41(String D41);

}
