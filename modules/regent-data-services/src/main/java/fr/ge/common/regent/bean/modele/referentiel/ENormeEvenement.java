package fr.ge.common.regent.bean.modele.referentiel;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

/**
 * Le Class ENormeEvenement.
 */
@Entity
@Proxy(lazy = false)
@Table(name = "norme_evenement")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class ENormeEvenement implements Serializable {

  /** La constante serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** Le id. */
  @Id
  @Column(name = "CH_NUM", nullable = false)
  private Integer id;

  /** Le evenement. */
  @Column(name = "CH_EVENEMENT", nullable = false)
  private String evenement;

  /** Le rubrique. */
  @Column(name = "CH_RUBRIQUE", nullable = false)
  private String rubrique;

  /** Le version. */
  @Column(name = "CH_VERSION", nullable = false)
  private String version;

  /** rubrique obligatoire ou pas. */
  @Column(name = "CH_CONDITION", nullable = false)
  private Boolean condition;

  /**
   * Get le id.
   *
   * @return le id
   */
  public Integer getId() {
    return id;
  }

  /**
   * Set le id.
   *
   * @param id
   *          le nouveau id
   */
  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * Get le evenement.
   *
   * @return le evenement
   */
  public String getEvenement() {
    return evenement;
  }

  /**
   * Set le evenement.
   *
   * @param evenement
   *          le nouveau evenement
   */
  public void setEvenement(String evenement) {
    this.evenement = evenement;
  }

  /**
   * Get le rubrique.
   *
   * @return le rubrique
   */
  public String getRubrique() {
    return rubrique;
  }

  /**
   * Set le rubrique.
   *
   * @param rubrique
   *          le nouveau rubrique
   */
  public void setRubrique(String rubrique) {
    this.rubrique = rubrique;
  }

  /**
   * Get le version.
   *
   * @return le version
   */
  public String getVersion() {
    return version;
  }

  /**
   * Set le version.
   *
   * @param version
   *          le nouveau version
   */
  public void setVersion(String version) {
    this.version = version;
  }

  /**
   * Checks if is condition.
   *
   * @return boolean
   */
  public Boolean isCondition() {
    return condition;
  }

  /**
   * Mutateur sur l'attribut condition.
   *
   * @param condition
   *          le nouveau condition
   */
  public void setCondition(Boolean condition) {
    this.condition = condition;
  }

}
