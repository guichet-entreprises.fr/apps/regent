package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;
import org.xmlfield.core.types.XmlString;

/**
 * Le Interface LGE.
 */
@ResourceXPath("/LGE")
public interface LGE {

  /**
   * Get le e61.
   * 
   * @return le e61
   */
  @FieldXPath("E61")
  IE61[] getE61();

  /**
   * Ajoute le to e61.
   * 
   * @return le i e61
   */
  IE61 addToE61();

  /**
   * Set le e61.
   * 
   * @param E61
   *          le nouveau e61
   */
  void setE61(IE61[] E61);

  /**
   * Get le e62.
   * 
   * @return le e62
   */
  @FieldXPath("E62")
  String getE62();

  /**
   * Set le e62.
   * 
   * @param E62
   *          le nouveau e62
   */
  void setE62(String E62);

  /**
   * Get le e64.
   * 
   * @return le e64
   */
  @FieldXPath("E64")
  String getE64();

  /**
   * Set le e64.
   * 
   * @param E64
   *          le nouveau e64
   */
  void setE64(String E64);

  /**
   * Get le e65.
   * 
   * @return le e65
   */
  @FieldXPath("E65")
  XmlString[] getE65();

  /**
   * Ajoute le to e65.
   * 
   * @return le xml string
   */
  XmlString addToE65();

  /**
   * Set le e65.
   * 
   * @param E65
   *          le nouveau e65
   */
  void setE65(XmlString[] E65);

  /**
   * Get le e66.
   * 
   * @return le e66
   */
  @FieldXPath("E66")
  String getE66();

  /**
   * Set le e66.
   * 
   * @param E66
   *          le nouveau e66
   */
  void setE66(String E66);

  /**
   * Get le e63.
   * 
   * @return le e63
   */
  @FieldXPath("E63")
  IE63[] getE63();

  /**
   * Ajoute le to e63.
   * 
   * @return le i e63
   */
  IE63 addToE63();

  /**
   * Set le e63.
   * 
   * @param E63
   *          le nouveau e63
   */
  void setE63(IE63[] E63);

  /**
   * Get le e67.
   * 
   * @return le e67
   */
  @FieldXPath("E67")
  String getE67();

  /**
   * Set le e67.
   * 
   * @param E67
   *          le nouveau e67
   */
  void setE67(String E67);

  /**
   * Get le e68.
   * 
   * @return le e68
   */
  @FieldXPath("E68")
  String getE68();

  /**
   * Set le e68.
   * 
   * @param E68
   *          le nouveau e68
   */
  void setE68(String E68);

}
