package fr.ge.common.regent.bean.formalite.dialogue.regularisation;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import fr.ge.common.regent.bean.formalite.dialogue.AbstractPersonneLieeBean;

/**
 * Le Class PersonneLieeBean.
 */
public class PersonneLieeBean extends AbstractPersonneLieeBean {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 6539284808238674120L;

    /** Le id technique. */
    private String idTechnique;

    /**
     * Get le id technique.
     *
     * @return the idTechnique
     */
    public String getIdTechnique() {
        return idTechnique;
    }

    /**
     * Set le id technique.
     *
     * @param idTechnique
     *            the idTechnique to set
     */
    public void setIdTechnique(String idTechnique) {
        this.idTechnique = idTechnique;
    }

    /**
     * Instancie un nouveau personne liee bean.
     */
    public PersonneLieeBean() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see fr.guichetentreprises.vue.AbstractPersonneLieeBean#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

}
