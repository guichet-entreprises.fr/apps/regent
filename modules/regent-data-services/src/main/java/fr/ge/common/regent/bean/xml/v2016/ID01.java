package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;
import org.xmlfield.core.types.XmlString;

/**
 * Le Interface ID01.
 */
@ResourceXPath("/D01")
public interface ID01 {

  /**
   * Get le D011.
   *
   * @return le D011
   */
  @FieldXPath("D01.1")
  String getD011();

  /**
   * Set le D011.
   *
   * @param D011
   *          le nouveau D011
   */
  void setD011(String D011);

  /**
   * Get le D012.
   *
   * @return le D012
   */
  @FieldXPath("D01.2")
  String getD012();

  /**
   * Set le D012.
   *
   * @param D012
   *          le nouveau D012
   */
  void setD012(String D012);

  /**
   * Get le D013.
   *
   * @return le D013
   */
  @FieldXPath("D01.3")
  XmlString[] getD013();

  /**
   * Add To le D013.
   * 
   * @return D013
   */
  XmlString addToD013();

  /**
   * Set le D013.
   *
   * @param D013
   *          le nouveau D013
   */
  void setD013(XmlString[] D013);

  /**
   * Get le D013.
   *
   * @return le D013
   */
  @FieldXPath("D01.4")
  String getD014();

  /**
   * Set le D013.
   *
   * @param D013
   *          le nouveau D013
   */
  void setD014(String D014);

}
