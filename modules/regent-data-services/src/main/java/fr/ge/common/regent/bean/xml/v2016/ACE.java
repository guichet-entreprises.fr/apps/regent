package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface ACE.
 */
@ResourceXPath("/ACE")
public interface ACE {

  /**
   * Get le e70.
   *
   * @return le e70
   */
  @FieldXPath("E70")
  String getE70();

  /**
   * Set le e70.
   *
   * @param E70
   *          le nouveau e70
   */
  void setE70(String E70);

  /**
   * Get le e71.
   *
   * @return le e71
   */
  @FieldXPath("E71")
  String getE71();

  /**
   * Set le e71.
   *
   * @param E71
   *          le nouveau e71
   */
  void setE71(String E71);

  /**
   * Get le e78.
   *
   * @return le e78
   */
  @FieldXPath("E78")
  String getE78();

  /**
   * Set le e78.
   *
   * @param E78
   *          le nouveau e78
   */
  void setE78(String E78);

  /**
   * Get le e79.
   *
   * @return le e79
   */
  @FieldXPath("E79")
  IE79[] getE79();

  /**
   * Ajoute le to e79.
   *
   * @return le i e79
   */
  IE79 addToE79();

  /**
   * Set le e79.
   *
   * @param E79
   *          le nouveau e79
   */
  void setE79(IE79[] E79);

  /**
   * Get le e76.
   *
   * @return le e76
   */
  @FieldXPath("E76")
  IE76[] getE76();

  /**
   * Ajoute le to e76.
   *
   * @return le i e76
   */
  IE76 addToE76();

  /**
   * Set le e76.
   *
   * @param E76
   *          le nouveau e76
   */
  void setE76(IE76[] E76);

  /**
   * Get le e80.
   *
   * @return le e80
   */
  @FieldXPath("E80")
  String getE80();

  /**
   * Set le e80.
   *
   * @param E80
   *          le nouveau e80
   */
  void setE80(String E80);

}
