package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface JES.
 */
@ResourceXPath("/JES")
public interface IJES {

  /**
   * Get le A30.
   *
   * @return le A30
   */
  @FieldXPath("A30")
  IA30[] getA30();

  /**
   * Ajoute le to A30.
   *
   * @return le A30
   */
  IA30 addToA30();

  /**
   * Set le A30.
   *
   * @param A30
   *          le nouveau A30
   */
  void setA30(IA30[] A30);
}
