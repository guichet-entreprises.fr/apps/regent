package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface Entreprise.
 */
@ResourceXPath("/Entreprise")
public interface Entreprise {

  /**
   * Get le ipu.
   *
   * @return le ipu
   */
  @FieldXPath("IPU")
  IPU[] getIPU();

  /**
   * Ajoute le to ipu.
   *
   * @return le ipu
   */
  IPU addToIPU();

  /**
   * Set le ipu.
   *
   * @param IPU
   *          le nouveau ipu
   */
  void setIPU(IPU[] IPU);

  /**
   * Get le siu.
   *
   * @return le siu
   */
  @FieldXPath("SIU")
  SIU[] getSIU();

  /**
   * Ajoute le to siu.
   *
   * @return le siu
   */
  SIU addToSIU();

  /**
   * Set le siu.
   *
   * @param SIU
   *          le nouveau siu
   */
  void setSIU(SIU[] SIU);

  /**
   * Get le cpu.
   *
   * @return le cpu
   */
  @FieldXPath("CPU")
  CPU[] getCPU();

  /**
   * Ajoute le to cpu.
   *
   * @return le cpu
   */
  CPU addToCPU();

  /**
   * Set le cpu.
   *
   * @param CPU
   *          le nouveau cpu
   */
  void setCPU(CPU[] CPU);

  /**
   * Get le aqu.
   *
   * @return le aqu
   */
  @FieldXPath("AQU")
  AQU[] getAQU();

  /**
   * Ajoute le to aqu.
   *
   * @return le aqu
   */
  AQU addToAQU();

  /**
   * Set le aqu.
   *
   * @param AQU
   *          le nouveau aqu
   */
  void setAQU(AQU[] AQU);

  /**
   * Get le RFU.
   *
   * @return le RFU
   */
  @FieldXPath("RFU")
  RFU[] getRFU();

  /**
   * Ajoute le to RFU.
   *
   * @return le RFU
   */
  RFU addToRFU();

  /**
   * Set le RFU.
   *
   * @param RFU
   *          le nouveau RFU
   */
  void setRFU(RFU[] RFU);

  /**
   * Get le OVU.
   *
   * @return le OVU
   */
  @FieldXPath("OVU")
  OVU[] getOVU();

  /**
   * Ajoute le to OVU.
   *
   * @return le OVU
   */
  OVU addToOVU();

  /**
   * Set le OVU.
   *
   * @param OVU
   *          le nouveau OVU
   */
  void setOVU(OVU[] OVU);

  /**
   * Get le jgu.
   *
   * @return le jgu
   */
  @FieldXPath("JGU")
  JGU[] getJGU();

  /**
   * Ajoute le to jgu.
   *
   * @return le jgu
   */
  JGU addToJGU();

  /**
   * Set le jgu.
   *
   * @param JGU
   *          le nouveau jgu
   */
  void setJGU(JGU[] JGU);

  /**
   * Get le jmu.
   *
   * @return le jmu
   */
  @FieldXPath("JMU")
  JMU[] getJMU();

  /**
   * Ajoute le to jmu.
   *
   * @return le jmu
   */
  JMU addToJMU();

  /**
   * Set le jmu.
   *
   * @param JGU
   *          le nouveau jmu
   */
  void setJMU(JGU[] JGU);

  /**
   * Get le etablissement.
   *
   * @return le etablissement
   */
  @FieldXPath("Etablissement")
  Etablissement[] getEtablissement();

  /**
   * Ajoute le to etablissement.
   *
   * @return le etablissement
   */
  Etablissement addToEtablissement();

  /**
   * Set le etablissement.
   *
   * @param Etablissement
   *          le nouveau etablissement
   */
  void setEtablissement(Etablissement[] Etablissement);

  /**
   * Get le etablissement ferme.
   *
   * @return le etablissement ferme
   */
  @FieldXPath("EtablissementFerme")
  EtablissementFerme[] getEtablissementFerme();

  /**
   * Ajoute le to etablissement ferme.
   *
   * @return le etablissement ferme
   */
  EtablissementFerme addToEtablissementFerme();

  /**
   * Set le etablissement ferme.
   *
   * @param EtablissementFerme
   *          le nouveau etablissement ferme
   */
  void setEtablissementFerme(EtablissementFerme[] EtablissementFerme);

  /**
   * Get le personne physique dirigeante.
   *
   * @return le personne physique dirigeante
   */
  @FieldXPath("PersonnePhysiqueDirigeante")
  PersonnePhysiqueDirigeante[] getPersonnePhysiqueDirigeante();

  /**
   * Ajoute le to personne physique dirigeante.
   *
   * @return le personne physique dirigeante
   */
  PersonnePhysiqueDirigeante addToPersonnePhysiqueDirigeante();

  /**
   * Set le personne physique dirigeante.
   *
   * @param PersonnePhysiqueDirigeante
   *          le nouveau personne physique dirigeante
   */
  void setPersonnePhysiqueDirigeante(PersonnePhysiqueDirigeante[] PersonnePhysiqueDirigeante);

  /**
   * Get le OFU.
   *
   * @return le OFU
   */
  @FieldXPath("OFU")
  OFU[] getOFU();

  /**
   * Ajoute le to OFU.
   *
   * @return le OFU
   */
  OFU addToOFU();

  /**
   * Set le OFU.
   *
   * @param OFU
   *          le nouveau OFU
   */
  void setOFU(OFU[] OFU);

  /**
   * Get le ISU.
   *
   * @return le ISU
   */
  @FieldXPath("ISU")
  IISU[] getISU();

  /**
   * Ajoute le to ISU.
   *
   * @return le ISU
   */
  IISU addToISU();

  /**
   * Set le ISU.
   *
   * @param ISU
   *          le nouveau ISU
   */
  void setISU(IISU[] ISU);

  /**
   * Get le OIU.
   *
   * @return le OIU
   */
  @FieldXPath("OIU")
  IOIU[] getOIU();

  /**
   * Ajoute le to OIU.
   *
   * @return le OIU
   */
  IOIU addToOIU();

  /**
   * Set le OIU.
   *
   * @param OIU
   *          le nouveau OIU
   */
  void setOIU(IOIU[] OIU);

  /**
   * Get le LIU.
   *
   * @return le LIU
   */
  @FieldXPath("LIU")
  ILIU[] getLIU();

  /**
   * Ajoute le to LIU.
   *
   * @return le LIU
   */
  ILIU addToLIU();

  /**
   * Set le LIU.
   *
   * @param LIU
   *          le nouveau LIU
   */
  void setLIU(ILIU[] LIU);

  /**
   * Accesseur sur l'attribut {@link #personne morale dirigeante}.
   *
   * @return personne morale dirigeante
   */
  @FieldXPath("PersonneMoraleDirigeante")
  PersonneMoraleDirigeante[] getPersonneMoraleDirigeante();

  /**
   * Ajoute la personne physique dirigeante.
   *
   * @return la personne physique dirigeante
   */
  PersonneMoraleDirigeante addToPersonneMoraleDirigeante();

  /**
   * Mutateur sur l'attribut {@link #personne morale dirigeante}.
   *
   * @param PersonneMoraleDirigeante
   *          la nouvelle valeur de l'attribut personne morale dirigeante
   */
  void setPersonneMoraleDirigeante(PersonneMoraleDirigeante[] PersonneMoraleDirigeante);
}
