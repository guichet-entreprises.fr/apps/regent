/**
 * 
 */
package fr.ge.common.regent.constante.enumeration;

/**
 * @author $Author: traimbau $
 * @version $Revision: 0 $
 */
public enum InfosXmlTcEnum {

	GREFFE("G", "10001001", "10000001", "V2012.02"), CCI("C", "00161001", "00161002", "V2009.01"), CMA("M", "00161001",
			"00161003",
			"V2009.01"), URSSAF("U", "00161001", "00161005", "V2009.01"), CA("X", "00161001", "00161004", "V2009.01");

	String network;
	String emetteurCode;
	String destinataireCode;
	String versionXmlTc;

	/**
	 * Constructeur de la classe.
	 *
	 * @param network
	 * @param emetteurCode
	 * @param destinataireCode
	 * @param versionXmlTc
	 */
	private InfosXmlTcEnum(String network, String emetteurCode, String destinataireCode, String versionXmlTc) {
		this.network = network;
		this.emetteurCode = emetteurCode;
		this.destinataireCode = destinataireCode;
		this.versionXmlTc = versionXmlTc;
	}

	/**
	 * Accesseur sur l'attribut {@link #network}.
	 *
	 * @return char network
	 */
	public String getNetwork() {
		return network;
	}

	/**
	 * Mutateur sur l'attribut {@link #network}.
	 *
	 * @param network
	 *            la nouvelle valeur de l'attribut network
	 */
	public void setNetwork(String network) {
		this.network = network;
	}

	/**
	 * Accesseur sur l'attribut {@link #emetteurCode}.
	 *
	 * @return String emetteurCode
	 */
	public String getEmetteurCode() {
		return emetteurCode;
	}

	/**
	 * Mutateur sur l'attribut {@link #emetteurCode}.
	 *
	 * @param emetteurCode
	 *            la nouvelle valeur de l'attribut emetteurCode
	 */
	public void setEmetteurCode(String emetteurCode) {
		this.emetteurCode = emetteurCode;
	}

	/**
	 * Accesseur sur l'attribut {@link #destinataireCode}.
	 *
	 * @return String destinataireCode
	 */
	public String getDestinataireCode() {
		return destinataireCode;
	}

	/**
	 * Mutateur sur l'attribut {@link #destinataireCode}.
	 *
	 * @param destinataireCode
	 *            la nouvelle valeur de l'attribut destinataireCode
	 */
	public void setDestinataireCode(String destinataireCode) {
		this.destinataireCode = destinataireCode;
	}

	/**
	 * Accesseur sur l"attribut {@link #versionXmlTc}.
	 *
	 * @return String versionXmlTc
	 */
	public String getVersionXmlTc() {
		return versionXmlTc;
	}

	/**
	 * Mutateur sur l"attribut {@link #versionXmlTc}.
	 *
	 * @param versionXmlTc
	 *            la nouvelle valeur de l'attribut versionXmlTc
	 */
	public void setVersionXmlTc(String versionXmlTc) {
		this.versionXmlTc = versionXmlTc;
	}

}
