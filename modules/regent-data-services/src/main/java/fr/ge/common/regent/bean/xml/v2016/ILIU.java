package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Interface ILIU.
 */
@ResourceXPath("/LIU")
public interface ILIU {

  /**
   * Accesseur sur l'attribut {@link #U36}.
   *
   * @return U36
   */
  @FieldXPath("U36")
  IU36[] getU36();

  /**
   * Ajoute U36.
   *
   * @return U36
   */
  IU36 addToU36();

  /**
   * Mutateur sur l'attribut {@link #U36}.
   *
   * @param U36
   *          la nouvelle valeur de l'attribut U36
   */
  void setU36(IU36[] U36);
}
