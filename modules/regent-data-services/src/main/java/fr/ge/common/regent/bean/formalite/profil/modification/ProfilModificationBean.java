package fr.ge.common.regent.bean.formalite.profil.modification;

import static fr.ge.common.regent.util.CodeAPEUtils.transformeCodeAPE;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import fr.ge.common.regent.bean.formalite.dialogue.creation.PostalCommuneBean;
import fr.ge.common.regent.bean.formalite.profil.AbstractProfilEntreprise;

/**
 * L'objet Bean pour A1 : Modification.
 * 
 * @author hhichri
 */
public class ProfilModificationBean extends AbstractProfilEntreprise {

    /** serialVersionUID. */
    private static final long serialVersionUID = 7413819746143371927L;

    /** Forme Juridique. */
    private String formeJuridique;

    /** Micro Social Oui Non. */
    private String microSocialOuiNon;

    /** Modig Oui Non. */
    private String modifOuiNon;

    /** Modif Evenements 2. */
    private List<String> modifEvenements2 = new ArrayList<String>();

    /** Activite Principale Domaine. */
    private String activitePrincipaleDomaine;

    /** activite Principale Secteur. */
    private String activitePrincipaleSecteur;

    /** activitePrincipaleCodeAPE. */
    private String activitePrincipaleCodeAPE;

    /** La description libre de l'activité principale. */
    private String descrLibreActivitePrincipale;

    /** Activite Principale Secteur 2. */
    private String activitePrincipaleSecteur2;

    /** Existe Activité Secondaire. */
    private String existeActiviteSecondaire;

    /** activiteSecondaireDomaine. */
    private String activiteSecondaireDomaine;

    /** activiteSecondaireSecteur. */
    private String activiteSecondaireSecteur;

    /** La description libre de l'activité secondaire. */
    private String descrLibreActiviteSecondaire;

    /** postal Commune 2. */
    private PostalCommuneBean postalCommune2 = new PostalCommuneBean();

    /** Le postal commune1. */
    private PostalCommuneBean postalCommune1 = new PostalCommuneBean();

    /** Option CMA CCI 1. */
    private String optionCMACCI1;

    /** Option CMA CCI 2. */
    private String optionCMACCI2;

    /** Est inscrit au registre public. */
    private String estInscritRegistrePublic;

    /** Le numero de la formalite. */
    private String numeroFormalite;

    /** optionCMACCI. */
    private String optionCMACCI;

    /** modifEvenements. */
    private List<String> modifEvenements = new ArrayList<String>();

    /** Type CFE. */
    private String typeCfe;

    /** Reseau CFE. */
    private String reseauCFE;

    /** Aqpa. */
    private boolean aqpa;

    /**
     * Valeur technique, informe si l'utilisateur s'est connecté via dgfip ou
     * pas.
     */
    private String provenanceImpot;

    /**
     * Get le forme juridique.
     * 
     * @return the formeJuridique
     */
    public String getFormeJuridique() {
        return this.formeJuridique;
    }

    /**
     * Get provenanceImpot.
     * 
     * @return the provenanceImpot
     */
    public String getProvenanceImpot() {
        return provenanceImpot;
    }

    /**
     * Set provenanceImpot.
     * 
     * @param provenanceImpot
     *            the provenanceImpot to set
     */
    public void setProvenanceImpot(final String provenanceImpot) {
        this.provenanceImpot = provenanceImpot;
    }

    /**
     * Set le forme juridique.
     * 
     * @param formeJuridique
     *            the formeJuridique to set
     */
    public void setFormeJuridique(final String formeJuridique) {
        this.formeJuridique = formeJuridique;
    }

    /**
     * Get le micro social oui non.
     * 
     * @return the microSocialOuiNon
     */
    public String getMicroSocialOuiNon() {
        return this.microSocialOuiNon;
    }

    /**
     * Set le micro social oui non.
     * 
     * @param microSocialOuiNon
     *            the microSocialOuiNon to set
     */
    public void setMicroSocialOuiNon(final String microSocialOuiNon) {
        this.microSocialOuiNon = microSocialOuiNon;
    }

    /**
     * Get le modif oui non.
     * 
     * @return the modifOuiNon
     */
    public String getModifOuiNon() {
        return this.modifOuiNon;
    }

    /**
     * Set le modif oui non.
     * 
     * @param modifOuiNon
     *            the modifOuiNon to set
     */
    public void setModifOuiNon(final String modifOuiNon) {
        this.modifOuiNon = modifOuiNon;
    }

    /**
     * Get le modif evenements2.
     * 
     * @return the modifEvenements2
     */
    public List<String> getModifEvenements2() {
        return this.modifEvenements2;
    }

    /**
     * Set le modif evenements2.
     * 
     * @param modifEvenements2
     *            the modifEvenements2 to set
     */
    public void setModifEvenements2(final List<String> modifEvenements2) {
        this.modifEvenements2 = modifEvenements2;
    }

    /**
     * Get le activite principale domaine.
     * 
     * @return the activitePrincipaleDomaine
     */
    public String getActivitePrincipaleDomaine() {
        return this.activitePrincipaleDomaine;
    }

    /**
     * Set le activite principale domaine.
     * 
     * @param activitePrincipaleDomaine
     *            the activitePrincipaleDomaine to set
     */
    public void setActivitePrincipaleDomaine(final String activitePrincipaleDomaine) {
        this.activitePrincipaleDomaine = activitePrincipaleDomaine;
    }

    /**
     * Get le activite principale secteur.
     * 
     * @return the activitePrincipaleSecteur
     */
    public String getActivitePrincipaleSecteur() {
        return this.activitePrincipaleSecteur;
    }

    /**
     * Set le activite principale secteur.
     * 
     * @param activitePrincipaleSecteur
     *            the activitePrincipaleSecteur to set
     */
    public void setActivitePrincipaleSecteur(final String activitePrincipaleSecteur) {
        this.activitePrincipaleSecteur = activitePrincipaleSecteur;
    }

    /**
     * Get le activite principale code ape.
     * 
     * @return the activitePrincipaleCodeAPE
     */
    public String getActivitePrincipaleCodeAPE() {
        return transformeCodeAPE(this.activitePrincipaleCodeAPE);
    }

    /**
     * Set le activite principale code ape.
     * 
     * @param activitePrincipaleCodeAPE
     *            the activitePrincipaleCodeAPE to set
     */
    public void setActivitePrincipaleCodeAPE(final String activitePrincipaleCodeAPE) {
        this.activitePrincipaleCodeAPE = transformeCodeAPE(activitePrincipaleCodeAPE);
    }

    /**
     * Get le activite principale secteur2.
     * 
     * @return the activitePrincipaleSecteur2
     */
    public String getActivitePrincipaleSecteur2() {
        return this.activitePrincipaleSecteur2;
    }

    /**
     * Set le activite principale secteur2.
     * 
     * @param activitePrincipaleSecteur2
     *            the activitePrincipaleSecteur2 to set
     */
    public void setActivitePrincipaleSecteur2(final String activitePrincipaleSecteur2) {
        this.activitePrincipaleSecteur2 = activitePrincipaleSecteur2;
    }

    /**
     * Get le existe activite secondaire.
     * 
     * @return the existeActiviteSecondaire
     */
    public String getExisteActiviteSecondaire() {
        return this.existeActiviteSecondaire;
    }

    /**
     * Set le existe activite secondaire.
     * 
     * @param existeActiviteSecondaire
     *            the existeActiviteSecondaire to set
     */
    public void setExisteActiviteSecondaire(final String existeActiviteSecondaire) {
        this.existeActiviteSecondaire = existeActiviteSecondaire;
    }

    /**
     * Get le activite secondaire domaine.
     * 
     * @return the activiteSecondaireDomaine
     */
    public String getActiviteSecondaireDomaine() {
        return this.activiteSecondaireDomaine;
    }

    /**
     * Set le activite secondaire domaine.
     * 
     * @param activiteSecondaireDomaine
     *            the activiteSecondaireDomaine to set
     */
    public void setActiviteSecondaireDomaine(final String activiteSecondaireDomaine) {
        this.activiteSecondaireDomaine = activiteSecondaireDomaine;
    }

    /**
     * Accesseur sur l'attribut {@link #descrLibreActivitePrincipale}.
     * 
     * @return String descrLibreActivitePrincipale
     */
    public String getDescrLibreActivitePrincipale() {
        return descrLibreActivitePrincipale;
    }

    /**
     * Mutateur sur l'attribut {@link #descrLibreActivitePrincipale}.
     * 
     * @param descrLibreActivitePrincipale
     *            la nouvelle valeur de l'attribut descrLibreActivitePrincipale
     */
    public void setDescrLibreActivitePrincipale(final String descrLibreActivitePrincipale) {
        this.descrLibreActivitePrincipale = descrLibreActivitePrincipale;
    }

    /**
     * Get le activite secondaire secteur.
     * 
     * @return the activiteSecondaireSecteur
     */
    public String getActiviteSecondaireSecteur() {
        return this.activiteSecondaireSecteur;
    }

    /**
     * Set le activite secondaire secteur.
     * 
     * @param activiteSecondaireSecteur
     *            the activiteSecondaireSecteur to set
     */
    public void setActiviteSecondaireSecteur(final String activiteSecondaireSecteur) {
        this.activiteSecondaireSecteur = activiteSecondaireSecteur;
    }

    /**
     * Get le postal commune2.
     * 
     * @return the postalCommune2
     */
    public PostalCommuneBean getPostalCommune2() {
        return this.postalCommune2;
    }

    /**
     * Set le postal commune2.
     * 
     * @param postalCommune2
     *            the postalCommune2 to set
     */
    public void setPostalCommune2(final PostalCommuneBean postalCommune2) {
        this.postalCommune2 = postalCommune2;
    }

    /**
     * Get le option cmacc i1.
     * 
     * @return the optionCMACCI1
     */
    public String getOptionCMACCI1() {
        return this.optionCMACCI1;
    }

    /**
     * Set le option cmacc i1.
     * 
     * @param optionCMACCI1
     *            the optionCMACCI1 to set
     */
    public void setOptionCMACCI1(final String optionCMACCI1) {
        this.optionCMACCI1 = optionCMACCI1;
    }

    /**
     * Get le option cmacc i2.
     * 
     * @return the optionCMACCI2
     */
    public String getOptionCMACCI2() {
        return this.optionCMACCI2;
    }

    /**
     * Set le option cmacc i2.
     * 
     * @param optionCMACCI2
     *            the optionCMACCI2 to set
     */
    public void setOptionCMACCI2(final String optionCMACCI2) {
        this.optionCMACCI2 = optionCMACCI2;
    }

    /**
     * Get le est inscrit registre public.
     * 
     * @return the estInscritRegistrePublic
     */
    public String getEstInscritRegistrePublic() {
        return this.estInscritRegistrePublic;
    }

    /**
     * Set le est inscrit registre public.
     * 
     * @param estInscritRegistrePublic
     *            the estInscritRegistrePublic to set
     */
    public void setEstInscritRegistrePublic(final String estInscritRegistrePublic) {
        this.estInscritRegistrePublic = estInscritRegistrePublic;
    }

    /**
     * Get le numero formalite.
     * 
     * @return the numeroFormalite
     */
    public String getNumeroFormalite() {
        return this.numeroFormalite;
    }

    /**
     * Set le numero formalite.
     * 
     * @param numeroFormalite
     *            the numeroFormalite to set
     */
    public void setNumeroFormalite(final String numeroFormalite) {
        this.numeroFormalite = numeroFormalite;
    }

    /**
     * Get le modif evenements.
     * 
     * @return the modifEvenements
     */
    public List<String> getModifEvenements() {
        return this.modifEvenements;
    }

    /**
     * Set le modif evenements.
     * 
     * @param modifEvenements
     *            the modifEvenements to set
     */
    public void setModifEvenements(final List<String> modifEvenements) {
        this.modifEvenements = modifEvenements;
    }

    /**
     * Get le type cfe.
     * 
     * @return the typeCfe
     */
    public String getTypeCfe() {
        return this.typeCfe;
    }

    /**
     * Set le type cfe.
     * 
     * @param typeCfe
     *            the typeCfe to set
     */
    public void setTypeCfe(final String typeCfe) {
        this.typeCfe = typeCfe;
    }

    /**
     * Get le option cmacci.
     * 
     * @return the optionCMACCI
     */
    public String getOptionCMACCI() {
        return this.optionCMACCI;
    }

    /**
     * Set le option cmacci.
     * 
     * @param optionCMACCI
     *            the optionCMACCI to set
     */
    public void setOptionCMACCI(final String optionCMACCI) {
        this.optionCMACCI = optionCMACCI;
    }

    /**
     * Get le reseau cfe.
     * 
     * @return the reseauCFE
     */
    public String getReseauCFE() {
        return this.reseauCFE;
    }

    /**
     * Set le reseau cfe.
     * 
     * @param reseauCFE
     *            the reseauCFE to set
     */
    public void setReseauCFE(final String reseauCFE) {
        this.reseauCFE = reseauCFE;
    }

    /**
     * Verifie si c'est aqpa.
     * 
     * @return the aqpa
     */
    public boolean isAqpa() {
        return this.aqpa;
    }

    /**
     * Set le aqpa.
     * 
     * @param aqpa
     *            the aqpa to set
     */
    public void setAqpa(final boolean aqpa) {
        this.aqpa = aqpa;
    }

    /**
     * Get le postal commune1.
     * 
     * @return the postalCommune1
     */
    public PostalCommuneBean getPostalCommune1() {
        return this.postalCommune1;
    }

    /**
     * Set le postal commune1.
     * 
     * @param postalCommune1
     *            the postalCommune1 to set
     */
    public void setPostalCommune1(final PostalCommuneBean postalCommune1) {
        this.postalCommune1 = postalCommune1;
    }

    /**
     * Accesseur sur l'attribut {@link #descrLibreActiviteSecondaire}.
     * 
     * @return String descrLibreActiviteSecondaire
     */
    public String getDescrLibreActiviteSecondaire() {
        return descrLibreActiviteSecondaire;
    }

    /**
     * Mutateur sur l'attribut {@link #descrLibreActiviteSecondaire}.
     * 
     * @param descrLibreActiviteSecondaire
     *            la nouvelle valeur de l'attribut descrLibreActiviteSecondaire
     */
    public void setDescrLibreActiviteSecondaire(final String descrLibreActiviteSecondaire) {
        this.descrLibreActiviteSecondaire = descrLibreActiviteSecondaire;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

}
