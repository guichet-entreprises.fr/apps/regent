package fr.ge.common.regent.bean.formalite.dialogue.modification;

import java.io.Serializable;

import fr.ge.common.regent.bean.formalite.dialogue.AbstractFormalite;

/**
 * Le Class FormaliteModificationBean.
 */
public class DialogueModificationBean extends AbstractFormalite<EntrepriseBean> implements Serializable {

    /** serialVersionUID *. */
    private static final long serialVersionUID = 6995087239145470529L;

    /** Le resau cfe. */
    private String resauCFE;

    /** Le code commune activite. */
    private String codeCommuneActivite;

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see fr.guichetentreprises.vue.AbstractFormalite#createNewEntreprise()
     */
    @Override
    public EntrepriseBean createNewEntreprise() {
        return new EntrepriseBean();
    }

    /**
     * Get le resau cfe.
     * 
     * @return the resauCFE
     */
    public String getResauCFE() {
        return resauCFE;
    }

    /**
     * Set le resau cfe.
     * 
     * @param resauCFE
     *            the resauCFE to set
     */
    public void setResauCFE(String resauCFE) {
        this.resauCFE = resauCFE;
    }

    /**
     * Get le code commune activite.
     * 
     * @return the codeCommuneActivite
     */
    public String getCodeCommuneActivite() {
        return this.codeCommuneActivite;
    }

    /**
     * Set le code commune activite.
     * 
     * @param codeCommuneActivite
     *            the codeCommuneActivite to set
     */
    public void setCodeCommuneActivite(String codeCommuneActivite) {
        this.codeCommuneActivite = codeCommuneActivite;
    }

}
