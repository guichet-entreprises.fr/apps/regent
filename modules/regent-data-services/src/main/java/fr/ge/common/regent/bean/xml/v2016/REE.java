package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface REE.
 */
@ResourceXPath("/REE")
public interface REE {

  /**
   * Get le e51.
   *
   * @return le e51
   */
  @FieldXPath("E51")
  String getE51();

  /**
   * Set le e51.
   *
   * @param E51
   *          le nouveau e51
   */
  void setE51(String E51);

  /**
   * Get le e52.
   *
   * @return le e52
   */
  @FieldXPath("E52")
  String getE52();

  /**
   * Set le e52.
   *
   * @param E52
   *          le nouveau e52
   */
  void setE52(String E52);

  /**
   * Get le E53.
   *
   * @return le E53
   */
  @FieldXPath("E53")
  String getE53();

  /**
   * Set le E53.
   *
   * @param E53
   *          le nouveau E53
   */
  void setE53(String E53);

  /**
   * Get le E54.
   *
   * @return le E54
   */
  @FieldXPath("E54")
  String getE54();

  /**
   * Set le E54.
   *
   * @param E54
   *          le nouveau E54
   */
  void setE54(String E54);

  /**
   * Get le E55.
   *
   * @return le E55
   */
  @FieldXPath("E55")
  String getE55();

  /**
   * Set le E55.
   *
   * @param E55
   *          le nouveau E55
   */
  void setE55(String E55);

  /**
   * Get le E56.
   *
   * @return le E56
   */
  @FieldXPath("E56")
  String getE56();

  /**
   * Set le E56.
   *
   * @param E56
   *          le nouveau E56
   */
  void setE56(String E56);

}
