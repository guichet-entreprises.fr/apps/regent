package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface SIF.
 */
@ResourceXPath("/SIF")
public interface SIF {

  /**
   * Get le c40.
   *
   * @return le c40
   */
  @FieldXPath("C40")
  IC40[] getC40();

  /**
   * Ajoute le to c40.
   *
   * @return le i c40
   */
  IC40 addToC40();

  /**
   * Set le c40.
   *
   * @param C40
   *          le nouveau c40
   */
  void setC40(IC40[] C40);

  /**
   * Get le c41.
   *
   * @return le c41
   */
  @FieldXPath("C41")
  String getC41();

  /**
   * Set le c41.
   *
   * @param C41
   *          le nouveau c41
   */
  void setC41(String C41);

  /**
   * Get le c42.
   *
   * @return le c42
   */
  @FieldXPath("C42")
  String getC42();

  /**
   * Set le c42.
   *
   * @param C42
   *          le nouveau c42
   */
  void setC42(String C42);

  /**
   * Get le c43.
   *
   * @return le c43
   */
  @FieldXPath("C43")
  String getC43();

  /**
   * Set le c43.
   *
   * @param C43
   *          le nouveau c43
   */
  void setC43(String C43);

  /**
   * Get le c44.
   *
   * @return le c44
   */
  @FieldXPath("C44")
  String getC44();

  /**
   * Set le c44.
   *
   * @param C44
   *          le nouveau c44
   */
  void setC44(String C44);

  /**
   * Get le c45.
   *
   * @return le C45
   */
  @FieldXPath("C45")
  String getC45();

  /**
   * Set le C45.
   *
   * @param C45
   *          le nouveau C45
   */
  void setC45(String C45);
}
