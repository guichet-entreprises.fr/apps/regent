package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IE92.
 */
@ResourceXPath("/E92")
public interface IE92 {

  /**
   * Get le e921.
   *
   * @return le e921
   */
  @FieldXPath("E92.1")
  String getE921();

  /**
   * Set le e921.
   *
   * @param E921
   *          le nouveau e921
   */
  void setE921(String E921);

}
