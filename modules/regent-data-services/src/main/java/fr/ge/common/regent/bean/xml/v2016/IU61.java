package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * IU61.
 * 
 * @author $Author: hhichri $
 * @version $Revision: 0 $
 */
@ResourceXPath("/U61")
public interface IU61 {

  /**
   * Get le U61.1.
   *
   * @return le U61.1
   */
  @FieldXPath("U61.1")
  String getU611();

  /**
   * Set le U61.1.
   *
   * @param U611
   *          .1 le nouveau U61.1
   */
  void setU611(String U611);

  /**
   * Get le U61.2.
   *
   * @return le U61.2
   */
  @FieldXPath("U61.2")
  String getU612();

  /**
   * Set le U61.2.
   *
   * @param U612
   *          .2 le nouveau U61.2
   */
  void setU612(String U612);

}
