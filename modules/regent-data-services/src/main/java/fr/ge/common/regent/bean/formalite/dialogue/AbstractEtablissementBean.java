package fr.ge.common.regent.bean.formalite.dialogue;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.format.annotation.NumberFormat;

import fr.ge.common.regent.bean.formalite.FormaliteVueBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.AdresseBean;

/**
 * La classe AbstractEtablissementBean.
 * 
 * @author $Author: hhichri $
 * @version $Revision: 0 $
 * @param <T>
 */
public abstract class AbstractEtablissementBean<T extends AbstractActiviteSoumiseQualification> extends FormaliteVueBean {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 2911077573445327336L;

    /** Le domiciliation. */
    private String domiciliation;

    /** Le adresse. */
    private AdresseBean adresse = new AdresseBean();

    /** Le date debut activite. */
    private String dateDebutActivite;

    /** Le activite permanente saisonniere. */
    private String activitePermanenteSaisonniere;

    /** Le non sedentarite qualite. */
    private String nonSedentariteQualite;

    /** Le est ambulant ue. */
    private String estAmbulantUE;

    /** Le activites. */
    protected String activites;

    /** Le activite plus importante. */
    protected String activitePlusImportante;

    /** Le aqpa situation. */
    private String aqpaSituation;

    /** Le aqpa intitule diplome. */
    private String aqpaIntituleDiplome;

    /** Le activite nature. */
    private String activiteNature;

    /** Le activite nature autre. */
    private String activiteNatureAutre;

    /** Le activite lieu exercice magasin. */
    @NumberFormat
    private Integer activiteLieuExerciceMagasin;

    /** Le nom commercial professionnel. */
    private String nomCommercialProfessionnel;

    /** Le enseigne. */
    private String enseigne;

    /** Le origine fonds. */
    private String origineFonds;

    /** Le loueur mandant du fonds debut contrat. */
    private String loueurMandantDuFondsDebutContrat;

    /** Le loueur mandant du fonds duree contrat indeterminee. */
    private String loueurMandantDuFondsDureeContratIndeterminee;

    /** Le loueur mandant du fonds fin contrat. */
    private String loueurMandantDuFondsFinContrat;

    /** Le loueur mandant du fonds renouvellement tacite reconduction. */
    private String loueurMandantDuFondsRenouvellementTaciteReconduction;

    /** Le plan cession. */
    private String planCession;

    /** Le fonds uniquement artisanal. */
    private String fondsUniquementArtisanal;

    /** Le journal annonces legales nom. */
    private String journalAnnoncesLegalesNom;

    /** Le journal annonces legales date parution. */
    private String journalAnnoncesLegalesDateParution;

    /** Le personne pouvoir presence. */
    private String personnePouvoirPresence;

    /** Le personne pouvoir nombre. */
    @NumberFormat
    private Integer personnePouvoirNombre;

    /** Le effectif salarie presence. */
    private String effectifSalariePresence;

    /** Le effectif salarie nombre. */
    @NumberFormat
    private Integer effectifSalarieNombre;

    /** Le effectif salarie apprentis. */
    @NumberFormat
    private Integer effectifSalarieApprentis;

    /** Le categorie. */
    private String categorie;

    /** Le situation. */
    private String situation;

    /** Le effectif salarie embauche premier salarie. */
    private String effectifSalarieEmbauchePremierSalarie;

    /** Le nombre dirigeants. */
    @NumberFormat
    private Integer nombreActivitesSoumisesQualification;

    /** Liste des activités soumises à qualification. **/
    private List<T> activitesSoumisesQualification;

    /**
     * Accesseur sur l'attribut {@link #activitesSoumisesQualification}.
     *
     * @return List<T> activitesSoumisesQualification
     */
    public List<T> getActivitesSoumisesQualification() {
        return activitesSoumisesQualification;
    }

    /**
     * Mutateur sur l'attribut {@link #activitesSoumisesQualification}.
     *
     * @param activitesSoumisesQualification
     *            la nouvelle valeur de l'attribut
     *            activitesSoumisesQualification
     */
    public void setActivitesSoumisesQualification(List<T> activitesSoumisesQualification) {
        this.activitesSoumisesQualification = activitesSoumisesQualification;
    }

    /**
     * Get le domiciliation.
     *
     * @return the domiciliation
     */
    public String getDomiciliation() {
        return domiciliation;
    }

    /**
     * Set le domiciliation.
     *
     * @param domiciliation
     *            the domiciliation to set
     */
    public void setDomiciliation(String domiciliation) {
        this.domiciliation = domiciliation;
    }

    /**
     * Get le adresse.
     *
     * @return the adresse
     */
    public AdresseBean getAdresse() {
        return adresse;
    }

    /**
     * Set le adresse.
     *
     * @param adresse
     *            the adresse to set
     */
    public void setAdresse(AdresseBean adresse) {
        this.adresse = adresse;
    }

    /**
     * Get le date debut activite.
     *
     * @return the dateDebutActivite
     */
    public String getDateDebutActivite() {
        return dateDebutActivite;
    }

    /**
     * Set le date debut activite.
     *
     * @param dateDebutActivite
     *            the dateDebutActivite to set
     */
    public void setDateDebutActivite(String dateDebutActivite) {
        this.dateDebutActivite = dateDebutActivite;
    }

    /**
     * Get le activite permanente saisonniere.
     *
     * @return the activitePermanenteSaisonniere
     */
    public String getActivitePermanenteSaisonniere() {
        return activitePermanenteSaisonniere;
    }

    /**
     * Set le activite permanente saisonniere.
     *
     * @param activitePermanenteSaisonniere
     *            the activitePermanenteSaisonniere to set
     */
    public void setActivitePermanenteSaisonniere(String activitePermanenteSaisonniere) {
        this.activitePermanenteSaisonniere = activitePermanenteSaisonniere;
    }

    /**
     * Get le non sedentarite qualite.
     *
     * @return the nonSedentariteQualite
     */
    public String getNonSedentariteQualite() {
        return nonSedentariteQualite;
    }

    /**
     * Set le non sedentarite qualite.
     *
     * @param nonSedentariteQualite
     *            the nonSedentariteQualite to set
     */
    public void setNonSedentariteQualite(String nonSedentariteQualite) {
        this.nonSedentariteQualite = nonSedentariteQualite;
    }

    /**
     * Get le est ambulant ue.
     *
     * @return the estAmbulantUE
     */
    public String getEstAmbulantUE() {
        return estAmbulantUE;
    }

    /**
     * Set le est ambulant ue.
     *
     * @param estAmbulantUE
     *            the estAmbulantUE to set
     */
    public void setEstAmbulantUE(String estAmbulantUE) {
        this.estAmbulantUE = estAmbulantUE;
    }

    /**
     * Get le activites.
     *
     * @return the activites
     */
    public String getActivites() {
        return activites;
    }

    /**
     * Set le activites.
     *
     * @param activites
     *            the activites to set
     */
    public void setActivites(String activites) {
        this.activites = activites;
    }

    /**
     * Get le activite plus importante.
     *
     * @return the activitePlusImportante
     */
    public String getActivitePlusImportante() {
        return activitePlusImportante;
    }

    /**
     * Set le activite plus importante.
     *
     * @param activitePlusImportante
     *            the activitePlusImportante to set
     */
    public void setActivitePlusImportante(String activitePlusImportante) {
        this.activitePlusImportante = activitePlusImportante;
    }

    /**
     * Get le aqpa situation.
     *
     * @return the aqpaSituation
     */
    public String getAqpaSituation() {
        return aqpaSituation;
    }

    /**
     * Set le aqpa situation.
     *
     * @param aqpaSituation
     *            the aqpaSituation to set
     */
    public void setAqpaSituation(String aqpaSituation) {
        this.aqpaSituation = aqpaSituation;
    }

    /**
     * Get le aqpa intitule diplome.
     *
     * @return the aqpaIntituleDiplome
     */
    public String getAqpaIntituleDiplome() {
        return aqpaIntituleDiplome;
    }

    /**
     * Set le aqpa intitule diplome.
     *
     * @param aqpaIntituleDiplome
     *            the aqpaIntituleDiplome to set
     */
    public void setAqpaIntituleDiplome(String aqpaIntituleDiplome) {
        this.aqpaIntituleDiplome = aqpaIntituleDiplome;
    }

    /**
     * Get le activite nature.
     *
     * @return the activiteNature
     */
    public String getActiviteNature() {
        return activiteNature;
    }

    /**
     * Set le activite nature.
     *
     * @param activiteNature
     *            the activiteNature to set
     */
    public void setActiviteNature(String activiteNature) {
        this.activiteNature = activiteNature;
    }

    /**
     * Get le activite nature autre.
     *
     * @return the activiteNatureAutre
     */
    public String getActiviteNatureAutre() {
        return activiteNatureAutre;
    }

    /**
     * Set le activite nature autre.
     *
     * @param activiteNatureAutre
     *            the activiteNatureAutre to set
     */
    public void setActiviteNatureAutre(String activiteNatureAutre) {
        this.activiteNatureAutre = activiteNatureAutre;
    }

    /**
     * Get le activite lieu exercice magasin.
     *
     * @return the activiteLieuExerciceMagasin
     */
    public Integer getActiviteLieuExerciceMagasin() {
        return activiteLieuExerciceMagasin;
    }

    /**
     * Set le activite lieu exercice magasin.
     *
     * @param activiteLieuExerciceMagasin
     *            the activiteLieuExerciceMagasin to set
     */
    public void setActiviteLieuExerciceMagasin(Integer activiteLieuExerciceMagasin) {
        this.activiteLieuExerciceMagasin = activiteLieuExerciceMagasin;
    }

    /**
     * Get le nom commercial professionnel.
     *
     * @return the nomCommercialProfessionnel
     */
    public String getNomCommercialProfessionnel() {
        return nomCommercialProfessionnel;
    }

    /**
     * Set le nom commercial professionnel.
     *
     * @param nomCommercialProfessionnel
     *            the nomCommercialProfessionnel to set
     */
    public void setNomCommercialProfessionnel(String nomCommercialProfessionnel) {
        this.nomCommercialProfessionnel = nomCommercialProfessionnel;
    }

    /**
     * Get le enseigne.
     *
     * @return the enseigne
     */
    public String getEnseigne() {
        return enseigne;
    }

    /**
     * Set le enseigne.
     *
     * @param enseigne
     *            the enseigne to set
     */
    public void setEnseigne(String enseigne) {
        this.enseigne = enseigne;
    }

    /**
     * Get le origine fonds.
     *
     * @return the origineFonds
     */
    public String getOrigineFonds() {
        return origineFonds;
    }

    /**
     * Set le origine fonds.
     *
     * @param origineFonds
     *            the origineFonds to set
     */
    public void setOrigineFonds(String origineFonds) {
        this.origineFonds = origineFonds;
    }

    /**
     * Get le loueur mandant du fonds debut contrat.
     *
     * @return the loueurMandantDuFondsDebutContrat
     */
    public String getLoueurMandantDuFondsDebutContrat() {
        return loueurMandantDuFondsDebutContrat;
    }

    /**
     * Set le loueur mandant du fonds debut contrat.
     *
     * @param loueurMandantDuFondsDebutContrat
     *            the loueurMandantDuFondsDebutContrat to set
     */
    public void setLoueurMandantDuFondsDebutContrat(String loueurMandantDuFondsDebutContrat) {
        this.loueurMandantDuFondsDebutContrat = loueurMandantDuFondsDebutContrat;
    }

    /**
     * Get le loueur mandant du fonds duree contrat indeterminee.
     *
     * @return the loueurMandantDuFondsDureeContratIndeterminee
     */
    public String getLoueurMandantDuFondsDureeContratIndeterminee() {
        return loueurMandantDuFondsDureeContratIndeterminee;
    }

    /**
     * Set le loueur mandant du fonds duree contrat indeterminee.
     *
     * @param loueurMandantDuFondsDureeContratIndeterminee
     *            the loueurMandantDuFondsDureeContratIndeterminee to set
     */
    public void setLoueurMandantDuFondsDureeContratIndeterminee(String loueurMandantDuFondsDureeContratIndeterminee) {
        this.loueurMandantDuFondsDureeContratIndeterminee = loueurMandantDuFondsDureeContratIndeterminee;
    }

    /**
     * Get le loueur mandant du fonds fin contrat.
     *
     * @return the loueurMandantDuFondsFinContrat
     */
    public String getLoueurMandantDuFondsFinContrat() {
        return loueurMandantDuFondsFinContrat;
    }

    /**
     * Set le loueur mandant du fonds fin contrat.
     *
     * @param loueurMandantDuFondsFinContrat
     *            the loueurMandantDuFondsFinContrat to set
     */
    public void setLoueurMandantDuFondsFinContrat(String loueurMandantDuFondsFinContrat) {
        this.loueurMandantDuFondsFinContrat = loueurMandantDuFondsFinContrat;
    }

    /**
     * Get le loueur mandant du fonds renouvellement tacite reconduction.
     *
     * @return the loueurMandantDuFondsRenouvellementTaciteReconduction
     */
    public String getLoueurMandantDuFondsRenouvellementTaciteReconduction() {
        return loueurMandantDuFondsRenouvellementTaciteReconduction;
    }

    /**
     * Set le loueur mandant du fonds renouvellement tacite reconduction.
     *
     * @param loueurMandantDuFondsRenouvellementTaciteReconduction
     *            the loueurMandantDuFondsRenouvellementTaciteReconduction to
     *            set
     */
    public void setLoueurMandantDuFondsRenouvellementTaciteReconduction(String loueurMandantDuFondsRenouvellementTaciteReconduction) {
        this.loueurMandantDuFondsRenouvellementTaciteReconduction = loueurMandantDuFondsRenouvellementTaciteReconduction;
    }

    /**
     * Get le plan cession.
     *
     * @return the planCession
     */
    public String getPlanCession() {
        return planCession;
    }

    /**
     * Set le plan cession.
     *
     * @param planCession
     *            the planCession to set
     */
    public void setPlanCession(String planCession) {
        this.planCession = planCession;
    }

    /**
     * Get le fonds uniquement artisanal.
     *
     * @return the fondsUniquementArtisanal
     */
    public String getFondsUniquementArtisanal() {
        return fondsUniquementArtisanal;
    }

    /**
     * Set le fonds uniquement artisanal.
     *
     * @param fondsUniquementArtisanal
     *            the fondsUniquementArtisanal to set
     */
    public void setFondsUniquementArtisanal(String fondsUniquementArtisanal) {
        this.fondsUniquementArtisanal = fondsUniquementArtisanal;
    }

    /**
     * Get le journal annonces legales nom.
     *
     * @return the journalAnnoncesLegalesNom
     */
    public String getJournalAnnoncesLegalesNom() {
        return journalAnnoncesLegalesNom;
    }

    /**
     * Set le journal annonces legales nom.
     *
     * @param journalAnnoncesLegalesNom
     *            the journalAnnoncesLegalesNom to set
     */
    public void setJournalAnnoncesLegalesNom(String journalAnnoncesLegalesNom) {
        this.journalAnnoncesLegalesNom = journalAnnoncesLegalesNom;
    }

    /**
     * Get le journal annonces legales date parution.
     *
     * @return the journalAnnoncesLegalesDateParution
     */
    public String getJournalAnnoncesLegalesDateParution() {
        return journalAnnoncesLegalesDateParution;
    }

    /**
     * Set le journal annonces legales date parution.
     *
     * @param journalAnnoncesLegalesDateParution
     *            the journalAnnoncesLegalesDateParution to set
     */
    public void setJournalAnnoncesLegalesDateParution(String journalAnnoncesLegalesDateParution) {
        this.journalAnnoncesLegalesDateParution = journalAnnoncesLegalesDateParution;
    }

    /**
     * Get le personne pouvoir presence.
     *
     * @return the personnePouvoirPresence
     */
    public String getPersonnePouvoirPresence() {
        return personnePouvoirPresence;
    }

    /**
     * Set le personne pouvoir presence.
     *
     * @param personnePouvoirPresence
     *            the personnePouvoirPresence to set
     */
    public void setPersonnePouvoirPresence(String personnePouvoirPresence) {
        this.personnePouvoirPresence = personnePouvoirPresence;
    }

    /**
     * Get le personne pouvoir nombre.
     *
     * @return the personnePouvoirNombre
     */
    public Integer getPersonnePouvoirNombre() {
        return personnePouvoirNombre;
    }

    /**
     * Set le personne pouvoir nombre.
     *
     * @param personnePouvoirNombre
     *            the personnePouvoirNombre to set
     */
    public void setPersonnePouvoirNombre(Integer personnePouvoirNombre) {
        this.personnePouvoirNombre = personnePouvoirNombre;
    }

    /**
     * Get le categorie.
     *
     * @return the categorie
     */
    public String getCategorie() {
        return categorie;
    }

    /**
     * Set le categorie.
     *
     * @param categorie
     *            the categorie to set
     */
    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    /**
     * Get le situation.
     *
     * @return the situation
     */
    public String getSituation() {
        return situation;
    }

    /**
     * Set le situation.
     *
     * @param situation
     *            the situation to set
     */
    public void setSituation(String situation) {
        this.situation = situation;
    }

    /**
     * Get le effectif salarie presence.
     *
     * @return the effectifSalariePresence
     */
    public String getEffectifSalariePresence() {
        return effectifSalariePresence;
    }

    /**
     * Set le effectif salarie presence.
     *
     * @param effectifSalariePresence
     *            the effectifSalariePresence to set
     */
    public void setEffectifSalariePresence(String effectifSalariePresence) {
        this.effectifSalariePresence = effectifSalariePresence;
    }

    /**
     * Get le effectif salarie nombre.
     *
     * @return the effectifSalarieNombre
     */
    public Integer getEffectifSalarieNombre() {
        return effectifSalarieNombre;
    }

    /**
     * Set le effectif salarie nombre.
     *
     * @param effectifSalarieNombre
     *            the effectifSalarieNombre to set
     */
    public void setEffectifSalarieNombre(Integer effectifSalarieNombre) {
        this.effectifSalarieNombre = effectifSalarieNombre;
    }

    /**
     * Get le effectif salarie apprentis.
     *
     * @return the effectifSalarieApprentis
     */
    public Integer getEffectifSalarieApprentis() {
        return effectifSalarieApprentis;
    }

    /**
     * Set le effectif salarie apprentis.
     *
     * @param effectifSalarieApprentis
     *            the effectifSalarieApprentis to set
     */
    public void setEffectifSalarieApprentis(Integer effectifSalarieApprentis) {
        this.effectifSalarieApprentis = effectifSalarieApprentis;
    }

    /**
     * Getter de l'attribut effectifSalarieEmbauchePremierSalarie.
     * 
     * @return la valeur de effectifSalarieEmbauchePremierSalarie
     */
    public String getEffectifSalarieEmbauchePremierSalarie() {
        return this.effectifSalarieEmbauchePremierSalarie;
    }

    /**
     * Setter de l'attribut effectifSalarieEmbauchePremierSalarie.
     * 
     * @param effectifSalarieEmbauchePremierSalarie
     *            la nouvelle valeur de effectifSalarieEmbauchePremierSalarie
     */
    public void setEffectifSalarieEmbauchePremierSalarie(String effectifSalarieEmbauchePremierSalarie) {
        this.effectifSalarieEmbauchePremierSalarie = effectifSalarieEmbauchePremierSalarie;
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    /**
     * Accesseur sur l'attribut {@link #nombreActivitesSoumisesQualification}.
     *
     * @return Integer nombreActivitesSoumisesQualification
     */
    public Integer getNombreActivitesSoumisesQualification() {
        return nombreActivitesSoumisesQualification;
    }

    /**
     * Mutateur sur l'attribut {@link #nombreActivitesSoumisesQualification}.
     *
     * @param nombreActivitesSoumisesQualification
     *            la nouvelle valeur de l'attribut
     *            nombreActivitesSoumisesQualification
     */
    public void setNombreActivitesSoumisesQualification(Integer nombreActivitesSoumisesQualification) {
        this.nombreActivitesSoumisesQualification = nombreActivitesSoumisesQualification;
    }

    /**
     * Instancie un nouveau abstract etablissement bean.
     */
    public AbstractEtablissementBean() {
        super();

        if (this.nombreActivitesSoumisesQualification == null) {

            activitesSoumisesQualification = new ArrayList<T>();
        } else {

            activitesSoumisesQualification = new ArrayList<T>(this.getNombreActivitesSoumisesQualification());
        }

    }
}
