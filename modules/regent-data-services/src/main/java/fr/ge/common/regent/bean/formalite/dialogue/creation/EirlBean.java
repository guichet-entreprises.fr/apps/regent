package fr.ge.common.regent.bean.formalite.dialogue.creation;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import fr.ge.common.regent.bean.formalite.dialogue.AbstractEirlBean;

/**
 * Le Class EirlBean.
 */
public class EirlBean extends AbstractEirlBean implements IFormaliteVue {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 8165788173506711655L;

    /** Le activites accessoires bicbnc. */
    private String activitesAccessoiresBICBNC;

    /** Le date cloture exercice comptable. */
    private String dateClotureExerciceComptable;

    /** Le denomination. */
    private String denomination;

    /** Le objet. */
    private String objet;

    /** Le objet portant sur ensemble activites. */
    private String objetPortantSurEnsembleActivites;

    /** Le precedent eirl denomination. */
    private String precedentEIRLDenomination;

    /** Le precedent eirl lieu immatriculation. */
    private String precedentEIRLLieuImmatriculation;

    /** Le precedent eirl registre. */
    private String precedentEIRLRegistre;

    /** Le precedent eirlsiren. */
    private String precedentEIRLSIREN;

    /** Le registre depot. */
    private String registreDepot;

    /** Le statut eirl. */
    private String statutEIRL;

    /** Le definition patrimoine. */
    private List<String> definitionPatrimoine = new ArrayList<String>();

    /**
     * Getter de l'attribut activitesAccessoiresBICBNC.
     * 
     * @return la valeur de activitesAccessoiresBICBNC
     */
    public String getActivitesAccessoiresBICBNC() {
        return this.activitesAccessoiresBICBNC;
    }

    /**
     * Getter de l'attribut dateClotureExerciceComptable.
     * 
     * @return la valeur de dateClotureExerciceComptable
     */
    public String getDateClotureExerciceComptable() {
        return this.dateClotureExerciceComptable;
    }

    /**
     * Getter de l'attribut denomination.
     * 
     * @return la valeur de denomination
     */
    public String getDenomination() {
        return this.denomination;
    }

    /**
     * Getter de l'attribut objet.
     * 
     * @return la valeur de objet
     */
    public String getObjet() {
        return this.objet;
    }

    /**
     * Getter de l'attribut objetPortantSurEnsembleActivites.
     * 
     * @return la valeur de objetPortantSurEnsembleActivites
     */
    public String getObjetPortantSurEnsembleActivites() {
        return this.objetPortantSurEnsembleActivites;
    }

    /**
     * Getter de l'attribut precedentEIRLDenomination.
     * 
     * @return la valeur de precedentEIRLDenomination
     */
    public String getPrecedentEIRLDenomination() {
        return this.precedentEIRLDenomination;
    }

    /**
     * Getter de l'attribut precedentEIRLLieuImmatriculation.
     * 
     * @return la valeur de precedentEIRLLieuImmatriculation
     */
    public String getPrecedentEIRLLieuImmatriculation() {
        return this.precedentEIRLLieuImmatriculation;
    }

    /**
     * Getter de l'attribut precedentEIRLRegistre.
     * 
     * @return la valeur de precedentEIRLRegistre
     */
    public String getPrecedentEIRLRegistre() {
        return this.precedentEIRLRegistre;
    }

    /**
     * Getter de l'attribut precedentEIRLSIREN.
     * 
     * @return la valeur de precedentEIRLSIREN
     */
    public String getPrecedentEIRLSIREN() {
        return this.precedentEIRLSIREN;
    }

    /**
     * /** Getter de l'attribut registreDepot.
     * 
     * @return la valeur de registreDepot
     */
    public String getRegistreDepot() {
        return this.registreDepot;
    }

    /**
     * Getter de l'attribut statutEIRL.
     * 
     * @return la valeur de statutEIRL
     */
    public String getStatutEIRL() {
        return this.statutEIRL;
    }

    /**
     * Setter de l'attribut activitesAccessoiresBICBNC.
     * 
     * @param activitesAccessoiresBICBNC
     *            la nouvelle valeur de activitesAccessoiresBICBNC
     */
    public void setActivitesAccessoiresBICBNC(String activitesAccessoiresBICBNC) {
        this.activitesAccessoiresBICBNC = activitesAccessoiresBICBNC;
    }

    /**
     * Setter de l'attribut dateClotureExerciceComptable.
     * 
     * @param dateClotureExerciceComptable
     *            la nouvelle valeur de dateClotureExerciceComptable
     */
    public void setDateClotureExerciceComptable(String dateClotureExerciceComptable) {
        this.dateClotureExerciceComptable = dateClotureExerciceComptable;
    }

    /**
     * Setter de l'attribut denomination.
     * 
     * @param denomination
     *            la nouvelle valeur de denomination
     */
    public void setDenomination(String denomination) {
        this.denomination = denomination;
    }

    /**
     * Setter de l'attribut objet.
     * 
     * @param objet
     *            la nouvelle valeur de objet
     */
    public void setObjet(String objet) {
        this.objet = objet;
    }

    /**
     * Setter de l'attribut objetPortantSurEnsembleActivites.
     * 
     * @param objetPortantSurEnsembleActivites
     *            la nouvelle valeur de objetPortantSurEnsembleActivites
     */
    public void setObjetPortantSurEnsembleActivites(String objetPortantSurEnsembleActivites) {
        this.objetPortantSurEnsembleActivites = objetPortantSurEnsembleActivites;
    }

    /**
     * Setter de l'attribut precedentEIRLDenomination.
     * 
     * @param precedentEIRLDenomination
     *            la nouvelle valeur de precedentEIRLDenomination
     */
    public void setPrecedentEIRLDenomination(String precedentEIRLDenomination) {
        this.precedentEIRLDenomination = precedentEIRLDenomination;
    }

    /**
     * Setter de l'attribut precedentEIRLLieuImmatriculation.
     * 
     * @param precedentEIRLLieuImmatriculation
     *            la nouvelle valeur de precedentEIRLLieuImmatriculation
     */
    public void setPrecedentEIRLLieuImmatriculation(String precedentEIRLLieuImmatriculation) {
        this.precedentEIRLLieuImmatriculation = precedentEIRLLieuImmatriculation;
    }

    /**
     * Setter de l'attribut precedentEIRLRegistre.
     * 
     * @param precedentEIRLRegistre
     *            la nouvelle valeur de precedentEIRLRegistre
     */
    public void setPrecedentEIRLRegistre(String precedentEIRLRegistre) {
        this.precedentEIRLRegistre = precedentEIRLRegistre;
    }

    /**
     * Setter de l'attribut precedentEIRLSIREN.
     * 
     * @param precedentEIRLSIREN
     *            la nouvelle valeur de precedentEIRLSIREN
     */
    public void setPrecedentEIRLSIREN(String precedentEIRLSIREN) {
        this.precedentEIRLSIREN = precedentEIRLSIREN;
    }

    /**
     * Setter de l'attribut registreDepot.
     * 
     * @param registreDepot
     *            la nouvelle valeur de registreDepot
     */
    public void setRegistreDepot(String registreDepot) {
        this.registreDepot = registreDepot;
    }

    /**
     * Setter de l'attribut statutEIRL.
     * 
     * @param statutEIRL
     *            la nouvelle valeur de statutEIRL
     */
    public void setStatutEIRL(String statutEIRL) {
        this.statutEIRL = statutEIRL;
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    /**
     * Get le definition patrimoine.
     *
     * @return the definitionPatrimoine
     */
    public List<String> getDefinitionPatrimoine() {
        return definitionPatrimoine;
    }

    /**
     * Set le definition patrimoine.
     *
     * @param definitionPatrimoine
     *            the definitionPatrimoine to set
     */
    public void setDefinitionPatrimoine(List<String> definitionPatrimoine) {
        this.definitionPatrimoine = definitionPatrimoine;
    }

}
