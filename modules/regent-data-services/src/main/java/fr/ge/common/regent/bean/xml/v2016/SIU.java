package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface SIU.
 */
@ResourceXPath("/SIU")
public interface SIU {

  /**
   * Get le u10.
   *
   * @return le u10
   */
  @FieldXPath("U10")
  String getU10();

  /**
   * Set le u10.
   *
   * @param U10
   *          le nouveau u10
   */
  void setU10(String U10);

  /**
   * Get le u15.
   *
   * @return le u15
   */
  @FieldXPath("U15")
  String getU15();

  /**
   * Set le u15.
   *
   * @param U15
   *          le nouveau u15
   */
  void setU15(String U15);

  /**
   * Get le u16.
   *
   * @return le u16
   */
  @FieldXPath("U16")
  String getU16();

  /**
   * Set le u16.
   *
   * @param U16
   *          le nouveau u16
   */
  void setU16(String U16);

  /**
   * Get le u11.
   *
   * @return le u11
   */
  @FieldXPath("U11")
  IU11[] getU11();

  /**
   * Ajoute le to u11.
   *
   * @return le i u11
   */
  IU11 addToU11();

  /**
   * Set le u11.
   *
   * @param U11
   *          le nouveau u11
   */
  void setU11(IU11[] U11);

  /**
   * Get le U14.
   *
   * @return le U14
   */
  @FieldXPath("U14")
  IU14[] getU14();

  /**
   * Ajoute le to U14.
   *
   * @return le i U14
   */
  IU14 addToU14();

  /**
   * Set le U14.
   *
   * @param U14
   *          le nouveau U14
   */
  void setU14(IU14[] U14);

}
