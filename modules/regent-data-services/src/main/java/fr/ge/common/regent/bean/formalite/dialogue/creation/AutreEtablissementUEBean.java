package fr.ge.common.regent.bean.formalite.dialogue.creation;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import fr.ge.common.regent.bean.formalite.FormaliteVueBean;

/**
 * Le Class AutreEtablissementUEBean.
 */
public class AutreEtablissementUEBean extends FormaliteVueBean implements IFormaliteVue {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -2093870625939387776L;

    /** Le id technique. */
    private String idTechnique;

    /** Le activites. */
    private String activites;

    /** Le adresse. */
    private AdresseBean adresse = new AdresseBean();

    /** Le lieu immatriculation. */
    private String lieuImmatriculation;

    /** Le numero immatriculation. */
    private String numeroImmatriculation;

    /**
     * Getter de l'attribut activites.
     * 
     * @return la valeur de activites
     */
    public String getActivites() {
        return this.activites;
    }

    /**
     * Getter de l'attribut adresse.
     * 
     * @return la valeur de adresse
     */
    public AdresseBean getAdresse() {
        return this.adresse;
    }

    /**
     * Getter de l'attribut lieuImmatriculation.
     * 
     * @return la valeur de lieuImmatriculation
     */
    public String getLieuImmatriculation() {
        return this.lieuImmatriculation;
    }

    /**
     * Getter de l'attribut numeroImmatriculation.
     * 
     * @return la valeur de numeroImmatriculation
     */
    public String getNumeroImmatriculation() {
        return this.numeroImmatriculation;
    }

    /**
     * Setter de l'attribut activites.
     * 
     * @param activites
     *            la nouvelle valeur de activites
     */
    public void setActivites(String activites) {
        this.activites = activites;
    }

    /**
     * Setter de l'attribut adresse.
     * 
     * @param adresse
     *            la nouvelle valeur de adresse
     */
    public void setAdresse(AdresseBean adresse) {
        this.adresse = adresse;
    }

    /**
     * Setter de l'attribut lieuImmatriculation.
     * 
     * @param lieuImmatriculation
     *            la nouvelle valeur de lieuImmatriculation
     */
    public void setLieuImmatriculation(String lieuImmatriculation) {
        this.lieuImmatriculation = lieuImmatriculation;
    }

    /**
     * Setter de l'attribut numeroImmatriculation.
     * 
     * @param numeroImmatriculation
     *            la nouvelle valeur de numeroImmatriculation
     */
    public void setNumeroImmatriculation(String numeroImmatriculation) {
        this.numeroImmatriculation = numeroImmatriculation;
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    /**
     * Get le id technique.
     *
     * @return the idTechnique
     */
    public String getIdTechnique() {
        return idTechnique;
    }

    /**
     * Set le id technique.
     *
     * @param idTechnique
     *            the idTechnique to set
     */
    public void setIdTechnique(String idTechnique) {
        this.idTechnique = idTechnique;
    }

}
