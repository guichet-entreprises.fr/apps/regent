package fr.ge.common.regent.bean.xml.formalite.dialogue.regularisation;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

import fr.ge.common.regent.bean.xml.formalite.IFormalite;
import fr.ge.common.regent.bean.xml.transverse.IAdresse;

/**
 * La classe XML Field de AutreEtablissementUE.
 * 
 * @author roveda
 *
 */
@ResourceXPath("/formalite")
public interface IAutreEtablissementUE extends IFormalite {

    /** La constante MODEL_VERSION. */
    static final int MODEL_VERSION = 1;

    /**
     * Get le activites.
     *
     * @return le activites
     */
    @FieldXPath(value = "activites")
    String getActivites();

    /**
     * Set le activites.
     *
     * @param activites
     *            le nouveau activites
     */
    void setActivites(String activites);

    /**
     * Get le lieu immatriculation.
     *
     * @return le lieu immatriculation
     */
    @FieldXPath(value = "lieuImmatriculation")
    String getLieuImmatriculation();

    /**
     * Set le lieu immatriculation.
     *
     * @param lieuImmatriculation
     *            le nouveau lieu immatriculation
     */
    void setLieuImmatriculation(String lieuImmatriculation);

    /**
     * Get le numero immatriculation.
     *
     * @return le numero immatriculation
     */
    @FieldXPath(value = "numeroImmatriculation")
    String getNumeroImmatriculation();

    /**
     * Set le numero immatriculation.
     *
     * @param numeroImmatriculation
     *            le nouveau numero immatriculation
     */
    void setNumeroImmatriculation(String numeroImmatriculation);

    /**
     * Get le adresse.
     *
     * @return le adresse
     */
    @FieldXPath(value = "adresse")
    IAdresse getAdresse();

    /**
     * New adresse.
     *
     * @return le i adresse
     */
    IAdresse newAdresse();

    /**
     * Set le adresse.
     *
     * @param adresse
     *            le nouveau adresse
     */
    void setAdresse(IAdresse adresse);

}
