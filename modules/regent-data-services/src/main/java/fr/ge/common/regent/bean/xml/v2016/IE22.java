package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IE22.
 */
@ResourceXPath("/E22")
public interface IE22 {

  /**
   * Get le e221.
   *
   * @return le e221
   */
  @FieldXPath("E22.1")
  String getE221();

  /**
   * Set le e221.
   *
   * @param E221
   *          le nouveau e221
   */
  void setE221(String E221);

  /**
   * Get le e222.
   *
   * @return le e222
   */
  @FieldXPath("E22.2")
  String getE222();

  /**
   * Set le e222.
   *
   * @param E222
   *          le nouveau e222
   */
  void setE222(String E222);

}
