package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;
import org.xmlfield.core.types.XmlString;

/**
 * Le Interface IE93.
 */
@ResourceXPath("/E93")
public interface IE93 {

  /**
   * Get le e932.
   *
   * @return le e932
   */
  @FieldXPath("E93.2")
  String getE932();

  /**
   * Set le e932.
   *
   * @param E932
   *          le nouveau e932
   */
  void setE932(String E932);

  /**
   * Get le e934.
   *
   * @return le e934
   */
  @FieldXPath("E93.4")
  String getE934();

  /**
   * Set le e934.
   *
   * @param E934
   *          le nouveau e934
   */
  void setE934(String E934);

  /**
   * Get le e933.
   *
   * @return le e933
   */
  @FieldXPath("E93.3")
  XmlString[] getE933();

  /**
   * Ajoute le to e933.
   *
   * @return le xml string
   */
  XmlString addToE933();

  /**
   * Set le c20.
   *
   * @param E933
   *          le nouveau c20
   */
  void setC20(XmlString[] E933);

}
