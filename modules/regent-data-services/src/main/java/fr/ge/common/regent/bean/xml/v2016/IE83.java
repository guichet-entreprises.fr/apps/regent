package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Interface IP95.
 */
@ResourceXPath("/E83")
public interface IE83 {

  /**
   * Get le E83.1.
   *
   * @return le E83.1
   */
  @FieldXPath("E83.1")
  String getE831();

  /**
   * Set le E83.1.
   *
   * @param E831
   *          .1 .1 le nouveau E83.1
   */
  void setE831(String E831);

  /**
   * Get le E83.2.
   *
   * @return le E83.2
   */
  @FieldXPath("E83.2")
  String getE832();

  /**
   * Set le E83.2.
   *
   * @param E832
   *          .2 .1 .1 le nouveau E83.2
   */
  void setE832(String E832);
}
