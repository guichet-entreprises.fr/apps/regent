package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IE61.
 */
@ResourceXPath("/E61")
public interface IE61 {

  /**
   * Get le e611.
   *
   * @return le e611
   */
  @FieldXPath("E61.1")
  String getE611();

  /**
   * Set le e611.
   *
   * @param E611
   *          le nouveau e611
   */
  void setE611(String E611);

  /**
   * Get le e612.
   *
   * @return le e612
   */
  @FieldXPath("E61.2")
  String getE612();

  /**
   * Set le e612.
   *
   * @param E612
   *          le nouveau e612
   */
  void setE612(String E612);

  /**
   * Get le e613.
   *
   * @return le e613
   */
  @FieldXPath("E61.3")
  String getE613();

  /**
   * Set le e613.
   *
   * @param E613
   *          le nouveau e613
   */
  void setE613(String E613);

}
