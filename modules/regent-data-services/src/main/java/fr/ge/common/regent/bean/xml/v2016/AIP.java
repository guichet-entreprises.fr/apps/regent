package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;
import org.xmlfield.core.types.XmlString;

/**
 * Le Interface AIP.
 */
@ResourceXPath("/AIP")
public interface AIP {

  /**
   * Get le p11.
   *
   * @return le p11
   */
  @FieldXPath("P11")
  String getP11();

  /**
   * Set le p11.
   *
   * @param P11
   *          le nouveau p11
   */
  void setP11(String P11);

  /**
   * Get le p12.
   *
   * @return le p12
   */
  @FieldXPath("P12")
  String getP12();

  /**
   * Set le p12.
   *
   * @param P12
   *          le nouveau p12
   */
  void setP12(String P12);

  /**
   * Get le p13.
   *
   * @return le p13
   */
  @FieldXPath("P13")
  String getP13();

  /**
   * Set le p13.
   *
   * @param P13
   *          le nouveau p13
   */
  void setP13(String P13);

  /**
   * Get le p14.
   *
   * @return le p14
   */
  @FieldXPath("P14")
  XmlString[] getP14();

  /**
   * Ajoute le to p14.
   *
   * @return le xml string
   */
  XmlString addToP14();

  /**
   * Set le p14.
   *
   * @param P14
   *          le nouveau p14
   */
  void setP14(XmlString[] P14);

  /**
   * Get le p15.
   *
   * @return le p15
   */
  @FieldXPath("P15")
  String getP15();

  /**
   * Set le p15.
   *
   * @param P15
   *          le nouveau p15
   */
  void setP15(String P15);

}
