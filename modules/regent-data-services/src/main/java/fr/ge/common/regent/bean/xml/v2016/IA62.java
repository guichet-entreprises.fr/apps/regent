package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Interface A62.
 */
@ResourceXPath("/A62")
public interface IA62 {

  /**
   * Get le A62.1.
   *
   * @return le A62.1
   */
  @FieldXPath("A62.1")
  String getA621();

  /**
   * Set le A621.
   *
   * @param A621
   *          le nouveau A621
   */
  void setA621(String A621);

  /**
   * Get le A622.
   *
   * @return le A622
   */
  @FieldXPath("A62.2")
  String getA622();

  /**
   * Set le A622.
   *
   * @param A622
   *          le nouveau A622
   */
  void setA622(String A622);

  /**
   * Get le A623.
   *
   * @return le A623
   */
  @FieldXPath("A62.3")
  String getA623();

  /**
   * Set le A623.
   *
   * @param A623
   *          le nouveau A623
   */
  void setA623(String A623);

  /**
   * Get le A624.
   *
   * @return le A624
   */
  @FieldXPath("A62.4")
  String getA624();

  /**
   * Set le A624.
   *
   * @param A624
   *          le nouveau A624
   */
  void setA624(String A624);

}
