/**
 * 
 */
package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Interface R06.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
@ResourceXPath("/R06")
public interface R06 {

  /**
   * Accesseur sur l'attribut {@link #r061}.
   *
   * @return r061
   */
  @FieldXPath("R06.1")
  String getR061();

  /**
   * Mutateur sur l'attribut {@link #r061}.
   *
   * @param R061
   *          la nouvelle valeur de l'attribut r061
   */
  void setR061(String R061);

  /**
   * Accesseur sur l'attribut {@link #r062}.
   *
   * @return r062
   */
  @FieldXPath("R06.2")
  String getR062();

  /**
   * Mutateur sur l'attribut {@link #r062}.
   *
   * @param R062
   *          la nouvelle valeur de l'attribut r062
   */
  void setR062(String R062);

  /**
   * Accesseur sur l'attribut {@link #r063}.
   *
   * @return r063
   */
  @FieldXPath("R06.3")
  String getR063();

  /**
   * Mutateur sur l'attribut {@link #r063}.
   *
   * @param R063
   *          la nouvelle valeur de l'attribut r063
   */
  void setR063(String R063);

  /**
   * Accesseur sur l'attribut {@link #r064}.
   *
   * @return r057
   */
  @FieldXPath("R06.4")
  String getR064();

  /**
   * Mutateur sur l'attribut {@link #r064}.
   *
   * @param R064
   *          la nouvelle valeur de l'attribut r064
   */
  void setR064(String R064);

}
