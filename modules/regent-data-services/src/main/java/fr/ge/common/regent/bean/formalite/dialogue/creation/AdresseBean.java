package fr.ge.common.regent.bean.formalite.dialogue.creation;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Le Class AdresseBean.
 */
public class AdresseBean implements IFormaliteVue {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 6358816908941097214L;

    /** Le code pays. */
    private String codePays;

    /** Le code postal commune. */
    private PostalCommuneBean codePostalCommune = new PostalCommuneBean();

    /** Le complement adresse. */
    private String complementAdresse;

    /** Le distribution speciale. */
    private String distributionSpeciale;

    /** Le indice repetition. */
    private String indiceRepetition;

    /** Le nom voie. */
    private String nomVoie;

    /** Le numero voie. */
    private String numeroVoie;

    /** Le type voie. */
    private String typeVoie;

    /** Le ville. */
    private String ville;

    /**
     * Getter de l'attribut codePays.
     * 
     * @return la valeur de codePays
     */
    public String getCodePays() {
        return this.codePays;
    }

    /**
     * Getter de l'attribut codePostalCommune.
     * 
     * @return la valeur de codePostalCommune
     */
    public PostalCommuneBean getCodePostalCommune() {
        return this.codePostalCommune;
    }

    /**
     * Getter de l'attribut complementAdresse.
     * 
     * @return la valeur de complementAdresse
     */
    public String getComplementAdresse() {
        return this.complementAdresse;
    }

    /**
     * Getter de l'attribut distributionSpeciale.
     * 
     * @return la valeur de distributionSpeciale
     */
    public String getDistributionSpeciale() {
        return this.distributionSpeciale;
    }

    /**
     * Getter de l'attribut indiceRepetition.
     * 
     * @return la valeur de indiceRepetition
     */
    public String getIndiceRepetition() {
        return this.indiceRepetition;
    }

    /**
     * Getter de l'attribut nomVoie.
     * 
     * @return la valeur de nomVoie
     */
    public String getNomVoie() {
        return this.nomVoie;
    }

    /**
     * Getter de l'attribut numeroVoie.
     * 
     * @return la valeur de numeroVoie
     */
    public String getNumeroVoie() {
        return this.numeroVoie;
    }

    /**
     * Getter de l'attribut typeVoie.
     * 
     * @return la valeur de typeVoie
     */
    public String getTypeVoie() {
        return this.typeVoie;
    }

    /**
     * Getter de l'attribut ville.
     * 
     * @return la valeur de ville
     */
    public String getVille() {
        return this.ville;
    }

    /**
     * Setter de l'attribut codePays.
     * 
     * @param codePays
     *            la nouvelle valeur de codePays
     */
    public void setCodePays(String codePays) {
        this.codePays = codePays;
    }

    /**
     * Setter de l'attribut codePostalCommune.
     * 
     * @param codePostalCommune
     *            la nouvelle valeur de codePostalCommune
     */
    public void setCodePostalCommune(PostalCommuneBean codePostalCommune) {
        this.codePostalCommune = codePostalCommune;
    }

    /**
     * Setter de l'attribut complementAdresse.
     * 
     * @param complementAdresse
     *            la nouvelle valeur de complementAdresse
     */
    public void setComplementAdresse(String complementAdresse) {
        this.complementAdresse = complementAdresse;
    }

    /**
     * Setter de l'attribut distributionSpeciale.
     * 
     * @param distributionSpeciale
     *            la nouvelle valeur de distributionSpeciale
     */
    public void setDistributionSpeciale(String distributionSpeciale) {
        this.distributionSpeciale = distributionSpeciale;
    }

    /**
     * Setter de l'attribut indiceRepetition.
     * 
     * @param indiceRepetition
     *            la nouvelle valeur de indiceRepetition
     */
    public void setIndiceRepetition(String indiceRepetition) {
        this.indiceRepetition = indiceRepetition;
    }

    /**
     * Setter de l'attribut nomVoie.
     * 
     * @param nomVoie
     *            la nouvelle valeur de nomVoie
     */
    public void setNomVoie(String nomVoie) {
        this.nomVoie = nomVoie;
    }

    /**
     * Setter de l'attribut numeroVoie.
     * 
     * @param numeroVoie
     *            la nouvelle valeur de numeroVoie
     */
    public void setNumeroVoie(String numeroVoie) {
        this.numeroVoie = numeroVoie;
    }

    /**
     * Setter de l'attribut typeVoie.
     * 
     * @param typeVoie
     *            la nouvelle valeur de typeVoie
     */
    public void setTypeVoie(String typeVoie) {
        this.typeVoie = typeVoie;
    }

    /**
     * Setter de l'attribut ville.
     * 
     * @param ville
     *            la nouvelle valeur de ville
     */
    public void setVille(String ville) {
        this.ville = ville;
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

}
