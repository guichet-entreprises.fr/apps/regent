package fr.ge.common.regent.bean.formalite.profil.creation;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import fr.ge.common.regent.bean.formalite.dialogue.creation.PostalCommuneBean;
import fr.ge.common.regent.bean.formalite.profil.AbstractProfilEntreprise;

/**
 * Le Class ProfilBean.
 *
 * @author bsadil
 */
public class ProfilCreationBean extends AbstractProfilEntreprise {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 5272085655141784071L;

    /** Le activite des creation. */
    private String activiteDesCreation;

    /** Le activite dpt siege. */
    private String activiteDptSiege;

    /** Le activite lps. */
    private String activiteLPS;

    /** Le activite non salarie. */
    private String activiteNonSalarie;

    /** Le activite principale code activite. */
    private String activitePrincipaleCodeActivite;

    /** Le activite principale domaine. */
    private String activitePrincipaleDomaine;

    /** Le activite principale domaine1. */
    private String activitePrincipaleDomaine1;

    /** Le activite principale domaine2. */
    private String activitePrincipaleDomaine2;

    /** Le activite principale libelle activite. */
    private String activitePrincipaleLibelleActivite;

    /** Le activite principale secteur. */
    private String activitePrincipaleSecteur;

    /** Le activite principale secteur1. */
    private String activitePrincipaleSecteur1;

    /** Le activite principale secteur2. */
    private String activitePrincipaleSecteur2;

    /** Le activite principale secteur3. */
    private String activitePrincipaleSecteur3;

    /** Le activite secondaire code activite. */
    private String activiteSecondaireCodeActivite;

    /** Le activite secondaire domaine. */
    private String activiteSecondaireDomaine;

    /** Le activite secondaire domaine1. */
    private String activiteSecondaireDomaine1;

    /** Le activite secondaire domaine2. */
    private String activiteSecondaireDomaine2;

    /** Le activite secondaire libelle activite. */
    private String activiteSecondaireLibelleActivite;

    /** Le activite secondaire secteur. */
    private String activiteSecondaireSecteur;

    /** Le activite secondaire secteur1. */
    private String activiteSecondaireSecteur1;

    /** Le activite secondaire secteur2. */
    private String activiteSecondaireSecteur2;

    /** Le activite secondaire secteur3. */
    private String activiteSecondaireSecteur3;

    /** Le activite siege. */
    private String activiteSiege;

    /** Le ambulant. */
    private String ambulant;

    /** Le cfe. */
    protected CfeBean cfe = new CfeBean();

    /** Le code departement. */
    private String codeDepartement;

    /** Le existe activite secondaire. */
    private String existeActiviteSecondaire;

    /** Le nom dossier. */
    private String nomDossier;

    /** Le non connaissance secteur principal. */
    private String nonConnaissanceSecteurPrincipal;

    /** Le non connaissance secteur secondaire. */
    private String nonConnaissanceSecteurSecondaire;

    /** Le numero formalite. */
    private String numeroFormalite;

    /** Le option cmacci. */
    private String optionCMACCI;

    /** Le option cmacc i1. */
    private String optionCMACCI1;

    /** Le option cmacc i2. */
    private String optionCMACCI2;

    /** Le postal commune. */
    private PostalCommuneBean postalCommune = new PostalCommuneBean();

    /** Le secteur cfe. */
    private String secteurCfe;

    /** Le type cfe. */
    private String typeCfe;

    /** Le reseau cfe. */
    private String reseauCFE;

    /** Le type personne. */
    private String typePersonne;

    /** Le recherche activite oui non. */
    private String rechercheActiviteOuiNon;

    /** Le recherche activite oui non libelle. */
    private String rechercheActiviteOuiNonLibelle;

    /** Le recherche activite. */
    private String rechercheActivite;

    /** Le recherche activite secondaire oui non. */
    private String rechercheActiviteSecondaireOuiNon;

    /** Le recherche activite secondaire. */
    private String rechercheActiviteSecondaire;

    /** Le code naf principal. */
    private String codeNafPrincipal;

    /** Le code naf secondaire. */
    private String codeNafSecondaire;

    /** Le aut debit de boisson. */
    private String autDebitDeBoisson;

    /** Le micro social oui non. */
    private String microSocialOuiNon;

    /** Le eirl oui non. */
    private String eirlOuiNon;

    /** Le texte micro social. */
    private String texteMicroSocial;

    /** Le activite principale code activite non micro. */
    private String activitePrincipaleCodeActiviteNonMicro;

    /** Le activite principale code activite micro. */
    private String activitePrincipaleCodeActiviteMicro;

    /** Le existe activite secondaire non micro. */
    private String existeActiviteSecondaireNonMicro;

    /** Le existe activite secondaire micro. */
    private String existeActiviteSecondaireMicro;

    /** Le activite secondaire code activite non micro. */
    private String activiteSecondaireCodeActiviteNonMicro;

    /** Le activite secondaire code activite micro. */
    private String activiteSecondaireCodeActiviteMicro;

    /** Le activite principale secteur non micro. */
    private String activitePrincipaleSecteurNonMicro;

    /** Le activite principale secteur micro. */
    private String activitePrincipaleSecteurMicro;

    /** Le activite secondaire secteur non micro. */
    private String activiteSecondaireSecteurNonMicro;

    /** Le activite secondaire secteur micro. */
    private String activiteSecondaireSecteurMicro;

    /** Le type personne non micro. */
    private String typePersonneNonMicro;

    /** Le type personne micro. */
    private String typePersonneMicro;

    /**
     * Set le aut debit de boisson.
     *
     * @param autDebitDeBoisson
     *            the autDebitDeBoisson to set
     */
    public void setAutDebitDeBoisson(String autDebitDeBoisson) {
        this.autDebitDeBoisson = autDebitDeBoisson;
    }

    /**
     * Get le aut debit de boisson.
     *
     * @return the autDebitDeBoisson
     */
    public String getAutDebitDeBoisson() {
        return this.autDebitDeBoisson;
    }

    /**
     * Get le code naf principal.
     *
     * @return the codeNafPrincipal
     */
    public String getCodeNafPrincipal() {
        return this.codeNafPrincipal;
    }

    /**
     * Set le code naf principal.
     *
     * @param codeNafPrincipal
     *            the codeNafPrincipal to set
     */
    public void setCodeNafPrincipal(String codeNafPrincipal) {
        this.codeNafPrincipal = codeNafPrincipal;
    }

    /**
     * Get le code naf secondaire.
     *
     * @return the codeNafSecondaire
     */
    public String getCodeNafSecondaire() {
        return this.codeNafSecondaire;
    }

    /**
     * Set le code naf secondaire.
     *
     * @param codeNafSecondaire
     *            the codeNafSecondaire to set
     */
    public void setCodeNafSecondaire(String codeNafSecondaire) {
        this.codeNafSecondaire = codeNafSecondaire;
    }

    /**
     * Getter de l'attribut activiteDesCreation.
     * 
     * @return la valeur de activiteDesCreation
     */
    public String getActiviteDesCreation() {
        return this.activiteDesCreation;
    }

    /**
     * Getter de l'attribut activiteDptSiege.
     * 
     * @return la valeur de activiteDptSiege
     */
    public String getActiviteDptSiege() {
        return this.activiteDptSiege;
    }

    /**
     * getActiviteLPS.
     * 
     * @return activiteLPS
     */
    public String getActiviteLPS() {
        return this.activiteLPS;
    }

    /**
     * Getter de l'attribut activiteNonSalarie.
     * 
     * @return la valeur de activiteNonSalarie
     */
    public String getActiviteNonSalarie() {
        return this.activiteNonSalarie;
    }

    /**
     * Getter de l'attribut activitePrincipaleCodeActivite.
     * 
     * @return la valeur de activitePrincipaleCodeActivite
     */
    public String getActivitePrincipaleCodeActivite() {
        return this.activitePrincipaleCodeActivite;
    }

    /**
     * Get le activite principale domaine.
     *
     * @return the activitePrincipaleDomaine
     */
    public String getActivitePrincipaleDomaine() {
        return this.activitePrincipaleDomaine;
    }

    /**
     * Getter de l'attribut activitePrincipaleDomaine1.
     * 
     * @return la valeur de activitePrincipaleDomaine1
     */
    public String getActivitePrincipaleDomaine1() {
        return this.activitePrincipaleDomaine1;
    }

    /**
     * Getter de l'attribut activitePrincipaleDomaine2.
     * 
     * @return la valeur de activitePrincipaleDomaine2
     */
    public String getActivitePrincipaleDomaine2() {
        return this.activitePrincipaleDomaine2;
    }

    /**
     * Getter de l'attribut activitePrincipaleLibelleActivite.
     * 
     * @return la valeur de activitePrincipaleLibelleActivite
     */
    public String getActivitePrincipaleLibelleActivite() {
        return this.activitePrincipaleLibelleActivite;
    }

    /**
     * Get le activite principale secteur.
     *
     * @return the activitePrincipaleSecteur
     */
    public String getActivitePrincipaleSecteur() {
        return this.activitePrincipaleSecteur;
    }

    /**
     * Getter de l'attribut activitePrincipaleSecteur1.
     * 
     * @return la valeur de activitePrincipaleSecteur1
     */
    public String getActivitePrincipaleSecteur1() {
        return this.activitePrincipaleSecteur1;
    }

    /**
     * Getter de l'attribut activitePrincipaleSecteur2.
     * 
     * @return la valeur de activitePrincipaleSecteur2
     */
    public String getActivitePrincipaleSecteur2() {
        return this.activitePrincipaleSecteur2;
    }

    /**
     * Getter de l'attribut activiteSecondaireCodeActivite.
     * 
     * @return la valeur de activiteSecondaireCodeActivite
     */
    public String getActiviteSecondaireCodeActivite() {
        return this.activiteSecondaireCodeActivite;
    }

    /**
     * Get le activite secondaire domaine.
     *
     * @return the activiteSecondaireDomaine
     */
    public String getActiviteSecondaireDomaine() {
        return this.activiteSecondaireDomaine;
    }

    /**
     * Getter de l'attribut activiteSecondaireDomaine1.
     * 
     * @return la valeur de activiteSecondaireDomaine1
     */
    public String getActiviteSecondaireDomaine1() {
        return this.activiteSecondaireDomaine1;
    }

    /**
     * Getter de l'attribut activiteSecondaireDomaine2.
     * 
     * @return la valeur de activiteSecondaireDomaine2
     */
    public String getActiviteSecondaireDomaine2() {
        return this.activiteSecondaireDomaine2;
    }

    /**
     * Getter de l'attribut activiteSecondaireLibelleActivite.
     * 
     * @return la valeur de activiteSecondaireLibelleActivite
     */
    public String getActiviteSecondaireLibelleActivite() {
        return this.activiteSecondaireLibelleActivite;
    }

    /**
     * Get le activite secondaire secteur.
     *
     * @return the activiteSecondaireSecteur
     */
    public String getActiviteSecondaireSecteur() {
        return this.activiteSecondaireSecteur;
    }

    /**
     * Getter de l'attribut activiteSecondaireSecteur1.
     * 
     * @return la valeur de activiteSecondaireSecteur1
     */
    public String getActiviteSecondaireSecteur1() {
        return this.activiteSecondaireSecteur1;
    }

    /**
     * Getter de l'attribut activiteSecondaireSecteur2.
     * 
     * @return la valeur de activiteSecondaireSecteur2
     */
    public String getActiviteSecondaireSecteur2() {
        return this.activiteSecondaireSecteur2;
    }

    /**
     * Getter de l'attribut activiteSiege.
     * 
     * @return la valeur de activiteSiege
     */
    public String getActiviteSiege() {
        return this.activiteSiege;
    }

    /**
     * Getter de l'attribut ambulant.
     * 
     * @return la valeur de ambulant
     */
    public String getAmbulant() {
        return this.ambulant;
    }

    /**
     * Get le cfe.
     *
     * @return the cfe
     */
    public CfeBean getCfe() {
        return this.cfe;
    }

    /**
     * Get le code departement.
     *
     * @return the codeDepartement
     */
    public String getCodeDepartement() {
        return this.codeDepartement;
    }

    /**
     * Getter de l'attribut existeActiviteSecondaire.
     * 
     * @return la valeur de existeActiviteSecondaire
     */
    public String getExisteActiviteSecondaire() {
        return this.existeActiviteSecondaire;
    }

    /**
     * Getter de l'attribut nomDossier.
     * 
     * @return la valeur de nomDossier
     */
    public String getNomDossier() {
        return this.nomDossier;
    }

    /**
     * Getter de l'attribut nonConnaissanceSecteurPrincipal.
     * 
     * @return la valeur de nonConnaissanceSecteurPrincipal
     */
    public String getNonConnaissanceSecteurPrincipal() {
        return this.nonConnaissanceSecteurPrincipal;
    }

    /**
     * Getter de l'attribut nonConnaissanceSecteurSecondaire.
     * 
     * @return la valeur de nonConnaissanceSecteurSecondaire
     */
    public String getNonConnaissanceSecteurSecondaire() {
        return this.nonConnaissanceSecteurSecondaire;
    }

    /**
     * Getter de l'attribut numeroFormalite.
     * 
     * @return la valeur de numeroFormalite
     */
    public String getNumeroFormalite() {
        return this.numeroFormalite;
    }

    /**
     * Getter de l'attribut optionCMACCI.
     * 
     * @return la valeur de optionCMACCI
     */
    public String getOptionCMACCI() {
        return this.optionCMACCI;
    }

    /**
     * getOptionCMACCI1.
     * 
     * @return optionCMACCI1
     */
    public String getOptionCMACCI1() {
        return this.optionCMACCI1;
    }

    /**
     * getOptionCMACCI2.
     * 
     * @return optionCMACCI2
     */
    public String getOptionCMACCI2() {
        return this.optionCMACCI2;
    }

    /**
     * Getter de l'attribut postalcommune.
     * 
     * @return la valeur de postalcommune
     */
    public PostalCommuneBean getPostalCommune() {
        return this.postalCommune;
    }

    /**
     * Get le secteur cfe.
     *
     * @return the secteurCfe
     */
    public String getSecteurCfe() {
        return this.secteurCfe;
    }

    /**
     * Get le type cfe.
     *
     * @return the typeCfe
     */
    public String getTypeCfe() {
        return this.typeCfe;
    }

    /**
     * Get le reseau cfe.
     *
     * @return the reseauCFE
     */
    public String getReseauCFE() {
        return this.reseauCFE;
    }

    /**
     * Getter de l'attribut typePersonne.
     * 
     * @return la valeur de typePersonne
     */
    public String getTypePersonne() {
        return this.typePersonne;
    }

    /**
     * Setter de l'attribut activiteDesCreation.
     * 
     * @param activiteDesCreation
     *            la nouvelle valeur de activiteDesCreation
     */
    public void setActiviteDesCreation(String activiteDesCreation) {
        this.activiteDesCreation = activiteDesCreation;
    }

    /**
     * Setter de l'attribut activiteDptSiege.
     * 
     * @param activiteDptSiege
     *            la nouvelle valeur de activiteDptSiege
     */
    public void setActiviteDptSiege(String activiteDptSiege) {
        this.activiteDptSiege = activiteDptSiege;
    }

    /**
     * setActiviteLPS.
     * 
     * @param activiteLPS
     *            activiteLPS
     */
    public void setActiviteLPS(String activiteLPS) {
        this.activiteLPS = activiteLPS;
    }

    /**
     * Setter de l'attribut activiteNonSalarie.
     * 
     * @param activiteNonSalarie
     *            la nouvelle valeur de activiteNonSalarie
     */
    public void setActiviteNonSalarie(String activiteNonSalarie) {
        this.activiteNonSalarie = activiteNonSalarie;
    }

    /**
     * Setter de l'attribut activitePrincipaleCodeActivite.
     * 
     * @param activitePrincipaleCodeActivite
     *            la nouvelle valeur de activitePrincipaleCodeActivite
     */
    public void setActivitePrincipaleCodeActivite(String activitePrincipaleCodeActivite) {
        this.activitePrincipaleCodeActivite = activitePrincipaleCodeActivite;
    }

    /**
     * Set le activite principale domaine.
     *
     * @param activitePrincipaleDomaine
     *            the activitePrincipaleDomaine to set
     */
    public void setActivitePrincipaleDomaine(String activitePrincipaleDomaine) {
        this.activitePrincipaleDomaine = activitePrincipaleDomaine;
    }

    /**
     * Setter de l'attribut activitePrincipaleDomaine1.
     * 
     * @param activitePrincipaleDomaine1
     *            la nouvelle valeur de activitePrincipaleDomaine1
     */
    public void setActivitePrincipaleDomaine1(String activitePrincipaleDomaine1) {
        this.activitePrincipaleDomaine1 = activitePrincipaleDomaine1;
    }

    /**
     * Setter de l'attribut activitePrincipaleDomaine2.
     * 
     * @param activitePrincipaleDomaine2
     *            la nouvelle valeur de activitePrincipaleDomaine2
     */
    public void setActivitePrincipaleDomaine2(String activitePrincipaleDomaine2) {
        this.activitePrincipaleDomaine2 = activitePrincipaleDomaine2;
    }

    /**
     * Setter de l'attribut activitePrincipaleLibelleActivite.
     * 
     * @param activitePrincipaleLibelleActivite
     *            la nouvelle valeur de activitePrincipaleLibelleActivite
     */
    public void setActivitePrincipaleLibelleActivite(String activitePrincipaleLibelleActivite) {
        this.activitePrincipaleLibelleActivite = activitePrincipaleLibelleActivite;
    }

    /**
     * Set le activite principale secteur.
     *
     * @param activitePrincipaleSecteur
     *            the activitePrincipaleSecteur to set
     */
    public void setActivitePrincipaleSecteur(String activitePrincipaleSecteur) {
        this.activitePrincipaleSecteur = activitePrincipaleSecteur;
    }

    /**
     * Setter de l'attribut activitePrincipaleSecteur1.
     * 
     * @param activitePrincipaleSecteur1
     *            la nouvelle valeur de activitePrincipaleSecteur1
     */
    public void setActivitePrincipaleSecteur1(String activitePrincipaleSecteur1) {
        this.activitePrincipaleSecteur1 = activitePrincipaleSecteur1;
    }

    /**
     * Setter de l'attribut activitePrincipaleSecteur2.
     * 
     * @param activitePrincipaleSecteur2
     *            la nouvelle valeur de activitePrincipaleSecteur2
     */
    public void setActivitePrincipaleSecteur2(String activitePrincipaleSecteur2) {
        this.activitePrincipaleSecteur2 = activitePrincipaleSecteur2;
    }

    /**
     * Setter de l'attribut activiteSecondaireCodeActivite.
     * 
     * @param activiteSecondaireCodeActivite
     *            la nouvelle valeur de activiteSecondaireCodeActivite
     */
    public void setActiviteSecondaireCodeActivite(String activiteSecondaireCodeActivite) {
        this.activiteSecondaireCodeActivite = activiteSecondaireCodeActivite;
    }

    /**
     * Set le activite secondaire domaine.
     *
     * @param activiteSecondaireDomaine
     *            the activiteSecondaireDomaine to set
     */
    public void setActiviteSecondaireDomaine(String activiteSecondaireDomaine) {
        this.activiteSecondaireDomaine = activiteSecondaireDomaine;
    }

    /**
     * Setter de l'attribut activiteSecondaireDomaine1.
     * 
     * @param activiteSecondaireDomaine1
     *            la nouvelle valeur de activiteSecondaireDomaine1
     */
    public void setActiviteSecondaireDomaine1(String activiteSecondaireDomaine1) {
        this.activiteSecondaireDomaine1 = activiteSecondaireDomaine1;
    }

    /**
     * Setter de l'attribut activiteSecondaireDomaine2.
     * 
     * @param activiteSecondaireDomaine2
     *            la nouvelle valeur de activiteSecondaireDomaine2
     */
    public void setActiviteSecondaireDomaine2(String activiteSecondaireDomaine2) {
        this.activiteSecondaireDomaine2 = activiteSecondaireDomaine2;
    }

    /**
     * Setter de l'attribut activiteSecondaireLibelleActivite.
     * 
     * @param activiteSecondaireLibelleActivite
     *            la nouvelle valeur de activiteSecondaireLibelleActivite
     */
    public void setActiviteSecondaireLibelleActivite(String activiteSecondaireLibelleActivite) {
        this.activiteSecondaireLibelleActivite = activiteSecondaireLibelleActivite;
    }

    /**
     * Set le activite secondaire secteur.
     *
     * @param activiteSecondaireSecteur
     *            the activiteSecondaireSecteur to set
     */
    public void setActiviteSecondaireSecteur(String activiteSecondaireSecteur) {
        this.activiteSecondaireSecteur = activiteSecondaireSecteur;
    }

    /**
     * Setter de l'attribut activiteSecondaireSecteur1.
     * 
     * @param activiteSecondaireSecteur1
     *            la nouvelle valeur de activiteSecondaireSecteur1
     */
    public void setActiviteSecondaireSecteur1(String activiteSecondaireSecteur1) {
        this.activiteSecondaireSecteur1 = activiteSecondaireSecteur1;
    }

    /**
     * Setter de l'attribut activiteSecondaireSecteur2.
     * 
     * @param activiteSecondaireSecteur2
     *            la nouvelle valeur de activiteSecondaireSecteur2
     */
    public void setActiviteSecondaireSecteur2(String activiteSecondaireSecteur2) {
        this.activiteSecondaireSecteur2 = activiteSecondaireSecteur2;
    }

    /**
     * Setter de l'attribut activiteSiege.
     * 
     * @param activiteSiege
     *            la nouvelle valeur de activiteSiege
     */
    public void setActiviteSiege(String activiteSiege) {
        this.activiteSiege = activiteSiege;
    }

    /**
     * Setter de l'attribut ambulant.
     * 
     * @param ambulant
     *            la nouvelle valeur de ambulant
     */
    public void setAmbulant(String ambulant) {
        this.ambulant = ambulant;
    }

    /**
     * Set le cfe.
     *
     * @param cfe
     *            the cfe to set
     */
    public void setCfe(CfeBean cfe) {
        this.cfe = cfe;
    }

    /**
     * Set le code departement.
     *
     * @param codeDepartement
     *            the codeDepartement to set
     */
    public void setCodeDepartement(String codeDepartement) {
        this.codeDepartement = codeDepartement;
    }

    /**
     * Setter de l'attribut existeActiviteSecondaire.
     * 
     * @param existeActiviteSecondaire
     *            la nouvelle valeur de existeActiviteSecondaire
     */
    public void setExisteActiviteSecondaire(String existeActiviteSecondaire) {
        this.existeActiviteSecondaire = existeActiviteSecondaire;
    }

    /**
     * Setter de l'attribut nomDossier.
     * 
     * @param nomDossier
     *            la nouvelle valeur de nomDossier
     */
    public void setNomDossier(String nomDossier) {
        this.nomDossier = nomDossier;
    }

    /**
     * Setter de l'attribut nonConnaissanceSecteurPrincipal.
     * 
     * @param nonConnaissanceSecteurPrincipal
     *            la nouvelle valeur de nonConnaissanceSecteurPrincipal
     */
    public void setNonConnaissanceSecteurPrincipal(String nonConnaissanceSecteurPrincipal) {
        this.nonConnaissanceSecteurPrincipal = nonConnaissanceSecteurPrincipal;
    }

    /**
     * Setter de l'attribut nonConnaissanceSecteurSecondaire.
     * 
     * @param nonConnaissanceSecteurSecondaire
     *            la nouvelle valeur de nonConnaissanceSecteurSecondaire
     */
    public void setNonConnaissanceSecteurSecondaire(String nonConnaissanceSecteurSecondaire) {
        this.nonConnaissanceSecteurSecondaire = nonConnaissanceSecteurSecondaire;
    }

    /**
     * Setter de l'attribut numeroFormalite.
     * 
     * @param numeroFormalite
     *            la nouvelle valeur de numeroFormalite
     */
    public void setNumeroFormalite(String numeroFormalite) {
        this.numeroFormalite = numeroFormalite;
    }

    /**
     * Setter de l'attribut optionCMACCI.
     * 
     * @param optionCMACCI
     *            la nouvelle valeur de optionCMACCI
     */
    public void setOptionCMACCI(String optionCMACCI) {
        this.optionCMACCI = optionCMACCI;
    }

    /**
     * setOptionCMACCI1.
     * 
     * @param optionCMACCI1
     *            optionCMACCI1
     */
    public void setOptionCMACCI1(String optionCMACCI1) {
        this.optionCMACCI1 = optionCMACCI1;
    }

    /**
     * setOptionCMACCI2.
     * 
     * @param optionCMACCI2
     *            optionCMACCI2
     */
    public void setOptionCMACCI2(String optionCMACCI2) {
        this.optionCMACCI2 = optionCMACCI2;
    }

    /**
     * Setter de l'attribut postalcommune.
     * 
     * @param postalcommune
     *            la nouvelle valeur de postalcommune
     */
    public void setPostalCommune(PostalCommuneBean postalcommune) {
        this.postalCommune = postalcommune;
    }

    /**
     * Set le secteur cfe.
     *
     * @param secteurCfe
     *            the secteurCfe to set
     */
    public void setSecteurCfe(String secteurCfe) {
        this.secteurCfe = secteurCfe;
    }

    /**
     * Set le type cfe.
     *
     * @param typeCfe
     *            the typeCfe to set
     */
    public void setTypeCfe(String typeCfe) {
        this.typeCfe = typeCfe;
    }

    /**
     * Set le reseau cfe.
     *
     * @param reseauCFE
     *            the reseauCFE to set
     */
    public void setReseauCFE(String reseauCFE) {
        this.reseauCFE = reseauCFE;
    }

    /**
     * Setter de l'attribut typePersonne.
     * 
     * @param typePersonne
     *            la nouvelle valeur de typePersonne
     */
    public void setTypePersonne(String typePersonne) {
        this.typePersonne = typePersonne;
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    /**
     * Getter de l'attribut rechercheActiviteOuiNon.
     *
     * @return rechercheActiviteOuiNon
     */
    public String getRechercheActiviteOuiNon() {
        return this.rechercheActiviteOuiNon;
    }

    /**
     * Setter de l'attribut setRechercheActiviteOuiNon.
     * 
     * @param rechercheActiviteOuiNon
     *            la nouvelle valeur de rechercheActiviteOuiNon
     */
    public void setRechercheActiviteOuiNon(String rechercheActiviteOuiNon) {
        this.rechercheActiviteOuiNon = rechercheActiviteOuiNon;
    }

    /**
     * Getter de l'attribut rechercheActivite.
     *
     * @return rechercheActivite
     */
    public String getRechercheActivite() {
        return this.rechercheActivite;
    }

    /**
     * Setter de l'attribut rechercheActivite.
     * 
     * @param rechercheActivite
     *            la nouvelle valeur de rechercheActivite
     */
    public void setRechercheActivite(String rechercheActivite) {
        this.rechercheActivite = rechercheActivite;
    }

    /**
     * Getter de l'attribut rechercheActiviteSecondaireOuiNon.
     *
     * @return rechercheActiviteSecondaireOuiNon
     */
    public String getRechercheActiviteSecondaireOuiNon() {
        return this.rechercheActiviteSecondaireOuiNon;
    }

    /**
     * Setter de l'attribut setRechercheActivitSecondaireeOuiNon.
     *
     * @param rechercheActiviteSecondaireOuiNon
     *            le nouveau recherche activite secondaire oui non
     */
    public void setRechercheActiviteSecondaireOuiNon(String rechercheActiviteSecondaireOuiNon) {
        this.rechercheActiviteSecondaireOuiNon = rechercheActiviteSecondaireOuiNon;
    }

    /**
     * Getter de l'attribut rechercheActiviteSecondaire.
     *
     * @return rechercheActiviteSecondaire
     */
    public String getRechercheActiviteSecondaire() {
        return this.rechercheActiviteSecondaire;
    }

    /**
     * Setter de l'attribut rechercheActiviteSecondaire.
     *
     * @param rechercheActiviteSecondaire
     *            le nouveau recherche activite secondaire
     */
    public void setRechercheActiviteSecondaire(String rechercheActiviteSecondaire) {
        this.rechercheActiviteSecondaire = rechercheActiviteSecondaire;
    }

    /**
     * Get le activite principale secteur3.
     *
     * @return the activitePrincipaleSecteur3
     */
    public String getActivitePrincipaleSecteur3() {
        return this.activitePrincipaleSecteur3;
    }

    /**
     * Set le activite principale secteur3.
     *
     * @param activitePrincipaleSecteur3
     *            the activitePrincipaleSecteur3 to set
     */
    public void setActivitePrincipaleSecteur3(String activitePrincipaleSecteur3) {
        this.activitePrincipaleSecteur3 = activitePrincipaleSecteur3;
    }

    /**
     * Get le activite secondaire secteur3.
     *
     * @return the activiteSecondaireSecteur3
     */
    public String getActiviteSecondaireSecteur3() {
        return this.activiteSecondaireSecteur3;
    }

    /**
     * Set le activite secondaire secteur3.
     *
     * @param activiteSecondaireSecteur3
     *            the activiteSecondaireSecteur3 to set
     */
    public void setActiviteSecondaireSecteur3(String activiteSecondaireSecteur3) {
        this.activiteSecondaireSecteur3 = activiteSecondaireSecteur3;
    }

    /**
     * Get le recherche activite oui non libelle.
     *
     * @return the rechercheActiviteOuiNonLibelle
     */
    public String getRechercheActiviteOuiNonLibelle() {
        return this.rechercheActiviteOuiNonLibelle;
    }

    /**
     * Set le recherche activite oui non libelle.
     *
     * @param rechercheActiviteOuiNonLibelle
     *            the rechercheActiviteOuiNonLibelle to set
     */
    public void setRechercheActiviteOuiNonLibelle(String rechercheActiviteOuiNonLibelle) {
        this.rechercheActiviteOuiNonLibelle = rechercheActiviteOuiNonLibelle;
    }

    /**
     * Get le micro social oui non.
     *
     * @return le micro social oui non
     */
    public String getMicroSocialOuiNon() {
        return this.microSocialOuiNon;
    }

    /**
     * Set le micro social oui non.
     *
     * @param microSocialOuiNon
     *            le nouveau micro social oui non
     */
    public void setMicroSocialOuiNon(String microSocialOuiNon) {
        this.microSocialOuiNon = microSocialOuiNon;
    }

    /**
     * Get le eirl oui non.
     *
     * @return le eirl oui non
     */
    public String getEirlOuiNon() {
        return this.eirlOuiNon;
    }

    /**
     * Set le eirl oui non.
     *
     * @param eirlOuiNon
     *            le nouveau eirl oui non
     */
    public void setEirlOuiNon(String eirlOuiNon) {
        this.eirlOuiNon = eirlOuiNon;
    }

    /**
     * Get le texte micro social.
     *
     * @return le texte micro social
     */
    public String getTexteMicroSocial() {
        return this.texteMicroSocial;
    }

    /**
     * Set le texte micro social.
     *
     * @param texteMicroSocial
     *            le nouveau texte micro social
     */
    public void setTexteMicroSocial(String texteMicroSocial) {
        this.texteMicroSocial = texteMicroSocial;
    }

    /**
     * Get le activite principale code activite non micro.
     *
     * @return the activitePrincipaleCodeActiviteNonMicro
     */
    public String getActivitePrincipaleCodeActiviteNonMicro() {
        return this.activitePrincipaleCodeActiviteNonMicro;
    }

    /**
     * Set le activite principale code activite non micro.
     *
     * @param activitePrincipaleCodeActiviteNonMicro
     *            the activitePrincipaleCodeActiviteNonMicro to set
     */
    public void setActivitePrincipaleCodeActiviteNonMicro(String activitePrincipaleCodeActiviteNonMicro) {
        this.activitePrincipaleCodeActiviteNonMicro = activitePrincipaleCodeActiviteNonMicro;
    }

    /**
     * Get le activite principale code activite micro.
     *
     * @return the activitePrincipaleCodeActiviteMicro
     */
    public String getActivitePrincipaleCodeActiviteMicro() {
        return this.activitePrincipaleCodeActiviteMicro;
    }

    /**
     * Set le activite principale code activite micro.
     *
     * @param activitePrincipaleCodeActiviteMicro
     *            the activitePrincipaleCodeActiviteMicro to set
     */
    public void setActivitePrincipaleCodeActiviteMicro(String activitePrincipaleCodeActiviteMicro) {
        this.activitePrincipaleCodeActiviteMicro = activitePrincipaleCodeActiviteMicro;
    }

    /**
     * Get le existe activite secondaire non micro.
     *
     * @return the existeActiviteSecondaireNonMicro
     */
    public String getExisteActiviteSecondaireNonMicro() {
        return this.existeActiviteSecondaireNonMicro;
    }

    /**
     * Set le existe activite secondaire non micro.
     *
     * @param existeActiviteSecondaireNonMicro
     *            the existeActiviteSecondaireNonMicro to set
     */
    public void setExisteActiviteSecondaireNonMicro(String existeActiviteSecondaireNonMicro) {
        this.existeActiviteSecondaireNonMicro = existeActiviteSecondaireNonMicro;
    }

    /**
     * Get le existe activite secondaire micro.
     *
     * @return the existeActiviteSecondaireMicro
     */
    public String getExisteActiviteSecondaireMicro() {
        return this.existeActiviteSecondaireMicro;
    }

    /**
     * Set le existe activite secondaire micro.
     *
     * @param existeActiviteSecondaireMicro
     *            the existeActiviteSecondaireMicro to set
     */
    public void setExisteActiviteSecondaireMicro(String existeActiviteSecondaireMicro) {
        this.existeActiviteSecondaireMicro = existeActiviteSecondaireMicro;
    }

    /**
     * Get le activite secondaire code activite non micro.
     *
     * @return the activiteSecondaireCodeActiviteNonMicro
     */
    public String getActiviteSecondaireCodeActiviteNonMicro() {
        return this.activiteSecondaireCodeActiviteNonMicro;
    }

    /**
     * Set le activite secondaire code activite non micro.
     *
     * @param activiteSecondaireCodeActiviteNonMicro
     *            the activiteSecondaireCodeActiviteNonMicro to set
     */
    public void setActiviteSecondaireCodeActiviteNonMicro(String activiteSecondaireCodeActiviteNonMicro) {
        this.activiteSecondaireCodeActiviteNonMicro = activiteSecondaireCodeActiviteNonMicro;
    }

    /**
     * Get le activite secondaire code activite micro.
     *
     * @return the activiteSecondaireCodeActiviteMicro
     */
    public String getActiviteSecondaireCodeActiviteMicro() {
        return this.activiteSecondaireCodeActiviteMicro;
    }

    /**
     * Set le activite secondaire code activite micro.
     *
     * @param activiteSecondaireCodeActiviteMicro
     *            the activiteSecondaireCodeActiviteMicro to set
     */
    public void setActiviteSecondaireCodeActiviteMicro(String activiteSecondaireCodeActiviteMicro) {
        this.activiteSecondaireCodeActiviteMicro = activiteSecondaireCodeActiviteMicro;
    }

    /**
     * Get le activite principale secteur non micro.
     *
     * @return the activitePrincipaleSecteurNonMicro
     */
    public String getActivitePrincipaleSecteurNonMicro() {
        return this.activitePrincipaleSecteurNonMicro;
    }

    /**
     * Set le activite principale secteur non micro.
     *
     * @param activitePrincipaleSecteurNonMicro
     *            the activitePrincipaleSecteurNonMicro to set
     */
    public void setActivitePrincipaleSecteurNonMicro(String activitePrincipaleSecteurNonMicro) {
        this.activitePrincipaleSecteurNonMicro = activitePrincipaleSecteurNonMicro;
    }

    /**
     * Get le activite principale secteur micro.
     *
     * @return the activitePrincipaleSecteurMicro
     */
    public String getActivitePrincipaleSecteurMicro() {
        return this.activitePrincipaleSecteurMicro;
    }

    /**
     * Set le activite principale secteur micro.
     *
     * @param activitePrincipaleSecteurMicro
     *            the activitePrincipaleSecteurMicro to set
     */
    public void setActivitePrincipaleSecteurMicro(String activitePrincipaleSecteurMicro) {
        this.activitePrincipaleSecteurMicro = activitePrincipaleSecteurMicro;
    }

    /**
     * Get le activite secondaire secteur non micro.
     *
     * @return the activiteSecondaireSecteurNonMicro
     */
    public String getActiviteSecondaireSecteurNonMicro() {
        return this.activiteSecondaireSecteurNonMicro;
    }

    /**
     * Set le activite secondaire secteur non micro.
     *
     * @param activiteSecondaireSecteurNonMicro
     *            the activiteSecondaireSecteurNonMicro to set
     */
    public void setActiviteSecondaireSecteurNonMicro(String activiteSecondaireSecteurNonMicro) {
        this.activiteSecondaireSecteurNonMicro = activiteSecondaireSecteurNonMicro;
    }

    /**
     * Get le activite secondaire secteur micro.
     *
     * @return the activiteSecondaireSecteurMicro
     */
    public String getActiviteSecondaireSecteurMicro() {
        return this.activiteSecondaireSecteurMicro;
    }

    /**
     * Set le activite secondaire secteur micro.
     *
     * @param activiteSecondaireSecteurMicro
     *            the activiteSecondaireSecteurMicro to set
     */
    public void setActiviteSecondaireSecteurMicro(String activiteSecondaireSecteurMicro) {
        this.activiteSecondaireSecteurMicro = activiteSecondaireSecteurMicro;
    }

    /**
     * Get le type personne non micro.
     *
     * @return the typePersonneNonMicro
     */
    public String getTypePersonneNonMicro() {
        return this.typePersonneNonMicro;
    }

    /**
     * Set le type personne non micro.
     *
     * @param typePersonneNonMicro
     *            the typePersonneNonMicro to set
     */
    public void setTypePersonneNonMicro(String typePersonneNonMicro) {
        this.typePersonneNonMicro = typePersonneNonMicro;
    }

    /**
     * Get le type personne micro.
     *
     * @return the typePersonneMicro
     */
    public String getTypePersonneMicro() {
        return this.typePersonneMicro;
    }

    /**
     * Set le type personne micro.
     *
     * @param typePersonneMicro
     *            the typePersonneMicro to set
     */
    public void setTypePersonneMicro(String typePersonneMicro) {
        this.typePersonneMicro = typePersonneMicro;
    }

}
