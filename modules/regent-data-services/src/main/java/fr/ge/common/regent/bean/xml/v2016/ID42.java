package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;
import org.xmlfield.core.types.XmlString;

/**
 * Le Interface IC403.
 */
@ResourceXPath("/D42")
public interface ID42 {

  /**
   * Get le D42.2.
   *
   * @return le D42.2
   */
  @FieldXPath("D42.2")
  String getD422();

  /**
   * Set le D42.2.
   *
   * @param D422
   *          le nouveau D422
   */
  void setD422(String D422);

  /**
   * Get le D42.3.
   *
   * @return le D42.3
   */
  @FieldXPath("D42.3")
  XmlString[] getD423();

  /**
   * Ajoute le to D42.3.
   *
   * @return le xml string
   */
  XmlString addToD423();

  /**
   * Set le D42.3.
   *
   * @param D423
   *          le nouveau D42.3
   */
  void setD423(XmlString[] D423);

  /**
   * Get le D42.4.
   *
   * @return le D42.4
   */
  @FieldXPath("D42.4")
  String getD424();

  /**
   * Set le D42.4.
   *
   * @param D424
   *          le nouveau D42.4
   */
  void setD424(String D424);
}
