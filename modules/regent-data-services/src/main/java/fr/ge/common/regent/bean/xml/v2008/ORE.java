package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface ORE.
 */
@ResourceXPath("/ORE")
public interface ORE {

  /**
   * Get le e21.
   *
   * @return le e21
   */
  @FieldXPath("E21")
  IE21[] getE21();

  /**
   * Ajoute le to e21.
   *
   * @return le i e21
   */
  IE21 addToE21();

  /**
   * Set le e21.
   *
   * @param E21
   *          le nouveau e21
   */
  void setE21(IE21[] E21);

  /**
   * Get le e22.
   *
   * @return le e22
   */
  @FieldXPath("E22")
  IE22[] getE22();

  /**
   * Ajoute le to e22.
   *
   * @return le i e22
   */
  IE22 addToE22();

  /**
   * Set le e22.
   *
   * @param E22
   *          le nouveau e22
   */
  void setE22(IE22[] E22);
}
