package fr.ge.common.regent.bean.formalite.dialogue.modification;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.format.annotation.NumberFormat;

import fr.ge.common.regent.bean.formalite.dialogue.AbstractEntrepriseBean;
import fr.ge.common.regent.bean.formalite.dialogue.regularisation.PersonneLieeBean;

/**
 * Le Class EntrepriseBean.
 */
public class EntrepriseBean extends AbstractEntrepriseBean<EtablissementBean, EntrepriseLieeBean, DirigeantBean, PersonneLieeBean> {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -7998540450466394889L;

    /** Le siren. */
    private String siren;

    /** Le greffe immat. */
    private String greffeImmat;

    /** Le cma immat. */
    private String cmaImmat;

    /** Le greffe secondaire oui non. */
    private String greffeSecondaireOuiNon;

    /** Le greffe secondaire nombre. */
    @NumberFormat
    private Integer greffeSecondaireNombre;

    /** Le greffe secondaire libelle. */
    private String greffeSecondaireLibelle;

    /** Le adresse entreprise pp situation. */
    private String adresseEntreprisePPSituation;

    /** L'adresse entreprise pm situation. */
    private String adresseEntreprisePMSituation;

    /** Le entreprises liees precedents exploitants. */
    private List<EntrepriseLieeBean> entreprisesLieesPrecedentsExploitants = new ArrayList<EntrepriseLieeBean>();

    /** Le eirl. */
    private EirlBean eirl = new EirlBean();

    /** Le nombre dirigeants. */
    private String nombreDirigeants;

    /** eirlDeclaration. **/
    private String eirlDeclaration;

    /** Le lieuDepotImpot. */
    private String lieuDepotImpot;

    /**
     * Get le entreprises liees precedents exploitants.
     *
     * @return the entreprisesLieesPrecedentsExploitants
     */
    public List<EntrepriseLieeBean> getEntreprisesLieesPrecedentsExploitants() {
        return entreprisesLieesPrecedentsExploitants;
    }

    /**
     * Set le entreprises liees precedents exploitants.
     *
     * @param entreprisesLieesPrecedentsExploitants
     *            the entreprisesLieesPrecedentsExploitants to set
     */
    public void setEntreprisesLieesPrecedentsExploitants(List<EntrepriseLieeBean> entreprisesLieesPrecedentsExploitants) {
        this.entreprisesLieesPrecedentsExploitants = entreprisesLieesPrecedentsExploitants;
    }

    /**
     * Get le siren.
     *
     * @return the siren
     */
    public String getSiren() {
        return this.siren;
    }

    /**
     * Set le siren.
     *
     * @param siren
     *            the siren to set
     */
    public void setSiren(String siren) {
        this.siren = siren;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EtablissementBean createNewEtablissement() {
        return new EtablissementBean();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EntrepriseLieeBean createNewEntrepriseLiee() {
        return new EntrepriseLieeBean();
    }

    /**
     * Get le greffe immat.
     *
     * @return the greffeImmat
     */
    public String getGreffeImmat() {
        return greffeImmat;
    }

    /**
     * Set le greffe immat.
     *
     * @param greffeImmat
     *            the greffeImmat to set
     */
    public void setGreffeImmat(String greffeImmat) {
        this.greffeImmat = greffeImmat;
    }

    /**
     * Get le cma immat.
     *
     * @return the cmaImmat
     */
    public String getCmaImmat() {
        return cmaImmat;
    }

    /**
     * Set le cma immat.
     *
     * @param cmaImmat
     *            the cmaImmat to set
     */
    public void setCmaImmat(String cmaImmat) {
        this.cmaImmat = cmaImmat;
    }

    /**
     * Get le greffe secondaire oui non.
     *
     * @return the greffeSecondaireOuiNon
     */
    public String getGreffeSecondaireOuiNon() {
        return greffeSecondaireOuiNon;
    }

    /**
     * Set le greffe secondaire oui non.
     *
     * @param greffeSecondaireOuiNon
     *            the greffeSecondaireOuiNon to set
     */
    public void setGreffeSecondaireOuiNon(String greffeSecondaireOuiNon) {
        this.greffeSecondaireOuiNon = greffeSecondaireOuiNon;
    }

    /**
     * Get le greffe secondaire nombre.
     *
     * @return the greffeSecondaireNombre
     */
    public Integer getGreffeSecondaireNombre() {
        return greffeSecondaireNombre;
    }

    /**
     * Set le greffe secondaire nombre.
     *
     * @param greffeSecondaireNombre
     *            the greffeSecondaireNombre to set
     */
    public void setGreffeSecondaireNombre(Integer greffeSecondaireNombre) {
        this.greffeSecondaireNombre = greffeSecondaireNombre;
    }

    /**
     * Get le greffe secondaire libelle.
     *
     * @return the greffeSecondaireLibelle
     */
    public String getGreffeSecondaireLibelle() {
        return greffeSecondaireLibelle;
    }

    /**
     * Set le greffe secondaire libelle.
     *
     * @param greffeSecondaireLibelle
     *            the greffeSecondaireLibelle to set
     */
    public void setGreffeSecondaireLibelle(String greffeSecondaireLibelle) {
        this.greffeSecondaireLibelle = greffeSecondaireLibelle;
    }

    /**
     * Get le adresse entreprise pp situation.
     *
     * @return the adresseEntreprisePPSituation
     */
    @Override
    public String getAdresseEntreprisePPSituation() {
        return adresseEntreprisePPSituation;
    }

    /**
     * Set le adresse entreprise pp situation.
     *
     * @param adresseEntreprisePPSituation
     *            the adresseEntreprisePPSituation to set
     */
    @Override
    public void setAdresseEntreprisePPSituation(String adresseEntreprisePPSituation) {
        this.adresseEntreprisePPSituation = adresseEntreprisePPSituation;
    }

    /**
     * Get le eirl.
     *
     * @return the eirl
     */
    public EirlBean getEirl() {
        return eirl;
    }

    /**
     * Set le eirl.
     *
     * @param eirl
     *            the eirl to set
     */
    public void setEirl(EirlBean eirl) {
        this.eirl = eirl;
    }

    /**
     * Set le nombre dirigeants.
     *
     * @param nombreDirigeants
     *            the nombreDirigeants to set
     */
    public void setNombreDirigeants(final String nombreDirigeants) {
        this.nombreDirigeants = nombreDirigeants;
    }

    /**
     * Get eirlDeclaration.
     * 
     * @return the eirlDeclaration
     */
    public String getEirlDeclaration() {
        return eirlDeclaration;
    }

    /**
     * Set eirlDeclaration.
     * 
     * @param eirlDeclaration
     *            the eirlDeclaration to set
     */
    public void setEirlDeclaration(final String eirlDeclaration) {
        this.eirlDeclaration = eirlDeclaration;
    }

    /**
     * Accesseur sur l'attribut {@link #adresseEntreprisePMSituation}.
     *
     * @return String adresseEntreprisePMSituation
     */
    public String getAdresseEntreprisePMSituation() {
        return adresseEntreprisePMSituation;
    }

    /**
     * Mutateur sur l'attribut {@link #adresseEntreprisePMSituation}.
     *
     * @param adresseEntreprisePMSituation
     *            la nouvelle valeur de l'attribut adresseEntreprisePMSituation
     */
    public void setAdresseEntreprisePMSituation(final String adresseEntreprisePMSituation) {
        this.adresseEntreprisePMSituation = adresseEntreprisePMSituation;
    }

    /**
     * Accesseur sur l'attribut {@link #lieuDepotImpot}.
     *
     * @return String lieuDepotImpot
     */
    public String getLieuDepotImpot() {
        return lieuDepotImpot;
    }

    /**
     * Mutateur sur l'attribut {@link #lieuDepotImpot}.
     *
     * @param lieuDepotImpot
     *            la nouvelle valeur de l'attribut lieuDepotImpot
     */
    public void setLieuDepotImpot(String lieuDepotImpot) {
        this.lieuDepotImpot = lieuDepotImpot;
    }
}
