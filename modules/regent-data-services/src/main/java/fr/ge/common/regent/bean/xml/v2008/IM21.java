/**
 * 
 */
package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * L'interface IM21.
 */
@ResourceXPath("/M21")
public interface IM21 {

  /**
   * Get le m211.
   *
   * @return le m211
   */
  @FieldXPath("M21.1")
  String getM211();

  /**
   * Set le m211.
   *
   * @param m211
   *          le nouveau m211
   */
  void setM211(String m211);

  /**
   * Get le m212.
   *
   * @return le m212
   */
  @FieldXPath("M21.2")
  String getM212();

  /**
   * Set le m212.
   *
   * @param m212
   *          le nouveau m212
   */
  void setM212(String m212);

  /**
   * Get le m214.
   *
   * @return le m214
   */
  @FieldXPath("M21.4")
  String getM214();

  /**
   * Set le m214.
   *
   * @param m214
   *          le nouveau m214
   */
  void setM214(String m214);
}
