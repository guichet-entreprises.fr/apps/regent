package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;


/**
 * Le Interface ID03.
 */
@ResourceXPath("/D03")

public interface ID03 {

	
	  /**
	   * Get le D031.
	   *
	   * @return le D031
	   */
	  @FieldXPath("D03.1")
	  String getD031();

	  /**
	   * Set le D031.
	   *
	   * @param D031
	   *          le nouveau D031
	   */
	  void setD031(String D031);
		
	  /**
	   * Get le D032.
	   *
	   * @return le D032
	   */
	  @FieldXPath("D03.2")
	  String getD032();

	  /**
	   * Set le D032.
	   *
	   * @param D032
	   *          le nouveau D032
	   */
	  void setD032(String D032);
		
	  /**
	   * Get le D033.
	   *
	   * @return le D033
	   */
	  @FieldXPath("D03.3")
	  String getD033();

	  /**
	   * Set le D033.
	   *
	   * @param D033
	   *          le nouveau D033
	   */
	  void setD033(String D033);
		
	  /**
	   * Get le D02.
	   *
	   * @return le D02
	   */
	  @FieldXPath("D03.4")
	  String getD034();

	  /**
	   * Set le D02.
	   *
	   * @param D02
	   *          le nouveau D02
	   */
	  void setD034(String D034);

}
