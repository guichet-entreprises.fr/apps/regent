package fr.ge.common.regent.bean.xml.formalite.dialogue.modification;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;
import org.xmlfield.core.types.XmlString;

import fr.ge.common.regent.bean.xml.formalite.IFormalite;
import fr.ge.common.regent.bean.xml.transverse.IAdresse;

/**
 * Le Interface IEirlModification.
 */
@ResourceXPath("/formalite")
public interface IEirlModification extends IFormalite {

    /** La constante MODEL_VERSION. */
    static final int MODEL_VERSION = 1;

    /**
     * Get le modif statut eir l1.
     *
     * @return le modif statut eir l1
     */

    /**
     * Getter de l'attribut statutEIRL.
     * 
     * @return la valeur de statutEIRL
     */
    @FieldXPath(value = "statutEIRL")
    String getStatutEIRL();

    /**
     * Setter de l'attribut statutEIRL.
     * 
     * @param statutEIRL
     *            la nouvelle valeur de statutEIRL
     */
    void setStatutEIRL(String statutEIRL);

    /**
     * Get le modif statut eir l1.
     *
     * @return le modif statut eir l1
     */
    @FieldXPath(value = "modifStatutEIRL1")
    String getModifStatutEIRL1();

    /**
     * Set le modif statut eir l1.
     *
     * @param modifStatutEIRL1
     *            le nouveau modif statut eir l1
     */
    void setModifStatutEIRL1(String modifStatutEIRL1);

    /**
     * Get le modif date eirl.
     *
     * @return le modif date eirl
     */
    @FieldXPath(value = "modifDateEIRL")
    String getModifDateEIRL();

    /**
     * Set le modif date eirl.
     *
     * @param modifDateEIRL
     *            le nouveau modif date eirl
     */
    void setModifDateEIRL(String modifDateEIRL);

    /**
     * Get le denomination.
     *
     * @return le denomination
     */
    @FieldXPath(value = "denomination")
    String getDenomination();

    /**
     * Set le denomination.
     *
     * @param denomination
     *            le nouveau denomination
     */
    void setDenomination(String denomination);

    /**
     * Get le modif nouvelle denomination eirl.
     *
     * @return le modif nouvelle denomination eirl
     */
    @FieldXPath(value = "modifNouvelleDenominationEIRL")
    String getModifNouvelleDenominationEIRL();

    /**
     * Set le modif nouvelle denomination eirl.
     *
     * @param modifNouvelleDenominationEIRL
     *            le nouveau modif nouvelle denomination eirl
     */
    void setModifNouvelleDenominationEIRL(String modifNouvelleDenominationEIRL);

    /**
     * Get le modif adresse e irl.
     *
     * @return le modif adresse e irl
     */
    @FieldXPath(value = "modifAdresseEIrl")
    IAdresse getModifAdresseEIrl();

    /**
     * Set le modif adresse e irl.
     *
     * @param modifAdresseEIrl
     *            le nouveau modif adresse e irl
     */
    void setModifAdresseEIrl(IAdresse modifAdresseEIrl);

    /**
     * New modif adresse e irl.
     *
     * @return le i adresse
     */
    IAdresse newModifAdresseEIrl();

    /**
     * Get le date cloture exercice comptable.
     *
     * @return le date cloture exercice comptable
     */
    @FieldXPath(value = "dateClotureExerciceComptable")
    String getDateClotureExerciceComptable();

    /**
     * Set le date cloture exercice comptable.
     *
     * @param dateClotureExerciceComptable
     *            le nouveau date cloture exercice comptable
     */
    void setDateClotureExerciceComptable(String dateClotureExerciceComptable);

    /**
     * Get le modif date cloture exercice comptable.
     *
     * @return le modif date cloture exercice comptable
     */
    @FieldXPath(value = "modifDateClotureExerciceComptable")
    String getModifDateClotureExerciceComptable();

    /**
     * Set le modif date cloture exercice comptable.
     *
     * @param modifDateClotureExerciceComptable
     *            le nouveau modif date cloture exercice comptable
     */
    void setModifDateClotureExerciceComptable(String modifDateClotureExerciceComptable);

    /**
     * Get le objet.
     *
     * @return le objet
     */
    @FieldXPath(value = "objet")
    String getObjet();

    /**
     * Set le objet.
     *
     * @param objet
     *            le nouveau objet
     */
    void setObjet(String objet);

    /**
     * Get le modif objet.
     *
     * @return le modif objet
     */
    @FieldXPath(value = "modifObjet")
    String getModifObjet();

    /**
     * Set le modif objet.
     *
     * @param modifObjet
     *            le nouveau modif objet
     */
    void setModifObjet(String modifObjet);

    /**
     * Get le registre depot.
     *
     * @return le registre depot
     */
    @FieldXPath(value = "registreDepot")
    String getRegistreDepot();

    /**
     * Set le registre depot.
     *
     * @param registreDepot
     *            le nouveau registre depot
     */
    void setRegistreDepot(String registreDepot);

    /**
     * Get le precedent eirl denomination.
     *
     * @return le precedent eirl denomination
     */
    @FieldXPath(value = "precedentEIRLDenomination")
    String getPrecedentEIRLDenomination();

    /**
     * Set le precedent eirl denomination.
     *
     * @param precedentEIRLDenomination
     *            le nouveau precedent eirl denomination
     */
    void setPrecedentEIRLDenomination(String precedentEIRLDenomination);

    /**
     * Get le precedent eirl registre.
     *
     * @return le precedent eirl registre
     */
    @FieldXPath(value = "precedentEIRLRegistre")
    String getPrecedentEIRLRegistre();

    /**
     * Set le precedent eirl registre.
     *
     * @param precedentEIRLRegistre
     *            le nouveau precedent eirl registre
     */
    void setPrecedentEIRLRegistre(String precedentEIRLRegistre);

    /**
     * Get le precedent eirl lieu immatriculation.
     *
     * @return le precedent eirl lieu immatriculation
     */
    @FieldXPath(value = "precedentEIRLLieuImmatriculation")
    String getPrecedentEIRLLieuImmatriculation();

    /**
     * Set le precedent eirl lieu immatriculation.
     *
     * @param precedentEIRLLieuImmatriculation
     *            le nouveau precedent eirl lieu immatriculation
     */
    void setPrecedentEIRLLieuImmatriculation(String precedentEIRLLieuImmatriculation);

    /**
     * Get le precedent eirlsiren.
     *
     * @return le precedent eirlsiren
     */
    @FieldXPath(value = "precedentEIRLSIREN")
    String getPrecedentEIRLSIREN();

    /**
     * Set le precedent eirlsiren.
     *
     * @param precedentEIRLSIREN
     *            le nouveau precedent eirlsiren
     */
    void setPrecedentEIRLSIREN(String precedentEIRLSIREN);

    /**
     * Get le registre depot2.
     *
     * @return le registre depot2
     */
    @FieldXPath(value = "registreDepot2")
    String getRegistreDepot2();

    /**
     * Set le registre depot2.
     *
     * @param registreDepot2
     *            le nouveau registre depot2
     */
    void setRegistreDepot2(String registreDepot2);

    /**
     * Get le lieu registre depot.
     *
     * @return le lieu registre depot
     */
    @FieldXPath(value = "lieuRegistreDepot")
    String getLieuRegistreDepot();

    /**
     * Set le lieu registre depot.
     *
     * @param lieuRegistreDepot
     *            le nouveau lieu registre depot
     */
    void setLieuRegistreDepot(String lieuRegistreDepot);

    /**
     * Get le destination patrimoine affecte.
     *
     * @return le destination patrimoine affecte
     */
    @FieldXPath(value = "destinationPatrimoineAffecte")
    String getDestinationPatrimoineAffecte();

    /**
     * Set le destination patrimoine affecte.
     *
     * @param destinationPatrimoineAffecte
     *            le nouveau destination patrimoine affecte
     */
    void setDestinationPatrimoineAffecte(String destinationPatrimoineAffecte);

    /**
     * Get le versement liberatoire oui non.
     *
     * @return le versement liberatoire oui non
     */
    @FieldXPath(value = "versementLiberatoireOuiNon")
    String getVersementLiberatoireOuiNon();

    /**
     * Set le versement liberatoire oui non.
     *
     * @param versementLiberatoireOuiNon
     *            le nouveau versement liberatoire oui non
     */
    void setVersementLiberatoireOuiNon(String versementLiberatoireOuiNon);

    /**
     * Get le definition patrimoine.
     *
     * @return le definition patrimoine
     */

    @FieldXPath(value = "definitionPatrimoines/definitionPatrimoine")
    XmlString[] getDefinitionPatrimoine();

    /**
     * Set le definitionPatrimoine .
     * 
     * @param definitionPatrimoine
     *            le nouveau definitionPatrimoine
     */
    void setDefinitionPatrimoine(XmlString[] definitionPatrimoine);

    /**
     * Retire le from DefinitionPatrimoine DefinitionPatrimoine.
     * 
     * @param evenement
     *            le evenement
     */
    void removeFromDefinitionPatrimoine(XmlString definitionPatrimoine);

    /**
     * Ajoute le to DefinitionPatrimoine DefinitionPatrimoine.
     * 
     * @return le xml string
     */
    XmlString addToDefinitionPatrimoine();

}
