package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface CAE.
 */
@ResourceXPath("/CAE")
public interface CAE {

  /**
   * Get le e73.
   *
   * @return le e73
   */
  @FieldXPath("E73")
  IE73[] getE73();

  /**
   * Ajoute le to e73.
   *
   * @return le i e73
   */
  IE73 addToE73();

  /**
   * Set le e73.
   *
   * @param E73
   *          le nouveau e73
   */
  void setE73(IE73[] E73);

  /**
   * Get le e75.
   *
   * @return le e75
   */
  @FieldXPath("E75")
  IE75[] getE75();

  /**
   * Ajoute le to e75.
   *
   * @return le i e75
   */
  IE75 addToE75();

  /**
   * Set le e75.
   *
   * @param E75
   *          le nouveau e75
   */
  void setE75(IE75[] E75);

  /**
   * Get le e77.
   *
   * @return le e77
   */
  @FieldXPath("E77")
  IE77[] getE77();

  /**
   * Ajoute le to e77.
   *
   * @return le i e77
   */
  IE77 addToE77();

  /**
   * Set le e77.
   *
   * @param E77
   *          le nouveau e77
   */
  void setE77(IE77[] E77);

}
