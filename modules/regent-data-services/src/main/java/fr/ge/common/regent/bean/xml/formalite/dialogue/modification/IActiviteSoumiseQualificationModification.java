package fr.ge.common.regent.bean.xml.formalite.dialogue.modification;

import org.xmlfield.annotations.ResourceXPath;

import fr.ge.common.regent.bean.xml.formalite.IActiviteSoumiseQualification;

/**
 * L'interface de l'entité Activité soumise à qualification de l'entreprise.
 * 
 * @author $Author: hhichri $
 * @version $Revision: 0 $
 */
@ResourceXPath("/formalite")
public interface IActiviteSoumiseQualificationModification extends IActiviteSoumiseQualification {

}
