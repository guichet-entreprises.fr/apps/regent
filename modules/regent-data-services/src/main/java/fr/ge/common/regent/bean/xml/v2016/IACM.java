package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * L'interface ACM.
 */
@ResourceXPath("/ACM")
public interface IACM {

  /**
   * Get le m62.
   *
   * @return le m62
   */
  @FieldXPath("M62")
  String getM62();

  /**
   * Set le m62.
   *
   * @param m62
   *          le nouveau m62
   */
  void setM62(String m62);
}
