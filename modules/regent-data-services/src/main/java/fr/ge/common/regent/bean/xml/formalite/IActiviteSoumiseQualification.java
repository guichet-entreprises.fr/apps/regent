/**
 * 
 */
package fr.ge.common.regent.bean.xml.formalite;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Classe de refacto pour l'activité soumise à qualification.
 * 
 * @author $Author: hhichri $
 * @version $Revision: 0 $
 */
@ResourceXPath("/formalite")
public interface IActiviteSoumiseQualification extends IFormalite {
  /**
   * Getter de l'attribut activiteJQPA.
   * 
   * @return la valeur de activiteJQPA
   */
  @FieldXPath(value = "activiteJQPA")
  String getActiviteJQPA();

  /**
   * Accesseur sur l'attribut activite plus importante.
   *
   * @return activite plus importante
   */
  @FieldXPath(value = "aqpaSituation")
  String getAqpaSituation();

  /**
   * Getter de l'attribut qualitePersonneQualifieePP.
   * 
   * @return la valeur de qualitePersonneQualifieePP
   */
  @FieldXPath(value = "qualitePersonneQualifieePP")
  String getQualitePersonneQualifieePP();

  /**
   * Getter de l'attribut qualitePersonneQualifieePPAutre.
   * 
   * @return la valeur de qualitePersonneQualifieePPAutre
   */
  @FieldXPath(value = "qualitePersonneQualifieePPAutre")
  String getQualitePersonneQualifieePPAutre();

  /**
   * Getter de l'attribut identitePPNomNaissance.
   * 
   * @return la valeur de identitePPNomNaissance
   */
  @FieldXPath(value = "identitePPNomNaissance")
  String getIdentitePPNomNaissance();

  /**
   * Getter de l'attribut identitePPNomUsage.
   * 
   * @return la valeur de identitePPNomUsage
   */
  @FieldXPath(value = "identitePPNomUsage")
  String getIdentitePPNomUsage();

  /**
   * Getter de l'attribut identitePPPrenom1.
   * 
   * @return la valeur de identitePPPrenom1
   */
  @FieldXPath(value = "identitePPPrenom1")
  String getIdentitePPPrenom1();

  /**
   * Getter de l'attribut identitePPPrenom1.
   * 
   * @return la valeur de identitePPPrenom1
   */
  @FieldXPath(value = "identitePPPrenom2")
  String getIdentitePPPrenom2();

  /**
   * Getter de l'attribut identitePPPrenom3.
   * 
   * @return la valeur de identitePPPrenom3
   */
  @FieldXPath(value = "identitePPPrenom3")
  String getIdentitePPPrenom3();

  /**
   * Getter de l'attribut identitePPDateNaissance.
   * 
   * @return la valeur de identitePPDateNaissance
   */
  @FieldXPath(value = "identitePPDateNaissance")
  String getIdentitePPDateNaissance();

  /**
   * Getter de l'attribut identitePPLieuNaissancePays.
   * 
   * @return la valeur de identitePPLieuNaissancePays
   */
  @FieldXPath(value = "identitePPLieuNaissancePays")
  String getIdentitePPLieuNaissancePays();

  /**
   * Getter de l'attribut identitePPLieuNaissanceDepartement.
   * 
   * @return la valeur de identitePPLieuNaissanceDepartement
   */
  @FieldXPath(value = "identitePPLieuNaissanceDepartement")
  String getIdentitePPLieuNaissanceDepartement();

  /**
   * Getter de l'attribut identitePPLieuNaissanceCommune.
   * 
   * @return la valeur de identitePPLieuNaissanceCommune
   */
  @FieldXPath(value = "identitePPLieuNaissanceCommune")
  String getIdentitePPLieuNaissanceCommune();

  /**
   * Getter de l'attribut identitePPLieuNaissanceVille.
   * 
   * @return la valeur de identitePPLieuNaissanceVille
   */
  @FieldXPath(value = "identitePPLieuNaissanceVille")
  String getIdentitePPLieuNaissanceVille();

  /**
   * Getter de l'attribut qualitePersonneQualifieePM.
   * 
   * @return la valeur de qualitePersonneQualifieePM
   */
  @FieldXPath(value = "qualitePersonneQualifieePM")
  String getQualitePersonneQualifieePM();

  /**
   * Getter de l'attribut qualitePersonneQualifieePMAutre.
   * 
   * @return la valeur de qualitePersonneQualifieePMAutre
   */
  @FieldXPath(value = "qualitePersonneQualifieePMAutre")
  String getQualitePersonneQualifieePMAutre();

  /**
   * Getter de l'attribut identitePMNomNaissance.
   * 
   * @return la valeur de identitePMNomNaissance
   */
  @FieldXPath(value = "identitePMNomNaissance")
  String getIdentitePMNomNaissance();

  /**
   * Getter de l'attribut identitePMNomUsage.
   * 
   * @return la valeur de identitePMNomUsage
   */
  @FieldXPath(value = "identitePMNomUsage")
  String getIdentitePMNomUsage();

  /**
   * Getter de l'attribut identitePMPrenom1.
   * 
   * @return la valeur de identitePMPrenom1
   */
  @FieldXPath(value = "identitePMPrenom1")
  String getIdentitePMPrenom1();

  /**
   * Getter de l'attribut identitePMPrenom1.
   * 
   * @return la valeur de identitePMPrenom1
   */
  @FieldXPath(value = "identitePMPrenom2")
  String getIdentitePMPrenom2();

  /**
   * Getter de l'attribut identitePMPrenom3.
   * 
   * @return la valeur de identitePMPrenom3
   */
  @FieldXPath(value = "identitePMPrenom3")
  String getIdentitePMPrenom3();

  /**
   * Getter de l'attribut identitePMDateNaissance.
   * 
   * @return la valeur de identitePMDateNaissance
   */
  @FieldXPath(value = "identitePMDateNaissance")
  String getIdentitePMDateNaissance();

  /**
   * Getter de l'attribut identitePMLieuNaissancePays.
   * 
   * @return la valeur de identitePMLieuNaissancePays
   */
  @FieldXPath(value = "identitePMLieuNaissancePays")
  String getIdentitePMLieuNaissancePays();

  /**
   * Getter de l'attribut identitePMLieuNaissanceDepartement.
   * 
   * @return la valeur de identitePMLieuNaissanceDepartement
   */
  @FieldXPath(value = "identitePMLieuNaissanceDepartement")
  String getIdentitePMLieuNaissanceDepartement();

  /**
   * Getter de l'attribut identitePMLieuNaissanceCommune.
   * 
   * @return la valeur de identitePMLieuNaissanceCommune
   */
  @FieldXPath(value = "identitePMLieuNaissanceCommune")
  String getIdentitePMLieuNaissanceCommune();

  /**
   * Getter de l'attribut identitePMLieuNaissanceVille.
   * 
   * @return la valeur de identitePMLieuNaissanceVille
   */
  @FieldXPath(value = "identitePMLieuNaissanceVille")
  String getIdentitePMLieuNaissanceVille();

  /**
   * setter de l'attribut activiteJQPA.
   * 
   * @param valeur
   *          valeur de activiteJQPA
   */
  void setActiviteJQPA(String valeur);

  /**
   * Accesseur sur l'attribut activite plus importante.
   *
   * @param valeur
   *          Aqpa Situation
   */
  void setAqpaSituation(String valeur);

  /**
   * setter de l'attribut qualitePersonneQualifieePP.
   * 
   * @param valeur
   *          valeur de qualitePersonneQualifieePP
   */
  void setQualitePersonneQualifieePP(String valeur);

  /**
   * setter de l'attribut qualitePersonneQualifieePPAutre.
   * 
   * @param valeur
   *          valeur de qualitePersonneQualifieePPAutre
   */
  void setQualitePersonneQualifieePPAutre(String valeur);

  /**
   * setter de l'attribut identitePPNomNaissance.
   * 
   * @param valeur
   *          valeur de identitePPNomNaissance
   */
  void setIdentitePPNomNaissance(String valeur);

  /**
   * setter de l'attribut identitePPNomUsage.
   * 
   * @param valeur
   *          valeur de identitePPNomUsage
   */
  void setIdentitePPNomUsage(String valeur);

  /**
   * setter de l'attribut identitePPPrenom1.
   * 
   * @param valeur
   *          valeur de identitePPPrenom1
   */
  void setIdentitePPPrenom1(String valeur);

  /**
   * setter de l'attribut identitePPPrenom1.
   * 
   * @param valeur
   *          valeur de identitePPPrenom1
   */
  void setIdentitePPPrenom2(String valeur);

  /**
   * setter de l'attribut identitePPPrenom3.
   * 
   * @param valeur
   *          valeur de identitePPPrenom3
   */
  void setIdentitePPPrenom3(String valeur);

  /**
   * setter de l'attribut identitePPDateNaissance.
   * 
   * @param valeur
   *          valeur de identitePPDateNaissance
   */
  void setIdentitePPDateNaissance(String valeur);

  /**
   * setter de l'attribut identitePPLieuNaissancePays.
   * 
   * @param valeur
   *          valeur de identitePPLieuNaissancePays
   */
  void setIdentitePPLieuNaissancePays(String valeur);

  /**
   * setter de l'attribut identitePPLieuNaissanceDepartement.
   * 
   * @param valeur
   *          valeur de identitePPLieuNaissanceDepartement
   */
  void setIdentitePPLieuNaissanceDepartement(String valeur);

  /**
   * setter de l'attribut identitePPLieuNaissanceCommune.
   * 
   * @param valeur
   *          valeur de identitePPLieuNaissanceCommune
   */
  void setIdentitePPLieuNaissanceCommune(String valeur);

  /**
   * setter de l'attribut identitePPLieuNaissanceVille.
   * 
   * @param valeur
   *          valeur de identitePPLieuNaissanceVille
   */
  void setIdentitePPLieuNaissanceVille(String valeur);

  /**
   * setter de l'attribut qualitePersonneQualifieePM.
   * 
   * @param valeur
   *          valeur de qualitePersonneQualifieePM
   */
  void setQualitePersonneQualifieePM(String valeur);

  /**
   * setter de l'attribut qualitePersonneQualifieePMAutre.
   * 
   * @param valeur
   *          valeur de qualitePersonneQualifieePMAutre
   */
  void setQualitePersonneQualifieePMAutre(String valeur);

  /**
   * setter de l'attribut identitePMNomNaissance.
   * 
   * @param valeur
   *          valeur de identitePMNomNaissance
   */
  void setIdentitePMNomNaissance(String valeur);

  /**
   * setter de l'attribut identitePMNomUsage.
   * 
   * @param valeur
   *          valeur de identitePMNomUsage
   */
  void setIdentitePMNomUsage(String valeur);

  /**
   * setter de l'attribut identitePMPrenom1.
   * 
   * @param valeur
   *          valeur de identitePMPrenom1
   */
  void setIdentitePMPrenom1(String valeur);

  /**
   * setter de l'attribut identitePMPrenom1.
   * 
   * @param valeur
   *          valeur de identitePMPrenom1
   */
  void setIdentitePMPrenom2(String valeur);

  /**
   * setter de l'attribut identitePMPrenom3.
   * 
   * @param valeur
   *          valeur de identitePMPrenom3
   */
  void setIdentitePMPrenom3(String valeur);

  /**
   * setter de l'attribut identitePMDateNaissance.
   * 
   * @param valeur
   *          valeur de identitePMDateNaissance
   */
  void setIdentitePMDateNaissance(String valeur);

  /**
   * setter de l'attribut identitePMLieuNaissancePays.
   * 
   * @param valeur
   *          valeur de identitePMLieuNaissancePays
   */
  void setIdentitePMLieuNaissancePays(String valeur);

  /**
   * setter de l'attribut identitePMLieuNaissanceDepartement.
   * 
   * @param valeur
   *          valeur de identitePMLieuNaissanceDepartement
   */
  void setIdentitePMLieuNaissanceDepartement(String valeur);

  /**
   * setter de l'attribut identitePMLieuNaissanceCommune.
   * 
   * @param valeur
   *          valeur de identitePMLieuNaissanceCommune
   */
  void setIdentitePMLieuNaissanceCommune(String valeur);

  /**
   * setter de l'attribut identitePMLieuNaissanceVille.
   * 
   * @param valeur
   *          valeur de identitePMLieuNaissanceVille
   */
  void setIdentitePMLieuNaissanceVille(String valeur);

}
