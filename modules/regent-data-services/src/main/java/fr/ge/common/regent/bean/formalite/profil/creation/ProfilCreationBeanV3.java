/**
 * 
 */
package fr.ge.common.regent.bean.formalite.profil.creation;

/**
 * Class ProfilCreationBeanV3.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
public class ProfilCreationBeanV3 extends ProfilCreationBeanV2 {

  /** UID. */
  private static final long serialVersionUID = 8052440939916200926L;

}
