package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface JGE.
 */
@ResourceXPath("/JGE")
public interface JGE {

  /**
   * Get le j00.
   *
   * @return le j00
   */
  @FieldXPath("J00")
  String getJ00();

  /**
   * Set le j00.
   *
   * @param J00
   *          le nouveau j00
   */
  void setJ00(String J00);
}
