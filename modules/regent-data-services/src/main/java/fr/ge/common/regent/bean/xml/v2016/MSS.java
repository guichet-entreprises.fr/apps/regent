package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface CAS.
 */
@ResourceXPath("/MSS")
public interface MSS {

  /**
   * Get le A31.
   *
   * @return le A31
   */
  @FieldXPath("A31")
  IA31[] getA31();

  /**
   * Ajoute le to A31.
   *
   * @return le i A31
   */
  IA31 addToA31();

  /**
   * Set le A31.
   *
   * @param A31
   *          le nouveau A31
   */
  void setA31(IA31[] A31);

}
