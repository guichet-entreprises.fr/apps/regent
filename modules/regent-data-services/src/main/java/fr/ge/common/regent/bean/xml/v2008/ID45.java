package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IC403.
 */
@ResourceXPath("/D45")
public interface ID45 {

  /**
   * Get le D45.6.
   *
   * @return le D45.6
   */
  @FieldXPath("D45.6")
  String getD456();

  /**
   * Set le D45.6
   *
   * @param D456
   *          le nouveau D45.6
   */
  void setD456(String D456);

  /**
   * Get le D45.7.
   *
   * @return le D45.7
   */
  @FieldXPath("D45.7")
  String getD457();

  /**
   * Set le D45.7.
   *
   * @param D457
   *          le nouveau D457
   */
  void setD457(String D457);

  /**
   * Get le D45.8.
   *
   * @return le D45.8
   */
  @FieldXPath("D45.8")
  String getD458();

  /**
   * Set le D45.8.
   *
   * @param D458
   *          le nouveau D45.8
   */
  void setD458(String D458);

  /**
   * Get le D45.10.
   *
   * @return le D45.10
   */
  @FieldXPath("D45.10")
  String getD4510();

  /**
   * Set le D45.10.
   *
   * @param D4510
   *          le nouveau D45.10
   */
  void setD4510(String D4510);

  /**
   * Get le D45.11.
   *
   * @return le D45.11
   */
  @FieldXPath("D45.11")
  String getD4511();

  /**
   * Set le D45.11.
   *
   * @param D4511
   *          le nouveau D45.11
   */
  void setD4511(String D4511);

  /**
   * Get le D45.12.
   *
   * @return le D45.12
   */
  @FieldXPath("D45.12")
  String getD4512();

  /**
   * Set le D45.12.
   *
   * @param D4512
   *          le nouveau D45.12
   */
  void setD4512(String D4512);

  /**
   * Get le D45.13.
   *
   * @return le D45.13
   */
  @FieldXPath("D45.13")
  String getD4513();

  /**
   * Set le D45.13.
   *
   * @param D4513
   *          le nouveau D45.13
   */
  void setD4513(String D4513);

  /**
   * Get le D45.14.
   *
   * @return le D45.14
   */
  @FieldXPath("D45.14")
  String getD4514();

  /**
   * Set le D45.14.
   *
   * @param D4514
   *          le nouveau D45.14
   */
  void setD4514(String D4514);

}
