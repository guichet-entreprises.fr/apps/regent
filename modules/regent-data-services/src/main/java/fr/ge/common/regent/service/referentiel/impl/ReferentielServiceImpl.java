package fr.ge.common.regent.service.referentiel.impl;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import fr.ge.common.regent.bean.IXmlRegent;
import fr.ge.common.regent.bean.modele.referentiel.ENormeEvenement;
import fr.ge.common.regent.bean.modele.referentiel.ENormeFlux;
import fr.ge.common.regent.bean.xml.TypeFluxXml;
import fr.ge.common.regent.constante.TypeDossierEnum;
import fr.ge.common.regent.constante.enumeration.DestinataireXmlRegentEnum;
import fr.ge.common.regent.persistance.dao.referentiel.NormeEvenementDao;
import fr.ge.common.regent.persistance.dao.referentiel.NormeFluxDao;
import fr.ge.common.regent.service.referentiel.ReferentielReglesService;
import fr.ge.common.regent.service.referentiel.ReferentielService;

/**
 * Classe d'implémentation des interfaces d'accès aux référentiels
 * {@link ReferentielService}, {@link ReferentielReglesService},
 * {@link ReferentielSupraService} et {@link ReferentielSupraProfilService}.
 * 
 * @author $Author: fbeaurai $
 * @version $Revision: 0 $
 */
public class ReferentielServiceImpl implements ReferentielService {

    /** String get. */
    private static final String GET_METHOD_PREFIX = "get";

    /** Le norme flux dao. */
    @Autowired
    private NormeFluxDao normeFluxDao;

    /** normeEvenement dao *. */
    @Autowired
    private NormeEvenementDao normeEvenementDao;

    /** Injection du service d'accès au domaine regles. */
    @Autowired
    private ReferentielReglesService refReglesService;

    /** Le logger fonctionnel. */
    private static final Logger LOGGER_FONC = LoggerFactory.getLogger(ReferentielServiceImpl.class);

    /**
     * On recupere une liste normeFlux, exclusivement depuis la base de données
     * 
     * @param destinataire
     *            le destinataire
     * @param evenement
     *            le evenement
     * @return List<ENormeFlux> normesFlux;
     */
    private List<ENormeFlux> getNormesFlux(final DestinataireXmlRegentEnum destinataire, final List<String> evenement) {
        List<ENormeFlux> normesFlux;

        // Recherche dans la base de données
        normesFlux = getNormeFluxDao().getFluxByEvenementAndDestinataire(destinataire, evenement);
        if (normesFlux.isEmpty()) {
            LOGGER_FONC.warn("Aucune ligne trouvée dans normeflux avec le destinataire " + destinataire + " pour les événements " + evenement);
        }
        return normesFlux;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true, value = "regent")
    public List<TypeFluxXml> listerFluxParDestinataireEtEvenements(final DestinataireXmlRegentEnum destinataire, final List<String> evenement, final TypeDossierEnum typeDossier) {
        // si existe dans liste prédefinis si non (destinataire, evenement);

        List<TypeFluxXml> retours = new ArrayList<TypeFluxXml>();

        List<ENormeFlux> normesFlux = this.getNormesFlux(destinataire, evenement);

        if (CollectionUtils.isNotEmpty(normesFlux)) {

            Map<String, TypeFluxXml> mapFlux = new HashMap<String, TypeFluxXml>();

            try {

                String getterName = GET_METHOD_PREFIX + StringUtils.capitalize(destinataire.getNomAttribut());
                Method method = ENormeFlux.class.getMethod(getterName);
                TypeFluxXml typeFluxXml = null;
                List<String> evenements = new ArrayList<String>();

                HashMap<String, Boolean> typeOperationKey = new HashMap<String, Boolean>();

                for (ENormeFlux normeFlux : normesFlux) {
                    // Recupere le flux de l'evenement pour le destinataire en
                    // question

                    // on ne traite qu'un seul type d'opération, celui passé en
                    // paramètre
                    // (C=Création, R=Régularisation, M=Modification,
                    // F=Cessation)
                    String normeFluxTypeDossier = normeFlux.getType();
                    if (normeFluxTypeDossier.equals(typeDossier.getTypeTechnique())) {

                        String flux = new String((String) method.invoke(normeFlux));

                        /**
                         * key composé du type de flux (partenaire) et type
                         * opération(modif/cess/regul.)
                         */
                        String keyMapFlux = flux.toUpperCase() + "-" + normeFluxTypeDossier;
                        String evenementCourant = normeFlux.getEvenement();
                        if (mapFlux.containsKey(keyMapFlux) && Boolean.TRUE.equals(typeOperationKey.get(normeFluxTypeDossier))) {

                            typeFluxXml = mapFlux.get(keyMapFlux);
                            typeFluxXml.setPoid(Integer.valueOf(Math.min(normeFlux.getPoid().intValue(), typeFluxXml.getPoid().intValue())));
                            if (typeFluxXml.getMapRoleEvenement().containsKey(normeFlux.getRole())) {
                                typeFluxXml.getMapRoleEvenement().get(normeFlux.getRole()).add(evenementCourant);
                                // on enrichit le typeFluxXml avec l'evenement
                                // courant.
                                if (!typeFluxXml.getEvenements().contains(evenementCourant)) {
                                    typeFluxXml.getEvenements().add(evenementCourant);
                                }
                            } else {
                                evenements = new ArrayList<String>();
                                evenements.add(evenementCourant);
                                typeFluxXml.getMapRoleEvenement().put(normeFlux.getRole(), evenements);
                                typeFluxXml.setEvenements(evenements);
                            }
                            typeFluxXml.setTypeDeFlux(flux);
                            typeFluxXml.setRoleDeFlux(normeFlux.getRole());

                        } else {
                            evenements = new ArrayList<String>();
                            evenements.add(evenementCourant);
                            typeFluxXml = new TypeFluxXml(new HashMap<String, List<String>>(), normeFlux.getPoid(), flux, normeFluxTypeDossier, normeFlux.getRole(), evenements);
                            typeFluxXml.getMapRoleEvenement().put(normeFlux.getRole(), evenements);
                            mapFlux.put(keyMapFlux, typeFluxXml);
                        }

                        // différent type opération(Modif/Regul/Cess) existantes
                        // pour les lignes normesFlux
                        typeOperationKey.put(normeFluxTypeDossier, true);
                    }
                }

            } catch (Exception e) {
                LOGGER_FONC.error(e.getMessage(), e);
            }

            retours.addAll(mapFlux.values());
            Collections.sort(retours);
        }
        return retours;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true, value = "regent")
    public Map<String, Boolean> getRubriqueOblogatoireOuConditionnelle(String version, List<String> evenements) {
        Map<String, Boolean> rubriqueOblogatoireOuConditionnelle = new HashMap<String, Boolean>();
        List<ENormeEvenement> listRubrique = getNormeEvenementDao().getRubriqueOblogatoireOuConditionnelle(version, evenements);

        for (ENormeEvenement eNormeEvenement : listRubrique) {
            String rubrique = eNormeEvenement.getRubrique();
            if (rubriqueOblogatoireOuConditionnelle.containsKey(rubrique)) {
                Boolean oblogatoireOuConditionnelle = rubriqueOblogatoireOuConditionnelle.get(rubrique);
                BooleanUtils.toBooleanDefaultIfNull(oblogatoireOuConditionnelle, false);
                if (!oblogatoireOuConditionnelle.equals(eNormeEvenement.isCondition())) {
                    rubriqueOblogatoireOuConditionnelle.put(rubrique, Boolean.FALSE);
                }
            } else {
                rubriqueOblogatoireOuConditionnelle.put(rubrique, eNormeEvenement.isCondition());
            }
        }
        return rubriqueOblogatoireOuConditionnelle;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<IXmlRegent> recupererToutesLesReglesRegent() {
        return getRefReglesService().recupererToutesLesReglesRegent();
    }

    /**
     * Accesseur sur l'attribut {@link #refReglesService}.
     * 
     * @return RefReglesService refReglesService
     */
    public ReferentielReglesService getRefReglesService() {
        return refReglesService;
    }

    /**
     * Accesseur sur l'attribut {@link #norme flux dao}.
     * 
     * @return the normeFluxDao
     */
    public NormeFluxDao getNormeFluxDao() {
        return normeFluxDao;
    }

    /**
     * Accesseur sur l'attribut {@link #norme evenement dao}.
     * 
     * @return the normeEvenementDao
     */
    public NormeEvenementDao getNormeEvenementDao() {
        return normeEvenementDao;
    }
}
