package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IE76.
 */
@ResourceXPath("/E76")
public interface IE76 {

  /**
   * Get le e761.
   *
   * @return le e761
   */
  @FieldXPath("E76.1")
  String getE761();

  /**
   * Set le e761.
   *
   * @param E761
   *          le nouveau e761
   */
  void setE761(String E761);

  /**
   * Get le e762.
   *
   * @return le e762
   */
  @FieldXPath("E76.2")
  String getE762();

  /**
   * Set le e762.
   *
   * @param E762
   *          le nouveau e762
   */
  void setE762(String E762);

}
