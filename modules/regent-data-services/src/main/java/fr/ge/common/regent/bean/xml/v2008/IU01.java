package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IU01.
 */
@ResourceXPath("/U01")
public interface IU01 {

  /**
   * Get le u011.
   *
   * @return le u011
   */
  @FieldXPath("U01.1")
  String getU011();

  /**
   * Set le u011.
   *
   * @param U011
   *          le nouveau u011
   */
  void setU011(String U011);

}
