/**
 * 
 */
package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Interface R02.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
@ResourceXPath("/R02")
public interface R02 {

  /**
   * Accesseur sur l'attribut {@link #r023}.
   *
   * @return r023
   */
  @FieldXPath("R02.3")
  String getR023();

  /**
   * Mutateur sur l'attribut {@link #r023}.
   *
   * @param R023
   *          la nouvelle valeur de l'attribut r023
   */
  void setR023(String R023);

  /**
   * Accesseur sur l'attribut {@link #r025}.
   *
   * @return r025
   */
  @FieldXPath("R02.5")
  String getR025();

  /**
   * Mutateur sur l'attribut {@link #r025}.
   *
   * @param R025
   *          la nouvelle valeur de l'attribut r025
   */
  void setR025(String R025);

  /**
   * Accesseur sur l'attribut {@link #r026}.
   *
   * @return r026
   */
  @FieldXPath("R02.6")
  String getR026();

  /**
   * Mutateur sur l'attribut {@link #r026}.
   *
   * @param R026
   *          la nouvelle valeur de l'attribut r026
   */
  void setR026(String R026);

  /**
   * Accesseur sur l'attribut {@link #r027}.
   *
   * @return r027
   */
  @FieldXPath("R02.7")
  String getR027();

  /**
   * Mutateur sur l'attribut {@link #r027}.
   *
   * @param R027
   *          la nouvelle valeur de l'attribut r027
   */
  void setR027(String R027);

  /**
   * Accesseur sur l'attribut {@link #r028}.
   *
   * @return r028
   */
  @FieldXPath("R02.8")
  String getR028();

  /**
   * Mutateur sur l'attribut {@link #r028}.
   *
   * @param R028
   *          la nouvelle valeur de l'attribut r028
   */
  void setR028(String R028);

  /**
   * Accesseur sur l'attribut {@link #r0210}.
   *
   * @return r0210
   */
  @FieldXPath("R02.10")
  String getR0210();

  /**
   * Mutateur sur l'attribut {@link #r0210}.
   *
   * @param R0210
   *          la nouvelle valeur de l'attribut r0210
   */
  void setR0210(String R0210);

  /**
   * Accesseur sur l'attribut {@link #r0211}.
   *
   * @return r0211
   */
  @FieldXPath("R02.11")
  String getR0211();

  /**
   * Mutateur sur l'attribut {@link #r0211}.
   *
   * @param R0211
   *          la nouvelle valeur de l'attribut r0211
   */
  void setR0211(String R0211);

  /**
   * Accesseur sur l'attribut {@link #r0212}.
   *
   * @return r0212
   */
  @FieldXPath("R02.12")
  String getR0212();

  /**
   * Mutateur sur l'attribut {@link #r0212}.
   *
   * @param R0212
   *          la nouvelle valeur de l'attribut r0212
   */
  void setR0212(String R0212);

  /**
   * Accesseur sur l'attribut {@link #r0213}.
   *
   * @return r0213
   */
  @FieldXPath("R02.13")
  String getR0213();

  /**
   * Mutateur sur l'attribut {@link #r0213}.
   *
   * @param R0213
   *          la nouvelle valeur de l'attribut r0213
   */
  void setR0213(String R0213);

  /**
   * Accesseur sur l'attribut {@link #r0214}.
   *
   * @return r0214
   */
  @FieldXPath("R02.14")
  String getR0214();

  /**
   * Mutateur sur l'attribut {@link #r0214}.
   *
   * @param R0214
   *          la nouvelle valeur de l'attribut r0214
   */
  void setR0214(String R0214);

}
