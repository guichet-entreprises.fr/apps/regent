package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Interface IIDU.
 */
@ResourceXPath("/IDU")
public interface IIDU {

  /**
   * Get le u53.
   *
   * @return le u53
   */
  @FieldXPath("U53")
  IU53[] getU53();

  /**
   * Ajoute le to u53.
   *
   * @return le u53
   */
  IU53 addToU53();

  /**
   * Set le u53.
   *
   * @param U53
   *          le nouveau u53
   */
  void setU53(IU53[] U53);

  /**
   * Get le u55.
   *
   * @return le u55
   */
  @FieldXPath("U55")
  String getU55();

  /**
   * Set le u55
   *
   * @param u55
   *          le nouveau u55
   */
  void setU55(String U55);

  /**
   * Get le u56.
   *
   * @return le u56
   */
  @FieldXPath("U56")
  String getU56();

  /**
   * Set le u56
   *
   * @param u56
   *          le nouveau u56
   */
  void setU56(String U56);
}
