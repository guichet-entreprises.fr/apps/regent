package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface Service.
 */
@ResourceXPath("Service")
public interface Service {

  /**
   * Get le idf.
   *
   * @return le idf
   */
  @FieldXPath("IDF")
  IDF[] getIDF();

  /**
   * Ajoute le to idf.
   *
   * @return le idf
   */
  IDF addToIDF();

  /**
   * Set le idf.
   *
   * @param IDF
   *          le nouveau idf
   */
  void setIDF(IDF[] IDF);

  /**
   * Get le edf.
   *
   * @return le edf
   */
  @FieldXPath("EDF")
  EDF[] getEDF();

  /**
   * Ajoute le to edf.
   *
   * @return le edf
   */
  EDF addToEDF();

  /**
   * Set le edf.
   *
   * @param EDF
   *          le nouveau edf
   */
  void setEDF(EDF[] EDF);

  /**
   * Get le adf.
   *
   * @return le adf
   */
  @FieldXPath("ADF")
  ADF[] getADF();

  /**
   * Ajoute le to adf.
   *
   * @return le adf
   */
  ADF addToADF();

  /**
   * Set le adf.
   *
   * @param ADF
   *          le nouveau adf
   */
  void setADF(ADF[] ADF);

  /**
   * Get le dmf.
   *
   * @return le dmf
   */
  @FieldXPath("DMF")
  DMF[] getDMF();

  /**
   * Ajoute le to dmf.
   *
   * @return le dmf
   */
  DMF addToDMF();

  /**
   * Set le dmf.
   *
   * @param DMF
   *          le nouveau dmf
   */
  void setDMF(DMF[] DMF);

  /**
   * Get le sif.
   *
   * @return le sif
   */
  @FieldXPath("SIF")
  SIF[] getSIF();

  /**
   * Ajoute le to sif.
   *
   * @return le sif
   */
  SIF addToSIF();

  /**
   * Set le sif.
   *
   * @param SIF
   *          le nouveau sif
   */
  void setSIF(SIF[] SIF);
}
