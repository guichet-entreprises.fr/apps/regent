package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IU11.
 */
@ResourceXPath("/U14")
public interface IU14 {

  /**
   * Get le U14.2.
   *
   * @return le U14.2
   */
  @FieldXPath("U14.2")
  String getU142();

  /**
   * Set le U142.
   *
   * @param U113
   *          le nouveau U142
   */
  void setU142(String U142);

  /**
   * Get le u115.
   *
   * @return le u115
   */
  @FieldXPath("U14.3")
  String getU143();

  /**
   * Set le u115.
   *
   * @param U115
   *          le nouveau u115
   */
  void setU143(String U143);

}
