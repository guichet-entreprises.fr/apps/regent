package fr.ge.common.regent.bean.xml.formalite.profil.creation;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface ICfe.
 */
@ResourceXPath("/cfe")
public interface ICfe {

  /**
   * Get le adresse1.
   *
   * @return the adresse1
   */
  @FieldXPath("adresse1")
  String getAdresse1();

  /**
   * Get le adresse2.
   *
   * @return the adresse2
   */
  @FieldXPath("adresse2")
  String getAdresse2();

  /**
   * Get le adresse3.
   *
   * @return the adresse3
   */
  @FieldXPath("adresse3")
  String getAdresse3();

  /**
   * Getter de l'attribut code.
   * 
   * @return la valeur de code
   */
  @FieldXPath("code")
  String getCode();

  /**
   * Get le fax.
   *
   * @return the fax
   */
  @FieldXPath("fax")
  String getFax();

  /**
   * Getter de l'attribut labelActivitesSimplifiees.
   * 
   * @return la valeur de labelActivitesSimplifiees
   */
  @FieldXPath("labelActivitesSimplifiees")
  String getLabelActivitesSimplifiees();

  /**
   * Getter de l'attribut lienAgencePresseLE.
   * 
   * @return la valeur de lienAgencePresseLE
   */
  @FieldXPath("lienAgencePresseLE")
  String getLienAgencePresseLE();

  /**
   * Getter de l'attribut lienAgencePresseLPS.
   * 
   * @return la valeur de lienAgencePresseLPS
   */
  @FieldXPath("lienAgencePresseLPS")
  String getLienAgencePresseLPS();

  /**
   * Getter de l'attribut lienAgentVoyageLE.
   * 
   * @return la valeur de lienAgentVoyageLE
   */
  @FieldXPath("lienAgentVoyageLE")
  String getLienAgentVoyageLE();

  /**
   * Getter de l'attribut lienAgentVoyageLPS.
   * 
   * @return la valeur de lienAgentVoyageLPS
   */
  @FieldXPath("lienAgentVoyageLPS")
  String getLienAgentVoyageLPS();

  /**
   * Getter de l'attribut lienCollecteDechetsHuile.
   * 
   * @return la valeur de lienCollecteDechetsHuile
   */
  @FieldXPath("lienCollecteDechetsHuile")
  String getLienCollecteDechetsHuile();

  /**
   * Getter de l'attribut lienCollecteDechetsPneus.
   * 
   * @return la valeur de lienCollecteDechetsPneus
   */
  @FieldXPath("lienCollecteDechetsPneus")
  String getLienCollecteDechetsPneus();

  /**
   * Getter de l'attribut lienControleurAscenceurLE.
   * 
   * @return la valeur de lienControleurAscenceurLE
   */
  @FieldXPath("lienControleurAscenceurLE")
  String getLienControleurAscenceurLE();

  /**
   * Getter de l'attribut lienControleurAscenceurLPS.
   * 
   * @return la valeur de lienControleurAscenceurLPS
   */
  @FieldXPath("lienControleurAscenceurLPS")
  String getLienControleurAscenceurLPS();

  /**
   * Getter de l'attribut lienDebitTabac.
   * 
   * @return la valeur de lienDebitTabac
   */
  @FieldXPath("lienDebitTabac")
  String getLienDebitTabac();

  /**
   * Getter de l'attribut lienDiagnostiqueurImmobilierLE.
   * 
   * @return la valeur de lienDiagnostiqueurImmobilierLE
   */
  @FieldXPath("lienDiagnostiqueurImmobilierLE")
  String getLienDiagnostiqueurImmobilierLE();

  /**
   * Getter de l'attribut lienDiagnostiqueurImmobilierLPS.
   * 
   * @return la valeur de lienDiagnostiqueurImmobilierLPS
   */
  @FieldXPath("lienDiagnostiqueurImmobilierLPS")
  String getLienDiagnostiqueurImmobilierLPS();

  /**
   * Getter de l'attribut lienEditeur.
   * 
   * @return la valeur de lienEditeur
   */
  @FieldXPath("lienEditeur")
  String getLienEditeur();

  /**
   * Getter de l'attribut lienExplorationPlateauContinentalLE.
   * 
   * @return la valeur de lienExplorationPlateauContinentalLE
   */
  @FieldXPath("lienExplorationPlateauContinentalLE")
  String getLienExplorationPlateauContinentalLE();

  /**
   * Getter de l'attribut lienExplorationPlateauContinentalLPS.
   * 
   * @return la valeur de lienExplorationPlateauContinentalLPS
   */
  @FieldXPath("lienExplorationPlateauContinentalLPS")
  String getLienExplorationPlateauContinentalLPS();

  /**
   * Getter de l'attribut lienGenieClimatiqueLE.
   * 
   * @return la valeur de lienGenieClimatiqueLE
   */
  @FieldXPath("lienGenieClimatiqueLE")
  String getLienGenieClimatiqueLE();

  /**
   * Getter de l'attribut lienGenieClimatiqueLPS.
   * 
   * @return la valeur de lienGenieClimatiqueLPS
   */
  @FieldXPath("lienGenieClimatiqueLPS")
  String getLienGenieClimatiqueLPS();

  /**
   * Getter de l'attribut lienGestionDechets.
   * 
   * @return la valeur de lienGestionDechets
   */

  @FieldXPath("lienGestionDechets")
  String getLienGestionDechets();

  /**
   * Getter de l'attribut lienationPeriodiques.
   * 
   * @return la valeur de lienationPeriodiques
   */
  @FieldXPath("lienationPeriodiques")
  String getLienationPeriodiques();

  /**
   * Getter de l'attribut lienTerrassement.
   * 
   * @return la valeur de lienTerrassement
   */
  @FieldXPath("lienTerrassement")
  String getLienTerrassement();

  /**
   * Getter de l'attribut lienTraitementDechetsHuile.
   * 
   * @return la valeur de lienTraitementDechetsHuile
   */
  @FieldXPath("lienTraitementDechetsHuile")
  String getLienTraitementDechetsHuile();

  /**
   * Getter de l'attribut lienTraitementDechetsPneus.
   * 
   * @return la valeur de lienTraitementDechetsPneus
   */
  @FieldXPath("lienTraitementDechetsPneus")
  String getLienTraitementDechetsPneus();

  /**
   * Get le mail.
   *
   * @return the mail
   */
  @FieldXPath("mail")
  String getMail();

  /**
   * Get le nom.
   *
   * @return the nom
   */
  @FieldXPath("nom")
  String getNom();

  /**
   * Get le observation.
   *
   * @return the observation
   */
  @FieldXPath("observation")
  String getObservation();

  /**
   * getPriseConnaissance.
   * 
   * @return priseConnaissance
   */
  @FieldXPath("priseConnaissance")
  String getPriseConnaissance();

  /**
   * Get le site.
   *
   * @return the site
   */
  @FieldXPath("site")
  String getSite();

  /**
   * Get le telephone.
   *
   * @return the telephone
   */
  @FieldXPath("telephone")
  String getTelephone();

  /**
   * Set le adresse1.
   *
   * @param adresse1
   *          the adresse1 to set
   */
  void setAdresse1(String adresse1);

  /**
   * Set le adresse2.
   *
   * @param adresse2
   *          the adresse2 to set
   */
  void setAdresse2(String adresse2);

  /**
   * Set le adresse3.
   *
   * @param adresse3
   *          the adresse3 to set
   */
  void setAdresse3(String adresse3);

  /**
   * Setter de l'attribut code.
   * 
   * @param code
   *          la nouvelle valeur de code
   */
  void setCode(String code);

  /**
   * Set le fax.
   *
   * @param fax
   *          the fax to set
   */
  void setFax(String fax);

  /**
   * Setter de l'attribut labelActivitesSimplifiees.
   * 
   * @param labelActivitesSimplifiees
   *          la nouvelle valeur de labelActivitesSimplifiees
   */
  void setLabelActivitesSimplifiees(String labelActivitesSimplifiees);

  /**
   * Setter de l'attribut lienAgencePresseLE.
   * 
   * @param lienAgencePresseLE
   *          la nouvelle valeur de lienAgencePresseLE
   */
  void setLienAgencePresseLE(String lienAgencePresseLE);

  /**
   * Setter de l'attribut lienAgencePresseLPS.
   * 
   * @param lienAgencePresseLPS
   *          la nouvelle valeur de lienAgencePresseLPS
   */
  void setLienAgencePresseLPS(String lienAgencePresseLPS);

  /**
   * Setter de l'attribut lienAgentVoyageLE.
   * 
   * @param lienAgentVoyageLE
   *          la nouvelle valeur de lienAgentVoyageLE
   */
  void setLienAgentVoyageLE(String lienAgentVoyageLE);

  /**
   * Setter de l'attribut lienAgentVoyageLPS.
   * 
   * @param lienAgentVoyageLPS
   *          la nouvelle valeur de lienAgentVoyageLPS
   */
  void setLienAgentVoyageLPS(String lienAgentVoyageLPS);

  /**
   * Setter de l'attribut lienCollecteDechetsHuile.
   * 
   * @param lienCollecteDechetsHuile
   *          la nouvelle valeur de lienCollecteDechetsHuile
   */
  void setLienCollecteDechetsHuile(String lienCollecteDechetsHuile);

  /**
   * Setter de l'attribut lienCollecteDechetsPneus.
   * 
   * @param lienCollecteDechetsPneus
   *          la nouvelle valeur de lienCollecteDechetsPneus
   */
  void setLienCollecteDechetsPneus(String lienCollecteDechetsPneus);

  /**
   * Setter de l'attribut lienControleurAscenceurLE.
   * 
   * @param lienControleurAscenceurLE
   *          la nouvelle valeur de lienControleurAscenceurLE
   */
  void setLienControleurAscenceurLE(String lienControleurAscenceurLE);

  /**
   * Setter de l'attribut lienControleurAscenceurLPS.
   * 
   * @param lienControleurAscenceurLPS
   *          la nouvelle valeur de lienControleurAscenceurLPS
   */
  void setLienControleurAscenceurLPS(String lienControleurAscenceurLPS);

  /**
   * Setter de l'attribut lienDebitTabac.
   * 
   * @param lienDebitTabac
   *          la nouvelle valeur de lienDebitTabac
   */
  void setLienDebitTabac(String lienDebitTabac);

  /**
   * Setter de l'attribut lienDiagnostiqueurImmobilierLE.
   * 
   * @param lienDiagnostiqueurImmobilierLE
   *          la nouvelle valeur de lienDiagnostiqueurImmobilierLE
   */
  void setLienDiagnostiqueurImmobilierLE(String lienDiagnostiqueurImmobilierLE);

  /**
   * Setter de l'attribut lienDiagnostiqueurImmobilierLPS.
   * 
   * @param lienDiagnostiqueurImmobilierLPS
   *          la nouvelle valeur de lienDiagnostiqueurImmobilierLPS
   */
  void setLienDiagnostiqueurImmobilierLPS(String lienDiagnostiqueurImmobilierLPS);

  /**
   * Setter de l'attribut lienEditeur.
   * 
   * @param lienEditeur
   *          la nouvelle valeur de lienEditeur
   */
  void setLienEditeur(String lienEditeur);

  /**
   * Setter de l'attribut lienExplorationPlateauContinentalLE.
   * 
   * @param lienExplorationPlateauContinentalLE
   *          la nouvelle valeur de lienExplorationPlateauContinentalLE
   */
  void setLienExplorationPlateauContinentalLE(String lienExplorationPlateauContinentalLE);

  /**
   * Setter de l'attribut lienExplorationPlateauContinentalLPS.
   * 
   * @param lienExplorationPlateauContinentalLPS
   *          la nouvelle valeur de lienExplorationPlateauContinentalLPS
   */
  void setLienExplorationPlateauContinentalLPS(String lienExplorationPlateauContinentalLPS);

  /**
   * Setter de l'attribut lienGenieClimatiqueLE.
   * 
   * @param lienGenieClimatiqueLE
   *          la nouvelle valeur de lienGenieClimatiqueLE
   */
  void setLienGenieClimatiqueLE(String lienGenieClimatiqueLE);

  /**
   * Setter de l'attribut lienGenieClimatiqueLPS.
   * 
   * @param lienGenieClimatiqueLPS
   *          la nouvelle valeur de lienGenieClimatiqueLPS
   */
  void setLienGenieClimatiqueLPS(String lienGenieClimatiqueLPS);

  /**
   * Setter de l'attribut lienGestionDechets.
   * 
   * @param lienGestionDechets
   *          la nouvelle valeur de lienGestionDechets
   */
  void setLienGestionDechets(String lienGestionDechets);

  /**
   * Setter de l'attribut lienationPeriodiques.
   * 
   * @param lienationPeriodiques
   *          la nouvelle valeur de lienationPeriodiques
   */
  void setLienationPeriodiques(String lienationPeriodiques);

  /**
   * Setter de l'attribut lienTerrassement.
   * 
   * @param lienTerrassement
   *          la nouvelle valeur de lienTerrassement
   */
  void setLienTerrassement(String lienTerrassement);

  /**
   * Setter de l'attribut lienTraitementDechetsHuile.
   * 
   * @param lienTraitementDechetsHuile
   *          la nouvelle valeur de lienTraitementDechetsHuile
   */
  void setLienTraitementDechetsHuile(String lienTraitementDechetsHuile);

  /**
   * Setter de l'attribut lienTraitementDechetsPneus.
   * 
   * @param lienTraitementDechetsPneus
   *          la nouvelle valeur de lienTraitementDechetsPneus
   */
  void setLienTraitementDechetsPneus(String lienTraitementDechetsPneus);

  /**
   * Set le mail.
   *
   * @param mail
   *          the mail to set
   */
  void setMail(String mail);

  /**
   * Set le nom.
   *
   * @param nom
   *          the nom to set
   */
  void setNom(String nom);

  /**
   * Set le observation.
   *
   * @param observation
   *          the observation to set
   */
  void setObservation(String observation);

  /**
   * setPriseConnaissance.
   * 
   * @param priseConnaissance
   *          priseConnaissance
   */
  void setPriseConnaissance(String priseConnaissance);

  /**
   * Set le site.
   *
   * @param site
   *          the site to set
   */
  void setSite(String site);

  /**
   * Set le telephone.
   *
   * @param telephone
   *          the telephone to set
   */
  void setTelephone(String telephone);

}
