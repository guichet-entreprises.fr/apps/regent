package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * L'interface JGM.
 */
@ResourceXPath("/JGM")
public interface IJGM {

  /**
   * Get le j00.
   *
   * @return le j00
   */
  @FieldXPath("J00")
  String getJ00();

  /**
   * Set le j00.
   *
   * @param j00
   *          le nouveau j00
   */
  void sgetJ00(String j00);
}
