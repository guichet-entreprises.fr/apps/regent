package fr.ge.common.regent.bean.xml;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;

/**
 * Le Class TypeFluxXml.
 */
public class TypeFluxXml implements Comparable < TypeFluxXml > {

  /** chaque role pour plusieurs evenement */
  private Map < String, List < String > > mapRoleEvenement;

  /** Le poid. */
  private Integer poid;

  /** Le type de flux. */
  private String typeDeFlux;

  /** le type d'opération (modif/cess/regul.) */
  private String typeOperation;

  /** Le role de flux. */
  private String roleDeFlux;

  /** Le evenements. */
  private List < String > evenements;

  /**
   * Instancie un nouveau type flux xml.
   */
  public TypeFluxXml() {
    super();
  }

  /**
   * Instancie un nouveau type flux xml.
   *
   * @param evenements
   *          le evenements
   * @param poid
   *          le poid
   * @param typeDeFlux
   *          le type de flux
   * @param roleFlux
   *          le role flux
   */
  public TypeFluxXml(Map < String, List < String > > mapRoleEvenement, Integer poid, String typeDeFlux, String typeOperation,
    String roleFlux, List < String > evenements) {
    super();
    this.mapRoleEvenement = mapRoleEvenement;
    this.poid = poid;
    this.typeDeFlux = typeDeFlux;
    this.roleDeFlux = roleFlux;
    this.typeOperation = typeOperation;
    this.evenements = evenements;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((evenements == null) ? 0 : evenements.hashCode());
    result = prime * result + ((mapRoleEvenement == null) ? 0 : mapRoleEvenement.hashCode());
    result = prime * result + ((poid == null) ? 0 : poid.hashCode());
    result = prime * result + ((roleDeFlux == null) ? 0 : roleDeFlux.hashCode());
    result = prime * result + ((typeDeFlux == null) ? 0 : typeDeFlux.hashCode());
    result = prime * result + ((typeOperation == null) ? 0 : typeOperation.hashCode());
    return result;
  }

  /**
   * {@inheritDoc}
   */
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (!(obj instanceof TypeFluxXml)) {
      return false;
    }
    TypeFluxXml other = (TypeFluxXml) obj;
    return new EqualsBuilder().append(evenements, other.evenements).append(mapRoleEvenement, other.mapRoleEvenement)
      .append(poid, other.poid).append(roleDeFlux, other.roleDeFlux).append(typeDeFlux, other.typeDeFlux)
      .append(typeOperation, other.typeOperation).isEquals();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    return "TypeFluxXml [mapRoleEvenement=" + mapRoleEvenement + ", poid=" + poid + ", typeDeFlux=" + typeDeFlux
      + ", typeOperation=" + typeOperation + ", roleDeFlux=" + roleDeFlux + ", evenements=" + evenements + "]";
  }

  /**
   * Get le poid.
   *
   * @return le poid
   */
  public Integer getPoid() {
    return poid;
  }

  /**
   * Set le poid.
   *
   * @param poid
   *          le nouveau poid
   */
  public void setPoid(Integer poid) {
    this.poid = poid;
  }

  /**
   * Get le type de flux.
   *
   * @return le type de flux
   */
  public String getTypeDeFlux() {
    return typeDeFlux;
  }

  /**
   * Set le type de flux.
   *
   * @param typeDeFlux
   *          le nouveau type de flux
   */
  public void setTypeDeFlux(String typeDeFlux) {
    this.typeDeFlux = typeDeFlux;
  }

  /**
   * Get le role de flux.
   *
   * @return le role de flux
   */
  public String getRoleDeFlux() {
    return roleDeFlux;
  }

  /**
   * Set le role de flux.
   *
   * @param roleDeFlux
   *          le nouveau role de flux
   */
  public void setRoleDeFlux(String roleDeFlux) {
    this.roleDeFlux = roleDeFlux;
  }

  /**
   * {@inheritDoc}
   */
  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Comparable#compareTo(java.lang.Object)
   */
  @Override
  public int compareTo(TypeFluxXml o) {
    return this.poid.compareTo(o.poid);
  }

  /**
   * Accesseur sur l'attribut map role evenement.
   *
   * @return map role evenement
   */
  public Map < String, List < String > > getMapRoleEvenement() {
    return mapRoleEvenement;
  }

  /**
   * Sets the map role evenement.
   *
   * @param mapRoleEvenement
   *          map role evenement
   */
  public void setMapRoleEvenement(Map < String, List < String > > mapRoleEvenement) {
    this.mapRoleEvenement = mapRoleEvenement;
  }

  /**
   * Accesseur sur l'attribut evenements.
   *
   * @return evenements
   */
  public List < String > getEvenements() {
    return evenements;
  }

  /**
   * Mutateur sur l'attribut evenements.
   *
   * @param evenements
   *          le nouveau evenements
   */
  public void setEvenements(List < String > evenements) {
    this.evenements = evenements;
  }

  /**
   * Accesseur sur l'attribut type operation.
   *
   * @return type operation
   */
  public String getTypeOperation() {
    return typeOperation;
  }

  /**
   * Mutateur sur l'attribut type operation.
   *
   * @param typeOperation
   *          le nouveau type operation
   */
  public void setTypeOperation(String typeOperation) {
    this.typeOperation = typeOperation;
  }

}
