/*
 * 
 */
package fr.ge.common.regent.service.referentiel;

import java.util.List;
import java.util.Map;

import fr.ge.common.regent.bean.xml.TypeFluxXml;
import fr.ge.common.regent.constante.TypeDossierEnum;
import fr.ge.common.regent.constante.enumeration.DestinataireXmlRegentEnum;

/**
 * Interface représentant le service d'accès aux référentiels. Cette classe est
 * l'interface fille des interfaces d'accès aux différents référentiels. <b>
 * Elle est le point d'accès direct aux méthodes de lecture inter-domaines.</b>
 * <br/>
 * <br/>
 * Les méthodes ci-dessous sont destinées à la lecture des tables non
 * catégorisées dans les sous-domaines désignés.
 * 
 * @author $Author: fbeaurai $
 * @version $Revision: 0 $
 */
public interface ReferentielService extends ReferentielReglesService {

    /**
     * Détermine, à partir des événements et du destinataire, le nombre de flux
     * a émettre. Pour chaque destinataire réseau, on recherche en fonction des
     * événements les différents types de flux (par exemple pour les événement
     * 24p, 16p & 17p et pour le destinataire CMA, on a deux flux : Z et M) pour
     * un type de dossier donnée.
     * 
     * L’ordonnancement des flux est déterminé par le poid. Pour 2 évenement
     * ayant le meme flux, le poids pris en compte pour le flux est celui de
     * l'évenement dont le poids est plus prioritaire.
     * 
     * @param destinataire
     *            : destinataire du flux.
     * @param evenement
     *            : Liste des évenements concernés.
     * @param typeDossier
     *            : TypeDossierEnum C=Création, R=Régularisation,
     *            M=Modification, F=Cessation
     * 
     * @return Liste des flux a émettre
     */
    List<TypeFluxXml> listerFluxParDestinataireEtEvenements(DestinataireXmlRegentEnum destinataire, List<String> evenement, TypeDossierEnum typeDossier);

    /**
     * Accesseur sur l'attribut {@link #rubrique oblogatoire ou conditionnelle}.
     *
     * @param version
     *            la version de l'xmlRegnet
     * @param evenements
     *            la liste des evenemets
     * @return une map contient rubrique==> conditionnelle obligatoire
     */
    Map<String, Boolean> getRubriqueOblogatoireOuConditionnelle(String version, List<String> evenements);

}
