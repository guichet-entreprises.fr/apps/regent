/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 */

package fr.ge.common.liasse.dao;

/**
 * Liasse Number DAO
 * 
 * to get the next sequence number to generate the liasse number
 * 
 * @author mtakerra
 *
 */
public interface LiasseNumberDao {

    /**
     * get the next value to generate the liasse number.
     * 
     * @return next value of the sequence
     */

    
    Long nextVal();
}
