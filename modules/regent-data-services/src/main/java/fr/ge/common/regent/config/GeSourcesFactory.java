package fr.ge.common.regent.config;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.regent.core.input.InputSource;
import fr.ge.common.regent.core.input.XmlElementInputSource;
import fr.ge.common.regent.service.referentiel.ReferentielService;

/**
 * Une factory pour creer l'objet GipgeSources.
 */
public class GeSourcesFactory {

    /** La constante REF_DEPARTEMENT. */
    protected static final String REF_DEPARTEMENT = "refDepartement";

    /** La constante REF_GREFFE. */
    protected static final String REF_GREFFE = "refGreffe";

    /** La constante REF_NATIONALITE. */
    protected static final String REF_NATIONALITE = "refNationalite";

    /** La constante REF_PAYS. */
    protected static final String REF_PAYS = "refPays";

    /** La constante REF_PAYS_EUROPEENS. */
    protected static final String REF_PAYS_EUROPEENS = "refPaysEuropeens";

    /** La constante REF_PREFECTURE. */
    protected static final String REF_PREFECTURE = "refPrefecture";

    /** La constante REF_TYPE_VOIE. */
    protected static final String REF_TYPE_VOIE = "refTypeVoie";

    /** La constante REF_SECTEURS. */
    protected static final String REF_SECTEURS = "refSecteurs";

    /** La constante REF_DOMAINES. */
    protected static final String REF_DOMAINES = "refDomaines";

    /** La constante REF_DOMAINES_RSI. */
    protected static final String REF_DOMAINES_RSI = "domaineRsi";

    /** La constante REF_DOMAINES_RSI_V2 avec domaine non trouvé. */
    protected static final String REF_DOMAINES_RSI_V2 = "domaineRsiV2";

    /** La constante GROUP_REGENT. */
    protected static final String GROUP_REGENT = "regent";

    /** Le ref service. */
    @Autowired
    private ReferentielService refService;

    /** Le xml sources. */
    protected final List<InputSource> xmlSources;

    /**
     * Instancie un nouveau gipge sources factory.
     */
    public GeSourcesFactory() {
        this.xmlSources = new ArrayList<InputSource>();

    }

    /**
     * Ajoute le regent from db.
     */
    protected void addRegentFromDb() {
        this.xmlSources.add(new XmlElementInputSource(GROUP_REGENT, this.refService.recupererToutesLesReglesRegent()));
    }

    /**
     * Get le xml sources.
     * 
     * @return le xml sources
     */
    public List<InputSource> getXmlSources() {
        return this.xmlSources;
    }

    /**
     * Initialise le.
     * 
     * @throws IOException
     *             Signale l'apparition d'une I/O exception.
     * @throws URISyntaxException
     *             le URI syntax exception
     */
    public void init() throws IOException, URISyntaxException {
        this.addRegentFromDb();
    }

    /**
     * Set le ref service.
     * 
     * @param refService
     *            the refService to set
     */
    public void setRefService(final ReferentielService refService) {
        this.refService = refService;
    }

}
