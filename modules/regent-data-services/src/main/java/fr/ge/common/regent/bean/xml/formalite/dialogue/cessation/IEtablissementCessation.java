package fr.ge.common.regent.bean.xml.formalite.dialogue.cessation;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

import fr.ge.common.regent.bean.xml.formalite.IFormalite;
import fr.ge.common.regent.bean.xml.transverse.IAdresse;

/**
 * Le Interface IEtablissementCessation.
 */
@ResourceXPath("/formalite")
public interface IEtablissementCessation extends IFormalite {

    /**
     * Get l' adresse de l'etablissement.
     *
     * @return adresse
     */
    @FieldXPath(value = "adresse")
    IAdresse getAdresse();

    /**
     * New adresse1.
     *
     * @return le i adresse
     */
    IAdresse newAdresse();

    /**
     * Set le adresse.
     *
     * @param adresse
     *            le nouveau adresse
     */
    void setAdresse(IAdresse adresse);

    /**
     * Get destinationEtablissement.
     *
     * @return destinationEtablissement
     */
    @FieldXPath(value = "destinationEtablissement")
    String getDestinationEtablissement();

    /**
     * Get autre destinationAutre.
     *
     * @return adresse
     */

    @FieldXPath(value = "destinationAutre")
    String getDestinationAutre();

    /**
     * Set le destination etablissement principal.
     *
     * @param destinationEtablissement
     *            the destinationEtablissement to set
     */
    void setDestinationEtablissement(final String destinationEtablissement);

    /**
     * 
     * Set le destination autre.
     * 
     * @param destinationAutre
     */

    void setDestinationAutre(final String destinationAutre);

}
