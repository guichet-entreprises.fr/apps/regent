package fr.ge.common.regent.bean.formalite.dialogue.creation;

import fr.ge.common.regent.bean.formalite.dialogue.AbstractFormalite;

/**
 * Le Class FormaliteBean.
 */
public class DialogueCreationBean extends AbstractFormalite<EntrepriseBean> implements IFormaliteVue {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 658916298533640748L;

    /** Le est auto entrepreneur. */
    private boolean estAutoEntrepreneur;

    /** Le est eirl. */
    private boolean estEirl;

    /** Le is aqpa. */
    private boolean isAqpa;

    /** Le user id. */
    private Long userId;

    /** Le terminee. */
    private boolean terminee;

    /** Le code commune activite. */
    private String codeCommuneActivite;

    /**
     * Get le code commune activite.
     * 
     * @return the codeCommuneActivite
     */
    public String getCodeCommuneActivite() {
        return this.codeCommuneActivite;
    }

    /**
     * Set le code commune activite.
     * 
     * @param codeCommuneActivite
     *            the codeCommuneActivite to set
     */
    public void setCodeCommuneActivite(String codeCommuneActivite) {
        this.codeCommuneActivite = codeCommuneActivite;
    }

    /**
     * Verifie si c'est terminee.
     * 
     * @return the terminee
     */
    public boolean isTerminee() {
        return terminee;
    }

    /**
     * Set le terminee.
     * 
     * @param terminee
     *            the terminee to set
     */
    public void setTerminee(boolean terminee) {
        this.terminee = terminee;
    }

    /**
     * Getter de l'attribut aqpa.
     * 
     * @return la valeur de aqpa
     */
    public boolean isAqpa() {
        return this.isAqpa;
    }

    /**
     * Getter de l'attribut estAutoEntrepreneur.
     * 
     * @return la valeur de estAutoEntrepreneur
     */
    public boolean isEstAutoEntrepreneur() {
        return this.estAutoEntrepreneur;
    }

    /**
     * Getter de l'attribut estEirl.
     * 
     * @return la valeur de estEirl
     */
    public boolean isEstEirl() {
        return this.estEirl;
    }

    /**
     * Getter de l'attribut isAqpa.
     * 
     * @return la valeur de isAqpa
     */
    public boolean isIsAqpa() {
        return this.isAqpa;
    }

    /**
     * Setter de l'attribut aqpa.
     * 
     * @param isAqpa
     *            la nouvelle valeur de aqpa
     */
    public void setAqpa(boolean isAqpa) {
        this.isAqpa = isAqpa;
    }

    /**
     * Setter de l'attribut estAutoEntrepreneur.
     * 
     * @param estAutoEntrepreneur
     *            la nouvelle valeur de estAutoEntrepreneur
     */
    public void setEstAutoEntrepreneur(boolean estAutoEntrepreneur) {
        this.estAutoEntrepreneur = estAutoEntrepreneur;
    }

    /**
     * Setter de l'attribut estEirl.
     * 
     * @param estEirl
     *            la nouvelle valeur de estEirl
     */
    public void setEstEirl(boolean estEirl) {
        this.estEirl = estEirl;
    }

    /**
     * Setter de l'attribut isAqpa.
     * 
     * @param isAqpa
     *            la nouvelle valeur de isAqpa
     */
    public void setIsAqpa(boolean isAqpa) {
        this.isAqpa = isAqpa;
    }

    /**
     * Get le user id.
     *
     * @return the userId
     */
    public Long getUserId() {
        return this.userId;
    }

    /**
     * Set le user id.
     *
     * @param userId
     *            the userId to set
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EntrepriseBean createNewEntreprise() {

        return new EntrepriseBean();
    }

}
