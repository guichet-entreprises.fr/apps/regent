package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IE21.
 */
@ResourceXPath("/E21")
public interface IE21 {

  /**
   * Get le e211.
   *
   * @return le e211
   */
  @FieldXPath("E21.1")
  String getE211();

  /**
   * Set le e211.
   *
   * @param E211
   *          le nouveau e211
   */
  void setE211(String E211);

}
