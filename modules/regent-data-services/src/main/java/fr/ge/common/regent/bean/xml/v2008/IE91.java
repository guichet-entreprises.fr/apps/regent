package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IE91.
 */
@ResourceXPath("/E91")
public interface IE91 {

  /**
   * Get le e911.
   *
   * @return le e911
   */
  @FieldXPath("E91.1")
  String getE911();

  /**
   * Set le e911.
   *
   * @param E911
   *          le nouveau e911
   */
  void setE911(String E911);

  /**
   * Get le e912.
   *
   * @return le e912
   */
  @FieldXPath("E91.2")
  String getE912();

  /**
   * Set le e912.
   *
   * @param E912
   *          le nouveau e912
   */
  void setE912(String E912);

}
