package fr.ge.common.regent.service;

import java.lang.reflect.InvocationTargetException;
import java.util.LinkedHashMap;
import java.util.List;

import org.xmlfield.core.exception.XmlFieldParsingException;

import fr.ge.common.regent.service.exception.XmlFatalException;
import fr.ge.common.regent.service.exception.XmlInvalidException;
import fr.ge.common.regent.ws.v1.bean.RegentResultBean;
import fr.ge.common.regent.xml.XmlFieldElement;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * Le Interface ServiceGeneationRegentFromDB.
 */
public interface ServiceGeneationRegentFromDB {

    /**
     * Generate the XML Regent.
     *
     * @param listAuthoritiesCode
     *            the authority code as a list
     * @param profil
     *            the profil as an object
     * @param formalite
     *            the formality as an object
     * @param typeFormalite
     *            the record type identifier
     * @param numeroDossier
     *            the record code identifier
     * @param codeEdi
     *            the authority code
     * @param roleDestinataire
     *            the authority role
     * @param evenements
     *            the evenements
     * @param version
     *            the regent version use for generation
     * @param numeroLiasse
     *            the liasse
     * @param typeFlux
     *            the type flux
     * @return the regent xml as text
     * @throws TechnicalException
     *             le erreur traitement exception
     * @throws NoSuchMethodException
     *             le no such method exception
     * @throws IllegalAccessException
     *             le illegal access exception
     * @throws InvocationTargetException
     *             le invocation target exception
     * @throws XmlFieldParsingException
     *             le xml field parsing exception
     */
    String getRegentXmlCap(List<String> listAuthoritiesCode, Object profil, Object formalite, String typeFormalite, String codeEdi, String roleDestinataire, List<String> evenements, String version,
            String numeroLiasse, String typeFlux) throws TechnicalException, SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException,
            XmlFieldParsingException, XmlInvalidException, XmlFatalException;

    /**
     * Generate the XML Regent.
     *
     * @param listAuthoritiesCode
     *            the authority code as a list
     * @param profil
     *            the profil as JSON
     * @param formalite
     *            the formality as JSON
     * @param numeroDossier
     *            the record code identifier
     * @param codeEdi
     *            the authority code
     * @param roleDestinataire
     *            the authority role
     * @param evenements
     *            the evenements
     * @param version
     *            the regent version use for generation
     * @param numeroLiasse
     *            the liasse
     * @param typeFlux
     *            the type flux
     * @return the regent xml as text
     * @throws TechnicalException
     *             le erreur traitement exception
     */
    RegentResultBean getRegentXmlCap(List<String> listAuthoritiesCode, String profil, String formalite, String codeEdi, String roleDestinataire, List<String> evenements, String version,
            String numeroLiasse, String typeFlux) throws TechnicalException;

    /**
     * Validate an XML-regent for a given version according to its XSD (throws a
     * TechnicalException in case of failed validation).
     * 
     * @param version
     *            the version of the XML-regent
     * @param regentXml
     *            the XML-REGENT to validate.
     */
    public void validateXmlRegent(String version, String regentXml);

    /**
     * Returns the tags that must contain the XML-REGENT with the received
     * values or bourrage values in case of mandatory null tags
     * 
     * @param codeEdi
     *            Edi code of the recipient
     * @param roleDestinataire
     *            role of the recipient
     * @param evenements
     *            list of events contained in the formality
     * @param version
     *            version of the XML-REGENT
     * @param numeroLiasse
     *            generated code associated with the xml-regent to be sent
     * @param mappedRegent
     *            a Map<tag, value> of tags to generate in the XML-REGENT
     * @return a Map<Xpath, value> Xpath of tags with the value to insert in
     */
    public LinkedHashMap<String, Object> fillMandatoryTags(String codeEdi, String roleDestinataire, List<String> evenements, String version, String numeroLiasse,
            LinkedHashMap<String, Object> mappedRegent);

    /**
     * Returns the tags by events and xml-regent version
     * 
     * @param evenements
     *            list of events for which we generate the lest of tags
     * @param version
     *            the version of the xml rgeent (V2008.11, V2016.02)
     * @return the list of tags generated
     */
    public List<XmlFieldElement> getTagsByEventsAndVersion(List<String> evenements, String version);
}
