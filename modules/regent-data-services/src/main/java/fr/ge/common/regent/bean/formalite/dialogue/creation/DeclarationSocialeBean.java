package fr.ge.common.regent.bean.formalite.dialogue.creation;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.format.annotation.NumberFormat;

import fr.ge.common.regent.bean.formalite.FormaliteVueBean;

/**
 * Le Class DeclarationSocialeBean.
 */
public class DeclarationSocialeBean extends FormaliteVueBean implements IFormaliteVue {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 5291582968342734081L;

    /** Le activite autre que declaree departement. */
    private String activiteAutreQueDeclareeDepartement;

    /** Le activite autre que declaree pays. */
    private String activiteAutreQueDeclareePays;

    /** Le activite autre que declaree presence. */
    private String activiteAutreQueDeclareePresence;

    /** Le activite autre que declaree statut. */
    private String activiteAutreQueDeclareeStatut;

    /** Le activite autre que declaree statut autre. */
    private String activiteAutreQueDeclareeStatutAutre;

    /** Le activite exercee anterieurement code geographique. */
    private String activiteExerceeAnterieurementCodeGeographique;

    /** Le activite exercee anterieurement commune. */
    private String activiteExerceeAnterieurementCommune;

    /** Le activite exercee anterieurement date cessation. */
    private String activiteExerceeAnterieurementDateCessation;

    /** Le activite exercee anterieurement departement. */
    private String activiteExerceeAnterieurementDepartement;

    /** Le activite exercee anterieurement libelle. */
    private String activiteExerceeAnterieurementLibelle;

    /** Le activite exercee anterieurement pays. */
    private String activiteExerceeAnterieurementPays;

    /** Le activite exercee anterieurement presence. */
    private String activiteExerceeAnterieurementPresence;

    /** Le activite exercee anterieurement siren. */
    private String activiteExerceeAnterieurementSIREN;

    /** Le affiliation msa. */
    private String affiliationMSA;

    /** Le ayant droit nombre. */
    @NumberFormat
    private Integer ayantDroitNombre;

    /** Le ayant droit presence. */
    private String ayantDroitPresence;

    /** Le ayant droits. */
    private List<AyantDroitBean> ayantDroits = new ArrayList<AyantDroitBean>();

    /** Le conjoint couvert assurance maladie1. */
    private String conjointCouvertAssuranceMaladie1;

    /** Le conjoint couvert assurance maladie2. */
    private String conjointCouvertAssuranceMaladie2;

    /** Le conjoint numero securite sociale. */
    private String conjointNumeroSecuriteSociale;

    /** Le demande dotation jeune agriculteur. */
    private String demandeDotationJeuneAgriculteur;

    /** Le departement organisme conventionne. */
    private String departementOrganismeConventionne;

    /** Le est beneficiaire rsarmi. */
    private String estBeneficiaireRSARMI;

    /** Le est jeune agriculteur. */
    private String estJeuneAgriculteur;

    /** Le est majeur hors ue. */
    private Boolean estMajeurHorsUE = Boolean.TRUE;

    /**
     * La date de naissance du dirigeant qui doit etre conforme à la date du NIR
     * (Num Sécu Sociale.
     */
    private String dateNaissanceDirigeant;

    /** Le numero securite sociale. */
    private String numeroSecuriteSociale;

    /** Le option micro social. */
    private String optionMicroSocial;

    /** Le option micro social versement. */
    private String optionMicroSocialVersement;

    /** Le organisme conventionne code. */
    private String organismeConventionneCode;

    /** Le organisme conventionne code reseau a. */
    private String organismeConventionneCodeReseauA;

    /** Le organisme conventionne code reseau non a. */
    private String organismeConventionneCodeReseauNonA;

    /** Le organisme conventionne libelle. */
    private String organismeConventionneLibelle;

    /** Le organisme servant pension. */
    private String organismeServantPension;

    /** Le regime assurance maladie. */
    private String regimeAssuranceMaladie;

    /** Le regime assurance maladie autre. */
    private String regimeAssuranceMaladieAutre;

    /** Le statut conjoint. */
    private String statutConjoint;

    /** Le titre sejour date expiration. */
    private String titreSejourDateExpiration;

    /** Le titre sejour lieu delivrance code. */
    private String titreSejourLieuDelivranceCode;

    /** Le titre sejour lieu delivrance commune. */
    private String titreSejourLieuDelivranceCommune;

    /**
     * Getter de l'attribut activiteAutreQueDeclareeDepartement.
     * 
     * @return la valeur de activiteAutreQueDeclareeDepartement
     */
    public String getActiviteAutreQueDeclareeDepartement() {
        return this.activiteAutreQueDeclareeDepartement;
    }

    /**
     * Getter de l'attribut activiteAutreQueDeclareePays.
     * 
     * @return la valeur de activiteAutreQueDeclareePays
     */
    public String getActiviteAutreQueDeclareePays() {
        return this.activiteAutreQueDeclareePays;
    }

    /**
     * Getter de l'attribut activiteAutreQueDeclareePresence.
     * 
     * @return la valeur de activiteAutreQueDeclareePresence
     */
    public String getActiviteAutreQueDeclareePresence() {
        return this.activiteAutreQueDeclareePresence;
    }

    /**
     * Getter de l'attribut activiteAutreQueDeclareeStatut.
     * 
     * @return la valeur de activiteAutreQueDeclareeStatut
     */
    public String getActiviteAutreQueDeclareeStatut() {
        return this.activiteAutreQueDeclareeStatut;
    }

    /**
     * Getter de l'attribut activiteAutreQueDeclareeStatutAutre.
     * 
     * @return la valeur de activiteAutreQueDeclareeStatutAutre
     */
    public String getActiviteAutreQueDeclareeStatutAutre() {
        return this.activiteAutreQueDeclareeStatutAutre;
    }

    /**
     * getActiviteExerceeAnterieurementCodeGeographique.
     * 
     * @return activiteExerceeAnterieurementCodeGeographique
     */
    public String getActiviteExerceeAnterieurementCodeGeographique() {
        return activiteExerceeAnterieurementCodeGeographique;
    }

    /**
     * Getter de l'attribut activiteExerceeAnterieurementCommune.
     * 
     * @return la valeur de activiteExerceeAnterieurementCommune
     */
    public String getActiviteExerceeAnterieurementCommune() {
        return this.activiteExerceeAnterieurementCommune;
    }

    /**
     * Getter de l'attribut activiteExerceeAnterieurementDateCessation.
     * 
     * @return la valeur de activiteExerceeAnterieurementDateCessation
     */
    public String getActiviteExerceeAnterieurementDateCessation() {
        return this.activiteExerceeAnterieurementDateCessation;
    }

    /**
     * Getter de l'attribut activiteExerceeAnterieurementDepartement.
     * 
     * @return la valeur de activiteExerceeAnterieurementDepartement
     */
    public String getActiviteExerceeAnterieurementDepartement() {
        return this.activiteExerceeAnterieurementDepartement;
    }

    /**
     * Getter de l'attribut activiteExerceeAnterieurementLibelle.
     * 
     * @return la valeur de activiteExerceeAnterieurementLibelle
     */
    public String getActiviteExerceeAnterieurementLibelle() {
        return this.activiteExerceeAnterieurementLibelle;
    }

    /**
     * Getter de l'attribut activiteExerceeAnterieurementPays.
     * 
     * @return la valeur de activiteExerceeAnterieurementPays
     */
    public String getActiviteExerceeAnterieurementPays() {
        return this.activiteExerceeAnterieurementPays;
    }

    /**
     * Getter de l'attribut activiteExerceeAnterieurementPresence.
     * 
     * @return la valeur de activiteExerceeAnterieurementPresence
     */
    public String getActiviteExerceeAnterieurementPresence() {
        return this.activiteExerceeAnterieurementPresence;
    }

    /**
     * Getter de l'attribut activiteExerceeAnterieurementSIREN.
     * 
     * @return la valeur de activiteExerceeAnterieurementSIREN
     */
    public String getActiviteExerceeAnterieurementSIREN() {
        return this.activiteExerceeAnterieurementSIREN;
    }

    /**
     * Getter de l'attribut affiliationMSA.
     * 
     * @return la valeur de affiliationMSA
     */
    public String getAffiliationMSA() {
        return this.affiliationMSA;
    }

    /**
     * Getter de l'attribut ayantDroitNombre.
     * 
     * @return la valeur de ayantDroitNombre
     */
    public Integer getAyantDroitNombre() {
        return this.ayantDroitNombre;
    }

    /**
     * Getter de l'attribut ayantDroitPresence.
     * 
     * @return la valeur de ayantDroitPresence
     */
    public String getAyantDroitPresence() {
        return this.ayantDroitPresence;
    }

    /**
     * Getter de l'attribut ayantDroits.
     * 
     * @return la valeur de ayantDroits
     */
    public List<AyantDroitBean> getAyantDroits() {
        return this.ayantDroits;
    }

    /**
     * Getter de l'attribut conjointCouvertAssuranceMaladie1.
     * 
     * @return la valeur de conjointCouvertAssuranceMaladie1
     */
    public String getConjointCouvertAssuranceMaladie1() {
        return this.conjointCouvertAssuranceMaladie1;
    }

    /**
     * Getter de l'attribut conjointCouvertAssuranceMaladie2.
     * 
     * @return la valeur de conjointCouvertAssuranceMaladie2
     */
    public String getConjointCouvertAssuranceMaladie2() {
        return this.conjointCouvertAssuranceMaladie2;
    }

    /**
     * Getter de l'attribut conjointNumeroSecuriteSociale.
     * 
     * @return la valeur de conjointNumeroSecuriteSociale
     */
    public String getConjointNumeroSecuriteSociale() {
        return this.conjointNumeroSecuriteSociale;
    }

    /**
     * Getter de l'attribut demandeDotationJeuneAgriculteur.
     * 
     * @return la valeur de demandeDotationJeuneAgriculteur
     */
    public String getDemandeDotationJeuneAgriculteur() {
        return this.demandeDotationJeuneAgriculteur;
    }

    /**
     * getDepartementOrganismeConventionne.
     * 
     * @return departementOrganismeConventionne
     */
    public String getDepartementOrganismeConventionne() {
        return this.departementOrganismeConventionne;
    }

    /**
     * Getter de l'attribut estBeneficiaireRSARMI.
     * 
     * @return la valeur de estBeneficiaireRSARMI
     */
    public String getEstBeneficiaireRSARMI() {
        return this.estBeneficiaireRSARMI;
    }

    /**
     * Getter de l'attribut estJeuneAgriculteur.
     * 
     * @return la valeur de estJeuneAgriculteur
     */
    public String getEstJeuneAgriculteur() {
        return this.estJeuneAgriculteur;
    }

    /**
     * Getter de l'attribut estMajeurHorsUE.
     * 
     * @return la valeur de estMajeurHorsUE
     */
    public Boolean getEstMajeurHorsUE() {
        return this.estMajeurHorsUE;
    }

    /**
     * Getter de l'attribut numeroSecuriteSociale.
     * 
     * @return la valeur de numeroSecuriteSociale
     */
    public String getNumeroSecuriteSociale() {
        return this.numeroSecuriteSociale;
    }

    /**
     * Getter de l'attribut optionMicroSocial.
     * 
     * @return la valeur de optionMicroSocial
     */
    public String getOptionMicroSocial() {
        return this.optionMicroSocial;
    }

    /**
     * Getter de l'attribut optionMicroSocialVersement.
     * 
     * @return la valeur de optionMicroSocialVersement
     */
    public String getOptionMicroSocialVersement() {
        return this.optionMicroSocialVersement;
    }

    /**
     * Getter de l'attribut organismeConventionneCode.
     * 
     * @return la valeur de organismeConventionneCode
     */
    public String getOrganismeConventionneCode() {
        return this.organismeConventionneCode;
    }

    /**
     * getOrganismeConventionneCodeReseauA.
     * 
     * @return organismeConventionneCodeReseauA
     */
    public String getOrganismeConventionneCodeReseauA() {
        return this.organismeConventionneCodeReseauA;
    }

    /**
     * getOrganismeConventionneCodeReseauNonA.
     * 
     * @return organismeConventionneCodeReseauNonA
     */
    public String getOrganismeConventionneCodeReseauNonA() {
        return this.organismeConventionneCodeReseauNonA;
    }

    /**
     * Getter de l'attribut organismeConventionneLibelle.
     * 
     * @return la valeur de organismeConventionneLibelle
     */
    public String getOrganismeConventionneLibelle() {
        return this.organismeConventionneLibelle;
    }

    /**
     * Getter de l'attribut organismeServantPension.
     * 
     * @return la valeur de organismeServantPension
     */
    public String getOrganismeServantPension() {
        return this.organismeServantPension;
    }

    /**
     * Getter de l'attribut regimeAssuranceMaladie.
     * 
     * @return la valeur de regimeAssuranceMaladie
     */
    public String getRegimeAssuranceMaladie() {
        return this.regimeAssuranceMaladie;
    }

    /**
     * Getter de l'attribut regimeAssuranceMaladieAutre.
     * 
     * @return la valeur de regimeAssuranceMaladieAutre
     */
    public String getRegimeAssuranceMaladieAutre() {
        return this.regimeAssuranceMaladieAutre;
    }

    /**
     * Getter de l'attribut statutConjoint.
     * 
     * @return la valeur de statutConjoint
     */
    public String getStatutConjoint() {
        return this.statutConjoint;
    }

    /**
     * Getter de l'attribut titreSejourDateExpiration.
     * 
     * @return la valeur de titreSejourDateExpiration
     */
    public String getTitreSejourDateExpiration() {
        return this.titreSejourDateExpiration;
    }

    /**
     * Getter de l'attribut titreSejourLieuDelivranceCode.
     * 
     * @return la valeur de titreSejourLieuDelivranceCode
     */
    public String getTitreSejourLieuDelivranceCode() {
        return this.titreSejourLieuDelivranceCode;
    }

    /**
     * Getter de l'attribut titreSejourLieuDelivranceCommune.
     * 
     * @return la valeur de titreSejourLieuDelivranceCommune
     */
    public String getTitreSejourLieuDelivranceCommune() {
        return this.titreSejourLieuDelivranceCommune;
    }

    /**
     * Setter de l'attribut activiteAutreQueDeclareeDepartement.
     * 
     * @param activiteAutreQueDeclareeDepartement
     *            la nouvelle valeur de activiteAutreQueDeclareeDepartement
     */
    public void setActiviteAutreQueDeclareeDepartement(String activiteAutreQueDeclareeDepartement) {
        this.activiteAutreQueDeclareeDepartement = activiteAutreQueDeclareeDepartement;
    }

    /**
     * Setter de l'attribut activiteAutreQueDeclareePays.
     * 
     * @param activiteAutreQueDeclareePays
     *            la nouvelle valeur de activiteAutreQueDeclareePays
     */
    public void setActiviteAutreQueDeclareePays(String activiteAutreQueDeclareePays) {
        this.activiteAutreQueDeclareePays = activiteAutreQueDeclareePays;
    }

    /**
     * Setter de l'attribut activiteAutreQueDeclareePresence.
     * 
     * @param activiteAutreQueDeclareePresence
     *            la nouvelle valeur de activiteAutreQueDeclareePresence
     */
    public void setActiviteAutreQueDeclareePresence(String activiteAutreQueDeclareePresence) {
        this.activiteAutreQueDeclareePresence = activiteAutreQueDeclareePresence;
    }

    /**
     * Setter de l'attribut activiteAutreQueDeclareeStatut.
     * 
     * @param activiteAutreQueDeclareeStatut
     *            la nouvelle valeur de activiteAutreQueDeclareeStatut
     */
    public void setActiviteAutreQueDeclareeStatut(String activiteAutreQueDeclareeStatut) {
        this.activiteAutreQueDeclareeStatut = activiteAutreQueDeclareeStatut;
    }

    /**
     * Setter de l'attribut activiteAutreQueDeclareeStatutAutre.
     * 
     * @param activiteAutreQueDeclareeStatutAutre
     *            la nouvelle valeur de activiteAutreQueDeclareeStatutAutre
     */
    public void setActiviteAutreQueDeclareeStatutAutre(String activiteAutreQueDeclareeStatutAutre) {
        this.activiteAutreQueDeclareeStatutAutre = activiteAutreQueDeclareeStatutAutre;
    }

    /**
     * setActiviteExerceeAnterieurementCodeGeographique.
     * 
     * @param activiteExerceeAnterieurementCodeGeographique
     *            activiteExerceeAnterieurementCodeGeographique
     */
    public void setActiviteExerceeAnterieurementCodeGeographique(String activiteExerceeAnterieurementCodeGeographique) {
        this.activiteExerceeAnterieurementCodeGeographique = activiteExerceeAnterieurementCodeGeographique;
    }

    /**
     * Setter de l'attribut activiteExerceeAnterieurementCommune.
     * 
     * @param activiteExerceeAnterieurementCommune
     *            la nouvelle valeur de activiteExerceeAnterieurementCommune
     */
    public void setActiviteExerceeAnterieurementCommune(String activiteExerceeAnterieurementCommune) {
        this.activiteExerceeAnterieurementCommune = activiteExerceeAnterieurementCommune;
    }

    /**
     * Setter de l'attribut activiteExerceeAnterieurementDateCessation.
     * 
     * @param activiteExerceeAnterieurementDateCessation
     *            la nouvelle valeur de
     *            activiteExerceeAnterieurementDateCessation
     */
    public void setActiviteExerceeAnterieurementDateCessation(String activiteExerceeAnterieurementDateCessation) {
        this.activiteExerceeAnterieurementDateCessation = activiteExerceeAnterieurementDateCessation;
    }

    /**
     * Setter de l'attribut activiteExerceeAnterieurementDepartement.
     * 
     * @param activiteExerceeAnterieurementDepartement
     *            la nouvelle valeur de activiteExerceeAnterieurementDepartement
     */
    public void setActiviteExerceeAnterieurementDepartement(String activiteExerceeAnterieurementDepartement) {
        this.activiteExerceeAnterieurementDepartement = activiteExerceeAnterieurementDepartement;
    }

    /**
     * Setter de l'attribut activiteExerceeAnterieurementLibelle.
     * 
     * @param activiteExerceeAnterieurementLibelle
     *            la nouvelle valeur de activiteExerceeAnterieurementLibelle
     */
    public void setActiviteExerceeAnterieurementLibelle(String activiteExerceeAnterieurementLibelle) {
        this.activiteExerceeAnterieurementLibelle = activiteExerceeAnterieurementLibelle;
    }

    /**
     * Setter de l'attribut activiteExerceeAnterieurementPays.
     * 
     * @param activiteExerceeAnterieurementPays
     *            la nouvelle valeur de activiteExerceeAnterieurementPays
     */
    public void setActiviteExerceeAnterieurementPays(String activiteExerceeAnterieurementPays) {
        this.activiteExerceeAnterieurementPays = activiteExerceeAnterieurementPays;
    }

    /**
     * Setter de l'attribut activiteExerceeAnterieurementPresence.
     * 
     * @param activiteExerceeAnterieurementPresence
     *            la nouvelle valeur de activiteExerceeAnterieurementPresence
     */
    public void setActiviteExerceeAnterieurementPresence(String activiteExerceeAnterieurementPresence) {
        this.activiteExerceeAnterieurementPresence = activiteExerceeAnterieurementPresence;
    }

    /**
     * Setter de l'attribut activiteExerceeAnterieurementSIREN.
     * 
     * @param activiteExerceeAnterieurementSIREN
     *            la nouvelle valeur de activiteExerceeAnterieurementSIREN
     */
    public void setActiviteExerceeAnterieurementSIREN(String activiteExerceeAnterieurementSIREN) {
        this.activiteExerceeAnterieurementSIREN = activiteExerceeAnterieurementSIREN;
    }

    /**
     * Setter de l'attribut affiliationMSA.
     * 
     * @param affiliationMSA
     *            la nouvelle valeur de affiliationMSA
     */
    public void setAffiliationMSA(String affiliationMSA) {
        this.affiliationMSA = affiliationMSA;
    }

    /**
     * Setter de l'attribut ayantDroitNombre.
     * 
     * @param ayantDroitNombre
     *            la nouvelle valeur de ayantDroitNombre
     */
    public void setAyantDroitNombre(Integer ayantDroitNombre) {
        this.ayantDroitNombre = ayantDroitNombre;
    }

    /**
     * Setter de l'attribut ayantDroitPresence.
     * 
     * @param ayantDroitPresence
     *            la nouvelle valeur de ayantDroitPresence
     */
    public void setAyantDroitPresence(String ayantDroitPresence) {
        this.ayantDroitPresence = ayantDroitPresence;
    }

    /**
     * Setter de l'attribut ayantDroits.
     * 
     * @param ayantDroits
     *            la nouvelle valeur de ayantDroits
     */
    public void setAyantDroits(List<AyantDroitBean> ayantDroits) {
        this.ayantDroits = ayantDroits;
    }

    /**
     * Setter de l'attribut conjointCouvertAssuranceMaladie1.
     * 
     * @param conjointCouvertAssuranceMaladie1
     *            la nouvelle valeur de conjointCouvertAssuranceMaladie1
     */
    public void setConjointCouvertAssuranceMaladie1(String conjointCouvertAssuranceMaladie1) {
        this.conjointCouvertAssuranceMaladie1 = conjointCouvertAssuranceMaladie1;
    }

    /**
     * Setter de l'attribut conjointCouvertAssuranceMaladie2.
     * 
     * @param conjointCouvertAssuranceMaladie2
     *            la nouvelle valeur de conjointCouvertAssuranceMaladie2
     */
    public void setConjointCouvertAssuranceMaladie2(String conjointCouvertAssuranceMaladie2) {
        this.conjointCouvertAssuranceMaladie2 = conjointCouvertAssuranceMaladie2;
    }

    /**
     * Setter de l'attribut conjointNumeroSecuriteSociale.
     * 
     * @param conjointNumeroSecuriteSociale
     *            la nouvelle valeur de conjointNumeroSecuriteSociale
     */
    public void setConjointNumeroSecuriteSociale(String conjointNumeroSecuriteSociale) {
        this.conjointNumeroSecuriteSociale = conjointNumeroSecuriteSociale;
    }

    /**
     * Setter de l'attribut demandeDotationJeuneAgriculteur.
     * 
     * @param demandeDotationJeuneAgriculteur
     *            la nouvelle valeur de demandeDotationJeuneAgriculteur
     */
    public void setDemandeDotationJeuneAgriculteur(String demandeDotationJeuneAgriculteur) {
        this.demandeDotationJeuneAgriculteur = demandeDotationJeuneAgriculteur;
    }

    /**
     * setDepartementOrganismeConventionne.
     * 
     * @param departementOrganismeConventionne
     *            departementOrganismeConventionne
     */
    public void setDepartementOrganismeConventionne(String departementOrganismeConventionne) {
        this.departementOrganismeConventionne = departementOrganismeConventionne;
    }

    /**
     * Setter de l'attribut estBeneficiaireRSARMI.
     * 
     * @param estBeneficiaireRSARMI
     *            la nouvelle valeur de estBeneficiaireRSARMI
     */
    public void setEstBeneficiaireRSARMI(String estBeneficiaireRSARMI) {
        this.estBeneficiaireRSARMI = estBeneficiaireRSARMI;
    }

    /**
     * Setter de l'attribut estJeuneAgriculteur.
     * 
     * @param estJeuneAgriculteur
     *            la nouvelle valeur de estJeuneAgriculteur
     */
    public void setEstJeuneAgriculteur(String estJeuneAgriculteur) {
        this.estJeuneAgriculteur = estJeuneAgriculteur;
    }

    /**
     * Setter de l'attribut estMajeurHorsUE.
     * 
     * @param estMajeurHorsUE
     *            la nouvelle valeur de estMajeurHorsUE
     */
    public void setEstMajeurHorsUE(Boolean estMajeurHorsUE) {
        this.estMajeurHorsUE = estMajeurHorsUE;
    }

    /**
     * Setter de l'attribut numeroSecuriteSociale.
     * 
     * @param numeroSecuriteSociale
     *            la nouvelle valeur de numeroSecuriteSociale
     */
    public void setNumeroSecuriteSociale(String numeroSecuriteSociale) {
        this.numeroSecuriteSociale = numeroSecuriteSociale;
    }

    /**
     * Setter de l'attribut optionMicroSocial.
     * 
     * @param optionMicroSocial
     *            la nouvelle valeur de optionMicroSocial
     */
    public void setOptionMicroSocial(String optionMicroSocial) {
        this.optionMicroSocial = optionMicroSocial;
    }

    /**
     * Setter de l'attribut optionMicroSocialVersement.
     * 
     * @param optionMicroSocialVersement
     *            la nouvelle valeur de optionMicroSocialVersement
     */
    public void setOptionMicroSocialVersement(String optionMicroSocialVersement) {
        this.optionMicroSocialVersement = optionMicroSocialVersement;
    }

    /**
     * Setter de l'attribut organismeConventionneCode.
     * 
     * @param organismeConventionneCode
     *            la nouvelle valeur de organismeConventionneCode
     */
    public void setOrganismeConventionneCode(String organismeConventionneCode) {
        this.organismeConventionneCode = organismeConventionneCode;
    }

    /**
     * setOrganismeConventionneCodeReseauA.
     * 
     * @param organismeConventionneCodeReseauA
     *            organismeConventionneCodeReseauA
     */
    public void setOrganismeConventionneCodeReseauA(String organismeConventionneCodeReseauA) {
        this.organismeConventionneCodeReseauA = organismeConventionneCodeReseauA;
    }

    /**
     * setOrganismeConventionneCodeReseauNonA.
     * 
     * @param organismeConventionneCodeReseauNonA
     *            organismeConventionneCodeReseauNonA
     */
    public void setOrganismeConventionneCodeReseauNonA(String organismeConventionneCodeReseauNonA) {
        this.organismeConventionneCodeReseauNonA = organismeConventionneCodeReseauNonA;
    }

    /**
     * Setter de l'attribut organismeConventionneLibelle.
     * 
     * @param organismeConventionneLibelle
     *            la nouvelle valeur de organismeConventionneLibelle
     */
    public void setOrganismeConventionneLibelle(String organismeConventionneLibelle) {
        this.organismeConventionneLibelle = organismeConventionneLibelle;
    }

    /**
     * Setter de l'attribut organismeServantPension.
     * 
     * @param organismeServantPension
     *            la nouvelle valeur de organismeServantPension
     */
    public void setOrganismeServantPension(String organismeServantPension) {
        this.organismeServantPension = organismeServantPension;
    }

    /**
     * Setter de l'attribut regimeAssuranceMaladie.
     * 
     * @param regimeAssuranceMaladie
     *            la nouvelle valeur de regimeAssuranceMaladie
     */
    public void setRegimeAssuranceMaladie(String regimeAssuranceMaladie) {
        this.regimeAssuranceMaladie = regimeAssuranceMaladie;
    }

    /**
     * Setter de l'attribut regimeAssuranceMaladieAutre.
     * 
     * @param regimeAssuranceMaladieAutre
     *            la nouvelle valeur de regimeAssuranceMaladieAutre
     */
    public void setRegimeAssuranceMaladieAutre(String regimeAssuranceMaladieAutre) {
        this.regimeAssuranceMaladieAutre = regimeAssuranceMaladieAutre;
    }

    /**
     * Setter de l'attribut statutConjoint.
     * 
     * @param statutConjoint
     *            la nouvelle valeur de statutConjoint
     */
    public void setStatutConjoint(String statutConjoint) {
        this.statutConjoint = statutConjoint;
    }

    /**
     * Setter de l'attribut titreSejourDateExpiration.
     * 
     * @param titreSejourDateExpiration
     *            la nouvelle valeur de titreSejourDateExpiration
     */
    public void setTitreSejourDateExpiration(String titreSejourDateExpiration) {
        this.titreSejourDateExpiration = titreSejourDateExpiration;
    }

    /**
     * Setter de l'attribut titreSejourLieuDelivranceCode.
     * 
     * @param titreSejourLieuDelivranceCode
     *            la nouvelle valeur de titreSejourLieuDelivranceCode
     */
    public void setTitreSejourLieuDelivranceCode(String titreSejourLieuDelivranceCode) {
        this.titreSejourLieuDelivranceCode = titreSejourLieuDelivranceCode;
    }

    /**
     * Setter de l'attribut titreSejourLieuDelivranceCommune.
     * 
     * @param titreSejourLieuDelivranceCommune
     *            la nouvelle valeur de titreSejourLieuDelivranceCommune
     */
    public void setTitreSejourLieuDelivranceCommune(String titreSejourLieuDelivranceCommune) {
        this.titreSejourLieuDelivranceCommune = titreSejourLieuDelivranceCommune;
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    /**
     * Gets the date naissance dirigeant.
     *
     * @return the date naissance dirigeant
     */
    public String getDateNaissanceDirigeant() {
        return dateNaissanceDirigeant;
    }

    /**
     * Sets the date naissance dirigeant.
     *
     * @param dateNaissanceDirigeant
     *            the new date naissance dirigeant
     */
    public void setDateNaissanceDirigeant(String dateNaissanceDirigeant) {
        this.dateNaissanceDirigeant = dateNaissanceDirigeant;
    }

}
