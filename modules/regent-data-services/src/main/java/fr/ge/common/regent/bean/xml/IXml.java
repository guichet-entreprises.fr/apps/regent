package fr.ge.common.regent.bean.xml;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.validation.annotations.NotEmpty;

import fr.ge.common.regent.xml.annotation.Indexed;

/**
 * Le Interface IXml.
 */
public interface IXml {

    /**
     * Version du modèle de données. Cette version change lorqu'une modification
     * fonctionnelle ou technique rend les anciennes versions de l'objet
     * incompatible avec la nouvelle version.
     *
     * @return le model version
     * @category Obligatoire
     */
    @NotEmpty
    @FieldXPath("@modelVersion")
    @Indexed("model_version")
    int getModelVersion();

    /**
     * Identifiant unique du document XML.
     *
     * @return le id technique
     * @category Obligatoire
     */
    @FieldXPath("idTechnique")
    @Indexed("id")
    String getIdTechnique();

    /**
     * Version du document XML.
     * 
     * <p>
     * Information technique utilisÃ©e pour identifier les conflits (édition
     * simultannée d'un même document).
     *
     * @return le version
     */
    @FieldXPath("version")
    @Indexed("version")
    String getVersion();

    /**
     * Set le model version.
     *
     * @param version
     *            le nouveau model version
     */
    void setModelVersion(int version);

    /**
     * Set le id technique.
     *
     * @param idTechnique
     *            le nouveau id technique
     */
    void setIdTechnique(String idTechnique);

    /**
     * Set le version.
     *
     * @param version
     *            le nouveau version
     */
    void setVersion(String version);

}
