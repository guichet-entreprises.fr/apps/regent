package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface AQU.
 */
@ResourceXPath("/OVU")
public interface OVU {

  /**
   * Get le U42.
   *
   * @return le U42
   */
  @FieldXPath("U42")
  String getU42();

  /**
   * Set le U42.
   *
   * @param U42
   *          le nouveau U42
   */
  void setU42(String U42);

}
