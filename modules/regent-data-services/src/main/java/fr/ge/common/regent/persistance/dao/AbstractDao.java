package fr.ge.common.regent.persistance.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

/**
 * Implémentation de l'interface IGenericDao<T, U>.
 *
 * Classe générique d'accès aux données en base. Elle permet d'effectuer des
 * opération simple : ajouter, supprimer, mettre à jour, récupérer un ou tous
 * les éléments d'une table.
 *
 * @param <T>
 *            Type de la table correspondante
 * @param <U>
 *            Type de la clé primaire de la table
 */
public abstract class AbstractDao<T, U extends Serializable> implements IGenericDao<T, U> {

    /** Le clazz. */
    protected Class<T> clazz;

    /** Le session factory. */
    private SessionFactory sessionFactory;

    /**
     * Constructeur.
     *
     * @param clazz
     *            la classe correspondante à la table
     */
    public AbstractDao(Class<T> clazz) {
        this.clazz = clazz;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T get(U uuid) {
        T retour = null;
        if (uuid != null) {
            retour = (T) this.getSession().get(this.clazz, uuid);
        }
        return retour;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<T> getAll(String column) {
        List<T> retour = null;
        if (column != null) {
            retour = this.getSession().createCriteria(this.clazz).addOrder(Order.asc(column)).list();
        }
        return retour;
    }

    /**
     * getter.
     *
     * @return session
     */
    public Session getSession() {
        return this.sessionFactory.getCurrentSession();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public U save(T o) {
        U retour = null;
        if (o != null) {
            retour = (U) this.getSession().save(o);
        }
        return retour;
    }

    /**
     * Setter.
     *
     * @param sessionFactory
     *            session
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Get le clazz.
     *
     * @return the clazz
     */
    public Class<T> getClazz() {
        return this.clazz;
    }

}