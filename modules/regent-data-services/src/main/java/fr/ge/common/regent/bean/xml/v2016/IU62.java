package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * IU62.
 * 
 * @author $Author: hhichri $
 * @version $Revision: 0 $
 */
@ResourceXPath("/U62")
public interface IU62 {

  /**
   * Get le U62.1.
   *
   * @return le U62.1
   */
  @FieldXPath("U62.1")
  String getU621();

  /**
   * Set le U62.1.
   *
   * @param U621
   *          .1 le nouveau U62.1
   */
  void setU621(String U621);

  /**
   * Get le U62.2.
   *
   * @return le U62.2
   */
  @FieldXPath("U62.2")
  String getU622();

  /**
   * Set le U62.2.
   *
   * @param U622
   *          .2 le nouveau U62.2
   */
  void setU622(String U622);

  /**
   * Get le U623.
   *
   * @return le U623
   */
  @FieldXPath("U62.3")
  IU623[] getU623();

  /**
   * Ajoute le to U623.
   *
   * @return le U623
   */
  IU623 addToU623();

  /**
   * Set le U623.
   *
   * @param U623
   *          le nouveau U623
   */
  void setU623(IU623[] U623);

}
