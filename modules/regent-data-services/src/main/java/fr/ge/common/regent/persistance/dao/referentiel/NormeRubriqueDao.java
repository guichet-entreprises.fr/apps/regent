/**
 *
 */
package fr.ge.common.regent.persistance.dao.referentiel;

import java.util.Collection;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;

import fr.ge.common.regent.bean.modele.referentiel.ENormeEvenement;
import fr.ge.common.regent.bean.modele.referentiel.ENormeRubrique;
import fr.ge.common.regent.persistance.dao.AbstractDao;

/**
 * @author $Author: kelmoura $
 * @version $Revision: 0 $
 */
public class NormeRubriqueDao extends AbstractDao<ENormeRubrique, String> {

    /**
     * Instancie un nouveau norme rubrique dao.
     *
     * @param type
     */
    public NormeRubriqueDao(Class<ENormeRubrique> type) {
        super(type);
    }

    public List<ENormeRubrique> getRubriquesPathByEvenementAndVersion(final String version, final List<String> evenements) {
        final Collection<String> values = evenements;
        final DetachedCriteria subCriteria = DetachedCriteria.forClass(ENormeEvenement.class);
        subCriteria.add(Property.forName("evenement").in(values));
        subCriteria.add(Property.forName("version").eq(version));
        subCriteria.setProjection(Projections.distinct(Projections.property("rubrique")));

        return this.getSession().createCriteria(this.getClazz()).add(Property.forName("rubrique").in(subCriteria)).list();
    }
}
