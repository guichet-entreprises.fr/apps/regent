/**
 * 
 */
package fr.ge.common.regent.bean.formalite.dialogue;

import fr.ge.common.regent.bean.formalite.FormaliteVueBean;

/**
 * La classe Abstract ActiviteSoumiseQualification.
 * 
 * @author $Author: hhichri $
 * @version $Revision: 0 $
 */
public abstract class AbstractActiviteSoumiseQualification extends FormaliteVueBean {

    /** serialVersionUID. **/
    private static final long serialVersionUID = 2253445582892809207L;

    /** l'activite soumise à qualification. **/
    protected String activiteJQPA;

    /** l'aqpa Situation. **/
    protected String aqpaSituation;

    /**
     * Qualité de la personne qualifiée exerçant le contrôle effectif et
     * permanent de l’activité (personne Physique).
     **/
    protected String qualitePersonneQualifieePP;

    /** Autre qualité (personne physique). **/
    protected String qualitePersonneQualifieePPAutre;

    /** nom de naissance (personne physique). **/
    protected String identitePPNomNaissance;

    /** nom d'usage (personne physique). **/
    protected String identitePPNomUsage;

    /** prénom1 (personne physique). **/
    protected String identitePPPrenom1;

    /** prénom2 (personne physique). **/
    protected String identitePPPrenom2;

    /** prénom3 (personne physique). **/
    protected String identitePPPrenom3;

    /** date de naissance (personne physique). **/
    protected String identitePPDateNaissance;

    /** pays de naissance (personne physique). **/
    protected String identitePPLieuNaissancePays;

    /** département de naissance (personne physique). **/
    protected String identitePPLieuNaissanceDepartement;

    /** commune de naissance (personne physique). **/
    protected String identitePPLieuNaissanceCommune;

    /** ville de naissance (personne physique). **/
    protected String identitePPLieuNaissanceVille;

    /**
     * Qualité de la personne qualifiée exerçant le contrôle effectif et
     * permanent de l’activité (personne morale).
     **/
    protected String qualitePersonneQualifieePM;

    /** Autre qualité (personne morale). **/
    protected String qualitePersonneQualifieePMAutre;

    /** nom de naissance (personne morale). **/
    protected String identitePMNomNaissance;

    /** nom d'usage (personne morale). **/
    protected String identitePMNomUsage;

    /** prénom1 (personne morale). **/
    protected String identitePMPrenom1;

    /** prénom2 (personne morale). **/
    protected String identitePMPrenom2;

    /** prénom3 (personne morale). **/
    protected String identitePMPrenom3;

    /** date de naissance (personne morale). **/
    protected String identitePMDateNaissance;

    /** pays de naissance (personne morale). **/
    protected String identitePMLieuNaissancePays;

    /** département de naissance (personne morale). **/
    protected String identitePMLieuNaissanceDepartement;

    /** commune de naissance (personne morale). **/
    protected String identitePMLieuNaissanceCommune;

    /** ville de naissance (personne morale). **/
    protected String identitePMLieuNaissanceVille;

    /**
     * Accesseur sur l'attribut {@link #activiteJQPA}.
     *
     * @return String activiteJQPA
     */
    public String getActiviteJQPA() {
        return activiteJQPA;
    }

    /**
     * Mutateur sur l'attribut {@link #activiteJQPA}.
     *
     * @param activiteJQPA
     *            la nouvelle valeur de l'attribut activiteJQPA
     */
    public void setActiviteJQPA(String activiteJQPA) {
        this.activiteJQPA = activiteJQPA;
    }

    /**
     * Accesseur sur l'attribut {@link #aqpaSituation}.
     *
     * @return String aqpaSituation
     */
    public String getAqpaSituation() {
        return aqpaSituation;
    }

    /**
     * Mutateur sur l'attribut {@link #aqpaSituation}.
     *
     * @param aqpaSituation
     *            la nouvelle valeur de l'attribut aqpaSituation
     */
    public void setAqpaSituation(String aqpaSituation) {
        this.aqpaSituation = aqpaSituation;
    }

    /**
     * Accesseur sur l'attribut {@link #qualitePersonneQualifieePP}.
     *
     * @return String qualitePersonneQualifieePP
     */
    public String getQualitePersonneQualifieePP() {
        return qualitePersonneQualifieePP;
    }

    /**
     * Mutateur sur l'attribut {@link #qualitePersonneQualifieePP}.
     *
     * @param qualitePersonneQualifieePP
     *            la nouvelle valeur de l'attribut qualitePersonneQualifieePP
     */
    public void setQualitePersonneQualifieePP(String qualitePersonneQualifieePP) {
        this.qualitePersonneQualifieePP = qualitePersonneQualifieePP;
    }

    /**
     * Accesseur sur l'attribut {@link #qualitePersonneQualifieePPAutre}.
     *
     * @return String qualitePersonneQualifieePPAutre
     */
    public String getQualitePersonneQualifieePPAutre() {
        return qualitePersonneQualifieePPAutre;
    }

    /**
     * Mutateur sur l'attribut {@link #qualitePersonneQualifieePPAutre}.
     *
     * @param qualitePersonneQualifieePPAutre
     *            la nouvelle valeur de l'attribut
     *            qualitePersonneQualifieePPAutre
     */
    public void setQualitePersonneQualifieePPAutre(String qualitePersonneQualifieePPAutre) {
        this.qualitePersonneQualifieePPAutre = qualitePersonneQualifieePPAutre;
    }

    /**
     * Accesseur sur l'attribut {@link #identitePPNomNaissance}.
     *
     * @return String identitePPNomNaissance
     */
    public String getIdentitePPNomNaissance() {
        return identitePPNomNaissance;
    }

    /**
     * Mutateur sur l'attribut {@link #identitePPNomNaissance}.
     *
     * @param identitePPNomNaissance
     *            la nouvelle valeur de l'attribut identitePPNomNaissance
     */
    public void setIdentitePPNomNaissance(String identitePPNomNaissance) {
        this.identitePPNomNaissance = identitePPNomNaissance;
    }

    /**
     * Accesseur sur l'attribut {@link #identitePPPrenom1}.
     *
     * @return String identitePPPrenom1
     */
    public String getIdentitePPPrenom1() {
        return identitePPPrenom1;
    }

    /**
     * Mutateur sur l'attribut {@link #identitePPPrenom1}.
     *
     * @param identitePPPrenom1
     *            la nouvelle valeur de l'attribut identitePPPrenom1
     */
    public void setIdentitePPPrenom1(String identitePPPrenom1) {
        this.identitePPPrenom1 = identitePPPrenom1;
    }

    /**
     * Accesseur sur l'attribut {@link #identitePPPrenom2}.
     *
     * @return String identitePPPrenom2
     */
    public String getIdentitePPPrenom2() {
        return identitePPPrenom2;
    }

    /**
     * Mutateur sur l'attribut {@link #identitePPPrenom2}.
     *
     * @param identitePPPrenom2
     *            la nouvelle valeur de l'attribut identitePPPrenom2
     */
    public void setIdentitePPPrenom2(String identitePPPrenom2) {
        this.identitePPPrenom2 = identitePPPrenom2;
    }

    /**
     * Accesseur sur l'attribut {@link #identitePPPrenom3}.
     *
     * @return String identitePPPrenom3
     */
    public String getIdentitePPPrenom3() {
        return identitePPPrenom3;
    }

    /**
     * Mutateur sur l'attribut {@link #identitePPPrenom3}.
     *
     * @param identitePPPrenom3
     *            la nouvelle valeur de l'attribut identitePPPrenom3
     */
    public void setIdentitePPPrenom3(String identitePPPrenom3) {
        this.identitePPPrenom3 = identitePPPrenom3;
    }

    /**
     * Accesseur sur l'attribut {@link #identitePPDateNaissance}.
     *
     * @return String identitePPDateNaissance
     */
    public String getIdentitePPDateNaissance() {
        return identitePPDateNaissance;
    }

    /**
     * Mutateur sur l'attribut {@link #identitePPDateNaissance}.
     *
     * @param identitePPDateNaissance
     *            la nouvelle valeur de l'attribut identitePPDateNaissance
     */
    public void setIdentitePPDateNaissance(String identitePPDateNaissance) {
        this.identitePPDateNaissance = identitePPDateNaissance;
    }

    /**
     * Accesseur sur l'attribut {@link #identitePPLieuNaissancePays}.
     *
     * @return String identitePPLieuNaissancePays
     */
    public String getIdentitePPLieuNaissancePays() {
        return identitePPLieuNaissancePays;
    }

    /**
     * Mutateur sur l'attribut {@link #identitePPLieuNaissancePays}.
     *
     * @param identitePPLieuNaissancePays
     *            la nouvelle valeur de l'attribut identitePPLieuNaissancePays
     */
    public void setIdentitePPLieuNaissancePays(String identitePPLieuNaissancePays) {
        this.identitePPLieuNaissancePays = identitePPLieuNaissancePays;
    }

    /**
     * Accesseur sur l'attribut {@link #identitePPLieuNaissanceDepartement}.
     *
     * @return String identitePPLieuNaissanceDepartement
     */
    public String getIdentitePPLieuNaissanceDepartement() {
        return identitePPLieuNaissanceDepartement;
    }

    /**
     * Mutateur sur l'attribut {@link #identitePPLieuNaissanceDepartement}.
     *
     * @param identitePPLieuNaissanceDepartement
     *            la nouvelle valeur de l'attribut
     *            identitePPLieuNaissanceDepartement
     */
    public void setIdentitePPLieuNaissanceDepartement(String identitePPLieuNaissanceDepartement) {
        this.identitePPLieuNaissanceDepartement = identitePPLieuNaissanceDepartement;
    }

    /**
     * Accesseur sur l'attribut {@link #identitePPLieuNaissanceCommune}.
     *
     * @return String identitePPLieuNaissanceCommune
     */
    public String getIdentitePPLieuNaissanceCommune() {
        return identitePPLieuNaissanceCommune;
    }

    /**
     * Mutateur sur l'attribut {@link #identitePPLieuNaissanceCommune}.
     *
     * @param identitePPLieuNaissanceCommune
     *            la nouvelle valeur de l'attribut
     *            identitePPLieuNaissanceCommune
     */
    public void setIdentitePPLieuNaissanceCommune(String identitePPLieuNaissanceCommune) {
        this.identitePPLieuNaissanceCommune = identitePPLieuNaissanceCommune;
    }

    /**
     * Accesseur sur l'attribut {@link #identitePPLieuNaissanceVille}.
     *
     * @return String identitePPLieuNaissanceVille
     */
    public String getIdentitePPLieuNaissanceVille() {
        return identitePPLieuNaissanceVille;
    }

    /**
     * Mutateur sur l'attribut {@link #identitePPLieuNaissanceVille}.
     *
     * @param identitePPLieuNaissanceVille
     *            la nouvelle valeur de l'attribut identitePPLieuNaissanceVille
     */
    public void setIdentitePPLieuNaissanceVille(String identitePPLieuNaissanceVille) {
        this.identitePPLieuNaissanceVille = identitePPLieuNaissanceVille;
    }

    /**
     * Accesseur sur l'attribut {@link #qualitePersonneQualifieePM}.
     *
     * @return String qualitePersonneQualifieePM
     */
    public String getQualitePersonneQualifieePM() {
        return qualitePersonneQualifieePM;
    }

    /**
     * Mutateur sur l'attribut {@link #qualitePersonneQualifieePM}.
     *
     * @param qualitePersonneQualifieePM
     *            la nouvelle valeur de l'attribut qualitePersonneQualifieePM
     */
    public void setQualitePersonneQualifieePM(String qualitePersonneQualifieePM) {
        this.qualitePersonneQualifieePM = qualitePersonneQualifieePM;
    }

    /**
     * Accesseur sur l'attribut {@link #qualitePersonneQualifieePMAutre}.
     *
     * @return String qualitePersonneQualifieePMAutre
     */
    public String getQualitePersonneQualifieePMAutre() {
        return qualitePersonneQualifieePMAutre;
    }

    /**
     * Mutateur sur l'attribut {@link #qualitePersonneQualifieePMAutre}.
     *
     * @param qualitePersonneQualifieePMAutre
     *            la nouvelle valeur de l'attribut
     *            qualitePersonneQualifieePMAutre
     */
    public void setQualitePersonneQualifieePMAutre(String qualitePersonneQualifieePMAutre) {
        this.qualitePersonneQualifieePMAutre = qualitePersonneQualifieePMAutre;
    }

    /**
     * Accesseur sur l'attribut {@link #identitePMNomNaissance}.
     *
     * @return String identitePMNomNaissance
     */
    public String getIdentitePMNomNaissance() {
        return identitePMNomNaissance;
    }

    /**
     * Mutateur sur l'attribut {@link #identitePMNomNaissance}.
     *
     * @param identitePMNomNaissance
     *            la nouvelle valeur de l'attribut identitePMNomNaissance
     */
    public void setIdentitePMNomNaissance(String identitePMNomNaissance) {
        this.identitePMNomNaissance = identitePMNomNaissance;
    }

    /**
     * Accesseur sur l'attribut {@link #identitePMPrenom1}.
     *
     * @return String identitePMPrenom1
     */
    public String getIdentitePMPrenom1() {
        return identitePMPrenom1;
    }

    /**
     * Mutateur sur l'attribut {@link #identitePMPrenom1}.
     *
     * @param identitePMPrenom1
     *            la nouvelle valeur de l'attribut identitePMPrenom1
     */
    public void setIdentitePMPrenom1(String identitePMPrenom1) {
        this.identitePMPrenom1 = identitePMPrenom1;
    }

    /**
     * Accesseur sur l'attribut {@link #identitePMPrenom2}.
     *
     * @return String identitePMPrenom2
     */
    public String getIdentitePMPrenom2() {
        return identitePMPrenom2;
    }

    /**
     * Mutateur sur l'attribut {@link #identitePMPrenom2}.
     *
     * @param identitePMPrenom2
     *            la nouvelle valeur de l'attribut identitePMPrenom2
     */
    public void setIdentitePMPrenom2(String identitePMPrenom2) {
        this.identitePMPrenom2 = identitePMPrenom2;
    }

    /**
     * Accesseur sur l'attribut {@link #identitePMPrenom3}.
     *
     * @return String identitePMPrenom3
     */
    public String getIdentitePMPrenom3() {
        return identitePMPrenom3;
    }

    /**
     * Mutateur sur l'attribut {@link #identitePMPrenom3}.
     *
     * @param identitePMPrenom3
     *            la nouvelle valeur de l'attribut identitePMPrenom3
     */
    public void setIdentitePMPrenom3(String identitePMPrenom3) {
        this.identitePMPrenom3 = identitePMPrenom3;
    }

    /**
     * Accesseur sur l'attribut {@link #identitePMDateNaissance}.
     *
     * @return String identitePMDateNaissance
     */
    public String getIdentitePMDateNaissance() {
        return identitePMDateNaissance;
    }

    /**
     * Mutateur sur l'attribut {@link #identitePMDateNaissance}.
     *
     * @param identitePMDateNaissance
     *            la nouvelle valeur de l'attribut identitePMDateNaissance
     */
    public void setIdentitePMDateNaissance(String identitePMDateNaissance) {
        this.identitePMDateNaissance = identitePMDateNaissance;
    }

    /**
     * Accesseur sur l'attribut {@link #identitePMLieuNaissancePays}.
     *
     * @return String identitePMLieuNaissancePays
     */
    public String getIdentitePMLieuNaissancePays() {
        return identitePMLieuNaissancePays;
    }

    /**
     * Mutateur sur l'attribut {@link #identitePMLieuNaissancePays}.
     *
     * @param identitePMLieuNaissancePays
     *            la nouvelle valeur de l'attribut identitePMLieuNaissancePays
     */
    public void setIdentitePMLieuNaissancePays(String identitePMLieuNaissancePays) {
        this.identitePMLieuNaissancePays = identitePMLieuNaissancePays;
    }

    /**
     * Accesseur sur l'attribut {@link #identitePMLieuNaissanceDepartement}.
     *
     * @return String identitePMLieuNaissanceDepartement
     */
    public String getIdentitePMLieuNaissanceDepartement() {
        return identitePMLieuNaissanceDepartement;
    }

    /**
     * Mutateur sur l'attribut {@link #identitePMLieuNaissanceDepartement}.
     *
     * @param identitePMLieuNaissanceDepartement
     *            la nouvelle valeur de l'attribut
     *            identitePMLieuNaissanceDepartement
     */
    public void setIdentitePMLieuNaissanceDepartement(String identitePMLieuNaissanceDepartement) {
        this.identitePMLieuNaissanceDepartement = identitePMLieuNaissanceDepartement;
    }

    /**
     * Accesseur sur l'attribut {@link #identitePMLieuNaissanceCommune}.
     *
     * @return String identitePMLieuNaissanceCommune
     */
    public String getIdentitePMLieuNaissanceCommune() {
        return identitePMLieuNaissanceCommune;
    }

    /**
     * Mutateur sur l'attribut {@link #identitePMLieuNaissanceCommune}.
     *
     * @param identitePMLieuNaissanceCommune
     *            la nouvelle valeur de l'attribut
     *            identitePMLieuNaissanceCommune
     */
    public void setIdentitePMLieuNaissanceCommune(String identitePMLieuNaissanceCommune) {
        this.identitePMLieuNaissanceCommune = identitePMLieuNaissanceCommune;
    }

    /**
     * Accesseur sur l'attribut {@link #identitePMLieuNaissanceVille}.
     *
     * @return String identitePMLieuNaissanceVille
     */
    public String getIdentitePMLieuNaissanceVille() {
        return identitePMLieuNaissanceVille;
    }

    /**
     * Mutateur sur l'attribut {@link #identitePMLieuNaissanceVille}.
     *
     * @param identitePMLieuNaissanceVille
     *            la nouvelle valeur de l'attribut identitePMLieuNaissanceVille
     */
    public void setIdentitePMLieuNaissanceVille(String identitePMLieuNaissanceVille) {
        this.identitePMLieuNaissanceVille = identitePMLieuNaissanceVille;
    }

    /**
     * Accesseur sur l'attribut {@link #identitePPNomUsage}.
     *
     * @return String identitePPNomUsage
     */
    public String getIdentitePPNomUsage() {
        return identitePPNomUsage;
    }

    /**
     * Mutateur sur l'attribut {@link #identitePPNomUsage}.
     *
     * @param identitePPNomUsage
     *            la nouvelle valeur de l'attribut identitePPNomUsage
     */
    public void setIdentitePPNomUsage(String identitePPNomUsage) {
        this.identitePPNomUsage = identitePPNomUsage;
    }

    /**
     * Accesseur sur l'attribut {@link #identitePMNomUsage}.
     *
     * @return String identitePMNomUsage
     */
    public String getIdentitePMNomUsage() {
        return identitePMNomUsage;
    }

    /**
     * Mutateur sur l'attribut {@link #identitePMNomUsage}.
     *
     * @param identitePMNomUsage
     *            la nouvelle valeur de l'attribut identitePMNomUsage
     */
    public void setIdentitePMNomUsage(String identitePMNomUsage) {
        this.identitePMNomUsage = identitePMNomUsage;
    }

}
