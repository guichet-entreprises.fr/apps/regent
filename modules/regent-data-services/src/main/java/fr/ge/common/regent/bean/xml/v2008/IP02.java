package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IP02.
 */
@ResourceXPath("/P02")
public interface IP02 {

  /**
   * Get le p021.
   *
   * @return le p021
   */
  @FieldXPath("P02.1")
  String getP021();

  /**
   * Set le p021.
   *
   * @param P021
   *          le nouveau p021
   */
  void setP021(String P021);

  /**
   * Get le p022.
   *
   * @return le p022
   */
  @FieldXPath("P02.2")
  String getP022();

  /**
   * Set le p022.
   *
   * @param P022
   *          le nouveau p022
   */
  void setP022(String P022);

}
