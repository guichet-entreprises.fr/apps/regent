package fr.ge.common.regent.bean.xml.formalite.dialogue.creation;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

import fr.ge.common.regent.bean.xml.formalite.IFormalite;

/**
 * Le Interface de l'entité régime fiscal de l'entreprise
 * 
 * 
 * 
 */
@ResourceXPath("/formalite")
public interface IRegimeFiscal extends IFormalite {

    /**
     * Getter de la date de cloture d'exercice comptable.
     * 
     * @return la valeur de dateClotureExerciceComptable
     */
    @FieldXPath(value = "dateClotureExerciceComptable")
    String getDateClotureExerciceComptable();

    /**
     * Getter de l'attribut enregistrementPrealableStatuts.
     * 
     * @return la valeur de enregistrementPrealableStatuts
     */
    @FieldXPath(value = "enregistrementPrealableStatuts")
    String getEnregistrementPrealableStatuts();

    /**
     * Getter de l'attribut enregistrementPrealableStatutsDate.
     * 
     * @return la valeur de enregistrementPrealableStatutsDate
     */
    @FieldXPath(value = "enregistrementPrealableStatutsDate")
    String getEnregistrementPrealableStatutsDate();

    /**
     * Getter de l'attribut enregistrementPrealableStatutsLieu.
     * 
     * @return la valeur de enregistrementPrealableStatutsLieu
     */
    @FieldXPath(value = "enregistrementPrealableStatutsLieu")
    String getEnregistrementPrealableStatutsLieu();

    /**
     * Getter de l'attribut regimeImpositionBenefices.
     * 
     * @return la valeur de regimeImpositionBenefices
     */
    @FieldXPath(value = "regimeImpositionBenefices")
    String getRegimeImpositionBenefices();

    /**
     * Getter de l'attribut regimeImpositionBeneficesOptionsParticulieres.
     * 
     * @return la valeur de regimeImpositionBeneficesOptionsParticulieres
     */
    @FieldXPath(value = "regimeImpositionBeneficesOptionsParticulieres")
    String getRegimeImpositionBeneficesOptionsParticulieres();

    /**
     * Getter de l'attribut
     * regimeImpositionBeneficesOptionsParticulieresBooleen.
     * 
     * @return la valeur de regimeImpositionBeneficesOptionsParticulieresBooleen
     */
    @FieldXPath(value = "regimeImpositionBeneficesOptionsParticulieresBooleen")
    String getRegimeImpositionBeneficesOptionsParticulieresBooleen();

    /**
     * Getter de l'attribut regimeImpositionBeneficesVersementLiberatoire.
     * 
     * @return la valeur de regimeImpositionBeneficesVersementLiberatoire
     */
    @FieldXPath(value = "regimeImpositionBeneficesVersementLiberatoire")
    String getRegimeImpositionBeneficesVersementLiberatoire();

    /**
     * Getter de l'attribut regimeImpositionTVA.
     * 
     * @return la valeur de regimeImpositionTVA
     */
    @FieldXPath(value = "regimeImpositionTVA")
    String getRegimeImpositionTVA();

    /**
     * Getter de l'attribut regimeImpositionTVAConditionVersement.
     * 
     * @return la valeur de regimeImpositionTVAConditionVersement
     */
    @FieldXPath(value = "regimeImpositionTVAConditionVersement")
    String getRegimeImpositionTVAConditionVersement();

    /**
     * Getter de l'attribut regimeImpositionTVAOptionsParticulieres1.
     * 
     * @return la valeur de regimeImpositionTVAOptionsParticulieres1
     */
    @FieldXPath(value = "regimeImpositionTVAOptionsParticulieres1")
    String getRegimeImpositionTVAOptionsParticulieres1();

    /**
     * Getter de l'attribut regimeImpositionTVAOptionsParticulieres2.
     * 
     * @return la valeur de regimeImpositionTVAOptionsParticulieres2
     */
    @FieldXPath(value = "regimeImpositionTVAOptionsParticulieres2")
    String getRegimeImpositionTVAOptionsParticulieres2();

    /**
     * Getter de l'attribut regimeImpositionTVAOptionsParticulieres3.
     * 
     * @return la valeur de regimeImpositionTVAOptionsParticulieres3
     */
    @FieldXPath(value = "regimeImpositionTVAOptionsParticulieres3")
    String getRegimeImpositionTVAOptionsParticulieres3();

    /**
     * Getter de l'attribut regimeImpositionTVAOptionsParticulieres4.
     * 
     * @return la valeur de regimeImpositionTVAOptionsParticulieres4
     */
    @FieldXPath(value = "regimeImpositionTVAOptionsParticulieres4")
    String getRegimeImpositionTVAOptionsParticulieres4();

    /**
     * Getter de l'attribut typeRegime.
     * 
     * @return la valeur de typeRegime
     */
    @FieldXPath(value = "typeRegime")
    String getTypeRegime();

    /**
     * Setter de l'attribut dateClotureExerciceComptable.
     * 
     * @param dateClotureExerciceComptable
     *            la nouvelle valeur de dateClotureExerciceComptable
     */

    void setDateClotureExerciceComptable(String dateClotureExerciceComptable);

    /**
     * Setter de l'attribut enregistrementPrealableStatuts.
     * 
     * @param enregistrementPrealableStatuts
     *            la nouvelle valeur de enregistrementPrealableStatuts
     */
    void setEnregistrementPrealableStatuts(String enregistrementPrealableStatuts);

    /**
     * Setter de l'attribut enregistrementPrealableStatutsDate.
     * 
     * @param enregistrementPrealableStatutsDate
     *            la nouvelle valeur de enregistrementPrealableStatutsDate
     */
    void setEnregistrementPrealableStatutsDate(String enregistrementPrealableStatutsDate);

    /**
     * Setter de l'attribut enregistrementPrealableStatutsLieu.
     * 
     * @param enregistrementPrealableStatutsLieu
     *            la nouvelle valeur de enregistrementPrealableStatutsLieu
     */
    void setEnregistrementPrealableStatutsLieu(String enregistrementPrealableStatutsLieu);

    /**
     * Setter de l'attribut regimeImpositionBenefices.
     * 
     * @param regimeImpositionBenefices
     *            la nouvelle valeur de regimeImpositionBenefices
     */

    void setRegimeImpositionBenefices(String regimeImpositionBenefices);

    /**
     * Setter de l'attribut regimeImpositionBeneficesOptionsParticulieres.
     * 
     * @param regimeImpositionBeneficesOptionsParticulieres
     *            la nouvelle valeur de
     *            regimeImpositionBeneficesOptionsParticulieres
     */
    void setRegimeImpositionBeneficesOptionsParticulieres(String regimeImpositionBeneficesOptionsParticulieres);

    /**
     * Setter de l'attribut
     * regimeImpositionBeneficesOptionsParticulieresBooleen.
     *
     * @param regimeImpositionBeneficesOptionsParticulieresBooleen
     *            le nouveau regime imposition benefices options particulieres
     *            booleen
     */
    void setRegimeImpositionBeneficesOptionsParticulieresBooleen(String regimeImpositionBeneficesOptionsParticulieresBooleen);

    /**
     * Setter de l'attribut regimeImpositionBeneficesVersementLiberatoire.
     * 
     * @param regimeImpositionBeneficesVersementLiberatoire
     *            la nouvelle valeur de
     *            regimeImpositionBeneficesVersementLiberatoire
     */
    void setRegimeImpositionBeneficesVersementLiberatoire(String regimeImpositionBeneficesVersementLiberatoire);

    /**
     * Setter de l'attribut regimeImpositionTVA.
     * 
     * @param regimeImpositionTVA
     *            la nouvelle valeur de regimeImpositionTVA
     */
    void setRegimeImpositionTVA(String regimeImpositionTVA);

    /**
     * Setter de l'attribut regimeImpositionTVAConditionVersement.
     * 
     * @param regimeImpositionTVAConditionVersement
     *            la nouvelle valeur de regimeImpositionTVAConditionVersement
     */
    void setRegimeImpositionTVAConditionVersement(String regimeImpositionTVAConditionVersement);

    /**
     * Setter de l'attribut regimeImpositionTVAOptionsParticulieres1.
     * 
     * @param regimeImpositionTVAOptionsParticulieres1
     *            la nouvelle valeur de regimeImpositionTVAOptionsParticulieres1
     */
    void setRegimeImpositionTVAOptionsParticulieres1(String regimeImpositionTVAOptionsParticulieres1);

    /**
     * Setter de l'attribut regimeImpositionTVAOptionsParticulieres2.
     * 
     * @param regimeImpositionTVAOptionsParticulieres2
     *            la nouvelle valeur de regimeImpositionTVAOptionsParticulieres2
     */
    void setRegimeImpositionTVAOptionsParticulieres2(String regimeImpositionTVAOptionsParticulieres2);

    /**
     * Setter de l'attribut regimeImpositionTVAOptionsParticulieres3.
     * 
     * @param regimeImpositionTVAOptionsParticulieres3
     *            la nouvelle valeur de regimeImpositionTVAOptionsParticulieres3
     */
    void setRegimeImpositionTVAOptionsParticulieres3(String regimeImpositionTVAOptionsParticulieres3);

    /**
     * Setter de l'attribut regimeImpositionTVAOptionsParticulieres4.
     * 
     * @param regimeImpositionTVAOptionsParticulieres4
     *            la nouvelle valeur de regimeImpositionTVAOptionsParticulieres4
     */
    void setRegimeImpositionTVAOptionsParticulieres4(String regimeImpositionTVAOptionsParticulieres4);

    /**
     * Setter de l'attribut typeRegime.
     * 
     * @param typeRegime
     *            la nouvelle valeur de typeRegime
     */
    void setTypeRegime(String typeRegime);

}
