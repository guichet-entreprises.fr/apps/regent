package fr.ge.common.regent.bean.formalite.dialogue.creation;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import fr.ge.common.regent.bean.formalite.dialogue.AbstractDirigeantBean;

/**
 * Le Class DirigeantBean.
 */
public class DirigeantBean extends AbstractDirigeantBean<ConjointBean> implements IFormaliteVue {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 5931967482818580153L;

    /** Le id technique. */
    private String idTechnique;

    /** Le declaration sociale. */
    private DeclarationSocialeBean declarationSociale = new DeclarationSocialeBean();

    /** Le dirigeant present statuts. */
    private String dirigeantPresentStatuts;

    /** Le inscription cac publiee. */
    private String inscriptionCACPubliee;

    /** Le nature. */
    private String nature;

    /** Le pm adresse. */
    private AdresseBean pmAdresse = new AdresseBean();

    /** Le pm denomination. */
    private String pmDenomination;

    /** Le pm forme juridique. */
    private String pmFormeJuridique;

    /** Le pm lieu immatriculation. */
    private String pmLieuImmatriculation;

    /** Le pm numero identification. */
    private String pmNumeroIdentification;

    /** Le pm representant adresse. */
    private AdresseBean pmRepresentantAdresse = new AdresseBean();

    /** Le pm representant civilite. */
    private String pmRepresentantCivilite;

    /** Le pm representant date naissance. */
    private String pmRepresentantDateNaissance;

    /** Le pm representant lieu naissance commune. */
    private String pmRepresentantLieuNaissanceCommune;

    /** Le pm representant lieu naissance departement. */
    private String pmRepresentantLieuNaissanceDepartement;

    /** Le pm representant lieu naissance pays. */
    private String pmRepresentantLieuNaissancePays;

    /** Le pm representant lieu naissance ville. */
    private String pmRepresentantLieuNaissanceVille;

    /** Le pm representant nationalite. */
    private String pmRepresentantNationalite;

    /** Le pm representant nom naissance. */
    private String pmRepresentantNomNaissance;

    /** Le pm representant nom usage. */
    private String pmRepresentantNomUsage;

    /** Le pm representant pm prenom1. */
    private String pmRepresentantPMPrenom1;

    /** Le pm representant pm prenom2. */
    private String pmRepresentantPMPrenom2;

    /** Le pm representant pm prenom3. */
    private String pmRepresentantPMPrenom3;

    /** Le pm representant pm prenom4. */
    private String pmRepresentantPMPrenom4;

    /** Le pm representant qualite. */
    private String pmRepresentantQualite;

    /** Le qualite. */
    private String qualite;

    /**
     * le libelle de la qualite du dirigeant.
     * 
     */
    private String libelleQualite;

    /**
     * Get le id technique.
     * 
     * @return the idTechnique
     */
    public String getIdTechnique() {
        return this.idTechnique;
    }

    /**
     * Set le id technique.
     * 
     * @param idTechnique
     *            the idTechnique to set
     */
    public void setIdTechnique(String idTechnique) {
        this.idTechnique = idTechnique;
    }

    /**
     * Getter de l'attribut declarationSociale.
     * 
     * @return la valeur de declarationSociale
     */
    public DeclarationSocialeBean getDeclarationSociale() {
        return this.declarationSociale;
    }

    /**
     * Getter de l'attribut dirigeantPresentStatuts.
     * 
     * @return la valeur de dirigeantPresentStatuts
     */
    public String getDirigeantPresentStatuts() {
        return this.dirigeantPresentStatuts;
    }

    /**
     * Getter de l'attribut inscriptionCACPubliee.
     * 
     * @return la valeur de inscriptionCACPubliee
     */
    public String getInscriptionCACPubliee() {
        return this.inscriptionCACPubliee;
    }

    /**
     * Getter de l'attribut nature.
     * 
     * @return la valeur de nature
     */
    public String getNature() {
        return this.nature;
    }

    /**
     * Getter de l'attribut pmAdresse.
     * 
     * @return la valeur de pmAdresse
     */
    public AdresseBean getPmAdresse() {
        return this.pmAdresse;
    }

    /**
     * Getter de l'attribut pmDenomination.
     * 
     * @return la valeur de pmDenomination
     */
    public String getPmDenomination() {
        return this.pmDenomination;
    }

    /**
     * Getter de l'attribut pmFormeJuridique.
     * 
     * @return la valeur de pmFormeJuridique
     */
    public String getPmFormeJuridique() {
        return this.pmFormeJuridique;
    }

    /**
     * Getter de l'attribut pmLieuImmatriculation.
     * 
     * @return la valeur de pmLieuImmatriculation
     */
    public String getPmLieuImmatriculation() {
        return this.pmLieuImmatriculation;
    }

    /**
     * Getter de l'attribut pmNumeroIdentification.
     * 
     * @return la valeur de pmNumeroIdentification
     */
    public String getPmNumeroIdentification() {
        return this.pmNumeroIdentification;
    }

    /**
     * Getter de l'attribut pmRepresentantAdresse.
     * 
     * @return la valeur de pmRepresentantAdresse
     */
    public AdresseBean getPmRepresentantAdresse() {
        return this.pmRepresentantAdresse;
    }

    /**
     * Getter de l'attribut pmRepresentantCivilite.
     * 
     * @return la valeur de pmRepresentantCivilite
     */
    public String getPmRepresentantCivilite() {
        return this.pmRepresentantCivilite;
    }

    /**
     * Getter de l'attribut pmRepresentantDateNaissance.
     * 
     * @return la valeur de pmRepresentantDateNaissance
     */
    public String getPmRepresentantDateNaissance() {
        return this.pmRepresentantDateNaissance;
    }

    /**
     * Getter de l'attribut pmRepresentantLieuNaissanceCommune.
     * 
     * @return la valeur de pmRepresentantLieuNaissanceCommune
     */
    public String getPmRepresentantLieuNaissanceCommune() {
        return this.pmRepresentantLieuNaissanceCommune;
    }

    /**
     * Getter de l'attribut pmRepresentantLieuNaissanceDepartement.
     * 
     * @return la valeur de pmRepresentantLieuNaissanceDepartement
     */
    public String getPmRepresentantLieuNaissanceDepartement() {
        return this.pmRepresentantLieuNaissanceDepartement;
    }

    /**
     * Getter de l'attribut pmRepresentantLieuNaissancePays.
     * 
     * @return la valeur de pmRepresentantLieuNaissancePays
     */
    public String getPmRepresentantLieuNaissancePays() {
        return this.pmRepresentantLieuNaissancePays;
    }

    /**
     * Getter de l'attribut pmRepresentantLieuNaissanceVille.
     * 
     * @return la valeur de pmRepresentantLieuNaissanceVille
     */
    public String getPmRepresentantLieuNaissanceVille() {
        return this.pmRepresentantLieuNaissanceVille;
    }

    /**
     * Getter de l'attribut pmRepresentantNationalite.
     * 
     * @return la valeur de pmRepresentantNationalite
     */
    public String getPmRepresentantNationalite() {
        return this.pmRepresentantNationalite;
    }

    /**
     * Getter de l'attribut pmRepresentantNomNaissance.
     * 
     * @return la valeur de pmRepresentantNomNaissance
     */
    public String getPmRepresentantNomNaissance() {
        return this.pmRepresentantNomNaissance;
    }

    /**
     * Getter de l'attribut pmRepresentantNomUsage.
     * 
     * @return la valeur de pmRepresentantNomUsage
     */
    public String getPmRepresentantNomUsage() {
        return this.pmRepresentantNomUsage;
    }

    /**
     * Getter de l'attribut pmRepresentantPMPrenom1.
     * 
     * @return la valeur de pmRepresentantPMPrenom1
     */
    public String getPmRepresentantPMPrenom1() {
        return this.pmRepresentantPMPrenom1;
    }

    /**
     * Getter de l'attribut pmRepresentantPMPrenom2.
     * 
     * @return la valeur de pmRepresentantPMPrenom2
     */
    public String getPmRepresentantPMPrenom2() {
        return this.pmRepresentantPMPrenom2;
    }

    /**
     * Getter de l'attribut pmRepresentantPMPrenom3.
     * 
     * @return la valeur de pmRepresentantPMPrenom3
     */
    public String getPmRepresentantPMPrenom3() {
        return this.pmRepresentantPMPrenom3;
    }

    /**
     * Getter de l'attribut pmRepresentantPMPrenom4.
     * 
     * @return la valeur de pmRepresentantPMPrenom4
     */
    public String getPmRepresentantPMPrenom4() {
        return this.pmRepresentantPMPrenom4;
    }

    /**
     * Getter de l'attribut pmRepresentantQualite.
     * 
     * @return la valeur de pmRepresentantQualite
     */
    public String getPmRepresentantQualite() {
        return this.pmRepresentantQualite;
    }

    /**
     * Getter de l'attribut qualite.
     * 
     * @return la valeur de qualite
     */
    public String getQualite() {
        return this.qualite;
    }

    /**
     * Setter de l'attribut declarationSociale.
     * 
     * @param declarationSociale
     *            la nouvelle valeur de declarationSociale
     */
    public void setDeclarationSociale(DeclarationSocialeBean declarationSociale) {
        this.declarationSociale = declarationSociale;
    }

    /**
     * Setter de l'attribut dirigeantPresentStatuts.
     * 
     * @param dirigeantPresentStatuts
     *            la nouvelle valeur de dirigeantPresentStatuts
     */
    public void setDirigeantPresentStatuts(String dirigeantPresentStatuts) {
        this.dirigeantPresentStatuts = dirigeantPresentStatuts;
    }

    /**
     * Setter de l'attribut inscriptionCACPubliee.
     * 
     * @param inscriptionCACPubliee
     *            la nouvelle valeur de inscriptionCACPubliee
     */
    public void setInscriptionCACPubliee(String inscriptionCACPubliee) {
        this.inscriptionCACPubliee = inscriptionCACPubliee;
    }

    /**
     * Setter de l'attribut nature.
     * 
     * @param nature
     *            la nouvelle valeur de nature
     */
    public void setNature(String nature) {
        this.nature = nature;
    }

    /**
     * Setter de l'attribut pmAdresse.
     * 
     * @param pmAdresse
     *            la nouvelle valeur de pmAdresse
     */
    public void setPmAdresse(AdresseBean pmAdresse) {
        this.pmAdresse = pmAdresse;
    }

    /**
     * Setter de l'attribut pmDenomination.
     * 
     * @param pmDenomination
     *            la nouvelle valeur de pmDenomination
     */
    public void setPmDenomination(String pmDenomination) {
        this.pmDenomination = pmDenomination;
    }

    /**
     * Setter de l'attribut pmFormeJuridique.
     * 
     * @param pmFormeJuridique
     *            la nouvelle valeur de pmFormeJuridique
     */
    public void setPmFormeJuridique(String pmFormeJuridique) {
        this.pmFormeJuridique = pmFormeJuridique;
    }

    /**
     * Setter de l'attribut pmLieuImmatriculation.
     * 
     * @param pmLieuImmatriculation
     *            la nouvelle valeur de pmLieuImmatriculation
     */
    public void setPmLieuImmatriculation(String pmLieuImmatriculation) {
        this.pmLieuImmatriculation = pmLieuImmatriculation;
    }

    /**
     * Setter de l'attribut pmNumeroIdentification.
     * 
     * @param pmNumeroIdentification
     *            la nouvelle valeur de pmNumeroIdentification
     */
    public void setPmNumeroIdentification(String pmNumeroIdentification) {
        this.pmNumeroIdentification = pmNumeroIdentification;
    }

    /**
     * Setter de l'attribut pmRepresentantAdresse.
     * 
     * @param pmRepresentantAdresse
     *            la nouvelle valeur de pmRepresentantAdresse
     */
    public void setPmRepresentantAdresse(AdresseBean pmRepresentantAdresse) {
        this.pmRepresentantAdresse = pmRepresentantAdresse;
    }

    /**
     * Setter de l'attribut pmRepresentantCivilite.
     * 
     * @param pmRepresentantCivilite
     *            la nouvelle valeur de pmRepresentantCivilite
     */
    public void setPmRepresentantCivilite(String pmRepresentantCivilite) {
        this.pmRepresentantCivilite = pmRepresentantCivilite;
    }

    /**
     * Setter de l'attribut pmRepresentantDateNaissance.
     * 
     * @param pmRepresentantDateNaissance
     *            la nouvelle valeur de pmRepresentantDateNaissance
     */
    public void setPmRepresentantDateNaissance(String pmRepresentantDateNaissance) {
        this.pmRepresentantDateNaissance = pmRepresentantDateNaissance;
    }

    /**
     * Setter de l'attribut pmRepresentantLieuNaissanceCommune.
     * 
     * @param pmRepresentantLieuNaissanceCommune
     *            la nouvelle valeur de pmRepresentantLieuNaissanceCommune
     */
    public void setPmRepresentantLieuNaissanceCommune(String pmRepresentantLieuNaissanceCommune) {
        this.pmRepresentantLieuNaissanceCommune = pmRepresentantLieuNaissanceCommune;
    }

    /**
     * Setter de l'attribut pmRepresentantLieuNaissanceDepartement.
     * 
     * @param pmRepresentantLieuNaissanceDepartement
     *            la nouvelle valeur de pmRepresentantLieuNaissanceDepartement
     */
    public void setPmRepresentantLieuNaissanceDepartement(String pmRepresentantLieuNaissanceDepartement) {
        this.pmRepresentantLieuNaissanceDepartement = pmRepresentantLieuNaissanceDepartement;
    }

    /**
     * Setter de l'attribut pmRepresentantLieuNaissancePays.
     * 
     * @param pmRepresentantLieuNaissancePays
     *            la nouvelle valeur de pmRepresentantLieuNaissancePays
     */
    public void setPmRepresentantLieuNaissancePays(String pmRepresentantLieuNaissancePays) {
        this.pmRepresentantLieuNaissancePays = pmRepresentantLieuNaissancePays;
    }

    /**
     * Setter de l'attribut pmRepresentantLieuNaissanceVille.
     * 
     * @param pmRepresentantLieuNaissanceVille
     *            la nouvelle valeur de pmRepresentantLieuNaissanceVille
     */
    public void setPmRepresentantLieuNaissanceVille(String pmRepresentantLieuNaissanceVille) {
        this.pmRepresentantLieuNaissanceVille = pmRepresentantLieuNaissanceVille;
    }

    /**
     * Setter de l'attribut pmRepresentantNationalite.
     * 
     * @param pmRepresentantNationalite
     *            la nouvelle valeur de pmRepresentantNationalite
     */
    public void setPmRepresentantNationalite(String pmRepresentantNationalite) {
        this.pmRepresentantNationalite = pmRepresentantNationalite;
    }

    /**
     * Setter de l'attribut pmRepresentantNomNaissance.
     * 
     * @param pmRepresentantNomNaissance
     *            la nouvelle valeur de pmRepresentantNomNaissance
     */
    public void setPmRepresentantNomNaissance(String pmRepresentantNomNaissance) {
        this.pmRepresentantNomNaissance = pmRepresentantNomNaissance;
    }

    /**
     * Setter de l'attribut pmRepresentantNomUsage.
     * 
     * @param pmRepresentantNomUsage
     *            la nouvelle valeur de pmRepresentantNomUsage
     */
    public void setPmRepresentantNomUsage(String pmRepresentantNomUsage) {
        this.pmRepresentantNomUsage = pmRepresentantNomUsage;
    }

    /**
     * Setter de l'attribut pmRepresentantPMPrenom1.
     * 
     * @param pmRepresentantPMPrenom1
     *            la nouvelle valeur de pmRepresentantPMPrenom1
     */
    public void setPmRepresentantPMPrenom1(String pmRepresentantPMPrenom1) {
        this.pmRepresentantPMPrenom1 = pmRepresentantPMPrenom1;
    }

    /**
     * Setter de l'attribut pmRepresentantPMPrenom2.
     * 
     * @param pmRepresentantPMPrenom2
     *            la nouvelle valeur de pmRepresentantPMPrenom2
     */
    public void setPmRepresentantPMPrenom2(String pmRepresentantPMPrenom2) {
        this.pmRepresentantPMPrenom2 = pmRepresentantPMPrenom2;
    }

    /**
     * Setter de l'attribut pmRepresentantPMPrenom3.
     * 
     * @param pmRepresentantPMPrenom3
     *            la nouvelle valeur de pmRepresentantPMPrenom3
     */
    public void setPmRepresentantPMPrenom3(String pmRepresentantPMPrenom3) {
        this.pmRepresentantPMPrenom3 = pmRepresentantPMPrenom3;
    }

    /**
     * Setter de l'attribut pmRepresentantPMPrenom4.
     * 
     * @param pmRepresentantPMPrenom4
     *            la nouvelle valeur de pmRepresentantPMPrenom4
     */
    public void setPmRepresentantPMPrenom4(String pmRepresentantPMPrenom4) {
        this.pmRepresentantPMPrenom4 = pmRepresentantPMPrenom4;
    }

    /**
     * Setter de l'attribut pmRepresentantQualite.
     * 
     * @param pmRepresentantQualite
     *            la nouvelle valeur de pmRepresentantQualite
     */
    public void setPmRepresentantQualite(String pmRepresentantQualite) {
        this.pmRepresentantQualite = pmRepresentantQualite;
    }

    /**
     * Accesseur sur l'attribut libelle qualite.
     *
     * @return libelle qualite
     */
    public String getLibelleQualite() {
        return libelleQualite;
    }

    /**
     * Mutateur sur l'attribut libelle qualite.
     *
     * @param libelleQualite
     *            le nouveau libelle qualite
     */
    public void setLibelleQualite(String libelleQualite) {
        this.libelleQualite = libelleQualite;
    }

    /**
     * Setter de l'attribut qualite.
     * 
     * @param qualite
     *            la nouvelle valeur de qualite
     */
    public void setQualite(String qualite) {
        this.qualite = qualite;
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see fr.guichetentreprises.vue.AbstractDirigeantBean#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see fr.guichetentreprises.vue.IConjointFactory#createNewConjoint()
     */
    @Override
    public ConjointBean createNewConjoint() {
        return new ConjointBean();
    }

}
