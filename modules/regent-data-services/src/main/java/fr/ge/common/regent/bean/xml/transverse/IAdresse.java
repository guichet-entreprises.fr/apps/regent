package fr.ge.common.regent.bean.xml.transverse;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IAdresse.
 */
@ResourceXPath("/adresse")
public interface IAdresse {

  /**
   * Get le code pays.
   *
   * @return le code pays
   */
  @FieldXPath(value = "codePays")
  String getCodePays();

  /**
   * Get le code postal commune.
   *
   * @return le code postal commune
   */
  @FieldXPath("codePostalCommune")
  IPostalCommune getCodePostalCommune();

  /**
   * New code postal commune.
   *
   * @return le i postal commune
   */
  IPostalCommune newCodePostalCommune();

  /**
   * Get le complement adresse.
   *
   * @return le complement adresse
   */
  @FieldXPath(value = "complementAdresse")
  String getComplementAdresse();

  /**
   * Get le distribution speciale.
   *
   * @return le distribution speciale
   */
  @FieldXPath(value = "distributionSpeciale")
  String getDistributionSpeciale();

  /**
   * Get le indice repetition.
   *
   * @return le indice repetition
   */
  @FieldXPath(value = "indiceRepetition")
  String getIndiceRepetition();

  /**
   * Get le nom voie.
   *
   * @return le nom voie
   */
  @FieldXPath(value = "nomVoie")
  String getNomVoie();

  /**
   * Get le numero voie.
   *
   * @return le numero voie
   */
  @FieldXPath(value = "numeroVoie")
  String getNumeroVoie();

  /**
   * Get le type voie.
   *
   * @return le type voie
   */
  @FieldXPath(value = "typeVoie")
  String getTypeVoie();

  /**
   * Get le ville.
   *
   * @return le ville
   */
  @FieldXPath(value = "ville")
  String getVille();

  /**
   * Set le code pays.
   *
   * @param codePays
   *          le nouveau code pays
   */
  void setCodePays(String codePays);

  /**
   * Set le code postal commune.
   *
   * @param codePostalCommune
   *          le nouveau code postal commune
   */
  void setCodePostalCommune(IPostalCommune codePostalCommune);

  /**
   * Set le complement adresse.
   *
   * @param complementAdresse
   *          le nouveau complement adresse
   */
  void setComplementAdresse(String complementAdresse);

  /**
   * Set le distribution speciale.
   *
   * @param distributionSpeciale
   *          le nouveau distribution speciale
   */
  void setDistributionSpeciale(String distributionSpeciale);

  /**
   * Set le indice repetition.
   *
   * @param indiceRepetition
   *          le nouveau indice repetition
   */
  void setIndiceRepetition(String indiceRepetition);

  /**
   * Set le nom voie.
   *
   * @param nomVoie
   *          le nouveau nom voie
   */
  void setNomVoie(String nomVoie);

  /**
   * Set le numero voie.
   *
   * @param numeroVoie
   *          le nouveau numero voie
   */
  void setNumeroVoie(String numeroVoie);

  /**
   * Set le type voie.
   *
   * @param typeVoie
   *          le nouveau type voie
   */
  void setTypeVoie(String typeVoie);

  /**
   * Set le ville.
   *
   * @param ville
   *          le nouveau ville
   */
  void setVille(String ville);

}
