package fr.ge.common.regent.util.mapper.formalite.dialogue;

import java.util.List;
import java.util.UUID;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xmlfield.core.types.XmlString;

import fr.ge.common.regent.bean.formalite.dialogue.creation.ActiviteSoumiseQualificationBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.AdresseBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.AutreEtablissementUEBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.AyantDroitBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.ConjointBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.DeclarationSocialeBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.DialogueCreationBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.DirigeantBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.EirlBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.EntrepriseBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.EntrepriseLieeBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.EtablissementBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.PersonneLieeBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.PostalCommuneBean;
import fr.ge.common.regent.bean.xml.formalite.dialogue.creation.IActiviteSoumiseQualificationCreation;
import fr.ge.common.regent.bean.xml.formalite.dialogue.creation.IAutreEtablissementUECreation;
import fr.ge.common.regent.bean.xml.formalite.dialogue.creation.IAyantDroit;
import fr.ge.common.regent.bean.xml.formalite.dialogue.creation.IDialogueCreation;
import fr.ge.common.regent.bean.xml.formalite.dialogue.creation.IDirigeantCreation;
import fr.ge.common.regent.bean.xml.formalite.dialogue.creation.IEirlCreation;
import fr.ge.common.regent.bean.xml.formalite.dialogue.creation.IEntrepriseCreation;
import fr.ge.common.regent.bean.xml.formalite.dialogue.creation.IEntrepriseLieeCreation;
import fr.ge.common.regent.bean.xml.formalite.dialogue.creation.IEtablissementCreation;
import fr.ge.common.regent.bean.xml.formalite.dialogue.creation.IPersonneLieeCreation;
import fr.ge.common.regent.bean.xml.formalite.dialogue.creation.IRegimeFiscal;
import fr.ge.common.regent.bean.xml.transverse.IAdresse;
import fr.ge.common.regent.util.mapper.formalite.IMapperFormaliteUtils;

/**
 * Le Class MapperFormaliteCreationUtils.
 */

@Component
public class MapperDialogueCreationUtils implements IMapperFormaliteUtils<IDialogueCreation, DialogueCreationBean> {

    /** formalite mapper. */
    @Autowired
    private Mapper formaliteMapper;

    /** Le adresse création mapper. */
    @Autowired
    private Mapper adresseMapper;

    /** Le entreprise création mapper. */
    @Autowired
    private Mapper entrepriseMapper;

    /** Le etablissement création mapper. */
    @Autowired
    private Mapper etablissementMapper;

    /** Mapper de l'activité soumise à qualification. **/
    @Autowired
    private Mapper activiteSoumiseQualificationMapper;

    /** Le postal commune création mapper. */
    @Autowired
    private Mapper postalCommuneMapper;

    /** Le entreprise liee création mapper. */
    @Autowired
    private Mapper entrepriseLieeMapper;

    /** Le autre eirl création mapper. */
    @Autowired
    private Mapper eirlMapper;

    /** Le dirigeant création mapper. */
    @Autowired
    private Mapper dirigeantMapper;

    /** Le conjoint création mapper. */
    @Autowired
    private Mapper conjointMapper;

    /** Le declarationsociale création mapper. */
    @Autowired
    private Mapper declarationSocialeMapper;

    /** Le ayantsDroit création mapper. */
    @Autowired
    private Mapper ayantDroitMapper;

    /** Le personne liee création mapper. */
    @Autowired
    private Mapper personneLieeMapper;

    /** Le autre établissement céation mapper. */
    @Autowired
    private Mapper autreEtablissementUEMapper;

    /** Le autre regimeFiscalMapper céation mapper. */
    @Autowired
    private Mapper regimeFiscalMapper;

    /** mapper entreprise creation utils. */
    @Autowired
    private MapperEntrepriseCreationUtils mapperEntrepriseCreationUtils;

    /**
     * mapper pour préparer un bean depuis une interface (stocké en base).
     *
     * @param iFormaliteCréation
     *            le i formalite création// entite en base
     * @param formaliteCreationBean
     *            le formalite création bean // bean d'affichage
     */
    @Override
    public void mapper(IDialogueCreation iFormaliteCreation, DialogueCreationBean formaliteCreationBean) {
        if (iFormaliteCreation != null) {
            // Mapper Adresse signature
            mapperAdresseSignature(iFormaliteCreation, formaliteCreationBean);

            // Mapper Correspondance Adresse
            mapperCorrespondanceAdresse(iFormaliteCreation, formaliteCreationBean);

            // Mapper Entreprise
            // Méthode déplacée dans MapperEntrepriseCreationUtils
            // mapperEntreprise(iFormaliteCreation,
            // formaliteCreationBean);
            this.mapperEntrepriseCreationUtils.mapperEntreprise(iFormaliteCreation, formaliteCreationBean);

            formaliteCreationBean.setTerminee(iFormaliteCreation.getTerminee());

            /* mapping champs simples */
            this.formaliteMapper.map(iFormaliteCreation, formaliteCreationBean);
        }
    }

    /**
     * Mapper Correspondance Adresse Interface Xml Field vers Bean
     * 
     * @param iFormaliteCreation
     *            {@link IDialogueCreation}
     * @param formaliteCreationBean
     *            {@link DialogueCreationBean}
     */
    private void mapperCorrespondanceAdresse(IDialogueCreation iFormaliteCreation, DialogueCreationBean formaliteCreationBean) {
        IAdresse iCorrespondanceAdresse = iFormaliteCreation.getCorrespondanceAdresse();
        if (iCorrespondanceAdresse != null) {
            AdresseBean iCorrAdresseBean = formaliteCreationBean.getCorrespondanceAdresse();
            this.adresseMapper.map(iCorrespondanceAdresse, iCorrAdresseBean);
            if (iCorrespondanceAdresse.getCodePostalCommune() != null) {
                this.postalCommuneMapper.map(iCorrespondanceAdresse.getCodePostalCommune(), iCorrAdresseBean.getCodePostalCommune());
            }
        }
    }

    /**
     * Mapper Adresse Signature Interface Xml Field vers Bean
     * 
     * @param iFormaliteCreation
     *            {@link IDialogueCreation}
     * @param formaliteCreationBean
     *            {@link DialogueCreationBean}
     */
    private void mapperAdresseSignature(IDialogueCreation iFormaliteCreation, DialogueCreationBean formaliteCreationBean) {
        IAdresse iAdresseSignature = iFormaliteCreation.getAdresseSignature();
        if (iAdresseSignature != null) {
            AdresseBean adresseSignatureBean = formaliteCreationBean.getAdresseSignature();
            this.adresseMapper.map(iAdresseSignature, adresseSignatureBean);
            if (iAdresseSignature.getCodePostalCommune() != null) {
                this.postalCommuneMapper.map(iAdresseSignature.getCodePostalCommune(), adresseSignatureBean.getCodePostalCommune());
            }
        }
    }

    /**
     * Mapper {@link DialogueCreationBean} en {@link IDialogueCreation}
     *
     * @param formaliteCreationBean
     *            {@link DialogueCreationBean} : le formalite création bean
     * @param iFormaliteCreation
     *            {@link IDialogueCreation} : le i formalite création
     */
    @Override
    public void mapper(DialogueCreationBean formaliteCreationBean, IDialogueCreation iFormaliteCreation) {

        // Mapper Adresse signature
        mapperAdresseSignature(formaliteCreationBean, iFormaliteCreation);

        // Mapper Correspondance Adresse
        mapperCorrespondanceAdresse(formaliteCreationBean, iFormaliteCreation);

        // Mapper Entreprise : FIXME : méthode à déplcare dans
        // MapperEntrepriseCreationUtils
        mapperEntreprise(formaliteCreationBean, iFormaliteCreation);

        // Mapper Les champs simples Formalite
        this.formaliteMapper.map(formaliteCreationBean, iFormaliteCreation);

    }

    /**
     * Mapper Entreprise Bean vers Ineterface Xml Field
     * 
     * @param formaliteCreationBean
     *            {@link DialogueCreationBean}
     * @param iFormaliteCreation
     *            {@link IDialogueCreation}
     */
    private void mapperEntreprise(DialogueCreationBean formaliteCreationBean, IDialogueCreation iFormaliteCreation) {
        iFormaliteCreation.newEntreprise();
        IEntrepriseCreation iEntreprise = iFormaliteCreation.getEntreprise();
        EntrepriseBean entrepriseBean = formaliteCreationBean.getEntreprise();
        if (entrepriseBean != null) {
            // Mapper l'adresse de l'entreprise
            mapperAdresseEntreprise(entrepriseBean, iEntreprise);

            // Mapper Entreprise responsable du contrat d'appui
            mapperEntrepriseResponsableContratAppui(entrepriseBean, iEntreprise);

            // Mapper EntrepriseLieeDomiciliation
            EntrepriseLieeBean entrepriseLieeDomiciliationBean = mapperEntrepriseLieeDomiciliation(entrepriseBean, iEntreprise);

            // Mapper EntrepriseLieeLoueurMandant
            mapperEntrepriseLoueurMondant(entrepriseBean, iEntreprise, entrepriseLieeDomiciliationBean);

            // Mapper Etablissement
            mapperEtablissement(entrepriseBean, iEntreprise);

            // Mapper Eirl
            mapperEirl(entrepriseBean, iEntreprise);

            // Mapper Regime Fiscal Eirl
            mapperRegimeFiscalEirl(entrepriseBean, iEntreprise);

            // Mapper Regime Fiscal Eirl Secondaire
            mapperRegimeFiscalEirlSecondaire(entrepriseBean, iEntreprise);

            // Mapper Regime Fiscal Eirl Etablissement
            mapperRegimeFiscalEtablissement(entrepriseBean, iEntreprise);

            // Mapper Regime Fiscal Eirl Etablissement Secondaire
            mapperRegimeFiscalEtablissementSecondaire(entrepriseBean, iEntreprise);

            // Mapper les champs Simples Entreprise
            this.entrepriseMapper.map(entrepriseBean, iEntreprise);

            /** Mapper les listes pour entreprise **/

            // Mapper la liste des dirigeants
            if (!CollectionUtils.isEmpty(entrepriseBean.getDirigeants())) {
                this.addDirigeant(entrepriseBean, iEntreprise);
            }

            // Mapper la liste des EntrepriseLieePrecedentiExploitant
            if (!CollectionUtils.isEmpty(entrepriseBean.getEntreprisesLieesPrecedentsExploitants())) {
                this.addEntrepriseLieePrecedentiExploitant(entrepriseBean, iEntreprise);
            }

            // Mapper la liste des EntrepriseLieeEntrepriseLieeFusionScission
            if (!CollectionUtils.isEmpty(entrepriseBean.getEntreprisesLieesFusionScission())) {
                this.addEntrepriseLieeEntrepriseLieeFusionScission(entrepriseBean, iEntreprise);
            }

            // Mapper la liste des PersonneLiee
            if (!CollectionUtils.isEmpty(entrepriseBean.getPersonneLiee())) {
                this.addPersonneLiee(entrepriseBean, iEntreprise);
            }

            // Mapper la liste des EtablissementsUE
            if (!CollectionUtils.isEmpty(entrepriseBean.getEtablissementsUE())) {
                this.addEtablissementsUE(entrepriseBean, iEntreprise);
            }

            // Mapper la liste des CapitalApportsNature
            if (!CollectionUtils.isEmpty(entrepriseBean.getCapitalApportsNature())) {
                this.addCapitalApportsNature(entrepriseBean, iEntreprise);
            }
        }
    }

    /**
     * Mapper Regime Fiscal Eirl Etablissement Secondaire
     * 
     * @param entrepriseBean
     *            {@link EntrepriseBean}
     * @param iEntreprise
     *            {@link IEntrepriseCreation}
     */
    private void mapperRegimeFiscalEtablissementSecondaire(EntrepriseBean entrepriseBean, IEntrepriseCreation iEntreprise) {
        iEntreprise.newRegimeFiscalEtablissementSecondaire();
        IRegimeFiscal iRegimeEtablissementSecondaire = iEntreprise.getRegimeFiscalEtablissementSecondaire();
        if (entrepriseBean.getRegimeFiscalEtablissementSecondaire() != null) {
            this.regimeFiscalMapper.map(entrepriseBean.getRegimeFiscalEtablissementSecondaire(), iRegimeEtablissementSecondaire);
        }
    }

    /**
     * Mapper Regime Fiscal Eirl Etablissement
     * 
     * @param entrepriseBean
     *            {@link EntrepriseBean}
     * @param iEntreprise
     *            {@link IEntrepriseCreation}
     */
    private void mapperRegimeFiscalEtablissement(EntrepriseBean entrepriseBean, IEntrepriseCreation iEntreprise) {
        iEntreprise.newRegimeFiscalEtablissement();
        IRegimeFiscal iRegimeFiscalEtablissement = iEntreprise.getRegimeFiscalEtablissement();

        if (entrepriseBean.getRegimeFiscalEtablissement() != null) {
            this.regimeFiscalMapper.map(entrepriseBean.getRegimeFiscalEtablissement(), iRegimeFiscalEtablissement);
        }
    }

    /**
     * Mapper Regime Fiscal Eirl Secondaire
     * 
     * @param entrepriseBean
     *            {@link EntrepriseBean}
     * @param iEntreprise
     *            {@link IEntrepriseCreation}
     */
    private void mapperRegimeFiscalEirlSecondaire(EntrepriseBean entrepriseBean, IEntrepriseCreation iEntreprise) {
        iEntreprise.newRegimeFiscalEirlSecondaire();
        IRegimeFiscal iRegimeEirlSecondaire = iEntreprise.getRegimeFiscalEirlSecondaire();
        if (entrepriseBean.getRegimeFiscalEirlSecondaire() != null) {
            this.regimeFiscalMapper.map(entrepriseBean.getRegimeFiscalEirlSecondaire(), iRegimeEirlSecondaire);
        }
    }

    /**
     * Mapper Regime fiscal Eirl Bean vers Interface XML Field
     * 
     * @param entrepriseBean
     *            {@link EntrepriseBean}
     * @param iEntreprise
     *            {@link IEntrepriseCreation}
     */
    private void mapperRegimeFiscalEirl(EntrepriseBean entrepriseBean, IEntrepriseCreation iEntreprise) {
        iEntreprise.newRegimeFiscalEirl();
        IRegimeFiscal iRegimeEirl = iEntreprise.getRegimeFiscalEirl();

        if (entrepriseBean.getRegimeFiscalEirl() != null) {
            this.regimeFiscalMapper.map(entrepriseBean.getRegimeFiscalEirl(), iRegimeEirl);
        }
    }

    /**
     * Mapper Eirl Bean vers Interface XML Field
     * 
     * @param entrepriseBean
     *            {@link EntrepriseBean}
     * @param iEntreprise
     *            {@link IEntrepriseCreation}
     */
    private void mapperEirl(EntrepriseBean entrepriseBean, IEntrepriseCreation iEntreprise) {
        // iEntreprise.newEirl();
        EirlBean eirlBean = entrepriseBean.getEirl();
        if (null != eirlBean) {
            IEirlCreation iEirlCreation = iEntreprise.newEirl();
            /** mapper les champs simples de EIRL **/
            this.eirlMapper.map(eirlBean, iEirlCreation);

            for (XmlString definitionPatrimoine : iEirlCreation.getDefinitionPatrimoine()) {
                iEirlCreation.removeFromDefinitionPatrimoine(definitionPatrimoine);
            }
            for (String modifIdentiteNomPrenomCourant : eirlBean.getDefinitionPatrimoine()) {
                XmlString modifIdentiteNomPrenom = iEirlCreation.addToDefinitionPatrimoine();
                modifIdentiteNomPrenom.setString(modifIdentiteNomPrenomCourant);
            }
        }
    }

    /**
     * Mapper Etablissement Bean vers Interface XML Field.
     * 
     * @param entrepriseBean
     *            {@link EntrepriseBean}
     * @param iEntreprise
     *            {@link IEntrepriseCreation}
     */
    public void mapperEtablissement(EntrepriseBean entrepriseBean, IEntrepriseCreation iEntreprise) {
        iEntreprise.newEtablissement();
        IEtablissementCreation iEtablissement = iEntreprise.getEtablissement();

        iEtablissement.newAdresse();
        iEtablissement.getAdresse().newCodePostalCommune();

        EtablissementBean etablissementBean = entrepriseBean.getEtablissement();
        if (etablissementBean != null) {
            AdresseBean adresseEtablissementBean = etablissementBean.getAdresse();
            if (adresseEtablissementBean != null) {
                this.adresseMapper.map(adresseEtablissementBean, iEtablissement.getAdresse());
                this.postalCommuneMapper.map(etablissementBean.getAdresse().getCodePostalCommune(), iEtablissement.getAdresse().getCodePostalCommune());
            }
        }

        if (etablissementBean != null) {
            this.etablissementMapper.map(etablissementBean, iEtablissement);
        }
        if (!CollectionUtils.isEmpty(etablissementBean.getActivitesExerceesAgricole())) {
            this.addActivitesExerceesAgricole(etablissementBean, iEtablissement);
        }
        if (!CollectionUtils.isEmpty(etablissementBean.getActivitesSoumisesQualification())) {
            this.addActivitesSoumisesQualification(etablissementBean, iEtablissement);
        }
    }

    /**
     * Mapper E,treprise Loueur Mondant Bean vers Interface Xml Field.
     * 
     * @param entrepriseBean
     *            {@link EntrepriseBean}
     * @param iEntreprise
     *            {@link IEntrepriseCreation}
     * @param entrepriseLieeDomiciliationBean
     *            {@link EntrepriseLieeBean}
     */
    private void mapperEntrepriseLoueurMondant(EntrepriseBean entrepriseBean, IEntrepriseCreation iEntreprise, EntrepriseLieeBean entrepriseLieeDomiciliationBean) {
        iEntreprise.newEntrepriseLieeLoueurMandant();
        IEntrepriseLieeCreation iEntrepriseLieeMandant = iEntreprise.getEntrepriseLieeLoueurMandant();
        iEntrepriseLieeMandant.newAdresse();
        iEntrepriseLieeMandant.getAdresse().newCodePostalCommune();
        EntrepriseLieeBean entrepriseLieeMandantBean = entrepriseBean.getEntrepriseLieeLoueurMandant();
        if (entrepriseLieeDomiciliationBean != null && entrepriseLieeMandantBean != null) {
            AdresseBean adresseEntrLieeMandantBean = entrepriseLieeMandantBean.getAdresse();
            if (entrepriseLieeMandantBean != null) {
                this.adresseMapper.map(adresseEntrLieeMandantBean, iEntrepriseLieeMandant.getAdresse());
                this.postalCommuneMapper.map(entrepriseLieeMandantBean.getAdresse().getCodePostalCommune(), iEntrepriseLieeMandant.getAdresse().getCodePostalCommune());
            }
        }
    }

    /**
     * Mapper Entreprise Liee Domiciliation Bean vers Interface Xml Field
     * 
     * @param entrepriseBean
     *            {@link EntrepriseBean}
     * @param iEntreprise
     *            {@link IEntrepriseCreation}
     * @return {@link EntrepriseLieeBean}
     */
    private EntrepriseLieeBean mapperEntrepriseLieeDomiciliation(EntrepriseBean entrepriseBean, IEntrepriseCreation iEntreprise) {
        iEntreprise.newEntrepriseLieeDomiciliation();
        IEntrepriseLieeCreation iEntrepriseLieeDomiciliation = iEntreprise.getEntrepriseLieeDomiciliation();
        iEntrepriseLieeDomiciliation.newAdresse();
        iEntrepriseLieeDomiciliation.getAdresse().newCodePostalCommune();
        EntrepriseLieeBean entrepriseLieeDomiciliationBean = entrepriseBean.getEntrepriseLieeDomiciliation();
        if (entrepriseLieeDomiciliationBean != null) {
            AdresseBean adresseEntrLieeDomBean = entrepriseLieeDomiciliationBean.getAdresse();
            if (adresseEntrLieeDomBean != null) {
                this.adresseMapper.map(adresseEntrLieeDomBean, iEntrepriseLieeDomiciliation.getAdresse());
                this.postalCommuneMapper.map(entrepriseLieeDomiciliationBean.getAdresse().getCodePostalCommune(), iEntrepriseLieeDomiciliation.getAdresse().getCodePostalCommune());
            }
        }
        return entrepriseLieeDomiciliationBean;
    }

    /**
     * Mapper Entreprise Responsable Contrat Appui Bean Vers Interface XML Field
     * 
     * @param entrepriseBean
     *            {@link EntrepriseBean}
     * @param iEntreprise
     *            {@link IEntrepriseCreation}
     */
    private void mapperEntrepriseResponsableContratAppui(EntrepriseBean entrepriseBean, IEntrepriseCreation iEntreprise) {
        iEntreprise.newEntrepriseLieeContratAppui();
        IEntrepriseLieeCreation iEntrepriseLieeContratAppuiCreation = iEntreprise.getEntrepriseLieeContratAppui();
        iEntrepriseLieeContratAppuiCreation.newAdresse();
        iEntrepriseLieeContratAppuiCreation.getAdresse().newCodePostalCommune();
        EntrepriseLieeBean entrepriseLieeContratAppuiBean = entrepriseBean.getEntrepriseLieeContratAppui();
        if (entrepriseLieeContratAppuiBean != null) {

            AdresseBean adresseEntrLieeContAppBean = entrepriseLieeContratAppuiBean.getAdresse();
            if (adresseEntrLieeContAppBean != null) {

                this.adresseMapper.map(adresseEntrLieeContAppBean, iEntrepriseLieeContratAppuiCreation.getAdresse());
                this.postalCommuneMapper.map(adresseEntrLieeContAppBean.getCodePostalCommune(), iEntrepriseLieeContratAppuiCreation.getAdresse().getCodePostalCommune());
            }
        }
    }

    /**
     * Mapper Adresse entreprise Bean Vers Interface XML Field
     * 
     * @param entrepriseBean
     *            {@link EntrepriseBean}
     * @param iEntreprise
     *            {@link IEntrepriseCreation}
     */
    private void mapperAdresseEntreprise(EntrepriseBean entrepriseBean, IEntrepriseCreation iEntreprise) {
        iEntreprise.newAdresse();
        IAdresse iAdresseEntreprise = iEntreprise.getAdresse();
        this.adresseMapper.map(entrepriseBean.getAdresse(), iAdresseEntreprise);
        iAdresseEntreprise.newCodePostalCommune();
        this.postalCommuneMapper.map(entrepriseBean.getAdresse().getCodePostalCommune(), iAdresseEntreprise.getCodePostalCommune());
    }

    /**
     * Mapper Correspondance Adresse Bean vers Interface Xml Field
     * 
     * @param formaliteCreationBean
     *            {@link DialogueCreationBean}
     * @param iFormaliteCreation
     *            {@link IDialogueCreation}
     */
    private void mapperCorrespondanceAdresse(DialogueCreationBean formaliteCreationBean, IDialogueCreation iFormaliteCreation) {
        iFormaliteCreation.newCorrespondanceAdresse();
        IAdresse icorrespondanceAdresse = iFormaliteCreation.getCorrespondanceAdresse();
        this.adresseMapper.map(formaliteCreationBean.getCorrespondanceAdresse(), icorrespondanceAdresse);
        icorrespondanceAdresse.newCodePostalCommune();
        this.postalCommuneMapper.map(formaliteCreationBean.getCorrespondanceAdresse().getCodePostalCommune(), icorrespondanceAdresse.getCodePostalCommune());
    }

    /**
     * Mapper L'adresse Signature Bean vers Interface
     * 
     * @param formaliteCreationBean
     *            {@link DialogueCreationBean}
     * @param iFormaliteCreation
     *            {@link IDialogueCreation}
     */
    private void mapperAdresseSignature(DialogueCreationBean formaliteCreationBean, IDialogueCreation iFormaliteCreation) {
        iFormaliteCreation.newAdresseSignature();
        IAdresse iAdresseSignature = iFormaliteCreation.getAdresseSignature();
        this.adresseMapper.map(formaliteCreationBean.getAdresseSignature(), iAdresseSignature);
        iAdresseSignature.newCodePostalCommune();
        this.postalCommuneMapper.map(formaliteCreationBean.getAdresseSignature().getCodePostalCommune(), iAdresseSignature.getCodePostalCommune());
    }

    /**
     * Mapper de {@link EntrepriseBean} vers {@link IEntrepriseCreation} la
     * liste des CapitalApportsNature
     * 
     * @param entrepriseBean
     *            {@link EntrepriseBean}
     * @param iEntreprise
     *            {@link IEntrepriseCreation}
     */
    private void addCapitalApportsNature(EntrepriseBean entrepriseBean, IEntrepriseCreation iEntreprise) {
        /*
         * parcourir la liste des modifIdentiteNomPrenom existants sur
         * l'interface XML et les supprimer
         */
        for (XmlString capitalApportsNature : iEntreprise.getCapitalApportsNature()) {
            iEntreprise.removeFromCapitalApportsNature(capitalApportsNature);
        }
        /*
         * parcourir la liste des évenements dans le Bean pour les rajouter à
         * l'interface
         */
        for (String capitalApportsNatureBean : entrepriseBean.getCapitalApportsNature()) {
            XmlString capitalApportsNatureI = iEntreprise.addToCapitalApportsNature();
            capitalApportsNatureI.setString(capitalApportsNatureBean);
        }

    }

    /**
     * Adds the activites exercees agricole.
     *
     * @param etablissementBean
     *            etablissement bean
     * @param iEtablissement
     *            i etablissement
     */
    private void addActivitesExerceesAgricole(EtablissementBean etablissementBean, IEtablissementCreation iEtablissement) {
        /*
         * parcourir la liste des modifIdentiteNomPrenom existants sur
         * l'interface XML et les supprimer
         */
        for (XmlString activiteExerceesAgricole : iEtablissement.getActivitesExerceesAgricole()) {
            iEtablissement.removeFromActivitesExerceesAgricole(activiteExerceesAgricole);
        }
        /*
         * parcourir la liste des évenements dans le Bean pour les rajouter à
         * l'interface
         */
        for (String activiteExerceesAgricoleB : etablissementBean.getActivitesExerceesAgricole()) {
            XmlString activiteExerceesAgricoleI = iEtablissement.addToActivitesExerceesAgricole();
            activiteExerceesAgricoleI.setString(activiteExerceesAgricoleB);
        }

    }

    /**
     * Adds the activites soumises à qualification.
     * 
     * @param etablissementBean
     *            {@link EtablissementBean}
     * @param iEtablissement
     *            {@link IEtablissementCreation}
     */
    public void addActivitesSoumisesQualification(EtablissementBean etablissementBean, IEtablissementCreation iEtablissement) {
        /*
         * parcourir la liste existants sur l'interface XML et les supprimer
         */
        for (IActiviteSoumiseQualificationCreation iActiviteSoumiseQualification : iEtablissement.getActivitesSoumisesQualification()) {
            iEtablissement.removeFromActivitesSoumisesQualification(iActiviteSoumiseQualification);
        }
        /*
         * parcourir la liste des évenements dans le Bean pour les rajouter à
         * l'interface
         */

        for (ActiviteSoumiseQualificationBean activiteSoumiseQualificationBean : etablissementBean.getActivitesSoumisesQualification()) {
            IActiviteSoumiseQualificationCreation iActiviteSoumiseQualification = iEtablissement.addToActivitesSoumisesQualification();
            // DEBUT- Mapper iActiviteSoumiseQualification
            // Gestion idTechnique, nécessaire pour la gestion des impacts
            // (écran)
            if (activiteSoumiseQualificationBean.getIdTechnique() == null || activiteSoumiseQualificationBean.getIdTechnique().isEmpty()) {
                activiteSoumiseQualificationBean.setIdTechnique(UUID.randomUUID().toString());
            }

            if (activiteSoumiseQualificationBean != null) {
                this.activiteSoumiseQualificationMapper.map(activiteSoumiseQualificationBean, iActiviteSoumiseQualification);
            }
        }
    }

    /**
     * Ajoute le dirigeant.
     *
     * @param entrepriseBean
     *            le entreprise bean
     * @param iEntreprise
     *            le i entreprise
     */
    private void addDirigeant(EntrepriseBean entrepriseBean, IEntrepriseCreation iEntreprise) {

        /*
         * parcourir la liste des Dirigeant existants sur l'interface XML et les
         * supprimer
         */
        IDirigeantCreation[] iDirigeantList = iEntreprise.getDirigeants();
        for (IDirigeantCreation iDirigeantCourant : iDirigeantList) {
            iEntreprise.removeFromDirigeants(iDirigeantCourant);
        }
        /*
         * parcourir la liste des Dirigeant dans le Bean pour les rajouter à
         * l'interface
         */
        List<DirigeantBean> dirigeantListBean = entrepriseBean.getDirigeants();
        for (DirigeantBean dirigeantBean : dirigeantListBean) {
            // Gestion idTechnique, nécessaire pour la gestion des impacts
            // (écran)
            if (StringUtils.isBlank(dirigeantBean.getIdTechnique())) {
                dirigeantBean.setIdTechnique(UUID.randomUUID().toString());
            }

            IDirigeantCreation iDirigeant = iEntreprise.addToDirigeants();
            // DEBUT - Mapper de conjoint du dirigeant
            iDirigeant.newConjoint();
            iDirigeant.getConjoint().newPpAdresse();
            iDirigeant.getConjoint().getPpAdresse().newCodePostalCommune();

            if (dirigeantBean.getConjoint() == null) {
                dirigeantBean.setConjoint(new ConjointBean());
            }

            this.mappingDirigeantAdresse(dirigeantBean, iDirigeant);

            // Mapper ayantsDroit

            // DEBUT - Mapper Dirigeant
            iDirigeant.newDeclarationSociale();
            if (dirigeantBean.getDeclarationSociale() == null) {
                dirigeantBean.setDeclarationSociale(new DeclarationSocialeBean());
            }
            this.declarationSocialeMapper.map(dirigeantBean.getDeclarationSociale(), iDirigeant.getDeclarationSociale());

            /*
             * parcourir la liste des Dirigeant dans le Bean pour les rajouter à
             * l'interface
             */

            List<AyantDroitBean> ayantsDroitBean = dirigeantBean.getDeclarationSociale().getAyantDroits();
            // -->SONAR
            this.mappingAyantDroitList(iDirigeant, ayantsDroitBean);
            // Mapper Simple de Dirigeant
            this.dirigeantMapper.map(dirigeantBean, iDirigeant);
            // FIN - Mapper Dirigeant
        }

    }

    /**
     * Mapping des données de type Adresse
     * 
     * @param dirigeantBean
     * @param iDirigeant
     */
    private void mappingDirigeantAdresse(DirigeantBean dirigeantBean, IDirigeantCreation iDirigeant) {
        if (dirigeantBean.getConjoint().getPpAdresse() == null) {
            dirigeantBean.getConjoint().setPpAdresse(new AdresseBean());
        }

        this.adresseMapper.map(dirigeantBean.getConjoint().getPpAdresse(), iDirigeant.getConjoint().getPpAdresse());
        this.postalCommuneMapper.map(dirigeantBean.getConjoint().getPpAdresse().getCodePostalCommune(), iDirigeant.getConjoint().getPpAdresse().getCodePostalCommune());

        this.conjointMapper.map(dirigeantBean.getConjoint(), iDirigeant.getConjoint());
        // PP-Adresse
        iDirigeant.newPpAdresse();
        iDirigeant.getPpAdresse().newCodePostalCommune();
        if (dirigeantBean.getPpAdresse() == null) {
            dirigeantBean.setPpAdresse(new AdresseBean());
        }
        this.adresseMapper.map(dirigeantBean.getPpAdresse(), iDirigeant.getPpAdresse());
        this.postalCommuneMapper.map(dirigeantBean.getPpAdresse().getCodePostalCommune(), iDirigeant.getPpAdresse().getCodePostalCommune());

        // PP-Adresse
        iDirigeant.newPmAdresse();
        iDirigeant.getPmAdresse().newCodePostalCommune();
        if (dirigeantBean.getPmAdresse() == null) {
            dirigeantBean.setPmAdresse(new AdresseBean());
        }
        this.adresseMapper.map(dirigeantBean.getPmAdresse(), iDirigeant.getPmAdresse());
        this.postalCommuneMapper.map(dirigeantBean.getPmAdresse().getCodePostalCommune(), iDirigeant.getPmAdresse().getCodePostalCommune());

        // Ambulant-Adresse
        iDirigeant.newPpAdresseAmbulant();
        if (dirigeantBean.getPpAdresseAmbulant() == null) {
            dirigeantBean.setPpAdresseAmbulant(new PostalCommuneBean());
        }
        this.postalCommuneMapper.map(dirigeantBean.getPpAdresseAmbulant(), iDirigeant.getPpAdresseAmbulant());

        // Forain-Adresse
        iDirigeant.newPpAdresseForain();
        if (dirigeantBean.getPpAdresseForain() == null) {
            dirigeantBean.setPpAdresseForain(new PostalCommuneBean());
        }
        this.postalCommuneMapper.map(dirigeantBean.getPpAdresseForain(), iDirigeant.getPpAdresseForain());

        // PM-Adresse
        iDirigeant.newPmRepresentantAdresse();
        iDirigeant.getPmRepresentantAdresse().newCodePostalCommune();
        if (dirigeantBean.getPmRepresentantAdresse() == null) {
            dirigeantBean.setPmRepresentantAdresse(new AdresseBean());
        }
        this.adresseMapper.map(dirigeantBean.getPmRepresentantAdresse(), iDirigeant.getPmRepresentantAdresse());
        this.postalCommuneMapper.map(dirigeantBean.getPmRepresentantAdresse().getCodePostalCommune(), iDirigeant.getPmRepresentantAdresse().getCodePostalCommune());
    }

    /**
     * Mapping AyantDroitList
     * 
     * @param iDirigeant
     * @param ayantsDroitBean
     */
    private void mappingAyantDroitList(IDirigeantCreation iDirigeant, List<AyantDroitBean> ayantsDroitBean) {
        for (AyantDroitBean ayantDroitBean : ayantsDroitBean) {

            // DEBUT- Mapper iEntrepriseLieePrecedentiExploitant // Gestion
            // idTechnique, nécessaire
            // pour la gestion des impacts // (écran)

            if (StringUtils.isBlank(ayantDroitBean.getIdTechnique())) {
                ayantDroitBean.setIdTechnique(UUID.randomUUID().toString());
            }

            IAyantDroit iyantDroit = iDirigeant.getDeclarationSociale().addToAyantDroits();
            // -->SONAR
            this.mappingAyantDroitBean(ayantDroitBean, iyantDroit);

            for (AyantDroitBean ayantDroitAyantDroitBean : ayantDroitBean.getAyantDroits()) {

                if (StringUtils.isBlank(ayantDroitAyantDroitBean.getIdTechnique())) {
                    ayantDroitAyantDroitBean.setIdTechnique(UUID.randomUUID().toString());
                }
                IAyantDroit iyanatDroitAyanatDroit = iyantDroit.addToAyantDroits();
                // -->SONAR
                this.mappingAyantDroitBean(ayantDroitAyantDroitBean, iyanatDroitAyanatDroit);
            }
        }
    }

    /**
     * Mapping Factorisation AyantDroitBean
     * 
     * @param ayantDroitBean
     * @param iyantDroit
     */
    private void mappingAyantDroitBean(AyantDroitBean ayantDroitBean, IAyantDroit iyantDroit) {
        iyantDroit.newAdresse();
        iyantDroit.getAdresse().newCodePostalCommune();

        if (null != ayantDroitBean) {
            if (null == ayantDroitBean.getAdresse()) {
                ayantDroitBean.setAdresse(new AdresseBean());
            }
            this.adresseMapper.map(ayantDroitBean.getAdresse(), iyantDroit.getAdresse());
            this.postalCommuneMapper.map(ayantDroitBean.getAdresse().getCodePostalCommune(), iyantDroit.getAdresse().getCodePostalCommune());

            this.ayantDroitMapper.map(ayantDroitBean, iyantDroit);
        }
    }

    /**
     * Ajoute le entreprise liee precedenti exploitant.
     *
     * @param entrepriseBean
     *            le entreprise bean
     * @param iEntreprise
     *            le i entreprise
     */
    private void addEntrepriseLieePrecedentiExploitant(EntrepriseBean entrepriseBean, IEntrepriseCreation iEntreprise) {

        /*
         * parcourir la liste des EntrepriseLieePrecedentiExploitant existants
         * sur l'interface XML et les supprimer
         */
        IEntrepriseLieeCreation[] entrLieePrecedentiExploitantList = iEntreprise.getEntreprisesLieesPrecedentsExploitants();
        for (IEntrepriseLieeCreation iEntrLieePrecExploitant : entrLieePrecedentiExploitantList) {
            iEntreprise.removeFromEntreprisesLieesPrecedentsExploitants(iEntrLieePrecExploitant);
        }
        /*
         * parcourir la liste des EntrepriseLieePrecedentiExploitant dans le
         * Bean pour les rajouter à l'interface
         */
        java.util.List<EntrepriseLieeBean> entrLieePrecExploitListBean = entrepriseBean.getEntreprisesLieesPrecedentsExploitants();
        for (EntrepriseLieeBean entrepriseLieePrecedentExploitantBean : entrLieePrecExploitListBean) {
            IEntrepriseLieeCreation iEntrepriseLieePrecedentExploitant = iEntreprise.addToEntreprisesLieesPrecedentsExploitants();
            // DEBUT- Mapper iEntrepriseLieePrecedentiExploitant
            // Gestion idTechnique, nécessaire pour la gestion des impacts
            // (écran)
            if (entrepriseLieePrecedentExploitantBean.getIdTechnique() == null || entrepriseLieePrecedentExploitantBean.getIdTechnique().isEmpty()) {
                entrepriseLieePrecedentExploitantBean.setIdTechnique(UUID.randomUUID().toString());
            }

            iEntrepriseLieePrecedentExploitant.newAdresse();
            iEntrepriseLieePrecedentExploitant.getAdresse().newCodePostalCommune();
            if (entrepriseLieePrecedentExploitantBean != null) {
                if (entrepriseLieePrecedentExploitantBean.getAdresse() == null) {
                    entrepriseLieePrecedentExploitantBean.setAdresse(new AdresseBean());
                }
                this.adresseMapper.map(entrepriseLieePrecedentExploitantBean.getAdresse(), iEntrepriseLieePrecedentExploitant.getAdresse());
                this.postalCommuneMapper.map(entrepriseLieePrecedentExploitantBean.getAdresse().getCodePostalCommune(), iEntrepriseLieePrecedentExploitant.getAdresse().getCodePostalCommune());

                this.entrepriseLieeMapper.map(entrepriseLieePrecedentExploitantBean, iEntrepriseLieePrecedentExploitant);
            }
            // FIN - Mapper iEntrepriseLieePrecedentiExploitant
        }

    }

    /**
     * Ajoute le entreprise liee precedenti exploitant.
     *
     * @param entrepriseBean
     *            le entreprise bean
     * @param iEntreprise
     *            le i entreprise
     */
    private void addEntrepriseLieeEntrepriseLieeFusionScission(EntrepriseBean entrepriseBean, IEntrepriseCreation iEntreprise) {

        /*
         * parcourir la liste des entrLieeEntrepriseLieeFusionScission existants
         * sur l'interface XML et les supprimer
         */
        IEntrepriseLieeCreation[] entrLieeEntrepriseLieeFusionScission = iEntreprise.getEntreprisesLieesFusionScission();
        for (IEntrepriseLieeCreation iEntrLieeFS : entrLieeEntrepriseLieeFusionScission) {
            iEntreprise.removeFromEntreprisesLieesFusionScission(iEntrLieeFS);
        }
        /*
         * parcourir la liste des entrLieeEntrepriseLieeFusionScission dans le
         * Bean pour les rajouter à l'interface
         */
        java.util.List<EntrepriseLieeBean> entrLieeFSListBean = entrepriseBean.getEntreprisesLieesFusionScission();
        for (EntrepriseLieeBean entrepriseLieeFSBean : entrLieeFSListBean) {
            IEntrepriseLieeCreation iEntrepriseLieeFS = iEntreprise.addToEntreprisesLieesFusionScission();
            // DEBUT- Mapper entrLieeEntrepriseLieeFusionScission
            // Gestion idTechnique, nécessaire pour la gestion des impacts
            // (écran)
            if (entrepriseLieeFSBean.getIdTechnique() == null || entrepriseLieeFSBean.getIdTechnique().isEmpty()) {
                entrepriseLieeFSBean.setIdTechnique(UUID.randomUUID().toString());
            }

            iEntrepriseLieeFS.newAdresse();
            iEntrepriseLieeFS.getAdresse().newCodePostalCommune();
            if (entrepriseLieeFSBean != null) {
                if (entrepriseLieeFSBean.getAdresse() == null) {
                    entrepriseLieeFSBean.setAdresse(new AdresseBean());
                }
                this.adresseMapper.map(entrepriseLieeFSBean.getAdresse(), iEntrepriseLieeFS.getAdresse());
                this.postalCommuneMapper.map(entrepriseLieeFSBean.getAdresse().getCodePostalCommune(), iEntrepriseLieeFS.getAdresse().getCodePostalCommune());

                this.entrepriseLieeMapper.map(entrepriseLieeFSBean, iEntrepriseLieeFS);
            }
            // FIN - Mapper entrLieeEntrepriseLieeFusionScission
        }

    }

    /**
     * Ajoute le personne liee.
     *
     * @param entrepriseBean
     *            le entreprise bean
     * @param iEntreprise
     *            le i entreprise
     */
    private void addPersonneLiee(EntrepriseBean entrepriseBean, IEntrepriseCreation iEntreprise) {
        /*
         * parcourir la liste des évenements existants sur l'interface XML et
         * les supprimer
         */
        IPersonneLieeCreation[] personnesLieesCreation = iEntreprise.getPersonneLiee();
        for (IPersonneLieeCreation iPersonneLieeCreation : personnesLieesCreation) {
            iEntreprise.removeFromPersonneLiee(iPersonneLieeCreation);
        }

        /*
         * parcourir la liste des évenements dans le Bean pour les rajouter à
         * l'interface
         */
        List<PersonneLieeBean> personnesLieesBean = entrepriseBean.getPersonneLiee();
        for (PersonneLieeBean personneLieeBean : personnesLieesBean) {
            IPersonneLieeCreation iPersonneLieeCreation = iEntreprise.addToPersonneLiee();
            iPersonneLieeCreation.newPpAdresse();
            iPersonneLieeCreation.getPpAdresse().newCodePostalCommune();
            if (personneLieeBean != null) {
                // Gestion idTechnique, nécessaire pour la gestion des impacts
                // (écran)
                if (personneLieeBean.getIdTechnique() == null || personneLieeBean.getIdTechnique().isEmpty()) {
                    personneLieeBean.setIdTechnique(UUID.randomUUID().toString());
                }
                if (personneLieeBean.getPpAdresse() == null) {
                    personneLieeBean.setPpAdresse(new AdresseBean());
                }
                this.adresseMapper.map(personneLieeBean.getPpAdresse(), iPersonneLieeCreation.getPpAdresse());
                this.postalCommuneMapper.map(personneLieeBean.getPpAdresse().getCodePostalCommune(), iPersonneLieeCreation.getPpAdresse().getCodePostalCommune());
                this.personneLieeMapper.map(personneLieeBean, iPersonneLieeCreation);
            }
        }
    }

    /**
     * Ajoute le etablissements ue.
     *
     * @param entrepriseBean
     *            le entreprise bean
     * @param iEntreprise
     *            le i entreprise
     */
    private void addEtablissementsUE(EntrepriseBean entrepriseBean, IEntrepriseCreation iEntreprise) {
        /*
         * parcourir la liste des etablissementsUE existants sur l'interface XML
         * et les supprimer
         */
        IAutreEtablissementUECreation[] etablissementsUE = iEntreprise.getEtablissementsUE();
        for (IAutreEtablissementUECreation iAutreEtablissementUE : etablissementsUE) {
            iEntreprise.removeFromEtablissementsUE(iAutreEtablissementUE);
        }
        /*
         * parcourir la liste des évenements dans le Bean pour les rajouter à
         * l'interface
         */
        List<AutreEtablissementUEBean> etablissementsUEBean = entrepriseBean.getEtablissementsUE();
        for (AutreEtablissementUEBean autreEtablissementUEBean : etablissementsUEBean) {
            // Gestion idTechnique, nécessaire pour la gestion des impacts
            // (écran)
            if (autreEtablissementUEBean.getIdTechnique() == null || autreEtablissementUEBean.getIdTechnique().isEmpty()) {
                autreEtablissementUEBean.setIdTechnique(UUID.randomUUID().toString());
            }

            IAutreEtablissementUECreation iAutreEtablissementUE = iEntreprise.addToEtablissementsUE();
            iAutreEtablissementUE.newAdresse();
            iAutreEtablissementUE.getAdresse().newCodePostalCommune();
            if (autreEtablissementUEBean.getAdresse() == null) {
                autreEtablissementUEBean.setAdresse(new AdresseBean());
            }
            this.adresseMapper.map(autreEtablissementUEBean.getAdresse(), iAutreEtablissementUE.getAdresse());
            this.postalCommuneMapper.map(autreEtablissementUEBean.getAdresse().getCodePostalCommune(), iAutreEtablissementUE.getAdresse().getCodePostalCommune());
            this.autreEtablissementUEMapper.map(autreEtablissementUEBean, iAutreEtablissementUE);
        }

    }

    /**
     * Accesseur sur l'attribut entreprise mapper.
     *
     * @return entreprise mapper
     */
    public Mapper getEntrepriseMapper() {
        return entrepriseMapper;
    }

    /**
     * Accesseur sur l'attribut formalite mapper.
     *
     * @return formalite mapper
     */
    public Mapper getFormaliteMapper() {
        return formaliteMapper;
    }

    /**
     * Mutateur sur l'attribut formalite mapper.
     *
     * @param formaliteMapper
     *            le nouveau formalite mapper
     */
    public void setFormaliteMapper(Mapper formaliteMapper) {
        this.formaliteMapper = formaliteMapper;
    }

    /**
     * Mutateur sur l'attribut entreprise mapper.
     *
     * @param entrepriseMapper
     *            le nouveau entreprise mapper
     */
    public void setEntrepriseMapper(Mapper entrepriseMapper) {
        this.entrepriseMapper = entrepriseMapper;
    }

    /**
     * Accesseur sur l'attribut dirigeant mapper.
     *
     * @return dirigeant mapper
     */
    public Mapper getDirigeantMapper() {
        return dirigeantMapper;
    }

    /**
     * Mutateur sur l'attribut dirigeant mapper.
     *
     * @param dirigeantMapper
     *            le nouveau dirigeant mapper
     */
    public void setDirigeantMapper(Mapper dirigeantMapper) {
        this.dirigeantMapper = dirigeantMapper;
    }

    /**
     * Accesseur sur l'attribut etablissement mapper.
     *
     * @return etablissement mapper
     */
    public Mapper getEtablissementMapper() {
        return etablissementMapper;
    }

    /**
     * Mutateur sur l'attribut etablissement mapper.
     *
     * @param etablissementMapper
     *            le nouveau etablissement mapper
     */
    public void setEtablissementMapper(Mapper etablissementMapper) {
        this.etablissementMapper = etablissementMapper;
    }

    /**
     * Accesseur sur l'attribut entreprise liee mapper.
     *
     * @return entreprise liee mapper
     */
    public Mapper getEntrepriseLieeMapper() {
        return entrepriseLieeMapper;
    }

    /**
     * Mutateur sur l'attribut entreprise liee mapper.
     *
     * @param entrepriseLieeMapper
     *            le nouveau entreprise liee mapper
     */
    public void setEntrepriseLieeMapper(Mapper entrepriseLieeMapper) {
        this.entrepriseLieeMapper = entrepriseLieeMapper;
    }

    /**
     * Accesseur sur l'attribut eirl mapper.
     *
     * @return eirl mapper
     */
    public Mapper getEirlMapper() {
        return eirlMapper;
    }

    /**
     * Mutateur sur l'attribut eirl mapper.
     *
     * @param eirlMapper
     *            le nouveau eirl mapper
     */
    public void setEirlMapper(Mapper eirlMapper) {
        this.eirlMapper = eirlMapper;
    }

    /**
     * Accesseur sur l'attribut declaration sociale mapper.
     *
     * @return declaration sociale mapper
     */
    public Mapper getDeclarationSocialeMapper() {
        return declarationSocialeMapper;
    }

    /**
     * Mutateur sur l'attribut declaration sociale mapper.
     *
     * @param declarationSocialeMapper
     *            le nouveau declaration sociale mapper
     */
    public void setDeclarationSocialeMapper(Mapper declarationSocialeMapper) {
        this.declarationSocialeMapper = declarationSocialeMapper;
    }

    /**
     * Accesseur sur l'attribut conjoint mapper.
     *
     * @return conjoint mapper
     */
    public Mapper getConjointMapper() {
        return conjointMapper;
    }

    /**
     * Mutateur sur l'attribut conjoint mapper.
     *
     * @param conjointMapper
     *            le nouveau conjoint mapper
     */
    public void setConjointMapper(Mapper conjointMapper) {
        this.conjointMapper = conjointMapper;
    }

    /**
     * Accesseur sur l'attribut ayant droit mapper.
     *
     * @return ayant droit mapper
     */
    public Mapper getAyantDroitMapper() {
        return ayantDroitMapper;
    }

    /**
     * Mutateur sur l'attribut ayant droit mapper.
     *
     * @param ayantDroitMapper
     *            le nouveau ayant droit mapper
     */
    public void setAyantDroitMapper(Mapper ayantDroitMapper) {
        this.ayantDroitMapper = ayantDroitMapper;
    }

    /**
     * Accesseur sur l'attribut personne liee mapper.
     *
     * @return personne liee mapper
     */
    public Mapper getPersonneLieeMapper() {
        return personneLieeMapper;
    }

    /**
     * Mutateur sur l'attribut personne liee mapper.
     *
     * @param personneLieeMapper
     *            le nouveau personne liee mapper
     */
    public void setPersonneLieeMapper(Mapper personneLieeMapper) {
        this.personneLieeMapper = personneLieeMapper;
    }

    /**
     * Accesseur sur l'attribut autre etablissement ue mapper.
     *
     * @return autre etablissement ue mapper
     */
    public Mapper getAutreEtablissementUEMapper() {
        return autreEtablissementUEMapper;
    }

    /**
     * Mutateur sur l'attribut autre etablissement ue mapper.
     *
     * @param autreEtablissementUEMapper
     *            le nouveau autre etablissement ue mapper
     */
    public void setAutreEtablissementUEMapper(Mapper autreEtablissementUEMapper) {
        this.autreEtablissementUEMapper = autreEtablissementUEMapper;
    }

    /**
     * Accesseur sur l'attribut regime fiscal mapper.
     *
     * @return regime fiscal mapper
     */
    public Mapper getRegimeFiscalMapper() {
        return regimeFiscalMapper;
    }

    /**
     * Mutateur sur l'attribut regime fiscal mapper.
     *
     * @param regimeFiscalMapper
     *            le nouveau regime fiscal mapper
     */
    public void setRegimeFiscalMapper(Mapper regimeFiscalMapper) {
        this.regimeFiscalMapper = regimeFiscalMapper;
    }

    /**
     * Accesseur sur l'attribut adresse mapper.
     *
     * @return adresse mapper
     */
    public Mapper getAdresseMapper() {
        return adresseMapper;
    }

    /**
     * Mutateur sur l'attribut adresse mapper.
     *
     * @param adresseMapper
     *            le nouveau adresse mapper
     */
    public void setAdresseMapper(Mapper adresseMapper) {
        this.adresseMapper = adresseMapper;
    }

    /**
     * Accesseur sur l'attribut postal commune mapper.
     *
     * @return postal commune mapper
     */
    public Mapper getPostalCommuneMapper() {
        return postalCommuneMapper;
    }

    /**
     * Mutateur sur l'attribut postal commune mapper.
     *
     * @param postalCommuneMapper
     *            le nouveau postal commune mapper
     */
    public void setPostalCommuneMapper(Mapper postalCommuneMapper) {
        this.postalCommuneMapper = postalCommuneMapper;
    }

    /**
     * Accesseur sur l'attribut {@link #activiteSoumiseQualificationMapper}.
     *
     * @return Mapper activiteSoumiseQualificationMapper
     */
    public Mapper getActiviteSoumiseQualificationMapper() {
        return activiteSoumiseQualificationMapper;
    }

    /**
     * Mutateur sur l'attribut {@link #activiteSoumiseQualificationMapper}.
     *
     * @param activiteSoumiseQualificationMapper
     *            la nouvelle valeur de l'attribut
     *            activiteSoumiseQualificationMapper
     */
    public void setActiviteSoumiseQualificationMapper(Mapper activiteSoumiseQualificationMapper) {
        this.activiteSoumiseQualificationMapper = activiteSoumiseQualificationMapper;
    }

}
