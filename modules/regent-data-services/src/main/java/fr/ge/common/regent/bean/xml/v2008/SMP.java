package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface SMP.
 */
@ResourceXPath("/SMP")
public interface SMP {

  /**
   * Get le p25.
   *
   * @return le p25
   */
  @FieldXPath("P25")
  IP25[] getP25();

  /**
   * Ajoute le to p25.
   *
   * @return le i p25
   */
  IP25 addToP25();

  /**
   * Set le p25.
   *
   * @param P25
   *          le nouveau p25
   */
  void setP25(IP25[] P25);

  /**
   * Get le p40.
   *
   * @return le p40
   */
  @FieldXPath("P40")
  String getP40();

  /**
   * Set le p40.
   *
   * @param P40
   *          le nouveau p40
   */
  void setP40(String P40);

  /**
   * Get le p41.
   *
   * @return le p41
   */
  @FieldXPath("P41")
  IP41[] getP41();

  /**
   * Ajoute le to p41.
   *
   * @return le i p41
   */
  IP41 addToP41();

  /**
   * Set le p41.
   *
   * @param P41
   *          le nouveau p41
   */
  void setP41(IP41[] P41);

  /**
   * Get le p42.
   *
   * @return le p42
   */
  @FieldXPath("P42")
  String getP42();

  /**
   * Set le p42.
   *
   * @param P42
   *          le nouveau p42
   */
  void setP42(String P42);

  /**
   * Get le P43.
   *
   * @return le P43
   */
  @FieldXPath("P43")
  IP43[] getP43();

  /**
   * Ajoute le to P43.
   *
   * @return le i P43
   */
  IP43 addToP43();

  /**
   * Set le P43.
   *
   * @param P43
   *          le nouveau P43
   */
  void setP43(IP43[] P43);

  /**
   * Get le p45.
   *
   * @return le p45
   */
  @FieldXPath("P45")
  String getP45();

  /**
   * Set le p45.
   *
   * @param P45
   *          le nouveau p45
   */
  void setP45(String P45);

}
