package fr.ge.common.regent.services.declenchement;

import java.util.List;

/**
 * Le Interface DeclenchementRubriqueXmlRegent.
 */
public interface IDeclenchementRubriqueXmlRegentService {

    /**
     * cette methode permet de faire le delta entre listRubriqueByEvenement et
     * listEvenementByDEstinataire.
     *
     * @param version
     *            le version
     * @param evenements
     *            le evenements
     * @param role
     *            le role
     * @param destinataire
     *            le destinataire
     * @return le list rubrique for xml regent
     */
    List<String> getListRubriqueForXmlRegent(String version, List<String> evenements, String role, String destinataire);

    /**
     * cette methode permet de récupérer la liste des rubrique par évènement et
     * par version
     *
     * @param version
     *            le version
     * @param evenements
     *            le evenements
     * @return the list rubrique related with a version of xml regent and list
     *         of events
     */
    List<String> getListRubriqueByEventAndVersion(List<String> evenements, String version);
}
