package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IP43.
 */
@ResourceXPath("/P43")
public interface IP43 {

  /**
   * Get le p433.
   *
   * @return le p433
   */
  @FieldXPath("P43.3")
  String getP433();

  /**
   * Set le p433.
   *
   * @param P433
   *          le nouveau p433
   */
  void setP433(String P433);

  /**
   * Get le P435.
   *
   * @return le P435
   */
  @FieldXPath("P43.5")
  String getP435();

  /**
   * Set le P435.
   *
   * @param P435
   *          le nouveau P435
   */
  void setP435(String P435);

  /**
   * Get le P436.
   *
   * @return le P436
   */
  @FieldXPath("P43.6")
  String getP436();

  /**
   * Set le P436.
   *
   * @param P436
   *          le nouveau P436
   */
  void setP436(String P436);

  /**
   * Get le P437.
   *
   * @return le P437
   */
  @FieldXPath("P43.7")
  String getP437();

  /**
   * Set le P437.
   *
   * @param P437
   *          le nouveau P437
   */
  void setP437(String P437);

  /**
   * Get le P438.
   *
   * @return le P438
   */
  @FieldXPath("P43.8")
  String getP438();

  /**
   * Set le P438.
   *
   * @param P438
   *          le nouveau P438
   */
  void setP438(String P438);

  /**
   * Get le P4310.
   *
   * @return le P4310
   */
  @FieldXPath("P43.10")
  String getP4310();

  /**
   * Set le P4310.
   *
   * @param P4310
   *          le nouveau P4310
   */
  void setP4310(String P4310);

  /**
   * Get le P4311.
   *
   * @return le P4311
   */
  @FieldXPath("P43.11")
  String getP4311();

  /**
   * Set le P4311.
   *
   * @param P4311
   *          le nouveau P4311
   */
  void setP4311(String P4311);

  /**
   * Get le P4312.
   *
   * @return le P4312
   */
  @FieldXPath("P43.12")
  String getP4312();

  /**
   * Set le P4312.
   *
   * @param P4312
   *          le nouveau P4312
   */
  void setP4312(String P4312);

  /**
   * Get le P4313.
   *
   * @return le P4313
   */
  @FieldXPath("P43.13")
  String getP4313();

  /**
   * Set le P4313.
   *
   * @param P4313
   *          le nouveau P4313
   */
  void setP4313(String P4313);

  /**
   * Get le P4314.
   *
   * @return le P4314
   */
  @FieldXPath("P43.14")
  String getP4314();

  /**
   * Set le P4314.
   *
   * @param P4314
   *          le nouveau P4314
   */
  void setP4314(String P4314);

}
