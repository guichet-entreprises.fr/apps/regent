package fr.ge.common.regent.bean.formalite;

import java.io.Serializable;

/**
 * 
 * Classe parent de tous les beans.
 * 
 */
public class FormaliteVueBean implements Serializable {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -2945658950163926797L;

    private String identifiantFormalite;

    /** Id technique. */
    private String idTechnique;

    /** Le id. */
    private Long id;

    /** Le version. */
    private String version;

    /** Le numero dossier. */
    private String numeroDossier;

    /** Le utilisateur id. */
    private String utilisateurId;

    /** Le utilisateur id. */
    private String typePersonne;

    /**
     * Accesseur sur l'attribut type personne.
     *
     * @return type personne
     */
    public String getTypePersonne() {
        return typePersonne;
    }

    /**
     * Mutateur sur l'attribut type personne.
     *
     * @param typePersonne
     *            le nouveau type personne
     */
    public void setTypePersonne(String typePersonne) {
        this.typePersonne = typePersonne;
    }

    /**
     * Get le idTechnique.
     *
     * @return le idTechnique
     */
    public String getIdTechnique() {
        return idTechnique;
    }

    /**
     * Set le idTechnique.
     *
     * @param idTechnique
     *            le nouveau idTechnique
     */
    public void setIdTechnique(String idTechnique) {
        this.idTechnique = idTechnique;
    }

    /**
     * Get le version.
     *
     * @return le version
     */
    public String getVersion() {
        return this.version;
    }

    /**
     * Set le version.
     *
     * @param version
     *            le nouveau version
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * getId.
     * 
     * @return id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * setId.
     * 
     * @param id
     *            id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Get le numero dossier.
     *
     * @return le numero dossier
     */
    public String getNumeroDossier() {
        return this.numeroDossier;
    }

    /**
     * Set le numero dossier.
     *
     * @param numeroDossier
     *            le nouveau numero dossier
     */
    public void setNumeroDossier(String numeroDossier) {
        this.numeroDossier = numeroDossier;
    }

    /**
     * Get le utilisateur id.
     *
     * @return le utilisateur id
     */
    public String getUtilisateurId() {
        return this.utilisateurId;
    }

    /**
     * Set le utilisateur id.
     *
     * @param utilisateurId
     *            le nouveau utilisateur id
     */
    public void setUtilisateurId(String utilisateurId) {
        this.utilisateurId = utilisateurId;
    }

    /**
     * Accesseur sur l'attribut {@link #identifiantFormalite}.
     *
     * @return String identifiantFormalite
     */
    public String getIdentifiantFormalite() {
        return identifiantFormalite;
    }

    /**
     * Mutateur sur l'attribut {@link #identifiantFormalite}.
     *
     * @param identifiantFormalite 
     *          la nouvelle valeur de l'attribut identifiantFormalite
     */
    public void setIdentifiantFormalite(String identifiantFormalite) {
        this.identifiantFormalite = identifiantFormalite;
    }

}
