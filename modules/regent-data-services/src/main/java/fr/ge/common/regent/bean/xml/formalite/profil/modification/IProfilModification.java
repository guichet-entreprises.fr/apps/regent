package fr.ge.common.regent.bean.xml.formalite.profil.modification;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;
import org.xmlfield.core.types.XmlString;

import fr.ge.common.regent.bean.xml.formalite.IFormalite;
import fr.ge.common.regent.bean.xml.formalite.profil.creation.ICfe;
import fr.ge.common.regent.bean.xml.transverse.IPostalCommune;

/**
 * La classe XML Field de Profil Entreprise Modification A1.
 * 
 * @author hhichri
 * 
 */
@ResourceXPath("/formalite")
public interface IProfilModification extends IFormalite {

    /** La constante MODEL_VERSION. */
    static final int MODEL_VERSION = 1;

    /**
     * Get le forme juridique.
     * 
     * @return le forme juridique
     */
    @FieldXPath(value = "formeJuridique")
    String getFormeJuridique();

    /**
     * Get le micro social oui non.
     * 
     * @return le micro social oui non
     */
    @FieldXPath(value = "microSocialOuiNon")
    String getMicroSocialOuiNon();

    /**
     * Get le modif oui non.
     * 
     * @return le modif oui non
     */
    @FieldXPath(value = "modifOuiNon")
    String getModifOuiNon();

    /**
     * Get le modif evenements2.
     * 
     * @return le modif evenements2
     */
    @FieldXPath(value = "modifsEvenements2/modifEvenements2")
    XmlString[] getModifEvenements2();

    /**
     * Get le activite principale domaine.
     * 
     * @return le activite principale domaine
     */
    @FieldXPath(value = "activitePrincipaleDomaine")
    String getActivitePrincipaleDomaine();

    /**
     * Get le activite principale secteur.
     * 
     * @return le activite principale secteur
     */
    @FieldXPath(value = "activitePrincipaleSecteur")
    String getActivitePrincipaleSecteur();

    /**
     * Get le activite principale code activite.
     * 
     * @return le activite principale code activite
     */
    @FieldXPath(value = "activitePrincipaleCodeActivite")
    String getActivitePrincipaleCodeActivite();

    /**
     * Get le activite principale code ape.
     * 
     * @return le activite principale code ape
     */
    @FieldXPath(value = "activitePrincipaleCodeAPE")
    String getActivitePrincipaleCodeAPE();

    /**
     * Get le activite principale secteur2.
     * 
     * @return le activite principale secteur2
     */
    @FieldXPath(value = "activitePrincipaleSecteur2")
    String getActivitePrincipaleSecteur2();

    /**
     * Get le activite principale libelle.
     * 
     * @return le activite principale libelle
     */
    @FieldXPath(value = "activitePrincipaleLibelle")
    String getActivitePrincipaleLibelle();

    /**
     * Get le existe activite secondaire.
     * 
     * @return le existe activite secondaire
     */
    @FieldXPath(value = "existeActiviteSecondaire")
    String getExisteActiviteSecondaire();

    /**
     * Get le activite secondaire domaine.
     * 
     * @return le activite secondaire domaine
     */
    @FieldXPath(value = "activiteSecondaireDomaine")
    String getActiviteSecondaireDomaine();

    /**
     * Get le activite secondaire secteur.
     * 
     * @return le activite secondaire secteur
     */
    @FieldXPath(value = "activiteSecondaireSecteur")
    String getActiviteSecondaireSecteur();

    /**
     * Get le activite secondaire code activite.
     * 
     * @return le activite secondaire code activite
     */
    @FieldXPath(value = "activiteSecondaireCodeActivite")
    String getActiviteSecondaireCodeActivite();

    /**
     * Get le postal commune.
     * 
     * @return le postal commune
     */
    @FieldXPath("postalCommune")
    IPostalCommune getPostalCommune();

    /**
     * New postal commune.
     * 
     * @return le i postal commune
     */
    IPostalCommune newPostalCommune();

    /**
     * Get le postal commune1.
     * 
     * @return le postal commune1
     */
    @FieldXPath("postalCommune1")
    IPostalCommune getPostalCommune1();

    /**
     * New postal commune1.
     * 
     * @return le i postal commune
     */
    IPostalCommune newPostalCommune1();

    /**
     * Get le postal commune2.
     * 
     * @return le postal commune2
     */
    @FieldXPath("postalCommune2")
    IPostalCommune getPostalCommune2();

    /**
     * New postal commune2.
     * 
     * @return le i postal commune
     */
    IPostalCommune newPostalCommune2();

    /**
     * Get le nom dossier.
     * 
     * @return le nom dossier
     */
    @FieldXPath(value = "nomDossier")
    String getNomDossier();

    /**
     * Get le option cmacc i1.
     * 
     * @return le option cmacc i1
     */
    @FieldXPath(value = "optionCMACCI1")
    String getOptionCMACCI1();

    /**
     * Get le option cmacc i2.
     * 
     * @return le option cmacc i2
     */
    @FieldXPath(value = "optionCMACCI2")
    String getOptionCMACCI2();

    /**
     * Get le option cmacci.
     * 
     * @return le option cmacci
     */
    @FieldXPath(value = "optionCMACCI")
    String getOptionCMACCI();

    /**
     * Get le est inscrit registre public.
     * 
     * @return le est inscrit registre public
     */
    @FieldXPath(value = "estInscritRegistrePublic")
    String getEstInscritRegistrePublic();

    /**
     * Get le cfe.
     * 
     * @return le cfe
     */
    @FieldXPath(value = "cfe")
    ICfe getCfe();

    /**
     * New cfe.
     * 
     * @return le i cfe
     */
    ICfe newCfe();

    /**
     * Get le modif evenements.
     * 
     * @return le modif evenements
     */
    @FieldXPath(value = "modifsEvenements/modifEvenements")
    XmlString[] getModifEvenements();

    /**
     * Get le type cfe.
     * 
     * @return le type cfe
     */
    @FieldXPath(value = "typeCfe")
    String getTypeCfe();

    /**
     * {@inheritDoc}
     */
    @Override
    /*
     * (non-Javadoc)
     * 
     * @see fr.guichetentreprises.objetsMetier.IFormalite#getReseauCFE()
     */
    @FieldXPath(value = "reseauCFE")
    String getReseauCFE();

    /**
     * Get le secteur cfe.
     * 
     * @return le secteur cfe
     */
    @FieldXPath(value = "secteurCfe")
    String getSecteurCfe();

    /**
     * Get le evenement.
     * 
     * @return le evenement
     */
    @FieldXPath(value = "evenements/evenement")
    XmlString[] getEvenement();

    /**
     * Get le aqpa.
     * 
     * @return le aqpa
     */
    @Override
    @FieldXPath(value = "aqpa")
    boolean getAqpa();

    /**
     * {@inheritDoc}
     */
    @Override
    void setNumeroFormalite(String numeroFormalite);

    /**
     * Set le forme juridique.
     * 
     * @param formeJuridique
     *            le nouveau forme juridique
     */
    void setFormeJuridique(String formeJuridique);

    /**
     * Set le micro social oui non.
     * 
     * @param microSocialOuiNon
     *            le nouveau micro social oui non
     */
    void setMicroSocialOuiNon(String microSocialOuiNon);

    /**
     * Set le modif oui non.
     * 
     * @param modifOuiNon
     *            le nouveau modif oui non
     */
    void setModifOuiNon(String modifOuiNon);

    /**
     * Set le cfe.
     * 
     * @param cfe
     *            le nouveau cfe
     */
    void setCfe(ICfe cfe);

    /**
     * Set le modif evenements2.
     * 
     * @param modifEvenements2
     *            le nouveau modif evenements2
     */
    void setModifEvenements2(XmlString[] modifEvenements2);

    /**
     * Set le activite principale domaine.
     * 
     * @param activitePrincipaleDomaine
     *            le nouveau activite principale domaine
     */
    void setActivitePrincipaleDomaine(String activitePrincipaleDomaine);

    /**
     * Set le activite principale secteur.
     * 
     * @param activitePrincipaleSecteur
     *            le nouveau activite principale secteur
     */
    void setActivitePrincipaleSecteur(String activitePrincipaleSecteur);

    /**
     * Set le activite principale code activite.
     * 
     * @param activitePrincipaleCodeActivite
     *            le nouveau activite principale code activite
     */
    void setActivitePrincipaleCodeActivite(String activitePrincipaleCodeActivite);

    /**
     * Set le activite principale code ape.
     * 
     * @param activitePrincipaleCodeAPE
     *            le nouveau activite principale code ape
     */
    void setActivitePrincipaleCodeAPE(String activitePrincipaleCodeAPE);

    /**
     * Set le activite principale secteur2.
     * 
     * @param activitePrincipaleSecteur2
     *            le nouveau activite principale secteur2
     */
    void setActivitePrincipaleSecteur2(String activitePrincipaleSecteur2);

    /**
     * Set le activite principale libelle.
     * 
     * @param activitePrincipaleLibelle
     *            le nouveau activite principale libelle
     */
    void setActivitePrincipaleLibelle(String activitePrincipaleLibelle);

    /**
     * Set le existe activite secondaire.
     * 
     * @param activitePrincipaleSecteur2
     *            le nouveau existe activite secondaire
     */
    void setExisteActiviteSecondaire(String activitePrincipaleSecteur2);

    /**
     * Set le activite secondaire domaine.
     */
    void setActiviteSecondaireDomaine();

    /**
     * Set le activite secondaire secteur.
     * 
     * @param activiteSecondaireDomaine
     *            le nouveau activite secondaire secteur
     */
    void setActiviteSecondaireSecteur(String activiteSecondaireDomaine);

    /**
     * Set le activite secondaire code activite.
     * 
     * @param activiteSecondaireCodeActivite
     *            le nouveau activite secondaire code activite
     */
    void setActiviteSecondaireCodeActivite(String activiteSecondaireCodeActivite);

    /**
     * Set le nom dossier.
     * 
     * @param nomDossier
     *            le nouveau nom dossier
     */
    void setNomDossier(String nomDossier);

    /**
     * Set le option cmacc i1.
     * 
     * @param optionCMACCI1
     *            le nouveau option cmacc i1
     */
    void setOptionCMACCI1(String optionCMACCI1);

    /**
     * Set le option cmacc i2.
     * 
     * @param optionCMACCI2
     *            le nouveau option cmacc i2
     */
    void setOptionCMACCI2(String optionCMACCI2);

    /**
     * Set le postal commune.
     * 
     * @param postalCommune
     *            le nouveau postal commune
     */
    void setPostalCommune(IPostalCommune postalCommune);

    /**
     * Set le postal commune1.
     * 
     * @param postalCommune1
     *            le nouveau postal commune1
     */
    void setPostalCommune1(IPostalCommune postalCommune1);

    /**
     * Set le postal commune2.
     * 
     * @param postalCommune2
     *            le nouveau postal commune2
     */
    void setPostalCommune2(IPostalCommune postalCommune2);

    /**
     * Set le est inscrit registre public.
     * 
     * @param estInscritRegistrePublic
     *            le nouveau est inscrit registre public
     */
    void setEstInscritRegistrePublic(String estInscritRegistrePublic);

    /**
     * Retire le from modif evenements2.
     * 
     * @param evenement
     *            le evenement
     */
    void removeFromModifEvenements2(XmlString evenement);

    /**
     * Ajoute le to modif evenements2.
     * 
     * @return le xml string
     */
    XmlString addToModifEvenements2();

    /**
     * Set le modif evenements.
     * 
     * @param modifEvenements
     *            le nouveau modif evenements
     */
    void setModifEvenements(XmlString[] modifEvenements);

    /**
     * Méthode pour rajouter un évenement.
     * 
     * @return {@link XmlString}
     */
    XmlString addToModifEvenements();

    /**
     * Méthode qui supprime un modif evenement.
     * 
     * @param evenement
     *            le evenement
     */
    void removeFromModifEvenements(XmlString evenement);

    /**
     * Set le type cfe.
     * 
     * @param typeCfe
     *            le nouveau type cfe
     */
    void setTypeCfe(String typeCfe);

    /**
     * Set le option cmacci.
     * 
     * @param optionCMACCI
     *            le nouveau option cmacci
     */
    void setOptionCMACCI(String optionCMACCI);

    /**
     * {@inheritDoc}
     */
    @Override
    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.guichetentreprises.objetsMetier.IFormalite#setReseauCFE(java.lang.
     * String)
     */
    void setReseauCFE(String reseauCFE);

    /**
     * Set le secteur cfe.
     * 
     * @param secteurCFE
     *            le nouveau secteur cfe
     */
    void setSecteurCfe(String secteurCFE);

    /**
     * Set le evenement.
     * 
     * @param evenement
     *            le nouveau evenement
     */
    void setEvenement(XmlString[] evenement);

    /**
     * Méthode pour rajouter un évenement.
     * 
     * @return {@link XmlString}
     */
    XmlString addToEvenement();

    /**
     * Méthode qui supprime un évenemnt.
     * 
     * @param evenement
     *            {@link XmlString}
     */
    void removeFromEvenement(XmlString evenement);

    /**
     * Set le aqpa.
     * 
     * @param aqpa
     *            le nouveau aqpa
     */
    @Override
    void setAqpa(boolean aqpa);

    /**
     * Accesseur sur l'attribut {@link #descr libre activite principale}.
     *
     * @return descr libre activite principale
     */
    @FieldXPath(value = "descrLibreActivitePrincipale")
    String getDescrLibreActivitePrincipale();

    /**
     * Mutateur sur l'attribut {@link #descr libre activite principale}.
     *
     * @param descrLibreActivitePrincipale
     *            la nouvelle valeur de l'attribut descr libre activite
     *            principale
     */
    void setDescrLibreActivitePrincipale(String descrLibreActivitePrincipale);

    /**
     * Accesseur sur l'attribut {@link #descr libre activite secondaire}.
     *
     * @return descr libre activite secondaire
     */
    @FieldXPath(value = "descrLibreActiviteSecondaire")
    String getDescrLibreActiviteSecondaire();

    /**
     * Mutateur sur l'attribut {@link #descr libre activite secondaire}.
     *
     * @param descrLibreActiviteSecondaire
     *            la nouvelle valeur de l'attribut descr libre activite
     *            secondaire
     */
    void setDescrLibreActiviteSecondaire(String descrLibreActiviteSecondaire);

}
