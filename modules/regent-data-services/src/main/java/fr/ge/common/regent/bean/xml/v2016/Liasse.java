package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface Liasse.
 */
@ResourceXPath("/Liasse")
public interface Liasse {

  /**
   * Get le entreprise.
   *
   * @return le entreprise
   */
  @FieldXPath("Entreprise")
  Entreprise[] getEntreprise();

  /**
   * Ajoute le to entreprise.
   *
   * @return le entreprise
   */
  Entreprise addToEntreprise();

  /**
   * Set le entreprise.
   *
   * @param Entreprise
   *          le nouveau entreprise
   */
  void setEntreprise(Entreprise[] Entreprise);

  /**
   * Get le service.
   *
   * @return le service
   */
  @FieldXPath("Service")
  Service[] getService();

  /**
   * Ajoute le to service.
   *
   * @return le service
   */
  Service addToService();

  /**
   * Set le service.
   *
   * @param Service
   *          le nouveau service
   */
  void setService(Service[] Service);

  /**
   * Get le personne physique.
   *
   * @return le personne physique
   */
  @FieldXPath("PersonnePhysique")
  PersonnePhysique[] getPersonnePhysique();

  /**
   * Ajoute le to personne physique.
   *
   * @return le personne physique
   */
  PersonnePhysique addToPersonnePhysique();

  /**
   * Set le personne physique.
   *
   * @param PersonnePhysique
   *          le nouveau personne physique
   */
  void setPersonnePhysique(PersonnePhysique[] PersonnePhysique);

  /**
   * Gets the personne morale.
   *
   * @return the personne morale
   */
  @FieldXPath("PersonneMorale")
  PersonneMorale[] getPersonneMorale();

  /**
   * Add to personne morale.
   *
   * @return the personne morale
   */
  PersonneMorale addToPersonneMorale();

  /**
   * Sets the personne morale.
   *
   * @param personneMorale
   *          the new personne morale
   */
  void setPersonneMorale(PersonneMorale[] personneMorale);

  /**
   * Gets the ExploitationEnCommun.
   *
   * @return the ExploitationEnCommun
   */
  @FieldXPath("ExploitationEnCommun")
  ExploitationEnCommun[] getExploitationEnCommun();

  /**
   * Add to ExploitationEnCommun.
   *
   * @return the ExploitationEnCommun
   */
  ExploitationEnCommun addToExploitationEnCommun();

  /**
   * Sets ExploitationEnCommun.
   *
   * @param exploitationEnCommun
   *          the new ExploitationEnCommun
   */
  void setExploitationEnCommun(ExploitationEnCommun[] exploitationEnCommun);

}
