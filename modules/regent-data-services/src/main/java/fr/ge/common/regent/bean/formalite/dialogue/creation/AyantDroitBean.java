package fr.ge.common.regent.bean.formalite.dialogue.creation;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.format.annotation.NumberFormat;

import fr.ge.common.regent.bean.formalite.FormaliteVueBean;

/**
 * Le Class AyantDroitBean.
 */
public class AyantDroitBean extends FormaliteVueBean implements IFormaliteVue {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 2117847565579678771L;

    /** Le id technique. */
    private String idTechnique;

    /** Le activite autre que declaree departement. */
    private String activiteAutreQueDeclareeDepartement;

    /** Le activite autre que declaree pays. */
    private String activiteAutreQueDeclareePays;

    /** Le activite autre que declaree presence. */
    private String activiteAutreQueDeclareePresence;

    /** Le activite autre que declaree statut. */
    private String activiteAutreQueDeclareeStatut;

    /** Le activite autre que declaree statut autre. */
    private String activiteAutreQueDeclareeStatutAutre;

    /** Le adresse. */
    private AdresseBean adresse = new AdresseBean();

    /** Le affiliation msa. */
    private String affiliationMSA;

    /** Le ayant droit nombre. */
    @NumberFormat
    private Integer ayantDroitNombre;

    /** Le ayant droit presence. */
    private String ayantDroitPresence;

    /** Le ayant droits. */
    private List<AyantDroitBean> ayantDroits = new ArrayList<AyantDroitBean>();

    /** Le conjoint couvert assurance maladie. */
    private String conjointCouvertAssuranceMaladie;

    /** Le enfant scolarise. */
    private String enfantScolarise;

    /** Le identite complement civilite. */
    private String identiteComplementCivilite;

    /** Le identite complement date naissance. */
    private String identiteComplementDateNaissance;

    /** Le identite complement lieu naissance commune. */
    private String identiteComplementLieuNaissanceCommune;

    /** Le identite complement lieu naissance departement. */
    private String identiteComplementLieuNaissanceDepartement;

    /** Le identite complement lieu naissance pays. */
    private String identiteComplementLieuNaissancePays;

    /** Le identite complement lieu naissance ville. */
    private String identiteComplementLieuNaissanceVille;

    /** Le identite nationalite. */
    private String identiteNationalite;

    /** Le identite nom naissance. */
    private String identiteNomNaissance;

    /** Le identite prenom1. */
    private String identitePrenom1;

    /** Le identite prenom2. */
    private String identitePrenom2;

    /** Le identite prenom3. */
    private String identitePrenom3;

    /** Le identite prenom4. */
    private String identitePrenom4;

    /** Le lien parente. */
    private String lienParente;

    /** Le niveau. */
    private String niveau;

    /** Le numero securite sociale. */
    private String numeroSecuriteSociale;

    /** Le numero securite sociale connu. */
    private String numeroSecuriteSocialeConnu;

    /** Le organisme conventionne code. */
    private String organismeConventionneCode;

    /** Le organisme servant pension. */
    private String organismeServantPension;

    /** Le regime assurance maladie. */
    private String regimeAssuranceMaladie;

    /** Le regime assurance maladie autre. */
    private String regimeAssuranceMaladieAutre;

    /** Le titre sejour date expiration. */
    private String titreSejourDateExpiration;

    /** Le titre sejour lieu delivrance commune. */
    private String titreSejourLieuDelivranceCommune;

    /** Le titre sejour numero. */
    private String titreSejourNumero;

    /**
     * Get le id technique.
     * 
     * @return the idTechnique
     */
    public String getIdTechnique() {
        return this.idTechnique;
    }

    /**
     * Set le id technique.
     * 
     * @param idTechnique
     *            the idTechnique to set
     */
    public void setIdTechnique(String idTechnique) {
        this.idTechnique = idTechnique;
    }

    /**
     * Getter de l'attribut activiteAutreQueDeclareeDepartement.
     * 
     * @return la valeur de activiteAutreQueDeclareeDepartement
     */
    public String getActiviteAutreQueDeclareeDepartement() {
        return this.activiteAutreQueDeclareeDepartement;
    }

    /**
     * Getter de l'attribut activiteAutreQueDeclareePays.
     * 
     * @return la valeur de activiteAutreQueDeclareePays
     */
    public String getActiviteAutreQueDeclareePays() {
        return this.activiteAutreQueDeclareePays;
    }

    /**
     * Getter de l'attribut activiteAutreQueDeclareePresence.
     * 
     * @return la valeur de activiteAutreQueDeclareePresence
     */
    public String getActiviteAutreQueDeclareePresence() {
        return this.activiteAutreQueDeclareePresence;
    }

    /**
     * Getter de l'attribut activiteAutreQueDeclareeStatut.
     * 
     * @return la valeur de activiteAutreQueDeclareeStatut
     */
    public String getActiviteAutreQueDeclareeStatut() {
        return this.activiteAutreQueDeclareeStatut;
    }

    /**
     * Getter de l'attribut activiteAutreQueDeclareeStatutAutre.
     * 
     * @return la valeur de activiteAutreQueDeclareeStatutAutre
     */
    public String getActiviteAutreQueDeclareeStatutAutre() {
        return this.activiteAutreQueDeclareeStatutAutre;
    }

    /**
     * Getter de l'attribut adresse.
     * 
     * @return la valeur de adresse
     */
    public AdresseBean getAdresse() {
        return this.adresse;
    }

    /**
     * Getter de l'attribut affiliationMSA.
     * 
     * @return la valeur de affiliationMSA
     */
    public String getAffiliationMSA() {
        return this.affiliationMSA;
    }

    /**
     * Getter de l'attribut ayantDroitNombre.
     * 
     * @return la valeur de ayantDroitNombre
     */
    public Integer getAyantDroitNombre() {
        return this.ayantDroitNombre;
    }

    /**
     * Getter de l'attribut ayantDroitPresence.
     * 
     * @return la valeur de ayantDroitPresence
     */
    public String getAyantDroitPresence() {
        return this.ayantDroitPresence;
    }

    /**
     * Getter de l'attribut ayantDroits.
     * 
     * @return la valeur de ayantDroits
     */
    public List<AyantDroitBean> getAyantDroits() {
        return this.ayantDroits;
    }

    /**
     * Mutateur sur l'attribut ayant droits.
     *
     * @param ayantDroits
     *            le nouveau ayant droits
     */
    public void setAyantDroits(List<AyantDroitBean> ayantDroits) {
        this.ayantDroits = ayantDroits;
    }

    /**
     * Getter de l'attribut conjointCouvertAssuranceMaladie.
     * 
     * @return la valeur de conjointCouvertAssuranceMaladie
     */
    public String getConjointCouvertAssuranceMaladie() {
        return this.conjointCouvertAssuranceMaladie;
    }

    /**
     * Getter de l'attribut enfantScolarise.
     * 
     * @return la valeur de enfantScolarise
     */
    public String getEnfantScolarise() {
        return this.enfantScolarise;
    }

    /**
     * Getter de l'attribut identiteComplementCivilite.
     * 
     * @return la valeur de identiteComplementCivilite
     */
    public String getIdentiteComplementCivilite() {
        return this.identiteComplementCivilite;
    }

    /**
     * Getter de l'attribut identiteComplementDateNaissance.
     * 
     * @return la valeur de identiteComplementDateNaissance
     */
    public String getIdentiteComplementDateNaissance() {
        return this.identiteComplementDateNaissance;
    }

    /**
     * Getter de l'attribut identiteComplementLieuNaissanceCommune.
     * 
     * @return la valeur de identiteComplementLieuNaissanceCommune
     */
    public String getIdentiteComplementLieuNaissanceCommune() {
        return this.identiteComplementLieuNaissanceCommune;
    }

    /**
     * Getter de l'attribut identiteComplementLieuNaissanceDepartement.
     * 
     * @return la valeur de identiteComplementLieuNaissanceDepartement
     */
    public String getIdentiteComplementLieuNaissanceDepartement() {
        return this.identiteComplementLieuNaissanceDepartement;
    }

    /**
     * Getter de l'attribut identiteComplementLieuNaissancePays.
     * 
     * @return la valeur de identiteComplementLieuNaissancePays
     */
    public String getIdentiteComplementLieuNaissancePays() {
        return this.identiteComplementLieuNaissancePays;
    }

    /**
     * Getter de l'attribut identiteComplementLieuNaissanceVille.
     * 
     * @return la valeur de identiteComplementLieuNaissanceVille
     */
    public String getIdentiteComplementLieuNaissanceVille() {
        return this.identiteComplementLieuNaissanceVille;
    }

    /**
     * Getter de l'attribut identiteNationalite.
     * 
     * @return la valeur de identiteNationalite
     */
    public String getIdentiteNationalite() {
        return this.identiteNationalite;
    }

    /**
     * Getter de l'attribut identiteNomNaissance.
     * 
     * @return la valeur de identiteNomNaissance
     */
    public String getIdentiteNomNaissance() {
        return this.identiteNomNaissance;
    }

    /**
     * Getter de l'attribut identitePrenom1.
     * 
     * @return la valeur de identitePrenom1
     */
    public String getIdentitePrenom1() {
        return this.identitePrenom1;
    }

    /**
     * Getter de l'attribut identitePrenom2.
     * 
     * @return la valeur de identitePrenom2
     */
    public String getIdentitePrenom2() {
        return this.identitePrenom2;
    }

    /**
     * Getter de l'attribut identitePrenom3.
     * 
     * @return la valeur de identitePrenom3
     */
    public String getIdentitePrenom3() {
        return this.identitePrenom3;
    }

    /**
     * Getter de l'attribut identitePrenom4.
     * 
     * @return la valeur de identitePrenom4
     */
    public String getIdentitePrenom4() {
        return this.identitePrenom4;
    }

    /**
     * Getter de l'attribut lienParente.
     * 
     * @return la valeur de lienParente
     */
    public String getLienParente() {
        return this.lienParente;
    }

    /**
     * Getter de l'attribut niveau.
     * 
     * @return la valeur de niveau
     */
    public String getNiveau() {
        return this.niveau;
    }

    /**
     * Getter de l'attribut numeroSecuriteSociale.
     * 
     * @return la valeur de numeroSecuriteSociale
     */
    public String getNumeroSecuriteSociale() {
        return this.numeroSecuriteSociale;
    }

    /**
     * Getter de l'attribut numeroSecuriteSocialeConnu.
     * 
     * @return la valeur de numeroSecuriteSocialeConnu
     */
    public String getNumeroSecuriteSocialeConnu() {
        return this.numeroSecuriteSocialeConnu;
    }

    /**
     * Getter de l'attribut organismeConventionneCode.
     * 
     * @return la valeur de organismeConventionneCode
     */
    public String getOrganismeConventionneCode() {
        return this.organismeConventionneCode;
    }

    /**
     * Getter de l'attribut organismeServantPension.
     * 
     * @return la valeur de organismeServantPension
     */
    public String getOrganismeServantPension() {
        return this.organismeServantPension;
    }

    /**
     * Getter de l'attribut regimeAssuranceMaladie.
     * 
     * @return la valeur de regimeAssuranceMaladie
     */
    public String getRegimeAssuranceMaladie() {
        return this.regimeAssuranceMaladie;
    }

    /**
     * Getter de l'attribut regimeAssuranceMaladieAutre.
     * 
     * @return la valeur de regimeAssuranceMaladieAutre
     */
    public String getRegimeAssuranceMaladieAutre() {
        return this.regimeAssuranceMaladieAutre;
    }

    /**
     * Getter de l'attribut titreSejourDateExpiration.
     * 
     * @return la valeur de titreSejourDateExpiration
     */
    public String getTitreSejourDateExpiration() {
        return this.titreSejourDateExpiration;
    }

    /**
     * Getter de l'attribut titreSejourLieuDelivranceCommune.
     * 
     * @return la valeur de titreSejourLieuDelivranceCommune
     */
    public String getTitreSejourLieuDelivranceCommune() {
        return this.titreSejourLieuDelivranceCommune;
    }

    /**
     * Getter de l'attribut titreSejourNumero.
     * 
     * @return la valeur de titreSejourNumero
     */
    public String getTitreSejourNumero() {
        return this.titreSejourNumero;
    }

    /**
     * Setter de l'attribut activiteAutreQueDeclareeDepartement.
     * 
     * @param activiteAutreQueDeclareeDepartement
     *            la nouvelle valeur de activiteAutreQueDeclareeDepartement
     */
    public void setActiviteAutreQueDeclareeDepartement(String activiteAutreQueDeclareeDepartement) {
        this.activiteAutreQueDeclareeDepartement = activiteAutreQueDeclareeDepartement;
    }

    /**
     * Setter de l'attribut activiteAutreQueDeclareePays.
     * 
     * @param activiteAutreQueDeclareePays
     *            la nouvelle valeur de activiteAutreQueDeclareePays
     */
    public void setActiviteAutreQueDeclareePays(String activiteAutreQueDeclareePays) {
        this.activiteAutreQueDeclareePays = activiteAutreQueDeclareePays;
    }

    /**
     * Setter de l'attribut activiteAutreQueDeclareePresence.
     * 
     * @param activiteAutreQueDeclareePresence
     *            la nouvelle valeur de activiteAutreQueDeclareePresence
     */
    public void setActiviteAutreQueDeclareePresence(String activiteAutreQueDeclareePresence) {
        this.activiteAutreQueDeclareePresence = activiteAutreQueDeclareePresence;
    }

    /**
     * Setter de l'attribut activiteAutreQueDeclareeStatut.
     * 
     * @param activiteAutreQueDeclareeStatut
     *            la nouvelle valeur de activiteAutreQueDeclareeStatut
     */
    public void setActiviteAutreQueDeclareeStatut(String activiteAutreQueDeclareeStatut) {
        this.activiteAutreQueDeclareeStatut = activiteAutreQueDeclareeStatut;
    }

    /**
     * Setter de l'attribut activiteAutreQueDeclareeStatutAutre.
     * 
     * @param activiteAutreQueDeclareeStatutAutre
     *            la nouvelle valeur de activiteAutreQueDeclareeStatutAutre
     */
    public void setActiviteAutreQueDeclareeStatutAutre(String activiteAutreQueDeclareeStatutAutre) {
        this.activiteAutreQueDeclareeStatutAutre = activiteAutreQueDeclareeStatutAutre;
    }

    /**
     * Setter de l'attribut adresse.
     * 
     * @param adresse
     *            la nouvelle valeur de adresse
     */
    public void setAdresse(AdresseBean adresse) {
        this.adresse = adresse;
    }

    /**
     * Setter de l'attribut affiliationMSA.
     * 
     * @param affiliationMSA
     *            la nouvelle valeur de affiliationMSA
     */
    public void setAffiliationMSA(String affiliationMSA) {
        this.affiliationMSA = affiliationMSA;
    }

    /**
     * Setter de l'attribut ayantDroitNombre.
     * 
     * @param ayantDroitNombre
     *            la nouvelle valeur de ayantDroitNombre
     */
    public void setAyantDroitNombre(Integer ayantDroitNombre) {
        this.ayantDroitNombre = ayantDroitNombre;
    }

    /**
     * Setter de l'attribut ayantDroitPresence.
     * 
     * @param ayantDroitPresence
     *            la nouvelle valeur de ayantDroitPresence
     */
    public void setAyantDroitPresence(String ayantDroitPresence) {
        this.ayantDroitPresence = ayantDroitPresence;
    }

    /**
     * Setter de l'attribut conjointCouvertAssuranceMaladie.
     * 
     * @param conjointCouvertAssuranceMaladie
     *            la nouvelle valeur de conjointCouvertAssuranceMaladie
     */
    public void setConjointCouvertAssuranceMaladie(String conjointCouvertAssuranceMaladie) {
        this.conjointCouvertAssuranceMaladie = conjointCouvertAssuranceMaladie;
    }

    /**
     * Setter de l'attribut enfantScolarise.
     * 
     * @param enfantScolarise
     *            la nouvelle valeur de enfantScolarise
     */
    public void setEnfantScolarise(String enfantScolarise) {
        this.enfantScolarise = enfantScolarise;
    }

    /**
     * Setter de l'attribut identiteComplementCivilite.
     * 
     * @param identiteComplementCivilite
     *            la nouvelle valeur de identiteComplementCivilite
     */
    public void setIdentiteComplementCivilite(String identiteComplementCivilite) {
        this.identiteComplementCivilite = identiteComplementCivilite;
    }

    /**
     * Setter de l'attribut identiteComplementDateNaissance.
     * 
     * @param identiteComplementDateNaissance
     *            la nouvelle valeur de identiteComplementDateNaissance
     */
    public void setIdentiteComplementDateNaissance(String identiteComplementDateNaissance) {
        this.identiteComplementDateNaissance = identiteComplementDateNaissance;
    }

    /**
     * Setter de l'attribut identiteComplementLieuNaissanceCommune.
     * 
     * @param identiteComplementLieuNaissanceCommune
     *            la nouvelle valeur de identiteComplementLieuNaissanceCommune
     */
    public void setIdentiteComplementLieuNaissanceCommune(String identiteComplementLieuNaissanceCommune) {
        this.identiteComplementLieuNaissanceCommune = identiteComplementLieuNaissanceCommune;
    }

    /**
     * Setter de l'attribut identiteComplementLieuNaissanceDepartement.
     * 
     * @param identiteComplementLieuNaissanceDepartement
     *            la nouvelle valeur de
     *            identiteComplementLieuNaissanceDepartement
     */
    public void setIdentiteComplementLieuNaissanceDepartement(String identiteComplementLieuNaissanceDepartement) {
        this.identiteComplementLieuNaissanceDepartement = identiteComplementLieuNaissanceDepartement;
    }

    /**
     * Setter de l'attribut identiteComplementLieuNaissancePays.
     * 
     * @param identiteComplementLieuNaissancePays
     *            la nouvelle valeur de identiteComplementLieuNaissancePays
     */
    public void setIdentiteComplementLieuNaissancePays(String identiteComplementLieuNaissancePays) {
        this.identiteComplementLieuNaissancePays = identiteComplementLieuNaissancePays;
    }

    /**
     * Setter de l'attribut identiteComplementLieuNaissanceVille.
     * 
     * @param identiteComplementLieuNaissanceVille
     *            la nouvelle valeur de identiteComplementLieuNaissanceVille
     */
    public void setIdentiteComplementLieuNaissanceVille(String identiteComplementLieuNaissanceVille) {
        this.identiteComplementLieuNaissanceVille = identiteComplementLieuNaissanceVille;
    }

    /**
     * Setter de l'attribut identiteNationalite.
     * 
     * @param identiteNationalite
     *            la nouvelle valeur de identiteNationalite
     */
    public void setIdentiteNationalite(String identiteNationalite) {
        this.identiteNationalite = identiteNationalite;
    }

    /**
     * Setter de l'attribut identiteNomNaissance.
     * 
     * @param identiteNomNaissance
     *            la nouvelle valeur de identiteNomNaissance
     */
    public void setIdentiteNomNaissance(String identiteNomNaissance) {
        this.identiteNomNaissance = identiteNomNaissance;
    }

    /**
     * Setter de l'attribut identitePrenom1.
     * 
     * @param identitePrenom1
     *            la nouvelle valeur de identitePrenom1
     */
    public void setIdentitePrenom1(String identitePrenom1) {
        this.identitePrenom1 = identitePrenom1;
    }

    /**
     * Setter de l'attribut identitePrenom2.
     * 
     * @param identitePrenom2
     *            la nouvelle valeur de identitePrenom2
     */
    public void setIdentitePrenom2(String identitePrenom2) {
        this.identitePrenom2 = identitePrenom2;
    }

    /**
     * Setter de l'attribut identitePrenom3.
     * 
     * @param identitePrenom3
     *            la nouvelle valeur de identitePrenom3
     */
    public void setIdentitePrenom3(String identitePrenom3) {
        this.identitePrenom3 = identitePrenom3;
    }

    /**
     * Setter de l'attribut identitePrenom4.
     * 
     * @param identitePrenom4
     *            la nouvelle valeur de identitePrenom4
     */
    public void setIdentitePrenom4(String identitePrenom4) {
        this.identitePrenom4 = identitePrenom4;
    }

    /**
     * Setter de l'attribut lienParente.
     * 
     * @param lienParente
     *            la nouvelle valeur de lienParente
     */
    public void setLienParente(String lienParente) {
        this.lienParente = lienParente;
    }

    /**
     * Setter de l'attribut niveau.
     * 
     * @param niveau
     *            la nouvelle valeur de niveau
     */
    public void setNiveau(String niveau) {
        this.niveau = niveau;
    }

    /**
     * Setter de l'attribut numeroSecuriteSociale.
     * 
     * @param numeroSecuriteSociale
     *            la nouvelle valeur de numeroSecuriteSociale
     */
    public void setNumeroSecuriteSociale(String numeroSecuriteSociale) {
        this.numeroSecuriteSociale = numeroSecuriteSociale;
    }

    /**
     * Setter de l'attribut numeroSecuriteSocialeConnu.
     * 
     * @param numeroSecuriteSocialeConnu
     *            la nouvelle valeur de numeroSecuriteSocialeConnu
     */
    public void setNumeroSecuriteSocialeConnu(String numeroSecuriteSocialeConnu) {
        this.numeroSecuriteSocialeConnu = numeroSecuriteSocialeConnu;
    }

    /**
     * Setter de l'attribut organismeConventionneCode.
     * 
     * @param organismeConventionneCode
     *            la nouvelle valeur de organismeConventionneCode
     */
    public void setOrganismeConventionneCode(String organismeConventionneCode) {
        this.organismeConventionneCode = organismeConventionneCode;
    }

    /**
     * Setter de l'attribut organismeServantPension.
     * 
     * @param organismeServantPension
     *            la nouvelle valeur de organismeServantPension
     */
    public void setOrganismeServantPension(String organismeServantPension) {
        this.organismeServantPension = organismeServantPension;
    }

    /**
     * Setter de l'attribut regimeAssuranceMaladie.
     * 
     * @param regimeAssuranceMaladie
     *            la nouvelle valeur de regimeAssuranceMaladie
     */
    public void setRegimeAssuranceMaladie(String regimeAssuranceMaladie) {
        this.regimeAssuranceMaladie = regimeAssuranceMaladie;
    }

    /**
     * Setter de l'attribut regimeAssuranceMaladieAutre.
     * 
     * @param regimeAssuranceMaladieAutre
     *            la nouvelle valeur de regimeAssuranceMaladieAutre
     */
    public void setRegimeAssuranceMaladieAutre(String regimeAssuranceMaladieAutre) {
        this.regimeAssuranceMaladieAutre = regimeAssuranceMaladieAutre;
    }

    /**
     * Setter de l'attribut titreSejourDateExpiration.
     * 
     * @param titreSejourDateExpiration
     *            la nouvelle valeur de titreSejourDateExpiration
     */
    public void setTitreSejourDateExpiration(String titreSejourDateExpiration) {
        this.titreSejourDateExpiration = titreSejourDateExpiration;
    }

    /**
     * Setter de l'attribut titreSejourLieuDelivranceCommune.
     * 
     * @param titreSejourLieuDelivranceCommune
     *            la nouvelle valeur de titreSejourLieuDelivranceCommune
     */
    public void setTitreSejourLieuDelivranceCommune(String titreSejourLieuDelivranceCommune) {
        this.titreSejourLieuDelivranceCommune = titreSejourLieuDelivranceCommune;
    }

    /**
     * Setter de l'attribut titreSejourNumero.
     * 
     * @param titreSejourNumero
     *            la nouvelle valeur de titreSejourNumero
     */
    public void setTitreSejourNumero(String titreSejourNumero) {
        this.titreSejourNumero = titreSejourNumero;
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

}
