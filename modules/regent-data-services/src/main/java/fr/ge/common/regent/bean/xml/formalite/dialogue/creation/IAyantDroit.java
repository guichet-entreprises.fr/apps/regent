package fr.ge.common.regent.bean.xml.formalite.dialogue.creation;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

import fr.ge.common.regent.bean.xml.formalite.IFormalite;
import fr.ge.common.regent.bean.xml.transverse.IAdresse;

/**
 * Le Interface de l'entité des ayants droit à la déclaration sociale du
 * dirigeant de l'entreprise
 * 
 * 
 * 
 */
@ResourceXPath("/formalite")
public interface IAyantDroit extends IFormalite {

    /**
     * Getter d'une activité autre que déclarée dans le département .
     * 
     * @return la valeur de activiteAutreQueDeclareeDepartement
     */
    @FieldXPath(value = "activiteAutreQueDeclareeDepartement")
    String getActiviteAutreQueDeclareeDepartement();

    /**
     * Getter d'une activité autre que déclarée dans le pays
     * 
     * @return la valeur de activiteAutreQueDeclareePays
     */
    @FieldXPath(value = "activiteAutreQueDeclareePays")
    String getActiviteAutreQueDeclareePays();

    /**
     * Getter de l'attribut activiteAutreQueDeclareePresence.
     * 
     * @return la valeur de activiteAutreQueDeclareePresence
     */
    @FieldXPath(value = "activiteAutreQueDeclareePresence")
    String getActiviteAutreQueDeclareePresence();

    /**
     * Getter d'une activite autre que declareeStatut.
     * 
     * @return la valeur de activiteAutreQueDeclareeStatut
     */
    @FieldXPath(value = "activiteAutreQueDeclareeStatut")
    String getActiviteAutreQueDeclareeStatut();

    /**
     * Getter d'activite autre que declaree statut autre.
     * 
     * @return la valeur de activiteAutreQueDeclareeStatutAutre
     */
    @FieldXPath(value = "activiteAutreQueDeclareeStatutAutre")
    String getActiviteAutreQueDeclareeStatutAutre();

    /**
     * Getter de l'affiliation à la MSA.
     * 
     * @return la valeur de affiliationMSA
     */
    @FieldXPath(value = "affiliationMSA")
    String getAffiliationMSA();

    /**
     * Getter de nombre des ayants droit.
     * 
     * @return la valeur de ayantDroitNombre
     */
    @FieldXPath(value = "ayantDroitNombre")
    Integer getAyantDroitNombre();

    /**
     * Getter ayants droit bénéficiant de l'assurance maladie
     * 
     * 
     * @return la valeur de ayantDroitPresence
     */
    @FieldXPath(value = "ayantDroitPresence")
    String getAyantDroitPresence();

    /**
     * Getter de l'attribut ayantDroits.
     * 
     * @return la valeur de ayantDroits
     */

    /** Getter de la liste des ayants droit de l'ayant droit. */

    @FieldXPath(value = "ayantDroits/ayantDroit")
    IAyantDroit[] getAyantDroits();

    /**
     * Retire le ayant Droit de la liste des ayants droit.
     *
     * @param ayantDroit
     *            le ayantDroit
     */
    void removeFromAyantDroits(final IAyantDroit ayantDroit);

    /**
     * Ajoute un ayant droit de la liste des ayants droit.
     *
     * @return les ayantDroit
     */
    IAyantDroit addToAyantDroits();

    /**
     * Getter de conjoint couvert par l'assurance maladie.
     * 
     * @return la valeur de conjointCouvertAssuranceMaladie
     */
    @FieldXPath(value = "conjointCouvertAssuranceMaladie")
    String getConjointCouvertAssuranceMaladie();

    /**
     * Getter de fl'enfant couvert pa la securité sociale.
     * 
     * @return la valeur de enfantScolarise
     */
    @FieldXPath(value = "enfantScolarise")
    String getEnfantScolarise();

    /**
     * Getter de l'dentite complement civilite(sexe )
     * 
     * @return la valeur de identiteComplementCivilite
     */
    @FieldXPath(value = "identiteComplementCivilite")
    String getIdentiteComplementCivilite();

    /**
     * Getter de l'attribut identiteComplementDateNaissance.
     * 
     * @return la valeur de identiteComplementDateNaissance
     */
    @FieldXPath(value = "identiteComplementDateNaissance")
    String getIdentiteComplementDateNaissance();

    /**
     * Getter de l'attribut identiteComplementLieuNaissanceCommune.
     * 
     * @return la valeur de identiteComplementLieuNaissanceCommune
     */
    @FieldXPath(value = "identiteComplementLieuNaissanceCommune")
    String getIdentiteComplementLieuNaissanceCommune();

    /**
     * Getter de l'attribut identiteComplementLieuNaissanceDepartement.
     * 
     * @return la valeur de identiteComplementLieuNaissanceDepartement
     */
    @FieldXPath(value = "identiteComplementLieuNaissanceDepartement")
    String getIdentiteComplementLieuNaissanceDepartement();

    /**
     * Getter de l'attribut identiteComplementLieuNaissancePays.
     * 
     * @return la valeur de identiteComplementLieuNaissancePays
     */
    @FieldXPath(value = "identiteComplementLieuNaissancePays")
    String getIdentiteComplementLieuNaissancePays();

    /**
     * Getter de l'attribut identiteComplementLieuNaissanceVille.
     * 
     * @return la valeur de identiteComplementLieuNaissanceVille
     */
    @FieldXPath(value = "identiteComplementLieuNaissanceVille")
    String getIdentiteComplementLieuNaissanceVille();

    /**
     * Getter de l'attribut identiteNationalite.
     * 
     * @return la valeur de identiteNationalite
     */
    @FieldXPath(value = "identiteNationalite")
    String getIdentiteNationalite();

    /**
     * Getter de l'attribut identiteNomNaissance.
     * 
     * @return la valeur de identiteNomNaissance
     */
    @FieldXPath(value = "identiteNomNaissance")
    String getIdentiteNomNaissance();

    /**
     * Getter de l'attribut identitePrenom1.
     * 
     * @return la valeur de identitePrenom1
     */
    @FieldXPath(value = "identitePrenom1")
    String getIdentitePrenom1();

    /**
     * Getter de l'attribut identitePrenom2.
     * 
     * @return la valeur de identitePrenom2
     */
    @FieldXPath(value = "identitePrenom2")
    String getIdentitePrenom2();

    /**
     * Getter de l'attribut identitePrenom3.
     * 
     * @return la valeur de identitePrenom3
     */
    @FieldXPath(value = "identitePrenom3")
    String getIdentitePrenom3();

    /**
     * Getter de l'attribut identitePrenom4.
     * 
     * @return la valeur de identitePrenom4
     */
    @FieldXPath(value = "identitePrenom4")
    String getIdentitePrenom4();

    /**
     * Getter de l'attribut lienParente.
     * 
     * @return la valeur de lienParente
     */
    @FieldXPath(value = "lienParente")
    String getLienParente();

    /**
     * Getter de l'attribut niveau.
     * 
     * @return la valeur de niveau
     */
    @FieldXPath(value = "niveau")
    String getNiveau();

    /**
     * Getter de l'attribut numero de la sécurite Sociale.
     * 
     * @return la valeur de numeroSecuriteSociale
     */
    @FieldXPath(value = "numeroSecuriteSociale")
    String getNumeroSecuriteSociale();

    /**
     * Getter de numero de la sécurite sociale connu.
     * 
     * @return la valeur de numeroSecuriteSocialeConnu
     */
    @FieldXPath(value = "numeroSecuriteSocialeConnu")
    String getNumeroSecuriteSocialeConnu();

    /**
     * Getter de l'organisme conventionne de code.
     * 
     * @return la valeur de organismeConventionneCode
     */
    @FieldXPath(value = "organismeConventionneCode")
    String getOrganismeConventionneCode();

    /**
     * Getter de l'organisme servant pension.
     * 
     * @return la valeur de organismeServantPension
     */
    @FieldXPath(value = "organismeServantPension")
    String getOrganismeServantPension();

    /**
     * Getter de regime d'assurance maladie.
     * 
     * @return la valeur de regimeAssuranceMaladie
     */
    @FieldXPath(value = "regimeAssuranceMaladie")
    String getRegimeAssuranceMaladie();

    /**
     * Getter de l'attribut regime d'assurance maladie autre.
     * 
     * @return la valeur de regimeAssuranceMaladieAutre
     */
    @FieldXPath(value = "regimeAssuranceMaladieAutre")
    String getRegimeAssuranceMaladieAutre();

    /**
     * Getter de la date d'experation de titre de séjour
     * 
     * @return la valeur de titreSejourDateExpiration
     */
    @FieldXPath(value = "titreSejourDateExpiration")
    String getTitreSejourDateExpiration();

    /**
     * Getter de la commune de délivrance de titre de séjour.
     * 
     * @return la valeur de titreSejourLieuDelivranceCommune
     */
    @FieldXPath(value = "titreSejourLieuDelivranceCommune")
    String getTitreSejourLieuDelivranceCommune();

    /**
     * Getter de numéro de titre de séjour.
     * 
     * @return la valeur de titreSejourNumero
     */
    @FieldXPath(value = "titreSejourNumero")
    String getTitreSejourNumero();

    /**
     * Get le adresse.
     *
     * @return le adresse
     */
    @FieldXPath(value = "adresse")
    IAdresse getAdresse();

    /**
     * New adresse.
     *
     * @return le i adresse
     */
    IAdresse newAdresse();

    /**
     * Set le adresse.
     *
     * @param adresse
     *            le nouveau adresse
     */
    void setAdresse(IAdresse adresse);

    /**
     * Setter d'une activité autre que déclarée dans le département.
     * 
     * @param activiteAutreQueDeclareeDepartement
     *            la nouvelle valeur de activiteAutreQueDeclareeDepartement
     */
    void setActiviteAutreQueDeclareeDepartement(String activiteAutreQueDeclareeDepartement);

    /**
     * Setter d'une activité autre que déclarée dans le pays
     * 
     * @param activiteAutreQueDeclareePays
     *            la nouvelle valeur de activiteAutreQueDeclareePays
     */
    void setActiviteAutreQueDeclareePays(String activiteAutreQueDeclareePays);

    /**
     * Setter de l' activite autre que declaree presence.
     * 
     * @param activiteAutreQueDeclareePresence
     *            la nouvelle valeur de activiteAutreQueDeclareePresence
     */
    void setActiviteAutreQueDeclareePresence(String activiteAutreQueDeclareePresence);

    /**
     * Setter de l'attribut activite autre que declaree statut.
     * 
     * @param activiteAutreQueDeclareeStatut
     *            la nouvelle valeur de activiteAutreQueDeclareeStatut
     */
    void setActiviteAutreQueDeclareeStatut(String activiteAutreQueDeclareeStatut);

    /**
     * Setter de l'activiteAutre que declaree statutAutre.
     * 
     * @param activiteAutreQueDeclareeStatutAutre
     *            la nouvelle valeur de activiteAutreQueDeclareeStatutAutre
     */
    void setActiviteAutreQueDeclareeStatutAutre(String activiteAutreQueDeclareeStatutAutre);

    /**
     * Setter de l'attribut affiliation à la MSA.
     * 
     * @param affiliationMSA
     *            la nouvelle valeur de affiliationMSA
     */
    void setAffiliationMSA(String affiliationMSA);

    /**
     * Setter de nombre des ayants Droit.
     * 
     * @param ayantDroitNombre
     *            la nouvelle valeur de ayantDroitNombre
     */
    void setAyantDroitNombre(Integer ayantDroitNombre);

    /**
     * Setter de Presence d' ayant Droit .
     * 
     * @param ayantDroitPresence
     *            la nouvelle valeur de ayantDroitPresence
     */
    void setAyantDroitPresence(String ayantDroitPresence);

    /**
     * Setter de la liste des ayants Droit.
     * 
     * @param ayantDroits
     *            la nouvelle valeur de ayantDroits
     */
    void setAyantDroits(IAyantDroit[] ayantDroits);

    /**
     * Setter de conjoint couvert par assurance maladie.
     * 
     * @param conjointCouvertAssuranceMaladie
     *            la nouvelle valeur de conjointCouvertAssuranceMaladie
     */
    void setConjointCouvertAssuranceMaladie(String conjointCouvertAssuranceMaladie);

    /**
     * Setter de l'enfant scolarise.
     * 
     * @param enfantScolarise
     *            la nouvelle valeur de enfantScolarise
     */
    void setEnfantScolarise(String enfantScolarise);

    /**
     * Setter de l'identité complement civilite(sexe).
     * 
     * @param identiteComplementCivilite
     *            la nouvelle valeur de identiteComplementCivilite
     */
    void setIdentiteComplementCivilite(String identiteComplementCivilite);

    /**
     * Setter de l'identité complement date de naissance.
     * 
     * @param identiteComplementDateNaissance
     *            la nouvelle valeur de identiteComplementDateNaissance
     */
    void setIdentiteComplementDateNaissance(String identiteComplementDateNaissance);

    /**
     * Setter de l'identite complement de lieu de naissance commune.
     * 
     * @param identiteComplementLieuNaissanceCommune
     *            la nouvelle valeur de identiteComplementLieuNaissanceCommune
     */
    void setIdentiteComplementLieuNaissanceCommune(String identiteComplementLieuNaissanceCommune);

    /**
     * Setter de l'identité complement de lieu de naissance département.
     * 
     * @param identiteComplementLieuNaissanceDepartement
     *            la nouvelle valeur de
     *            identiteComplementLieuNaissanceDepartement
     */
    void setIdentiteComplementLieuNaissanceDepartement(String identiteComplementLieuNaissanceDepartement);

    /**
     * Setter de l'identité complement de Lieu de Naissance Pays.
     * 
     * @param identiteComplementLieuNaissancePays
     *            la nouvelle valeur de identiteComplementLieuNaissancePays
     */
    void setIdentiteComplementLieuNaissancePays(String identiteComplementLieuNaissancePays);

    /**
     * Setter de l'identité complement lieu naissance Ville.
     * 
     * @param identiteComplementLieuNaissanceVille
     *            la nouvelle valeur de identiteComplementLieuNaissanceVille
     */
    void setIdentiteComplementLieuNaissanceVille(String identiteComplementLieuNaissanceVille);

    /**
     * Setter de l'attribut identiteNationalite.
     * 
     * @param identiteNationalite
     *            la nouvelle valeur de identiteNationalite
     */
    void setIdentiteNationalite(String identiteNationalite);

    /**
     * Setter de l'identite nom de naissance.
     * 
     * @param identiteNomNaissance
     *            la nouvelle valeur de identiteNomNaissance
     */
    void setIdentiteNomNaissance(String identiteNomNaissance);

    /**
     * Setter de l'identité Prenom1.
     * 
     * @param identitePrenom1
     *            la nouvelle valeur de identitePrenom1
     */
    void setIdentitePrenom1(String identitePrenom1);

    /**
     * Setter de l'identite Prenom2.
     * 
     * @param identitePrenom2
     *            la nouvelle valeur de identitePrenom2
     */
    void setIdentitePrenom2(String identitePrenom2);

    /**
     * Setter de l'identité Prenom3.
     * 
     * @param identitePrenom3
     *            la nouvelle valeur de identitePrenom3
     */
    void setIdentitePrenom3(String identitePrenom3);

    /**
     * Setter de l'identité Prenom4.
     * 
     * @param identitePrenom4
     *            la nouvelle valeur de identitePrenom4
     */
    void setIdentitePrenom4(String identitePrenom4);

    /**
     * Setter de lien de parente.
     * 
     * @param lienParente
     *            la nouvelle valeur de lienParente
     */
    void setLienParente(String lienParente);

    /**
     * Setter de l'attribut niveau.
     * 
     * @param niveau
     *            la nouvelle valeur de niveau
     */
    void setNiveau(String niveau);

    /**
     * Setter de numero de la sécurite sociale.
     * 
     * @param numeroSecuriteSociale
     *            la nouvelle valeur de numeroSecuriteSociale
     */
    void setNumeroSecuriteSociale(String numeroSecuriteSociale);

    /**
     * Setter de numero de la sécurite sociale connu.
     * 
     * @param numeroSecuriteSocialeConnu
     *            la nouvelle valeur de numeroSecuriteSocialeConnu
     */
    void setNumeroSecuriteSocialeConnu(String numeroSecuriteSocialeConnu);

    /**
     * Setter de code de l'organisme conventionné.
     * 
     * @param organismeConventionneCode
     *            la nouvelle valeur de organismeConventionneCode
     */
    void setOrganismeConventionneCode(String organismeConventionneCode);

    /**
     * Setter de l'organisme servant pension.
     * 
     * @param organismeServantPension
     *            la nouvelle valeur de organismeServantPension
     */
    void setOrganismeServantPension(String organismeServantPension);

    /**
     * Setter de regime d'assurance maladie.
     * 
     * @param regimeAssuranceMaladie
     *            la nouvelle valeur de regimeAssuranceMaladie
     */
    void setRegimeAssuranceMaladie(String regimeAssuranceMaladie);

    /**
     * Setter de l'attribut regime d'assurance maladie Autre.
     * 
     * @param regimeAssuranceMaladieAutre
     *            la nouvelle valeur de regimeAssuranceMaladieAutre
     */
    void setRegimeAssuranceMaladieAutre(String regimeAssuranceMaladieAutre);

    /**
     * Setter de date d'expiration de titre de sejour.
     * 
     * @param titreSejourDateExpiration
     *            la nouvelle valeur de titreSejourDateExpiration
     */
    void setTitreSejourDateExpiration(String titreSejourDateExpiration);

    /**
     * Setter de commune de délivrance de titre de séjour.
     * 
     * @param titreSejourLieuDelivranceCommune
     *            la nouvelle valeur de titreSejourLieuDelivranceCommune
     */
    void setTitreSejourLieuDelivranceCommune(String titreSejourLieuDelivranceCommune);

    /**
     * Setter de numéro de titre de séjour
     * 
     * @param titreSejourNumero
     *            la nouvelle valeur de titreSejourNumero
     */
    void setTitreSejourNumero(String titreSejourNumero);

}
