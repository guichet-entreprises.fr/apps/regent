package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface ICD.
 */
@ResourceXPath("/ICD")
public interface ICD {

  /**
   * Get le D44.
   *
   * @return le D44
   */
  @FieldXPath("D44")
  String getD44();

  /**
   * Set le D44.
   *
   * @param D44
   *          le nouveau D44
   */
  void setD44(String D44);

  /**
   * Get le D42.
   *
   * @return le D42
   */
  @FieldXPath("D42")
  ID42[] getD42();

  /**
   * Ajoute le to D42.
   *
   * @return le D42
   */
  ID42 addToD42();

  /**
   * Set le D42.
   *
   * @param D42
   *          le nouveau D42
   */
  void setD42(ID42[] D42);

  /**
   * Get le D43.
   *
   * @return le D43
   */
  @FieldXPath("D43")
  ID43[] getD43();

  /**
   * Ajoute le to D43.
   *
   * @return le D43
   */
  ID43 addToD43();

  /**
   * Set le D43.
   *
   * @param D43
   *          le nouveau D43
   */
  void setD43(ID43[] D43);

  /**
   * Get le D45.
   *
   * @return le D45
   */
  @FieldXPath("D45")
  ID45[] getD45();

  /**
   * Ajoute le to D45.
   *
   * @return le D45
   */
  ID45 addToD45();

  /**
   * Set le D45.
   *
   * @param D45
   *          le nouveau D45
   */
  void setD45(ID45[] D45);

}
