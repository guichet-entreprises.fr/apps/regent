package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IPU.
 */
@ResourceXPath("/IPU")
public interface IPU {

  /**
   * Get le u01.
   *
   * @return le u01
   */
  @FieldXPath("U01")
  IU01[] getU01();

  /**
   * Ajoute le to u01.
   *
   * @return le i u01
   */
  IU01 addToU01();

  /**
   * Set le.
   *
   * @param U01
   *          le u01
   */
  void set(IU01[] U01);

  /**
   * Get le u02.
   *
   * @return le u02
   */
  @FieldXPath("U02")
  String getU02();

  /**
   * Set le u02.
   *
   * @param U02
   *          le nouveau u02
   */
  void setU02(String U02);

  /**
   * Get le u04.
   *
   * @return le u04
   */
  @FieldXPath("U04")
  String getU04();

  /**
   * Set le u04.
   *
   * @param U04
   *          le nouveau u04
   */
  void setU04(String U04);

  /**
   * Get le u06.
   *
   * @return le u06
   */
  @FieldXPath("U06")
  String getU06();

  /**
   * Set le u06.
   *
   * @param U06
   *          le nouveau u06
   */
  void setU06(String U06);
}
