package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface CPU.
 */
@ResourceXPath("/CPU")
public interface CPU {

  /**
   * Get le u21.
   *
   * @return le u21
   */
  @FieldXPath("U21")
  String getU21();

  /**
   * Set le u21.
   *
   * @param U21
   *          le nouveau u21
   */
  void setU21(String U21);

  /**
   * Get le u24.
   *
   * @return le u24
   */
  @FieldXPath("U24")
  String getU24();

  /**
   * Set le u24.
   *
   * @param U24
   *          le nouveau u24
   */
  void setU24(String U24);

  /**
   * Get le U26.
   *
   * @return le U26
   */
  @FieldXPath("U26")
  String getU26();

  /**
   * Set le U26.
   *
   * @param U26
   *          le nouveau U26
   */
  void setU26(String U26);

  /**
   * Get le U28.
   *
   * @return le U28
   */
  @FieldXPath("U28")
  String getU28();

  /**
   * Set le U28.
   *
   * @param U28
   *          le nouveau U28
   */
  void setU28(String U28);

  /**
   * Get le U29.
   *
   * @return le U29
   */
  @FieldXPath("U29")
  String getU29();

  /**
   * Set le U29.
   *
   * @param U29
   *          le nouveau U29
   */
  void setU29(String U29);

  
  
  /**
   * Get le U40.
   *
   * @return le U40
   */
  @FieldXPath("U40")
  IU40[] getU40();

  /**
   * Ajoute le to U40.
   *
   * @return le i U40
   */
  IU40 addToU40();

  /**
   * Set le U40.
   *
   * @param U40
   *          le nouveau U40
   */
  void setU40(IU40[] U40);
  
  

  /**
   * Get le u22.
   *
   * @return le u22
   */
  @FieldXPath("U22")
  String getU22();

  /**
   * Set le u22.
   *
   * @param U22
   *          le nouveau u22
   */
  void setU22(String U22);

  /**
   * Get le u27.
   *
   * @return le u27
   */
  @FieldXPath("U27")
  String getU27();

  /**
   * Set le u27.
   *
   * @param U27
   *          le nouveau u27
   */
  void setU27(String U27);

}
