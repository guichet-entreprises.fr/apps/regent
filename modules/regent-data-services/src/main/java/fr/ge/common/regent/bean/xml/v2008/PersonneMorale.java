package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface PersonneMorale.
 */
@ResourceXPath("PersonneMorale")
public interface PersonneMorale {

  /**
   * Gets the icm.
   *
   * @return the icm
   */
  @FieldXPath("ICM")
  ICM[] getICM();

  /**
   * Add to icm.
   *
   * @return the icm
   */
  ICM addToICM();

  /**
   * Sets the icm.
   *
   * @param icm
   *          the new icm
   */
  void setICM(ICM[] icm);

  /**
   * Gets the fjm.
   *
   * @return the fjm
   */
  @FieldXPath("FJM")
  IFJM[] getFJM();

  /**
   * Add to fjm.
   *
   * @return the ifjm
   */
  IFJM addToFJM();

  /**
   * Sets the fjm.
   *
   * @param ifjm
   *          the new fjm
   */
  void setFJM(IFJM[] ifjm);

  /**
   * Gets the fsm.
   *
   * @return the fsm
   */
  @FieldXPath("FSM")
  IFSM[] getFSM();

  /**
   * Add to fsm.
   *
   * @return the ifsm
   */
  IFSM addToFSM();

  /**
   * Sets the fsm.
   *
   * @param fsm
   *          the new fsm
   */
  void setFSM(IFSM[] ifsm);

  /**
   * Gets the csm.
   *
   * @return the csm
   */
  @FieldXPath("CSM")
  ICSM[] getCSM();

  /**
   * Add to csm.
   *
   * @return the icsm
   */
  ICSM addToCSM();

  /**
   * Sets the csm.
   *
   * @param icsm
   *          the new csm
   */
  void setCSM(ICSM[] icsm);

  /**
   * Gets the acm.
   *
   * @return the acm
   */
  @FieldXPath("ACM")
  IACM[] getACM();

  /**
   * Add to acm.
   *
   * @return the iacm
   */
  IACM addToACM();

  /**
   * Sets the acm.
   *
   * @param acm
   *          the new acm
   */
  void setACM(IACM[] acm);

  /**
   * Gets the ngm.
   *
   * @return the ngm
   */
  @FieldXPath("NGM")
  INGM[] getNGM();

  /**
   * Add to ngm.
   *
   * @return the ingm
   */
  INGM addToNGM();

  /**
   * Sets the ngm.
   *
   * @param ngm
   *          the new ngm
   */
  void setNGM(INGM[] ngm);

  /**
   * Gets the jgm.
   *
   * @return the jgm
   */
  @FieldXPath("JGM")
  IJGM[] getJGM();

  /**
   * Add to jgm.
   *
   * @return the ijgm
   */
  IJGM addToJGM();

  /**
   * Sets the jgm.
   *
   * @param jgm
   *          the new jgm
   */
  void setJGM(IJGM[] jgm);

  /**
   * Gets the jim.
   *
   * @return the jim
   */
  @FieldXPath("JIM")
  IJIM[] getJIM();

  /**
   * Add to jim.
   *
   * @return the ijim
   */
  IJIM addToJIM();

  /**
   * Sets the jim.
   *
   * @param jim
   *          the new jim
   */
  void setJIM(IJIM[] jim);
}
