package fr.ge.common.regent.bean.xml.formalite.dialogue.cessation;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

import fr.ge.common.regent.bean.xml.formalite.IFormalite;

/**
 * Le Interface IDirigeantCessation.
 */
@ResourceXPath("/formalite")
public interface IDirigeantCessation extends IFormalite {

    /** La constante MODEL_VERSION. */
    static final int MODEL_VERSION = 1;

    /**
     * Get le pp nom naissance.
     *
     * @return le pp nom naissance
     */
    @FieldXPath(value = "ppNomNaissance")
    String getPpNomNaissance();

    /**
     * Get le pp nom usage.
     *
     * @return le pp nom usage
     */
    @FieldXPath(value = "ppNomUsage")
    String getPpNomUsage();

    /**
     * Get le pp prenom1.
     *
     * @return le pp prenom1
     */
    @FieldXPath(value = "ppPrenom1")
    String getPpPrenom1();

    /**
     * Get le pp prenom2.
     *
     * @return le pp prenom2
     */
    @FieldXPath(value = "ppPrenom2")
    String getPpPrenom2();

    /**
     * Get le pp prenom3.
     *
     * @return le pp prenom3
     */
    @FieldXPath(value = "ppPrenom3")
    String getPpPrenom3();

    /**
     * Get le pp prenom4.
     *
     * @return le pp prenom4
     */
    @FieldXPath(value = "ppPrenom4")
    String getPpPrenom4();

    /**
     * Get le pp date naissance.
     *
     * @return le pp date naissance
     */
    @FieldXPath(value = "ppDateNaissance")
    String getPpDateNaissance();

    /**
     * Get le pp lieu naissance pays.
     *
     * @return le pp lieu naissance pays
     */
    @FieldXPath(value = "ppLieuNaissancePays")
    String getPpLieuNaissancePays();

    /**
     * Get le pp lieu naissance departement.
     *
     * @return le pp lieu naissance departement
     */
    @FieldXPath(value = "ppLieuNaissanceDepartement")
    String getPpLieuNaissanceDepartement();

    /**
     * Get le pp lieu naissance commune.
     *
     * @return le pp lieu naissance commune
     */
    @FieldXPath(value = "ppLieuNaissanceCommune")
    String getPpLieuNaissanceCommune();

    /**
     * Get le pp lieu naissance ville.
     *
     * @return le pp lieu naissance ville
     */
    @FieldXPath(value = "ppLieuNaissanceVille")
    String getPpLieuNaissanceVille();

    /**
     * Get le departement organisme conventionne.
     *
     * @return le departement organisme conventionne
     */
    @FieldXPath(value = "departementOrganismeConventionne")
    String getDepartementOrganismeConventionne();

    /**
     * Get le pp organisme conventionne.
     *
     * @return le pp organisme conventionne
     */
    @FieldXPath(value = "ppOrganismeConventionne")
    String getPpOrganismeConventionne();

    /**
     * Set le pp nom naissance.
     *
     * @param ppNomNaissance
     *            le nouveau pp nom naissance
     */
    void setPpNomNaissance(String ppNomNaissance);

    /**
     * Set le pp nom usage.
     *
     * @param ppNomUsage
     *            le nouveau pp nom usage
     */
    void setPpNomUsage(String ppNomUsage);

    /**
     * Set le pp prenom1.
     *
     * @param ppPrenom1
     *            le nouveau pp prenom1
     */
    void setPpPrenom1(String ppPrenom1);

    /**
     * Set le pp prenom2.
     *
     * @param ppPrenom2
     *            le nouveau pp prenom2
     */
    void setPpPrenom2(String ppPrenom2);

    /**
     * Set le pp prenom3.
     *
     * @param ppPrenom3
     *            le nouveau pp prenom3
     */
    void setPpPrenom3(String ppPrenom3);

    /**
     * Set le pp prenom4.
     *
     * @param ppPrenom4
     *            le nouveau pp prenom4
     */
    void setPpPrenom4(String ppPrenom4);

    /**
     * Set le pp date naissance.
     *
     * @param ppDateNaissance
     *            le nouveau pp date naissance
     */
    void setPpDateNaissance(String ppDateNaissance);

    /**
     * Set le pp lieu naissance pays.
     *
     * @param ppLieuNaissancePays
     *            le nouveau pp lieu naissance pays
     */
    void setPpLieuNaissancePays(String ppLieuNaissancePays);

    /**
     * Set le pp lieu naissance departement.
     *
     * @param ppLieuNaissanceDepartement
     *            le nouveau pp lieu naissance departement
     */
    void setPpLieuNaissanceDepartement(String ppLieuNaissanceDepartement);

    /**
     * Set le pp lieu naissance commune.
     *
     * @param ppLieuNaissanceCommune
     *            le nouveau pp lieu naissance commune
     */
    void setPpLieuNaissanceCommune(String ppLieuNaissanceCommune);

    /**
     * Set le pp lieu naissance ville.
     *
     * @param ppLieuNaissanceVille
     *            le nouveau pp lieu naissance ville
     */
    void setPpLieuNaissanceVille(String ppLieuNaissanceVille);

    /**
     * Set le departement organisme conventionne.
     *
     * @param departementOrganismeConventionne
     *            le nouveau departement organisme conventionne
     */
    void setDepartementOrganismeConventionne(String departementOrganismeConventionne);

    /**
     * Set le pp organisme conventionne.
     *
     * @param ppOrganismeConventionne
     *            le nouveau pp organisme conventionne
     */
    void setPpOrganismeConventionne(String ppOrganismeConventionne);

}
