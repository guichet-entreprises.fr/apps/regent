package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

@ResourceXPath("/U51")
public interface IU51 {

  /**
   * Get le u511.
   *
   * @return le u511
   */
  @FieldXPath("U51.1")
  String getU511();

  /**
   * Set le u511
   *
   * @param U511
   *          le nouveau u511
   */
  void setU511(String U511);

  /**
   * Get le u512.
   *
   * @return le u512
   */
  @FieldXPath("U51.2")
  String getU512();

  /**
   * Set le u512
   *
   * @param U512
   *          le nouveau u512
   */
  void setU512(String U512);

  /**
   * Get le u513.
   *
   * @return le u513
   */
  @FieldXPath("U51.3")
  String getU513();

  /**
   * Set le u513
   *
   * @param U513
   *          le nouveau u513
   */
  void setU513(String U513);

  /**
   * Get le u514.
   *
   * @return le u514
   */
  @FieldXPath("U51.4")
  String getU514();

  /**
   * Set le u514
   *
   * @param u514
   *          le nouveau u514
   */
  void setU514(String U514);

}
