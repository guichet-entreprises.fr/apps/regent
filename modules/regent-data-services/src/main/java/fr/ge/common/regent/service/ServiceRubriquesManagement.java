/**
 *
 */
package fr.ge.common.regent.service;

import java.util.List;

import fr.ge.common.regent.bean.modele.referentiel.ENormeRubrique;

/**
 *
 * Service to get the Tags by evenments and version criteria
 *
 * @author $Author: kelmoura $
 *
 * @version $Revision: 0 $
 *
 * @param version
 *            : the version of the regent we are using, must not be null
 *
 * @param evenements
 *            : the events for which we are looking for the tag, must not be
 *            null
 *
 * @return The list of tags that will be used in the regent XML
 *
 */

public interface ServiceRubriquesManagement {

    List<ENormeRubrique> getRubriquesPathByEvenementAndVersion(final String version, final List<String> evenements);

}
