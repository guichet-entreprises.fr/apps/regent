package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface ADF.
 */
@ResourceXPath("/ADF")
public interface ADF {

  /**
   * Get le c36.
   *
   * @return le c36
   */
  @FieldXPath("C36")
  String getC36();

  /**
   * Set le c36.
   *
   * @param C36
   *          le nouveau c36
   */
  void setC36(String C36);

  /**
   * Get le c37.
   *
   * @return le c37
   */
  @FieldXPath("C37")
  IC37[] getC37();

  /**
   * Ajoute le to c37.
   *
   * @return le i c37
   */
  IC37 addToC37();

  /**
   * Set le c37.
   *
   * @param C37
   *          le nouveau c37
   */
  void setC37(IC37[] C37);

  /**
   * Get le c39.
   *
   * @return le c39
   */
  @FieldXPath("C39")
  IC39[] getC39();

  /**
   * Ajoute le to c39.
   *
   * @return le i c39
   */
  IC39 addToC39();

  /**
   * Set le c39.
   *
   * @param C39
   *          le nouveau c39
   */
  void setC39(IC39[] C39);

}
