package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IE12.
 */
@ResourceXPath("/E12")
public interface IE12 {

  /**
   * Get le e123.
   *
   * @return le e123
   */
  @FieldXPath("E12.3")
  String getE123();

  /**
   * Set le e123.
   *
   * @param E123
   *          le nouveau e123
   */
  void setE123(String E123);

  /**
   * Get le e125.
   *
   * @return le e125
   */
  @FieldXPath("E12.5")
  String getE125();

  /**
   * Set le e125.
   *
   * @param E125
   *          le nouveau e125
   */
  void setE125(String E125);

  /**
   * Get le e126.
   *
   * @return le e126
   */
  @FieldXPath("E12.6")
  String getE126();

  /**
   * Set le e126.
   *
   * @param E126
   *          le nouveau e126
   */
  void setE126(String E126);

  /**
   * Get le e127.
   *
   * @return le e127
   */
  @FieldXPath("E12.7")
  String getE127();

  /**
   * Set le e127.
   *
   * @param E127
   *          le nouveau e127
   */
  void setE127(String E127);

  /**
   * Get le e128.
   *
   * @return le e128
   */
  @FieldXPath("E12.8")
  String getE128();

  /**
   * Set le e128.
   *
   * @param E128
   *          le nouveau e128
   */
  void setE128(String E128);

  /**
   * Get le e1210.
   *
   * @return le e1210
   */
  @FieldXPath("E12.10")
  String getE1210();

  /**
   * Set le e1210.
   *
   * @param E1210
   *          le nouveau e1210
   */
  void setE1210(String E1210);

  /**
   * Get le e1211.
   *
   * @return le e1211
   */
  @FieldXPath("E12.11")
  String getE1211();

  /**
   * Set le e1211.
   *
   * @param E1211
   *          le nouveau e1211
   */
  void setE1211(String E1211);

  /**
   * Get le e1212.
   *
   * @return le e1212
   */
  @FieldXPath("E12.12")
  String getE1212();

  /**
   * Set le e1212.
   *
   * @param E1212
   *          le nouveau e1212
   */
  void setE1212(String E1212);

  /**
   * Get le e1213.
   *
   * @return le e1213
   */
  @FieldXPath("E12.13")
  String getE1213();

  /**
   * Set le e1213.
   *
   * @param E1213
   *          le nouveau e1213
   */
  void setE1213(String E1213);

  /**
   * Get le e1214.
   *
   * @return le e1214
   */
  @FieldXPath("E12.14")
  String getE1214();

  /**
   * Set le e1214.
   *
   * @param E1214
   *          le nouveau e1214
   */
  void setE1214(String E1214);

}
