package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IE02.
 */
@ResourceXPath("/U33")
public interface IU33 {

  /**
   * Get le U33.1.
   *
   * @return le U33.1
   */
  @FieldXPath("U33.1")
  String getU331();

  /**
   * Set le U331.
   *
   * @param U331
   *          le nouveau U331
   */
  void setU331(String U331);

  /**
   * Get le U33.2.
   *
   * @return le U33.2
   */
  @FieldXPath("U33.2")
  String getU332();

  /**
   * Set le U332.
   *
   * @param U332
   *          le nouveau U332
   */
  void setU332(String U332);

  /**
   * Get le U33.3.
   *
   * @return le U33.3
   */
  @FieldXPath("U33.3")
  String getU333();

  /**
   * Set le U33.3.
   *
   * @param U33
   *          .3 le nouveau U33.3
   */
  void setU333(String U333);

}
