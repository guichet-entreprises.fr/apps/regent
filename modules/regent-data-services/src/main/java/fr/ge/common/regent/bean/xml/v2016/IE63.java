package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IE63.
 */
@ResourceXPath("/E63")
public interface IE63 {

  /**
   * Get le e633.
   *
   * @return le e633
   */
  @FieldXPath("E63.3")
  String getE633();

  /**
   * Set le e633.
   *
   * @param E633
   *          le nouveau e633
   */
  void setE633(String E633);

  /**
   * Get le e635.
   *
   * @return le e635
   */
  @FieldXPath("E63.5")
  String getE635();

  /**
   * Set le e635.
   *
   * @param E635
   *          le nouveau e635
   */
  void setE635(String E635);

  /**
   * Get le e636.
   *
   * @return le e636
   */
  @FieldXPath("E63.6")
  String getE636();

  /**
   * Set le e636.
   *
   * @param E636
   *          le nouveau e636
   */
  void setE636(String E636);

  /**
   * Get le e637.
   *
   * @return le e637
   */
  @FieldXPath("E63.7")
  String getE637();

  /**
   * Set le e637.
   *
   * @param E637
   *          le nouveau e637
   */
  void setE637(String E637);

  /**
   * Get le e6310.
   *
   * @return le e6310
   */
  @FieldXPath("E63.10")
  String getE6310();

  /**
   * Set le e6310.
   *
   * @param E6310
   *          le nouveau e6310
   */
  void setE6310(String E6310);

  /**
   * Get le e638.
   *
   * @return le e638
   */
  @FieldXPath("E63.8")
  String getE638();

  /**
   * Set le e638.
   *
   * @param E638
   *          le nouveau e638
   */
  void setE638(String E638);

  /**
   * Get le e6311.
   *
   * @return le e6311
   */
  @FieldXPath("E63.11")
  String getE6311();

  /**
   * Set le e6311.
   *
   * @param E6311
   *          le nouveau e6311
   */
  void setE6311(String E6311);

  /**
   * Get le e6312.
   *
   * @return le e6312
   */
  @FieldXPath("E63.12")
  String getE6312();

  /**
   * Set le e6312.
   *
   * @param E6312
   *          le nouveau e6312
   */
  void setE6312(String E6312);

  /**
   * Get le e6313.
   *
   * @return le e6313
   */
  @FieldXPath("E63.13")
  String getE6313();

  /**
   * Set le e6313.
   *
   * @param E6313
   *          le nouveau e6313
   */
  void setE6313(String E6313);

  /**
   * Get le e6314.
   *
   * @return le e6314
   */
  @FieldXPath("E63.14")
  String getE6314();

  /**
   * Set le e6314.
   *
   * @param E6314
   *          le nouveau e6314
   */
  void setE6314(String E6314);

}
