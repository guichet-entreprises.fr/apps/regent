package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IE73.
 */
@ResourceXPath("E73")
public interface IE73 {

  /**
   * Get le e731.
   *
   * @return le e731
   */
  @FieldXPath("E73.1")
  String getE731();

  /**
   * Set le e731.
   *
   * @param E731
   *          le nouveau e731
   */
  void setE731(String E731);

  /**
   * Get le e732.
   *
   * @return le e732
   */
  @FieldXPath("E73.2")
  String getE732();

  /**
   * Set le e732.
   *
   * @param E732
   *          le nouveau e732
   */
  void setE732(String E732);

  /**
   * Get le e7311.
   *
   * @return le E7311
   */
  @FieldXPath("E73.11")
  IE7311[] getE7311();

  /**
   * Ajoute le to E7311.
   *
   * @return le i E7311
   */
  IE7311 addToE7311();

  /**
   * Set le E7311.
   *
   * @param e7311
   *          le nouveau E7311
   */
  void setE7311(IE7311[] E7311);

}
