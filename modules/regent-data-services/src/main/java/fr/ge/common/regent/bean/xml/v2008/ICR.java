/**
 * 
 */
package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Interface ICR.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
@ResourceXPath("/ICR")
public interface ICR {

  /**
   * Accesseur sur l'attribut {@link #r01}.
   *
   * @return r01
   */
  @FieldXPath("R01")
  R01[] getR01();

  /**
   * Ajoute r01.
   *
   * @return r01
   */
  R01 addToR01();

  /**
   * Mutateur sur l'attribut {@link #r01}.
   *
   * @param R01
   *          la nouvelle valeur de l'attribut r01
   */
  void setR01(R01[] R01);

  /**
   * Accesseur sur l'attribut {@link #r02}.
   *
   * @return r02
   */
  @FieldXPath("R02")
  R02[] getR02();

  /**
   * Ajoute r02.
   *
   * @return r02
   */
  R02 addToR02();

  /**
   * Mutateur sur l'attribut {@link #r02}.
   *
   * @param R02
   *          la nouvelle valeur de l'attribut r02
   */
  void setR02(R02[] R02);

  /**
   * Accesseur sur l'attribut {@link #r03}.
   *
   * @return r03
   */
  @FieldXPath("R03")
  String getR03();

  /**
   * Mutateur sur l'attribut {@link #r03}.
   *
   * @param R03
   *          la nouvelle valeur de l'attribut r03
   */
  void setR03(String R03);

  /**
   * Accesseur sur l'attribut {@link #r04}.
   *
   * @return r04
   */
  @FieldXPath("R04")
  R04[] getR04();

  /**
   * Ajoute r04.
   *
   * @return r02
   */
  R04 addToR04();

  /**
   * Mutateur sur l'attribut {@link #r04}.
   *
   * @param R04
   *          la nouvelle valeur de l'attribut r04
   */
  void setR04(R04[] R04);

  /**
   * Accesseur sur l'attribut {@link #r05}.
   *
   * @return r05
   */
  @FieldXPath("R05")
  R05[] getR05();

  /**
   * Ajoute r05.
   *
   * @return r02
   */
  R05 addToR05();

  /**
   * Mutateur sur l'attribut {@link #r05}.
   *
   * @param R05
   *          la nouvelle valeur de l'attribut r05
   */
  void setR05(R05[] R05);

  /**
   * Accesseur sur l'attribut {@link #r06}.
   *
   * @return r06
   */
  @FieldXPath("R06")
  R06[] getR06();

  /**
   * Ajoute r06.
   *
   * @return r02
   */
  R06 addToR06();

  /**
   * Mutateur sur l'attribut {@link #r06}.
   *
   * @param R06
   *          la nouvelle valeur de l'attribut r06
   */
  void setR06(R06[] R06);

  /**
   * Accesseur sur l'attribut {@link #r07}.
   *
   * @return r07
   */
  @FieldXPath("R07")
  String getR07();

  /**
   * Mutateur sur l'attribut {@link #r07}.
   *
   * @param R07
   *          la nouvelle valeur de l'attribut r07
   */
  void setR07(String R07);

  /**
   * Accesseur sur l'attribut {@link #r08}.
   *
   * @return r08
   */
  @FieldXPath("R08")
  String getR08();

  /**
   * Mutateur sur l'attribut {@link #r08}.
   *
   * @param R08
   *          la nouvelle valeur de l'attribut r08
   */
  void setR08(String R08);

  /**
   * Accesseur sur l'attribut {@link #r22}.
   *
   * @return r22
   */
  @FieldXPath("R22")
  R22[] getR22();

  /**
   * Ajoute r22.
   *
   * @return r02
   */
  R22 addToR22();

  /**
   * Mutateur sur l'attribut {@link #r22}.
   *
   * @param R22
   *          la nouvelle valeur de l'attribut r22
   */
  void setR22(R22[] R22);
}
