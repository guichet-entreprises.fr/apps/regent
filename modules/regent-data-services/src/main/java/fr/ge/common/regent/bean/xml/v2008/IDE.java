package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IDE.
 */
@ResourceXPath("/IDE")
public interface IDE {

  /**
   * Get le e91.
   *
   * @return le e91
   */
  @FieldXPath("E91")
  IE91[] getE91();

  /**
   * Ajoute le to e91.
   *
   * @return le i e91
   */
  IE91 addToE91();

  /**
   * Set le e91.
   *
   * @param E91
   *          le nouveau e91
   */
  void setE91(IE91[] E91);

  /**
   * Get le e92.
   *
   * @return le e92
   */
  @FieldXPath("E92")
  IE92[] getE92();

  /**
   * Ajoute le to e92.
   *
   * @return le i e92
   */
  IE92 addToE92();

  /**
   * Set le e92.
   *
   * @param E92
   *          le nouveau e92
   */
  void setE92(IE92[] E92);

  /**
   * Get le e93.
   *
   * @return le e93
   */
  @FieldXPath("E93")
  IE93[] getE93();

  /**
   * Ajoute le to e93.
   *
   * @return le i e93
   */
  IE93 addToE93();

  /**
   * Set le e93.
   *
   * @param E93
   *          le nouveau e93
   */
  void setE93(IE93[] E93);

  /**
   * Get le e94.
   *
   * @return le e94
   */
  @FieldXPath("E94")
  IE94[] getE94();

  /**
   * Ajoute le to e94.
   *
   * @return le i e94
   */
  IE94 addToE94();

  /**
   * Set le e94.
   *
   * @param E94
   *          le nouveau e94
   */
  void setE94(IE94[] E94);

  /**
   * Get le e95.
   *
   * @return le e95
   */
  @FieldXPath("E95")
  IE95[] getE95();

  /**
   * Ajoute le to e95.
   *
   * @return le i e95
   */
  IE95 addToE95();

  /**
   * Set le e95.
   *
   * @param E95
   *          le nouveau e95
   */
  void setE95(IE95[] E95);

  /**
   * Get le e96.
   *
   * @return le e96
   */
  @FieldXPath("E96")
  String getE96();

  /**
   * Set le e96.
   *
   * @param E96
   *          le nouveau e96
   */
  void setE96(String E96);

}
