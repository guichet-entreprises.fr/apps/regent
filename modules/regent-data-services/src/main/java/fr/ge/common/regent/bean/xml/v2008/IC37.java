package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IC37.
 */
@ResourceXPath("/C37")
public interface IC37 {

  /**
   * Get le c373.
   *
   * @return le c373
   */
  @FieldXPath("C37.3")
  String getC373();

  /**
   * Set le c373.
   *
   * @param C373
   *          le nouveau c373
   */
  void setC373(String C373);

  /**
   * Get le c374.
   *
   * @return le c374
   */
  @FieldXPath("C37.4")
  String getC374();

  /**
   * Set le c374.
   *
   * @param C374
   *          le nouveau c374
   */
  void setC374(String C374);

  /**
   * Get le c375.
   *
   * @return le c375
   */
  @FieldXPath("C37.5")
  String getC375();

  /**
   * Set le c375.
   *
   * @param C375
   *          le nouveau c375
   */
  void setC375(String C375);

  /**
   * Get le c376.
   *
   * @return le c376
   */
  @FieldXPath("C37.6")
  String getC376();

  /**
   * Set le c376.
   *
   * @param C376
   *          le nouveau c376
   */
  void setC376(String C376);

  /**
   * Get le c377.
   *
   * @return le c377
   */
  @FieldXPath("C37.7")
  String getC377();

  /**
   * Set le c377.
   *
   * @param C377
   *          le nouveau c377
   */
  void setC377(String C377);

  /**
   * Get le c378.
   *
   * @return le c378
   */
  @FieldXPath("C37.8")
  String getC378();

  /**
   * Set le c378.
   *
   * @param C378
   *          le nouveau c378
   */
  void setC378(String C378);

  /**
   * Get le c379.
   *
   * @return le c379
   */
  @FieldXPath("C37.9")
  String getC379();

  /**
   * Set le c379.
   *
   * @param C379
   *          le nouveau c379
   */
  void setC379(String C379);

  /**
   * Get le c3710.
   *
   * @return le c3710
   */
  @FieldXPath("C37.10")
  String getC3710();

  /**
   * Set le c3710.
   *
   * @param C3710
   *          le nouveau c3710
   */
  void setC3710(String C3710);

  /**
   * Get le c3711.
   *
   * @return le c3711
   */
  @FieldXPath("C37.11")
  String getC3711();

  /**
   * Set le c3711.
   *
   * @param C3711
   *          le nouveau c3711
   */
  void setC3711(String C3711);

  /**
   * Get le c3712.
   *
   * @return le c3712
   */
  @FieldXPath("C37.12")
  String getC3712();

  /**
   * Set le c3712.
   *
   * @param C3712
   *          le nouveau c3712
   */
  void setC3712(String C3712);

  /**
   * Get le c3713.
   *
   * @return le c3713
   */
  @FieldXPath("C37.13")
  String getC3713();

  /**
   * Set le c3713.
   *
   * @param C3713
   *          le nouveau c3713
   */
  void setC3713(String C3713);

  /**
   * Get le c3714.
   *
   * @return le c3714
   */
  @FieldXPath("C37.14")
  String getC3714();

  /**
   * Set le c3714.
   *
   * @param C3714
   *          le nouveau c3714
   */
  void setC3714(String C3714);

}
