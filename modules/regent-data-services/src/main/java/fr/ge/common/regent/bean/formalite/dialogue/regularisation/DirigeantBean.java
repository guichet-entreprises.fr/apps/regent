package fr.ge.common.regent.bean.formalite.dialogue.regularisation;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import fr.ge.common.regent.bean.formalite.dialogue.AbstractDirigeantBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.AdresseBean;

/**
 * Le Class DirigeantBean.
 *
 * @author roveda
 */
public class DirigeantBean extends AbstractDirigeantBean<ConjointBean> implements IFormaliteRegularisationVue {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -7854891332644736396L;

    /** Le id technique. */
    private String idTechnique;

    /** Le modif identite nom prenom. */
    private List<String> modifIdentiteNomPrenom = new ArrayList<String>();

    /** Le modif date identite. */
    private String modifDateIdentite;

    /** Le modif ancien nom naissance. */
    private String modifAncienNomNaissance;

    /** Le modif ancien nom usage. */
    private String modifAncienNomUsage;

    /** Le modif ancien prenoms1. */
    private String modifAncienPrenoms1;

    /** Le modif ancien prenoms2. */
    private String modifAncienPrenoms2;

    /** Le modif ancien prenoms3. */
    private String modifAncienPrenoms3;

    /** Le modif ancien prenoms4. */
    private String modifAncienPrenoms4;

    /** Le modif anciennenationalite. */
    private String modifAnciennenationalite;

    /** Le modif ancien domicile. */
    private AdresseBean modifAncienDomicile = new AdresseBean();

    /** Le modif date domicile. */
    private String modifDateDomicile;

    /**
     * Get le id technique.
     *
     * @return the idTechnique
     */
    public String getIdTechnique() {
        return idTechnique;
    }

    /**
     * Set le id technique.
     *
     * @param idTechnique
     *            the idTechnique to set
     */
    public void setIdTechnique(String idTechnique) {
        this.idTechnique = idTechnique;
    }

    /**
     * Get le modif identite nom prenom.
     *
     * @return the modifIdentiteNomPrenom
     */
    public List<String> getModifIdentiteNomPrenom() {
        return modifIdentiteNomPrenom;
    }

    /**
     * Set le modif identite nom prenom.
     *
     * @param modifIdentiteNomPrenom
     *            the modifIdentiteNomPrenom to set
     */
    public void setModifIdentiteNomPrenom(List<String> modifIdentiteNomPrenom) {
        this.modifIdentiteNomPrenom = modifIdentiteNomPrenom;
    }

    /**
     * Get le modif date identite.
     *
     * @return the modifDateIdentite
     */
    public String getModifDateIdentite() {
        return modifDateIdentite;
    }

    /**
     * Set le modif date identite.
     *
     * @param modifDateIdentite
     *            the modifDateIdentite to set
     */
    public void setModifDateIdentite(String modifDateIdentite) {
        this.modifDateIdentite = modifDateIdentite;
    }

    /**
     * Get le modif ancien nom naissance.
     *
     * @return the modifAncienNomNaissance
     */
    public String getModifAncienNomNaissance() {
        return modifAncienNomNaissance;
    }

    /**
     * Set le modif ancien nom naissance.
     *
     * @param modifAncienNomNaissance
     *            the modifAncienNomNaissance to set
     */
    public void setModifAncienNomNaissance(String modifAncienNomNaissance) {
        this.modifAncienNomNaissance = modifAncienNomNaissance;
    }

    /**
     * Get le modif ancien nom usage.
     *
     * @return the modifAncienNomUsage
     */
    public String getModifAncienNomUsage() {
        return modifAncienNomUsage;
    }

    /**
     * Set le modif ancien nom usage.
     *
     * @param modifAncienNomUsage
     *            the modifAncienNomUsage to set
     */
    public void setModifAncienNomUsage(String modifAncienNomUsage) {
        this.modifAncienNomUsage = modifAncienNomUsage;
    }

    /**
     * Get le modif ancien prenoms1.
     *
     * @return the modifAncienPrenoms1
     */
    public String getModifAncienPrenoms1() {
        return modifAncienPrenoms1;
    }

    /**
     * Set le modif ancien prenoms1.
     *
     * @param modifAncienPrenoms1
     *            the modifAncienPrenoms1 to set
     */
    public void setModifAncienPrenoms1(String modifAncienPrenoms1) {
        this.modifAncienPrenoms1 = modifAncienPrenoms1;
    }

    /**
     * Get le modif ancien prenoms2.
     *
     * @return the modifAncienPrenoms2
     */
    public String getModifAncienPrenoms2() {
        return modifAncienPrenoms2;
    }

    /**
     * Set le modif ancien prenoms2.
     *
     * @param modifAncienPrenoms2
     *            the modifAncienPrenoms2 to set
     */
    public void setModifAncienPrenoms2(String modifAncienPrenoms2) {
        this.modifAncienPrenoms2 = modifAncienPrenoms2;
    }

    /**
     * Get le modif ancien prenoms3.
     *
     * @return the modifAncienPrenoms3
     */
    public String getModifAncienPrenoms3() {
        return modifAncienPrenoms3;
    }

    /**
     * Set le modif ancien prenoms3.
     *
     * @param modifAncienPrenoms3
     *            the modifAncienPrenoms3 to set
     */
    public void setModifAncienPrenoms3(String modifAncienPrenoms3) {
        this.modifAncienPrenoms3 = modifAncienPrenoms3;
    }

    /**
     * Get le modif ancien prenoms4.
     *
     * @return the modifAncienPrenoms4
     */
    public String getModifAncienPrenoms4() {
        return modifAncienPrenoms4;
    }

    /**
     * Set le modif ancien prenoms4.
     *
     * @param modifAncienPrenoms4
     *            the modifAncienPrenoms4 to set
     */
    public void setModifAncienPrenoms4(String modifAncienPrenoms4) {
        this.modifAncienPrenoms4 = modifAncienPrenoms4;
    }

    /**
     * Get le modif anciennenationalite.
     *
     * @return the modifAnciennenationalite
     */
    public String getModifAnciennenationalite() {
        return modifAnciennenationalite;
    }

    /**
     * Set le modif anciennenationalite.
     *
     * @param modifAnciennenationalite
     *            the modifAnciennenationalite to set
     */
    public void setModifAnciennenationalite(String modifAnciennenationalite) {
        this.modifAnciennenationalite = modifAnciennenationalite;
    }

    /**
     * Get le modif ancien domicile.
     *
     * @return the modifAncienDomicile
     */
    public AdresseBean getModifAncienDomicile() {
        return modifAncienDomicile;
    }

    /**
     * Set le modif ancien domicile.
     *
     * @param modifAncienDomicile
     *            the modifAncienDomicile to set
     */
    public void setModifAncienDomicile(AdresseBean modifAncienDomicile) {
        this.modifAncienDomicile = modifAncienDomicile;
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see fr.guichetentreprises.vue.AbstractDirigeantBean#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see fr.guichetentreprises.vue.IConjointFactory#createNewConjoint()
     */
    @Override
    public ConjointBean createNewConjoint() {
        return new ConjointBean();
    }

    /**
     * Get le modif date domicile.
     * 
     * @return the modifDateDomicile
     */
    public String getModifDateDomicile() {
        return this.modifDateDomicile;
    }

    /**
     * Set le modif date domicile.
     * 
     * @param modifDateDomicile
     *            the modifDateDomicile to set
     */
    public void setModifDateDomicile(String modifDateDomicile) {
        this.modifDateDomicile = modifDateDomicile;
    }

}
