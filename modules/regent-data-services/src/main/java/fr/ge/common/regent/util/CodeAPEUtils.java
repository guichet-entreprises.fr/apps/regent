package fr.ge.common.regent.util;

/**
 * Class CodeAPEUtils .
 * 
 * @author roveda
 * 
 */
public final class CodeAPEUtils {

  /**
   * Mantis877 Pour pouvoir le remplacer il faut que les tableaux de bord utilisent les beans profil
   * au lieu des IXml
   * 
   * à utiliser uniquement pour le fonction "ajax" de saisie libre du déclarant ET pour le
   * transformer dozer {@link fr.guichetentreprises.util.converter.CodeAPEConverterTest} Permet la
   * saisie du code APE de plusieurs manière.
   * 
   * @param activitePrincipaleCodeAPE
   *          code APE
   * @return code APE reformaté
   */
  @SuppressWarnings("unused")
  public static String transformeCodeAPE(String activitePrincipaleCodeAPE) {
    if (activitePrincipaleCodeAPE != null && !activitePrincipaleCodeAPE.isEmpty()) {
      activitePrincipaleCodeAPE = activitePrincipaleCodeAPE.toUpperCase();
      int lengthActPrinCodeAPE = activitePrincipaleCodeAPE.length();
      if (!activitePrincipaleCodeAPE.contains(".") && lengthActPrinCodeAPE > 1) {
        activitePrincipaleCodeAPE = activitePrincipaleCodeAPE.substring(0, 2) + "."
          + activitePrincipaleCodeAPE.substring(2, lengthActPrinCodeAPE);
      }
    }
    return activitePrincipaleCodeAPE;
  }

}
