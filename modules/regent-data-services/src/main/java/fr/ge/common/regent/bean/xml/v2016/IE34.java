package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;
import org.xmlfield.core.types.XmlString;

/**
 * Le Interface IE34.
 */
@ResourceXPath("/E34")
public interface IE34 {

  /**
   * Get le e341.
   *
   * @return le e341
   */
  @FieldXPath("E34.1")
  String getE341();

  /**
   * Set le e341.
   *
   * @param E341
   *          le nouveau e341
   */
  void setE341(String E341);

  /**
   * Get le e342.
   *
   * @return le e342
   */
  @FieldXPath("E34.2")
  String getE342();

  /**
   * Set le e342.
   *
   * @param E342
   *          le nouveau e342
   */
  void setE342(String E342);

  /**
   * Get le e343.
   *
   * @return le e343
   */
  @FieldXPath("E34.3")
  String getE343();

  /**
   * Set le e343.
   *
   * @param E343
   *          le nouveau e343
   */
  void setE343(String E343);

  /**
   * Get le e344.
   *
   * @return le e344
   */
  @FieldXPath("E34.4")
  XmlString[] getE344();

  /**
   * Ajoute le to e344.
   *
   * @return le xml string
   */
  XmlString addToE344();

  /**
   * Set le e344.
   *
   * @param P013
   *          le nouveau e344
   */
  void setE344(XmlString[] P013);

  /**
   * Get le e347.
   *
   * @return le e347
   */
  @FieldXPath("E34.7")
  String getE347();

  /**
   * Set le e347.
   *
   * @param E347
   *          le nouveau e347
   */
  void setE347(String E347);

}
