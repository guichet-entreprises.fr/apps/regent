package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IA31.
 */
@ResourceXPath("A31")
public interface IA31 {

  /**
   * Get le A311.
   *
   * @return le A311
   */
  @FieldXPath("A31.1")
  String getA311();

  /**
   * Set le A311.
   *
   * @param A311
   *          le nouveau A311
   */
  void setA311(String A311);

  /**
   * Get le A312.
   *
   * @return le A312
   */
  @FieldXPath("A31.2")
  String getA312();

  /**
   * Set le A312.
   *
   * @param A312
   *          le nouveau A312
   */
  void setA312(String A312);

}
