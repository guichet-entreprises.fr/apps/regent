package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IA42.
 */
@ResourceXPath("/A42")
public interface IA42 {

  /**
   * Get le a411.
   *
   * @return le a411
   */
  @FieldXPath("A42.1")
  String getA421();

  /**
   * Set le a411.
   *
   * @param A411
   *          le nouveau a411
   */
  void setA421(String A421);

  /**
   * Get le a422.
   *
   * @return le a422
   */
  @FieldXPath("A42.2")
  String getA422();

  /**
   * Set le a422.
   *
   * @param A422
   *          le nouveau a422
   */
  void setA422(String A422);
}
