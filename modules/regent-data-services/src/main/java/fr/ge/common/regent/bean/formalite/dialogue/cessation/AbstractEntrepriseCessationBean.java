package fr.ge.common.regent.bean.formalite.dialogue.cessation;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import fr.ge.common.regent.bean.formalite.FormaliteVueBean;
import fr.ge.common.regent.bean.formalite.dialogue.IFormalite;

/**
 * Le Class AbstractEntrepriseCessationBean.
 *
 * @param <T>
 *            le type generique
 * @param <D>
 *            le type generique
 */
public abstract class AbstractEntrepriseCessationBean<T extends EtablissementCessationBean, D extends DirigeantCessationBean> extends FormaliteVueBean implements IFormalite {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -4533551972596379593L;

    /** Le etablissements. */
    private List<T> etablissements;

    /** Le dirigeants. */
    private List<D> dirigeants;

    /**
     * Instancie un nouveau abstract entreprise cessation bean.
     */
    public AbstractEntrepriseCessationBean() {
        super();
        if (CollectionUtils.isEmpty(etablissements)) {
            etablissements = new ArrayList<T>();
        }

        if (CollectionUtils.isEmpty(dirigeants)) {
            dirigeants = new ArrayList<D>();
        }
    }

    /**
     * Get le etablissements.
     *
     * @return le etablissements
     */
    public List<T> getEtablissements() {
        return this.etablissements;
    }

    /**
     * Set le etablissements.
     *
     * @param etablissements
     *            le nouveau etablissements
     */
    public void setEtablissements(List<T> etablissements) {
        this.etablissements = etablissements;
    }

    /**
     * Get le dirigeants.
     *
     * @return le dirigeants
     */
    public List<D> getDirigeants() {
        return this.dirigeants;
    }

    /**
     * Set le dirigeants.
     *
     * @param dirigeants
     *            le nouveau dirigeants
     */
    public void setDirigeants(List<D> dirigeants) {
        this.dirigeants = dirigeants;
    }

}
