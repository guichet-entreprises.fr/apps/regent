package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface SNS.
 */
@ResourceXPath("/SNS")
public interface ISNS {

  /**
   * Get le A21.
   *
   * @return le A21
   */
  @FieldXPath("A21")
  IA21[] getA21();

  /**
   * Ajoute le to A21.
   *
   * @return le A21
   */
  IA21 addToA21();

  /**
   * Set le A21.
   *
   * @param A21
   *          le nouveau A21
   */
  void setA21(IA21[] A21);

  /**
   * Get le A22.
   *
   * @return le A22
   */
  @FieldXPath("A22")
  IA22[] getA22();

  /**
   * Ajoute le to A22.
   *
   * @return le A22
   */
  IA22 addToA22();

  /**
   * Set le A22.
   *
   * @param A22
   *          le nouveau A22
   */
  void setA22(IA22[] A22);

  /**
   * Get le A23.
   *
   * @return le A23
   */
  @FieldXPath("A23")
  String getA23();

  /**
   * Set le A23.
   *
   * @param A23
   *          le nouveau A23
   */
  void setA23(String A23);

  /**
   * Get le A24.
   *
   * @return le A24
   */
  @FieldXPath("A24")
  IA24[] getA24();

  /**
   * Ajoute le to A24.
   *
   * @return le A24
   */
  IA24 addToA24();

  /**
   * Set le A24.
   *
   * @param A24
   *          le nouveau A24
   */
  void setA24(IA24[] A24);

  /**
   * Get le A27.
   *
   * @return le A27
   */
  @FieldXPath("A27")
  String getA27();

  /**
   * Set le A27.
   *
   * @param A27
   *          le nouveau A27
   */
  void setA27(String A27);

  /**
   * Get le A28.
   *
   * @return le A28
   */
  @FieldXPath("A28")
  String getA28();

  /**
   * Set le A28.
   *
   * @param A28
   *          le nouveau A28
   */
  void setA28(String A28);

  /**
   * Get le A29.
   *
   * @return le A29
   */
  @FieldXPath("A29")
  String getA29();

  /**
   * Set le A29.
   *
   * @param A29
   *          le nouveau A29
   */
  void setA29(String A29);
}
