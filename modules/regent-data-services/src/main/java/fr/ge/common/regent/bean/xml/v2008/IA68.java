/**
 * 
 */
package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.core.types.XmlString;

/**
 * Interface IA68.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
public interface IA68 {

  /**
   * Accesseur sur l'attribut {@link #a682}.
   *
   * @return a682
   */
  @FieldXPath("A68.2")
  String getA682();

  /**
   * Mutateur sur l'attribut {@link #a682}.
   *
   * @param A682
   *          la nouvelle valeur de l'attribut a682
   */
  void setA682(String A682);

  /**
   * Accesseur sur l'attribut {@link #a683}.
   *
   * @return a683
   */
  @FieldXPath("A68.3")
  XmlString[] getA683();

  /**
   * Mutateur sur l'attribut {@link #a683}.
   *
   * @param A683
   *          la nouvelle valeur de l'attribut a683
   */
  void setA683(XmlString[] A683);

  /**
   * Ajoute le to A683.
   *
   * @return le xml string
   */
  XmlString addToA683();

  /**
   * Accesseur sur l'attribut {@link #a684}.
   *
   * @return a684
   */
  @FieldXPath("A68.4")
  String getA684();

  /**
   * Mutateur sur l'attribut {@link #a684}.
   *
   * @param A684
   *          la nouvelle valeur de l'attribut a684
   */
  void setA684(String A684);
}
