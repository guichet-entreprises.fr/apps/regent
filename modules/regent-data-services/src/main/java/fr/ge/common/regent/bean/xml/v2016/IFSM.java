package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * L'interface FSM.
 */
@ResourceXPath("/FSM")
public interface IFSM {

  /**
   * Get le m54.
   *
   * @return le m54
   */
  @FieldXPath("M54")
  String getM54();

  /**
   * Set le m54.
   *
   * @param m54
   *          le nouveau m54
   */
  void setM54(String m54);

  /**
   * Get le M55.
   *
   * @return le M55
   */
  @FieldXPath("M55")
  IM55[] getM55();

  /**
   * Ajoute le to M55.
   *
   * @return le M55
   */
  IM55 addToM55();

  /**
   * Set le m55.
   *
   * @param m55
   *          le m55
   */
  void setM55(IM55[] m55);

  /**
   * Get le m54.
   *
   * @return le m54
   */
  @FieldXPath("M56")
  String getM56();

  /**
   * Set le m56.
   *
   * @param m56
   *          le nouveau m56
   */
  void setM56(String m56);
}
