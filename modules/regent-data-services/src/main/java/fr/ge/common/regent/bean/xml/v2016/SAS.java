package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface CAS.
 */
@ResourceXPath("/SAS")
public interface SAS {

  /**
   * Accesseur sur l'attribut {@link #a68}.
   *
   * @return a68
   */
  @FieldXPath("A68")
  IA68[] getA68();

  /**
   * Adds the to a68.
   *
   * @return i a68
   */
  IA68 addToA68();

  /**
   * Mutateur sur l'attribut {@link #a68}.
   *
   * @param A68
   *          la nouvelle valeur de l'attribut a68
   */
  void setA68(IA68[] A68);

  /**
   * Accesseur sur l'attribut {@link #a69}.
   *
   * @return a69
   */
  @FieldXPath("A69")
  IA69[] getA69();

  /**
   * Ajoute le to A69.
   *
   * @return le i A69
   */
  IA69 addToA69();

  /**
   * Set le A69.
   *
   * @param A69
   *          le nouveau A69
   */
  void setA69(IA69[] A69);

  /**
   * Get le A70.
   *
   * @return le A70
   */
  @FieldXPath("A70")
  String getA70();

  /**
   * Set le A70.
   *
   * @param A70
   *          le nouveau A70
   */
  void setA70(String A70);

}
