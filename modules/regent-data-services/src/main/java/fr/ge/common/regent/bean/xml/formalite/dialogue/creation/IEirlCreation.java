package fr.ge.common.regent.bean.xml.formalite.dialogue.creation;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;
import org.xmlfield.core.types.XmlString;

import fr.ge.common.regent.bean.xml.formalite.IFormalite;

/**
 * Le Interface de l'entité Entreprise individuelle à résponsabilité limitée
 * 
 * 
 * 
 */
@ResourceXPath("/formalite")
public interface IEirlCreation extends IFormalite {

    /**
     * Getter de l'attribut activitesAccessoiresBICBNC.
     * 
     * @return la valeur de activitesAccessoiresBICBNC
     */
    @FieldXPath(value = "activitesAccessoiresBICBNC")
    String getActivitesAccessoiresBICBNC();

    /**
     * Getter de l'attribut dateClotureExerciceComptable.
     * 
     * @return la valeur de dateClotureExerciceComptable
     */
    @FieldXPath(value = "dateClotureExerciceComptable")
    String getDateClotureExerciceComptable();

    /**
     * Getter de l'attribut denomination.
     * 
     * @return la valeur de denomination
     */
    @FieldXPath(value = "denomination")
    String getDenomination();

    /**
     * Getter de l'attribut objet.
     * 
     * @return la valeur de objet
     */
    @FieldXPath(value = "objet")
    String getObjet();

    /**
     * Getter de l'attribut objetPortantSurEnsembleActivites.
     * 
     * @return la valeur de objetPortantSurEnsembleActivites
     */
    @FieldXPath(value = "objetPortantSurEnsembleActivites")
    String getObjetPortantSurEnsembleActivites();

    /**
     * Getter de l'attribut precedentEIRLDenomination.
     * 
     * @return la valeur de precedentEIRLDenomination
     */
    @FieldXPath(value = "precedentEIRLDenomination")
    String getPrecedentEIRLDenomination();

    /**
     * Getter de l'attribut precedentEIRLLieuImmatriculation.
     * 
     * @return la valeur de precedentEIRLLieuImmatriculation
     */
    @FieldXPath(value = "precedentEIRLLieuImmatriculation")
    String getPrecedentEIRLLieuImmatriculation();

    /**
     * Getter de l'attribut precedentEIRLRegistre.
     * 
     * @return la valeur de precedentEIRLRegistre
     */
    @FieldXPath(value = "precedentEIRLRegistre")
    String getPrecedentEIRLRegistre();

    /**
     * Getter de l'attribut precedentEIRLSIREN.
     * 
     * @return la valeur de precedentEIRLSIREN
     */
    @FieldXPath(value = "precedentEIRLSIREN")
    String getPrecedentEIRLSIREN();

    /**
     * /** Getter de l'attribut registreDepot.
     * 
     * @return la valeur de registreDepot
     */
    @FieldXPath(value = "registreDepot")
    String getRegistreDepot();

    /**
     * Getter de l'attribut statutEIRL.
     * 
     * @return la valeur de statutEIRL
     */
    @FieldXPath(value = "statutEIRL")
    String getStatutEIRL();

    /**
     * Setter de l'attribut activitesAccessoiresBICBNC.
     * 
     * @param activitesAccessoiresBICBNC
     *            la nouvelle valeur de activitesAccessoiresBICBNC
     */
    void setActivitesAccessoiresBICBNC(String activitesAccessoiresBICBNC);

    /**
     * Setter de l'attribut dateClotureExerciceComptable.
     * 
     * @param dateClotureExerciceComptable
     *            la nouvelle valeur de dateClotureExerciceComptable
     */
    void setDateClotureExerciceComptable(String dateClotureExerciceComptable);

    /**
     * Setter de l'attribut denomination.
     * 
     * @param denomination
     *            la nouvelle valeur de denomination
     */
    void setDenomination(String denomination);

    /**
     * Setter de l'attribut objet.
     * 
     * @param objet
     *            la nouvelle valeur de objet
     */
    void setObjet(String objet);

    /**
     * Setter de l'attribut objetPortantSurEnsembleActivites.
     * 
     * @param objetPortantSurEnsembleActivites
     *            la nouvelle valeur de objetPortantSurEnsembleActivites
     */
    void setObjetPortantSurEnsembleActivites(String objetPortantSurEnsembleActivites);

    /**
     * Setter de l'attribut precedentEIRLDenomination.
     * 
     * @param precedentEIRLDenomination
     *            la nouvelle valeur de precedentEIRLDenomination
     */
    void setPrecedentEIRLDenomination(String precedentEIRLDenomination);

    /**
     * Setter de l'attribut precedentEIRLLieuImmatriculation.
     * 
     * @param precedentEIRLLieuImmatriculation
     *            la nouvelle valeur de precedentEIRLLieuImmatriculation
     */
    void setPrecedentEIRLLieuImmatriculation(String precedentEIRLLieuImmatriculation);

    /**
     * Setter de l'attribut precedentEIRLRegistre.
     * 
     * @param precedentEIRLRegistre
     *            la nouvelle valeur de precedentEIRLRegistre
     */
    void setPrecedentEIRLRegistre(String precedentEIRLRegistre);

    /**
     * Setter de l'attribut precedentEIRLSIREN.
     * 
     * @param precedentEIRLSIREN
     *            la nouvelle valeur de precedentEIRLSIREN
     */
    void setPrecedentEIRLSIREN(String precedentEIRLSIREN);

    /**
     * Setter de l'attribut registreDepot.
     * 
     * @param registreDepot
     *            la nouvelle valeur de registreDepot
     */
    void setRegistreDepot(String registreDepot);

    /**
     * Setter de l'attribut statutEIRL.
     * 
     * @param statutEIRL
     *            la nouvelle valeur de statutEIRL
     */
    void setStatutEIRL(String statutEIRL);

    /**
     * Get le definition patrimoine.
     *
     * @return le definition patrimoine
     */

    @FieldXPath(value = "definitionPatrimoines/definitionPatrimoine")
    XmlString[] getDefinitionPatrimoine();

    /**
     * Set le definitionPatrimoine .
     * 
     * @param definitionPatrimoine
     *            le nouveau definitionPatrimoine
     */
    void setDefinitionPatrimoine(XmlString[] definitionPatrimoine);

    /**
     * Retire une DefinitionPatrimoine.
     * 
     * @param definitionPatrimoine
     *            la definitionPatrimoine à retirer
     */
    void removeFromDefinitionPatrimoine(XmlString definitionPatrimoine);

    /**
     * Ajoute le to DefinitionPatrimoine DefinitionPatrimoine.
     * 
     * @return le xml string
     */
    XmlString addToDefinitionPatrimoine();

}
