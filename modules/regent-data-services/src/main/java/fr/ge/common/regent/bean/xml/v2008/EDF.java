package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface EDF.
 */
@ResourceXPath("/EDF")
public interface EDF {

  /**
   * Get le c10.
   *
   * @return le c10
   */
  @FieldXPath("C10")
  IC10[] getC10();

  /**
   * Ajoute le to c10.
   *
   * @return le i c10
   */
  IC10 addToC10();

  /**
   * Set le c10.
   *
   * @param C10
   *          le nouveau c10
   */
  void setC10(IC10[] C10);

  /**
   * Get le c11.
   *
   * @return le c11
   */
  @FieldXPath("C11")
  String getC11();

  /**
   * Set le c11.
   *
   * @param C11
   *          le nouveau c11
   */
  void setC11(String C11);
}
