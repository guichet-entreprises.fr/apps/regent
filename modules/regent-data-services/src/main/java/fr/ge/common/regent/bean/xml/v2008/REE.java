package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface REE.
 */
@ResourceXPath("/REE")
public interface REE {

  /**
   * Get le e51.
   *
   * @return le e51
   */
  @FieldXPath("E51")
  String getE51();

  /**
   * Set le e51.
   *
   * @param E51
   *          le nouveau e51
   */
  void setE51(String E51);

  /**
   * Get le e52.
   *
   * @return le e52
   */
  @FieldXPath("E52")
  String getE52();

  /**
   * Set le e52.
   *
   * @param E52
   *          le nouveau e52
   */
  void setE52(String E52);

}
