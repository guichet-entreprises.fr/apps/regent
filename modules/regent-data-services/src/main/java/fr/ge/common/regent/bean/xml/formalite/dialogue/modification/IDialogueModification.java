package fr.ge.common.regent.bean.xml.formalite.dialogue.modification;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

import fr.ge.common.regent.bean.xml.formalite.IFormalite;
import fr.ge.common.regent.bean.xml.transverse.IAdresse;

/**
 * La classe XML Field de Formalité Régularisation.
 * 
 * @author roveda
 * 
 */
@ResourceXPath("/formalite")
public interface IDialogueModification extends IFormalite {

    /** La constante MODEL_VERSION. */
    static final int MODEL_VERSION = 1;

    /**
     * Get le observations.
     *
     * @return le observations
     */
    @FieldXPath(value = "observations")
    String getObservations();

    /**
     * Get le correspondance destinataire.
     *
     * @return le correspondance destinataire
     */
    @FieldXPath(value = "correspondanceDestinataire")
    String getCorrespondanceDestinataire();

    /**
     * Get le correspondance adresse.
     *
     * @return le correspondance adresse
     */
    @FieldXPath(value = "correspondanceAdresse")
    IAdresse getCorrespondanceAdresse();

    /**
     * New correspondance adresse.
     *
     * @return le i adresse
     */
    IAdresse newCorrespondanceAdresse();

    /**
     * Get le telephone1.
     *
     * @return le telephone1
     */
    @FieldXPath(value = "telephone1")
    String getTelephone1();

    /**
     * Get le telephone2.
     *
     * @return le telephone2
     */
    @FieldXPath(value = "telephone2")
    String getTelephone2();

    /**
     * Get le fax.
     *
     * @return le fax
     */
    @FieldXPath(value = "fax")
    String getFax();

    /**
     * {@inheritDoc}
     */
    @Override
    @FieldXPath(value = "courriel")
    String getCourriel();

    /**
     * Get le signataire qualite.
     *
     * @return le signataire qualite
     */
    @FieldXPath(value = "signataireQualite")
    String getSignataireQualite();

    /**
     * Get le signataire nom.
     *
     * @return le signataire nom
     */
    @FieldXPath(value = "signataireNom")
    String getSignataireNom();

    /**
     * Get le adresse signature.
     *
     * @return le adresse signature
     */
    @FieldXPath(value = "adresseSignature")
    IAdresse getAdresseSignature();

    /**
     * New adresse signature.
     *
     * @return le i adresse
     */
    IAdresse newAdresseSignature();

    /**
     * Get le non Diffusion Information.
     *
     * @return le nonDiffusionInformation
     */
    @FieldXPath(value = "nonDiffusionInformation")
    String getNonDiffusionInformation();

    /**
     * Get le signature.
     *
     * @return le signature
     */
    @FieldXPath(value = "signature")
    String getSignature();

    /**
     * Get le signature lieu.
     *
     * @return le signature lieu
     */
    @FieldXPath(value = "signatureLieu")
    String getSignatureLieu();

    /**
     * Get le signature date.
     *
     * @return le signature date
     */
    @FieldXPath(value = "signatureDate")
    String getSignatureDate();

    /**
     * Get le resau cfe.
     *
     * @return le resau cfe
     */
    @FieldXPath(value = "resauCFE")
    String getResauCFE();

    /**
     * Set le resau cfe.
     *
     * @param resauCFE
     *            le nouveau resau cfe
     */
    void setResauCFE(String resauCFE);

    /**
     * Get le entreprise.
     *
     * @return le entreprise
     */
    @FieldXPath(value = "entreprise")
    IEntrepriseModification getEntreprise();

    /**
     * Set le entreprise.
     *
     * @param iEntrepriseModification
     *            le nouveau entreprise
     */
    void setEntreprise(IEntrepriseModification iEntrepriseModification);

    /**
     * New entreprise.
     *
     * @return le i entreprise modification
     */
    IEntrepriseModification newEntreprise();

    /**
     * Set le adresse signature.
     *
     * @param adresseSignature
     *            le nouveau adresse signature
     */
    void setAdresseSignature(IAdresse adresseSignature);

    /**
     * Set le correspondance adresse.
     *
     * @param correspondanceAdresse
     *            le nouveau correspondance adresse
     */
    void setCorrespondanceAdresse(IAdresse correspondanceAdresse);

    /**
     * Set le observations.
     *
     * @param observations
     *            le nouveau observations
     */
    void setObservations(String observations);

    /**
     * Set le correspondance destinataire.
     *
     * @param correspondanceDestinataire
     *            le nouveau correspondance destinataire
     */
    void setCorrespondanceDestinataire(String correspondanceDestinataire);

    /**
     * Set le telephone1.
     *
     * @param telephone1
     *            le nouveau telephone1
     */
    void setTelephone1(String telephone1);

    /**
     * Set le telephone2.
     *
     * @param telephone2
     *            le nouveau telephone2
     */
    void setTelephone2(String telephone2);

    /**
     * Set le fax.
     *
     * @param fax
     *            le nouveau fax
     */
    void setFax(String fax);

    /**
     * {@inheritDoc}
     */
    @Override
    void setCourriel(String courriel);

    /**
     * Set le signataire qualite.
     *
     * @param signataireQualite
     *            le nouveau signataire qualite
     */
    void setSignataireQualite(String signataireQualite);

    /**
     * Set le signataire nom.
     *
     * @param signataireNom
     *            le nouveau signataire nom
     */
    void setSignataireNom(String signataireNom);

    /**
     * Set le non Diffusion Information.
     *
     * @param nonDiffusionInformation
     *            le nouveau nonDiffusionInformation
     */
    void setNonDiffusionInformation(String nonDiffusionInformation);

    /**
     * Set le signature.
     *
     * @param signature
     *            le nouveau signature
     */
    void setSignature(String signature);

    /**
     * Set le signature lieu.
     *
     * @param signatureLieu
     *            le nouveau signature lieu
     */
    void setSignatureLieu(String signatureLieu);

    /**
     * Set le signature date.
     *
     * @param signatureDate
     *            le nouveau signature date
     */
    void setSignatureDate(String signatureDate);

}
