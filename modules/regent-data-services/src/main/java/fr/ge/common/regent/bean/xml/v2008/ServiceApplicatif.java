package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface ServiceApplicatif.
 */
@ResourceXPath("/ServiceApplicatif")
public interface ServiceApplicatif {

  /**
   * Get le liasse.
   *
   * @return le liasse
   */
  @FieldXPath("Liasse")
  Liasse[] getLiasse();

  /**
   * Ajoute le to liasse.
   *
   * @return le liasse
   */
  Liasse addToLiasse();

  /**
   * Set le liasse.
   *
   * @param Liasse
   *          le nouveau liasse
   */
  void setLiasse(Liasse[] Liasse);

  /**
   * Get le specification.
   *
   * @return le specification
   */
  @FieldXPath("Specification")
  Specification[] getSpecification();

  /**
   * Ajoute le to specification.
   *
   * @return le specification
   */
  Specification addToSpecification();

  /**
   * Set le specification.
   *
   * @param Specification
   *          le nouveau specification
   */
  void setSpecification(Specification[] Specification);

}
