package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IXmlGlobal.
 */
@ResourceXPath("/REGENT-XML")
public interface IXmlGlobal2016 {

  /**
   * Get le emetteur.
   *
   * @return le emetteur
   */
  @FieldXPath("Emetteur")
  String getEmetteur();

  /**
   * Set le emetteur.
   *
   * @param Emetteur
   *          le nouveau emetteur
   */
  void setEmetteur(String Emetteur);

  /**
   * Get le destinataire.
   *
   * @return le destinataire
   */
  @FieldXPath("Destinataire")
  String getDestinataire();

  /**
   * Set le destinataire.
   *
   * @param Destinataire
   *          le nouveau destinataire
   */
  void setDestinataire(String Destinataire);

  /**
   * Get le date heure emission.
   *
   * @return le date heure emission
   */
  @FieldXPath("DateHeureEmission")
  String getDateHeureEmission();

  /**
   * Set le date heure emission.
   *
   * @param DateHeureEmission
   *          le nouveau date heure emission
   */
  void setDateHeureEmission(String DateHeureEmission);

  /**
   * Get le version message.
   *
   * @return le version message
   */
  @FieldXPath("VersionMessage")
  String getVersionMessage();

  /**
   * Set le version message.
   *
   * @param VersionMessage
   *          le nouveau version message
   */
  void setVersionMessage(String VersionMessage);

  /**
   * Get le version norme.
   *
   * @return le version norme
   */
  @FieldXPath("VersionNorme")
  String getVersionNorme();

  /**
   * Set le version norme.
   *
   * @param VersionNorme
   *          le nouveau version norme
   */
  void setVersionNorme(String VersionNorme);

  /**
   * Get le nom service.
   *
   * @return le nom service
   */
  @FieldXPath("ServiceApplicatif/Specification/NomService")
  String getNomService();

  /**
   * Set le nom service.
   *
   * @param NomService
   *          le nouveau nom service
   */
  void setNomService(String NomService);

  /**
   * Get le version service.
   *
   * @return le version service
   */
  @FieldXPath("ServiceApplicatif/Specification/VersionService")
  String getVersionService();

  /**
   * Set le version service.
   *
   * @param VersionService
   *          le nouveau version service
   */
  void setVersionService(String VersionService);

  /**
   * Get le service applicatif.
   *
   * @return le service applicatif
   */
  @FieldXPath("ServiceApplicatif")
  ServiceApplicatif[] getServiceApplicatif();

  /**
   * Ajoute le to service applicatif.
   *
   * @return le service applicatif
   */
  ServiceApplicatif addToServiceApplicatif();

  /**
   * Set le service applicatif.
   *
   * @param ServiceApplicatif
   *          le nouveau service applicatif
   */
  void setServiceApplicatif(ServiceApplicatif[] ServiceApplicatif);

}
