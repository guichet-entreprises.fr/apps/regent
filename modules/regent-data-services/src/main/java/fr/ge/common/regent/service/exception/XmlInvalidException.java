package fr.ge.common.regent.service.exception;

/**
 * Exception signifiant que le XML contient des erreurs non fatales.
 * 
 */
public class XmlInvalidException extends AbstractXmlException {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 6222628227334733739L;

    /**
     * Constructeur.
     * 
     * @param message
     *            message d'erreur
     */
    public XmlInvalidException(String message) {
        super(message);
    }

}
