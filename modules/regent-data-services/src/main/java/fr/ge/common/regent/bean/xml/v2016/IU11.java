package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IU11.
 */
@ResourceXPath("/U11")
public interface IU11 {

  /**
   * Get le u113.
   *
   * @return le u113
   */
  @FieldXPath("U11.3")
  String getU113();

  /**
   * Set le u113.
   *
   * @param U113
   *          le nouveau u113
   */
  void setU113(String U113);

  /**
   * Get le u115.
   *
   * @return le u115
   */
  @FieldXPath("U11.5")
  String getU115();

  /**
   * Set le u115.
   *
   * @param U115
   *          le nouveau u115
   */
  void setU115(String U115);

  /**
   * Get le u116.
   *
   * @return le u116
   */
  @FieldXPath("U11.6")
  String getU116();

  /**
   * Set le u116.
   *
   * @param U116
   *          le nouveau u116
   */
  void setU116(String U116);

  /**
   * Get le u117.
   *
   * @return le u117
   */
  @FieldXPath("U11.7")
  String getU117();

  /**
   * Set le u117.
   *
   * @param U117
   *          le nouveau u117
   */
  void setU117(String U117);

  /**
   * Get le u118.
   *
   * @return le u118
   */
  @FieldXPath("U11.8")
  String getU118();

  /**
   * Set le u118.
   *
   * @param U118
   *          le nouveau u118
   */
  void setU118(String U118);

  /**
   * Get le u1110.
   *
   * @return le u1110
   */
  @FieldXPath("U11.10")
  String getU1110();

  /**
   * Set le u1110.
   *
   * @param U1110
   *          le nouveau u1110
   */
  void setU1110(String U1110);

  /**
   * Get le u1111.
   *
   * @return le u1111
   */
  @FieldXPath("U11.11")
  String getU1111();

  /**
   * Set le u1111.
   *
   * @param U1111
   *          le nouveau u1111
   */
  void setU1111(String U1111);

  /**
   * Get le u1112.
   *
   * @return le u1112
   */
  @FieldXPath("U11.12")
  String getU1112();

  /**
   * Set le u1112.
   *
   * @param U1112
   *          le nouveau u1112
   */
  void setU1112(String U1112);

  /**
   * Get le u1113.
   *
   * @return le u1113
   */
  @FieldXPath("U11.13")
  String getU1113();

  /**
   * Set le u1113.
   *
   * @param U1113
   *          le nouveau u1113
   */
  void setU1113(String U1113);

  /**
   * Get le u1114.
   *
   * @return le u1114
   */
  @FieldXPath("U11.14")
  String getU1114();

  /**
   * Set le u1114.
   *
   * @param U1114
   *          le nouveau u1114
   */
  void setU1114(String U1114);

}
