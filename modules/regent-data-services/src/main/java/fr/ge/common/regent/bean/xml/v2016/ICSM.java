package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * L'interface CSM.
 */
@ResourceXPath("/CSM")
public interface ICSM {

  /**
   * Get le M24.
   *
   * @return le M24
   */
  @FieldXPath("M24")
  IM24[] getM24();

  /**
   * Ajoute le to M24.
   *
   * @return le M24
   */
  IM24 addToM24();

  /**
   * Set le m24.
   *
   * @param m24
   *          le m24
   */
  void set(IM24[] m24);

  /**
   * Get le m25.
   *
   * @return le m25
   */
  @FieldXPath("M25")
  String getM25();

  /**
   * Set le m25.
   *
   * @param m25
   *          le nouveau m25
   */
  void setM25(String m25);

  /**
   * Get le m26.
   *
   * @return le m26
   */
  @FieldXPath("M26")
  String getM26();

  /**
   * Set le m26.
   *
   * @param m26
   *          le nouveau m26
   */
  void setM26(String m26);

  /**
   * Get le m28.
   *
   * @return le m28
   */
  @FieldXPath("M28")
  String getM28();

  /**
   * Set le m28.
   *
   * @param m28
   *          le nouveau m28
   */
  void setM28(String m28);
}
