package fr.ge.common.regent.context.function;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmlfield.core.exception.XmlFieldParsingException;

import fr.ge.common.regent.bean.formalite.dialogue.AbstractFormalite;
import fr.ge.common.regent.bean.formalite.dialogue.InsaisissabiliteBean;
import fr.ge.common.regent.bean.formalite.dialogue.cessation.DialogueCessationBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.ConjointBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.DialogueCreationBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.DirigeantBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.RegimeFiscalBean;
import fr.ge.common.regent.bean.formalite.dialogue.modification.DialogueModificationBean;
import fr.ge.common.regent.bean.formalite.dialogue.regularisation.DialogueRegularisationBean;
import fr.ge.common.regent.bean.formalite.profil.creation.ProfilCreationBean;
import fr.ge.common.regent.bean.xml.v2008.IXmlGlobal2008;
import fr.ge.common.regent.bean.xml.v2016.IXmlGlobal2016;
import fr.ge.common.regent.constante.TypeDossierEnum;
import fr.ge.common.regent.constante.VersionXmlRegentEnum;
import fr.ge.common.regent.xml.util.StringTransformerUtil;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * Le Class FonctionRegent.
 *
 * @author bsadil
 */
public class FonctionRegent {

    /** La constante OUI. */
    private static final String OUI = "oui";

    /** La constante SEPARATEUR_C43. */
    private static final String SEPARATEUR_C43 = "!";

    /** Réseau CFE Urssaf. */
    private static final String RESEAU_CFE_URSSAF = "U";

    /** Le logger fonctionnel. */
    private static final Logger LOGGER_FONC = LoggerFactory.getLogger(FonctionRegent.class);

    /**
     * Contient les modifications des type de liasses pour le XML Regent (balise
     * C05) <br>
     * Key : le <br>
     * Value : type de liasse modifié.
     */
    private static Map<String, String> mapCorrespondanceLiasse = new HashMap<String, String>();

    /**
     * Contient les modifications des type de liasses pour le XML Regent (balise
     * C05) <br>
     * Key : le libelle<br>
     * Value : la valeur du libelle.
     */
    private static Map<String, String> mapCorrespondanceLibelle = new HashMap<String, String>();

    /**
     * Contient la liste des réseaux pour lequel la balise C43 contient le
     * ME=1!#ge.fr#!Observation.
     */
    private static List<String> listCorrespondanceLiasseC43 = new ArrayList<String>();

    /** Le list telephone. */
    private static List<String> listTelephone = null;

    /** Le list prenom. */
    private static List<String> listPrenom = null;

    /** La constante OBS_C43. pour une dossier de type Régularisation */
    private static final String OBS_C43_REGUL = "ME=R!";

    /** La constante OBS_C43 pour une dossier de type modification. */
    private static final String OBS_C43_MODIF = "ME=O!";

    /**
     * La constante OBS_C43 pour un dossier de type Création événement 01P et
     * 05P.
     **/
    private static final String OBS_C43_CREATION_01P_05P = "ME=I!";

    /** La constante OBS_TAILLE_MAX. */
    private static final int OBS_TAILLE_MAX = 300;

    /** La constante PRENOM_CONJOINT. */
    private static final String PRENOM_CONJOINT = "conjoint";

    /** La constante PRENOM_DIRIGEANT. */
    private static final String PRENOM_DIRIGEANT = "dirigeant";

    /** La constante MAP_VERSION_XMLREGENT. */
    public static final Map<String, Class> MAP_VERSION_XMLREGENT = createMap();

    /**
     * Instancie un nouveau fonction regent.
     */
    public FonctionRegent() {
    }

    /**
     * Instancie un nouveau fonction regent.
     * 
     * @param correspondanceLiasseReseau
     *            le correspondance liasse reseau
     * @param correspondanceLiasseC43
     *            le correspondance liasse c43
     */
    public FonctionRegent(final String correspondanceLiasseReseau, final String correspondanceLiasseC43) {
        super();
        FonctionRegent.mapCorrespondanceLiasse = StringTransformerUtil.stringToMap(correspondanceLiasseReseau, true, true);
        FonctionRegent.listCorrespondanceLiasseC43 = StringTransformerUtil.stringToList(correspondanceLiasseC43, true);
    }

    /**
     * Cree le map.
     * 
     * @return le map
     */
    private static Map<String, Class> createMap() {
        Map<String, Class> result = new HashMap<String, Class>();
        result.put(VersionXmlRegentEnum.V2008_11.getVersion(), IXmlGlobal2008.class);
        result.put(VersionXmlRegentEnum.V2016_02.getVersion(), IXmlGlobal2016.class);
        return Collections.unmodifiableMap(result);
    }

    /**
     * Get le date du jour.
     * 
     * @param formatDate
     *            le format date
     * @return la date du jour pour le génération de l'xml regent
     */
    public static String getDateDuJour(final String formatDate) {
        Date date = new Date();
        // format date spécifique pour l'xmlRegent
        SimpleDateFormat sdf = new SimpleDateFormat(formatDate);

        return sdf.format(date).toString();
    }

    /**
     * Format MoisJour.
     * 
     * @param dateInString
     *            le date in string
     * @return formatJourMois pour l'xmlRegent qui prend en entrée String exp
     *         String Date =13/04
     */
    public static String formatJourMois(final String dateInString) {
        if (StringUtils.isNotEmpty(dateInString)) {
            SimpleDateFormat fromUser = new SimpleDateFormat("dd/MM");
            SimpleDateFormat myFormat = new SimpleDateFormat("--MM-dd");

            String reformattedStr = null;
            try {

                reformattedStr = myFormat.format(fromUser.parse(dateInString));

            } catch (ParseException e) {
                LOGGER_FONC.error(e.getMessage(), e);
            }

            return reformattedStr;
        } else {
            return null;
        }
    }

    /**
     * Format date.
     * 
     * @param dateInString
     *            le date in string
     * @return formatDate pour l'xmlRegent qui prend en entrée String exp String
     *         Date =13/04/2015
     */
    public static String formatDate(final String dateInString) {
        if (StringUtils.isNotEmpty(dateInString)) {
            SimpleDateFormat fromUser = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");

            String reformattedStr = null;
            try {
                reformattedStr = myFormat.format(fromUser.parse(dateInString));

            } catch (ParseException e) {
                LOGGER_FONC.error(e.getMessage(), e);
            }

            return reformattedStr;
        } else {
            return null;
        }
    }

    /**
     * Type de liasse.
     * 
     * @param reseauCfe
     *            le reseau cfe
     * @return la liasse en fonction du reseau CFE Pour la balise C05,
     *         Actuellement C = Commerce, M=Métier, L= Libéral Les liasses Y de
     *         la CCI, Z des CMA et X des Greffes ne devraient plus arriver. Il
     *         faut maintenant la correspondance ci-dessous pour cette cette
     *         balise : X=L Z=M Y=C
     */
    public static String typeDeLiasse(final String reseauCfe) {
        if (mapCorrespondanceLiasse.containsKey(reseauCfe)) {
            return mapCorrespondanceLiasse.get(reseauCfe);
        } else {
            return reseauCfe;
        }

    }

    /**
     * Get le list telephone.
     *
     * @param numeroFormalite
     *            le numero formalite
     * @return la liste des numéros de téléphone
     * @throws XmlFieldParsingException
     *             xml field parsing exception
     * @throws TechnicalException
     *             technique exception
     */
    public static List<String> getListTelephone(final String numeroFormalite) throws XmlFieldParsingException, TechnicalException {

        // DialogueCreationBean cfe = (DialogueCreationBean)
        // formaliteXMLService.getDialogueXML(DossierUtil.transformToNumeroDossier(numeroFormalite));
        // TODO Fonction à corriger
        DialogueCreationBean cfe = new DialogueCreationBean();
        listTelephone = new ArrayList<String>();

        if (StringUtils.isNotEmpty(cfe.getTelephone1())) {
            listTelephone.add(cfe.getTelephone1());
        }
        if (StringUtils.isNotEmpty(cfe.getTelephone2())) {
            listTelephone.add(cfe.getTelephone2());
        }
        return listTelephone;
    }

    /**
     * Get le list prenom.
     *
     * @param numeroFormalite
     *            le numero formalite
     * @param bean
     *            le bean
     * @return al liste des prenom de Conjoint ou du dirigeant
     * @throws XmlFieldParsingException
     *             xml field parsing exception
     * @throws TechnicalException
     *             technique exception
     */
    public static List<String> getListPrenom(final String numeroFormalite, final String bean) throws XmlFieldParsingException, TechnicalException {
        // DialogueCreationBean cfe = (DialogueCreationBean)
        // formaliteXMLService.getDialogueXML(DossierUtil.transformToNumeroDossier(numeroFormalite));
        DialogueCreationBean cfe = null;
        listPrenom = new ArrayList<String>();

        // -->SONAR
        List<String> prenoms = null;
        if (StringUtils.equals(bean, PRENOM_CONJOINT) && null != cfe.getEntreprise().getDirigeants() && null != cfe.getEntreprise().getDirigeants().get(0).getConjoint()) {
            ConjointBean conjoint = cfe.getEntreprise().getDirigeants().get(0).getConjoint();
            prenoms = getPrenomsDirigeantConjoint(conjoint);
        } else if (StringUtils.equals(bean, PRENOM_DIRIGEANT) && cfe.getEntreprise().getDirigeants() != null) {
            DirigeantBean dirigeantBean = cfe.getEntreprise().getDirigeants().get(0);
            prenoms = getPrenomsDirigeant(dirigeantBean);
        }

        if (null != prenoms && !prenoms.isEmpty()) {
            listPrenom.addAll(prenoms);
        }
        // <--

        return listPrenom;
    }

    /**
     * Retoune une liste de prénoms au niveau Dirigeant.
     *
     * @param dirigeantBean
     *            dirigeant bean
     * @return prenoms dirigeant
     */
    private static List<String> getPrenomsDirigeant(DirigeantBean dirigeantBean) {
        List<String> prenoms = new ArrayList<String>();

        if (StringUtils.isNotEmpty(dirigeantBean.getPpPrenom1())) {
            prenoms.add(dirigeantBean.getPpPrenom1());
        }

        if (StringUtils.isNotEmpty(dirigeantBean.getPpPrenom2())) {
            prenoms.add(dirigeantBean.getPpPrenom2());
        }

        if (StringUtils.isNotEmpty(dirigeantBean.getPpPrenom3())) {
            prenoms.add(dirigeantBean.getPpPrenom3());
        }

        if (StringUtils.isNotEmpty(dirigeantBean.getPpPrenom4())) {
            prenoms.add(dirigeantBean.getPpPrenom4());
        }

        return prenoms;
    }

    /**
     * Retoune une liste de prénoms au niveau Dirigeant-Conjoint.
     *
     * @param conjoint
     *            conjoint
     * @return prenoms dirigeant conjoint
     */
    private static List<String> getPrenomsDirigeantConjoint(ConjointBean conjoint) {
        List<String> prenoms = new ArrayList<String>();

        if (StringUtils.isNotEmpty(conjoint.getPpPrenom1())) {
            prenoms.add(conjoint.getPpPrenom1());
        }

        if (StringUtils.isNotEmpty(conjoint.getPpPrenom2())) {
            prenoms.add(conjoint.getPpPrenom2());
        }

        if (StringUtils.isNotEmpty(conjoint.getPpPrenom3())) {
            prenoms.add(conjoint.getPpPrenom3());
        }

        if (StringUtils.isNotEmpty(conjoint.getPpPrenom4())) {
            prenoms.add(conjoint.getPpPrenom4());
        }

        return prenoms;
    }

    /**
     * surcharge de la méthode getListValeurs qui sera utilisé pour les
     * rubriques avec occurences.
     * 
     * @param bean
     *            de type object
     * @param prefixAttribut
     *            Prefix de l'attribut à retourner
     * @return retours liste de string (exp: list des prenom de Conjoint ou du
     *         dirigeant)
     * @throws IllegalArgumentException
     *             le illegal argument exception
     * @throws IllegalAccessException
     *             le illegal access exception
     * @throws InvocationTargetException
     *             le invocation target exception
     */
    public static List<String> getListValeursOccurences(final Object bean, final String prefixAttribut) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {

        List<String> retours = new ArrayList<String>();
        if (bean == null) {
            return retours;
        } else {
            Method[] methodesBean = bean.getClass().getMethods();
            Pattern p = Pattern.compile("get" + StringUtils.capitalize(prefixAttribut));
            Matcher m = null;
            for (Method method : methodesBean) {
                m = p.matcher(method.getName());
                Object value = null;
                if (m.lookingAt()) {
                    value = method.invoke(bean);

                    if (value != null) {

                        retours.add(value.toString());
                    }
                }
            }
            return retours;
        }
    }

    /**
     * Accesseur sur l'attribut {@link #list method invoked}.
     *
     * @param bean
     *            bean
     * @param prefixAttribut
     *            prefix attribut
     * @return list method invoked
     * @throws IllegalArgumentException
     *             illegal argument exception
     * @throws IllegalAccessException
     *             illegal access exception
     * @throws InvocationTargetException
     *             invocation target exception
     */
    public static HashMap<Method, String> getListMethodInvoked(final Object bean, final String prefixAttribut) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {

        HashMap<Method, String> map = new HashMap<Method, String>();
        if (bean == null) {
            return map;
        } else {
            Method[] methodesBean = bean.getClass().getMethods();
            Pattern p = Pattern.compile("get" + StringUtils.capitalize(prefixAttribut));
            Matcher m = null;
            for (Method method : methodesBean) {
                m = p.matcher(method.getName());
                Object value = null;
                if (m.lookingAt()) {
                    value = method.invoke(bean);
                    if (value != null && !map.keySet().contains(method)) {
                        map.put(method, value.toString());
                    }
                }
            }
            return map;
        }
    }

    /**
     * Retourne une liste d'objets date de fin de saison et date debut de
     * saison.
     *
     * @param bean
     *            bean
     * @param prefixAttribut
     *            prefix attribut
     * @return List
     * @throws IllegalArgumentException
     *             illegal argument exception
     * @throws IllegalAccessException
     *             illegal access exception
     * @throws InvocationTargetException
     *             invocation target exception
     */

    public static List<Object> getListDates(Object bean, String prefixAttribut) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {

        List<Object> dateDebutFins = new ArrayList<Object>();
        // List<String> dateDebutFin2s= getListValeursOccurences(bean,
        // periodeActiviteSaisonniere2);

        HashMap<Method, String> map = getListMethodInvoked(bean, prefixAttribut);

        Objectdate objectdate1 = new Objectdate();
        Objectdate objectdate2 = new Objectdate();

        for (java.util.Map.Entry<Method, String> entry : map.entrySet()) {
            Method cle = entry.getKey();
            String valeur = entry.getValue();
            // traitements

            if (cle.getName() == "getPeriodeActiviteSaisonniere1DateDebut")

                objectdate1.setDateDebut(valeur);
            else if (cle.getName() == "getPeriodeActiviteSaisonniere1DateFin") {
                objectdate1.setDateFin(valeur);
            }

            if (cle.getName() == "getPeriodeActiviteSaisonniere2DateDebut")
                objectdate2.setDateDebut(valeur);
            else if (cle.getName() == "getPeriodeActiviteSaisonniere2DateFin") {
                objectdate2.setDateFin(valeur);
            }

        }
        if (objectdate1.getDateDebut() != null || objectdate1.getDateFin() != null) {
            dateDebutFins.add(objectdate1);
        }

        if (objectdate2.getDateDebut() != null || objectdate2.getDateFin() != null) {
            dateDebutFins.add(objectdate2);
        }

        return dateDebutFins;
    }

    /**
     * Get la liste Insaisissabilité.
     * 
     * @param numeroFormalite
     *            le numéro de la formalité xml
     * @return la liste des Insaisissabilité pour la rubrique ISP
     * @throws XmlFieldParsingException
     *             exception
     * @throws TechnicalException
     *             exception
     */
    public static List<InsaisissabiliteBean> getListInsaisissabilite(final Object cfe, final String typeFormalite) throws XmlFieldParsingException, TechnicalException {

        AbstractFormalite formaliteBean = null;
        List<InsaisissabiliteBean> listInsaisissabilite = new ArrayList<InsaisissabiliteBean>();

        if (typeFormalite.equals(TypeDossierEnum.CREATION.getFlowId())) {
            formaliteBean = (DialogueCreationBean) cfe;
        } else if (typeFormalite.equals(TypeDossierEnum.REGULARISATION.getFlowId())) {
            formaliteBean = (DialogueRegularisationBean) cfe;
        } else if (typeFormalite.equals(TypeDossierEnum.MODIFICATION.getFlowId())) {
            formaliteBean = (DialogueModificationBean) cfe;
        }

        if (null == formaliteBean || null == formaliteBean.getEntreprise()) {
            return null;
        }

        // déclaration autres biens
        if (formaliteBean.getEntreprise().getInsaisissabiliteDeclarationAutresBiens() != null && !formaliteBean.getEntreprise().getInsaisissabiliteDeclarationAutresBiens().isEmpty()) {
            InsaisissabiliteBean insaisissabiliteDeclaration = new InsaisissabiliteBean();
            insaisissabiliteDeclaration.setModalite("C");
            insaisissabiliteDeclaration.setLieu(formaliteBean.getEntreprise().getInsaisissabilitePublicationDeclarationAutresBiens());
            listInsaisissabilite.add(insaisissabiliteDeclaration);
        }

        // Renonciation autres biens
        if (formaliteBean.getEntreprise().getInsaisissabiliteRenonciationDeclarationAutresBiens() != null
                && !formaliteBean.getEntreprise().getInsaisissabiliteRenonciationDeclarationAutresBiens().isEmpty()) {
            InsaisissabiliteBean insaisissabiliteDeclaration = new InsaisissabiliteBean();
            insaisissabiliteDeclaration.setModalite("D");
            insaisissabiliteDeclaration.setLieu(formaliteBean.getEntreprise().getInsaisissabilitePublicationRenonciationDeclarationAutresBiens());
            listInsaisissabilite.add(insaisissabiliteDeclaration);
        }

        return listInsaisissabilite;
    }

    /**
     * Retourne une liste des régimes fiscals principal et secondaire de
     * l'établissement.
     *
     * @param regimePrincipal
     *            regime principal
     * @param regimeSecondaire
     *            regime secondaire
     * @param regimePrincipalBean
     *            regime principal bean
     * @param regimeFiscalSecondaireBean
     *            regime fiscal secondaire bean
     * @return List
     * @throws IllegalArgumentException
     *             illegal argument exception
     * @throws IllegalAccessException
     *             illegal access exception
     * @throws InvocationTargetException
     *             invocation target exception
     */

    public static List<Object> getListeRegimesEtablissement(String regimePrincipal, String regimeSecondaire, RegimeFiscalBean regimePrincipalBean, RegimeFiscalBean regimeFiscalSecondaireBean)
            throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        List<Object> retours = new ArrayList<Object>();
        ObjectRegime objectPrincipal = new ObjectRegime();
        ObjectRegime objectSecondaire = new ObjectRegime();

        if (regimePrincipal != null) {
            objectPrincipal.setValeur1(regimePrincipal.toString());
        }
        if (regimeSecondaire != null) {
            objectSecondaire.setValeur1(regimeSecondaire.toString());
        }

        if (regimePrincipalBean != null) {
            setObjetValeur2(regimePrincipalBean, objectPrincipal);
            setObjetValeur3(regimePrincipalBean, objectPrincipal);
        }

        if (regimeFiscalSecondaireBean != null) {
            setObjetValeur2(regimeFiscalSecondaireBean, objectSecondaire);
            setObjetValeur3(regimeFiscalSecondaireBean, objectSecondaire);
        }

        nonNulObjet(objectPrincipal, retours);
        nonNulObjet(objectSecondaire, retours);

        return retours;

    }

    /**
     * Sonar:Complexite du code, set l'attribut 3 de l'objet Set la valeur d'un
     * objet donné.
     *
     * @param regimeBean
     *            regime bean
     * @param regimeObjet
     *            regime objet
     * @throws IllegalArgumentException
     *             illegal argument exception
     * @throws IllegalAccessException
     *             illegal access exception
     * @throws InvocationTargetException
     *             invocation target exception
     */

    public static void setObjetValeur2(RegimeFiscalBean regimeBean, ObjectRegime regimeObjet) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {

        if (regimeBean != null) {
            if (regimeBean.getRegimeImpositionTVAOptionsParticulieres1() != null && regimeBean.getRegimeImpositionTVAOptionsParticulieres1().equals("oui")) {
                regimeObjet.setValeur2("410");
            } else if (regimeBean.getRegimeImpositionTVAOptionsParticulieres2() != null && regimeBean.getRegimeImpositionTVAOptionsParticulieres2().equals("oui")) {
                regimeObjet.setValeur2("411");
            } else if (regimeBean.getRegimeImpositionTVAOptionsParticulieres3() != null && regimeBean.getRegimeImpositionTVAOptionsParticulieres3().equals("oui")) {
                regimeObjet.setValeur2("412");

            } else if (regimeBean.getRegimeImpositionTVAOptionsParticulieres4() != null && regimeBean.getRegimeImpositionTVAOptionsParticulieres4().equals("oui")) {
                regimeObjet.setValeur2("414");

            } else
                regimeObjet.setValeur2(null);
        }
    }

    /**
     * Sonar:Complexite du code, set l'attribut 3 de l'objet Set la valeur d'un
     * objet donné.
     *
     * @param regimeBean
     *            regime bean
     * @param regimeObjet
     *            regime objet
     * @throws IllegalArgumentException
     *             illegal argument exception
     * @throws IllegalAccessException
     *             illegal access exception
     * @throws InvocationTargetException
     *             invocation target exception
     */
    public static void setObjetValeur3(RegimeFiscalBean regimeBean, ObjectRegime regimeObjet) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {

        if (regimeBean.getRegimeImpositionTVAConditionVersement() != null && regimeBean.getRegimeImpositionTVAConditionVersement().equals("oui")) {

            regimeObjet.setValeur3("512");
        }

    }

    /**
     * Sonar: vérifier si un des attributs d'un objet n'est pas nul.
     *
     * @param regimeObjet
     *            regime objet
     * @param retours
     *            retours
     * @throws IllegalArgumentException
     *             illegal argument exception
     * @throws IllegalAccessException
     *             illegal access exception
     * @throws InvocationTargetException
     *             invocation target exception
     */
    public static void nonNulObjet(ObjectRegime regimeObjet, List<Object> retours) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {

        if (regimeObjet.getValeur1() != null || regimeObjet.getValeur2() != null || regimeObjet.getValeur3() != null) {

            retours.add(regimeObjet);

        }
    }

    /*
     * retourne la liste des codes regime Fiscal principal et secondaire
     * 
     */

    /**
     * Accesseur sur l'attribut {@link #liste regimes}.
     *
     * @param regimeFiscalPrincipal
     *            regime fiscal principal
     * @param regimeFiscalSecondaire
     *            regime fiscal secondaire
     * @return liste regimes
     * @throws IllegalArgumentException
     *             illegal argument exception
     * @throws IllegalAccessException
     *             illegal access exception
     * @throws InvocationTargetException
     *             invocation target exception
     */
    public static List<String> getListeRegimes(String regimeFiscalPrincipal, String regimeFiscalSecondaire) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {

        List<String> retours = new ArrayList<String>();
        if (StringUtils.isEmpty(regimeFiscalPrincipal) && StringUtils.isEmpty(regimeFiscalSecondaire)) {
            return retours;
        }

        if (!StringUtils.isEmpty(regimeFiscalPrincipal)) {
            retours.add(regimeFiscalPrincipal.toString());
        }
        if (!StringUtils.isEmpty(regimeFiscalSecondaire)) {
            retours.add(regimeFiscalSecondaire.toString());
        }
        return retours;

    }

    /**
     * Class Objectdate.
     */
    public static class Objectdate {

        /** Réseau CFE Urssaf. */
        String dateDebut;

        /** date fin. */
        String dateFin;

        /**
         * Accesseur sur l'attribut {@link #date debut}.
         *
         * @return date debut
         */
        public String getDateDebut() {
            return dateDebut;
        }

        /**
         * Mutateur sur l'attribut {@link #date debut}.
         *
         * @param dateDebut
         *            la nouvelle valeur de l'attribut date debut
         */
        public void setDateDebut(String dateDebut) {
            this.dateDebut = dateDebut;
        }

        /**
         * Accesseur sur l'attribut {@link #date fin}.
         *
         * @return date fin
         */
        public String getDateFin() {
            return dateFin;
        }

        /**
         * Mutateur sur l'attribut {@link #date fin}.
         *
         * @param dateFin
         *            la nouvelle valeur de l'attribut date fin
         */
        public void setDateFin(String dateFin) {
            this.dateFin = dateFin;
        }

    };

    /**
     * Class ObjectRegime.
     */
    public static class ObjectRegime {

        /** Réseau CFE Urssaf. */
        String valeur1;

        /** valeur 2. */
        String valeur2;

        /** valeur 3. */
        String valeur3;

        /**
         * Accesseur sur l'attribut {@link #valeur 1}.
         *
         * @return valeur 1
         */
        public String getValeur1() {
            return valeur1;
        }

        /**
         * Mutateur sur l'attribut {@link #valeur 1}.
         *
         * @param valeur1
         *            la nouvelle valeur de l'attribut valeur 1
         */
        public void setValeur1(String valeur1) {
            this.valeur1 = valeur1;
        }

        /**
         * Accesseur sur l'attribut {@link #valeur 2}.
         *
         * @return valeur 2
         */
        public String getValeur2() {
            return valeur2;
        }

        /**
         * Mutateur sur l'attribut {@link #valeur 2}.
         *
         * @param valeur2
         *            la nouvelle valeur de l'attribut valeur 2
         */
        public void setValeur2(String valeur2) {
            this.valeur2 = valeur2;
        }

        /**
         * Accesseur sur l'attribut {@link #valeur 3}.
         *
         * @return valeur 3
         */
        public String getValeur3() {
            return valeur3;
        }

        /**
         * Mutateur sur l'attribut {@link #valeur 3}.
         *
         * @param valeur3
         *            la nouvelle valeur de l'attribut valeur 3
         */
        public void setValeur3(String valeur3) {
            this.valeur3 = valeur3;
        }
    };

    /**
     * Affiche l'entier passe en parametre suivant le format voulu.
     *
     * @param pattern
     *            : Le format desire (exemple : 00000 ==>resultat 00101
     * @param valeur
     *            valeur
     * @return string
     */
    public static String formatNumber(final String pattern, final Integer valeur) {

        if (StringUtils.isNotEmpty(pattern) && valeur != null) {
            NumberFormat nf = new DecimalFormat(pattern);
            return nf.format(valeur);
        }

        return null;
    }

    /**
     * Get le list valeurs.
     * 
     * @param numeroFormalite
     *            le numero formalite
     * @param bean
     *            le bean
     * @param prefixAttribut
     *            le prefix attribut
     * @return al liste des prenom de Conjoint ou du dirigeant
     * @throws XmlFieldParsingException
     *             le xml field parsing exception
     * @throws TechnicalException
     *             TechnicalException
     */
    public static List<String> getListValeurs(final Object cfe, final String bean, final String prefixAttribut) throws XmlFieldParsingException, TechnicalException {
        List<String> retours = new ArrayList<String>();
        if (StringUtils.isEmpty(prefixAttribut)) {
            return retours;
        }

        if (cfe == null) {
            return retours;
        }

        Object beanInstance = cfe;

        try {
            if (StringUtils.isNotEmpty(bean)) {
                String beanPath = StringUtils.remove(bean, '?');
                List<String> arboressenceBean = Arrays.asList(StringUtils.split(beanPath, "."));
                for (String metode : arboressenceBean) {
                    String prefixMethod = "get" + StringUtils.capitalize(metode);
                    if (beanInstance == null) {
                        return retours;
                    }
                    while (beanInstance instanceof List) {
                        beanInstance = ((List) beanInstance).get(0);
                    }

                    Method method = beanInstance.getClass().getMethod(prefixMethod);

                    beanInstance = method.invoke(beanInstance);

                }
            }

            if (beanInstance instanceof List) {
                beanInstance = ((List) beanInstance).get(0);
            }

            Method[] methodesBean = beanInstance.getClass().getMethods();
            Pattern p = Pattern.compile("get" + StringUtils.capitalize(prefixAttribut));
            Matcher m = null;
            for (Method method : methodesBean) {
                m = p.matcher(method.getName());
                Object value = null;
                if (m.lookingAt()) {
                    value = method.invoke(beanInstance);
                    if (value != null) {
                        retours.add(value.toString());
                    }
                }
            }

        } catch (Exception e) {
            LOGGER_FONC.error(e.getMessage(), e);
            return retours;
        }

        return retours;
    }

    /**
     * Store choices values.
     * 
     * @param libelle
     *            le libelle
     * @param mapKeyValeur
     *            le map key valeur
     * @return le string
     */
    public static String storeChoicesValues(final String libelle, final String mapKeyValeur) {

        mapCorrespondanceLibelle = StringTransformerUtil.stringToMap(mapKeyValeur, true, true);
        if (mapCorrespondanceLibelle.containsKey(StringUtils.upperCase(libelle))) {
            return mapCorrespondanceLibelle.get(StringUtils.upperCase(libelle));
        } else {
            return null;
        }
    }

    /**
     * cette méthode renvoie une liste des dirigeant en fonction de la nature du
     * dirigeant Personne physique ou Personne Moral.
     *
     * @param listDirigeant
     *            list dirigeant une liste de dirigeant complète contient les 2
     *            natures PP et PM
     * @param natureDirigeant
     *            nature dirigeant exp : Personne Physique ou Personne Morale
     * @return dirigeant une liste de dirigeant de personne Morale ou de
     *         personne Physique
     */
    public static List<DirigeantBean> getListDirigeants(List<DirigeantBean> listDirigeant, String natureDirigeant) {
        List<DirigeantBean> list = new ArrayList<DirigeantBean>();
        for (DirigeantBean dirigeantBean : listDirigeant) {
            if (StringUtils.equals(dirigeantBean.getNature(), natureDirigeant)) {
                list.add(dirigeantBean);
            }
        }
        return list;
    }

    /**
     * Getter de la balise C43 de la norme 2016. Pour les différents type de
     * dossier.
     * 
     * @param numeroFormalite
     *            le numero formalite
     * @param reseauCfe
     *            le reseau cfe
     * @return le contenue de la balise C43
     * @throws XmlFieldParsingException
     *             le xml field parsing exception
     * @throws TechnicalException
     *             : TechnicalException
     */
    public static String getBaliseC43(final Object profil, final Object cfe, final String typeFormalite) throws XmlFieldParsingException, TechnicalException {
        String observations = null;
        String dateDuJour = getDateDuJour("ddMMyyyy").toString();
        String baliseObsC43 = "";
        String reseauCfe = null;

        if (typeFormalite.equals(TypeDossierEnum.CREATION.getTypeTechnique())) {
            DialogueCreationBean cfeCreation = (DialogueCreationBean) cfe;
            ProfilCreationBean profilCreation = (ProfilCreationBean) profil;
            reseauCfe = cfeCreation.getReseauCFE();
            observations = cfeCreation.getObservations();
            baliseObsC43 = OBS_C43_CREATION_01P_05P;
            if (OUI.equals(profilCreation.getMicroSocialOuiNon())) {
                if (StringUtils.isNotEmpty(observations)) {
                    return StringUtils.substring(baliseObsC43 + observations, 0, 299);
                } else {
                    return baliseObsC43;
                }
            } else {
                return observations;
            }
        } else if (typeFormalite.equals(TypeDossierEnum.REGULARISATION.getTypeTechnique())) {
            DialogueRegularisationBean cfeRegularisation = (DialogueRegularisationBean) cfe;
            reseauCfe = cfeRegularisation.getReseauCFE();
            if (RESEAU_CFE_URSSAF.equals(reseauCfe)) {
                return StringUtils.EMPTY;
            }
            observations = cfeRegularisation.getObservations();
            baliseObsC43 = OBS_C43_REGUL;
            String dateDebutActivite = cfeRegularisation.getEntreprise().getEtablissement().getDateDebutActivite();
            dateDuJour = StringUtils.remove(dateDebutActivite.toString(), '/');
        } else if (typeFormalite.equals(TypeDossierEnum.MODIFICATION.getTypeTechnique())) {
            DialogueModificationBean cfeModification = (DialogueModificationBean) cfe;
            observations = cfeModification.getObservations();
            baliseObsC43 = OBS_C43_MODIF;
            dateDuJour = StringUtils.EMPTY;
            reseauCfe = cfeModification.getReseauCFE();
        } else if (typeFormalite.equals(TypeDossierEnum.CESSATION.getTypeTechnique())) {
            DialogueCessationBean cfeCessation = (DialogueCessationBean) cfe;
            observations = cfeCessation.getObservations();
            reseauCfe = cfeCessation.getReseauCFE();
        }

        String obs = new String();
        int tailleMaxObs = OBS_TAILLE_MAX - baliseObsC43.length() - getDateDuJour("ddMMyyyy").length();
        if (StringUtils.isNotEmpty(observations)) {
            if (observations.length() > tailleMaxObs) {
                // On tronque les observations de l'utilisateur
                observations = observations.substring(0, tailleMaxObs);
            }
            // on filtre le caractère séparateur '!' et le remplace par un
            // espace
            observations = observations.replace("!", " ");
        }

        obs = baliseObsC43 + (StringUtils.isNotEmpty(dateDuJour) ? dateDuJour + SEPARATEUR_C43 : "");
        if (listCorrespondanceLiasseC43.contains(reseauCfe)) {
            return obs;
        }
        if (StringUtils.isNotEmpty(observations)) {
            return obs + observations;
        } else {
            return obs;
        }
    }

    /**
     * construit le complement de la balise C43 pour l'XML regent en version
     * 2008.11.
     * 
     * @param formalite
     * @return le complement de la balise C43
     */
    private static String getComplement200811C43(final AbstractFormalite formalite) {
        StringBuilder complement = new StringBuilder();

        // NonDiffusionInformation
        if (formalite.getNonDiffusionInformation() == null || "non".equals(formalite.getNonDiffusionInformation())) {
            complement.append("C45=O!");
        } else {
            complement.append("C45=N!");
        }

        // nombreActivitesSoumisQualification
        Integer nbAQPA = formalite.getEntreprise().getEtablissement().getNombreActivitesSoumisesQualification();
        if (nbAQPA != null) {
            complement.append("JQPA=" + nbAQPA + "!");
        }

        // insaisissabiliteRenonciationRP
        if (formalite.getEntreprise().getInsaisissabiliteRenonciationRP() != null) {
            complement.append("P90.1=A!" + formalite.getEntreprise().getInsaisissabilitePublicationRenonciationRP() + "!");
        }

        // insaisissabiliteRevocationRP
        if (formalite.getEntreprise().getInsaisissabiliteRevocationRP() != null) {
            complement.append("P90.1=B!" + formalite.getEntreprise().getInsaisissabilitePublicationRevocationRP() + "!");
        }
        // insaisissabiliteDeclarationAutresBiens
        if (formalite.getEntreprise().getInsaisissabiliteDeclarationAutresBiens() != null) {
            complement.append("P90.2=C1!" + formalite.getEntreprise().getInsaisissabilitePublicationDeclarationAutresBiens() + "!");
        }
        // insaisissabiliteRenonciationDeclarationAutresBiens
        if (formalite.getEntreprise().getInsaisissabiliteRenonciationDeclarationAutresBiens() != null) {
            complement.append("P90.2=D1!" + formalite.getEntreprise().getInsaisissabilitePublicationRenonciationDeclarationAutresBiens() + "!");
        }

        return complement.toString();
    }

    /**
     * construit le complement de la balise C43 pour l'XML regent en version
     * 2008.11.
     * 
     * @param formalite
     * @return le complement de la balise C43
     */
    private static String getComplement200811C43Cessation(final DialogueCessationBean formalite) {
        StringBuilder complement = new StringBuilder();

        // NonDiffusionInformation
        if (formalite.getNonDiffusionInformation() == null || "non".equals(formalite.getNonDiffusionInformation())) {
            complement.append("C45=O!");
        } else {
            complement.append("C45=N!");
        }
        return complement.toString();
    }

    /**
     * Getter de la balise C43 de la norme v2008.11. Pour les différents type de
     * dossier.
     * 
     * @param numeroFormalite
     *            le numero formalite
     * @param reseauCfe
     *            le reseau cfe
     * @return le contenue de la balise C43
     * @throws XmlFieldParsingException
     *             le xml field parsing exception
     * @throws TechnicalException
     *             : TechnicalException
     */
    public static String getBaliseC43V2008(final Object profil, final Object cfe, final String typeFormalite) throws XmlFieldParsingException, TechnicalException {
        String baliseObsC43 = "";
        String observations = null;
        String dateDuJour = getDateDuJour("ddMMyyyy").toString();
        String reseauCfe = null;

        if (typeFormalite.equals(TypeDossierEnum.CREATION.getTypeTechnique())) {
            DialogueCreationBean cfeCreation = (DialogueCreationBean) cfe;
            ProfilCreationBean profilCreation = (ProfilCreationBean) profil;
            reseauCfe = cfeCreation.getReseauCFE();
            observations = cfeCreation.getObservations();
            baliseObsC43 = OBS_C43_CREATION_01P_05P + getComplement200811C43(cfeCreation);
            if (OUI.equals(profilCreation.getMicroSocialOuiNon())) {
                if (StringUtils.isNotEmpty(observations)) {
                    return StringUtils.substring(baliseObsC43 + observations, 0, 299);
                } else {
                    return baliseObsC43;
                }
            } else {
                return observations;
            }
        } else if (typeFormalite.equals(TypeDossierEnum.REGULARISATION.getTypeTechnique())) {
            if (RESEAU_CFE_URSSAF.equals(reseauCfe)) {
                return StringUtils.EMPTY;
            }
            DialogueRegularisationBean cfeRegularisation = (DialogueRegularisationBean) cfe;
            reseauCfe = cfeRegularisation.getReseauCFE();
            observations = cfeRegularisation.getObservations();
            dateDuJour = StringUtils.remove(cfeRegularisation.getEntreprise().getEtablissement().getDateDebutActivite().toString(), '/');
            baliseObsC43 = OBS_C43_REGUL + dateDuJour + SEPARATEUR_C43 + getComplement200811C43(cfeRegularisation);
            dateDuJour = StringUtils.EMPTY;
        } else if (typeFormalite.equals(TypeDossierEnum.MODIFICATION.getTypeTechnique())) {
            DialogueModificationBean cfeModification = (DialogueModificationBean) cfe;
            reseauCfe = cfeModification.getReseauCFE();
            observations = cfeModification.getObservations();
            baliseObsC43 = OBS_C43_MODIF + getComplement200811C43(cfeModification);
            dateDuJour = StringUtils.EMPTY;
        } else if (typeFormalite.equals(TypeDossierEnum.CESSATION.getTypeTechnique())) {
            DialogueCessationBean cfeCessation = (DialogueCessationBean) cfe;
            reseauCfe = cfeCessation.getReseauCFE();
            baliseObsC43 = getComplement200811C43Cessation(cfeCessation);
            observations = cfeCessation.getObservations();
            dateDuJour = StringUtils.EMPTY;
        }

        String obs = new String();
        int tailleMaxObs = OBS_TAILLE_MAX - baliseObsC43.length() - getDateDuJour("ddMMyyyy").length();
        if (StringUtils.isNotEmpty(observations)) {
            if (observations.length() > tailleMaxObs) {
                // On tronque les observations de l'utilisateur
                observations = observations.substring(0, tailleMaxObs);
            }
            // on filtre le caractère séparateur '!' et le remplace par un
            // espace
            observations = observations.replace("!", " ");
        }

        obs = baliseObsC43 + (StringUtils.isNotEmpty(dateDuJour) ? dateDuJour + SEPARATEUR_C43 : "");
        if (listCorrespondanceLiasseC43.contains(reseauCfe)) {
            return obs;
        }
        if (StringUtils.isNotEmpty(observations)) {
            return obs + observations;
        } else {
            return obs;
        }
    }

    /**
     * Get le code pays.
     * 
     * @param codePays
     *            le code pays
     * @return le code pays
     */
    public static String getCodePays(final String codePays) {
        String retour = null;
        if (StringUtils.isNotEmpty(codePays)) {
            retour = "99" + codePays;
        }
        return retour;

    }

    /**
     * Get le map correspondance liasse.
     * 
     * @return le map correspondance liasse
     */
    public static Map<String, String> getMapCorrespondanceLiasse() {
        return mapCorrespondanceLiasse;
    }

    /**
     * Set le map correspondance liasse.
     * 
     * @param mapCorrespondanceLiasse
     *            le map correspondance liasse
     */
    public static void setMapCorrespondanceLiasse(final Map<String, String> mapCorrespondanceLiasse) {
        FonctionRegent.mapCorrespondanceLiasse = mapCorrespondanceLiasse;
    }

    /**
     * Get le list correspondance liasse c43.
     * 
     * @return le list correspondance liasse c43
     */
    public static List<String> getListCorrespondanceLiasseC43() {
        return listCorrespondanceLiasseC43;
    }

    /**
     * Set le list correspondance liasse c43.
     * 
     * @param listCorrespondanceLiasseC43
     *            le nouveau list correspondance liasse c43
     */
    public static void setListCorrespondanceLiasseC43(final List<String> listCorrespondanceLiasseC43) {
        FonctionRegent.listCorrespondanceLiasseC43 = listCorrespondanceLiasseC43;
    }

}
