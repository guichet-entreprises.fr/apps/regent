package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface A30.
 */
@ResourceXPath("/A30")
public interface IA30 {

  /**
   * Get le A30.1.
   *
   * @return le A30.1
   */
  @FieldXPath("A30.1")
  String getA301();

  /**
   * Set le A30.1.
   *
   * @param A30.1
   *          le nouveau A30.1
   */
  void setA301(String A301);

  /**
   * Get le A30.2.
   *
   * @return le A30.2
   */
  @FieldXPath("A30.2")
  String getA302();

  /**
   * Set le A30.2.
   *
   * @param A30.2
   *          le nouveau A30.2
   */
  void setA302(String A302);

  /**
   * Get le A30.3.
   *
   * @return le A30.3
   */
  @FieldXPath("A30.3")
  String getA303();

  /**
   * Set le A30.3.
   *
   * @param A30.3
   *          le nouveau A30.3
   */
  void setA303(String A303);

  /**
   * Get le A30.4.
   *
   * @return le A30.4
   */
  @FieldXPath("A30.4")
  String getA304();

  /**
   * Set le A30.4.
   *
   * @param A30.4
   *          le nouveau A30.4
   */
  void setA304(String A304);
}
