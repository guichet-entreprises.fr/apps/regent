package fr.ge.common.regent.bean.formalite.dialogue.creation;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.format.annotation.NumberFormat;

import fr.ge.common.regent.bean.formalite.dialogue.AbstractEntrepriseBean;

/**
 * Le Class EntrepriseBean.
 */
public class EntrepriseBean extends AbstractEntrepriseBean<EtablissementBean, EntrepriseLieeBean, DirigeantBean, PersonneLieeBean> implements IFormaliteVue {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 4488230261651566348L;

    /** Le activites principales. */
    private String activitesPrincipales;

    /** Le adresse. */
    private AdresseBean adresse = new AdresseBean();

    /** Le adresse entreprise pm situation. */
    private String adresseEntreprisePMSituation;

    /** Le associe unique est president. */
    private String associeUniqueEstPresident;

    /** Le capital apports nature. */
    private List<String> capitalApportsNature = new ArrayList<String>();
    /** Le capital apports nature superieur30000. */
    private String capitalApportsNatureSuperieur30000;

    /** Le capital apports nature superieur moitie capital. */
    private String capitalApportsNatureSuperieurMoitieCapital;

    /** Le capital devise. */
    private String capitalDevise;

    /** Le capital montant. */
    private String capitalMontant;

    /** Le capital variable. */
    private String capitalVariable;

    /** Le capital variable minimum. */
    private String capitalVariableMinimum;

    /** Le contrat appui date fin. */
    private String contratAppuiDateFin;

    /** Le contrat appui presence. */
    private String contratAppuiPresence;

    /** Le date agrement gaec. */
    private String dateAgrementGAEC;

    /** Le date cloture exercice social. */
    private String dateClotureExerciceSocial;

    /** Le date cloture premier exercice social. */
    private String dateCloturePremierExerciceSocial;

    /** Le demande accre. */
    private String demandeACCRE;

    /** Le denomination. */
    private String denomination;

    /** Le duree. */
    @NumberFormat
    private Integer duree;

    /** Le eirl. */
    private EirlBean eirl;

    /** Le entreprise liee contrat appui. */
    private EntrepriseLieeBean entrepriseLieeContratAppui = new EntrepriseLieeBean();

    /** Le entreprises liees fusion scission. */
    private List<EntrepriseLieeBean> entreprisesLieesFusionScission = new ArrayList<EntrepriseLieeBean>();

    /** Le entreprises liees precedents exploitants. */
    private List<EntrepriseLieeBean> entreprisesLieesPrecedentsExploitants = new ArrayList<EntrepriseLieeBean>();

    /** Le est activite artisanale option. */
    private String estActiviteArtisanaleOption;

    /** Le est activite artisanale principale. */
    private String estActiviteArtisanalePrincipale;

    /** Le est inscrit rcs. */
    private String estInscritRCS;

    /** Le est inscrit rm. */
    private String estInscritRM;

    /** Le est proprietaire terres exploitees. */
    private String estProprietaireTerresExploitees;

    /** Le forme groupement pastoral. */
    private String formeGroupementPastoral;

    /** Le fusion scission. */
    private String fusionScission;

    /** Le fusion scission nombre. */
    @NumberFormat
    private Integer fusionScissionNombre;

    /** Le information depot fonds. */
    private String informationDepotFonds;

    /** Le nature gerance. */
    private String natureGerance;

    /** Le nature gerance societe associee. */
    private String natureGeranceSocieteAssociee;

    /** Le personne pouvoir nombre. */
    @NumberFormat
    private Integer personnePouvoirNombre;

    /** Le personne pouvoir presence. */
    private String personnePouvoirPresence;

    /** Le regime fiscal eirl. */
    private RegimeFiscalBean regimeFiscalEirl = new RegimeFiscalBean();

    /** Le regime fiscal eirl secondaire. */
    private RegimeFiscalBean regimeFiscalEirlSecondaire = new RegimeFiscalBean();

    /** Le regime fiscal etablissement. */
    private RegimeFiscalBean regimeFiscalEtablissement = new RegimeFiscalBean();

    /** Le regime fiscal etablissement secondaire. */
    private RegimeFiscalBean regimeFiscalEtablissementSecondaire = new RegimeFiscalBean();

    /** Le sigle. */
    private String sigle;

    /** Le statut legal particulier. */
    private String statutLegalParticulier;

    /** Le statuts types. */
    private String statutsTypes;

    /**
     * Getter de l'attribut serialversionuid.
     * 
     * @return la valeur de serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    /**
     * Getter de l'attribut activitesPrincipales.
     * 
     * @return la valeur de activitesPrincipales
     */
    public String getActivitesPrincipales() {
        return this.activitesPrincipales;
    }

    /**
     * Getter de l'attribut adresse.
     * 
     * @return la valeur de adresse
     */
    public AdresseBean getAdresse() {
        return this.adresse;
    }

    /**
     * Getter de l'attribut adresseEntreprisePMSituation.
     * 
     * @return la valeur de adresseEntreprisePMSituation
     */
    public String getAdresseEntreprisePMSituation() {
        return this.adresseEntreprisePMSituation;
    }

    /**
     * Getter de l'attribut associeUniqueEstPresident.
     * 
     * @return la valeur de associeUniqueEstPresident
     */
    public String getAssocieUniqueEstPresident() {
        return this.associeUniqueEstPresident;
    }

    /**
     * Getter de l'attribut capitalApportsNature.
     * 
     * @return la valeur de capitalApportsNature
     */
    public List<String> getCapitalApportsNature() {
        return this.capitalApportsNature;
    }

    /**
     * Getter de l'attribut capitalApportsNatureSuperieur30000.
     * 
     * @return la valeur de capitalApportsNatureSuperieur30000
     */
    public String getCapitalApportsNatureSuperieur30000() {
        return this.capitalApportsNatureSuperieur30000;
    }

    /**
     * Getter de l'attribut capitalApportsNatureSuperieurMoitieCapital.
     * 
     * @return la valeur de capitalApportsNatureSuperieurMoitieCapital
     */
    public String getCapitalApportsNatureSuperieurMoitieCapital() {
        return this.capitalApportsNatureSuperieurMoitieCapital;
    }

    /**
     * Getter de l'attribut capitalDevise.
     * 
     * @return la valeur de capitalDevise
     */
    public String getCapitalDevise() {
        return this.capitalDevise;
    }

    /**
     * Getter de l'attribut capitalMontant.
     * 
     * @return la valeur de capitalMontant
     */
    public String getCapitalMontant() {
        return this.capitalMontant;
    }

    /**
     * Getter de l'attribut capitalVariable.
     * 
     * @return la valeur de capitalVariable
     */
    public String getCapitalVariable() {
        return this.capitalVariable;
    }

    /**
     * Getter de l'attribut capitalVariableMinimum.
     * 
     * @return la valeur de capitalVariableMinimum
     */
    public String getCapitalVariableMinimum() {
        return this.capitalVariableMinimum;
    }

    /**
     * Getter de l'attribut contratAppuiDateFin.
     * 
     * @return la valeur de contratAppuiDateFin
     */
    public String getContratAppuiDateFin() {
        return this.contratAppuiDateFin;
    }

    /**
     * Getter de l'attribut contratAppuiPresence.
     * 
     * @return la valeur de contratAppuiPresence
     */
    public String getContratAppuiPresence() {
        return this.contratAppuiPresence;
    }

    /**
     * Getter de l'attribut dateAgrementGAEC.
     * 
     * @return la valeur de dateAgrementGAEC
     */
    public String getDateAgrementGAEC() {
        return this.dateAgrementGAEC;
    }

    /**
     * Getter de l'attribut dateClotureExerciceSocial.
     * 
     * @return la valeur de dateClotureExerciceSocial
     */
    public String getDateClotureExerciceSocial() {
        return this.dateClotureExerciceSocial;
    }

    /**
     * Getter de l'attribut dateCloturePremierExerciceSocial.
     * 
     * @return la valeur de dateCloturePremierExerciceSocial
     */
    public String getDateCloturePremierExerciceSocial() {
        return this.dateCloturePremierExerciceSocial;
    }

    /**
     * Getter de l'attribut demandeACCRE.
     * 
     * @return la valeur de demandeACCRE
     */
    public String getDemandeACCRE() {
        return this.demandeACCRE;
    }

    /**
     * Getter de l'attribut denomination.
     * 
     * @return la valeur de denomination
     */
    public String getDenomination() {
        return this.denomination;
    }

    /**
     * Getter de l'attribut duree.
     * 
     * @return la valeur de duree
     */
    public Integer getDuree() {
        return this.duree;
    }

    /**
     * Getter de l'attribut eirl.
     * 
     * @return la valeur de eirl
     */
    public EirlBean getEirl() {
        return this.eirl;
    }

    /**
     * getEntrepriseLieeContratAppui.
     * 
     * @return entrepriseLieeContratAppui
     */
    public EntrepriseLieeBean getEntrepriseLieeContratAppui() {
        return this.entrepriseLieeContratAppui;
    }

    /**
     * Getter de l'attribut entreprisesLieesFusionScission.
     * 
     * @return la valeur de entreprisesLieesFusionScission
     */
    public List<EntrepriseLieeBean> getEntreprisesLieesFusionScission() {
        return this.entreprisesLieesFusionScission;
    }

    /**
     * Getter de l'attribut entreprisesLieesPrecedentsExploitants.
     * 
     * @return la valeur de entreprisesLieesPrecedentsExploitants
     */
    public List<EntrepriseLieeBean> getEntreprisesLieesPrecedentsExploitants() {
        return this.entreprisesLieesPrecedentsExploitants;
    }

    /**
     * Getter de l'attribut estActiviteArtisanaleOption.
     * 
     * @return la valeur de estActiviteArtisanaleOption
     */
    public String getEstActiviteArtisanaleOption() {
        return this.estActiviteArtisanaleOption;
    }

    /**
     * Getter de l'attribut estActiviteArtisanalePrincipale.
     * 
     * @return la valeur de estActiviteArtisanalePrincipale
     */
    public String getEstActiviteArtisanalePrincipale() {
        return this.estActiviteArtisanalePrincipale;
    }

    /**
     * Getter de l'attribut estInscritRCS.
     * 
     * @return la valeur de estInscritRCS
     */
    public String getEstInscritRCS() {
        return this.estInscritRCS;
    }

    /**
     * Getter de l'attribut estInscritRM.
     * 
     * @return la valeur de estInscritRM
     */
    public String getEstInscritRM() {
        return this.estInscritRM;
    }

    /**
     * Getter de l'attribut estProprietaireTerresExploitees.
     * 
     * @return la valeur de estProprietaireTerresExploitees
     */
    public String getEstProprietaireTerresExploitees() {
        return this.estProprietaireTerresExploitees;
    }

    /**
     * Getter de l'attribut formeGroupementPastoral.
     * 
     * @return la valeur de formeGroupementPastoral
     */
    public String getFormeGroupementPastoral() {
        return this.formeGroupementPastoral;
    }

    /**
     * Getter de l'attribut fusionScission.
     * 
     * @return la valeur de fusionScission
     */
    public String getFusionScission() {
        return this.fusionScission;
    }

    /**
     * Getter de l'attribut fusionScissionNombre.
     * 
     * @return la valeur de fusionScissionNombre
     */
    public Integer getFusionScissionNombre() {
        return this.fusionScissionNombre;
    }

    /**
     * Getter de l'attribut informationDepotFonds.
     * 
     * @return la valeur de informationDepotFonds
     */
    public String getInformationDepotFonds() {
        return this.informationDepotFonds;
    }

    /**
     * Getter de l'attribut natureGerance.
     * 
     * @return la valeur de natureGerance
     */
    public String getNatureGerance() {
        return this.natureGerance;
    }

    /**
     * Getter de l'attribut natureGeranceSocieteAssociee.
     * 
     * @return la valeur de natureGeranceSocieteAssociee
     */
    public String getNatureGeranceSocieteAssociee() {
        return this.natureGeranceSocieteAssociee;
    }

    /**
     * Getter de l'attribut personnePouvoirNombre.
     * 
     * @return la valeur de personnePouvoirNombre
     */
    public Integer getPersonnePouvoirNombre() {
        return this.personnePouvoirNombre;
    }

    /**
     * Getter de l'attribut personnePouvoirPresence.
     * 
     * @return la valeur de personnePouvoirPresence
     */
    public String getPersonnePouvoirPresence() {
        return this.personnePouvoirPresence;
    }

    /**
     * Getter de l'attribut regimeFiscalEirl.
     * 
     * @return la valeur de regimeFiscalEirl
     */
    public RegimeFiscalBean getRegimeFiscalEirl() {
        return this.regimeFiscalEirl;
    }

    /**
     * Getter de l'attribut regimeFiscalEirlSecondaire.
     * 
     * @return la valeur de regimeFiscalEirlSecondaire
     */
    public RegimeFiscalBean getRegimeFiscalEirlSecondaire() {
        return this.regimeFiscalEirlSecondaire;
    }

    /**
     * Getter de l'attribut regimeFiscalEtablissement.
     * 
     * @return la valeur de regimeFiscalEtablissement
     */
    public RegimeFiscalBean getRegimeFiscalEtablissement() {
        return this.regimeFiscalEtablissement;
    }

    /**
     * Getter de l'attribut regimeFiscalEtablissementSecondaire.
     * 
     * @return la valeur de regimeFiscalEtablissementSecondaire
     */
    public RegimeFiscalBean getRegimeFiscalEtablissementSecondaire() {
        return this.regimeFiscalEtablissementSecondaire;
    }

    /**
     * Getter de l'attribut sigle.
     * 
     * @return la valeur de sigle
     */
    public String getSigle() {
        return this.sigle;
    }

    /**
     * Getter de l'attribut statutLegalParticulier.
     * 
     * @return la valeur de statutLegalParticulier
     */
    public String getStatutLegalParticulier() {
        return this.statutLegalParticulier;
    }

    /**
     * Getter de l'attribut statutsTypes.
     * 
     * @return la valeur de statutsTypes
     */
    public String getStatutsTypes() {
        return this.statutsTypes;
    }

    /**
     * Setter de l'attribut activitesPrincipales.
     * 
     * @param activitesPrincipales
     *            la nouvelle valeur de activitesPrincipales
     */
    public void setActivitesPrincipales(String activitesPrincipales) {
        this.activitesPrincipales = activitesPrincipales;
    }

    /**
     * Setter de l'attribut adresse.
     * 
     * @param adresse
     *            la nouvelle valeur de adresse
     */
    public void setAdresse(AdresseBean adresse) {
        this.adresse = adresse;
    }

    /**
     * Setter de l'attribut adresseEntreprisePMSituation.
     * 
     * @param adresseEntreprisePMSituation
     *            la nouvelle valeur de adresseEntreprisePMSituation
     */
    public void setAdresseEntreprisePMSituation(String adresseEntreprisePMSituation) {
        this.adresseEntreprisePMSituation = adresseEntreprisePMSituation;
    }

    /**
     * Setter de l'attribut associeUniqueEstPresident.
     * 
     * @param associeUniqueEstPresident
     *            la nouvelle valeur de associeUniqueEstPresident
     */
    public void setAssocieUniqueEstPresident(String associeUniqueEstPresident) {
        this.associeUniqueEstPresident = associeUniqueEstPresident;
    }

    /**
     * Setter de l'attribut capitalApportsNature.
     * 
     * @param capitalApportsNature
     *            la nouvelle valeur de capitalApportsNature
     */
    public void setCapitalApportsNature(List<String> capitalApportsNature) {
        this.capitalApportsNature = capitalApportsNature;
    }

    /**
     * Setter de l'attribut capitalApportsNatureSuperieur30000.
     * 
     * @param capitalApportsNatureSuperieur30000
     *            la nouvelle valeur de capitalApportsNatureSuperieur30000
     */
    public void setCapitalApportsNatureSuperieur30000(String capitalApportsNatureSuperieur30000) {
        this.capitalApportsNatureSuperieur30000 = capitalApportsNatureSuperieur30000;
    }

    /**
     * Setter de l'attribut capitalApportsNatureSuperieurMoitieCapital.
     * 
     * @param capitalApportsNatureSuperieurMoitieCapital
     *            la nouvelle valeur de
     *            capitalApportsNatureSuperieurMoitieCapital
     */
    public void setCapitalApportsNatureSuperieurMoitieCapital(String capitalApportsNatureSuperieurMoitieCapital) {
        this.capitalApportsNatureSuperieurMoitieCapital = capitalApportsNatureSuperieurMoitieCapital;
    }

    /**
     * Setter de l'attribut capitalDevise.
     * 
     * @param capitalDevise
     *            la nouvelle valeur de capitalDevise
     */
    public void setCapitalDevise(String capitalDevise) {
        this.capitalDevise = capitalDevise;
    }

    /**
     * Setter de l'attribut capitalMontant.
     * 
     * @param capitalMontant
     *            la nouvelle valeur de capitalMontant
     */
    public void setCapitalMontant(String capitalMontant) {
        this.capitalMontant = capitalMontant;
    }

    /**
     * Setter de l'attribut capitalVariable.
     * 
     * @param capitalVariable
     *            la nouvelle valeur de capitalVariable
     */
    public void setCapitalVariable(String capitalVariable) {
        this.capitalVariable = capitalVariable;
    }

    /**
     * Setter de l'attribut capitalVariableMinimum.
     * 
     * @param capitalVariableMinimum
     *            la nouvelle valeur de capitalVariableMinimum
     */
    public void setCapitalVariableMinimum(String capitalVariableMinimum) {
        this.capitalVariableMinimum = capitalVariableMinimum;
    }

    /**
     * Setter de l'attribut contratAppuiDateFin.
     * 
     * @param contratAppuiDateFin
     *            la nouvelle valeur de contratAppuiDateFin
     */
    public void setContratAppuiDateFin(String contratAppuiDateFin) {
        this.contratAppuiDateFin = contratAppuiDateFin;
    }

    /**
     * Setter de l'attribut contratAppuiPresence.
     * 
     * @param contratAppuiPresence
     *            la nouvelle valeur de contratAppuiPresence
     */
    public void setContratAppuiPresence(String contratAppuiPresence) {
        this.contratAppuiPresence = contratAppuiPresence;
    }

    /**
     * Setter de l'attribut dateAgrementGAEC.
     * 
     * @param dateAgrementGAEC
     *            la nouvelle valeur de dateAgrementGAEC
     */
    public void setDateAgrementGAEC(String dateAgrementGAEC) {
        this.dateAgrementGAEC = dateAgrementGAEC;
    }

    /**
     * Setter de l'attribut dateClotureExerciceSocial.
     * 
     * @param dateClotureExerciceSocial
     *            la nouvelle valeur de dateClotureExerciceSocial
     */
    public void setDateClotureExerciceSocial(String dateClotureExerciceSocial) {
        this.dateClotureExerciceSocial = dateClotureExerciceSocial;
    }

    /**
     * Setter de l'attribut dateCloturePremierExerciceSocial.
     * 
     * @param dateCloturePremierExerciceSocial
     *            la nouvelle valeur de dateCloturePremierExerciceSocial
     */
    public void setDateCloturePremierExerciceSocial(String dateCloturePremierExerciceSocial) {
        this.dateCloturePremierExerciceSocial = dateCloturePremierExerciceSocial;
    }

    /**
     * Setter de l'attribut demandeACCRE.
     * 
     * @param demandeACCRE
     *            la nouvelle valeur de demandeACCRE
     */
    public void setDemandeACCRE(String demandeACCRE) {
        this.demandeACCRE = demandeACCRE;
    }

    /**
     * Setter de l'attribut denomination.
     * 
     * @param denomination
     *            la nouvelle valeur de denomination
     */
    public void setDenomination(String denomination) {
        this.denomination = denomination;
    }

    /**
     * Setter de l'attribut duree.
     * 
     * @param duree
     *            la nouvelle valeur de duree
     */
    public void setDuree(Integer duree) {
        this.duree = duree;
    }

    /**
     * Setter de l'attribut eirl.
     * 
     * @param eirl
     *            la nouvelle valeur de eirl
     */
    public void setEirl(EirlBean eirl) {
        this.eirl = eirl;
    }

    /**
     * setEntrepriseLieeContratAppui.
     * 
     * @param entrepriseLieeContratAppui
     *            entrepriseLieeContratAppui
     */
    public void setEntrepriseLieeContratAppui(EntrepriseLieeBean entrepriseLieeContratAppui) {
        this.entrepriseLieeContratAppui = entrepriseLieeContratAppui;
    }

    /**
     * Setter de l'attribut entreprisesLieesFusionScission.
     * 
     * @param entreprisesLieesFusionScission
     *            la nouvelle valeur de entreprisesLieesFusionScission
     */
    public void setEntreprisesLieesFusionScission(List<EntrepriseLieeBean> entreprisesLieesFusionScission) {
        this.entreprisesLieesFusionScission = entreprisesLieesFusionScission;
    }

    /**
     * Setter de l'attribut entreprisesLieesPrecedentsExploitants.
     * 
     * @param entreprisesLieesPrecedentsExploitants
     *            la nouvelle valeur de entreprisesLieesPrecedentsExploitants
     */
    public void setEntreprisesLieesPrecedentsExploitants(List<EntrepriseLieeBean> entreprisesLieesPrecedentsExploitants) {
        this.entreprisesLieesPrecedentsExploitants = entreprisesLieesPrecedentsExploitants;
    }

    /**
     * Setter de l'attribut estActiviteArtisanaleOption.
     * 
     * @param estActiviteArtisanaleOption
     *            la nouvelle valeur de estActiviteArtisanaleOption
     */
    public void setEstActiviteArtisanaleOption(String estActiviteArtisanaleOption) {
        this.estActiviteArtisanaleOption = estActiviteArtisanaleOption;
    }

    /**
     * Setter de l'attribut estActiviteArtisanalePrincipale.
     * 
     * @param estActiviteArtisanalePrincipale
     *            la nouvelle valeur de estActiviteArtisanalePrincipale
     */
    public void setEstActiviteArtisanalePrincipale(String estActiviteArtisanalePrincipale) {
        this.estActiviteArtisanalePrincipale = estActiviteArtisanalePrincipale;
    }

    /**
     * Setter de l'attribut estInscritRCS.
     * 
     * @param estInscritRCS
     *            la nouvelle valeur de estInscritRCS
     */
    public void setEstInscritRCS(String estInscritRCS) {
        this.estInscritRCS = estInscritRCS;
    }

    /**
     * Setter de l'attribut estInscritRM.
     * 
     * @param estInscritRM
     *            la nouvelle valeur de estInscritRM
     */
    public void setEstInscritRM(String estInscritRM) {
        this.estInscritRM = estInscritRM;
    }

    /**
     * Setter de l'attribut estProprietaireTerresExploitees.
     * 
     * @param estProprietaireTerresExploitees
     *            la nouvelle valeur de estProprietaireTerresExploitees
     */
    public void setEstProprietaireTerresExploitees(String estProprietaireTerresExploitees) {
        this.estProprietaireTerresExploitees = estProprietaireTerresExploitees;
    }

    /**
     * Setter de l'attribut formeGroupementPastoral.
     * 
     * @param formeGroupementPastoral
     *            la nouvelle valeur de formeGroupementPastoral
     */
    public void setFormeGroupementPastoral(String formeGroupementPastoral) {
        this.formeGroupementPastoral = formeGroupementPastoral;
    }

    /**
     * Setter de l'attribut fusionScission.
     * 
     * @param fusionScission
     *            la nouvelle valeur de fusionScission
     */
    public void setFusionScission(String fusionScission) {
        this.fusionScission = fusionScission;
    }

    /**
     * Setter de l'attribut fusionScissionNombre.
     * 
     * @param fusionScissionNombre
     *            la nouvelle valeur de fusionScissionNombre
     */
    public void setFusionScissionNombre(Integer fusionScissionNombre) {
        this.fusionScissionNombre = fusionScissionNombre;
    }

    /**
     * Setter de l'attribut informationDepotFonds.
     * 
     * @param informationDepotFonds
     *            la nouvelle valeur de informationDepotFonds
     */
    public void setInformationDepotFonds(String informationDepotFonds) {
        this.informationDepotFonds = informationDepotFonds;
    }

    /**
     * Setter de l'attribut natureGerance.
     * 
     * @param natureGerance
     *            la nouvelle valeur de natureGerance
     */
    public void setNatureGerance(String natureGerance) {
        this.natureGerance = natureGerance;
    }

    /**
     * Setter de l'attribut natureGeranceSocieteAssociee.
     * 
     * @param natureGeranceSocieteAssociee
     *            la nouvelle valeur de natureGeranceSocieteAssociee
     */
    public void setNatureGeranceSocieteAssociee(String natureGeranceSocieteAssociee) {
        this.natureGeranceSocieteAssociee = natureGeranceSocieteAssociee;
    }

    /**
     * Setter de l'attribut personnePouvoirNombre.
     * 
     * @param personnePouvoirNombre
     *            la nouvelle valeur de personnePouvoirNombre
     */
    public void setPersonnePouvoirNombre(Integer personnePouvoirNombre) {
        this.personnePouvoirNombre = personnePouvoirNombre;
    }

    /**
     * Setter de l'attribut personnePouvoirPresence.
     * 
     * @param personnePouvoirPresence
     *            la nouvelle valeur de personnePouvoirPresence
     */
    public void setPersonnePouvoirPresence(String personnePouvoirPresence) {
        this.personnePouvoirPresence = personnePouvoirPresence;
    }

    /**
     * Setter de l'attribut regimeFiscalEirl.
     * 
     * @param regimeFiscalEirl
     *            la nouvelle valeur de regimeFiscalEirl
     */
    public void setRegimeFiscalEirl(RegimeFiscalBean regimeFiscalEirl) {
        this.regimeFiscalEirl = regimeFiscalEirl;
    }

    /**
     * Setter de l'attribut regimeFiscalEirlSecondaire.
     * 
     * @param regimeFiscalEirlSecondaire
     *            la nouvelle valeur de regimeFiscalEirlSecondaire
     */
    public void setRegimeFiscalEirlSecondaire(RegimeFiscalBean regimeFiscalEirlSecondaire) {
        this.regimeFiscalEirlSecondaire = regimeFiscalEirlSecondaire;
    }

    /**
     * Setter de l'attribut regimeFiscalEtablissement.
     * 
     * @param regimeFiscalEtablissement
     *            la nouvelle valeur de regimeFiscalEtablissement
     */
    public void setRegimeFiscalEtablissement(RegimeFiscalBean regimeFiscalEtablissement) {
        this.regimeFiscalEtablissement = regimeFiscalEtablissement;
    }

    /**
     * Setter de l'attribut regimeFiscalEtablissementSecondaire.
     * 
     * @param regimeFiscalEtablissementSecondaire
     *            la nouvelle valeur de regimeFiscalEtablissementSecondaire
     */
    public void setRegimeFiscalEtablissementSecondaire(RegimeFiscalBean regimeFiscalEtablissementSecondaire) {
        this.regimeFiscalEtablissementSecondaire = regimeFiscalEtablissementSecondaire;
    }

    /**
     * Setter de l'attribut sigle.
     * 
     * @param sigle
     *            la nouvelle valeur de sigle
     */
    public void setSigle(String sigle) {
        this.sigle = sigle;
    }

    /**
     * Setter de l'attribut statutLegalParticulier.
     * 
     * @param statutLegalParticulier
     *            la nouvelle valeur de statutLegalParticulier
     */
    public void setStatutLegalParticulier(String statutLegalParticulier) {
        this.statutLegalParticulier = statutLegalParticulier;
    }

    /**
     * Setter de l'attribut statutsTypes.
     * 
     * @param statutsTypes
     *            la nouvelle valeur de statutsTypes
     */
    public void setStatutsTypes(String statutsTypes) {
        this.statutsTypes = statutsTypes;
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see fr.guichetentreprises.vue.AbstractEntrepriseBean#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see fr.guichetentreprises.vue.IEntrepriseEtabFactory#createNewEtablissement()
     */
    @Override
    public EtablissementBean createNewEtablissement() {

        return new EtablissementBean();
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see fr.guichetentreprises.vue.IEntrepriseEtabFactory#createNewEntrepriseLiee()
     */
    @Override
    public EntrepriseLieeBean createNewEntrepriseLiee() {

        return new EntrepriseLieeBean();
    }

}
