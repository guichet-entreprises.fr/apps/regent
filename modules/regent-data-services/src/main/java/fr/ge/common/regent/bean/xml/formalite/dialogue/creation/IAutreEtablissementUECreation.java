package fr.ge.common.regent.bean.xml.formalite.dialogue.creation;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

import fr.ge.common.regent.bean.xml.formalite.IFormalite;
import fr.ge.common.regent.bean.xml.transverse.IAdresse;

/**
 * Le Interface de l'entité de Autre Etablissement situé dans l'union europeen.
 */
@ResourceXPath("/formalite")
public interface IAutreEtablissementUECreation extends IFormalite {

    /**
     * Getter les activites de l'établissment.
     * 
     * @return la valeur de activites
     */
    @FieldXPath(value = "activites")
    String getActivites();

    /**
     * Getter de lieu d'immatriculation de l'établissement .
     * 
     * @return la valeur de lieuImmatriculation
     */
    @FieldXPath(value = "lieuImmatriculation")
    String getLieuImmatriculation();

    /**
     * Getter de numero d'immatriculation
     * 
     * @return la valeur de numeroImmatriculation
     */
    @FieldXPath(value = "numeroImmatriculation")
    String getNumeroImmatriculation();

    /**
     * Setter de l'attribut activites.
     * 
     * @param activites
     *            la nouvelle valeur de activites
     */
    void setActivites(String activites);

    /**
     * Getter le adresse de l'établissement située dans l'union europeen.
     *
     * @return le adresse
     */
    @FieldXPath(value = "adresse")
    IAdresse getAdresse();

    /**
     * New adresse.
     *
     * @return le adresse de l'établissement
     */
    IAdresse newAdresse();

    /**
     * Setter de adresse de l'établissement .
     *
     * @param adresse
     *            le nouveau adresse
     */
    void setAdresse(IAdresse adresse);

    /**
     * Setter de lieu d'immatriculation de l'établissement
     * 
     * @param lieuImmatriculation
     *            la nouvelle valeur de lieuImmatriculation
     */
    void setLieuImmatriculation(String lieuImmatriculation);

    /**
     * Setter de numéro d'immatriculation de l'établissement
     * 
     * @param numeroImmatriculation
     *            la nouvelle valeur de numeroImmatriculation
     */
    void setNumeroImmatriculation(String numeroImmatriculation);

    /**
     * Get le id technique.
     *
     * @return the idTechnique
     */
    @FieldXPath(value = "idTechnique")
    String getIdTechnique();

    /**
     * Set le id technique.
     *
     * @param idTechnique
     *            the idTechnique to set
     */
    void setIdTechnique(String idTechnique);
}
