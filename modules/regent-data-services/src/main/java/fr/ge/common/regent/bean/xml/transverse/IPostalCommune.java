package fr.ge.common.regent.bean.xml.transverse;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IPostalCommune.
 */
@ResourceXPath("/postalCommune")
public interface IPostalCommune {

  /**
   * Getter de l'attribut codePostal.
   * 
   * @return la valeur de codePostal
   */
  @FieldXPath("codePostal")
  public String getCodePostal();

  /**
   * Getter de l'attribut commune.
   * 
   * @return la valeur de commune
   */
  @FieldXPath("commune")
  public String getCommune();

  /**
   * Setter de l'attribut codePostal.
   * 
   * @param codePostal
   *          la nouvelle valeur de codePostal
   */
  public void setCodePostal(String codePostal);

  /**
   * Setter de l'attribut commune.
   * 
   * @param commune
   *          la nouvelle valeur de commune
   */
  public void setCommune(String commune);

}
