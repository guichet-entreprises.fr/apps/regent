package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface Specification.
 */
@ResourceXPath("/Specification")
public interface Specification {

  /**
   * Get le nom service.
   *
   * @return le nom service
   */
  @FieldXPath("NomService")
  String getNomService();

  /**
   * Set le nom service.
   *
   * @param NomService
   *          le nouveau nom service
   */
  void setNomService(String NomService);

  /**
   * Get le version service.
   *
   * @return le version service
   */
  @FieldXPath("VersionService")
  String getVersionService();

  /**
   * Set le version service.
   *
   * @param VersionService
   *          le nouveau version service
   */
  void setVersionService(String VersionService);

}
