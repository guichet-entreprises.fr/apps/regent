package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IC403.
 */
@ResourceXPath("/C40.3")
public interface IC403 {

  /**
   * Get le c4033.
   *
   * @return le c4033
   */
  @FieldXPath("C40.3.3")
  String getC4033();

  /**
   * Set le c4033.
   *
   * @param C4033
   *          le nouveau c4033
   */
  void setC4033(String C4033);

  /**
   * Get le c4034.
   *
   * @return le c4034
   */
  @FieldXPath("C40.3.4")
  String getC4034();

  /**
   * Set le c4034.
   *
   * @param C4034
   *          le nouveau c4034
   */
  void setC4034(String C4034);

  /**
   * Get le c4035.
   *
   * @return le c4035
   */
  @FieldXPath("C40.3.5")
  String getC4035();

  /**
   * Set le c4035.
   *
   * @param C4035
   *          le nouveau c4035
   */
  void setC4035(String C4035);

  /**
   * Get le c4036.
   *
   * @return le c4036
   */
  @FieldXPath("C40.3.6")
  String getC4036();

  /**
   * Set le c4036.
   *
   * @param C4036
   *          le nouveau c4036
   */
  void setC4036(String C4036);

  /**
   * Get le c4037.
   *
   * @return le c4037
   */
  @FieldXPath("C40.3.7")
  String getC4037();

  /**
   * Set le c4037.
   *
   * @param C4037
   *          le nouveau c4037
   */
  void setC4037(String C4037);

  /**
   * Get le c4038.
   *
   * @return le c4038
   */
  @FieldXPath("C40.3.8")
  String getC4038();

  /**
   * Set le c4038.
   *
   * @param C4038
   *          le nouveau c4038
   */
  void setC4038(String C4038);

  /**
   * Get le c4039.
   *
   * @return le c4039
   */
  @FieldXPath("C40.3.9")
  String getC4039();

  /**
   * Set le c4039.
   *
   * @param C4039
   *          le nouveau c4039
   */
  void setC4039(String C4039);

  /**
   * Get le c40310.
   *
   * @return le c40310
   */
  @FieldXPath("C40.3.10")
  String getC40310();

  /**
   * Set le c40310.
   *
   * @param C40310
   *          le nouveau c40310
   */
  void setC40310(String C40310);

  /**
   * Get le c40311.
   *
   * @return le c40311
   */
  @FieldXPath("C40.3.11")
  String getC40311();

  /**
   * Set le c40311.
   *
   * @param C40311
   *          le nouveau c40311
   */
  void setC40311(String C40311);

  /**
   * Get le c40312.
   *
   * @return le c40312
   */
  @FieldXPath("C40.3.12")
  String getC40312();

  /**
   * Set le c40312.
   *
   * @param C40312
   *          le nouveau c40312
   */
  void setC40312(String C40312);

  /**
   * Get le c40313.
   *
   * @return le c40313
   */
  @FieldXPath("C40.3.13")
  String getC40313();

  /**
   * Set le c40313.
   *
   * @param C40313
   *          le nouveau c40313
   */
  void setC40313(String C40313);

  /**
   * Get le c40314.
   *
   * @return le c40314
   */
  @FieldXPath("C40.3.14")
  String getC40314();

  /**
   * Set le c40314.
   *
   * @param C40314
   *          le nouveau c40314
   */
  void setC40314(String C40314);

}
