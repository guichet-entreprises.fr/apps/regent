package fr.ge.common.regent.xml.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Le Interface Indexed.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD })
public @interface Indexed {

  /**
   * Value.
   *
   * @return le string
   */
  String value();
}
