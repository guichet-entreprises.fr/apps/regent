package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * L'interface JIM.
 */
@ResourceXPath("/JIM")
public interface IJIM {

  /**
   * Get le j00.
   *
   * @return le j00
   */
  @FieldXPath("J00")
  String getJ00();

  /**
   * Set le j00.
   *
   * @param j00
   *          le nouveau j00
   */
  void setJ00(String j00);
}
