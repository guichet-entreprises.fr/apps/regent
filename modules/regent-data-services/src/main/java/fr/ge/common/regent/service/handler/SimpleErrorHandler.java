package fr.ge.common.regent.service.handler;

import java.util.LinkedList;
import java.util.List;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * Gestionnaire d'erreur pour la validation XSD.
 * 
 */
public class SimpleErrorHandler implements ErrorHandler {

    /** Le exceptions. */
    final List<SAXParseException> exceptions = new LinkedList<SAXParseException>();

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see org.xml.sax.ErrorHandler#warning(org.xml.sax.SAXParseException)
     */
    @Override
    public void warning(SAXParseException exception) throws SAXException {
        this.exceptions.add(exception);
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see org.xml.sax.ErrorHandler#error(org.xml.sax.SAXParseException)
     */
    @Override
    public void error(SAXParseException exception) throws SAXException {
        this.exceptions.add(exception);
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see org.xml.sax.ErrorHandler#fatalError(org.xml.sax.SAXParseException)
     */
    @Override
    public void fatalError(SAXParseException exception) throws SAXException {
        this.exceptions.add(exception);
    }

    /**
     * Indique si des erreurs sont survenues après validation.
     * 
     * @return true s'il y a eu des erreurs false sinon
     */
    public boolean hasError() {
        return this.exceptions.size() > 0;
    }

    /**
     * Présente de manière lisible les erreurs enregistrées lors de la
     * validation.
     * 
     * @return String
     */
    public String printErrors() {
        StringBuilder s = new StringBuilder();
        for (SAXParseException e : this.exceptions) {
            s.append(e.getMessage() + "\n");
        }

        return s.toString();
    }

}
