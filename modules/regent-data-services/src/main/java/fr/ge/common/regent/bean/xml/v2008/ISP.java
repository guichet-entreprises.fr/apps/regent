package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface ACE.
 */
@ResourceXPath("/ISP")
public interface ISP {

  /**
   * Get le P90.
   *
   * @return le P90
   */
  @FieldXPath("P90")
  String getP90();

  /**
   * Set le P90.
   *
   * @param P90
   *          le nouveau P90
   */
  void setP90(String P90);

  /**
   * Get le P91.
   *
   * @return le P91
   */
  @FieldXPath("P91")
  String getP91();

  /**
   * Set le P91.
   *
   * @param P91
   *          le nouveau P91
   */
  void setP91(String P91);

}
