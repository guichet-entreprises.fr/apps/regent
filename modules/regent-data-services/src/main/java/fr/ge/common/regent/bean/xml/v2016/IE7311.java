package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

@ResourceXPath("E73.11")
public interface IE7311 {

		

	  /**
	   * Get le e73111.
	   *
	   * @return le e731
	   */
	  @FieldXPath("E73.111")
	  String getE73111();

	  /**
	   * Set le e73111
	   *
	   * @param E73111
	   *          le nouveau e73111
	   */
	  void setE73111(String E73111);
	  
	  
	  
	  /**
	   * Get le e73112.
	   *
	   * @return le e73112
	   */
	  @FieldXPath("E73.112")
	  String getE73112();

	  /**
	   * Set le e73112
	   *
	   * @param E73112
	   *          le nouveau e73112
	   */
	  void setE73112(String E73112);


}
