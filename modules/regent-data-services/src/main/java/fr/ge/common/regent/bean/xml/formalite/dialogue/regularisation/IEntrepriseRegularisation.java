package fr.ge.common.regent.bean.xml.formalite.dialogue.regularisation;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

import fr.ge.common.regent.bean.xml.formalite.IFormalite;

/**
 * La classe XML Field EntrepriseBean régularisation.
 * 
 * @author roveda
 * 
 */
@ResourceXPath("/formalite")
public interface IEntrepriseRegularisation extends IFormalite {

    /** La constante MODEL_VERSION. */
    static final int MODEL_VERSION = 1;

    /**
     * Get le exercice double activite.
     *
     * @return le exercice double activite
     */
    @FieldXPath(value = "exerciceDoubleActivite")
    String getExerciceDoubleActivite();

    /**
     * Get le siren.
     *
     * @return le siren
     */
    @FieldXPath(value = "siren")
    String getSiren();

    /**
     * Get le eirl declaration.
     *
     * @return le eirl declaration
     */
    @FieldXPath(value = "eirlDeclaration")
    String getEirlDeclaration();

    /**
     * Get le eirl registre depot.
     *
     * @return le eirl registre depot
     */
    @FieldXPath(value = "eirlRegistreDepot")
    String getEirlRegistreDepot();

    /**
     * Get le eirl registre lieu.
     *
     * @return le eirl registre lieu
     */
    @FieldXPath(value = "eirlRegistreLieu")
    String getEirlRegistreLieu();

    /**
     * Get le insaisissabilite declaration.
     *
     * @return le insaisissabilite declaration
     */
    @FieldXPath(value = "insaisissabiliteDeclaration")
    String getInsaisissabiliteDeclaration();

    /**
     * Get le insaisissabilite publication.
     *
     * @return le insaisissabilite publication
     */
    @FieldXPath(value = "insaisissabilitePublication")
    String getInsaisissabilitePublication();

    /**
     * Get le autre etablissement ue.
     *
     * @return le autre etablissement ue
     */
    @FieldXPath(value = "autreEtablissementUE")
    String getAutreEtablissementUE();

    /**
     * Get le autre etablissement ue nombre.
     *
     * @return le autre etablissement ue nombre
     */
    @FieldXPath(value = "autreEtablissementUENombre")
    Integer getAutreEtablissementUENombre();

    /**
     * Get le adresse entreprise pp situation.
     *
     * @return le adresse entreprise pp situation
     */
    @FieldXPath(value = "adresseEntreprisePPSituation")
    String getAdresseEntreprisePPSituation();

    /**
     * Get le etablissement.
     *
     * @return le etablissement
     */
    @FieldXPath(value = "etablissement")
    IEtablissementRegularisation getEtablissement();

    /**
     * New etablissement.
     *
     * @return le i etablissement regularisation
     */
    IEtablissementRegularisation newEtablissement();

    /**
     * Get le entreprise liee domiciliation.
     *
     * @return le entreprise liee domiciliation
     */
    @FieldXPath(value = "entrepriseLieeDomiciliation")
    IEntrepriseLieeRegularisation getEntrepriseLieeDomiciliation();

    /**
     * New entreprise liee domiciliation.
     *
     * @return le i entreprise liee regularisation
     */
    IEntrepriseLieeRegularisation newEntrepriseLieeDomiciliation();

    /**
     * Get le entreprise liee loueur mandant.
     *
     * @return le entreprise liee loueur mandant
     */
    @FieldXPath(value = "entrepriseLieeLoueurMandant")
    IEntrepriseLieeRegularisation getEntrepriseLieeLoueurMandant();

    /**
     * New entreprise liee loueur mandant.
     *
     * @return le i entreprise liee regularisation
     */
    IEntrepriseLieeRegularisation newEntrepriseLieeLoueurMandant();

    // Liste d'objets complexes

    /**
     * Get le entreprise liees precedents exploitants.
     *
     * @return le entreprise liees precedents exploitants
     */
    @FieldXPath(value = "entrepriseLieesPrecedentsExploitants")
    IEntrepriseLieeRegularisation[] getEntreprisesLieesPrecedentsExploitants();

    /**
     * Ajoute le to entreprise liees precedents exploitants.
     *
     * @return le i entreprise liee regularisation
     */
    IEntrepriseLieeRegularisation addToEntreprisesLieesPrecedentsExploitants();

    /**
     * Set le entreprise liees precedents exploitants.
     *
     * @param entrepriseLieesPrecedentsExploitants
     *            le nouveau entreprise liees precedents exploitants
     */
    void setEntreprisesLieesPrecedentsExploitants(IEntrepriseLieeRegularisation[] entrepriseLieesPrecedentsExploitants);

    /**
     * Retire le from entreprise liees precedents exploitants.
     *
     * @param entrepriseLieesPrecedentsExploitants
     *            le entreprise liees precedents exploitants
     */
    void removeFromEntrepriseLieesPrecedentsExploitants(IEntrepriseLieeRegularisation entrepriseLieesPrecedentsExploitants);

    /**
     * Get le dirigeants.
     *
     * @return le dirigeants
     */
    @FieldXPath(value = "dirigeants")
    IDirigeantRegularisation[] getDirigeants();

    /**
     * Ajoute le to dirigeants.
     *
     * @return le i dirigeant regularisation
     */
    IDirigeantRegularisation addToDirigeants();

    /**
     * Set le dirigeants.
     *
     * @param dirigeants
     *            le nouveau dirigeants
     */
    void setDirigeants(IDirigeantRegularisation[] dirigeants);

    /**
     * Retire le from dirigeants.
     *
     * @param dirigeants
     *            le dirigeants
     */
    void removeFromDirigeants(IDirigeantRegularisation dirigeants);

    /**
     * Get le etablissements ue.
     *
     * @return le etablissements ue
     */
    @FieldXPath(value = "etablissementsUE")
    IAutreEtablissementUE[] getEtablissementsUE();

    /**
     * Ajoute le to etablissements ue.
     *
     * @return le i autre etablissement ue
     */
    IAutreEtablissementUE addToEtablissementsUE();

    /**
     * Set le etablissements ue.
     *
     * @param etablissementsUE
     *            le nouveau etablissements ue
     */
    void setEtablissementsUE(IAutreEtablissementUE[] etablissementsUE);

    /**
     * Retire le from etablissements ue.
     *
     * @param etablissementsUE
     *            le etablissements ue
     */
    void removeFromEtablissementsUE(IAutreEtablissementUE etablissementsUE);

    /**
     * Get le personne liee.
     *
     * @return le personne liee
     */
    @FieldXPath(value = "personneLiee")
    IPersonneLieeRegularisation[] getPersonneLiee();

    /**
     * Ajoute le to personne liee.
     *
     * @return le i personne liee regularisation
     */
    IPersonneLieeRegularisation addToPersonneLiee();

    /**
     * Set le personne liee.
     *
     * @param personneLiee
     *            le nouveau personne liee
     */
    void setPersonneLiee(IPersonneLieeRegularisation[] personneLiee);

    /**
     * Retire le from personne liee.
     *
     * @param personneLiee
     *            le personne liee
     */
    void removeFromPersonneLiee(IPersonneLieeRegularisation personneLiee);

    // fin liste objet complexe

    /**
     * Get le forme juridique.
     *
     * @return le forme juridique
     */
    @FieldXPath(value = "formeJuridique")
    String getFormeJuridique();

    /**
     * Get le associe unique.
     *
     * @return le associe unique
     */
    @FieldXPath(value = "associeUnique")
    String getAssocieUnique();

    /**
     * Set le exercice double activite.
     *
     * @param exerciceDoubleActivite
     *            le nouveau exercice double activite
     */
    void setExerciceDoubleActivite(String exerciceDoubleActivite);

    /**
     * Set le siren.
     *
     * @param siren
     *            le nouveau siren
     */
    void setSiren(String siren);

    /**
     * Set le eirl declaration.
     *
     * @param eirlDeclaration
     *            le nouveau eirl declaration
     */
    void setEirlDeclaration(String eirlDeclaration);

    /**
     * Set le eirl registre depot.
     *
     * @param eirlRegistreDepot
     *            le nouveau eirl registre depot
     */
    void setEirlRegistreDepot(String eirlRegistreDepot);

    /**
     * Set le eirl registre lieu.
     *
     * @param eirlRegistreLieu
     *            le nouveau eirl registre lieu
     */
    void setEirlRegistreLieu(String eirlRegistreLieu);

    /**
     * Set le insaisissabilite declaration.
     *
     * @param insaisissabiliteDeclaration
     *            le nouveau insaisissabilite declaration
     */
    void setInsaisissabiliteDeclaration(String insaisissabiliteDeclaration);

    /**
     * Set le insaisissabilite publication.
     *
     * @param insaisissabilitePublication
     *            le nouveau insaisissabilite publication
     */
    void setInsaisissabilitePublication(String insaisissabilitePublication);

    /**
     * Set le autre etablissement ue.
     *
     * @param autreEtablissementUE
     *            le nouveau autre etablissement ue
     */
    void setAutreEtablissementUE(String autreEtablissementUE);

    /**
     * Set le autre etablissement ue nombre.
     *
     * @param autreEtablissementUENombre
     *            le nouveau autre etablissement ue nombre
     */
    void setAutreEtablissementUENombre(Integer autreEtablissementUENombre);

    /**
     * Set le adresse entreprise pp situation.
     *
     * @param adresseEntreprisePPSituation
     *            le nouveau adresse entreprise pp situation
     */
    void setAdresseEntreprisePPSituation(String adresseEntreprisePPSituation);

    /**
     * Set le etablissement.
     *
     * @param etablissement
     *            le nouveau etablissement
     */
    void setEtablissement(IEtablissementRegularisation etablissement);

    /**
     * Set le entreprise liee domiciliation.
     *
     * @param entrepriseLieeDomiciliation
     *            le nouveau entreprise liee domiciliation
     */
    void setEntrepriseLieeDomiciliation(IEntrepriseLieeRegularisation entrepriseLieeDomiciliation);

    /**
     * Set le entreprise liee loueur mandant.
     *
     * @param entrepriseLieeLoueurMandant
     *            le nouveau entreprise liee loueur mandant
     */
    void setEntrepriseLieeLoueurMandant(IEntrepriseLieeRegularisation entrepriseLieeLoueurMandant);

    /**
     * Set le forme juridique.
     *
     * @param formeJuridique
     *            le nouveau forme juridique
     */
    void setFormeJuridique(String formeJuridique);

    /**
     * Set le associe unique.
     *
     * @param associeUnique
     *            le nouveau associe unique
     */
    void setAssocieUnique(String associeUnique);
}
