package fr.ge.common.regent.bean.formalite.dialogue.cessation;

import fr.ge.common.regent.bean.formalite.dialogue.AbstractDirigeantCommunBean;

/**
 * Le Class DirigeantCessationBean.
 */
public class DirigeantCessationBean extends AbstractDirigeantCommunBean implements IFormaliteCessationVue {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 7982197323184350705L;

    /** Le id technique. */
    private String idTechnique;

    /**
     * Get le id technique.
     *
     * @return the idTechnique
     */
    public String getIdTechnique() {
        return idTechnique;
    }

    /**
     * Set le id technique.
     *
     * @param idTechnique
     *            the idTechnique to set
     */
    public void setIdTechnique(String idTechnique) {
        this.idTechnique = idTechnique;
    }

    /** Le pp organisme conventionne. */
    private String ppOrganismeConventionne;

    /** Le departement organisme conventionne. */
    private String departementOrganismeConventionne;

    /**
     * Get le pp organisme conventionne.
     *
     * @return le pp organisme conventionne
     */
    public String getPpOrganismeConventionne() {
        return this.ppOrganismeConventionne;
    }

    /**
     * Set le pp organisme conventionne.
     *
     * @param ppOrganismeConventionne
     *            le nouveau pp organisme conventionne
     */
    public void setPpOrganismeConventionne(String ppOrganismeConventionne) {
        this.ppOrganismeConventionne = ppOrganismeConventionne;
    }

    /**
     * Get le departement organisme conventionne.
     *
     * @return le departement organisme conventionne
     */
    public String getDepartementOrganismeConventionne() {
        return this.departementOrganismeConventionne;
    }

    /**
     * Set le departement organisme conventionne.
     *
     * @param departementOrganismeConventionne
     *            le nouveau departement organisme conventionne
     */
    public void setDepartementOrganismeConventionne(String departementOrganismeConventionne) {
        this.departementOrganismeConventionne = departementOrganismeConventionne;
    }

}
