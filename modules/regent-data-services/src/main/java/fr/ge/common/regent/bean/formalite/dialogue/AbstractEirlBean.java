package fr.ge.common.regent.bean.formalite.dialogue;

import fr.ge.common.regent.bean.formalite.FormaliteVueBean;

/**
 * Abstract Eirl Bean. Nécessaire pour XML Regent : balises P71/P72/P73
 * 
 * @author $Author: hhichri $
 * @version $Revision: 0 $
 */
public abstract class AbstractEirlBean extends FormaliteVueBean {

    /** serialVersionUID. **/
    private static final long serialVersionUID = -7712086414493080391L;

    /** Le modif nouvelle denomination eirl. */
    private String modifNouvelleDenominationEIRL;

    /** Le modif date cloture exercice comptable. */
    private String modifDateClotureExerciceComptable;

    /** Le modif objet. */
    private String modifObjet;

    /**
     * Get le modif nouvelle denomination eirl.
     *
     * @return the modifNouvelleDenominationEIRL
     */
    public String getModifNouvelleDenominationEIRL() {
        return modifNouvelleDenominationEIRL;
    }

    /**
     * Set le modif nouvelle denomination eirl.
     *
     * @param modifNouvelleDenominationEIRL
     *            the modifNouvelleDenominationEIRL to set
     */
    public void setModifNouvelleDenominationEIRL(String modifNouvelleDenominationEIRL) {
        this.modifNouvelleDenominationEIRL = modifNouvelleDenominationEIRL;
    }

    /**
     * Get le modif date cloture exercice comptable.
     *
     * @return the modifDateClotureExerciceComptable
     */
    public String getModifDateClotureExerciceComptable() {
        return modifDateClotureExerciceComptable;
    }

    /**
     * Set le modif date cloture exercice comptable.
     *
     * @param modifDateClotureExerciceComptable
     *            the modifDateClotureExerciceComptable to set
     */
    public void setModifDateClotureExerciceComptable(String modifDateClotureExerciceComptable) {
        this.modifDateClotureExerciceComptable = modifDateClotureExerciceComptable;
    }

    /**
     * Get le modif objet.
     *
     * @return the modifObjet
     */
    public String getModifObjet() {
        return modifObjet;
    }

    /**
     * Set le modif objet.
     *
     * @param modifObjet
     *            the modifObjet to set
     */
    public void setModifObjet(String modifObjet) {
        this.modifObjet = modifObjet;
    }
}
