package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface CAS.
 */
@ResourceXPath("/CAS")
public interface CAS {

  /**
   * Get le a42.
   *
   * @return le a42
   */
  @FieldXPath("A42")
  IA42[] getA42();

  /**
   * Ajoute le to a42.
   *
   * @return le i a42
   */
  IA42 addToA42();

  /**
   * Set le a42.
   *
   * @param A42
   *          le nouveau a42
   */
  void setA42(IA42[] A42);

  /**
   * Get le a44.
   *
   * @return le a44
   */
  @FieldXPath("A44")
  String getA44();

  /**
   * Set le a44.
   *
   * @param A44
   *          le nouveau a44
   */
  void setA44(String A44);
}
