package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface A24.
 */
@ResourceXPath("/A24")
public interface IA24 {

  /**
   * Get le A241.
   *
   * @return le A241
   */
  @FieldXPath("A24.1")
  String getA241();

  /**
   * Set le A241.
   *
   * @param A241
   *          le nouveau A241
   */
  void setA241(String A241);

  /**
   * Get le A242.
   *
   * @return le A242
   */
  @FieldXPath("A24.2")
  String getA242();

  /**
   * Set le A242.
   *
   * @param A242
   *          le nouveau A242
   */
  void setA242(String A242);

  /**
   * Get le A243.
   *
   * @return le A243
   */
  @FieldXPath("A24.3")
  String getA243();

  /**
   * Set le A243.
   *
   * @param A243
   *          le nouveau A243
   */
  void setA243(String A243);

}
