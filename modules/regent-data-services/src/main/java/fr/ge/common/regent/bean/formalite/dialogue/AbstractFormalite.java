package fr.ge.common.regent.bean.formalite.dialogue;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import fr.ge.common.regent.bean.formalite.FormaliteVueBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.AdresseBean;

/**
 * Le Class AbstractFormalite.
 * 
 * @param <T>
 *            le type generique
 */
public abstract class AbstractFormalite<T extends AbstractEntrepriseBean> extends FormaliteVueBean implements IFormalite {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -2850877139634716057L;

    /** Le signataire qualite. */
    private String signataireQualite;

    /** Le observations. */
    private String observations;

    /** Le correspondance destinataire. */
    private String correspondanceDestinataire;

    /** Le correspondance adresse. */
    private AdresseBean correspondanceAdresse = new AdresseBean();

    /** Le telephone1. */
    private String telephone1;

    /** Le telephone2. */
    private String telephone2;

    /** Le fax. */
    private String fax;

    /** Le courriel. */
    private String courriel;

    /** Le signataire nom. */
    private String signataireNom;

    /** Le adresse signature. */
    private AdresseBean adresseSignature = new AdresseBean();

    /** Le nonDiffusionInformation. */
    private String nonDiffusionInformation;

    /** Le signature. */
    private String signature;

    /** Le signature lieu. */
    private String signatureLieu;

    /** Le signature date. */
    private String signatureDate;

    /** Le entreprise. */
    private T entreprise;

    /** Le numero formalite. */
    private String numeroFormalite;

    /** Le type personne. */
    private String typePersonne;

    /** Le evenement. */
    private String evenement;

    /** Le reseau cfe. */
    private String reseauCFE;

    /** Le dossier id. */
    private Long dossierId;

    /** Le est agent commercial. */
    private boolean estAgentCommercial;

    /**
     * Cree le new entreprise.
     * 
     * @return le t
     */
    public abstract T createNewEntreprise();

    /**
     * Instancie un nouveau abstract formalite.
     */
    public AbstractFormalite() {
        super();
        if (entreprise == null) {
            entreprise = createNewEntreprise();
        }
    }

    /**
     * Get le reseau cfe.
     * 
     * @return the reseauCFE
     */
    public String getReseauCFE() {
        return this.reseauCFE;
    }

    /**
     * Set le reseau cfe.
     * 
     * @param reseauCFE
     *            the reseauCFE to set
     */
    public void setReseauCFE(final String reseauCFE) {
        this.reseauCFE = reseauCFE;
    }

    /**
     * Setter de l'attribut estAgentCommercial.
     * 
     * @param estAgentCommercial
     *            la nouvelle valeur de estAgentCommercial
     */
    public void setEstAgentCommercial(final boolean estAgentCommercial) {
        this.estAgentCommercial = estAgentCommercial;
    }

    /**
     * Getter de l'attribut estAgentCommercial.
     * 
     * @return la valeur de estAgentCommercial
     */
    public boolean isEstAgentCommercial() {
        return this.estAgentCommercial;
    }

    /**
     * Get le evenement.
     * 
     * @return the evenement
     */
    public String getEvenement() {
        return this.evenement;
    }

    /**
     * Set le evenement.
     * 
     * @param evenement
     *            the evenement to set
     */
    public void setEvenement(final String evenement) {
        this.evenement = evenement;
    }

    /**
     * Get le type personne.
     * 
     * @return the typePersonne
     */
    public String getTypePersonne() {
        return this.typePersonne;
    }

    /**
     * Set le type personne.
     * 
     * @param typePersonne
     *            the typePersonne to set
     */
    public void setTypePersonne(final String typePersonne) {
        this.typePersonne = typePersonne;
    }

    /**
     * Get le entreprise.
     * 
     * @return the entreprise
     */
    public T getEntreprise() {
        return this.entreprise;
    }

    /**
     * Set le entreprise.
     * 
     * @param entreprise
     *            the entreprise to set
     */
    public void setEntreprise(final T entreprise) {
        this.entreprise = entreprise;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    /**
     * Get le observations.
     * 
     * @return the observations
     */
    public String getObservations() {
        return this.observations;
    }

    /**
     * Set le observations.
     * 
     * @param observations
     *            the observations to set
     */
    public void setObservations(final String observations) {
        this.observations = observations;
    }

    /**
     * Get le correspondance destinataire.
     * 
     * @return the correspondanceDestinataire
     */
    public String getCorrespondanceDestinataire() {
        return this.correspondanceDestinataire;
    }

    /**
     * Set le correspondance destinataire.
     * 
     * @param correspondanceDestinataire
     *            the correspondanceDestinataire to set
     */
    public void setCorrespondanceDestinataire(final String correspondanceDestinataire) {
        this.correspondanceDestinataire = correspondanceDestinataire;
    }

    /**
     * Get le telephone1.
     * 
     * @return the telephone1
     */
    public String getTelephone1() {
        return this.telephone1;
    }

    /**
     * Set le telephone1.
     * 
     * @param telephone1
     *            the telephone1 to set
     */
    public void setTelephone1(final String telephone1) {
        this.telephone1 = telephone1;
    }

    /**
     * Get le telephone2.
     * 
     * @return the telephone2
     */
    public String getTelephone2() {
        return this.telephone2;
    }

    /**
     * Set le telephone2.
     * 
     * @param telephone2
     *            the telephone2 to set
     */
    public void setTelephone2(final String telephone2) {
        this.telephone2 = telephone2;
    }

    /**
     * Get le fax.
     * 
     * @return the fax
     */
    public String getFax() {
        return this.fax;
    }

    /**
     * Set le fax.
     * 
     * @param fax
     *            the fax to set
     */
    public void setFax(final String fax) {
        this.fax = fax;
    }

    /**
     * Get le courriel.
     * 
     * @return the courriel
     */
    public String getCourriel() {
        return this.courriel;
    }

    /**
     * Set le courriel.
     * 
     * @param courriel
     *            the courriel to set
     */
    public void setCourriel(final String courriel) {
        this.courriel = courriel;
    }

    /**
     * Get le signataire nom.
     * 
     * @return the signataireNom
     */
    public String getSignataireNom() {
        return this.signataireNom;
    }

    /**
     * Set le signataire nom.
     * 
     * @param signataireNom
     *            the signataireNom to set
     */
    public void setSignataireNom(final String signataireNom) {
        this.signataireNom = signataireNom;
    }

    /**
     * Get le signature.
     * 
     * @return the signature
     */
    public String getSignature() {
        return this.signature;
    }

    /**
     * Set le signature.
     * 
     * @param signature
     *            the signature to set
     */
    public void setSignature(final String signature) {
        this.signature = signature;
    }

    /**
     * Accesseur sur l'attribut {@link #nonDiffusionInformation}.
     *
     * @return String nonDiffusionInformation
     */
    public String getNonDiffusionInformation() {
        return nonDiffusionInformation;
    }

    /**
     * Mutateur sur l'attribut {@link #nonDiffusionInformation}.
     *
     * @param nonDiffusionInformation
     *            la nouvelle valeur de l'attribut nonDiffusionInformation
     */
    public void setNonDiffusionInformation(final String nonDiffusionInformation) {
        this.nonDiffusionInformation = nonDiffusionInformation;
    }

    /**
     * Get le signature lieu.
     * 
     * @return the signatureLieu
     */
    public String getSignatureLieu() {
        return this.signatureLieu;
    }

    /**
     * Set le signature lieu.
     * 
     * @param signatureLieu
     *            the signatureLieu to set
     */
    public void setSignatureLieu(final String signatureLieu) {
        this.signatureLieu = signatureLieu;
    }

    /**
     * Get le signature date.
     * 
     * @return the signatureDate
     */
    public String getSignatureDate() {
        return this.signatureDate;
    }

    /**
     * Set le signature date.
     * 
     * @param signatureDate
     *            the signatureDate to set
     */
    public void setSignatureDate(final String signatureDate) {
        this.signatureDate = signatureDate;
    }

    /**
     * Get le correspondance adresse.
     * 
     * @return the correspondanceAdresse
     */
    public AdresseBean getCorrespondanceAdresse() {
        return this.correspondanceAdresse;
    }

    /**
     * Set le correspondance adresse.
     * 
     * @param correspondanceAdresse
     *            the correspondanceAdresse to set
     */
    public void setCorrespondanceAdresse(final AdresseBean correspondanceAdresse) {
        this.correspondanceAdresse = correspondanceAdresse;
    }

    /**
     * Get le adresse signature.
     * 
     * @return the adresseSignature
     */
    public AdresseBean getAdresseSignature() {
        return this.adresseSignature;
    }

    /**
     * Set le adresse signature.
     * 
     * @param adresseSignature
     *            the adresseSignature to set
     */
    public void setAdresseSignature(final AdresseBean adresseSignature) {
        this.adresseSignature = adresseSignature;
    }

    /**
     * Get le numero formalite.
     * 
     * @return the numeroFormalite
     */
    public String getNumeroFormalite() {
        return this.numeroFormalite;
    }

    /**
     * Set le numero formalite.
     * 
     * @param numeroFormalite
     *            the numeroFormalite to set
     */
    public void setNumeroFormalite(final String numeroFormalite) {
        this.numeroFormalite = numeroFormalite;
    }

    /**
     * Get le dossier id.
     * 
     * @return the dossierId
     */
    public Long getDossierId() {
        return this.dossierId;
    }

    /**
     * Set le dossier id.
     * 
     * @param dossierId
     *            the dossierId to set
     */
    public void setDossierId(final Long dossierId) {
        this.dossierId = dossierId;
    }

    /**
     * Accesseur sur l'attribut {@link #signataireQualite}.
     *
     * @return String signataireQualite
     */
    public String getSignataireQualite() {
        return signataireQualite;
    }

    /**
     * Mutateur sur l'attribut {@link #signataireQualite}.
     *
     * @param signataireQualite
     *            la nouvelle valeur de l'attribut signataireQualite
     */
    public void setSignataireQualite(final String signataireQualite) {
        this.signataireQualite = signataireQualite;
    }

}
