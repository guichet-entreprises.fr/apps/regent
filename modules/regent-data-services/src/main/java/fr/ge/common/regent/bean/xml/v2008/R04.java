/**
 * 
 */
package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;
import org.xmlfield.core.types.XmlString;

/**
 * Interface R04.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
@ResourceXPath("/R04")
public interface R04 {

  /**
   * Accesseur sur l'attribut {@link #r042}.
   *
   * @return r042
   */
  @FieldXPath("R04.2")
  String getR042();

  /**
   * Mutateur sur l'attribut {@link #r042}.
   *
   * @param R042
   *          la nouvelle valeur de l'attribut r042
   */
  void setR042(String R042);

  /**
   * Accesseur sur l'attribut {@link #r043}.
   *
   * @return r043
   */
  @FieldXPath("R04.3")
  XmlString[] getR043();

  /**
   * Mutateur sur l'attribut {@link #r043}.
   *
   * @param R043
   *          la nouvelle valeur de l'attribut r043
   */
  void setR043(XmlString[] R043);

  /**
   * Ajoute le to R043.
   *
   * @return le xml string
   */
  XmlString addToR043();

  /**
   * Accesseur sur l'attribut {@link #r044}.
   *
   * @return r028
   */
  @FieldXPath("R04.4")
  String getR044();

  /**
   * Mutateur sur l'attribut {@link #r044}.
   *
   * @param R044
   *          la nouvelle valeur de l'attribut r044
   */
  void setR044(String R044);

}
