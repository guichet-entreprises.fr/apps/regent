package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;

/**
 * Le Interface NAP.
 */
public interface NAP {

  /**
   * Get le p21.
   *
   * @return le p21
   */
  @FieldXPath("P21")
  String getP21();

  /**
   * Set le p21.
   *
   * @param P21
   *          le nouveau p21
   */
  void setP21(String P21);

}
