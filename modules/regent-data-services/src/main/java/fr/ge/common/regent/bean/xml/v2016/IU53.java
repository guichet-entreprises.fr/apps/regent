package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

@ResourceXPath("/U53")
public interface IU53 {

  /**
   * Get le u531.
   *
   * @return le u531
   */
  @FieldXPath("U53.1")
  String getU531();

  /**
   * Set le u531
   *
   * @param U531
   *          le nouveau u531
   */
  void setU531(String U531);

  /**
   * Get le u533.
   *
   * @return le u533
   */
  @FieldXPath("U53.3")
  String getU533();

  /**
   * Set le u533
   *
   * @param U533
   *          le nouveau u533
   */
  void setU533(String U533);

  /**
   * Get le u534.
   *
   * @return le u534
   */
  @FieldXPath("U53.4")
  String getU534();

  /**
   * Set le u534
   *
   * @param u534
   *          le nouveau u534
   */
  void setU534(String U534);

  /**
   * Get le u535.
   *
   * @return le u535
   */
  @FieldXPath("U53.5")
  String getU535();

  /**
   * Set le u535
   *
   * @param u535
   *          le nouveau u535
   */
  void setU535(String U535);
}
