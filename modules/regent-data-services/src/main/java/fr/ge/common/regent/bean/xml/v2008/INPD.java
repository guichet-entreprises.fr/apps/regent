package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;


/**
 * Le Interface NPD.
 */
@ResourceXPath("/NPD")
public interface INPD {

	  /**
	   * Get le D21.
	   *
	   * @return le D21
	   */
	  @FieldXPath("D21") 
	  String getD21();

	  /**
	   * Set le D21.
	   *
	   * @param D21
	   *          le nouveau D21
	   */
	  void setD21(String D21);
	
	
	
	

}
