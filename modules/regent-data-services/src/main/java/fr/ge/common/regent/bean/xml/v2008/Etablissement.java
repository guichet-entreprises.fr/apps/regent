package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface Etablissement.
 */
@ResourceXPath("/Etablissement")
public interface Etablissement {

  /**
   * Get le iae.
   *
   * @return le iae
   */
  @FieldXPath("IAE")
  IAE[] getIAE();

  /**
   * Ajoute le to iae.
   *
   * @return le iae
   */
  IAE addToIAE();

  /**
   * Set le iae.
   *
   * @param IAE
   *          le nouveau iae
   */
  void setIAE(IAE[] IAE);

  /**
   * Get le dee.
   *
   * @return le dee
   */
  @FieldXPath("DEE")
  DEE[] getDEE();

  /**
   * Ajoute le to dee.
   *
   * @return le dee
   */
  DEE addToDEE();

  /**
   * Set le dee.
   *
   * @param DEE
   *          le nouveau dee
   */
  void setDEE(DEE[] DEE);

  /**
   * Get le jge.
   *
   * @return le jge
   */
  @FieldXPath("JGE")
  JGE[] getJGE();

  /**
   * Ajoute le to jge.
   *
   * @return le jge
   */
  JGE addToJGE();

  /**
   * Set le jge.
   *
   * @param JGE
   *          le nouveau jge
   */
  void setJGE(JGE[] JGE);

  /**
   * Get le ide.
   *
   * @return le ide
   */
  @FieldXPath("IDE")
  IDE[] getIDE();

  /**
   * Ajoute le to ide.
   *
   * @return le ide
   */
  IDE addToIDE();

  /**
   * Set le ide.
   *
   * @param IDE
   *          le nouveau ide
   */
  void setIDE(IDE[] IDE);

  /**
   * Get le sae.
   *
   * @return le sae
   */
  @FieldXPath("SAE")
  SAE[] getSAE();

  /**
   * Ajoute le to sae.
   *
   * @return le sae
   */
  SAE addToSAE();

  /**
   * Set le sae.
   *
   * @param SAE
   *          le nouveau sae
   */
  void setSAE(SAE[] SAE);

  /**
   * Get le cae.
   *
   * @return le cae
   */
  @FieldXPath("CAE")
  CAE[] getCAE();

  /**
   * Ajoute le to cae.
   *
   * @return le cae
   */
  CAE addToCAE();

  /**
   * Set le cae.
   *
   * @param CAE
   *          le nouveau cae
   */
  void setCAE(CAE[] CAE);

  /**
   * Get le ace.
   *
   * @return le ace
   */
  @FieldXPath("ACE")
  ACE[] getACE();

  /**
   * Ajoute le to ace.
   *
   * @return le ace
   */
  ACE addToACE();

  /**
   * Set le ace.
   *
   * @param ACE
   *          le nouveau ace
   */
  void setACE(ACE[] ACE);

  /**
   * Get le lge.
   *
   * @return le lge
   */
  @FieldXPath("LGE")
  LGE[] getLGE();

  /**
   * Ajoute le to lge.
   *
   * @return le lge
   */
  LGE addToLGE();

  /**
   * Set le lge.
   *
   * @param LGE
   *          le nouveau lge
   */
  void setLGE(LGE[] LGE);

  /**
   * Get le ree.
   *
   * @return le ree
   */
  @FieldXPath("REE")
  REE[] getREE();

  /**
   * Ajoute le to ree.
   *
   * @return le ree
   */
  REE addToREE();

  /**
   * Set le ree.
   *
   * @param REE
   *          le nouveau ree
   */
  void setREE(REE[] REE);

  /**
   * Get le pee.
   *
   * @return le pee
   */
  @FieldXPath("PEE")
  PEE[] getPEE();

  /**
   * Ajoute le to pee.
   *
   * @return le pee
   */
  PEE addToPEE();

  /**
   * Set le pee.
   *
   * @param PEE
   *          le nouveau pee
   */
  void setPEE(PEE[] PEE);

  /**
   * Get le ore.
   *
   * @return le ore
   */
  @FieldXPath("ORE")
  ORE[] getORE();

  /**
   * Ajoute le to ore.
   *
   * @return le ore
   */
  ORE addToORE();

  /**
   * Set le ore.
   *
   * @param ORE
   *          le nouveau ore
   */
  void setORE(ORE[] ORE);

  /**
   * Get le ice.
   *
   * @return le ice
   */
  @FieldXPath("ICE")
  ICE[] getICE();

  /**
   * Ajoute le to ice.
   *
   * @return le ice
   */
  ICE addToICE();

  /**
   * Set le ice.
   *
   * @param ICE
   *          le nouveau ice
   */
  void setICE(ICE[] ICE);
}
