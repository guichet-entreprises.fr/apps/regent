package fr.ge.common.regent.bean.formalite.profil.regularisation;

import static fr.ge.common.regent.util.CodeAPEUtils.transformeCodeAPE;

import java.util.List;

import fr.ge.common.regent.bean.formalite.dialogue.creation.PostalCommuneBean;
import fr.ge.common.regent.bean.formalite.profil.AbstractProfilEntreprise;
import fr.ge.common.regent.bean.formalite.profil.creation.CfeBean;

/**
 * Le bean Profil Régularisation.
 * 
 * @author hhichri
 * 
 */
public class ProfilRegularisationBean extends AbstractProfilEntreprise {

    /** La constante serialVersionUID. */
    /* serialVersionUID */
    private static final long serialVersionUID = 4084593818467027730L;

    /** Le forme juridique. */
    /* Forme Juridique. */
    private String formeJuridique;

    /** Le micro social oui non. */
    /* Micro Social Oui Non */
    private String microSocialOuiNon;

    /** Le modif oui non. */
    /* Modig Oui Non */
    private String modifOuiNon;

    /** Le modif evenements. */
    /* Modif Evenements */
    private List<String> modifEvenements;

    /** Le modif evenements2. */
    /* Modif Evenements 2 */
    private List<String> modifEvenements2;

    /** Le activite principale domaine. */
    /* Activite Principale Domaine */
    private String activitePrincipaleDomaine;

    /** Le activite principale secteur. */
    /* activite Principale Secteur */
    private String activitePrincipaleSecteur;

    /** Le activite principale code activite. */
    /* Activite Principale Code Activite */
    private String activitePrincipaleCodeActivite;

    /** Le activite principale code ape. */
    /* activitePrincipaleCodeAPE */
    private String activitePrincipaleCodeAPE;

    /** Le activite principale secteur2. */
    /* Activite Principale Secteur 2 */
    private String activitePrincipaleSecteur2;

    /** Le activite principale libelle. */
    /* Activite Principale Libelle */
    private String activitePrincipaleLibelle;

    /** Le existe activite secondaire. */
    /* Existe Activité Secondaire */
    private String existeActiviteSecondaire;

    /** Le activite secondaire domaine. */
    /* activiteSecondaireDomaine */
    private String activiteSecondaireDomaine;

    /** Le activite secondaire secteur. */
    /* activiteSecondaireSecteur */
    private String activiteSecondaireSecteur;

    /** Le activite secondaire code activite. */
    /* activiteSecondaireCodeActivite */
    private String activiteSecondaireCodeActivite;

    /** Le nom dossier. */
    private String nomDossier;

    /** Le option cmacc i1. */
    /* Option CMA CCI 1 */
    private String optionCMACCI1;

    /** Le option cmacc i2. */
    /* Option CMA CCI 2 */
    private String optionCMACCI2;

    /** Le est enregistre rm. */
    /* Est enregistré RM. */
    private String estEnregistreRM;

    /** Le est enregistre rcs. */
    /* Est enregistré RCS */
    private String estEnregistreRCS;

    /** Le postal commune. */
    private PostalCommuneBean postalCommune = new PostalCommuneBean();

    /** Le postal commune1. */
    private PostalCommuneBean postalCommune1 = new PostalCommuneBean();

    /** Le postal commune2. */
    private PostalCommuneBean postalCommune2 = new PostalCommuneBean();

    /** Le code departement. */
    private String codeDepartement;

    /** Le type cfe. */
    /* Type CFE */
    private String typeCfe;

    /** Le option cmacci. */
    private String optionCMACCI;

    /** Le secteur cfe. */
    /* Sercteur CFE */
    private String secteurCfe;

    /** Le cfe. */
    /* CFE */
    private CfeBean cfe = new CfeBean();

    /** Le aqpa. */
    private boolean aqpa;

    /** Le numero formalite. */
    private String numeroFormalite;

    /** Le reseau cfe. */
    private String reseauCFE;

    /**
     * Get le forme juridique.
     * 
     * @return the formeJuridique
     */
    public String getFormeJuridique() {
        return this.formeJuridique;
    }

    /**
     * Set le forme juridique.
     * 
     * @param formeJuridique
     *            the formeJuridique to set
     */
    public void setFormeJuridique(String formeJuridique) {
        this.formeJuridique = formeJuridique;
    }

    /**
     * Get le micro social oui non.
     * 
     * @return the microSocialOuiNon
     */
    public String getMicroSocialOuiNon() {
        return this.microSocialOuiNon;
    }

    /**
     * Set le micro social oui non.
     * 
     * @param microSocialOuiNon
     *            the microSocialOuiNon to set
     */
    public void setMicroSocialOuiNon(String microSocialOuiNon) {
        this.microSocialOuiNon = microSocialOuiNon;
    }

    /**
     * Get le modif oui non.
     * 
     * @return the modifOuiNon
     */
    public String getModifOuiNon() {
        return this.modifOuiNon;
    }

    /**
     * Set le modif oui non.
     * 
     * @param modifOuiNon
     *            the modifOuiNon to set
     */
    public void setModifOuiNon(String modifOuiNon) {
        this.modifOuiNon = modifOuiNon;
    }

    /**
     * Get le modif evenements.
     * 
     * @return the modifEvenements
     */
    public List<String> getModifEvenements() {
        return this.modifEvenements;
    }

    /**
     * Set le modif evenements.
     * 
     * @param modifEvenements
     *            the modifEvenements to set
     */
    public void setModifEvenements(List<String> modifEvenements) {
        this.modifEvenements = modifEvenements;
    }

    /**
     * Get le existe activite secondaire.
     * 
     * @return the existeActiviteSecondaire
     */
    public String getExisteActiviteSecondaire() {
        return this.existeActiviteSecondaire;
    }

    /**
     * Set le existe activite secondaire.
     * 
     * @param existeActiviteSecondaire
     *            the existeActiviteSecondaire to set
     */
    public void setExisteActiviteSecondaire(String existeActiviteSecondaire) {
        this.existeActiviteSecondaire = existeActiviteSecondaire;
    }

    /**
     * Get le modif evenements2.
     * 
     * @return the modifEvenements2
     */
    public List<String> getModifEvenements2() {
        return this.modifEvenements2;
    }

    /**
     * Set le modif evenements2.
     * 
     * @param modifEvenements2
     *            the modifEvenements2 to set
     */
    public void setModifEvenements2(List<String> modifEvenements2) {
        this.modifEvenements2 = modifEvenements2;
    }

    /**
     * Get le activite principale domaine.
     * 
     * @return the activitePrincipaleDomaine
     */
    public String getActivitePrincipaleDomaine() {
        return this.activitePrincipaleDomaine;
    }

    /**
     * Set le activite principale domaine.
     * 
     * @param activitePrincipaleDomaine
     *            the activitePrincipaleDomaine to set
     */
    public void setActivitePrincipaleDomaine(String activitePrincipaleDomaine) {
        this.activitePrincipaleDomaine = activitePrincipaleDomaine;
    }

    /**
     * Get le activite principale code activite.
     * 
     * @return the activitePrincipaleCodeActivite
     */
    public String getActivitePrincipaleCodeActivite() {
        return this.activitePrincipaleCodeActivite;
    }

    /**
     * Set le activite principale code activite.
     * 
     * @param activitePrincipaleCodeActivite
     *            the activitePrincipaleCodeActivite to set
     */
    public void setActivitePrincipaleCodeActivite(String activitePrincipaleCodeActivite) {
        this.activitePrincipaleCodeActivite = activitePrincipaleCodeActivite;
    }

    /**
     * Get le activite principale secteur.
     * 
     * @return the activitePrincipaleSecteur
     */
    public String getActivitePrincipaleSecteur() {
        return this.activitePrincipaleSecteur;
    }

    /**
     * Set le activite principale secteur.
     * 
     * @param activitePrincipaleSecteur
     *            the activitePrincipaleSecteur to set
     */
    public void setActivitePrincipaleSecteur(String activitePrincipaleSecteur) {
        this.activitePrincipaleSecteur = activitePrincipaleSecteur;
    }

    /**
     * Get le activite principale secteur2.
     * 
     * @return the activitePrincipaleSecteur2
     */
    public String getActivitePrincipaleSecteur2() {
        return this.activitePrincipaleSecteur2;
    }

    /**
     * Set le activite principale secteur2.
     * 
     * @param activitePrincipaleSecteur2
     *            the activitePrincipaleSecteur2 to set
     */
    public void setActivitePrincipaleSecteur2(String activitePrincipaleSecteur2) {
        this.activitePrincipaleSecteur2 = activitePrincipaleSecteur2;
    }

    /**
     * Get le option cmacc i1.
     * 
     * @return the optionCMACCI1
     */
    public String getOptionCMACCI1() {
        return this.optionCMACCI1;
    }

    /**
     * Set le option cmacc i1.
     * 
     * @param optionCMACCI1
     *            the optionCMACCI1 to set
     */
    public void setOptionCMACCI1(String optionCMACCI1) {
        this.optionCMACCI1 = optionCMACCI1;
    }

    /**
     * Get le option cmacc i2.
     * 
     * @return the optionCMACCI2
     */
    public String getOptionCMACCI2() {
        return this.optionCMACCI2;
    }

    /**
     * Set le option cmacc i2.
     * 
     * @param optionCMACCI2
     *            the optionCMACCI2 to set
     */
    public void setOptionCMACCI2(String optionCMACCI2) {
        this.optionCMACCI2 = optionCMACCI2;
    }

    /**
     * Get le est enregistre rm.
     * 
     * @return the estEnregistreRM
     */
    public String getEstEnregistreRM() {
        return this.estEnregistreRM;
    }

    /**
     * Set le est enregistre rm.
     * 
     * @param estEnregistreRM
     *            the estEnregistreRM to set
     */
    public void setEstEnregistreRM(String estEnregistreRM) {
        this.estEnregistreRM = estEnregistreRM;
    }

    /**
     * Get le est enregistre rcs.
     * 
     * @return the estEnregistreRCS
     */
    public String getEstEnregistreRCS() {
        return this.estEnregistreRCS;
    }

    /**
     * Set le est enregistre rcs.
     * 
     * @param estEnregistreRCS
     *            the estEnregistreRCS to set
     */
    public void setEstEnregistreRCS(String estEnregistreRCS) {
        this.estEnregistreRCS = estEnregistreRCS;
    }

    /**
     * Get le type cfe.
     * 
     * @return the typeCfe
     */
    public String getTypeCfe() {
        return this.typeCfe;
    }

    /**
     * Set le type cfe.
     * 
     * @param typeCfe
     *            the typeCfe to set
     */
    public void setTypeCfe(String typeCfe) {
        this.typeCfe = typeCfe;
    }

    /**
     * Get le code departement.
     * 
     * @return the codeDepartement
     */
    public String getCodeDepartement() {
        return this.codeDepartement;
    }

    /**
     * Set le code departement.
     * 
     * @param codeDepartement
     *            the codeDepartement to set
     */
    public void setCodeDepartement(String codeDepartement) {
        this.codeDepartement = codeDepartement;
    }

    /**
     * Get le cfe.
     * 
     * @return the cfe
     */
    public CfeBean getCfe() {
        return this.cfe;
    }

    /**
     * Set le cfe.
     * 
     * @param cfe
     *            the cfe to set
     */
    public void setCfe(CfeBean cfe) {
        this.cfe = cfe;
    }

    /**
     * Get le activite principale code ape.
     * 
     * @return the activitePrincipaleCodeAPE
     */
    public String getActivitePrincipaleCodeAPE() {
        return transformeCodeAPE(this.activitePrincipaleCodeAPE);
    }

    /**
     * Set le activite principale code ape.
     * 
     * @param activitePrincipaleCodeAPE
     *            the activitePrincipaleCodeAPE to set
     */
    public void setActivitePrincipaleCodeAPE(String activitePrincipaleCodeAPE) {
        this.activitePrincipaleCodeAPE = transformeCodeAPE(activitePrincipaleCodeAPE);
    }

    /**
     * Get le secteur cfe.
     * 
     * @return the secteurCfe
     */
    public String getSecteurCfe() {
        return this.secteurCfe;
    }

    /**
     * Set le secteur cfe.
     * 
     * @param secteurCfe
     *            the secteurCfe to set
     */
    public void setSecteurCfe(String secteurCfe) {
        this.secteurCfe = secteurCfe;
    }

    /**
     * Get le activite secondaire secteur.
     * 
     * @return the activiteSecondaireSecteur
     */
    public String getActiviteSecondaireSecteur() {
        return this.activiteSecondaireSecteur;
    }

    /**
     * Set le activite secondaire secteur.
     * 
     * @param activiteSecondaireSecteur
     *            the activiteSecondaireSecteur to set
     */
    public void setActiviteSecondaireSecteur(String activiteSecondaireSecteur) {
        this.activiteSecondaireSecteur = activiteSecondaireSecteur;
    }

    /**
     * Get le activite secondaire domaine.
     * 
     * @return the activiteSecondaireDomaine
     */
    public String getActiviteSecondaireDomaine() {
        return this.activiteSecondaireDomaine;
    }

    /**
     * Set le activite secondaire domaine.
     * 
     * @param activiteSecondaireDomaine
     *            the activiteSecondaireDomaine to set
     */
    public void setActiviteSecondaireDomaine(String activiteSecondaireDomaine) {
        this.activiteSecondaireDomaine = activiteSecondaireDomaine;
    }

    /**
     * Get le activite secondaire code activite.
     * 
     * @return the activiteSecondaireCodeActivite
     */
    public String getActiviteSecondaireCodeActivite() {
        return this.activiteSecondaireCodeActivite;
    }

    /**
     * Set le activite secondaire code activite.
     * 
     * @param activiteSecondaireCodeActivite
     *            the activiteSecondaireCodeActivite to set
     */
    public void setActiviteSecondaireCodeActivite(String activiteSecondaireCodeActivite) {
        this.activiteSecondaireCodeActivite = activiteSecondaireCodeActivite;
    }

    /**
     * Get le nom dossier.
     * 
     * @return the nomDossier
     */
    public String getNomDossier() {
        return this.nomDossier;
    }

    /**
     * Set le nom dossier.
     * 
     * @param nomDossier
     *            the nomDossier to set
     */
    public void setNomDossier(String nomDossier) {
        this.nomDossier = nomDossier;
    }

    /**
     * Get le postal commune.
     * 
     * @return the postalCommune
     */
    public PostalCommuneBean getPostalCommune() {
        return this.postalCommune;
    }

    /**
     * Set le postal commune.
     * 
     * @param postalCommune
     *            the postalCommune to set
     */
    public void setPostalCommune(PostalCommuneBean postalCommune) {
        this.postalCommune = postalCommune;
    }

    /**
     * Get le postal commune2.
     * 
     * @return the postalCommune2
     */
    public PostalCommuneBean getPostalCommune2() {
        return this.postalCommune2;
    }

    /**
     * Set le postal commune2.
     * 
     * @param postalCommune2
     *            the postalCommune2 to set
     */
    public void setPostalCommune2(PostalCommuneBean postalCommune2) {
        this.postalCommune2 = postalCommune2;
    }

    /**
     * Get le option cmacci.
     * 
     * @return the optionCMACCI
     */
    public String getOptionCMACCI() {
        return this.optionCMACCI;
    }

    /**
     * Set le option cmacci.
     * 
     * @param optionCMACCI
     *            the optionCMACCI to set
     */
    public void setOptionCMACCI(String optionCMACCI) {
        this.optionCMACCI = optionCMACCI;
    }

    /**
     * Verifie si c'est aqpa.
     * 
     * @return the aqpa
     */
    public boolean isAqpa() {
        return this.aqpa;
    }

    /**
     * Set le aqpa.
     * 
     * @param aqpa
     *            the aqpa to set
     */
    public void setAqpa(boolean aqpa) {
        this.aqpa = aqpa;
    }

    /**
     * Get le numero formalite.
     * 
     * @return the numeroFormalite
     */
    public String getNumeroFormalite() {
        return this.numeroFormalite;
    }

    /**
     * Set le numero formalite.
     * 
     * @param numeroFormalite
     *            the numeroFormalite to set
     */
    public void setNumeroFormalite(String numeroFormalite) {
        this.numeroFormalite = numeroFormalite;
    }

    /**
     * Get le activite principale libelle.
     * 
     * @return the activitePrincipaleLibelle
     */
    public String getActivitePrincipaleLibelle() {
        return this.activitePrincipaleLibelle;
    }

    /**
     * Set le activite principale libelle.
     * 
     * @param activitePrincipaleLibelle
     *            the activitePrincipaleLibelle to set
     */
    public void setActivitePrincipaleLibelle(String activitePrincipaleLibelle) {
        this.activitePrincipaleLibelle = activitePrincipaleLibelle;
    }

    /**
     * Get le reseau cfe.
     * 
     * @return the reseauCFE
     */
    public String getReseauCFE() {
        return this.reseauCFE;
    }

    /**
     * Set le reseau cfe.
     * 
     * @param reseauCFE
     *            the reseauCFE to set
     */
    public void setReseauCFE(String reseauCFE) {
        this.reseauCFE = reseauCFE;
    }

    /**
     * Get le postal commune1.
     * 
     * @return the postalCommune1
     */
    public PostalCommuneBean getPostalCommune1() {
        return this.postalCommune1;
    }

    /**
     * Set le postal commune1.
     * 
     * @param postalCommune1
     *            the postalCommune1 to set
     */
    public void setPostalCommune1(PostalCommuneBean postalCommune1) {
        this.postalCommune1 = postalCommune1;
    }

}
