/**
 * 
 */
package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Interface P90.1.
 *
 * @author $Author: aboidard $
 * @version $Revision: 0 $
 */
@ResourceXPath("/P90.1")
public interface IP901 {

  /**
   * Accesseur sur l'attribut {@link #P9011}.
   *
   * @return P9011
   */
  @FieldXPath("P90.1.1")
  String getP9011();

  /**
   * Mutateur sur l'attribut {@link #P9011}.
   *
   * @param p9011
   *          la nouvelle valeur de l'attribut P9011
   */
  void setP9011(String p9011);

  /**
   * Accesseur sur l'attribut {@link #P9012}.
   *
   * @return P9012
   */
  @FieldXPath("P90.1.2")
  String getP9012();

  /**
   * Mutateur sur l'attribut {@link #P9012}.
   *
   * @param p9012
   *          la nouvelle valeur de l'attribut P9012
   */
  void setP9012(String p9012);

}
