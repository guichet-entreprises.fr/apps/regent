package fr.ge.common.regent.bean.xml.formalite;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

import fr.ge.common.regent.bean.xml.IXml;
import fr.ge.common.regent.xml.annotation.Indexed;

/**
 * Le Interface IFormalite.
 */
@ResourceXPath("/formalite")
public interface IFormalite extends IXml {

    /** La constante MODEL_VERSION. */
    int MODEL_VERSION = 0;

    /**
     * représente l'identifiant de la formalité, pour le profil on a
     * 'profilEntreprises', pour le dialogue cfe on a 'cfe', pour les
     * autorisations il s'agit de l'id du flow.
     *
     * @return le identifiant formalite
     */
    @FieldXPath("identifiantFormalite")
    @Indexed("identifiant_formalite")
    String getIdentifiantFormalite();

    /**
     * Get le numero dossier.
     *
     * @return le numero dossier
     */
    @FieldXPath("numeroDossier")
    @Indexed("dossier")
    String getNumeroDossier();

    /**
     * Représente l'identifiant de l'utilisateur.
     *
     * @return le utilisateur id
     */
    @FieldXPath(value = "utilisateurId")
    @Indexed("user_id")
    String getUtilisateurId();

    /**
     * Permet de savoir si une formalitée est terminée.
     *
     * @return le terminee
     */
    @FieldXPath("terminee")
    boolean getTerminee();

    /**
     * Get le numero formalite.
     *
     * @return le numero formalite
     */
    @FieldXPath(value = "numeroFormalite")
    String getNumeroFormalite();

    /**
     * Get le dossier id.
     *
     * @return le dossier id
     */
    @FieldXPath(value = "dossierId")
    String getDossierId();

    /**
     * Get le code commune activite.
     *
     * @return le code commune activite
     */
    @FieldXPath(value = "codeCommuneActivite")
    String getCodeCommuneActivite();

    /**
     * Get le reseau cfe.
     *
     * @return le reseau cfe
     */
    @FieldXPath(value = "reseauCFE")
    String getReseauCFE();

    /**
     * Get le courriel.
     *
     * @return le courriel
     */
    @FieldXPath(value = "courriel")
    String getCourriel();

    /**
     * Get le type personne.
     * 
     * @return the typePersonne
     */
    @FieldXPath(value = "typePersonne")
    String getTypePersonne();

    /**
     * Getter de l'attribut estAutoEntrepreneur.
     * 
     * @return la valeur de estAutoEntrepreneur
     */
    @FieldXPath(value = "estAutoEntrepreneur")
    boolean getEstAutoEntrepreneur();

    /**
     * Getter de l'attribut optionAQPA.
     * 
     * @return la valeur de optionAQPA
     */
    @FieldXPath(value = "optionAQPA")
    boolean getOptionAQPA();

    /**
     * Getter de l'attribut estEirl. get
     * 
     * @return la valeur de estEirl
     */
    @FieldXPath(value = "estEirl")
    String getEstEirl();

    /**
     * Getter de l'attribut isAqpa.
     * 
     * @return la valeur de isAqpa
     */
    @FieldXPath(value = "isAqpa")
    boolean getIsAqpa();

    /**
     * Getter de l'attribut aqpa.
     * 
     * @return la valeur de aqpa
     */
    @FieldXPath(value = "isAqpa")
    boolean getAqpa();

    /**
     * Getter de l'attribut estAgentCommercial.
     * 
     * @return la valeur de estAgentCommercial
     */
    @FieldXPath(value = "estAgentCommercial")
    boolean getEstAgentCommercial();

    /**
     * Set le type personne.
     * 
     * @param typePersonne
     *            the typePersonne to set
     */
    void setTypePersonne(String typePersonne);

    /**
     * Set le identifiant formalite.
     *
     * @param identifiantFormalite
     *            le nouveau identifiant formalite
     */
    void setIdentifiantFormalite(String identifiantFormalite);

    /**
     * Set le utilisateur id.
     *
     * @param utilisateurId
     *            le nouveau utilisateur id
     */
    void setUtilisateurId(String utilisateurId);

    /**
     * Set le numero dossier.
     *
     * @param numeroDossier
     *            le nouveau numero dossier
     */
    void setNumeroDossier(String numeroDossier);

    /**
     * Set le terminee.
     *
     * @param terminee
     *            le nouveau terminee
     */
    void setTerminee(boolean terminee);

    /**
     * Set le numero formalite.
     *
     * @param numeroFormalite
     *            le nouveau numero formalite
     */
    void setNumeroFormalite(String numeroFormalite);

    /**
     * Set le dossier id.
     *
     * @param dossierId
     *            le nouveau dossier id
     */
    void setDossierId(String dossierId);

    /**
     * Set le code commune activite.
     *
     * @param commune
     *            le nouveau code commune activite
     */
    void setCodeCommuneActivite(String commune);

    /**
     * Set le reseau cfe.
     *
     * @param reseauCFE
     *            le nouveau reseau cfe
     */
    void setReseauCFE(String reseauCFE);

    /**
     * Set le courriel.
     *
     * @param courriel
     *            le nouveau courriel
     */
    void setCourriel(String courriel);

    /**
     * Setter de l'attribut aqpa.
     * 
     * @param isAqpa
     *            la nouvelle valeur de aqpa
     */
    void setAqpa(boolean isAqpa);

    /**
     * Setter de l'attribut estAgentCommercial.
     * 
     * @param estAgentCommercial
     *            la nouvelle valeur de estAgentCommercial
     */
    void setEstAgentCommercial(boolean estAgentCommercial);

    /**
     * Setter de l'attribut estAutoEntrepreneur.
     * 
     * @param estAutoEntrepreneur
     *            la nouvelle valeur de estAutoEntrepreneur
     */
    void setEstAutoEntrepreneur(boolean estAutoEntrepreneur);

    /**
     * Setter de l'attribut estEirl.
     * 
     * @param estEirl
     *            la nouvelle valeur de estEirl
     */
    void setEstEirl(String estEirl);

    /**
     * Setter de l'attribut isAqpa.
     * 
     * @param isAqpa
     *            la nouvelle valeur de isAqpa
     */
    void setIsAqpa(boolean isAqpa);

    /**
     * Setter de l'attribut optionAQPA.
     * 
     * @param optionAQPA
     *            la nouvelle valeur de optionAQPA
     */
    void setOptionAQPA(boolean optionAQPA);

}
