// Profil V2 demo : NE PAS SUPPR
package fr.ge.common.regent.bean.formalite.profil.creation;

/**
 * Class ProfilBeanV2.
 */
public class ProfilCreationBeanV2 extends ProfilCreationBean {

  /** La constante serialVersionUID. */
  private static final long serialVersionUID = -2292878198941071405L;

  /** descr libre activite principale. */
  private String descrLibreActivitePrincipale;

  /** activite principale non trouve. */
  private String activitePrincipaleNonTrouve;

  /** code cfe. */
  private String codeCfe;

  /** aqpa str. */
  private String aqpaStr;

  /** descr libre activite secondaire. */
  private String descrLibreActiviteSecondaire;

  /** activite secondaire non trouve. */
  private String activiteSecondaireNonTrouve;

  /** code cfe2. */
  private String codeCfe2;

  /** Le activite principale secteur micro 1. */
  private String activitePrincipaleSecteurMicro1;

  /** Le activite principale secteur micro 2. */
  private String activitePrincipaleSecteurMicro2;

  /** Le activite secondaire secteur micro 1. */
  private String activiteSecondaireSecteurMicro1;

  /** Le activite secondaire secteur micro 2. */
  private String activiteSecondaireSecteurMicro2;

  /**
   * Attribut tampon permettant de récupérer la valeur de activitePrincipaleSecteurMicro dans une variable tampon pour la cas d'une saisie
   * d'une act. sec.
   */
  private String activitePrincipaleSecteurMicroTampon;

  /** saisie libre décrivant l'activité principale. */
  private String precisionActivitePrincipale;

  /** saisie libre décrivant l'activité secondaire. */
  private String precisionActiviteSecondaire;

  /**
   * Accesseur sur l'attribut {@link #precisionActivitePrincipale}.
   *
   * @return String precisionActivitePrincipale
   */
  public String getPrecisionActivitePrincipale() {
    return precisionActivitePrincipale;
  }

  /**
   * Mutateur sur l'attribut {@link #precisionActivitePrincipale}.
   *
   * @param precisionActivitePrincipale
   *          la nouvelle valeur de l'attribut precisionActivitePrincipale
   */
  public void setPrecisionActivitePrincipale(final String precisionActivitePrincipale) {
    this.precisionActivitePrincipale = precisionActivitePrincipale;
  }

  /**
   * @return the activitePrincipaleSecteurMicroTampon
   */
  public String getActivitePrincipaleSecteurMicroTampon() {
    return activitePrincipaleSecteurMicroTampon;
  }

  /**
   * @param activitePrincipaleSecteurMicroTampon
   *          the activitePrincipaleSecteurMicroTampon to set
   */
  public void setActivitePrincipaleSecteurMicroTampon(String activitePrincipaleSecteurMicroTampon) {
    this.activitePrincipaleSecteurMicroTampon = activitePrincipaleSecteurMicroTampon;
  }

  /**
   * @return the descrLibreActivitePrincipale
   */
  public String getDescrLibreActivitePrincipale() {
    return this.descrLibreActivitePrincipale;
  }

  /**
   * @param descrLibreActivitePrincipale
   *          the descrLibreActivitePrincipale to set
   */
  public void setDescrLibreActivitePrincipale(String descrLibreActivitePrincipale) {
    this.descrLibreActivitePrincipale = descrLibreActivitePrincipale;
  }

  /**
   * @return the activitePrincipaleNonTrouve
   */
  public String getActivitePrincipaleNonTrouve() {
    return this.activitePrincipaleNonTrouve;
  }

  /**
   * @param activitePrincipaleNonTrouve
   *          the activitePrincipaleNonTrouve to set
   */
  public void setActivitePrincipaleNonTrouve(String activitePrincipaleNonTrouve) {
    this.activitePrincipaleNonTrouve = activitePrincipaleNonTrouve;
  }

  /**
   * @return the codeCfe
   */
  public String getCodeCfe() {
    return this.codeCfe;
  }

  /**
   * @param codeCfe
   *          the codeCfe to set
   */
  public void setCodeCfe(String codeCfe) {
    this.codeCfe = codeCfe;
  }

  /**
   * @return the aqpaStr
   */
  public String getAqpaStr() {
    return this.aqpaStr;
  }

  /**
   * @param aqpaStr
   *          the aqpaStr to set
   */
  public void setAqpaStr(String aqpaStr) {
    this.aqpaStr = aqpaStr;
  }

  /**
   * @return the descrLibreActiviteSecondaire
   */
  public String getDescrLibreActiviteSecondaire() {
    return this.descrLibreActiviteSecondaire;
  }

  /**
   * @param descrLibreActiviteSecondaire
   *          the descrLibreActiviteSecondaire to set
   */
  public void setDescrLibreActiviteSecondaire(String descrLibreActiviteSecondaire) {
    this.descrLibreActiviteSecondaire = descrLibreActiviteSecondaire;
  }

  /**
   * @return the activiteSecondaireNonTrouve
   */
  public String getActiviteSecondaireNonTrouve() {
    return this.activiteSecondaireNonTrouve;
  }

  /**
   * @param activiteSecondaireNonTrouve
   *          the activiteSecondaireNonTrouve to set
   */
  public void setActiviteSecondaireNonTrouve(String activiteSecondaireNonTrouve) {
    this.activiteSecondaireNonTrouve = activiteSecondaireNonTrouve;
  }

  /**
   * @return the codeCfe2
   */
  public String getCodeCfe2() {
    return this.codeCfe2;
  }

  /**
   * @param codeCfe2
   *          the codeCfe2 to set
   */
  public void setCodeCfe2(String codeCfe2) {
    this.codeCfe2 = codeCfe2;
  }

  /**
   * @return the activitePrincipaleSecteurMicro1
   */
  public String getActivitePrincipaleSecteurMicro1() {
    return this.activitePrincipaleSecteurMicro1;
  }

  /**
   * @param activitePrincipaleSecteurMicro1
   *          the activitePrincipaleSecteurMicro1 to set
   */
  public void setActivitePrincipaleSecteurMicro1(String activitePrincipaleSecteurMicro1) {
    this.activitePrincipaleSecteurMicro1 = activitePrincipaleSecteurMicro1;
  }

  /**
   * @return the activitePrincipaleSecteurMicro2
   */
  public String getActivitePrincipaleSecteurMicro2() {
    return this.activitePrincipaleSecteurMicro2;
  }

  /**
   * @param activitePrincipaleSecteurMicro2
   *          the activitePrincipaleSecteurMicro2 to set
   */
  public void setActivitePrincipaleSecteurMicro2(String activitePrincipaleSecteurMicro2) {
    this.activitePrincipaleSecteurMicro2 = activitePrincipaleSecteurMicro2;
  }

  /**
   * @return the activiteSecondaireSecteurMicro1
   */
  public String getActiviteSecondaireSecteurMicro1() {
    return this.activiteSecondaireSecteurMicro1;
  }

  /**
   * @param activiteSecondaireSecteurMicro1
   *          the activiteSecondaireSecteurMicro1 to set
   */
  public void setActiviteSecondaireSecteurMicro1(String activiteSecondaireSecteurMicro1) {
    this.activiteSecondaireSecteurMicro1 = activiteSecondaireSecteurMicro1;
  }

  /**
   * @return the activiteSecondaireSecteurMicro2
   */
  public String getActiviteSecondaireSecteurMicro2() {
    return this.activiteSecondaireSecteurMicro2;
  }

  /**
   * @param activiteSecondaireSecteurMicro2
   *          the activiteSecondaireSecteurMicro2 to set
   */
  public void setActiviteSecondaireSecteurMicro2(String activiteSecondaireSecteurMicro2) {
    this.activiteSecondaireSecteurMicro2 = activiteSecondaireSecteurMicro2;
  }

  /**
   * Accesseur sur l'attribut {@link #precisionActiviteSecondaire}.
   *
   * @return String precisionActiviteSecondaire
   */
  public String getPrecisionActiviteSecondaire() {
    return precisionActiviteSecondaire;
  }

  /**
   * Mutateur sur l'attribut {@link #precisionActiviteSecondaire}.
   *
   * @param precisionActiviteSecondaire 
   *          la nouvelle valeur de l'attribut precisionActiviteSecondaire
   */
  public void setPrecisionActiviteSecondaire(String precisionActiviteSecondaire) {
    this.precisionActiviteSecondaire = precisionActiviteSecondaire;
  }

}
// FIN Profil V2 demo : NE PAS SUPPR
