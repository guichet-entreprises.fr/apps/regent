package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface ADS.
 */
@ResourceXPath("/ADS")
public interface ADS {

  /**
   * Get le A60.
   *
   * @return le A60
   */
  @FieldXPath("A60")
  String getA60();

  /**
   * Set le A60.
   *
   * @param A60
   *          le nouveau A60
   */
  void setA60(String A60);

  /**
   * Get le A63.
   *
   * @return le A63
   */
  @FieldXPath("A63")
  String getA63();

  /**
   * Set le A63.
   *
   * @param A63
   *          le nouveau A63
   */
  void setA63(String A63);

  /**
   * Get le A66.
   *
   * @return le A66
   */
  @FieldXPath("A66")
  String getA66();

  /**
   * Set le A66.
   *
   * @param A66
   *          le nouveau A66
   */
  void setA66(String A66);

  /**
   * Get le J00.
   *
   * @return le J00
   */
  @FieldXPath("J00")
  String getJ00();

  /**
   * Set le J00.
   *
   * @param J00
   *          le nouveau J00
   */
  void setJ00(String J00);

  /**********************************************/

  /**
   * Get le A61.
   *
   * @return le A61
   */
  @FieldXPath("A61")
  IA61[] getA61();

  /**
   * Ajoute le to A61.
   *
   * @return le i A61
   */
  IA61 addToA61();

  /**
   * Set le A61.
   *
   * @param A61
   *          le nouveau A61
   */
  void setA61(IA61[] A61);

  /**
   * Get le A62.
   *
   * @return le A62
   */
  @FieldXPath("A62")
  IA62[] getA62();

  /**
   * Ajoute le to A62.
   *
   * @return le i A62
   */
  IA62 addToA62();

  /**
   * Set le A62.
   *
   * @param A62
   *          le nouveau A62
   */
  void setA62(IA62[] A62);

  /**
   * Get le A64.
   *
   * @return le A64
   */
  @FieldXPath("A64")
  IA64[] getA64();

  /**
   * Ajoute le to A64.
   *
   * @return le i A64
   */
  IA64 addToA64();

  /**
   * Set le A64.
   *
   * @param A64
   *          le nouveau A64
   */
  void setA64(IA64[] A64);

  /**
   * Get le A67.
   *
   * @return le A67
   */
  @FieldXPath("A67")
  IA67[] getA67();

  /**
   * Ajoute le to A67.
   *
   * @return le i A67
   */
  IA67 addToA67();

  /**
   * Set le A67.
   *
   * @param A67
   *          le nouveau A67
   */
  void setA67(IA67[] A67);

}
