package fr.ge.common.regent.bean.xml.formalite.dialogue.creation;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

import fr.ge.common.regent.bean.formalite.dialogue.creation.DeclarationSocialeBean;
import fr.ge.common.regent.bean.xml.formalite.IFormalite;
import fr.ge.common.regent.bean.xml.transverse.IAdresse;
import fr.ge.common.regent.bean.xml.transverse.IPostalCommune;

/**
 * Le Interface de l'entité dirigeant de l'entreprise
 * 
 * 
 * 
 */
@ResourceXPath("/formalite")
public interface IDirigeantCreation extends IFormalite {

    /**
     * Get le pp nom naissance.
     *
     * @return le pp nom naissance
     */
    @FieldXPath(value = "ppNomNaissance")
    String getPpNomNaissance();

    /**
     * Get le pp nom usage.
     *
     * @return le pp nom usage
     */
    @FieldXPath(value = "ppNomUsage")
    String getPpNomUsage();

    /**
     * Get le pp prenom1.
     *
     * @return le pp prenom1
     */
    @FieldXPath(value = "ppPrenom1")
    String getPpPrenom1();

    /**
     * Get le pp prenom2.
     *
     * @return le pp prenom2
     */
    @FieldXPath(value = "ppPrenom2")
    String getPpPrenom2();

    /**
     * Get le pp prenom3.
     *
     * @return le pp prenom3
     */
    @FieldXPath(value = "ppPrenom3")
    String getPpPrenom3();

    /**
     * Get le pp prenom4.
     *
     * @return le pp prenom4
     */
    @FieldXPath(value = "ppPrenom4")
    String getPpPrenom4();

    /**
     * Get le pp pseudonyme.
     *
     * @return le pp pseudonyme
     */
    @FieldXPath(value = "ppPseudonyme")
    String getPpPseudonyme();

    /**
     * Get le pp nationalite.
     *
     * @return le pp nationalite
     */
    @FieldXPath(value = "ppNationalite")
    String getPpNationalite();

    /**
     * Get le pp civilite.
     *
     * @return le pp civilite
     */
    @FieldXPath(value = "ppCivilite")
    String getPpCivilite();

    /**
     * Get le pp date naissance.
     *
     * @return le pp date naissance
     */
    @FieldXPath(value = "ppDateNaissance")
    String getPpDateNaissance();

    /**
     * Get le ppMineurEmancipe.
     *
     * @return le ppMineurEmancipe
     */
    @FieldXPath(value = "ppMineurEmancipe")
    String getPpMineurEmancipe();

    /**
     * Get le pp lieu naissance pays.
     *
     * @return le pp lieu naissance pays
     */
    @FieldXPath(value = "ppLieuNaissancePays")
    String getPpLieuNaissancePays();

    /**
     * Get le pp lieu naissance departement.
     *
     * @return le pp lieu naissance departement
     */
    @FieldXPath(value = "ppLieuNaissanceDepartement")
    String getPpLieuNaissanceDepartement();

    /**
     * Get le pp lieu naissance commune.
     *
     * @return le pp lieu naissance commune
     */
    @FieldXPath(value = "ppLieuNaissanceCommune")
    String getPpLieuNaissanceCommune();

    /**
     * Get le pp lieu naissance ville.
     *
     * @return le pp lieu naissance ville
     */
    @FieldXPath(value = "ppLieuNaissanceVille")
    String getPpLieuNaissanceVille();

    /**
     * Getter de l'attribut declarationSociale.
     * 
     * @return la valeur de declarationSociale
     */
    @FieldXPath(value = "declarationSociale")
    IDeclarationSociale getDeclarationSociale();

    /**
     * New conjoint.
     *
     * @return le i conjoint modification
     */
    IDeclarationSociale newDeclarationSociale();

    /**
     * Getter de l'attribut dirigeantPresentStatuts.
     * 
     * @return la valeur de dirigeantPresentStatuts
     */
    @FieldXPath(value = "dirigeantPresentStatuts")
    String getDirigeantPresentStatuts();

    /**
     * Getter de l'attribut inscriptionCACPubliee.
     * 
     * @return la valeur de inscriptionCACPubliee
     */
    @FieldXPath(value = "inscriptionCACPubliee")
    String getInscriptionCACPubliee();

    /**
     * Getter de l'attribut nature.
     * 
     * @return la valeur de nature
     */
    @FieldXPath(value = "nature")
    String getNature();

    /**
     * Setter de l'attribut nature.
     * 
     * @param nature
     *            la nouvelle valeur de nature
     */
    public void setNature(String nature);

    /**
     * Getter de l'attribut pmDenomination.
     * 
     * @return la valeur de pmDenomination
     */
    @FieldXPath(value = "pmDenomination")
    String getPmDenomination();

    /**
     * Getter de l'attribut pmFormeJuridique.
     * 
     * @return la valeur de pmFormeJuridique
     */
    @FieldXPath(value = "pmFormeJuridique")
    String getPmFormeJuridique();

    /**
     * Getter de l'attribut pmLieuImmatriculation.
     * 
     * @return la valeur de pmLieuImmatriculation
     */
    @FieldXPath(value = "pmLieuImmatriculation")
    String getPmLieuImmatriculation();

    /**
     * Getter de l'attribut pmNumeroIdentification.
     * 
     * @return la valeur de pmNumeroIdentification
     */
    @FieldXPath(value = "pmNumeroIdentification")
    String getPmNumeroIdentification();

    /**
     * Getter de l'attribut pmRepresentantAdresse.
     * 
     * @return la valeur de pmRepresentantAdresse
     */
    @FieldXPath(value = "pmRepresentantAdresse")
    IAdresse getPmRepresentantAdresse();

    /**
     * New adresse.
     *
     * @return le i adresse
     */
    IAdresse newPmRepresentantAdresse();

    /**
     * Getter de l'attribut pmRepresentantCivilite.
     * 
     * @return la valeur de pmRepresentantCivilite
     */
    @FieldXPath(value = "pmRepresentantCivilite")
    String getPmRepresentantCivilite();

    /**
     * Getter de l'attribut pmRepresentantDateNaissance.
     * 
     * @return la valeur de pmRepresentantDateNaissance
     */
    @FieldXPath(value = "pmRepresentantDateNaissance")
    String getPmRepresentantDateNaissance();

    /**
     * Getter de l'attribut pmRepresentantLieuNaissanceCommune.
     * 
     * @return la valeur de pmRepresentantLieuNaissanceCommune
     */
    @FieldXPath(value = "pmRepresentantLieuNaissanceCommune")
    String getPmRepresentantLieuNaissanceCommune();

    /**
     * Getter de l'attribut pmRepresentantLieuNaissanceDepartement.
     * 
     * @return la valeur de pmRepresentantLieuNaissanceDepartement
     */
    @FieldXPath(value = "pmRepresentantLieuNaissanceDepartement")
    String getPmRepresentantLieuNaissanceDepartement();

    /**
     * Getter de l'attribut pmRepresentantLieuNaissancePays.
     * 
     * @return la valeur de pmRepresentantLieuNaissancePays
     */
    @FieldXPath(value = "pmRepresentantLieuNaissancePays")
    String getPmRepresentantLieuNaissancePays();

    /**
     * Getter de l'attribut pmRepresentantLieuNaissanceVille.
     * 
     * @return la valeur de pmRepresentantLieuNaissanceVille
     */
    @FieldXPath(value = "pmRepresentantLieuNaissanceVille")
    String getPmRepresentantLieuNaissanceVille();

    /**
     * Getter de l'attribut pmRepresentantNationalite.
     * 
     * @return la valeur de pmRepresentantNationalite
     */
    @FieldXPath(value = "pmRepresentantNationalite")
    String getPmRepresentantNationalite();

    /**
     * Getter de l'attribut pmRepresentantNomNaissance.
     * 
     * @return la valeur de pmRepresentantNomNaissance
     */
    @FieldXPath(value = "pmRepresentantNomNaissance")
    String getPmRepresentantNomNaissance();

    /**
     * Getter de l'attribut pmRepresentantNomUsage.
     * 
     * @return la valeur de pmRepresentantNomUsage
     */
    @FieldXPath(value = "pmRepresentantNomUsage")
    String getPmRepresentantNomUsage();

    /**
     * Getter de l'attribut pmRepresentantPMPrenom1.
     * 
     * @return la valeur de pmRepresentantPMPrenom1
     */
    @FieldXPath(value = "pmRepresentantPMPrenom1")
    String getPmRepresentantPMPrenom1();

    /**
     * Getter de l'attribut pmRepresentantPMPrenom2.
     * 
     * @return la valeur de pmRepresentantPMPrenom2
     */
    @FieldXPath(value = "pmRepresentantPMPrenom2")
    String getPmRepresentantPMPrenom2();

    /**
     * Getter de l'attribut pmRepresentantPMPrenom3.
     * 
     * @return la valeur de pmRepresentantPMPrenom3
     */
    @FieldXPath(value = "pmRepresentantPMPrenom3")
    String getPmRepresentantPMPrenom3();

    /**
     * Getter de l'attribut pmRepresentantPMPrenom4.
     * 
     * @return la valeur de pmRepresentantPMPrenom4
     */
    @FieldXPath(value = "pmRepresentantPMPrenom4")
    String getPmRepresentantPMPrenom4();

    /**
     * Getter de l'attribut pmRepresentantQualite.
     * 
     * @return la valeur de pmRepresentantQualite
     */
    @FieldXPath(value = "pmRepresentantQualite")
    String getPmRepresentantQualite();

    /**
     * Getter de l'attribut conjointPresence.
     * 
     * @return la valeur de conjointPresence
     */
    @FieldXPath(value = "conjointPresence")
    String getConjointPresence();

    /**
     * Getter de l'attribut conjointRegime.
     * 
     * @return la valeur de conjointRegime
     */
    @FieldXPath(value = "conjointRegime")
    String getConjointRegime();

    /**
     * Getter de l'attribut conjointStatut.
     * 
     * @return la valeur de conjointStatut
     */
    @FieldXPath(value = "conjointStatut")
    String getConjointStatut();

    /**
     * Getter de l'attribut qualite.
     * 
     * @return la valeur de qualite
     */
    @FieldXPath(value = "qualite")
    String getQualite();

    /**
     * Getter de l'attribut libelleQualite.
     * 
     * @return la valeur de libelleQualite.
     */
    @FieldXPath(value = "libelleQualite")
    String getLibelleQualite();

    /**
     * Get le pp adresse.
     *
     * @return le pp adresse
     */
    @FieldXPath(value = "ppAdresse")
    IAdresse getPpAdresse();

    /**
     * New pp adresse.
     *
     * @return le i adresse
     */
    IAdresse newPpAdresse();

    @FieldXPath(value = "id")
    String getId();

    void setId(String id);

    /**
     * Set le pp adresse.
     *
     * @param ppAdresse
     *            le nouveau pp adresse
     */
    void setPpAdresse(IAdresse ppAdresse);

    /**
     * Get le pm adresse.
     *
     * @return le pm adresse
     */
    @FieldXPath(value = "pmAdresse")
    IAdresse getPmAdresse();

    /**
     * New pm adresse.
     *
     * @return le i adresse
     */
    IAdresse newPmAdresse();

    /**
     * Set le pm adresse.
     *
     * @param pmAdresse
     *            le nouveau pm adresse
     */
    void setPmAdresse(IAdresse pmAdresse);

    /**
     * Get le conjoint.
     *
     * @return le conjoint
     */
    @FieldXPath(value = "conjoint")
    IConjointCreation getConjoint();

    /**
     * Set le conjoint.
     *
     * @param conjoint
     *            le nouveau conjoint
     */
    void setConjoint(IConjointCreation conjoint);

    /**
     * New conjoint.
     *
     * @return le i conjoint modification
     */
    IConjointCreation newConjoint();

    /**
     * Get le pp adresse forain.
     *
     * @return le pp adresse forain
     */
    @FieldXPath(value = "ppAdresseForain")
    IPostalCommune getPpAdresseForain();

    /**
     * New pp adresse forain.
     *
     * @return le i postal commune
     */
    IPostalCommune newPpAdresseForain();

    /**
     * Set le pp adresse forain.
     *
     * @param ppAdresseForain
     *            le nouveau pp adresse forain
     */
    void setPpAdresseForain(IPostalCommune ppAdresseForain);

    /**
     * Get le pp adresse ambulant.
     *
     * @return le pp adresse ambulant
     */
    @FieldXPath(value = "ppAdresseAmbulant")
    IPostalCommune getPpAdresseAmbulant();

    /**
     * New pp adresse ambulant.
     *
     * @return le i postal commune
     */
    IPostalCommune newPpAdresseAmbulant();

    /**
     * Set le pp adresse ambulant.
     *
     * @param ppAdresseAmbulant
     *            le nouveau pp adresse ambulant
     */
    void setPpAdresseAmbulant(IPostalCommune ppAdresseAmbulant);

    /**
     * Set le pp nom naissance.
     *
     * @param ppNomNaissance
     *            le nouveau pp nom naissance
     */
    void setPpNomNaissance(String ppNomNaissance);

    /**
     * Set le pp nom usage.
     *
     * @param ppNomUsage
     *            le nouveau pp nom usage
     */
    void setPpNomUsage(String ppNomUsage);

    /**
     * Set le pp prenom1.
     *
     * @param ppPrenom1
     *            le nouveau pp prenom1
     */
    void setPpPrenom1(String ppPrenom1);

    /**
     * Set le pp prenom2.
     *
     * @param ppPrenom2
     *            le nouveau pp prenom2
     */
    void setPpPrenom2(String ppPrenom2);

    /**
     * Set le pp prenom3.
     *
     * @param ppPrenom3
     *            le nouveau pp prenom3
     */
    void setPpPrenom3(String ppPrenom3);

    /**
     * Set le pp prenom4.
     *
     * @param ppPrenom4
     *            le nouveau pp prenom4
     */
    void setPpPrenom4(String ppPrenom4);

    /**
     * Set le pp pseudonyme.
     *
     * @param ppPseudonyme
     *            le nouveau pp pseudonyme
     */
    void setPpPseudonyme(String ppPseudonyme);

    /**
     * Set le pp nationalite.
     *
     * @param ppNationalite
     *            le nouveau pp nationalite
     */
    void setPpNationalite(String ppNationalite);

    /**
     * Set le pp civilite.
     *
     * @param ppCivilite
     *            le nouveau pp civilite
     */
    void setPpCivilite(String ppCivilite);

    /**
     * Set le pp date naissance.
     *
     * @param ppDateNaissance
     *            le nouveau pp date naissance
     */
    void setPpDateNaissance(String ppDateNaissance);

    /**
     * Set le pp mineur emancipe.
     *
     * @param ppMineurEmancipe
     *            the ppMineurEmancipe to set
     */
    void setPpMineurEmancipe(String ppMineurEmancipe);

    /**
     * Set le pp lieu naissance pays.
     *
     * @param ppLieuNaissancePays
     *            le nouveau pp lieu naissance pays
     */
    void setPpLieuNaissancePays(String ppLieuNaissancePays);

    /**
     * Set le pp lieu naissance departement.
     *
     * @param ppLieuNaissanceDepartement
     *            le nouveau pp lieu naissance departement
     */
    void setPpLieuNaissanceDepartement(String ppLieuNaissanceDepartement);

    /**
     * Set le pp lieu naissance commune.
     *
     * @param ppLieuNaissanceCommune
     *            le nouveau pp lieu naissance commune
     */
    void setPpLieuNaissanceCommune(String ppLieuNaissanceCommune);

    /**
     * Set le pp lieu naissance ville.
     *
     * @param ppLieuNaissanceVille
     *            le nouveau pp lieu naissance ville
     */
    void setPpLieuNaissanceVille(String ppLieuNaissanceVille);

    /**
     * Setter de l'attribut declarationSociale.
     * 
     * @param declarationSociale
     *            la nouvelle valeur de declarationSociale
     */
    void setDeclarationSociale(DeclarationSocialeBean declarationSociale);

    /**
     * Setter de l'attribut dirigeantPresentStatuts.
     * 
     * @param dirigeantPresentStatuts
     *            la nouvelle valeur de dirigeantPresentStatuts
     */

    void setDirigeantPresentStatuts(String dirigeantPresentStatuts);

    /**
     * Setter de l'attribut inscriptionCACPubliee.
     * 
     * @param inscriptionCACPubliee
     *            la nouvelle valeur de inscriptionCACPubliee
     */
    void setInscriptionCACPubliee(String inscriptionCACPubliee);

    /**
     * Setter de l'attribut pmDenomination.
     * 
     * @param pmDenomination
     *            la nouvelle valeur de pmDenomination
     */
    void setPmDenomination(String pmDenomination);

    /**
     * Setter de l'attribut pmFormeJuridique.
     * 
     * @param pmFormeJuridique
     *            la nouvelle valeur de pmFormeJuridique
     */
    void setPmFormeJuridique(String pmFormeJuridique);

    /**
     * Setter de l'attribut pmLieuImmatriculation.
     * 
     * @param pmLieuImmatriculation
     *            la nouvelle valeur de pmLieuImmatriculation
     */
    void setPmLieuImmatriculation(String pmLieuImmatriculation);

    /**
     * Setter de l'attribut pmNumeroIdentification.
     * 
     * @param pmNumeroIdentification
     *            la nouvelle valeur de pmNumeroIdentification
     */
    void setPmNumeroIdentification(String pmNumeroIdentification);

    /**
     * Setter de l'attribut pmRepresentantAdresse.
     * 
     * @param pmRepresentantAdresse
     *            la nouvelle valeur de pmRepresentantAdresse
     */
    void setPmRepresentantAdresse(IAdresse pmRepresentantAdresse);

    /**
     * Setter de l'attribut pmRepresentantCivilite.
     * 
     * @param pmRepresentantCivilite
     *            la nouvelle valeur de pmRepresentantCivilite
     */
    void setPmRepresentantCivilite(String pmRepresentantCivilite);

    /**
     * Setter de l'attribut pmRepresentantDateNaissance.
     * 
     * @param pmRepresentantDateNaissance
     *            la nouvelle valeur de pmRepresentantDateNaissance
     */
    void setPmRepresentantDateNaissance(String pmRepresentantDateNaissance);

    /**
     * Setter de l'attribut pmRepresentantLieuNaissanceCommune.
     * 
     * @param pmRepresentantLieuNaissanceCommune
     *            la nouvelle valeur de pmRepresentantLieuNaissanceCommune
     */
    void setPmRepresentantLieuNaissanceCommune(String pmRepresentantLieuNaissanceCommune);

    /**
     * Setter de l'attribut pmRepresentantLieuNaissanceDepartement.
     * 
     * @param pmRepresentantLieuNaissanceDepartement
     *            la nouvelle valeur de pmRepresentantLieuNaissanceDepartement
     */
    void setPmRepresentantLieuNaissanceDepartement(String pmRepresentantLieuNaissanceDepartement);

    /**
     * Setter de l'attribut pmRepresentantLieuNaissancePays.
     * 
     * @param pmRepresentantLieuNaissancePays
     *            la nouvelle valeur de pmRepresentantLieuNaissancePays
     */
    void setPmRepresentantLieuNaissancePays(String pmRepresentantLieuNaissancePays);

    /**
     * Setter de l'attribut pmRepresentantLieuNaissanceVille.
     * 
     * @param pmRepresentantLieuNaissanceVille
     *            la nouvelle valeur de pmRepresentantLieuNaissanceVille
     */
    void setPmRepresentantLieuNaissanceVille(String pmRepresentantLieuNaissanceVille);

    /**
     * Setter de l'attribut pmRepresentantNationalite.
     * 
     * @param pmRepresentantNationalite
     *            la nouvelle valeur de pmRepresentantNationalite
     */
    void setPmRepresentantNationalite(String pmRepresentantNationalite);

    /**
     * Setter de l'attribut pmRepresentantNomNaissance.
     * 
     * @param pmRepresentantNomNaissance
     *            la nouvelle valeur de pmRepresentantNomNaissance
     */
    void setPmRepresentantNomNaissance(String pmRepresentantNomNaissance);

    /**
     * Setter de l'attribut pmRepresentantNomUsage.
     * 
     * @param pmRepresentantNomUsage
     *            la nouvelle valeur de pmRepresentantNomUsage
     */
    void setPmRepresentantNomUsage(String pmRepresentantNomUsage);

    /**
     * Setter de l'attribut pmRepresentantPMPrenom1.
     * 
     * @param pmRepresentantPMPrenom1
     *            la nouvelle valeur de pmRepresentantPMPrenom1
     */
    void setPmRepresentantPMPrenom1(String pmRepresentantPMPrenom1);

    /**
     * Setter de l'attribut pmRepresentantPMPrenom2.
     * 
     * @param pmRepresentantPMPrenom2
     *            la nouvelle valeur de pmRepresentantPMPrenom2
     */
    void setPmRepresentantPMPrenom2(String pmRepresentantPMPrenom2);

    /**
     * Setter de l'attribut pmRepresentantPMPrenom3.
     * 
     * @param pmRepresentantPMPrenom3
     *            la nouvelle valeur de pmRepresentantPMPrenom3
     */
    void setPmRepresentantPMPrenom3(String pmRepresentantPMPrenom3);

    /**
     * Setter de l'attribut pmRepresentantPMPrenom4.
     * 
     * @param pmRepresentantPMPrenom4
     *            la nouvelle valeur de pmRepresentantPMPrenom4
     */
    void setPmRepresentantPMPrenom4(String pmRepresentantPMPrenom4);

    /**
     * Setter de l'attribut pmRepresentantQualite.
     * 
     * @param pmRepresentantQualite
     *            la nouvelle valeur de pmRepresentantQualite
     */
    void setPmRepresentantQualite(String pmRepresentantQualite);

    /**
     * Setter de l'attribut qualite.
     * 
     * @param qualite
     *            la nouvelle valeur de qualite
     */
    void setQualite(String qualite);

    /**
     * Setter de l'attribut libelleQualite.
     * 
     * @param qualite
     *            la nouvelle valeur de qualite
     */
    void setLibelleQualite(String libelleQualite);

    /**
     * Setter de l'attribut conjointPresence.
     * 
     * @param conjointPresence
     *            la nouvelle valeur de conjointPresence
     */
    void setConjointPresence(String conjointPresence);

    /**
     * Setter de l'attribut conjointRegime.
     * 
     * @param conjointRegime
     *            la nouvelle valeur de conjointRegime
     */
    void setConjointRegime(String conjointRegime);

    /**
     * Setter de l'attribut conjointStatut.
     * 
     * @param conjointStatut
     *            la nouvelle valeur de conjointStatut
     */
    void setConjointStatut(String conjointStatut);

}
