package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * L'interface M01.
 */
@ResourceXPath("/M01")
public interface IM01 {

  /**
   * Gets the m011.
   *
   * @return the m011
   */
  @FieldXPath("M01.1")
  String getM011();

  /**
   * Sets the m011.
   *
   * @param m011
   *          the new m011
   */
  void setM011(String m011);

  /**
   * Gets the m012.
   *
   * @return the m012
   */
  @FieldXPath("M01.2")
  String getM012();

  /**
   * Sets the m012.
   *
   * @param m012
   *          the new m012
   */
  void setM012(String m012);

}
