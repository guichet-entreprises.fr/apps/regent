package fr.ge.common.regent.bean.formalite.dialogue.cessation;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import fr.ge.common.regent.bean.formalite.dialogue.creation.AdresseBean;

/**
 * Le Class FormaliteCessationBean.
 */
public class DialogueCessationBean extends AbstractFormaliteCessation<EntrepriseCessationBean> implements IFormaliteCessationVue {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -7461257548699712556L;

    /** Le observations. */
    private String observations;

    /** Le correspondance destinataire. */
    private String correspondanceDestinataire;

    /** Le correspondance adresse. */
    private AdresseBean correspondanceAdresse = new AdresseBean();

    /** Le telephone1. */
    private String telephone1;

    /** Le telephone2. */
    private String telephone2;

    /** Le fax. */
    private String fax;

    /** Le courriel. */
    private String courriel;

    /** Le signataire nom. */
    private String signataireNom;

    /** Le adresse signature. */
    private AdresseBean adresseSignature = new AdresseBean();

    /** le nonDiffusionInformation. */
    private String nonDiffusionInformation;

    /** Le signature. */
    private String signature;

    /** Le signature lieu. */
    private String signatureLieu;

    /** Le signature date. */
    private String signatureDate;

    /** Le code commune activite. */
    private String codeCommuneActivite;

    /**
     * Get le observations.
     * 
     * @return le observations
     */
    public String getObservations() {
        return this.observations;
    }

    /**
     * Set le observations.
     * 
     * @param observations
     *            le nouveau observations
     */
    public void setObservations(final String observations) {
        this.observations = observations;
    }

    /**
     * Get le correspondance destinataire.
     * 
     * @return le correspondance destinataire
     */
    public String getCorrespondanceDestinataire() {
        return this.correspondanceDestinataire;
    }

    /**
     * Set le correspondance destinataire.
     * 
     * @param correspondanceDestinataire
     *            le nouveau correspondance destinataire
     */
    public void setCorrespondanceDestinataire(final String correspondanceDestinataire) {
        this.correspondanceDestinataire = correspondanceDestinataire;
    }

    /**
     * Get le correspondance adresse.
     * 
     * @return the correspondanceAdresse
     */
    public AdresseBean getCorrespondanceAdresse() {
        return this.correspondanceAdresse;
    }

    /**
     * Set le correspondance adresse.
     * 
     * @param correspondanceAdresse
     *            the correspondanceAdresse to set
     */
    public void setCorrespondanceAdresse(final AdresseBean correspondanceAdresse) {
        this.correspondanceAdresse = correspondanceAdresse;
    }

    /**
     * Get le telephone1.
     * 
     * @return le telephone1
     */
    public String getTelephone1() {
        return this.telephone1;
    }

    /**
     * Set le telephone1.
     * 
     * @param telephone1
     *            le nouveau telephone1
     */
    public void setTelephone1(final String telephone1) {
        this.telephone1 = telephone1;
    }

    /**
     * Get le telephone2.
     * 
     * @return le telephone2
     */
    public String getTelephone2() {
        return this.telephone2;
    }

    /**
     * Set le telephone2.
     * 
     * @param telephone2
     *            le nouveau telephone2
     */
    public void setTelephone2(final String telephone2) {
        this.telephone2 = telephone2;
    }

    /**
     * Get le fax.
     * 
     * @return le fax
     */
    public String getFax() {
        return this.fax;
    }

    /**
     * Set le fax.
     * 
     * @param fax
     *            le nouveau fax
     */
    public void setFax(final String fax) {
        this.fax = fax;
    }

    /**
     * Get le courriel.
     * 
     * @return le courriel
     */
    public String getCourriel() {
        return this.courriel;
    }

    /**
     * Set le courriel.
     * 
     * @param courriel
     *            le nouveau courriel
     */
    public void setCourriel(final String courriel) {
        this.courriel = courriel;
    }

    /**
     * Get le signataire nom.
     * 
     * @return le signataire nom
     */
    public String getSignataireNom() {
        return this.signataireNom;
    }

    /**
     * Set le signataire nom.
     * 
     * @param signataireNom
     *            le nouveau signataire nom
     */
    public void setSignataireNom(final String signataireNom) {
        this.signataireNom = signataireNom;
    }

    /**
     * Get le adresse signature.
     * 
     * @return the adresseSignature
     */
    public AdresseBean getAdresseSignature() {
        return this.adresseSignature;
    }

    /**
     * Set le adresse signature.
     * 
     * @param adresseSignature
     *            the adresseSignature to set
     */
    public void setAdresseSignature(final AdresseBean adresseSignature) {
        this.adresseSignature = adresseSignature;
    }

    /**
     * Get le signature.
     * 
     * @return le signature
     */
    public String getSignature() {
        return this.signature;
    }

    /**
     * Set le signature.
     * 
     * @param signature
     *            le nouveau signature
     */
    public void setSignature(final String signature) {
        this.signature = signature;
    }

    /**
     * Accesseur sur l'attribut {@link #nonDiffusionInformation}.
     *
     * @return String nonDiffusionInformation
     */
    public String getNonDiffusionInformation() {
        return nonDiffusionInformation;
    }

    /**
     * Mutateur sur l'attribut {@link #nonDiffusionInformation}.
     *
     * @param nonDiffusionInformation
     *            la nouvelle valeur de l'attribut nonDiffusionInformation
     */
    public void setNonDiffusionInformation(final String nonDiffusionInformation) {
        this.nonDiffusionInformation = nonDiffusionInformation;
    }

    /**
     * Get le signature lieu.
     * 
     * @return le signature lieu
     */
    public String getSignatureLieu() {
        return this.signatureLieu;
    }

    /**
     * Set le signature lieu.
     * 
     * @param signatureLieu
     *            le nouveau signature lieu
     */
    public void setSignatureLieu(final String signatureLieu) {
        this.signatureLieu = signatureLieu;
    }

    /**
     * Get le signature date.
     * 
     * @return le signature date
     */
    public String getSignatureDate() {
        return this.signatureDate;
    }

    /**
     * Set le signature date.
     * 
     * @param signatureDate
     *            le nouveau signature date
     */
    public void setSignatureDate(final String signatureDate) {
        this.signatureDate = signatureDate;
    }

    /**
     * Get le code commune activite.
     * 
     * @return the codeCommuneActivite
     */
    public String getCodeCommuneActivite() {
        return this.codeCommuneActivite;
    }

    /**
     * Set le code commune activite.
     * 
     * @param codeCommuneActivite
     *            the codeCommuneActivite to set
     */
    public void setCodeCommuneActivite(final String codeCommuneActivite) {
        this.codeCommuneActivite = codeCommuneActivite;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EntrepriseCessationBean createNewEntreprise() {
        return new EntrepriseCessationBean();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

}
