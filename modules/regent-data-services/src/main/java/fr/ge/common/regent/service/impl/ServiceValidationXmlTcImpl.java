package fr.ge.common.regent.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.regent.constante.VersionXmlTcEnum;
import fr.ge.common.regent.service.exception.XmlFatalException;
import fr.ge.common.regent.service.exception.XmlInvalidException;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * Service validation XML-TC.
 * 
 * @author mtakerra
 *
 */
public class ServiceValidationXmlTcImpl {

    /** Le logger fonctionnel. */
    private static final Logger LOGGER_FONC = LoggerFactory.getLogger(ServiceValidationXmlTcImpl.class);

    /** Map <version, validateur> pour les validateurs XML-TC. */
    private final Map<String, Validateur> validateurTcMap = new HashMap<String, Validateur>();

    /** La constante RES_DIR, dossier de ressources des xsd. */
    private static final String RES_DIR = ServiceValidationXmlTcImpl.class.getResource("/xsd").getFile();

    /**
     * Constructeur.
     * 
     * @throws IOException
     * @throws FileNotFoundException
     * @throws TechnicalException
     */
    public ServiceValidationXmlTcImpl() throws TechnicalException, FileNotFoundException, IOException {
        super();
        this.initValidatorXmlTc();
    }

    /**
     * Initialise le validateur xml-tc.
     * 
     * @throws IOException
     * @throws FileNotFoundException
     * @throws TechniqueException
     *             le erreur technique exception
     */
    private void initValidatorXmlTc() throws TechnicalException, FileNotFoundException, IOException {
        for (VersionXmlTcEnum versionXmlTc : VersionXmlTcEnum.values()) {
            Validateur validatorXml = new Validateur(RES_DIR + File.separator + versionXmlTc.getXsdMessageXmlTc(), RES_DIR + File.separator + versionXmlTc.getXsdTypeXmlTc());
            this.validateurTcMap.put(versionXmlTc.getVersion(), validatorXml);
        }
    }

    /**
     * Valider un xml-tc selon une version.
     * 
     * @param version
     * @param xmlTc
     */
    public void validateXmlTc(String version, String xmlTc) {
        try {
            // -->Validation XSD
            this.validateurTcMap.get(version).validate(xmlTc);
        } catch (XmlInvalidException e) {
            LOGGER_FONC.warn("Erreur lors de la validation du XML TC", e.getMessage(), e);
            throw new TechnicalException("Erreur de la validation du XML TC", e);
        } catch (XmlFatalException e) {
            LOGGER_FONC.error(e.getMessage(), e);
            throw new TechnicalException(e);
        }
    }

}
