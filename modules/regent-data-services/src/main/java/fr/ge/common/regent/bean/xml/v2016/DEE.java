package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface DEE.
 */
@ResourceXPath("/DEE")
public interface DEE {

  /**
   * Get le e41.
   *
   * @return le e41
   */
  @FieldXPath("E41")
  IE41[] getE41();

  /**
   * Ajoute le to e41.
   *
   * @return le i e41
   */
  IE41 addToE41();

  /**
   * Set le e41.
   *
   * @param E41
   *          le nouveau e41
   */
  void setE41(IE41[] E41);
}
