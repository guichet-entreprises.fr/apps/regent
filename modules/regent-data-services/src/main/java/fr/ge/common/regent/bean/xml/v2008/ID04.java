package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface ID03.
 */
@ResourceXPath("/D04")
public interface ID04 {

  /**
   * Get le D043.
   *
   * @return le D043
   */
  @FieldXPath("D04.3")
  String getD043();

  /**
   * Set le D043.
   *
   * @param D043
   *          le nouveau D043
   */
  void setD043(String D043);

  /**
   * Get le D044.
   *
   * @return le D044
   */
  @FieldXPath("D04.4")
  String getD044();

  /**
   * Set le D044.
   *
   * @param D044
   *          le nouveau D044
   */
  void setD044(String D044);

  /**
   * Get le D045.
   *
   * @return le D045
   */
  @FieldXPath("D04.5")
  String getD045();

  /**
   * Set le D045.
   *
   * @param D045
   *          le nouveau D045
   */
  void setD045(String D045);

  /**
   * Get le D046.
   *
   * @return le D046
   */
  @FieldXPath("D04.6")
  String getD046();

  /**
   * Set le D046.
   *
   * @param D046
   *          le nouveau D046
   */
  void setD046(String D046);

  /**
   * Get le D047.
   *
   * @return le D047
   */
  @FieldXPath("D04.7")
  String getD047();

  /**
   * Set le D047.
   *
   * @param D047
   *          le nouveau D047
   */
  void setD047(String D047);

  /**
   * Get le D048.
   *
   * @return le D048
   */
  @FieldXPath("D04.8")
  String getD048();

  /**
   * Set le D048.
   *
   * @param D048
   *          le nouveau D048
   */
  void setD048(String D048);

  /**
   * Get le D049.
   *
   * @return le D049
   */
  @FieldXPath("D04.9")
  String getD049();

  /**
   * Set le D049.
   *
   * @param D049
   *          le nouveau D049
   */
  void setD049(String D049);

  /**
   * Get le D0410.
   *
   * @return le D0410
   */
  @FieldXPath("D04.10")
  String getD0410();

  /**
   * Set le D0410.
   *
   * @param D0410
   *          le nouveau D0410
   */
  void setD0410(String D0410);

  /**
   * Get le D0411.
   *
   * @return le D0411
   */
  @FieldXPath("D04.11")
  String getD0411();

  /**
   * Set le D0411.
   *
   * @param D0411
   *          le nouveau D0411
   */
  void setD0411(String D0411);

  /**
   * Get le D0412.
   *
   * @return le D0412
   */
  @FieldXPath("D04.12")
  String getD0412();

  /**
   * Set le D0412.
   *
   * @param D0412
   *          le nouveau D0412
   */
  void setD0412(String D0412);

  /**
   * Get le D0413.
   *
   * @return le D0413
   */
  @FieldXPath("D04.13")
  String getD0413();

  /**
   * Set le D0413.
   *
   * @param D0413
   *          le nouveau D0413
   */
  void setD0413(String D0413);

  /**
   * Get le D0414.
   *
   * @return le D0414
   */
  @FieldXPath("D04.14")
  String getD0414();

  /**
   * Set le D0414.
   *
   * @param D0414
   *          le nouveau D0414
   */
  void setD0414(String D0414);

}
