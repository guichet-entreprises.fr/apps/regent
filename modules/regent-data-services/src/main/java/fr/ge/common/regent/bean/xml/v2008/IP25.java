package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;
import org.xmlfield.core.types.XmlString;

/**
 * Le Interface IP25.
 */
@ResourceXPath("/P25")
public interface IP25 {

  /**
   * Get le p252.
   *
   * @return le p252
   */
  @FieldXPath("P25.2")
  String getP252();

  /**
   * Set le p252.
   *
   * @param P252
   *          le nouveau p252
   */
  void setP252(String P252);

  /**
   * Get le p253.
   *
   * @return le p253
   */
  @FieldXPath("P25.3")
  XmlString[] getP253();

  /**
   * Ajoute le to p253.
   *
   * @return le xml string
   */
  XmlString addToP253();

  /**
   * Set le p253.
   *
   * @param P253
   *          le nouveau p253
   */
  void setP253(XmlString[] P253);

  /**
   * Get le p254.
   *
   * @return le p254
   */
  @FieldXPath("P25.4")
  String getP254();

  /**
   * Set le p254.
   *
   * @param P254
   *          le nouveau p254
   */
  void setP254(String P254);

}
