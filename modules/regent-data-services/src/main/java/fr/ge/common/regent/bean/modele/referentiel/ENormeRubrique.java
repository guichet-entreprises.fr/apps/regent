/**
 *
 */
package fr.ge.common.regent.bean.modele.referentiel;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

/**
 * @author $Author: kelmoura $
 * @version $Revision: 0 $
 * 
 *          La classe qui représente le bean de présent dans la table
 *          norme_rubriques
 */

@Entity
@Proxy(lazy = false)
@Table(name = "norme_rubriques")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class ENormeRubrique implements Serializable {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -7991276121433038694L;

    /** Le id. */
    @Id
    @Column(name = "CH_NUM", nullable = false)
    private Integer id;

    /** Le rubrique. */
    @Column(name = "CH_RUBRIQUE", nullable = false)
    private String rubrique;

    /** Le libelle. */
    @Column(name = "CH_LIBELLE", nullable = true)
    private String libelle;

    /** Le path. */
    @Column(name = "CH_PATH", nullable = false)
    private String path;

    /** Le group, should be 4 caracters example: "G001". */
    @Column(name = "CH_GROUPE", nullable = true)
    private String groupe;

    /** La condition, can be "VRAI" or "FAUX". */
    @Column(name = "CH_CONDITION", nullable = true)
    private String condition;

    /**
     * Accesseur sur l'attribut {@link #id}.
     *
     * @return Integer id
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * Mutateur sur l'attribut {@link #id}.
     *
     * @param id
     *            la nouvelle valeur de l'attribut id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Accesseur sur l'attribut {@link #rubrique}.
     *
     * @return String rubrique
     */
    public String getRubrique() {
        return this.rubrique;
    }

    /**
     * Mutateur sur l'attribut {@link #rubrique}.
     *
     * @param rubrique
     *            la nouvelle valeur de l'attribut rubrique
     */
    public void setRubrique(String rubrique) {
        this.rubrique = rubrique;
    }

    /**
     * Accesseur sur l'attribut {@link #libelle}.
     *
     * @return String libelle
     */
    public String getLibelle() {
        return this.libelle;
    }

    /**
     * Mutateur sur l'attribut {@link #libelle}.
     *
     * @param libelle
     *            la nouvelle valeur de l'attribut libelle
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /**
     * Accesseur sur l'attribut {@link #path}.
     *
     * @return String path
     */
    public String getPath() {
        return this.path;
    }

    /**
     * Mutateur sur l'attribut {@link #path}.
     *
     * @param path
     *            la nouvelle valeur de l'attribut path
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * Accesseur sur l'attribut {@link #groupe}.
     *
     * @return String groupe
     */
    public String getGroupe() {
        return this.groupe;
    }

    /**
     * Mutateur sur l'attribut {@link #groupe}.
     *
     * @param groupe
     *            la nouvelle valeur de l'attribut groupe
     */
    public void setGroupe(String groupe) {
        this.groupe = groupe;
    }

    /**
     * Accesseur sur l'attribut {@link #condition}.
     *
     * @return String condition
     */
    public String getCondition() {
        return this.condition;
    }

    /**
     * Mutateur sur l'attribut {@link #condition}.
     *
     * @param condition
     *            la nouvelle valeur de l'attribut condition
     */
    public void setCondition(String condition) {
        this.condition = condition;
    }

    /**
     * Accesseur sur l'attribut {@link #serialversionuid}.
     *
     * @return long serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }
}
