package fr.ge.common.regent.bean.xml.formalite.dialogue.regularisation;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;
import org.xmlfield.core.types.XmlString;

import fr.ge.common.regent.bean.xml.formalite.IFormalite;
import fr.ge.common.regent.bean.xml.transverse.IAdresse;
import fr.ge.common.regent.bean.xml.transverse.IPostalCommune;

/**
 * La classe XML Field de Formalité Régularisation.
 * 
 * @author roveda
 *
 */
@ResourceXPath("/formalite")
public interface IDirigeantRegularisation extends IFormalite {

    /** La constante MODEL_VERSION. */
    static final int MODEL_VERSION = 1;

    /**
     * Get le pp nom naissance.
     *
     * @return le pp nom naissance
     */
    @FieldXPath(value = "ppNomNaissance")
    String getPpNomNaissance();

    /**
     * Get le pp nom usage.
     *
     * @return le pp nom usage
     */
    @FieldXPath(value = "ppNomUsage")
    String getPpNomUsage();

    /**
     * Get le pp prenom1.
     *
     * @return le pp prenom1
     */
    @FieldXPath(value = "ppPrenom1")
    String getPpPrenom1();

    /**
     * Get le pp prenom2.
     *
     * @return le pp prenom2
     */
    @FieldXPath(value = "ppPrenom2")
    String getPpPrenom2();

    /**
     * Get le pp prenom3.
     *
     * @return le pp prenom3
     */
    @FieldXPath(value = "ppPrenom3")
    String getPpPrenom3();

    /**
     * Get le pp prenom4.
     *
     * @return le pp prenom4
     */
    @FieldXPath(value = "ppPrenom4")
    String getPpPrenom4();

    /**
     * Get le pp pseudonyme.
     *
     * @return le pp pseudonyme
     */
    @FieldXPath(value = "ppPseudonyme")
    String getPpPseudonyme();

    /**
     * Get le pp nationalite.
     *
     * @return le pp nationalite
     */
    @FieldXPath(value = "ppNationalite")
    String getPpNationalite();

    /**
     * Get le pp civilite.
     *
     * @return le pp civilite
     */
    @FieldXPath(value = "ppCivilite")
    String getPpCivilite();

    /**
     * Get le pp date naissance.
     *
     * @return le pp date naissance
     */
    @FieldXPath(value = "ppDateNaissance")
    String getPpDateNaissance();

    /**
     * Get le pp lieu naissance pays.
     *
     * @return le pp lieu naissance pays
     */
    @FieldXPath(value = "ppLieuNaissancePays")
    String getPpLieuNaissancePays();

    /**
     * Get le pp lieu naissance departement.
     *
     * @return le pp lieu naissance departement
     */
    @FieldXPath(value = "ppLieuNaissanceDepartement")
    String getPpLieuNaissanceDepartement();

    /**
     * Get le pp lieu naissance commune.
     *
     * @return le pp lieu naissance commune
     */
    @FieldXPath(value = "ppLieuNaissanceCommune")
    String getPpLieuNaissanceCommune();

    /**
     * Get le pp lieu naissance ville.
     *
     * @return le pp lieu naissance ville
     */
    @FieldXPath(value = "ppLieuNaissanceVille")
    String getPpLieuNaissanceVille();

    /**
     * Get le modif identite nom prenom.
     *
     * @return le modif identite nom prenom
     */
    @FieldXPath(value = "modifIdentiteNomPrenom")
    XmlString[] getModifIdentiteNomPrenom();

    /**
     * Get le modif date identite.
     *
     * @return le modif date identite
     */
    @FieldXPath(value = "modifDateIdentite")
    String getModifDateIdentite();

    /**
     * Get le modif ancien nom naissance.
     *
     * @return le modif ancien nom naissance
     */
    @FieldXPath(value = "modifAncienNomNaissance")
    String getModifAncienNomNaissance();

    /**
     * Get le modif ancien nom usage.
     *
     * @return le modif ancien nom usage
     */
    @FieldXPath(value = "modifAncienNomUsage")
    String getModifAncienNomUsage();

    /**
     * Get le modif ancien prenoms1.
     *
     * @return le modif ancien prenoms1
     */
    @FieldXPath(value = "modifAncienPrenoms1")
    String getModifAncienPrenoms1();

    /**
     * Get le modif ancien prenoms2.
     *
     * @return le modif ancien prenoms2
     */
    @FieldXPath(value = "modifAncienPrenoms2")
    String getModifAncienPrenoms2();

    /**
     * Get le modif ancien prenoms3.
     *
     * @return le modif ancien prenoms3
     */
    @FieldXPath(value = "modifAncienPrenoms3")
    String getModifAncienPrenoms3();

    /**
     * Get le modif ancien prenoms4.
     *
     * @return le modif ancien prenoms4
     */
    @FieldXPath(value = "modifAncienPrenoms4")
    String getModifAncienPrenoms4();

    /**
     * Get le modif anciennenationalite.
     *
     * @return le modif anciennenationalite
     */
    @FieldXPath(value = "modifAnciennenationalite")
    String getModifAnciennenationalite();

    /**
     * Get le pp mineur emancipe.
     *
     * @return le pp mineur emancipe
     */
    @FieldXPath(value = "ppMineurEmancipe")
    String getPpMineurEmancipe();

    /**
     * Get le conjoint presence.
     *
     * @return le conjoint presence
     */
    @FieldXPath(value = "conjointPresence")
    String getConjointPresence();

    /**
     * Get le conjoint regime.
     *
     * @return le conjoint regime
     */
    @FieldXPath(value = "conjointRegime")
    String getConjointRegime();

    /**
     * Get le conjoint statut.
     *
     * @return le conjoint statut
     */
    @FieldXPath(value = "conjointStatut")
    String getConjointStatut();

    /**
     * Set le pp nom naissance.
     *
     * @param ppNomNaissance
     *            le nouveau pp nom naissance
     */
    void setPpNomNaissance(String ppNomNaissance);

    /**
     * Set le pp nom usage.
     *
     * @param ppNomUsage
     *            le nouveau pp nom usage
     */
    void setPpNomUsage(String ppNomUsage);

    /**
     * Set le pp prenom1.
     *
     * @param ppPrenom1
     *            le nouveau pp prenom1
     */
    void setPpPrenom1(String ppPrenom1);

    /**
     * Set le pp prenom2.
     *
     * @param ppPrenom2
     *            le nouveau pp prenom2
     */
    void setPpPrenom2(String ppPrenom2);

    /**
     * Set le pp prenom3.
     *
     * @param ppPrenom3
     *            le nouveau pp prenom3
     */
    void setPpPrenom3(String ppPrenom3);

    /**
     * Set le pp prenom4.
     *
     * @param ppPrenom4
     *            le nouveau pp prenom4
     */
    void setPpPrenom4(String ppPrenom4);

    /**
     * Set le pp pseudonyme.
     *
     * @param ppPseudonyme
     *            le nouveau pp pseudonyme
     */
    void setPpPseudonyme(String ppPseudonyme);

    /**
     * Set le pp nationalite.
     *
     * @param ppNationalite
     *            le nouveau pp nationalite
     */
    void setPpNationalite(String ppNationalite);

    /**
     * Set le pp civilite.
     *
     * @param ppCivilite
     *            le nouveau pp civilite
     */
    void setPpCivilite(String ppCivilite);

    /**
     * Set le pp date naissance.
     *
     * @param ppDateNaissance
     *            le nouveau pp date naissance
     */
    void setPpDateNaissance(String ppDateNaissance);

    /**
     * Set le pp lieu naissance pays.
     *
     * @param ppLieuNaissancePays
     *            le nouveau pp lieu naissance pays
     */
    void setPpLieuNaissancePays(String ppLieuNaissancePays);

    /**
     * Set le pp lieu naissance departement.
     *
     * @param ppLieuNaissanceDepartement
     *            le nouveau pp lieu naissance departement
     */
    void setPpLieuNaissanceDepartement(String ppLieuNaissanceDepartement);

    /**
     * Set le pp lieu naissance commune.
     *
     * @param ppLieuNaissanceCommune
     *            le nouveau pp lieu naissance commune
     */
    void setPpLieuNaissanceCommune(String ppLieuNaissanceCommune);

    /**
     * Set le pp lieu naissance ville.
     *
     * @param ppLieuNaissanceVille
     *            le nouveau pp lieu naissance ville
     */
    void setPpLieuNaissanceVille(String ppLieuNaissanceVille);

    /**
     * Set le modif identite nom prenom.
     *
     * @param modifIdentiteNomPrenom
     *            le nouveau modif identite nom prenom
     */
    void setModifIdentiteNomPrenom(XmlString[] modifIdentiteNomPrenom);

    /**
     * Retire le from modif identite nom prenom.
     *
     * @param modifIdentiteNomPrenom
     *            le modif identite nom prenom
     */
    void removeFromModifIdentiteNomPrenom(XmlString modifIdentiteNomPrenom);

    /**
     * Ajoute le to modif identite nom prenom.
     *
     * @return le xml string
     */
    XmlString addToModifIdentiteNomPrenom();

    /**
     * Set le modif date identite.
     *
     * @param modifDateIdentite
     *            le nouveau modif date identite
     */
    void setModifDateIdentite(String modifDateIdentite);

    /**
     * Set le modif ancien nom naissance.
     *
     * @param modifAncienNomNaissance
     *            le nouveau modif ancien nom naissance
     */
    void setModifAncienNomNaissance(String modifAncienNomNaissance);

    /**
     * Set le modif ancien nom usage.
     *
     * @param modifAncienNomUsage
     *            le nouveau modif ancien nom usage
     */
    void setModifAncienNomUsage(String modifAncienNomUsage);

    /**
     * Set le modif ancien prenoms1.
     *
     * @param modifAncienPrenoms1
     *            le nouveau modif ancien prenoms1
     */
    void setModifAncienPrenoms1(String modifAncienPrenoms1);

    /**
     * Set le modif ancien prenoms2.
     *
     * @param modifAncienPrenoms2
     *            le nouveau modif ancien prenoms2
     */
    void setModifAncienPrenoms2(String modifAncienPrenoms2);

    /**
     * Set le modif ancien prenoms3.
     *
     * @param modifAncienPrenoms3
     *            le nouveau modif ancien prenoms3
     */
    void setModifAncienPrenoms3(String modifAncienPrenoms3);

    /**
     * Set le modif ancien prenoms4.
     *
     * @param modifAncienPrenoms4
     *            le nouveau modif ancien prenoms4
     */
    void setModifAncienPrenoms4(String modifAncienPrenoms4);

    /**
     * Set le modif anciennenationalite.
     *
     * @param modifAnciennenationalite
     *            le nouveau modif anciennenationalite
     */
    void setModifAnciennenationalite(String modifAnciennenationalite);

    /**
     * Set le pp mineur emancipe.
     *
     * @param ppMineurEmancipe
     *            le nouveau pp mineur emancipe
     */
    void setPpMineurEmancipe(String ppMineurEmancipe);

    /**
     * Set le conjoint presence.
     *
     * @param conjointPresence
     *            le nouveau conjoint presence
     */
    void setConjointPresence(String conjointPresence);

    /**
     * Set le conjoint regime.
     *
     * @param conjointRegime
     *            le nouveau conjoint regime
     */
    void setConjointRegime(String conjointRegime);

    /**
     * Set le conjoint statut.
     *
     * @param conjointStatut
     *            le nouveau conjoint statut
     */
    void setConjointStatut(String conjointStatut);

    /**
     * Get le conjoint.
     *
     * @return le conjoint
     */
    @FieldXPath(value = "conjoint")
    IConjointRegularisation getConjoint();

    /**
     * New conjoint.
     *
     * @return le i conjoint regularisation
     */
    IConjointRegularisation newConjoint();

    /**
     * Set le conjoint.
     *
     * @param conjoint
     *            le nouveau conjoint
     */
    void setConjoint(IConjointRegularisation conjoint);

    /**
     * Get le pp adresse forain.
     *
     * @return le pp adresse forain
     */
    @FieldXPath(value = "ppAdresseForain")
    IPostalCommune getPpAdresseForain();

    /**
     * New pp adresse forain.
     *
     * @return le i postal commune
     */
    IPostalCommune newPpAdresseForain();

    /**
     * Set le pp adresse forain.
     *
     * @param ppAdresseForain
     *            le nouveau pp adresse forain
     */
    void setPpAdresseForain(IPostalCommune ppAdresseForain);

    /**
     * Get le pp adresse ambulant.
     *
     * @return le pp adresse ambulant
     */
    @FieldXPath(value = "ppAdresseAmbulant")
    IPostalCommune getPpAdresseAmbulant();

    /**
     * New pp adresse ambulant.
     *
     * @return le i postal commune
     */
    IPostalCommune newPpAdresseAmbulant();

    /**
     * Set le pp adresse ambulant.
     *
     * @param ppAdresseAmbulant
     *            le nouveau pp adresse ambulant
     */
    void setPpAdresseAmbulant(IPostalCommune ppAdresseAmbulant);

    /**
     * Get le pp adresse.
     *
     * @return le pp adresse
     */
    @FieldXPath(value = "ppAdresse")
    IAdresse getPpAdresse();

    /**
     * New pp adresse.
     *
     * @return le i adresse
     */
    IAdresse newPpAdresse();

    /**
     * Set le pp adresse.
     *
     * @param ppAdresse
     *            le nouveau pp adresse
     */
    void setPpAdresse(IAdresse ppAdresse);

    /**
     * Get le modif ancien domicile.
     *
     * @return le modif ancien domicile
     */
    @FieldXPath(value = "modifAncienDomicile")
    IAdresse getModifAncienDomicile();

    /**
     * New modif ancien domicile.
     *
     * @return le i adresse
     */
    IAdresse newModifAncienDomicile();

    /**
     * Set le modif ancien domicile.
     *
     * @param modifAncienDomicile
     *            le nouveau modif ancien domicile
     */
    void setModifAncienDomicile(IAdresse modifAncienDomicile);

}
