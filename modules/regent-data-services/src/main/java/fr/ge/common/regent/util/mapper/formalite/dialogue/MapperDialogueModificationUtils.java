package fr.ge.common.regent.util.mapper.formalite.dialogue;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.collections.CollectionUtils;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xmlfield.core.types.XmlString;

import fr.ge.common.regent.bean.formalite.dialogue.creation.AdresseBean;
import fr.ge.common.regent.bean.formalite.dialogue.modification.ActiviteSoumiseQualificationBean;
import fr.ge.common.regent.bean.formalite.dialogue.modification.ConjointBean;
import fr.ge.common.regent.bean.formalite.dialogue.modification.DialogueModificationBean;
import fr.ge.common.regent.bean.formalite.dialogue.modification.DirigeantBean;
import fr.ge.common.regent.bean.formalite.dialogue.modification.EirlBean;
import fr.ge.common.regent.bean.formalite.dialogue.modification.EntrepriseBean;
import fr.ge.common.regent.bean.formalite.dialogue.modification.EntrepriseLieeBean;
import fr.ge.common.regent.bean.formalite.dialogue.modification.EtablissementBean;
import fr.ge.common.regent.bean.xml.formalite.dialogue.modification.IActiviteSoumiseQualificationModification;
import fr.ge.common.regent.bean.xml.formalite.dialogue.modification.IConjointModification;
import fr.ge.common.regent.bean.xml.formalite.dialogue.modification.IDialogueModification;
import fr.ge.common.regent.bean.xml.formalite.dialogue.modification.IDirigeantModification;
import fr.ge.common.regent.bean.xml.formalite.dialogue.modification.IEirlModification;
import fr.ge.common.regent.bean.xml.formalite.dialogue.modification.IEntrepriseLieeModification;
import fr.ge.common.regent.bean.xml.formalite.dialogue.modification.IEntrepriseModification;
import fr.ge.common.regent.bean.xml.formalite.dialogue.modification.IEtablissementModification;
import fr.ge.common.regent.bean.xml.transverse.IAdresse;
import fr.ge.common.regent.util.mapper.formalite.IMapperFormaliteUtils;

/**
 * Le Class MapperFormaliteModificationUtils.
 */
@Component
public class MapperDialogueModificationUtils implements IMapperFormaliteUtils<IDialogueModification, DialogueModificationBean> {

    /** Le formalite modification mapper. */
    @Autowired
    private Mapper formaliteModificationMapper;

    /** Le adresse mapper. */
    @Autowired
    private Mapper adresseMapper;

    /** Le entreprise modification mapper. */
    @Autowired
    private Mapper entrepriseModificationMapper;

    /** Le dirigeant modification mapper. */
    @Autowired
    private Mapper dirigeantModificationMapper;

    /** Le conjoint modification mapper. */
    @Autowired
    private Mapper conjointModificationMapper;

    /** Le entreprise liee modification mapper. */
    @Autowired
    private Mapper entrepriseLieeModificationMapper;

    /** Le etablissement modification mapper. */
    @Autowired
    private Mapper etablissementModificationMapper;

    /** Le postal commune mapper. */
    @Autowired
    private Mapper postalCommuneMapper;

    /** eirl mapper. */
    @Autowired
    private Mapper eirlModificationMapper;

    /** activiteSoumiseQualificationRegularisationMapper. **/
    @Autowired
    private Mapper activiteSoumiseQualificationRegularisationMapper;

    /**
     * mapper pour préparer un bean depuis une interface (stocké en base).
     *
     * @param iFormaliteModification
     *            le i formalite modification
     * @param formaliteModificationBean
     *            le formalite modification bean
     */
    @Override
    public void mapper(IDialogueModification iFormaliteModification, DialogueModificationBean formaliteModificationBean) {

        // Mapper Adresse signature
        IAdresse iAdresseSignature = iFormaliteModification.getAdresseSignature();
        if (iAdresseSignature != null) {
            AdresseBean adresseSignatureBean = formaliteModificationBean.getAdresseSignature();
            this.adresseMapper.map(iAdresseSignature, adresseSignatureBean);
            if (iAdresseSignature.getCodePostalCommune() != null) {
                this.postalCommuneMapper.map(iAdresseSignature.getCodePostalCommune(), adresseSignatureBean.getCodePostalCommune());
            }

        }

        // Mapper Correspondance Adresse
        IAdresse iCorrespondanceAdresse = iFormaliteModification.getCorrespondanceAdresse();
        if (iCorrespondanceAdresse != null) {
            AdresseBean iCorrAdresseBean = formaliteModificationBean.getCorrespondanceAdresse();
            this.adresseMapper.map(iCorrespondanceAdresse, iCorrAdresseBean);
            if (iCorrespondanceAdresse.getCodePostalCommune() != null) {
                this.postalCommuneMapper.map(iCorrespondanceAdresse.getCodePostalCommune(), iCorrAdresseBean.getCodePostalCommune());
            }
        }
        // Mapper Entreprise
        IEntrepriseModification iEntrepriseModification = iFormaliteModification.getEntreprise();
        if (iEntrepriseModification != null) {
            EntrepriseBean entrepriseBean = formaliteModificationBean.getEntreprise();
            // Mapper Entreprise Liee Dom
            IEntrepriseLieeModification iEntrLieeDom = iEntrepriseModification.getEntrepriseLieeDomiciliation();
            mapperEntrepriseLieeDom(entrepriseBean, iEntrLieeDom);
            // FIN Mapper Entreprise Liee Dom

            // Mapper Entreprise Liee Loueur Mandant
            IEntrepriseLieeModification iEntrLieeLoueurMandant = iEntrepriseModification.getEntrepriseLieeLoueurMandant();
            mapperentrepriseLieeLoueurMdt(entrepriseBean, iEntrLieeLoueurMandant);
            // FIN Mapper Entreprise Liee Loueur Mandant

            IEtablissementModification iEtablissementM = iEntrepriseModification.getEtablissement();
            mapperEtablissement(iEtablissementM, entrepriseBean);

            /* Mapper les listes pour entreprise */

            entrepriseBean.getEntreprisesLieesPrecedentsExploitants().clear();
            // -->SONAR
            List<EntrepriseLieeBean> entrLieePrecedentExploitantList = this.addEntrepriseLieePrecedentsExploitant(iEntrepriseModification);
            entrepriseBean.getEntreprisesLieesPrecedentsExploitants().addAll(entrLieePrecedentExploitantList);

            // Dirigeant
            entrepriseBean.getDirigeants().clear();
            // -->SONAR
            List<DirigeantBean> dirigeants = this.mapEntrepriseDirigeantToDirigeantBean(iEntrepriseModification);
            entrepriseBean.getDirigeants().addAll(dirigeants);

            // Mapper Eirl
            EirlBean eirlBean = entrepriseBean.getEirl();
            if (eirlBean != null) {
                iEntrepriseModification.newEirl();
                this.eirlModificationMapper.map(iEntrepriseModification.getEirl(), eirlBean);
                /* mapper la liste DefinitionPatrimoine de eirl */
                if (eirlBean.getDefinitionPatrimoine() != null) {
                    eirlBean.getDefinitionPatrimoine().clear();
                    XmlString[] modifDefinitionPatrimoines = iEntrepriseModification.getEirl().getDefinitionPatrimoine();
                    for (XmlString modifDefinitionPatrimoine : modifDefinitionPatrimoines) {
                        eirlBean.getDefinitionPatrimoine().add(modifDefinitionPatrimoine.getString());
                    }
                }
            }
            // Fin Mapper EIRl

            // mapping champs simples pour Entreprise
            this.entrepriseModificationMapper.map(iEntrepriseModification, entrepriseBean);

        } // Fin mapping Entreprise

        /* mapping champs simples */
        this.formaliteModificationMapper.map(iFormaliteModification, formaliteModificationBean);

    }

    /**
     * Renvoie entre une liste de EntrepriseLieeBean à partir d'une liste de
     * IEntrepriseLieeModification
     * 
     * @param iEntrepriseModification
     */
    private List<EntrepriseLieeBean> addEntrepriseLieePrecedentsExploitant(IEntrepriseModification iEntrepriseModification) {
        List<EntrepriseLieeBean> entrLieePrecedentExploitantList = new ArrayList<EntrepriseLieeBean>();
        for (IEntrepriseLieeModification iEntrLieePrecExploitant : iEntrepriseModification.getEntreprisesLieesPrecedentsExploitants()) {
            EntrepriseLieeBean entrLieePrecedentExploitantBean = new EntrepriseLieeBean();

            this.entrepriseLieeModificationMapper.map(iEntrLieePrecExploitant, entrLieePrecedentExploitantBean);
            IAdresse iAdresseEntLieeDom = iEntrLieePrecExploitant.getAdresse();
            if (iAdresseEntLieeDom != null) {
                AdresseBean adresseEntLieeDomBean = entrLieePrecedentExploitantBean.getAdresse();
                this.adresseMapper.map(iAdresseEntLieeDom, adresseEntLieeDomBean);
                if (iAdresseEntLieeDom.getCodePostalCommune() != null) {
                    this.postalCommuneMapper.map(iAdresseEntLieeDom.getCodePostalCommune(), adresseEntLieeDomBean.getCodePostalCommune());
                }
            }
            IAdresse iAdresse = iEntrLieePrecExploitant.getAdresse();
            if (iAdresse != null) {
                AdresseBean adresseBean = entrLieePrecedentExploitantBean.getAdresse();
                this.adresseMapper.map(iAdresse, adresseBean);
                if (iAdresse.getCodePostalCommune() != null) {
                    this.postalCommuneMapper.map(iAdresse.getCodePostalCommune(), adresseBean.getCodePostalCommune());
                }
            }

            entrLieePrecedentExploitantList.add(entrLieePrecedentExploitantBean);
        }

        return entrLieePrecedentExploitantList;
    }

    /**
     * Renvoie entre une liste de DirigeantBean à partir d'une liste de
     * IDirigeantModification
     * 
     * @param iEntrepriseModification
     * @return
     */
    private List<DirigeantBean> mapEntrepriseDirigeantToDirigeantBean(IEntrepriseModification iEntrepriseModification) {
        List<DirigeantBean> dirigeants = new ArrayList<DirigeantBean>();
        for (IDirigeantModification iDirigeantM : iEntrepriseModification.getDirigeants()) {
            DirigeantBean dirigeantBean = new DirigeantBean();
            this.dirigeantModificationMapper.map(iDirigeantM, dirigeantBean);
            IConjointModification iConjointM = iDirigeantM.getConjoint();
            if (iConjointM != null) {
                if (dirigeantBean.getConjoint() == null) {
                    dirigeantBean.setConjoint(new ConjointBean());
                }
                this.conjointModificationMapper.map(iConjointM, dirigeantBean.getConjoint());
            }
            IAdresse iPpAdresse = iDirigeantM.getPpAdresse();
            if (iPpAdresse != null) {
                AdresseBean ppAdresseBean = dirigeantBean.getPpAdresse();
                this.adresseMapper.map(iPpAdresse, ppAdresseBean);
                if (iPpAdresse.getCodePostalCommune() != null) {
                    this.postalCommuneMapper.map(iPpAdresse.getCodePostalCommune(), ppAdresseBean.getCodePostalCommune());
                }
            }
            IAdresse iModifAncienDomicile = iDirigeantM.getModifAncienDomicile();
            if (iModifAncienDomicile != null) {
                AdresseBean modifAncienDomicileBean = dirigeantBean.getModifAncienDomicile();
                this.adresseMapper.map(iModifAncienDomicile, modifAncienDomicileBean);
                if (iModifAncienDomicile.getCodePostalCommune() != null) {
                    this.postalCommuneMapper.map(iModifAncienDomicile.getCodePostalCommune(), modifAncienDomicileBean.getCodePostalCommune());
                }
            }
            dirigeantBean.getModifIdentiteNomPrenom().clear();
            XmlString[] modifIdentiteNomPrenom = iDirigeantM.getModifIdentiteNomPrenom();
            for (XmlString iModifIdenNomPrenom : modifIdentiteNomPrenom) {
                dirigeantBean.getModifIdentiteNomPrenom().add(iModifIdenNomPrenom.getString());
            }
            dirigeants.add(dirigeantBean);
        }
        return dirigeants;
    }

    /**
     * Mapper {@link IEtablissementModification} to {@link EtablissementBean}.
     * 
     * @param entrepriseBean
     *            {@link EntrepriseBean}
     * @param iEtablissementM
     *            {@link IEtablissementModification}
     */
    private void mapperEtablissement(IEtablissementModification iEtablissementM, EntrepriseBean entrepriseBean) {
        if (iEtablissementM != null) {
            if (entrepriseBean.getEtablissement() == null) {
                entrepriseBean.setEtablissement(new EtablissementBean());
            }
            this.etablissementModificationMapper.map(iEtablissementM, entrepriseBean.getEtablissement());
            IAdresse iAdresseEtablissement = iEtablissementM.getAdresse();
            if (iAdresseEtablissement != null) {
                AdresseBean adresseEtablissementBean = entrepriseBean.getEtablissement().getAdresse();
                this.adresseMapper.map(iAdresseEtablissement, adresseEtablissementBean);
                if (iAdresseEtablissement.getCodePostalCommune() != null) {
                    this.postalCommuneMapper.map(iAdresseEtablissement.getCodePostalCommune(), adresseEtablissementBean.getCodePostalCommune());
                }
            }

            /* mapper la liste des activités soumises à qualification */
            mapperActivitesSoumisesQualification(iEtablissementM, entrepriseBean);
        }
    }

    /**
     * mapper la liste des activités soumises à qualification.
     * 
     * @param iEtablissementRegularisation
     *            {@link IEtablissementRegularisation}
     * @param entrepriseBean
     *            {@link EntrepriseBean}
     */
    private void mapperActivitesSoumisesQualification(IEtablissementModification iEtablissementM, EntrepriseBean entrepriseBean) {
        if (iEtablissementM.getActivitesSoumisesQualification() != null) {
            if (CollectionUtils.isNotEmpty(entrepriseBean.getEtablissement().getActivitesSoumisesQualification())) {
                entrepriseBean.getEtablissement().getActivitesSoumisesQualification().clear();
            } else {
                entrepriseBean.getEtablissement().setActivitesSoumisesQualification(new ArrayList<ActiviteSoumiseQualificationBean>());
            }
            IActiviteSoumiseQualificationModification[] iActivitesSoumisesQualification = iEtablissementM.getActivitesSoumisesQualification();
            for (IActiviteSoumiseQualificationModification iActiviteSoumiseQualification : iActivitesSoumisesQualification) {
                ActiviteSoumiseQualificationBean activiteSoumiseQualificationBean = new ActiviteSoumiseQualificationBean();
                activiteSoumiseQualificationRegularisationMapper.map(iActiviteSoumiseQualification, activiteSoumiseQualificationBean);
                entrepriseBean.getEtablissement().getActivitesSoumisesQualification().add(activiteSoumiseQualificationBean);
            }
        }
    }

    /**
     * Mapper Entreprise Liee Loueur Mandant.
     * 
     * @param entrepriseBean
     *            entrepriseBean
     * @param iEntrLieeLoueurMandant
     *            iEntrLieeLoueurMandant
     */
    private void mapperentrepriseLieeLoueurMdt(EntrepriseBean entrepriseBean, IEntrepriseLieeModification iEntrLieeLoueurMandant) {
        if (iEntrLieeLoueurMandant != null) {
            if (entrepriseBean.getEntrepriseLieeLoueurMandant() == null) {
                entrepriseBean.setEntrepriseLieeLoueurMandant(new EntrepriseLieeBean());
            }
            this.entrepriseLieeModificationMapper.map(iEntrLieeLoueurMandant, entrepriseBean.getEntrepriseLieeLoueurMandant());
            IAdresse iAdresseLieeLoueur = iEntrLieeLoueurMandant.getAdresse();
            if (iAdresseLieeLoueur != null) {
                AdresseBean adresseLieeLoueurBean = entrepriseBean.getEntrepriseLieeLoueurMandant().getAdresse();
                this.adresseMapper.map(iAdresseLieeLoueur, adresseLieeLoueurBean);
                if (iAdresseLieeLoueur.getCodePostalCommune() != null) {
                    this.postalCommuneMapper.map(iAdresseLieeLoueur.getCodePostalCommune(), adresseLieeLoueurBean.getCodePostalCommune());
                }
            }
        }
    }

    /**
     * Mapper Entreprise Liee Dom.
     * 
     * @param entrepriseBean
     *            entrepriseBean
     * @param iEntrLieeDom
     *            iEntrLieeDom
     */
    private void mapperEntrepriseLieeDom(EntrepriseBean entrepriseBean, IEntrepriseLieeModification iEntrLieeDom) {
        if (iEntrLieeDom != null) {
            if (entrepriseBean.getEntrepriseLieeDomiciliation() == null) {
                entrepriseBean.setEntrepriseLieeDomiciliation(new EntrepriseLieeBean());
            }
            this.entrepriseLieeModificationMapper.map(iEntrLieeDom, entrepriseBean.getEntrepriseLieeDomiciliation());
            IAdresse iAdresseEntLieeDom = iEntrLieeDom.getAdresse();
            if (iAdresseEntLieeDom != null) {
                AdresseBean adresseEntLieeDomBean = entrepriseBean.getEntrepriseLieeDomiciliation().getAdresse();
                this.adresseMapper.map(iAdresseEntLieeDom, adresseEntLieeDomBean);
                if (iAdresseEntLieeDom.getCodePostalCommune() != null) {
                    this.postalCommuneMapper.map(iAdresseEntLieeDom.getCodePostalCommune(), adresseEntLieeDomBean.getCodePostalCommune());
                }
            }
        }
    }

    /**
     * Mapper.
     *
     * @param formaliteModificationBean
     *            le formalite modification bean
     * @param iFormaliteModification
     *            le i formalite modification
     */
    @Override
    public void mapper(DialogueModificationBean formaliteModificationBean, IDialogueModification iFormaliteModification) {

        // Mapper Adresse signature
        iFormaliteModification.newAdresseSignature();
        IAdresse iAdresseSignature = iFormaliteModification.getAdresseSignature();
        this.adresseMapper.map(formaliteModificationBean.getAdresseSignature(), iAdresseSignature);
        iAdresseSignature.newCodePostalCommune();
        this.postalCommuneMapper.map(formaliteModificationBean.getAdresseSignature().getCodePostalCommune(), iAdresseSignature.getCodePostalCommune());

        // Mapper Correspondance Adresse
        iFormaliteModification.newCorrespondanceAdresse();
        IAdresse icorrespondanceAdresse = iFormaliteModification.getCorrespondanceAdresse();
        this.adresseMapper.map(formaliteModificationBean.getCorrespondanceAdresse(), icorrespondanceAdresse);
        icorrespondanceAdresse.newCodePostalCommune();
        this.postalCommuneMapper.map(formaliteModificationBean.getCorrespondanceAdresse().getCodePostalCommune(), icorrespondanceAdresse.getCodePostalCommune());

        // DEBUT- Mapper Entreprise
        iFormaliteModification.newEntreprise();
        IEntrepriseModification iEntreprise = iFormaliteModification.getEntreprise();

        EntrepriseBean entrepriseBean = formaliteModificationBean.getEntreprise();
        if (entrepriseBean != null) {
            // DEBUT- Mapper EntrepriseLieeDomiciliation
            iEntreprise.newEntrepriseLieeDomiciliation();
            IEntrepriseLieeModification iEntrepriseLieeDomiciliation = iEntreprise.getEntrepriseLieeDomiciliation();
            iEntrepriseLieeDomiciliation.newAdresse();
            iEntrepriseLieeDomiciliation.getAdresse().newCodePostalCommune();
            EntrepriseLieeBean entrepriseLieeDomiciliationBean = entrepriseBean.getEntrepriseLieeDomiciliation();
            if (entrepriseLieeDomiciliationBean != null) {
                AdresseBean adresseEntrLieeDomBean = entrepriseLieeDomiciliationBean.getAdresse();
                if (adresseEntrLieeDomBean != null) {
                    this.adresseMapper.map(adresseEntrLieeDomBean, iEntrepriseLieeDomiciliation.getAdresse());
                    this.postalCommuneMapper.map(entrepriseLieeDomiciliationBean.getAdresse().getCodePostalCommune(), iEntrepriseLieeDomiciliation.getAdresse().getCodePostalCommune());
                }
            }
            // FIN - Mapper EntrepriseLieeDomiciliation

            // DEBUT- Mapper EntrepriseLieeLoueurMandant
            iEntreprise.newEntrepriseLieeLoueurMandant();
            IEntrepriseLieeModification iEntrepriseLieeLoueurMandant = iEntreprise.newEntrepriseLieeLoueurMandant();
            iEntrepriseLieeLoueurMandant.newAdresse();
            iEntrepriseLieeLoueurMandant.getAdresse().newCodePostalCommune();

            EntrepriseLieeBean entrepriseLieeLoueurMandantBean = entrepriseBean.getEntrepriseLieeLoueurMandant();
            if (entrepriseLieeLoueurMandantBean != null) {
                AdresseBean adresseEntrLieeLoueurMandantBean = entrepriseLieeLoueurMandantBean.getAdresse();
                if (adresseEntrLieeLoueurMandantBean != null) {
                    this.adresseMapper.map(adresseEntrLieeLoueurMandantBean, iEntrepriseLieeLoueurMandant.getAdresse());
                    this.postalCommuneMapper.map(entrepriseLieeLoueurMandantBean.getAdresse().getCodePostalCommune(), iEntrepriseLieeLoueurMandant.getAdresse().getCodePostalCommune());
                }
            }
            // FIN- Mapper EntrepriseLieeLoueurMandant

            // DEBUT- Mapper Etablissement
            mapperEtablissement(entrepriseBean, iEntreprise);

            // FIN - Mapper Etablissement

            if (!CollectionUtils.isEmpty(entrepriseBean.getDirigeants())) {
                this.addDirigeant(entrepriseBean, iEntreprise);
            }

            // Mapper Eirl
            iEntreprise.newEirl();
            IEirlModification eirl = iEntreprise.getEirl();
            eirl.newModifAdresseEIrl();
            eirl.getModifAdresseEIrl().newCodePostalCommune();
            EirlBean eirlBean = entrepriseBean.getEirl();
            if (eirlBean != null) {
                /** mapper la modif eirl adresse */
                AdresseBean modifAdresseEIrlBean = eirlBean.getModifAdresseEIrl();
                if (modifAdresseEIrlBean != null) {
                    this.adresseMapper.map(modifAdresseEIrlBean, eirl.getModifAdresseEIrl());
                    this.postalCommuneMapper.map(modifAdresseEIrlBean.getCodePostalCommune(), eirl.getModifAdresseEIrl().getCodePostalCommune());
                }
                /** mapper les champs simples de EIRL **/
                this.eirlModificationMapper.map(eirlBean, eirl);
                /** Mapper liste pour bean dirigeant (modifIdentiteNomPrenom) */
                if (!CollectionUtils.isEmpty(eirlBean.getDefinitionPatrimoine())) {
                    this.addEirlDefinitionPatrimoine(eirlBean, eirl);
                }
            }
            // Fin mapper Eirl

            /* Mapper les listes pour entreprise */

            if (!CollectionUtils.isEmpty(entrepriseBean.getEntreprisesLieesPrecedentsExploitants())) {
                this.addEntrepriseLieePrecedentiExploitant(entrepriseBean, iEntreprise);
            }
            /* mapping champs simples EntrepriseModification. */
            this.entrepriseModificationMapper.map(entrepriseBean, iEntreprise);
        }
        // FIN - Mapper Entreprise
        /* mapping champs simples FormaliteModification */
        this.formaliteModificationMapper.map(formaliteModificationBean, iFormaliteModification);

    }

    /**
     * Mapper {@link EtablissementBean} to {@link IEtablissementModification}.
     * 
     * @param entrepriseBean
     *            {@link EntrepriseBean}
     * @param iEntreprise
     *            {@link IEntrepriseLieeModification}
     */
    private void mapperEtablissement(EntrepriseBean entrepriseBean, IEntrepriseModification iEntreprise) {
        iEntreprise.newEtablissement();
        IEtablissementModification iEtablissement = iEntreprise.getEtablissement();
        iEtablissement.newAdresse();
        iEtablissement.getAdresse().newCodePostalCommune();
        EtablissementBean etablissementBean = entrepriseBean.getEtablissement();
        if (etablissementBean != null) {
            AdresseBean adresseEtablissementBean = etablissementBean.getAdresse();
            if (adresseEtablissementBean != null) {
                this.adresseMapper.map(adresseEtablissementBean, iEtablissement.getAdresse());
                this.postalCommuneMapper.map(etablissementBean.getAdresse().getCodePostalCommune(), iEtablissement.getAdresse().getCodePostalCommune());
            }

            iEtablissement.newModifAncienneAdresse1();
            iEtablissement.getModifAncienneAdresse1().newCodePostalCommune();

            AdresseBean modifAncAdr1EtablissementBean = etablissementBean.getModifAncienneAdresse1();
            if (modifAncAdr1EtablissementBean != null) {
                this.adresseMapper.map(modifAncAdr1EtablissementBean, iEtablissement.getModifAncienneAdresse1());
                this.postalCommuneMapper.map(etablissementBean.getModifAncienneAdresse1().getCodePostalCommune(), iEtablissement.getModifAncienneAdresse1().getCodePostalCommune());
            }

        }
        if (etablissementBean != null) {
            this.etablissementModificationMapper.map(etablissementBean, iEtablissement);
        }
        // Mapper liste des activités soumisies à qualification
        if (!CollectionUtils.isEmpty(etablissementBean.getActivitesSoumisesQualification())) {
            this.addActivitesSoumisesQualification(etablissementBean, iEtablissement);
        }
    }

    /**
     * Adds the activites soumises à qualification.
     * 
     * @param etablissementBean
     *            {@link EtablissementBean}
     * @param iEtablissement
     *            {@link IEtablissementModification}
     */
    public void addActivitesSoumisesQualification(EtablissementBean etablissementBean, IEtablissementModification iEtablissement) {
        /*
         * parcourir la liste existants sur l'interface XML et les supprimer
         */
        for (IActiviteSoumiseQualificationModification iActiviteSoumiseQualification : iEtablissement.getActivitesSoumisesQualification()) {
            iEtablissement.removeFromActivitesSoumisesQualification(iActiviteSoumiseQualification);
        }
        /*
         * parcourir la liste des évenements dans le Bean pour les rajouter à
         * l'interface
         */

        for (ActiviteSoumiseQualificationBean activiteSoumiseQualificationBean : etablissementBean.getActivitesSoumisesQualification()) {
            IActiviteSoumiseQualificationModification iActiviteSoumiseQualification = iEtablissement.addToActivitesSoumisesQualification();
            // DEBUT- Mapper iActiviteSoumiseQualification
            // Gestion idTechnique, nécessaire pour la gestion des impacts
            // (écran)
            if (activiteSoumiseQualificationBean.getIdTechnique() == null || activiteSoumiseQualificationBean.getIdTechnique().isEmpty()) {
                activiteSoumiseQualificationBean.setIdTechnique(UUID.randomUUID().toString());
            }

            if (activiteSoumiseQualificationBean != null) {
                this.activiteSoumiseQualificationRegularisationMapper.map(activiteSoumiseQualificationBean, iActiviteSoumiseQualification);
            }
        }
    }

    /**
     * Ajoute le entreprise liee precedenti exploitant.
     *
     * @param entrepriseBean
     *            le entreprise bean
     * @param iEntreprise
     *            le i entreprise
     */
    private void addEntrepriseLieePrecedentiExploitant(EntrepriseBean entrepriseBean, IEntrepriseModification iEntreprise) {

        /*
         * parcourir la liste des EntrepriseLieePrecedentiExploitant existants
         * sur l'interface XML et les supprimer
         */
        IEntrepriseLieeModification[] entrLieePrecedentiExploitantList = iEntreprise.getEntreprisesLieesPrecedentsExploitants();
        for (IEntrepriseLieeModification iEntrLieePrecExploitant : entrLieePrecedentiExploitantList) {
            iEntreprise.removeFromEntreprisesLieesPrecedentsExploitants(iEntrLieePrecExploitant);
        }
        /*
         * parcourir la liste des EntrepriseLieePrecedentiExploitant dans le
         * Bean pour les rajouter à l'interface
         */
        java.util.List<EntrepriseLieeBean> entrLieePrecExploitListBean = entrepriseBean.getEntreprisesLieesPrecedentsExploitants();
        for (EntrepriseLieeBean entrepriseLieePrecedentExploitantBean : entrLieePrecExploitListBean) {
            IEntrepriseLieeModification iEntrepriseLieePrecedentExploitant = iEntreprise.addToEntreprisesLieesPrecedentsExploitants();
            // DEBUT- Mapper iEntrepriseLieePrecedentiExploitant
            // Gestion idTechnique, nécessaire pour la gestion des impacts
            // (écran)
            if (entrepriseLieePrecedentExploitantBean.getIdTechnique() == null || entrepriseLieePrecedentExploitantBean.getIdTechnique().isEmpty()) {
                entrepriseLieePrecedentExploitantBean.setIdTechnique(UUID.randomUUID().toString());
            }

            iEntrepriseLieePrecedentExploitant.newAdresse();
            iEntrepriseLieePrecedentExploitant.getAdresse().newCodePostalCommune();
            if (entrepriseLieePrecedentExploitantBean != null) {
                if (entrepriseLieePrecedentExploitantBean.getAdresse() == null) {
                    entrepriseLieePrecedentExploitantBean.setAdresse(new AdresseBean());
                }
                this.adresseMapper.map(entrepriseLieePrecedentExploitantBean.getAdresse(), iEntrepriseLieePrecedentExploitant.getAdresse());
                this.postalCommuneMapper.map(entrepriseLieePrecedentExploitantBean.getAdresse().getCodePostalCommune(), iEntrepriseLieePrecedentExploitant.getAdresse().getCodePostalCommune());

                this.entrepriseLieeModificationMapper.map(entrepriseLieePrecedentExploitantBean, iEntrepriseLieePrecedentExploitant);
            }
            // FIN - Mapper iEntrepriseLieePrecedentiExploitant
        }

    }

    /**
     * Ajoute le dirigeant.
     *
     * @param entrepriseBean
     *            le entreprise bean
     * @param iEntreprise
     *            le i entreprise
     */
    private void addDirigeant(EntrepriseBean entrepriseBean, IEntrepriseModification iEntreprise) {

        /*
         * parcourir la liste des Dirigeant existants sur l'interface XML et les
         * supprimer
         */
        IDirigeantModification[] iDirigeantList = iEntreprise.getDirigeants();
        for (IDirigeantModification iDirigeantCourant : iDirigeantList) {
            iEntreprise.removeFromDirigeants(iDirigeantCourant);
        }
        /*
         * parcourir la liste des Dirigeant dans le Bean pour les rajouter à
         * l'interface
         */
        java.util.List<DirigeantBean> dirigeantListBean = entrepriseBean.getDirigeants();
        for (DirigeantBean dirigeantBean : dirigeantListBean) {
            // Gestion idTechnique, nécessaire pour la gestion des impacts
            // (écran)
            if (dirigeantBean.getIdTechnique() == null || dirigeantBean.getIdTechnique().isEmpty()) {
                dirigeantBean.setIdTechnique(UUID.randomUUID().toString());
            }

            IDirigeantModification iDirigeant = iEntreprise.addToDirigeants();
            // DEBUT - Mapper Dirigeant
            iDirigeant.newConjoint();
            iDirigeant.getConjoint().newPpAdresse();
            iDirigeant.getConjoint().getPpAdresse().newCodePostalCommune();
            if (dirigeantBean.getConjoint() == null) {
                dirigeantBean.setConjoint(new ConjointBean());
            }
            if (dirigeantBean.getConjoint().getPpAdresse() == null) {
                dirigeantBean.getConjoint().setPpAdresse(new AdresseBean());
            }
            this.adresseMapper.map(dirigeantBean.getConjoint().getPpAdresse(), iDirigeant.getConjoint().getPpAdresse());
            this.postalCommuneMapper.map(dirigeantBean.getConjoint().getPpAdresse().getCodePostalCommune(), iDirigeant.getConjoint().getPpAdresse().getCodePostalCommune());

            this.conjointModificationMapper.map(dirigeantBean.getConjoint(), iDirigeant.getConjoint());

            iDirigeant.newPpAdresse();
            iDirigeant.getPpAdresse().newCodePostalCommune();
            if (dirigeantBean.getPpAdresse() == null) {
                dirigeantBean.setPpAdresse(new AdresseBean());
            }
            this.adresseMapper.map(dirigeantBean.getPpAdresse(), iDirigeant.getPpAdresse());
            this.postalCommuneMapper.map(dirigeantBean.getPpAdresse().getCodePostalCommune(), iDirigeant.getPpAdresse().getCodePostalCommune());

            iDirigeant.newModifAncienDomicile();
            iDirigeant.getModifAncienDomicile().newCodePostalCommune();
            if (dirigeantBean.getModifAncienDomicile() == null) {
                dirigeantBean.setModifAncienDomicile(new AdresseBean());
            }
            this.adresseMapper.map(dirigeantBean.getModifAncienDomicile(), iDirigeant.getModifAncienDomicile());
            this.postalCommuneMapper.map(dirigeantBean.getModifAncienDomicile().getCodePostalCommune(), iDirigeant.getModifAncienDomicile().getCodePostalCommune());

            /* Mapper liste pour bean dirigeant (modifIdentiteNomPrenom) */
            if (!CollectionUtils.isEmpty(dirigeantBean.getModifIdentiteNomPrenom())) {
                this.addModifIdentiteNomPrenom(dirigeantBean, iDirigeant);
            }

            this.dirigeantModificationMapper.map(dirigeantBean, iDirigeant);

            // FIN - Mapper Dirigeant
        }

    }

    /**
     * Ajoute le modif identite nom prenom.
     *
     * @param dirigeantBean
     *            le dirigeant bean
     * @param iDirigeant
     *            le i dirigeant
     */
    private void addModifIdentiteNomPrenom(DirigeantBean dirigeantBean, IDirigeantModification iDirigeant) {
        /*
         * parcourir la liste des modifIdentiteNomPrenom existants sur
         * l'interface XML et les supprimer
         */
        for (XmlString modifIdentiteNomPrenom : iDirigeant.getModifIdentiteNomPrenom()) {
            iDirigeant.removeFromModifIdentiteNomPrenom(modifIdentiteNomPrenom);
        }
        /*
         * parcourir la liste des évenements dans le Bean pour les rajouter à
         * l'interface
         */
        for (String modifIdentiteNomPrenomCourant : dirigeantBean.getModifIdentiteNomPrenom()) {
            XmlString modifIdentiteNomPrenom = iDirigeant.addToModifIdentiteNomPrenom();
            modifIdentiteNomPrenom.setString(modifIdentiteNomPrenomCourant);
        }

    }

    /**
     * Ajoute le mDefinitionPatrimoine
     *
     * @param eirlBean
     *            le eirlBean bean
     * @param ieirl
     *            le i eirlBean
     */
    private void addEirlDefinitionPatrimoine(EirlBean eirlBean, IEirlModification iEirlModification) {
        /*
         * parcourir la liste des modifIdentiteNomPrenom existants sur
         * l'interface XML et les supprimer
         */
        for (XmlString definitionPatrimoine : iEirlModification.getDefinitionPatrimoine()) {
            iEirlModification.removeFromDefinitionPatrimoine(definitionPatrimoine);
        }
        /*
         * parcourir la liste des évenements dans le Bean pour les rajouter à
         * l'interface
         */
        for (String modifIdentiteNomPrenomCourant : eirlBean.getDefinitionPatrimoine()) {
            XmlString modifIdentiteNomPrenom = iEirlModification.addToDefinitionPatrimoine();
            modifIdentiteNomPrenom.setString(modifIdentiteNomPrenomCourant);
        }

    }

    /**
     * Get le adresse mapper.
     *
     * @return the adresseMapper
     */
    public Mapper getAdresseMapper() {
        return this.adresseMapper;
    }

    /**
     * Set le adresse mapper.
     *
     * @param adresseMapper
     *            the adresseMapper to set
     */
    public void setAdresseMapper(Mapper adresseMapper) {
        this.adresseMapper = adresseMapper;
    }

    /**
     * Get le postal commune mapper.
     *
     * @return the postalCommuneMapper
     */
    public Mapper getPostalCommuneMapper() {
        return this.postalCommuneMapper;
    }

    /**
     * Set le postal commune mapper.
     *
     * @param postalCommuneMapper
     *            the postalCommuneMapper to set
     */
    public void setPostalCommuneMapper(Mapper postalCommuneMapper) {
        this.postalCommuneMapper = postalCommuneMapper;
    }

    /**
     * Get le formalite modification mapper.
     *
     * @return the formaliteModificationMapper
     */
    public Mapper getFormaliteModificationMapper() {
        return formaliteModificationMapper;
    }

    /**
     * Set le formalite modification mapper.
     *
     * @param formaliteModificationMapper
     *            the formaliteModificationMapper to set
     */
    public void setFormaliteModificationMapper(Mapper formaliteModificationMapper) {
        this.formaliteModificationMapper = formaliteModificationMapper;
    }

    /**
     * Get le entreprise modification mapper.
     *
     * @return the entrepriseModificationMapper
     */
    public Mapper getEntrepriseModificationMapper() {
        return entrepriseModificationMapper;
    }

    /**
     * Set le entreprise modification mapper.
     *
     * @param entrepriseModificationMapper
     *            the entrepriseModificationMapper to set
     */
    public void setEntrepriseModificationMapper(Mapper entrepriseModificationMapper) {
        this.entrepriseModificationMapper = entrepriseModificationMapper;
    }

    /**
     * Get le dirigeant modification mapper.
     *
     * @return the dirigeantModificationMapper
     */
    public Mapper getDirigeantModificationMapper() {
        return dirigeantModificationMapper;
    }

    /**
     * Set le dirigeant modification mapper.
     *
     * @param dirigeantModificationMapper
     *            the dirigeantModificationMapper to set
     */
    public void setDirigeantModificationMapper(Mapper dirigeantModificationMapper) {
        this.dirigeantModificationMapper = dirigeantModificationMapper;
    }

    /**
     * Get le conjoint modification mapper.
     *
     * @return the conjointModificationMapper
     */
    public Mapper getConjointModificationMapper() {
        return conjointModificationMapper;
    }

    /**
     * Set le conjoint modification mapper.
     *
     * @param conjointModificationMapper
     *            the conjointModificationMapper to set
     */
    public void setConjointModificationMapper(Mapper conjointModificationMapper) {
        this.conjointModificationMapper = conjointModificationMapper;
    }

    /**
     * Get le entreprise liee modification mapper.
     *
     * @return the entrepriseLieeModificationMapper
     */
    public Mapper getEntrepriseLieeModificationMapper() {
        return entrepriseLieeModificationMapper;
    }

    /**
     * Set le entreprise liee modification mapper.
     *
     * @param entrepriseLieeModificationMapper
     *            the entrepriseLieeModificationMapper to set
     */
    public void setEntrepriseLieeModificationMapper(Mapper entrepriseLieeModificationMapper) {
        this.entrepriseLieeModificationMapper = entrepriseLieeModificationMapper;
    }

    /**
     * Get le etablissement modification mapper.
     *
     * @return the etablissementModificationMapper
     */
    public Mapper getEtablissementModificationMapper() {
        return etablissementModificationMapper;
    }

    /**
     * Set le etablissement modification mapper.
     *
     * @param etablissementModificationMapper
     *            the etablissementModificationMapper to set
     */
    public void setEtablissementModificationMapper(Mapper etablissementModificationMapper) {
        this.etablissementModificationMapper = etablissementModificationMapper;
    }

    /**
     * @return the eirlModificationMapper
     */
    public Mapper getEirlModificationMapper() {
        return eirlModificationMapper;
    }

    /**
     * @param eirlModificationMapper
     *            the eirlModificationMapper to set
     */
    public void setEirlModificationMapper(Mapper eirlModificationMapper) {
        this.eirlModificationMapper = eirlModificationMapper;
    }

    /**
     * Accesseur sur l'attribut
     * {@link #activiteSoumiseQualificationRegularisationMapper}.
     *
     * @return Mapper activiteSoumiseQualificationRegularisationMapper
     */
    public Mapper getActiviteSoumiseQualificationRegularisationMapper() {
        return activiteSoumiseQualificationRegularisationMapper;
    }

    /**
     * Mutateur sur l'attribut
     * {@link #activiteSoumiseQualificationRegularisationMapper}.
     *
     * @param activiteSoumiseQualificationRegularisationMapper
     *            la nouvelle valeur de l'attribut
     *            activiteSoumiseQualificationRegularisationMapper
     */
    public void setActiviteSoumiseQualificationRegularisationMapper(Mapper activiteSoumiseQualificationRegularisationMapper) {
        this.activiteSoumiseQualificationRegularisationMapper = activiteSoumiseQualificationRegularisationMapper;
    }

}
