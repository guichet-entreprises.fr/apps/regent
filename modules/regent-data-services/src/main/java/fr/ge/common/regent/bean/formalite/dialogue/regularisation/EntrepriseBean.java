package fr.ge.common.regent.bean.formalite.dialogue.regularisation;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import fr.ge.common.regent.bean.formalite.dialogue.AbstractEntrepriseBean;

/**
 * Le Class EntrepriseBean.
 */
public class EntrepriseBean extends AbstractEntrepriseBean<EtablissementBean, EntrepriseLieeBean, DirigeantBean, PersonneLieeBean> {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -7998540450466394889L;

    /** Le siren. */
    private String siren;

    /** Le eirl declaration. */
    private String eirlDeclaration;

    /** Le eirl registre depot. */
    private String eirlRegistreDepot;

    /** Le eirl registre lieu. */
    private String eirlRegistreLieu;

    /** Le entreprise liees precedents exploitants. */
    private List<EntrepriseLieeBean> entreprisesLieesPrecedentsExploitants = new ArrayList<EntrepriseLieeBean>();

    /** L'adresse entreprise pp situation. */
    private String adresseEntreprisePPSituation;

    /** L'adresse entreprise pm situation. */
    private String adresseEntreprisePMSituation;

    /**
     * Get le siren.
     *
     * @return the siren
     */
    public String getSiren() {
        return this.siren;
    }

    /**
     * Set le siren.
     *
     * @param siren
     *            the siren to set
     */
    public void setSiren(String siren) {
        this.siren = siren;
    }

    /**
     * Get le eirl declaration.
     *
     * @return the eirlDeclaration
     */
    public String getEirlDeclaration() {
        return this.eirlDeclaration;
    }

    /**
     * Set le eirl declaration.
     *
     * @param eirlDeclaration
     *            the eirlDeclaration to set
     */
    public void setEirlDeclaration(String eirlDeclaration) {
        this.eirlDeclaration = eirlDeclaration;
    }

    /**
     * Get le eirl registre depot.
     *
     * @return the eirlRegistreDepot
     */
    public String getEirlRegistreDepot() {
        return this.eirlRegistreDepot;
    }

    /**
     * Set le eirl registre depot.
     *
     * @param eirlRegistreDepot
     *            the eirlRegistreDepot to set
     */
    public void setEirlRegistreDepot(String eirlRegistreDepot) {
        this.eirlRegistreDepot = eirlRegistreDepot;
    }

    /**
     * Get le eirl registre lieu.
     *
     * @return the eirlRegistreLieu
     */
    public String getEirlRegistreLieu() {
        return this.eirlRegistreLieu;
    }

    /**
     * Set le eirl registre lieu.
     *
     * @param eirlRegistreLieu
     *            the eirlRegistreLieu to set
     */
    public void setEirlRegistreLieu(String eirlRegistreLieu) {
        this.eirlRegistreLieu = eirlRegistreLieu;
    }

    /**
     * Accesseur sur l'attribut {@link #entreprisesLieesPrecedentsExploitants}.
     *
     * @return List<EntrepriseLieeBean> entreprisesLieesPrecedentsExploitants
     */
    public List<EntrepriseLieeBean> getEntreprisesLieesPrecedentsExploitants() {
        return entreprisesLieesPrecedentsExploitants;
    }

    /**
     * Mutateur sur l'attribut {@link #entreprisesLieesPrecedentsExploitants}.
     *
     * @param entreprisesLieesPrecedentsExploitants
     *            la nouvelle valeur de l'attribut
     *            entreprisesLieesPrecedentsExploitants
     */
    public void setEntreprisesLieesPrecedentsExploitants(List<EntrepriseLieeBean> entreprisesLieesPrecedentsExploitants) {
        this.entreprisesLieesPrecedentsExploitants = entreprisesLieesPrecedentsExploitants;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EtablissementBean createNewEtablissement() {
        return new EtablissementBean();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EntrepriseLieeBean createNewEntrepriseLiee() {
        return new EntrepriseLieeBean();
    }

    /**
     * Accesseur sur l'attribut {@link #adresseEntreprisePMSituation}.
     *
     * @return String adresseEntreprisePMSituation
     */
    public String getAdresseEntreprisePMSituation() {
        return adresseEntreprisePMSituation;
    }

    /**
     * Mutateur sur l'attribut {@link #adresseEntreprisePMSituation}.
     *
     * @param adresseEntreprisePMSituation
     *            la nouvelle valeur de l'attribut adresseEntreprisePMSituation
     */
    public void setAdresseEntreprisePMSituation(final String adresseEntreprisePMSituation) {
        this.adresseEntreprisePMSituation = adresseEntreprisePMSituation;
    }

    /**
     * Accesseur sur l'attribut {@link #adresseEntreprisePPSituation}.
     *
     * @return String adresseEntreprisePPSituation
     */
    public String getAdresseEntreprisePPSituation() {
        return adresseEntreprisePPSituation;
    }

    /**
     * Mutateur sur l'attribut {@link #adresseEntreprisePPSituation}.
     *
     * @param adresseEntreprisePPSituation
     *            la nouvelle valeur de l'attribut adresseEntreprisePPSituation
     */
    public void setAdresseEntreprisePPSituation(String adresseEntreprisePPSituation) {
        this.adresseEntreprisePPSituation = adresseEntreprisePPSituation;
    }

}
