package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IAE.
 */
@ResourceXPath("IAE")
public interface IAE {

  /**
   * Get le e13.
   *
   * @return le e13
   */
  @FieldXPath("E13")
  IE13[] getE13();

  /**
   * Ajoute le to e13.
   *
   * @return le i e13
   */
  IE13 addToE13();

  /**
   * Set le.
   *
   * @param E13
   *          le e13
   */
  void set(IE13[] E13);

  /**
   * Get le e12.
   *
   * @return le e12
   */
  @FieldXPath("E12")
  IE12[] getE12();

  /**
   * Ajoute le to e12.
   *
   * @return le i e12
   */
  IE12 addToE12();

  /**
   * Set le.
   *
   * @param E12
   *          le e12
   */
  void set(IE12[] E12);

  /**
   * Get le e17.
   *
   * @return le e17
   */
  @FieldXPath("E17")
  String getE17();

  /**
   * Set le e17.
   *
   * @param E17
   *          le nouveau e17
   */
  void setE17(String E17);

  /**
   * Get le e14.
   *
   * @return le e14
   */
  @FieldXPath("E14")
  String getE14();

  /**
   * Set le e14.
   *
   * @param E14
   *          le nouveau e14
   */
  void setE14(String E14);

}
