package fr.ge.common.regent.util.mapper.formalite.profil;

import java.util.ArrayList;

import org.apache.commons.collections.CollectionUtils;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xmlfield.core.types.XmlString;

import fr.ge.common.regent.bean.formalite.dialogue.creation.PostalCommuneBean;
import fr.ge.common.regent.bean.formalite.profil.cessation.ProfilCessationBean;
import fr.ge.common.regent.bean.formalite.profil.creation.CfeBean;
import fr.ge.common.regent.bean.xml.formalite.profil.cessation.IProfilCessation;
import fr.ge.common.regent.util.mapper.formalite.IMapperFormaliteUtils;

/**
 * Le Class MapperProfilCessationUtils.
 */
@Component
public class MapperProfilCessationUtils implements IMapperFormaliteUtils<IProfilCessation, ProfilCessationBean> {

    /** Le profil entreprise c mapper. */
    @Autowired
    private Mapper profilEntrepriseCMapper;

    /**
     * Mapper.
     *
     * @param iProfilEntrepriseCessation
     *            le i profil entreprise cessation
     * @param profilCessationBean
     *            le profil cessation bean
     */
    public void mapper(IProfilCessation iProfilEntrepriseCessation, ProfilCessationBean profilCessationBean) {
        // Instanciation des attributs de type complexe
        profilCessationBean.setCfe(new CfeBean());
        profilCessationBean.setPostalCommune(new PostalCommuneBean());

        profilCessationBean.setEvenement(new ArrayList<String>());

        // Mapping des champs simples
        this.getProfilEntrepriseCMapper().map(iProfilEntrepriseCessation, profilCessationBean);

        // Traitement des Listes (arrays)
        if (iProfilEntrepriseCessation.getEvenement() != null && iProfilEntrepriseCessation.getEvenement().length > 0) {
            for (XmlString evenement : iProfilEntrepriseCessation.getEvenement()) {
                profilCessationBean.getEvenement().add(evenement.getString());
            }
        }

    }

    /**
     * Mapper.
     *
     * @param profilCessationBean
     *            le profil cessation bean
     * @param iProfilEntrepriseCessation
     *            le i profil entreprise cessation
     */
    public void mapper(ProfilCessationBean profilCessationBean, IProfilCessation iProfilEntrepriseCessation) {
        /* Ajouter les objets */
        iProfilEntrepriseCessation.newCfe();
        iProfilEntrepriseCessation.newPostalCommune();

        /* Mapper les champs simples. */
        this.getProfilEntrepriseCMapper().map(profilCessationBean, iProfilEntrepriseCessation);

        /* Ajouter les évenemnts */
        if (!CollectionUtils.isEmpty(profilCessationBean.getEvenement())) {
            this.addEvenements(profilCessationBean, iProfilEntrepriseCessation);
        }

    }

    /**
     * Ajoute le evenements.
     *
     * @param profilCessationBean
     *            le profil cessation bean
     * @param iProfilEntrepriseCessation
     *            le i profil entreprise cessation
     */
    private void addEvenements(ProfilCessationBean profilCessationBean, IProfilCessation iProfilEntrepriseCessation) {
        /*
         * parcourir la liste des évenements existants sur l'interface XML et
         * les supprimer
         */
        for (XmlString evenement : iProfilEntrepriseCessation.getEvenement()) {
            iProfilEntrepriseCessation.removeFromEvenement(evenement);
        }
        /*
         * parcourir la liste des évenements dans le Bean pour les rajouter à
         * l'interface
         */
        for (String evenement : profilCessationBean.getEvenement()) {
            XmlString evenemntNew = iProfilEntrepriseCessation.addToEvenement();
            evenemntNew.setString(evenement);
        }

    }

    /**
     * Get le profil entreprise c mapper.
     *
     * @return le profil entreprise c mapper
     */
    public Mapper getProfilEntrepriseCMapper() {
        return profilEntrepriseCMapper;
    }

    /**
     * Set le profil entreprise c mapper.
     *
     * @param profilEntrepriseCMapper
     *            le nouveau profil entreprise c mapper
     */
    public void setProfilEntrepriseCMapper(Mapper profilEntrepriseCMapper) {
        this.profilEntrepriseCMapper = profilEntrepriseCMapper;
    }

}
