package fr.ge.common.regent.bean.modele.referentiel;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

/**
 * Le Class ENormeFlux.
 */
@Entity
@Proxy(lazy = false)
@Table(name = "normeflux")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class ENormeFlux implements Serializable {

  /** La constante serialVersionUID. */
  private static final long serialVersionUID = -6391217813023356782L;

  /** Le id. */
  @Id
  @Column(name = "CH_NUM", nullable = false)
  private String id;

  /** Le poid. */
  @Column(name = "CH_POID", nullable = false)
  private Integer poid;

  /** Le evenement. */
  @Column(name = "CH_EVENEMENT", nullable = true)
  private String evenement;

  /** Le flux cci. */
  @Column(name = "CH_CCI", nullable = true)
  private String fluxCci;

  /** Le flux cma. */
  @Column(name = "CH_CMA", nullable = true)
  private String fluxCma;

  /** Le flux urssaf. */
  @Column(name = "CH_URSSAF", nullable = true)
  private String fluxUrssaf;

  /** Le flux greffe. */
  @Column(name = "CH_GREFFE", nullable = true)
  private String fluxGreffe;

  /** Le flux cnba. */
  @Column(name = "CH_CNBA", nullable = true)
  private String fluxCnba;

  /** Le flux ca. */
  @Column(name = "CH_CA", nullable = true)
  private String fluxCa;

  /** Le flux ca. */
  @Column(name = "CH_AGTCOM", nullable = true)
  private String fluxAgtcom;

  /** Le role. */
  @Column(name = "CH_ROLE")
  private String role;

  /** Le type (Rég/Modf/Cess). */
  @Column(name = "CH_TYPE")
  private String type;

  /** Le libellé opération en fonction de l'évenement et du type. **/
  @Column(name = "CH_LIBELLE")
  private String libelle;

  /**
   * Get le id.
   *
   * @return le id
   */
  public String getId() {
    return id;
  }

  /**
   * Set le id.
   *
   * @param id
   *          le nouveau id
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Get le poid.
   *
   * @return le poid
   */
  public Integer getPoid() {
    return poid;
  }

  /**
   * Set le poid.
   *
   * @param poid
   *          le nouveau poid
   */
  public void setPoid(Integer poid) {
    this.poid = poid;
  }

  /**
   * Get le evenement.
   *
   * @return le evenement
   */
  public String getEvenement() {
    return evenement;
  }

  /**
   * Set le evenement.
   *
   * @param evenement
   *          le nouveau evenement
   */
  public void setEvenement(String evenement) {
    this.evenement = evenement;
  }

  /**
   * Get le flux cci.
   *
   * @return le flux cci
   */
  public String getFluxCci() {
    return fluxCci;
  }

  /**
   * Set le flux cci.
   *
   * @param fluxCci
   *          le nouveau flux cci
   */
  public void setFluxCci(String fluxCci) {
    this.fluxCci = fluxCci;
  }

  /**
   * Get le flux cma.
   *
   * @return le flux cma
   */
  public String getFluxCma() {
    return fluxCma;
  }

  /**
   * Set le flux cma.
   *
   * @param fluxCma
   *          le nouveau flux cma
   */
  public void setFluxCma(String fluxCma) {
    this.fluxCma = fluxCma;
  }

  /**
   * Get le flux urssaf.
   *
   * @return le flux urssaf
   */
  public String getFluxUrssaf() {
    return fluxUrssaf;
  }

  /**
   * Set le flux urssaf.
   *
   * @param fluxUrssaf
   *          le nouveau flux urssaf
   */
  public void setFluxUrssaf(String fluxUrssaf) {
    this.fluxUrssaf = fluxUrssaf;
  }

  /**
   * Get le flux greffe.
   *
   * @return le flux greffe
   */
  public String getFluxGreffe() {
    return fluxGreffe;
  }

  /**
   * Set le flux greffe.
   *
   * @param fluxGreffe
   *          le nouveau flux greffe
   */
  public void setFluxGreffe(String fluxGreffe) {
    this.fluxGreffe = fluxGreffe;
  }

  /**
   * Get le flux cnba.
   *
   * @return le flux cnba
   */
  public String getFluxCnba() {
    return fluxCnba;
  }

  /**
   * Set le flux cnba.
   *
   * @param fluxCnba
   *          le nouveau flux cnba
   */
  public void setFluxCnba(String fluxCnba) {
    this.fluxCnba = fluxCnba;
  }

  /**
   * Get le flux ca.
   *
   * @return le flux ca
   */
  public String getFluxCa() {
    return fluxCa;
  }

  /**
   * Set le flux ca.
   *
   * @param fluxCa
   *          le nouveau flux ca
   */
  public void setFluxCa(String fluxCa) {
    this.fluxCa = fluxCa;
  }

  /**
   * Get le role.
   *
   * @return le role
   */
  public String getRole() {
    return role;
  }

  /**
   * Set le role.
   *
   * @param role
   *          le nouveau role
   */
  public void setRole(String role) {
    this.role = role;
  }

  /**
   * @return the type
   */
  public String getType() {
    return type;
  }

  /**
   * @param type
   *          the type to set
   */
  public void setType(String type) {
    this.type = type;
  }

  /**
   * @return the libelle
   */
  public String getLibelle() {
    return libelle;
  }

  /**
   * @param libelle
   *          the libelle to set
   */
  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }

  /**
   * Accesseur sur l'attribut {@link #flux agtcom}.
   *
   * @return flux agtcom
   */
  public String getFluxAgtcom() {
    return fluxAgtcom;
  }

  /**
   * Mutateur sur l'attribut {@link #flux agtcom}.
   *
   * @param fluxAgtcom
   *          la nouvelle valeur de l'attribut flux agtcom
   */
  public void setFluxAgtcom(String fluxAgtcom) {
    this.fluxAgtcom = fluxAgtcom;
  }

}
