package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Interface A64.
 */
@ResourceXPath("/A64")
public interface IA64 {

  /**
   * Get le A64.1.
   *
   * @return le A64.1
   */
  @FieldXPath("A64.1")
  String getA641();

  /**
   * Set le A641.
   *
   * @param A641
   *          le nouveau A641
   */
  void setA641(String A641);

  /**
   * Get le A642.
   *
   * @return le A642
   */
  @FieldXPath("A64.2")
  String getA642();

  /**
   * Set le A642.
   *
   * @param A642
   *          le nouveau A642
   */
  void setA642(String A642);
}
