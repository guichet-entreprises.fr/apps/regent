package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface CPU.
 */
@ResourceXPath("/DAP")
public interface DAP {
	
	  /**
	   * Get le p76.
	   *
	   * @return le P76
	   */
	  @FieldXPath("P76")
	  IP76[] getP76();

	  /**
	   * Ajoute le to P76.
	   *
	   * @return le i P76
	   */
	  IP76 addToP76();

	  /**
	   * Set le P76.
	   *
	   * @param P76
	   *          le nouveau P76
	   */
	  void setP76(IP76[] P76);
	  
	  
	  /**
	   * Get le p73.
	   *
	   * @return le P73
	   */
	  @FieldXPath("P74")
	  IP74[] getP74();

	  /**
	   * Ajoute le to P73.
	   *
	   * @return le i P73
	   */
	  IP74 addToP74();

	  /**
	   * Set le P73.
	   *
	   * @param P73
	   *          le nouveau P73
	   */
	  void setP74(IP74[] P74);
	  
	  /**
	   * Get le p70.
	   *
	   * @return le p70
	   */
	  @FieldXPath("P70")
	  String getP70();

	  /**
	   * Set le p70.
	   *
	   * @param P70
	   *          le nouveau p70
	   */
	  void setP70(String P70);
	  
	  /**
	   * Get le p71.
	   *
	   * @return le p71
	   */
	  @FieldXPath("P71")
	  String getP71();

	  /**
	   * Set le p71.
	   *
	   * @param P71
	   *          le nouveau p71
	   */
	  void setP71(String P71);
	  
	  
	  /**
	   * Get le p70.
	   *
	   * @return le p70
	   */
	  @FieldXPath("P72")
	  String getP72();

	  /**
	   * Set le P72.
	   *
	   * @param P72
	   *          le nouveau P72
	   */
	  void setP72(String P72);
	  
	  /**
	   * Get le p73.
	   *
	   * @return le p73
	   */
	  @FieldXPath("P73")
	  String getP73();

	  /**
	   * Set le P73.
	   *
	   * @param P73
	   *          le nouveau P73
	   */
	  void setP73(String P73);
	  
	  /**
	   * Get le p75.
	   *
	   * @return le p75
	   */
	  @FieldXPath("P75")
	  String getP75();

	  /**
	   * Set le P75.
	   *
	   * @param P75
	   *          le nouveau P75
	   */
	  void setP75(String P75);
	  
	  /**
	   * Get le p77.
	   *
	   * @return le p77
	   */
	  @FieldXPath("P77")
	  String getP77();

	  /**
	   * Set le P77.
	   *
	   * @param P77
	   *          le nouveau P77
	   */
	  void setP77(String P77);
	  
	  /**
	   * Get le p78.
	   *
	   * @return le p78
	   */
	  @FieldXPath("P78")
	  String getP78();

	  /**
	   * Set le P78.
	   *
	   * @param P78
	   *          le nouveau P78
	   */
	  void setP78(String P78);
	  
	  
	  
	  /**
	   * Get le p79.
	   *
	   * @return le p79
	   * 	   */
	  @FieldXPath("P79")
	  String getP79();

	  /**
	   * Set le P79.
	   *
	   * @param P79
	   *          le nouveau P79
	   */
	  void setP79(String P79);
	  
	  

}
