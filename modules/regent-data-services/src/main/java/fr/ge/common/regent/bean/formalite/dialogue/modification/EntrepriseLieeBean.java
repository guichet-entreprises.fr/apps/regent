package fr.ge.common.regent.bean.formalite.dialogue.modification;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import fr.ge.common.regent.bean.formalite.dialogue.AbstractEntrepriseLieeBean;

/**
 * Le Class EntrepriseLieeBean.
 *
 * @author roveda
 */
public class EntrepriseLieeBean extends AbstractEntrepriseLieeBean {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -8811514009462322002L;

    /** Le id technique. */
    private String idTechnique;

    /**
     * Get le id technique.
     *
     * @return the idTechnique
     */
    public String getIdTechnique() {
        return idTechnique;
    }

    /**
     * Set le id technique.
     *
     * @param idTechnique
     *            the idTechnique to set
     */
    public void setIdTechnique(String idTechnique) {
        this.idTechnique = idTechnique;
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see fr.guichetentreprises.vue.AbstractEntrepriseLieeBean#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

}
