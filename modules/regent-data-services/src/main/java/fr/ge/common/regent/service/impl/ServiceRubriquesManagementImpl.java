/**
 *
 */
package fr.ge.common.regent.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import fr.ge.common.regent.bean.modele.referentiel.ENormeRubrique;
import fr.ge.common.regent.persistance.dao.referentiel.NormeRubriqueDao;
import fr.ge.common.regent.service.ServiceRubriquesManagement;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * @author $Author: kelmoura $
 * @version $Revision: 0 $
 */
public class ServiceRubriquesManagementImpl implements ServiceRubriquesManagement {

    @Autowired
    private NormeRubriqueDao normeRubriqueDao;

    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceRubriquesManagementImpl.class);

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true, value = "regent")
    public List<ENormeRubrique> getRubriquesPathByEvenementAndVersion(String version, List<String> evenements) {
        if (version != null && evenements != null) {

            List<ENormeRubrique> listRubriques = this.normeRubriqueDao.getRubriquesPathByEvenementAndVersion(version, evenements);
            LOGGER.info("A list of rubriques was otained from executing the request for the version {}, and its size is equal to {}.", version, listRubriques.size());
            return listRubriques;
        } else {
            LOGGER.error("Technical Error occured while getting the list of the tags (rubrique) for the version {}, the cause is that either The version or the list of evenments is null", version);
            throw new TechnicalException(String.format(
                    "Technical Error occured while getting the list of the tags (rubrique) for the version %s, the cause is that either The version or the list of evenments is null", version));
        }
    }

}
