package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;
import org.xmlfield.core.types.XmlString;

/**
 * Le Interface IC39.
 */
@ResourceXPath("/C39")
public interface IC39 {

  /**
   * Get le c391.
   *
   * @return le c391
   */
  @FieldXPath("C39.1")
  XmlString[] getC391();

  /**
   * Ajoute le to c391.
   *
   * @return le xml string
   */
  XmlString addToC391();

  /**
   * Set le c391.
   *
   * @param C391
   *          le nouveau c391
   */
  void setC391(XmlString[] C391);

  /**
   * Get le c392.
   *
   * @return le c392
   */
  @FieldXPath("C39.2")
  String getC392();

  /**
   * Set le c392.
   *
   * @param C392
   *          le nouveau c392
   */
  void setC392(String C392);

  /**
   * Get le c393.
   *
   * @return le c393
   */
  @FieldXPath("C39.3")
  String getC393();

  /**
   * Set le c393.
   *
   * @param C393
   *          le nouveau c393
   */
  void setC393(String C393);

}
