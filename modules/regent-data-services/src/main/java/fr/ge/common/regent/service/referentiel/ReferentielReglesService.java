/**
 * 
 */
package fr.ge.common.regent.service.referentiel;

import java.util.List;

import fr.ge.common.regent.bean.IXmlRegent;

/**
 * Interface d'accès aux référentiels identifiés <b> "regles_*" </b>.
 * 
 * @author $Author: fbeaurai $
 * @version $Revision: 0 $
 */
public interface ReferentielReglesService {

    /**
     * Service de récupération de toutes les regles regent à partir du
     * référentiel <b> </b>.
     * 
     * @return le list
     */
    List<IXmlRegent> recupererToutesLesReglesRegent();

}
