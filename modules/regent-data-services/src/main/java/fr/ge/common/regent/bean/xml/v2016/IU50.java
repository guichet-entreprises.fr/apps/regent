package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

@ResourceXPath("/U50")
public interface IU50 {

  /**
   * Get le u501.
   *
   * @return le u501
   */
  @FieldXPath("U50.1")
  String getU501();

  /**
   * Set le u501
   *
   * @param U501
   *          le nouveau u501
   */
  void setU501(String U501);

  /**
   * Get le u502.
   *
   * @return le u502
   */
  @FieldXPath("U50.2")
  String getU502();

  /**
   * Set le u502
   *
   * @param U502
   *          le nouveau u502
   */
  void setU502(String U502);

  /**
   * Get le u503.
   *
   * @return le u503
   */
  @FieldXPath("U50.3")
  String getU503();

  /**
   * Set le u503
   *
   * @param U503
   *          le nouveau u503
   */
  void setU503(String U503);

}
