package fr.ge.common.regent.bean.xml.formalite.dialogue.creation;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

import fr.ge.common.regent.bean.xml.formalite.IFormalite;

/**
 * Le Interface de l'entité de déclaration sociale de dirigeant de l'entreprise
 * 
 * 
 * 
 */
@ResourceXPath("/formalite")
public interface IDeclarationSociale extends IFormalite {

    /**
     * Getter de l'attribut activiteAutreQueDeclareeDepartement.
     * 
     * @return la valeur de activiteAutreQueDeclareePays
     */
    @FieldXPath(value = "activiteAutreQueDeclareeDepartement")
    public String getActiviteAutreQueDeclareeDepartement();

    /**
     * Getter de l'attribut activiteAutreQueDeclareePays.
     * 
     * @return la valeur de activiteAutreQueDeclareePays
     */
    @FieldXPath(value = "activiteAutreQueDeclareePays")
    String getActiviteAutreQueDeclareePays();

    /**
     * Getter de l'attribut activiteAutreQueDeclareePresence.
     * 
     * @return la valeur de activiteAutreQueDeclareePresence
     */
    @FieldXPath(value = "activiteAutreQueDeclareePresence")
    public String getActiviteAutreQueDeclareePresence();

    /**
     * Getter de l'attribut activiteAutreQueDeclareeStatut.
     * 
     * @return la valeur de activiteAutreQueDeclareeStatut
     */
    @FieldXPath(value = "activiteAutreQueDeclareeStatut")
    public String getActiviteAutreQueDeclareeStatut();

    /**
     * Getter de l'attribut activiteAutreQueDeclareeStatutAutre.
     * 
     * @return la valeur de activiteAutreQueDeclareeStatutAutre
     */
    @FieldXPath(value = "activiteAutreQueDeclareeStatutAutre")
    public String getActiviteAutreQueDeclareeStatutAutre();

    /**
     * getActiviteExerceeAnterieurementCodeGeographique.
     * 
     * @return activiteExerceeAnterieurementCodeGeographique
     */
    @FieldXPath(value = "activiteExerceeAnterieurementCodeGeographique")
    public String getActiviteExerceeAnterieurementCodeGeographique();

    /**
     * Getter de l'attribut activiteExerceeAnterieurementCommune.
     * 
     * @return la valeur de activiteExerceeAnterieurementCommune
     */
    @FieldXPath(value = "activiteExerceeAnterieurementCommune")
    public String getActiviteExerceeAnterieurementCommune();

    /**
     * Getter de l'attribut activiteExerceeAnterieurementDateCessation.
     * 
     * @return la valeur de activiteExerceeAnterieurementDateCessation
     */
    @FieldXPath(value = "activiteExerceeAnterieurementDateCessation")
    public String getActiviteExerceeAnterieurementDateCessation();

    /**
     * Getter de l'attribut activiteExerceeAnterieurementDepartement.
     * 
     * @return la valeur de activiteExerceeAnterieurementDepartement
     */
    @FieldXPath(value = "activiteExerceeAnterieurementDepartement")
    public String getActiviteExerceeAnterieurementDepartement();

    /**
     * Getter de l'attribut activiteExerceeAnterieurementLibelle.
     * 
     * @return la valeur de activiteExerceeAnterieurementLibelle
     */
    @FieldXPath(value = "activiteExerceeAnterieurementLibelle")
    public String getActiviteExerceeAnterieurementLibelle();

    /**
     * Getter de l'attribut activiteExerceeAnterieurementPays.
     * 
     * @return la valeur de activiteExerceeAnterieurementPays
     */
    @FieldXPath(value = "activiteExerceeAnterieurementPays")
    public String getActiviteExerceeAnterieurementPays();

    /**
     * Getter de l'attribut activiteExerceeAnterieurementPresence.
     * 
     * @return la valeur de activiteExerceeAnterieurementPresence
     */
    @FieldXPath(value = "activiteExerceeAnterieurementPresence")
    public String getActiviteExerceeAnterieurementPresence();

    /**
     * Getter de l'attribut activiteExerceeAnterieurementSIREN.
     * 
     * @return la valeur de activiteExerceeAnterieurementSIREN
     */
    @FieldXPath(value = "activiteExerceeAnterieurementSIREN")
    public String getActiviteExerceeAnterieurementSIREN();

    /**
     * Getter de l'attribut affiliationMSA.
     * 
     * @return la valeur de affiliationMSA
     */
    @FieldXPath(value = "affiliationMSA")
    public String getAffiliationMSA();

    /**
     * Getter de l'attribut ayantDroitNombre.
     * 
     * @return la valeur de ayantDroitNombre
     */
    @FieldXPath(value = "ayantDroitNombre")
    public Integer getAyantDroitNombre();

    /**
     * Getter de l'attribut ayantDroitPresence.
     * 
     * @return la valeur de ayantDroitPresence
     */
    @FieldXPath(value = "ayantDroitPresence")
    public String getAyantDroitPresence();

    /**
     * Getter de l'attribut ayantDroits.
     * 
     * @return la valeur de ayantDroits
     */

    // /** Le ayant droits. */

    @FieldXPath(value = "ayantDroits/ayantDroit")
    IAyantDroit[] getAyantDroits();

    /**
     * Retire le from dirigeants.
     *
     * @param etablissements
     *            le etablissements
     */
    void removeFromAyantDroits(final IAyantDroit ayantDroit);

    /**
     * Ajoute le to etablissements.
     *
     * @return les etablissements
     */
    IAyantDroit addToAyantDroits();

    @FieldXPath(value = "id")
    String getId();

    void setId(String id);

    /**
     * Getter de l'attribut conjointCouvertAssuranceMaladie1.
     * 
     * @return la valeur de conjointCouvertAssuranceMaladie1
     */
    @FieldXPath(value = "conjointCouvertAssuranceMaladie1")
    public String getConjointCouvertAssuranceMaladie1();

    /**
     * Getter de l'attribut conjointCouvertAssuranceMaladie2.
     * 
     * @return la valeur de conjointCouvertAssuranceMaladie2
     */
    @FieldXPath(value = "conjointCouvertAssuranceMaladie2")
    public String getConjointCouvertAssuranceMaladie2();

    /**
     * Getter de l'attribut conjointNumeroSecuriteSociale.
     * 
     * @return la valeur de conjointNumeroSecuriteSociale
     */
    @FieldXPath(value = "conjointNumeroSecuriteSociale")
    public String getConjointNumeroSecuriteSociale();

    /**
     * Getter de l'attribut demandeDotationJeuneAgriculteur.
     * 
     * @return la valeur de demandeDotationJeuneAgriculteur
     */
    @FieldXPath(value = "demandeDotationJeuneAgriculteur")
    public String getDemandeDotationJeuneAgriculteur();

    /**
     * getDepartementOrganismeConventionne.
     * 
     * @return departementOrganismeConventionne
     */
    @FieldXPath(value = "departementOrganismeConventionne")
    public String getDepartementOrganismeConventionne();

    /**
     * Getter de l'attribut estBeneficiaireRSARMI.
     * 
     * @return la valeur de estBeneficiaireRSARMI
     */
    @FieldXPath(value = "estBeneficiaireRSARMI")
    public String getEstBeneficiaireRSARMI();

    /**
     * Getter de l'attribut estJeuneAgriculteur.
     * 
     * @return la valeur de estJeuneAgriculteur
     */
    @FieldXPath(value = "estJeuneAgriculteur")
    public String getEstJeuneAgriculteur();

    /**
     * Getter de l'attribut numeroSecuriteSociale.
     * 
     * @return la valeur de numeroSecuriteSociale
     */
    @FieldXPath(value = "numeroSecuriteSociale")
    public String getNumeroSecuriteSociale();

    /**
     * Getter de l'attribut optionMicroSocial.
     * 
     * @return la valeur de optionMicroSocial
     */
    @FieldXPath(value = "optionMicroSocial")
    public String getOptionMicroSocial();

    /**
     * Getter de l'attribut optionMicroSocialVersement.
     * 
     * @return la valeur de optionMicroSocialVersement
     */
    @FieldXPath(value = "optionMicroSocialVersement")
    public String getOptionMicroSocialVersement();

    /**
     * Getter de l'attribut organismeConventionneCode.
     * 
     * @return la valeur de organismeConventionneCode
     */
    @FieldXPath(value = "organismeConventionneCode")
    public String getOrganismeConventionneCode();

    /**
     * getOrganismeConventionneCodeReseauA.
     * 
     * @return organismeConventionneCodeReseauA
     */
    @FieldXPath(value = "organismeConventionneCodeReseauA")
    public String getOrganismeConventionneCodeReseauA();

    /**
     * getOrganismeConventionneCodeReseauNonA.
     * 
     * @return organismeConventionneCodeReseauNonA
     */
    @FieldXPath(value = "organismeConventionneCodeReseauNonA")
    public String getOrganismeConventionneCodeReseauNonA();

    /**
     * Getter de l'attribut organismeConventionneLibelle.
     * 
     * @return la valeur de organismeConventionneLibelle
     */
    @FieldXPath(value = "organismeConventionneLibelle")
    public String getOrganismeConventionneLibelle();

    /**
     * Getter de l'attribut organismeServantPension.
     * 
     * @return la valeur de organismeServantPension
     */
    @FieldXPath(value = "organismeServantPension")
    public String getOrganismeServantPension();

    /**
     * Getter de l'attribut regimeAssuranceMaladie.
     * 
     * @return la valeur de regimeAssuranceMaladie
     */
    @FieldXPath(value = "regimeAssuranceMaladie")
    public String getRegimeAssuranceMaladie();

    /**
     * Getter de l'attribut regimeAssuranceMaladieAutre.
     * 
     * @return la valeur de regimeAssuranceMaladieAutre
     */
    @FieldXPath(value = "regimeAssuranceMaladieAutre")
    public String getRegimeAssuranceMaladieAutre();

    /**
     * Getter de l'attribut statutConjoint.
     * 
     * @return la valeur de statutConjoint
     */

    @FieldXPath(value = "statutConjoint")
    public String getStatutConjoint();

    /**
     * Getter de l'attribut titreSejourDateExpiration.
     * 
     * @return la valeur de titreSejourDateExpiration
     */
    @FieldXPath(value = "titreSejourDateExpiration")
    public String getTitreSejourDateExpiration();

    /**
     * Getter de l'attribut titreSejourLieuDelivranceCode.
     * 
     * @return la valeur de titreSejourLieuDelivranceCode
     */
    @FieldXPath(value = "titreSejourLieuDelivranceCode")
    public String getTitreSejourLieuDelivranceCode();

    /**
     * Getter de l'attribut titreSejourLieuDelivranceCommune.
     * 
     * @return la valeur de titreSejourLieuDelivranceCommune
     */
    @FieldXPath(value = "titreSejourLieuDelivranceCommune")
    String getTitreSejourLieuDelivranceCommune();

    /**
     * Setter de l'attribut activiteAutreQueDeclareeDepartement.
     * 
     * @param activiteAutreQueDeclareeDepartement
     *            la nouvelle valeur de activiteAutreQueDeclareeDepartement
     */
    void setActiviteAutreQueDeclareeDepartement(String activiteAutreQueDeclareeDepartement);

    /**
     * Setter de l'attribut activiteAutreQueDeclareePays.
     * 
     * @param activiteAutreQueDeclareePays
     *            la nouvelle valeur de activiteAutreQueDeclareePays
     */
    void setActiviteAutreQueDeclareePays(String activiteAutreQueDeclareePays);

    /**
     * Setter de l'attribut activiteAutreQueDeclareePresence.
     * 
     * @param activiteAutreQueDeclareePresence
     *            la nouvelle valeur de activiteAutreQueDeclareePresence
     */
    void setActiviteAutreQueDeclareePresence(String activiteAutreQueDeclareePresence);

    /**
     * Setter de l'attribut activiteAutreQueDeclareeStatut.
     * 
     * @param activiteAutreQueDeclareeStatut
     *            la nouvelle valeur de activiteAutreQueDeclareeStatut
     */
    void setActiviteAutreQueDeclareeStatut(String activiteAutreQueDeclareeStatut);

    /**
     * Setter de l'attribut activiteAutreQueDeclareeStatutAutre.
     * 
     * @param activiteAutreQueDeclareeStatutAutre
     *            la nouvelle valeur de activiteAutreQueDeclareeStatutAutre
     */
    void setActiviteAutreQueDeclareeStatutAutre(String activiteAutreQueDeclareeStatutAutre);

    /**
     * setActiviteExerceeAnterieurementCodeGeographique.
     * 
     * @param activiteExerceeAnterieurementCodeGeographique
     *            activiteExerceeAnterieurementCodeGeographique
     */
    public void setActiviteExerceeAnterieurementCodeGeographique(String activiteExerceeAnterieurementCodeGeographique);

    /**
     * Setter de l'attribut activiteExerceeAnterieurementCommune.
     * 
     * @param activiteExerceeAnterieurementCommune
     *            la nouvelle valeur de activiteExerceeAnterieurementCommune
     */
    public void setActiviteExerceeAnterieurementCommune(String activiteExerceeAnterieurementCommune);

    /**
     * Setter de l'attribut activiteExerceeAnterieurementDateCessation.
     * 
     * @param activiteExerceeAnterieurementDateCessation
     *            la nouvelle valeur de
     *            activiteExerceeAnterieurementDateCessation
     */
    public void setActiviteExerceeAnterieurementDateCessation(String activiteExerceeAnterieurementDateCessation);

    /**
     * Setter de l'attribut activiteExerceeAnterieurementDepartement.
     * 
     * @param activiteExerceeAnterieurementDepartement
     *            la nouvelle valeur de activiteExerceeAnterieurementDepartement
     */
    public void setActiviteExerceeAnterieurementDepartement(String activiteExerceeAnterieurementDepartement);

    /**
     * Setter de l'attribut activiteExerceeAnterieurementLibelle.
     * 
     * @param activiteExerceeAnterieurementLibelle
     *            la nouvelle valeur de activiteExerceeAnterieurementLibelle
     */
    public void setActiviteExerceeAnterieurementLibelle(String activiteExerceeAnterieurementLibelle);

    /**
     * Setter de l'attribut activiteExerceeAnterieurementPays.
     * 
     * @param activiteExerceeAnterieurementPays
     *            la nouvelle valeur de activiteExerceeAnterieurementPays
     */
    public void setActiviteExerceeAnterieurementPays(String activiteExerceeAnterieurementPays);

    /**
     * Setter de l'attribut activiteExerceeAnterieurementPresence.
     * 
     * @param activiteExerceeAnterieurementPresence
     *            la nouvelle valeur de activiteExerceeAnterieurementPresence
     */
    public void setActiviteExerceeAnterieurementPresence(String activiteExerceeAnterieurementPresence);

    /**
     * Setter de l'attribut activiteExerceeAnterieurementSIREN.
     * 
     * @param activiteExerceeAnterieurementSIREN
     *            la nouvelle valeur de activiteExerceeAnterieurementSIREN
     */
    public void setActiviteExerceeAnterieurementSIREN(String activiteExerceeAnterieurementSIREN);

    /**
     * Setter de l'attribut affiliationMSA.
     * 
     * @param affiliationMSA
     *            la nouvelle valeur de affiliationMSA
     */
    public void setAffiliationMSA(String affiliationMSA);

    /**
     * Setter de l'attribut ayantDroitNombre.
     * 
     * @param ayantDroitNombre
     *            la nouvelle valeur de ayantDroitNombre
     */
    public void setAyantDroitNombre(Integer ayantDroitNombre);

    /**
     * Setter de l'attribut ayantDroitPresence.
     * 
     * @param ayantDroitPresence
     *            la nouvelle valeur de ayantDroitPresence
     */
    public void setAyantDroitPresence(String ayantDroitPresence);

    /**
     * Setter de l'attribut ayantDroits.
     * 
     * @param ayantDroits
     *            la nouvelle valeur de ayantDroits
     */
    public void setAyantDroits(IAyantDroit[] ayantDroits);

    /**
     * Setter de l'attribut conjointCouvertAssuranceMaladie1.
     * 
     * @param conjointCouvertAssuranceMaladie1
     *            la nouvelle valeur de conjointCouvertAssuranceMaladie1
     */
    public void setConjointCouvertAssuranceMaladie1(String conjointCouvertAssuranceMaladie1);

    /**
     * Setter de l'attribut conjointCouvertAssuranceMaladie2.
     * 
     * @param conjointCouvertAssuranceMaladie2
     *            la nouvelle valeur de conjointCouvertAssuranceMaladie2
     */
    public void setConjointCouvertAssuranceMaladie2(String conjointCouvertAssuranceMaladie2);

    /**
     * Setter de l'attribut conjointNumeroSecuriteSociale.
     * 
     * @param conjointNumeroSecuriteSociale
     *            la nouvelle valeur de conjointNumeroSecuriteSociale
     */
    public void setConjointNumeroSecuriteSociale(String conjointNumeroSecuriteSociale);

    /**
     * Setter de l'attribut demandeDotationJeuneAgriculteur.
     * 
     * @param demandeDotationJeuneAgriculteur
     *            la nouvelle valeur de demandeDotationJeuneAgriculteur
     */
    public void setDemandeDotationJeuneAgriculteur(String demandeDotationJeuneAgriculteur);

    /**
     * setDepartementOrganismeConventionne.
     * 
     * @param departementOrganismeConventionne
     *            departementOrganismeConventionne
     */
    public void setDepartementOrganismeConventionne(String departementOrganismeConventionne);

    /**
     * Setter de l'attribut estBeneficiaireRSARMI.
     * 
     * @param estBeneficiaireRSARMI
     *            la nouvelle valeur de estBeneficiaireRSARMI
     */
    public void setEstBeneficiaireRSARMI(String estBeneficiaireRSARMI);

    /**
     * Setter de l'attribut estJeuneAgriculteur.
     * 
     * @param estJeuneAgriculteur
     *            la nouvelle valeur de estJeuneAgriculteur
     */
    public void setEstJeuneAgriculteur(String estJeuneAgriculteur);

    /**
     * Setter de l'attribut numeroSecuriteSociale.
     * 
     * @param numeroSecuriteSociale
     *            la nouvelle valeur de numeroSecuriteSociale
     */
    public void setNumeroSecuriteSociale(String numeroSecuriteSociale);

    /**
     * Setter de l'attribut optionMicroSocial.
     * 
     * @param optionMicroSocial
     *            la nouvelle valeur de optionMicroSocial
     */
    public void setOptionMicroSocial(String optionMicroSocial);

    /**
     * Setter de l'attribut optionMicroSocialVersement.
     * 
     * @param optionMicroSocialVersement
     *            la nouvelle valeur de optionMicroSocialVersement
     */
    public void setOptionMicroSocialVersement(String optionMicroSocialVersement);

    /**
     * Setter de l'attribut organismeConventionneCode.
     * 
     * @param organismeConventionneCode
     *            la nouvelle valeur de organismeConventionneCode
     */
    public void setOrganismeConventionneCode(String organismeConventionneCode);

    /**
     * setOrganismeConventionneCodeReseauA.
     * 
     * @param organismeConventionneCodeReseauA
     *            organismeConventionneCodeReseauA
     */
    public void setOrganismeConventionneCodeReseauA(String organismeConventionneCodeReseauA);

    /**
     * setOrganismeConventionneCodeReseauNonA.
     * 
     * @param organismeConventionneCodeReseauNonA
     *            organismeConventionneCodeReseauNonA
     */
    public void setOrganismeConventionneCodeReseauNonA(String organismeConventionneCodeReseauNonA);

    /**
     * Setter de l'attribut organismeConventionneLibelle.
     * 
     * @param organismeConventionneLibelle
     *            la nouvelle valeur de organismeConventionneLibelle
     */
    public void setOrganismeConventionneLibelle(String organismeConventionneLibelle);

    /**
     * Setter de l'attribut organismeServantPension.
     * 
     * @param organismeServantPension
     *            la nouvelle valeur de organismeServantPension
     */
    public void setOrganismeServantPension(String organismeServantPension);

    /**
     * Setter de l'attribut regimeAssuranceMaladie.
     * 
     * @param regimeAssuranceMaladie
     *            la nouvelle valeur de regimeAssuranceMaladie
     */
    public void setRegimeAssuranceMaladie(String regimeAssuranceMaladie);

    /**
     * Setter de l'attribut regimeAssuranceMaladieAutre.
     * 
     * @param regimeAssuranceMaladieAutre
     *            la nouvelle valeur de regimeAssuranceMaladieAutre
     */
    public void setRegimeAssuranceMaladieAutre(String regimeAssuranceMaladieAutre);

    /**
     * Setter de l'attribut statutConjoint.
     * 
     * @param statutConjoint
     *            la nouvelle valeur de statutConjoint
     */
    public void setStatutConjoint(String statutConjoint);

    /**
     * Setter de l'attribut titreSejourDateExpiration.
     * 
     * @param titreSejourDateExpiration
     *            la nouvelle valeur de titreSejourDateExpiration
     */
    public void setTitreSejourDateExpiration(String titreSejourDateExpiration);

    /**
     * Setter de l'attribut titreSejourLieuDelivranceCode.
     * 
     * @param titreSejourLieuDelivranceCode
     *            la nouvelle valeur de titreSejourLieuDelivranceCode
     */
    public void setTitreSejourLieuDelivranceCode(String titreSejourLieuDelivranceCode);

    /**
     * Setter de l'attribut titreSejourLieuDelivranceCommune.
     * 
     * @param titreSejourLieuDelivranceCommune
     *            la nouvelle valeur de titreSejourLieuDelivranceCommune
     */
    public void setTitreSejourLieuDelivranceCommune(String titreSejourLieuDelivranceCommune);

}
