package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * L'interface FJM.
 */
@ResourceXPath("/FJM")
public interface IFJM {

  /**
   * Get le M21.
   *
   * @return le M21
   */
  @FieldXPath("M21")
  IM21[] getM21();

  /**
   * Ajoute le to M21.
   *
   * @return le M21
   */
  IM21 addToM21();

  /**
   * Set le m21.
   *
   * @param m21
   *          le m21
   */
  void set(IM21[] m21);

  /**
   * Get le m23.
   *
   * @return le m23
   */
  @FieldXPath("M23")
  String getM23();

  /**
   * Set le m23.
   *
   * @param m23
   *          le nouveau m23
   */
  void setM23(String m23);
}
