/**
 * 
 */
package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Interface P90.
 *
 * @author $Author: aboidard $
 * @version $Revision: 0 $
 */
@ResourceXPath("/P90")
public interface IP90 {

  /**
   * Accesseur sur l'attribut {@link #P901}.
   *
   * @return P901
   */
  @FieldXPath("P90.1")
  IP901[] getP901();

  /**
   * ajoute le P901.
   * 
   * @return le P901
   */
  IP901 addToP901();

  /**
   * Mutateur sur l'attribut {@link #P901}.
   *
   * @param P901
   *          la nouvelle valeur de l'attribut P901
   */
  void setP901(IP901[] P901);

  /**
   * Accesseur sur l'attribut {@link #P902}.
   *
   * @return P902
   */
  @FieldXPath("P90.2")
  IP902[] getP902();

  /**
   * ajoute le p902.
   * 
   * @return le p902
   */
  IP902 addToP902();

  /**
   * Mutateur sur l'attribut {@link #P902}.
   *
   * @param p902
   *          la nouvelle valeur de l'attribut P902
   */
  void setP902(IP902[] p902);

}
