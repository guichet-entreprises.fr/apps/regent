package fr.ge.common.regent.bean.xml.formalite.dialogue.regularisation;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

import fr.ge.common.regent.bean.xml.formalite.IFormalite;
import fr.ge.common.regent.bean.xml.transverse.IAdresse;

/**
 * La classe XML Field de Formalité Régularisation.
 * 
 * @author roveda
 * 
 */
@ResourceXPath("/formalite")
public interface IEtablissementRegularisation extends IFormalite {

    /**
     * Get le domiciliation.
     *
     * @return le domiciliation
     */
    @FieldXPath(value = "domiciliation")
    String getDomiciliation();

    /**
     * Get le modif date adresse1.
     *
     * @return le modif date adresse1
     */
    @FieldXPath(value = "modifDateAdresse1")
    String getModifDateAdresse1();

    /**
     * Get le date debut activite.
     *
     * @return le date debut activite
     */
    @FieldXPath(value = "dateDebutActivite")
    String getDateDebutActivite();

    /**
     * Get le activite permanente saisonniere.
     *
     * @return le activite permanente saisonniere
     */
    @FieldXPath(value = "activitePermanenteSaisonniere")
    String getActivitePermanenteSaisonniere();

    /**
     * Get le non sedentarite qualite.
     *
     * @return le non sedentarite qualite
     */
    @FieldXPath(value = "nonSedentariteQualite")
    String getNonSedentariteQualite();

    /**
     * Get le est ambulant ue.
     *
     * @return le est ambulant ue
     */
    @FieldXPath(value = "estAmbulantUE")
    String getEstAmbulantUE();

    /**
     * Get le activites.
     *
     * @return le activites
     */
    @FieldXPath(value = "activites")
    String getActivites();

    /**
     * Get le activite plus importante.
     *
     * @return le activite plus importante
     */
    @FieldXPath(value = "activitePlusImportante")
    String getActivitePlusImportante();

    /**
     * Get le modif date activite.
     *
     * @return le modif date activite
     */
    @FieldXPath(value = "modifDateActivite")
    String getModifDateActivite();

    /**
     * Get le modif ancienne activite.
     *
     * @return le modif ancienne activite
     */
    @FieldXPath(value = "modifAncienneActivite")
    String getModifAncienneActivite();

    /**
     * Get le aqpa situation.
     *
     * @return le aqpa situation
     */
    @FieldXPath(value = "aqpaSituation")
    String getAqpaSituation();

    /**
     * Get le aqpa intitule diplome.
     *
     * @return le aqpa intitule diplome
     */
    @FieldXPath(value = "aqpaIntituleDiplome")
    String getAqpaIntituleDiplome();

    /**
     * Get le activite nature.
     *
     * @return le activite nature
     */
    @FieldXPath(value = "activiteNature")
    String getActiviteNature();

    /**
     * Get le activite nature autre.
     *
     * @return le activite nature autre
     */
    @FieldXPath(value = "activiteNatureAutre")
    String getActiviteNatureAutre();

    /**
     * Get le activite lieu exercice magasin.
     *
     * @return le activite lieu exercice magasin
     */
    @FieldXPath(value = "activiteLieuExerciceMagasin")
    Integer getActiviteLieuExerciceMagasin();

    /**
     * Get le nom commercial professionnel.
     *
     * @return le nom commercial professionnel
     */
    @FieldXPath(value = "nomCommercialProfessionnel")
    String getNomCommercialProfessionnel();

    /**
     * Get le enseigne.
     *
     * @return le enseigne
     */
    @FieldXPath(value = "enseigne")
    String getEnseigne();

    /**
     * Get le origine fonds.
     *
     * @return le origine fonds
     */
    @FieldXPath(value = "origineFonds")
    String getOrigineFonds();

    /**
     * Get le loueur mandant du fonds debut contrat.
     *
     * @return le loueur mandant du fonds debut contrat
     */
    @FieldXPath(value = "loueurMandantDuFondsDebutContrat")
    String getLoueurMandantDuFondsDebutContrat();

    /**
     * Get le loueur mandant du fonds duree contrat indeterminee.
     *
     * @return le loueur mandant du fonds duree contrat indeterminee
     */
    @FieldXPath(value = "loueurMandantDuFondsDureeContratIndeterminee")
    String getLoueurMandantDuFondsDureeContratIndeterminee();

    /**
     * Get le loueur mandant du fonds fin contrat.
     *
     * @return le loueur mandant du fonds fin contrat
     */
    @FieldXPath(value = "loueurMandantDuFondsFinContrat")
    String getLoueurMandantDuFondsFinContrat();

    /**
     * Get le loueur mandant du fonds renouvellement tacite reconduction.
     *
     * @return le loueur mandant du fonds renouvellement tacite reconduction
     */
    @FieldXPath(value = "loueurMandantDuFondsRenouvellementTaciteReconduction")
    String getLoueurMandantDuFondsRenouvellementTaciteReconduction();

    /**
     * Get le plan cession.
     *
     * @return le plan cession
     */
    @FieldXPath(value = "planCession")
    String getPlanCession();

    /**
     * Get le fonds uniquement artisanal.
     *
     * @return le fonds uniquement artisanal
     */
    @FieldXPath(value = "fondsUniquementArtisanal")
    String getFondsUniquementArtisanal();

    /**
     * Get le journal annonces legales nom.
     *
     * @return le journal annonces legales nom
     */
    @FieldXPath(value = "journalAnnoncesLegalesNom")
    String getJournalAnnoncesLegalesNom();

    /**
     * Get le journal annonces legales date parution.
     *
     * @return le journal annonces legales date parution
     */
    @FieldXPath(value = "journalAnnoncesLegalesDateParution")
    String getJournalAnnoncesLegalesDateParution();

    /**
     * Get le personne pouvoir presence.
     *
     * @return le personne pouvoir presence
     */
    @FieldXPath(value = "personnePouvoirPresence")
    String getPersonnePouvoirPresence();

    /**
     * Get le personne pouvoir nombre.
     *
     * @return le personne pouvoir nombre
     */
    @FieldXPath(value = "personnePouvoirNombre")
    Integer getPersonnePouvoirNombre();

    /**
     * Get le effectif salarie presence.
     *
     * @return le effectif salarie presence
     */
    @FieldXPath(value = "effectifSalariePresence")
    String getEffectifSalariePresence();

    /**
     * Get le effectif salarie nombre.
     *
     * @return le effectif salarie nombre
     */
    @FieldXPath(value = "effectifSalarieNombre")
    Integer getEffectifSalarieNombre();

    /**
     * Get le effectif salarie apprentis.
     *
     * @return le effectif salarie apprentis
     */
    @FieldXPath(value = "effectifSalarieApprentis")
    Integer getEffectifSalarieApprentis();

    /**
     * Get le categorie.
     *
     * @return le categorie
     */
    @FieldXPath(value = "categorie")
    String getCategorie();

    /**
     * Get le situation.
     *
     * @return le situation
     */
    @FieldXPath(value = "situation")
    String getSituation();

    /**
     * Get le adresse.
     *
     * @return le adresse
     */
    @FieldXPath(value = "adresse")
    IAdresse getAdresse();

    /**
     * New adresse.
     *
     * @return le i adresse
     */
    IAdresse newAdresse();

    /**
     * Set le adresse.
     *
     * @param adresse
     *            le nouveau adresse
     */
    void setAdresse(IAdresse adresse);

    /**
     * Get le modif ancienne adresse1.
     *
     * @return le modif ancienne adresse1
     */
    @FieldXPath(value = "modifAncienneAdresse1")
    IAdresse getModifAncienneAdresse1();

    /**
     * New modif ancienne adresse1.
     *
     * @return le i adresse
     */
    IAdresse newModifAncienneAdresse1();

    /**
     * Set le modif ancienne adresse1.
     *
     * @param modifAncienneAdresse1
     *            le nouveau modif ancienne adresse1
     */
    void setModifAncienneAdresse1(IAdresse modifAncienneAdresse1);

    /**
     * Set le domiciliation.
     *
     * @param domiciliation
     *            le nouveau domiciliation
     */
    void setDomiciliation(String domiciliation);

    /**
     * Set le date debut activite.
     *
     * @param dateDebutActivite
     *            le nouveau date debut activite
     */
    void setDateDebutActivite(String dateDebutActivite);

    /**
     * Set le activite permanente saisonniere.
     *
     * @param activitePermanenteSaisonniere
     *            le nouveau activite permanente saisonniere
     */
    void setActivitePermanenteSaisonniere(String activitePermanenteSaisonniere);

    /**
     * Set le non sedentarite qualite.
     *
     * @param nonSedentariteQualite
     *            le nouveau non sedentarite qualite
     */
    void setNonSedentariteQualite(String nonSedentariteQualite);

    /**
     * Set le est ambulant ue.
     *
     * @param estAmbulantUE
     *            le nouveau est ambulant ue
     */
    void setEstAmbulantUE(String estAmbulantUE);

    /**
     * Set le activites.
     *
     * @param activites
     *            le nouveau activites
     */
    void setActivites(String activites);

    /**
     * Set le activite plus importante.
     *
     * @param activitePlusImportante
     *            le nouveau activite plus importante
     */
    void setActivitePlusImportante(String activitePlusImportante);

    /**
     * Set le modif date activite.
     *
     * @param modifDateActivite
     *            le nouveau modif date activite
     */
    void setModifDateActivite(String modifDateActivite);

    /**
     * Set le modif ancienne activite.
     *
     * @param modifAncienneActivite
     *            le nouveau modif ancienne activite
     */
    void setModifAncienneActivite(String modifAncienneActivite);

    /**
     * Set le aqpa situation.
     *
     * @param aqpaSituation
     *            le nouveau aqpa situation
     */
    void setAqpaSituation(String aqpaSituation);

    /**
     * Set le aqpa intitule diplome.
     *
     * @param aqpaIntituleDiplome
     *            le nouveau aqpa intitule diplome
     */
    void setAqpaIntituleDiplome(String aqpaIntituleDiplome);

    /**
     * Set le activite nature.
     *
     * @param activiteNature
     *            le nouveau activite nature
     */
    void setActiviteNature(String activiteNature);

    /**
     * Set le activite nature autre.
     *
     * @param activiteNatureAutre
     *            le nouveau activite nature autre
     */
    void setActiviteNatureAutre(String activiteNatureAutre);

    /**
     * Set le activite lieu exercice magasin.
     *
     * @param activiteLieuExerciceMagasin
     *            le nouveau activite lieu exercice magasin
     */
    void setActiviteLieuExerciceMagasin(Integer activiteLieuExerciceMagasin);

    /**
     * Set le nom commercial professionnel.
     *
     * @param nomCommercialProfessionnel
     *            le nouveau nom commercial professionnel
     */
    void setNomCommercialProfessionnel(String nomCommercialProfessionnel);

    /**
     * Set le enseigne.
     *
     * @param enseigne
     *            le nouveau enseigne
     */
    void setEnseigne(String enseigne);

    /**
     * Set le origine fonds.
     *
     * @param origineFonds
     *            le nouveau origine fonds
     */
    void setOrigineFonds(String origineFonds);

    /**
     * Set le loueur mandant du fonds debut contrat.
     *
     * @param loueurMandantDuFondsDebutContrat
     *            le nouveau loueur mandant du fonds debut contrat
     */
    void setLoueurMandantDuFondsDebutContrat(String loueurMandantDuFondsDebutContrat);

    /**
     * Set le loueur mandant du fonds duree contrat indeterminee.
     *
     * @param loueurMandantDuFondsDureeContratIndeterminee
     *            le nouveau loueur mandant du fonds duree contrat indeterminee
     */
    void setLoueurMandantDuFondsDureeContratIndeterminee(String loueurMandantDuFondsDureeContratIndeterminee);

    /**
     * Set le loueur mandant du fonds fin contrat.
     *
     * @param loueurMandantDuFondsFinContrat
     *            le nouveau loueur mandant du fonds fin contrat
     */
    void setLoueurMandantDuFondsFinContrat(String loueurMandantDuFondsFinContrat);

    /**
     * Set le loueur mandant du fonds renouvellement tacite reconduction.
     *
     * @param loueurMandantDuFondsRenouvellementTaciteReconduction
     *            le nouveau loueur mandant du fonds renouvellement tacite
     *            reconduction
     */
    void setLoueurMandantDuFondsRenouvellementTaciteReconduction(String loueurMandantDuFondsRenouvellementTaciteReconduction);

    /**
     * Set le plan cession.
     *
     * @param planCession
     *            le nouveau plan cession
     */
    void setPlanCession(String planCession);

    /**
     * Set le fonds uniquement artisanal.
     *
     * @param fondsUniquementArtisanal
     *            le nouveau fonds uniquement artisanal
     */
    void setFondsUniquementArtisanal(String fondsUniquementArtisanal);

    /**
     * Set le journal annonces legales nom.
     *
     * @param journalAnnoncesLegalesNom
     *            le nouveau journal annonces legales nom
     */
    void setJournalAnnoncesLegalesNom(String journalAnnoncesLegalesNom);

    /**
     * Set le journal annonces legales date parution.
     *
     * @param journalAnnoncesLegalesDateParution
     *            le nouveau journal annonces legales date parution
     */
    void setJournalAnnoncesLegalesDateParution(String journalAnnoncesLegalesDateParution);

    /**
     * Set le personne pouvoir presence.
     *
     * @param personnePouvoirPresence
     *            le nouveau personne pouvoir presence
     */
    void setPersonnePouvoirPresence(String personnePouvoirPresence);

    /**
     * Set le personne pouvoir nombre.
     *
     * @param personnePouvoirNombre
     *            le nouveau personne pouvoir nombre
     */
    void setPersonnePouvoirNombre(Integer personnePouvoirNombre);

    /**
     * Set le effectif salarie presence.
     *
     * @param effectifSalariePresence
     *            le nouveau effectif salarie presence
     */
    void setEffectifSalariePresence(String effectifSalariePresence);

    /**
     * Set le effectif salarie nombre.
     *
     * @param effectifSalarieNombre
     *            le nouveau effectif salarie nombre
     */
    void setEffectifSalarieNombre(Integer effectifSalarieNombre);

    /**
     * Set le effectif salarie apprentis.
     *
     * @param effectifSalarieApprentis
     *            le nouveau effectif salarie apprentis
     */
    void setEffectifSalarieApprentis(Integer effectifSalarieApprentis);

    /**
     * Set le categorie.
     *
     * @param categorie
     *            le nouveau categorie
     */
    void setCategorie(String categorie);

    /**
     * Set le situation.
     *
     * @param situation
     *            le nouveau situation
     */
    void setSituation(String situation);

    /**
     * Set le modif date adresse1.
     *
     * @param modifDateAdresse1
     *            le nouveau modif date adresse1
     */
    void setModifDateAdresse1(String modifDateAdresse1);

    /**
     * Get le activiteSoumiseQualification.
     *
     * @return le activiteSoumiseQualification
     */
    @FieldXPath(value = "activitesSoumisesQualification/activiteSoumiseQualification")
    IActiviteSoumiseQualificationRegularisation[] getActivitesSoumisesQualification();

    /**
     * Ajoute le to activiteSoumiseQualification.
     *
     * @return le i activiteSoumiseQualification
     */
    IActiviteSoumiseQualificationRegularisation addToActivitesSoumisesQualification();

    /**
     * Set le ActivitesSoumisesQualification.
     *
     * @param activitesSoumisesQualification
     *            le nouveau ActivitesSoumisesQualification
     */
    void setActivitesSoumisesQualification(IActiviteSoumiseQualificationRegularisation[] activitesSoumisesQualification);

    /**
     * Retire le from activitesSoumisesQualification.
     *
     * @param activitesSoumisesQualification
     *            le activitesSoumisesQualification
     */
    void removeFromActivitesSoumisesQualification(IActiviteSoumiseQualificationRegularisation activitesSoumisesQualification);

    /**
     * Getter de l'attribut nombreActivitesSoumisesQualification.
     * 
     * @return la valeur de nombreActivitesSoumisesQualification
     */
    @FieldXPath(value = "nombreActivitesSoumisesQualification")
    Integer getNombreActivitesSoumisesQualification();

    /**
     * Setteur nombreActivitesSoumisesQualification.
     * 
     * @param nombreActivitesSoumisesQualification
     *            nombre des Activivites Soumises à Qualification
     */
    void setNombreActivitesSoumisesQualification(Integer nombreActivitesSoumisesQualification);

}
