package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * L'interface M24.
 */
@ResourceXPath("/M24")
public interface IM24 {

  /**
   * Get le m241.
   *
   * @return le m241
   */
  @FieldXPath("M24.1")
  String getM241();

  /**
   * Set le m241.
   *
   * @param m241
   *          le nouveau m241
   */
  void setM241(String m241);

  /**
   * Get le m242.
   *
   * @return le m242
   */
  @FieldXPath("M24.2")
  String getM242();

  /**
   * Set le m242.
   *
   * @param m242
   *          le nouveau m242
   */
  void setM242(String m242);

  /**
   * Get le m243.
   *
   * @return le m243
   */
  @FieldXPath("M24.3")
  String getM243();

  /**
   * Set le m243.
   *
   * @param m243
   *          le nouveau m243
   */
  void setM243(String m243);

  /**
   * Get le m244.
   *
   * @return le m244
   */
  @FieldXPath("M24.4")
  String getM244();

  /**
   * Set le m244.
   *
   * @param m244
   *          le nouveau m244
   */
  void setM244(String m244);
}
