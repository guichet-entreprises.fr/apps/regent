package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Interface A67.
 */
@ResourceXPath("/A67")
public interface IA67 {

  /**
   * Get le A67.1.
   *
   * @return le A67.1
   */
  @FieldXPath("A67.1")
  String getA671();

  /**
   * Set le A671.
   *
   * @param A671
   *          le nouveau A671
   */
  void setA671(String A671);

  /**
   * Get le A672.
   *
   * @return le A672
   */
  @FieldXPath("A67.2")
  String getA672();

  /**
   * Set le A672.
   *
   * @param A672
   *          le nouveau A672
   */
  void setA672(String A672);

  /**
   * Get le A673.
   *
   * @return le A673
   */
  @FieldXPath("A67.3")
  String getA673();

  /**
   * Set le A673.
   *
   * @param A673
   *          le nouveau A673
   */
  void setA673(String A673);

  /**
   * Get le A674.
   *
   * @return le A674
   */
  @FieldXPath("A67.4")
  String getA674();

  /**
   * Set le A674.
   *
   * @param A674
   *          le nouveau A674
   */
  void setA674(String A674);

}
