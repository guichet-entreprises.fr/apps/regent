package fr.ge.common.regent.bean.formalite.dialogue.modification;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import fr.ge.common.regent.bean.formalite.dialogue.AbstractConjointBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.AdresseBean;

/**
 * Le Class ConjointBean.
 */
public class ConjointBean extends AbstractConjointBean implements IFormaliteModificationVue {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -3148141275362250293L;

    /** Le categorie conjoint. */
    /* Catégorie Conjoint */
    private String categorieConjoint;

    /** Le modif date conjoint. */
    /* modifDateConjoint */
    private String modifDateConjoint;

    /** Le pp numero securite sociale. */
    /* ppNumeroSecuriteSociale */
    private String ppNumeroSecuriteSociale;

    /** Le pp adresse different gerant. */
    /* ppAdresseDifferentGerant */
    private String ppAdresseDifferentGerant;

    /** Le pp Couvert Securite Sociale. */
    private String ppCouvertSecuriteSociale;

    /** Le pp adresse. */
    /* ppAdresse */
    private AdresseBean ppAdresse = new AdresseBean();

    /**
     * Instancie un nouveau conjoint bean.
     */
    public ConjointBean() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see fr.guichetentreprises.vue.AbstractConjointBean#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    /**
     * Get le categorie conjoint.
     *
     * @return the categorieConjoint
     */
    public String getCategorieConjoint() {
        return categorieConjoint;
    }

    /**
     * Set le categorie conjoint.
     *
     * @param categorieConjoint
     *            the categorieConjoint to set
     */
    public void setCategorieConjoint(String categorieConjoint) {
        this.categorieConjoint = categorieConjoint;
    }

    /**
     * Get le modif date conjoint.
     *
     * @return the modifDateConjoint
     */
    public String getModifDateConjoint() {
        return modifDateConjoint;
    }

    /**
     * Set le modif date conjoint.
     *
     * @param modifDateConjoint
     *            the modifDateConjoint to set
     */
    public void setModifDateConjoint(String modifDateConjoint) {
        this.modifDateConjoint = modifDateConjoint;
    }

    /**
     * Get le pp numero securite sociale.
     *
     * @return the ppNumeroSecuriteSociale
     */
    public String getPpNumeroSecuriteSociale() {
        return ppNumeroSecuriteSociale;
    }

    /**
     * Set le pp numero securite sociale.
     *
     * @param ppNumeroSecuriteSociale
     *            the ppNumeroSecuriteSociale to set
     */
    public void setPpNumeroSecuriteSociale(String ppNumeroSecuriteSociale) {
        this.ppNumeroSecuriteSociale = ppNumeroSecuriteSociale;
    }

    /**
     * Get le pp adresse different gerant.
     *
     * @return the ppAdresseDifferentGerant
     */
    public String getPpAdresseDifferentGerant() {
        return ppAdresseDifferentGerant;
    }

    /**
     * Set le pp adresse different gerant.
     *
     * @param ppAdresseDifferentGerant
     *            the ppAdresseDifferentGerant to set
     */
    public void setPpAdresseDifferentGerant(String ppAdresseDifferentGerant) {
        this.ppAdresseDifferentGerant = ppAdresseDifferentGerant;
    }

    /**
     * Get le pp adresse.
     *
     * @return the ppAdresse
     */
    public AdresseBean getPpAdresse() {
        return ppAdresse;
    }

    /**
     * Set le pp adresse.
     *
     * @param ppAdresse
     *            the ppAdresse to set
     */
    public void setPpAdresse(AdresseBean ppAdresse) {
        this.ppAdresse = ppAdresse;
    }

    /**
     * get le PpCouvertSecuriteSociale
     * 
     * @return ppCouvertSecuriteSociale
     */
    public String getPpCouvertSecuriteSociale() {
        return ppCouvertSecuriteSociale;
    }

    /**
     * set le PpCouvertSecuriteSociale
     * 
     * @param PpCouvertSecuriteSociale
     */
    public void setPpCouvertSecuriteSociale(String ppCouvertSecuriteSociale) {
        this.ppCouvertSecuriteSociale = ppCouvertSecuriteSociale;
    }
}
