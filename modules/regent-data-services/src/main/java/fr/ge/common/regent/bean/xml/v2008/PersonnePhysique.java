package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface PersonnePhysique.
 */
@ResourceXPath("PersonnePhysique")
public interface PersonnePhysique {

  /**
   * Get le cap.
   *
   * @return le cap
   */
  @FieldXPath("CAP")
  CAP[] getCAP();

  /**
   * Ajoute le to cap.
   *
   * @return le cap
   */
  CAP addToCAP();

  /**
   * Set le cap.
   *
   * @param CAP
   *          le nouveau cap
   */
  void setCAP(CAP[] CAP);

  /**
   * Get le cap.
   *
   * @return le cap
   */
  @FieldXPath("DAP")
  DAP[] getDAP();

  /**
   * Ajoute le to cap.
   *
   * @return le cap
   */
  DAP addToDAP();

  /**
   * Set le cap.
   *
   * @param CAP
   *          le nouveau cap
   */
  void setDAP(DAP[] DAP);

  /**
   * Get le jgp.
   *
   * @return le jgp
   */
  @FieldXPath("JGP")
  JGP[] getJGP();

  /**
   * Ajoute le to jgp.
   *
   * @return le jgp
   */
  JGP addToJGP();

  /**
   * Set le jgp.
   *
   * @param JGP
   *          le nouveau jgp
   */
  void setJGP(JGP[] JGP);

  /**
   * Get le smp.
   *
   * @return le smp
   */
  @FieldXPath("SMP")
  SMP[] getSMP();

  /**
   * Ajoute le to smp.
   *
   * @return le smp
   */
  SMP addToSMP();

  /**
   * Set le smp.
   *
   * @param SMP
   *          le nouveau smp
   */
  void setSMP(SMP[] SMP);

  /**
   * Get le nap.
   *
   * @return le nap
   */
  @FieldXPath("NAP")
  NAP[] getNAP();

  /**
   * Ajoute le to nap.
   *
   * @return le nap
   */
  NAP addToNAP();

  /**
   * Set le nap.
   *
   * @param NAP
   *          le nouveau nap
   */
  void setNAP(NAP[] NAP);

  /**
   * Get le MEP.
   *
   * @return le MEP
   */
  @FieldXPath("MEP")
  MEP[] getMEP();

  /**
   * Ajoute le to MEP.
   *
   * @return le MEP
   */
  MEP addToMEP();

  /**
   * Set le MEP.
   *
   * @param MEP
   *          le nouveau MEP
   */
  void setMEP(MEP[] MEP);

  /**
   * Get le ISP.
   *
   * @return le ISP
   */
  @FieldXPath("ISP")
  ISP[] getISP();

  /**
   * Ajoute le to ISP.
   *
   * @return le ISP
   */
  ISP addToISP();

  /**
   * Set le ISP.
   *
   * @param ISP
   *          le nouveau ISP
   */
  void setISP(ISP[] ISP);

  /**
   * Get le aip.
   *
   * @return le aip
   */
  @FieldXPath("AIP")
  AIP[] getAIP();

  /**
   * Ajoute le to aip.
   *
   * @return le aip
   */
  AIP addToAIP();

  /**
   * Set le aip.
   *
   * @param AIP
   *          le nouveau aip
   */
  void setAIP(AIP[] AIP);

  /**
   * Get le icp.
   *
   * @return le icp
   */
  @FieldXPath("ICP")
  ICP[] getICP();

  /**
   * Ajoute le to icp.
   *
   * @return le icp
   */
  ICP addToICP();

  /**
   * Set le icp.
   *
   * @param ICP
   *          le nouveau icp
   */
  void setICP(ICP[] ICP);

  /**
   * Get le gcs.
   *
   * @return le gcs
   */
  @FieldXPath("GCS")
  GCS[] getGCS();

  /**
   * Ajoute le to gcs.
   *
   * @return le gcs
   */
  GCS addToGCS();

  /**
   * Set le gcs.
   *
   * @param GCS
   *          le nouveau gcs
   */
  void setGCS(GCS[] GCS);
}
