package fr.ge.common.regent.bean.formalite.profil.creation;

import java.io.Serializable;

/**
 * 
 * Classe parente de tous les beans du profil entreprise.
 * 
 */
public class ProfilCreationPourTestBean implements Serializable {

  /** La constante serialVersionUID. */
  private static final long serialVersionUID = 6635870283610191937L;

  /** Le version. */
  private String version;

  /**
   * Get le version.
   *
   * @return le version
   */
  public String getVersion() {
    return this.version;
  }

  /**
   * Set le version.
   *
   * @param version
   *          le nouveau version
   */
  public void setVersion(String version) {
    this.version = version;
  }

}
