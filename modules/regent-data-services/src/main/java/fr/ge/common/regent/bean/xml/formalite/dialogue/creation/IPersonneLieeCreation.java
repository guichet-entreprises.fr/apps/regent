package fr.ge.common.regent.bean.xml.formalite.dialogue.creation;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

import fr.ge.common.regent.bean.xml.formalite.IFormalite;
import fr.ge.common.regent.bean.xml.transverse.IAdresse;

/**
 * Le Interface de l'entité des personne liée à l'entreprise
 * 
 * 
 * 
 */
@ResourceXPath("/formalite")
public interface IPersonneLieeCreation extends IFormalite {

    /**
     * Getter de l'attribut adresse.
     * 
     * @return la valeur de adresse
     */

    @FieldXPath(value = "ppAdresse")
    IAdresse getPpAdresse();

    /**
     * New ppadresse.
     *
     * @return le i adresse
     */
    IAdresse newPpAdresse();

    /**
     * Setter de l'attribut ppAdresse.
     * 
     * @param ppAdresse
     *            la nouvelle valeur de ppAdresse
     */
    public void setPpAdresse(IAdresse ppAdresse);

    /**
     * Getter de l'attribut personnePouvoirLimiteEtablissement.
     * 
     * @return la valeur de personnePouvoirLimiteEtablissement
     */
    @FieldXPath(value = "personnePouvoirLimiteEtablissement")
    String getPersonnePouvoirLimiteEtablissement();

    /**
     * Getter de l'attribut ppCivilite.
     * 
     * @return la valeur de ppCivilite
     */
    @FieldXPath(value = "ppCivilite")
    String getPpCivilite();

    /**
     * Getter de l'attribut personneLieeEtablissementQualite.
     * 
     * @return la valeur de personneLieeEtablissementQualite
     */
    @FieldXPath(value = "personneLieeEtablissementQualite")
    String getPersonneLieeEtablissementQualite();

    /**
     * Getter de l'attribut ppDateNaissance.
     * 
     * @return la valeur de ppDateNaissance
     */
    @FieldXPath(value = "ppDateNaissance")
    String getPpDateNaissance();

    /**
     * Getter de l'attribut ppLieuNaissanceCommune.
     * 
     * @return la valeur de ppLieuNaissanceCommune
     */
    @FieldXPath(value = "ppLieuNaissanceCommune")
    String getPpLieuNaissanceCommune();

    /**
     * Getter de l'attribut ppLieuNaissanceDepartement.
     * 
     * @return la valeur de ppLieuNaissanceDepartement
     */
    @FieldXPath(value = "ppLieuNaissanceDepartement")
    String getPpLieuNaissanceDepartement();

    /**
     * Getter de l'attribut ppLieuNaissancePays.
     * 
     * @return la valeur de ppLieuNaissancePays
     */
    @FieldXPath(value = "ppLieuNaissancePays")
    String getPpLieuNaissancePays();

    /**
     * Getter de l'attribut ppLieuNaissanceVille.
     * 
     * @return la valeur de ppLieuNaissanceVille
     */
    @FieldXPath(value = "ppLieuNaissanceVille")
    String getPpLieuNaissanceVille();

    /**
     * Getter de l'attribut ppNationalite.
     * 
     * @return la valeur de ppNationalite
     */
    @FieldXPath(value = "ppNationalite")
    String getPpNationalite();

    /**
     * Getter de l'attribut ppNomNaissance.
     * 
     * @return la valeur de ppNomNaissance
     */
    @FieldXPath(value = "ppNomNaissance")
    String getPpNomNaissance();

    /**
     * Getter de l'attribut ppNomUsage.
     * 
     * @return la valeur de ppNomUsage
     */
    @FieldXPath(value = "ppNomUsage")
    String getPpNomUsage();

    /**
     * Getter de l'attribut ppPrenom1.
     * 
     * @return la valeur de ppPrenom1
     */
    @FieldXPath(value = "ppPrenom1")
    String getPpPrenom1();

    /**
     * Getter de l'attribut ppPrenom2.
     * 
     * @return la valeur de ppPrenom2
     */
    @FieldXPath(value = "ppPrenom2")
    String getPpPrenom2();

    /**
     * Getter de l'attribut ppPrenom3.
     * 
     * @return la valeur de ppPrenom3
     */
    @FieldXPath(value = "ppPrenom3")
    String getPpPrenom3();

    /**
     * Getter de l'attribut ppPrenom4.
     * 
     * @return la valeur de ppPrenom4
     */
    @FieldXPath(value = "ppPrenom4")
    String getPpPrenom4();

    /**
     * Setter de l'attribut personneLieeEtablissementQualite.
     * 
     * @param personneLieeEtablissementQualite
     *            la nouvelle valeur de personneLieeEtablissementQualite
     */
    void setPersonneLieeEtablissementQualite(String personneLieeEtablissementQualite);

    /**
     * Setter de l'attribut ppDateNaissance.
     * 
     * @param ppDateNaissance
     *            la nouvelle valeur de ppDateNaissance
     */
    void setPpDateNaissance(String ppDateNaissance);

    /**
     * Setter de l'attribut ppLieuNaissanceCommune.
     * 
     * @param ppLieuNaissanceCommune
     *            la nouvelle valeur de ppLieuNaissanceCommune
     */
    void setPpLieuNaissanceCommune(String ppLieuNaissanceCommune);

    /**
     * Setter de l'attribut ppLieuNaissanceDepartement.
     * 
     * @param ppLieuNaissanceDepartement
     *            la nouvelle valeur de ppLieuNaissanceDepartement
     */
    void setPpLieuNaissanceDepartement(String ppLieuNaissanceDepartement);

    /**
     * Setter de l'attribut ppLieuNaissancePays.
     * 
     * @param ppLieuNaissancePays
     *            la nouvelle valeur de ppLieuNaissancePays
     */
    void setPpLieuNaissancePays(String ppLieuNaissancePays);

    /**
     * Setter de l'attribut ppLieuNaissanceVille.
     * 
     * @param ppLieuNaissanceVille
     *            la nouvelle valeur de ppLieuNaissanceVille
     */
    void setPpLieuNaissanceVille(String ppLieuNaissanceVille);

    /**
     * Setter de l'attribut ppNationalite.
     * 
     * @param ppNationalite
     *            la nouvelle valeur de ppNationalite
     */
    void setPpNationalite(String ppNationalite);

    /**
     * Setter de l'attribut ppNomNaissance.
     * 
     * @param ppNomNaissance
     *            la nouvelle valeur de ppNomNaissance
     */
    void setPpNomNaissance(String ppNomNaissance);

    /**
     * Setter de l'attribut ppNomUsage.
     * 
     * @param ppNomUsage
     *            la nouvelle valeur de ppNomUsage
     */
    void setPpNomUsage(String ppNomUsage);

    /**
     * Setter de l'attribut ppPrenom1.
     * 
     * @param ppPrenom1
     *            la nouvelle valeur de ppPrenom1
     */
    void setPpPrenom1(String ppPrenom1);

    /**
     * Setter de l'attribut ppPrenom2.
     * 
     * @param ppPrenom2
     *            la nouvelle valeur de ppPrenom2
     */
    void setPpPrenom2(String ppPrenom2);

    /**
     * Setter de l'attribut ppPrenom3.
     * 
     * @param ppPrenom3
     *            la nouvelle valeur de ppPrenom3
     */
    void setPpPrenom3(String ppPrenom3);

    /**
     * Setter de l'attribut ppPrenom4.
     * 
     * @param ppPrenom4
     *            la nouvelle valeur de ppPrenom4
     */
    void setPpPrenom4(String ppPrenom4);

    /**
     * Setter de l'attribut personnePouvoirLimiteEtablissement.
     * 
     * @param personnePouvoirLimiteEtablissement
     *            la nouvelle valeur de personnePouvoirLimiteEtablissement
     */
    void setPersonnePouvoirLimiteEtablissement(String personnePouvoirLimiteEtablissement);

    /**
     * Setter de l'attribut ppCivilite.
     * 
     * @param ppCivilite
     *            la nouvelle valeur de ppCivilite
     */
    void setPpCivilite(String ppCivilite);
}
