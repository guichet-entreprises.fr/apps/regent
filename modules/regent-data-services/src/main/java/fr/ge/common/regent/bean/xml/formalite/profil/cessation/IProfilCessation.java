package fr.ge.common.regent.bean.xml.formalite.profil.cessation;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;
import org.xmlfield.core.types.XmlString;

import fr.ge.common.regent.bean.xml.formalite.IFormalite;
import fr.ge.common.regent.bean.xml.formalite.profil.creation.ICfe;
import fr.ge.common.regent.bean.xml.transverse.IPostalCommune;

/**
 * Le Interface IProfilEntrepriseCessation.
 */
@ResourceXPath("/formalite")
public interface IProfilCessation extends IFormalite {

    /** La constante MODEL_VERSION. */
    static final int MODEL_VERSION = 1;

    /**
     * Get le cfe.
     * 
     * @return le cfe
     */
    @FieldXPath(value = "cfe")
    ICfe getCfe();

    /**
     * New cfe.
     * 
     * @return le i cfe
     */
    ICfe newCfe();

    /**
     * Get le forme juridique.
     * 
     * @return le forme juridique
     */
    @FieldXPath(value = "formeJuridique")
    String getFormeJuridique();

    /**
     * Get le micro social oui non.
     * 
     * @return le micro social oui non
     */
    @FieldXPath(value = "microSocialOuiNon")
    String getMicroSocialOuiNon();

    /**
     * Get le activite principale code ape.
     * 
     * @return le activite principale code ape
     */
    @FieldXPath(value = "principaleCodeAPE")
    String getActivitePrincipaleCodeAPE();

    /**
     * Get le activite principale secteur.
     * 
     * @return le activite principale secteur
     */
    @FieldXPath(value = "activitePrincipaleSecteur")
    String getActivitePrincipaleSecteur();

    /**
     * Get le existe activite secondaire.
     * 
     * @return le existe activite secondaire
     */
    @FieldXPath(value = "existeActiviteSecondaire")
    String getExisteActiviteSecondaire();

    /**
     * Get le activite secondaire domaine.
     * 
     * @return le activite secondaire domaine
     */
    @FieldXPath(value = "activiteSecondaireDomaine")
    String getActiviteSecondaireDomaine();

    /**
     * Get le activite secondaire secteur.
     * 
     * @return le activite secondaire secteur
     */
    @FieldXPath(value = "activiteSecondaireSecteur")
    String getActiviteSecondaireSecteur();

    /**
     * Get le activite secondaire code activite.
     * 
     * @return le activite secondaire code activite
     */
    @FieldXPath(value = "activiteSecondaireCodeActivite")
    String getActiviteSecondaireCodeActivite();

    /**
     * Get le postal commune.
     * 
     * @return le postal commune
     */
    @FieldXPath(value = "postalCommune")
    IPostalCommune getPostalCommune();

    /**
     * New postal commune.
     * 
     * @return le i postal commune
     */
    IPostalCommune newPostalCommune();

    /**
     * Get le nom dossier.
     * 
     * @return le nom dossier
     */
    @FieldXPath(value = "nomDossier")
    String getNomDossier();

    /**
     * Get le option cmacc i1.
     * 
     * @return le option cmacc i1
     */
    @FieldXPath(value = "optionCMACCI1")
    String getOptionCMACCI1();

    /**
     * Get le option cmacc i2.
     * 
     * @return le option cmacc i2
     */
    @FieldXPath(value = "optionCMACCI2")
    String getOptionCMACCI2();

    /**
     * Get le secteur cfe.
     * 
     * @return le secteur cfe
     */
    @FieldXPath(value = "secteurCfe")
    String getSecteurCfe();

    /**
     * Get le est inscrit registre public.
     * 
     * @return le est inscrit registre public
     */
    @FieldXPath(value = "estInscritRegistrePublic")
    String getEstInscritRegistrePublic();

    /**
     * Get le activite principale secteur2.
     * 
     * @return le activite principale secteur2
     */
    @FieldXPath(value = "activitePrincipaleSecteur2")
    String getActivitePrincipaleSecteur2();

    /**
     * Get le type cfe.
     * 
     * @return le type cfe
     */
    @FieldXPath(value = "typeCfe")
    String getTypeCfe();

    /**
     * {@inheritDoc}
     */
    @Override
    /*
     * (non-Javadoc)
     * 
     * @see fr.guichetentreprises.objetsMetier.IFormalite#getReseauCFE()
     */
    @FieldXPath(value = "reseauCFE")
    String getReseauCFE();

    /**
     * Get le evenement.
     * 
     * @return le evenement
     */
    @FieldXPath(value = "evenements/evenement")
    XmlString[] getEvenement();

    /**
     * Set le cfe.
     * 
     * @param cfe
     *            le nouveau cfe
     */
    void setCfe(ICfe cfe);

    /**
     * Set le forme juridique.
     * 
     * @param formeJuridique
     *            le nouveau forme juridique
     */
    void setFormeJuridique(String formeJuridique);

    /**
     * Set le micro social oui non.
     * 
     * @param microSocialOuiNon
     *            le nouveau micro social oui non
     */
    void setMicroSocialOuiNon(String microSocialOuiNon);

    /**
     * Set le activite principale code ape.
     * 
     * @param activitePrincipaleCodeAPE
     *            le nouveau activite principale code ape
     */
    void setActivitePrincipaleCodeAPE(String activitePrincipaleCodeAPE);

    /**
     * Set le activite principale secteur.
     * 
     * @param activitePrincipaleSecteur
     *            le nouveau activite principale secteur
     */
    void setActivitePrincipaleSecteur(String activitePrincipaleSecteur);

    /**
     * Set le existe activite secondaire.
     * 
     * @param existeActiviteSecondaire
     *            le nouveau existe activite secondaire
     */
    void setExisteActiviteSecondaire(String existeActiviteSecondaire);

    /**
     * Set le activite secondaire domaine.
     * 
     * @param activiteSecondaireDomaine
     *            le nouveau activite secondaire domaine
     */
    void setActiviteSecondaireDomaine(String activiteSecondaireDomaine);

    /**
     * Set le activite secondaire secteur.
     * 
     * @param activiteSecondaireSecteur
     *            le nouveau activite secondaire secteur
     */
    void setActiviteSecondaireSecteur(String activiteSecondaireSecteur);

    /**
     * Set le activite secondaire code activite.
     * 
     * @param activiteSecondaireCodeActivite
     *            le nouveau activite secondaire code activite
     */
    void setActiviteSecondaireCodeActivite(String activiteSecondaireCodeActivite);

    /**
     * Accesseur sur l'attribut {@link #descr libre activite secondaire}.
     *
     * @return descr libre activite secondaire
     */
    @FieldXPath(value = "descrLibreActiviteSecondaire")
    String getDescrLibreActiviteSecondaire();

    /**
     * Mutateur sur l'attribut {@link #descr libre activite secondaire}.
     *
     * @param descrLibreActiviteSecondaire
     *            la nouvelle valeur de l'attribut descr libre activite
     *            secondaire
     */
    void setDescrLibreActiviteSecondaire(String descrLibreActiviteSecondaire);

    /**
     * Set le postal commune.
     * 
     * @param postalCommune
     *            le nouveau postal commune
     */
    void setPostalCommune(IPostalCommune postalCommune);

    /**
     * Set le secteur cfe.
     * 
     * @param secteurCFE
     *            le nouveau secteur cfe
     */
    void setSecteurCfe(String secteurCFE);

    /**
     * Set le nom dossier.
     * 
     * @param nomDossier
     *            le nouveau nom dossier
     */
    void setNomDossier(String nomDossier);

    /**
     * Set le option cmacc i1.
     * 
     * @param optionCMACCI1
     *            le nouveau option cmacc i1
     */
    void setOptionCMACCI1(String optionCMACCI1);

    /**
     * Set le option cmacc i2.
     * 
     * @param optionCMACCI2
     *            le nouveau option cmacc i2
     */
    void setOptionCMACCI2(String optionCMACCI2);

    /**
     * Set le est inscrit registre public.
     * 
     * @param estInscritRegistrePublic
     *            le nouveau est inscrit registre public
     */
    void setEstInscritRegistrePublic(String estInscritRegistrePublic);

    /**
     * Set le activite principale secteur2.
     * 
     * @param activitePrincipaleSecteur2
     *            le nouveau activite principale secteur2
     */
    void setActivitePrincipaleSecteur2(String activitePrincipaleSecteur2);

    /**
     * Set le type cfe.
     * 
     * @param typeCfe
     *            le nouveau type cfe
     */
    void setTypeCfe(String typeCfe);

    /**
     * {@inheritDoc}
     */
    @Override
    void setReseauCFE(String reseauCFE);

    /**
     * Set le evenement.
     * 
     * @param evenement
     *            le nouveau evenement
     */
    void setEvenement(XmlString[] evenement);

    /**
     * Méthode pour rajouter un évenement.
     * 
     * @return {@link XmlString}
     */
    XmlString addToEvenement();

    /**
     * Méthode qui supprime un évenemnt.
     * 
     * @param evenement
     *            {@link XmlString}
     */
    void removeFromEvenement(XmlString evenement);

}
