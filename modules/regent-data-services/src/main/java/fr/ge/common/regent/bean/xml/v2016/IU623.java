package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;
import org.xmlfield.core.types.XmlString;

/**
 * IU623.
 * 
 * @author $Author: hhichri $
 * @version $Revision: 0 $
 */
@ResourceXPath("/U62.3")
public interface IU623 {

  /**
   * Get le U62.3.1.
   *
   * @return le U62.3.1
   */
  @FieldXPath("U62.3.1")
  String getU6231();

  /**
   * Set le U62.3.1.
   *
   * @param U6231
   *          .1 le nouveau U62.3.1
   */
  void setU6231(String U6231);

  /**
   * Get le U62.3.2.
   *
   * @return le U62.3.2
   */
  @FieldXPath("U62.3.2")
  String getU6232();

  /**
   * Set le U62.3.2.
   *
   * @param U6232
   *          le nouveau U62.3.2
   */
  void setU6232(String U6232);

  /**
   * Get le U6233.
   *
   * @return le U6233
   */
  @FieldXPath("U62.3.3")
  XmlString[] getU6233();

  /**
   * Add To le U6233.
   * 
   * @return U6233
   */
  XmlString addToU6233();

  /**
   * Set le U6233.
   *
   * @param U6233
   *          le nouveau U6233
   */
  void setU6233(XmlString[] U6233);

  /**
   * Get le U62.3.4.
   *
   * @return le U62.3.4
   */
  @FieldXPath("U62.3.4")
  String getU6234();

  /**
   * Set le U62.3.4.
   *
   * @param U6234
   *          .4 le nouveau U62.3.4
   */
  void setU6234(String U6234);

  /**
   * Get le U62.3.5.
   *
   * @return le U62.3.5
   */
  @FieldXPath("U62.3.5")
  String getU6235();

  /**
   * Set le U62.3.5.
   *
   * @param U6235
   *          .5 le nouveau U62.3.5
   */
  void setU6235(String U6235);

  /**
   * Get le U62.3.6.
   *
   * @return le U62.3.6
   */
  @FieldXPath("U62.3.6")
  String getU6236();

  /**
   * Set le U62.3.6.
   *
   * @param U6236
   *          .6 le nouveau U62.3.6
   */
  void setU6236(String U6236);

  /**
   * Get le U62.3.7.
   *
   * @return le U62.3.7
   */
  @FieldXPath("U62.3.7")
  String getU6237();

  /**
   * Set le U62.3.7.
   *
   * @param U6237
   *          .7 le nouveau U62.3.7
   */
  void setU6237(String U6237);

}
