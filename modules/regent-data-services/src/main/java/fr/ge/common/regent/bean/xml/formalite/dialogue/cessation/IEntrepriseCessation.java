package fr.ge.common.regent.bean.xml.formalite.dialogue.cessation;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

import fr.ge.common.regent.bean.xml.formalite.IFormalite;

/**
 * Le Interface IEntrepriseCessation.
 */
@ResourceXPath("/formalite")
public interface IEntrepriseCessation extends IFormalite {

    /**
     * Get le exercice double activite.
     *
     * @return le exercice double activite
     */
    @FieldXPath(value = "exerciceDoubleActivite")
    String getExerciceDoubleActivite();

    /**
     * Get le siren.
     *
     * @return le siren
     */
    @FieldXPath(value = "siren")
    String getSiren();

    /**
     * Get le cma immat.
     *
     * @return le cma immat
     */
    @FieldXPath(value = "cmaImmat")
    String getCmaImmat();

    /**
     * Get le greffe immat.
     *
     * @return le greffe immat
     */
    @FieldXPath(value = "greffeImmat")
    String getGreffeImmat();

    /**
     * Get le date radiation.
     *
     * @return le date radiation
     */
    @FieldXPath(value = "dateRadiation")
    String getDateRadiation();

    /**
     * Get le cessation suite deces.
     *
     * @return le cessation suite deces
     */
    @FieldXPath(value = "cessationSuiteDeces")
    String getCessationSuiteDeces();

    /**
     * Get le date cessation emploi salarie.
     *
     * @return le date cessation emploi salarie
     */
    @FieldXPath(value = "dateCessationEmploiSalarie")
    String getDateCessationEmploiSalarie();

    /**
     * Get le eirl declaration.
     *
     * @return le eirl declaration
     */
    @FieldXPath(value = "eirlDeclaration")
    String getEirlDeclaration();

    /**
     * Get le eirl denomination.
     *
     * @return le eirl denomination
     */
    @FieldXPath(value = "eirlDenomination")
    String getEirlDenomination();

    /**
     * Get le eirl registre depot.
     *
     * @return le eirl registre depot
     */
    @FieldXPath(value = "eirlRegistreDepot")
    String getEirlRegistreDepot();

    /**
     * Get le message alerte.
     *
     * @return le message alerte
     */
    @FieldXPath(value = "messageAlerte")
    String getMessageAlerte();

    /**
     * Get le eirl registre lieu.
     *
     * @return le eirl registre lieu
     */
    @FieldXPath(value = "eirlRegistreLieu")
    String getEirlRegistreLieu();

    /**
     * Get le destination patrimoine affecte.
     *
     * @return le destination patrimoine affecte
     */
    @FieldXPath(value = "destinationPatrimoineAffecte")
    String getDestinationPatrimoineAffecte();

    /**
     * Get le non sedentarite qualite.
     *
     * @return le non sedentarite qualite
     */
    @FieldXPath(value = "nonSedentariteQualite")
    String getNonSedentariteQualite();

    /**
     * Get le etablissement.
     *
     * @return le etablissement
     */
    @FieldXPath(value = "etablissements/etablissement")
    IEtablissementCessation[] getEtablissements();

    /**
     * Get le dirigeants.
     *
     * @return le dirigeants
     */
    @FieldXPath(value = "dirigeants")
    IDirigeantCessation[] getDirigeants();

    /** S'il existe ou pas un autre etablissement. */

    /**
     * Get le lieu de depôt d'Impot.
     *
     * @return lieuDepotImpot
     */

    @FieldXPath(value = "lieuDepotImpot")
    String getLieuDepotImpot();

    /**
     * Get autre etablissement.
     *
     * @return existenceAutreEtablissement
     */

    @FieldXPath(value = "existenceAutreEtablissement")
    String getExistenceAutreEtablissement();

    /**
     * Get le nombre autre etablissement.
     *
     * @return nombreAutreEtablissement
     */

    @FieldXPath(value = "nombreAutreEtablissement")
    String getNombreAutreEtablissement();

    /**
     * Retire le from dirigeants.
     *
     * @param etablissements
     *            le etablissements
     */
    void removeFromEtablissements(final IEtablissementCessation etablissements);

    /**
     * Ajoute le to etablissements.
     *
     * @return les etablissements
     */
    IEtablissementCessation addToEtablissements();

    /**
     * Set le etablissements.
     *
     * @param etablissements
     *            le nouveau etablissements
     */
    void setEtablissements(final IEtablissementCessation[] etablissements);

    /**
     * Retire le from dirigeants.
     *
     * @param dirigeants
     *            le dirigeants
     */
    void removeFromDirigeants(IDirigeantCessation dirigeants);

    /**
     * Ajoute le to dirigeants.
     *
     * @return le i dirigeant cessation
     */
    IDirigeantCessation addToDirigeants();

    /**
     * Set le dirigeants.
     *
     * @param dirigeants
     *            le nouveau dirigeants
     */
    void setDirigeants(IDirigeantCessation[] dirigeants);

    /**
     * Set le siren.
     *
     * @param siren
     *            le nouveau siren
     */
    void setSiren(String siren);

    /**
     * Set le exercice double activite.
     *
     * @param exerciceDoubleActivite
     *            le nouveau exercice double activite
     */
    void setExerciceDoubleActivite(String exerciceDoubleActivite);

    /**
     * Set le greffe immat.
     *
     * @param greffeImmat
     *            le nouveau greffe immat
     */
    void setGreffeImmat(String greffeImmat);

    /**
     * Set le cma immat.
     *
     * @param cmaImmat
     *            le nouveau cma immat
     */
    void setCmaImmat(String cmaImmat);

    /**
     * Set le date radiation.
     *
     * @param dateRadiation
     *            le nouveau date radiation
     */
    void setDateRadiation(String dateRadiation);

    /**
     * Set le cessation suite deces.
     *
     * @param cessationSuiteDeces
     *            le nouveau cessation suite deces
     */
    void setCessationSuiteDeces(String cessationSuiteDeces);

    /**
     * Set le date cessation emploi salarie.
     *
     * @param dateCessationEmploiSalarie
     *            le nouveau date cessation emploi salarie
     */
    void setDateCessationEmploiSalarie(String dateCessationEmploiSalarie);

    /**
     * Set le eirl declaration.
     *
     * @param eirlDeclaration
     *            le nouveau eirl declaration
     */
    void setEirlDeclaration(String eirlDeclaration);

    /**
     * Set le eirl denomination.
     *
     * @param eirlDenomination
     *            le nouveau eirl denomination
     */
    void setEirlDenomination(String eirlDenomination);

    /**
     * Set le eirl registre depot.
     *
     * @param eirlRegistreDepot
     *            le nouveau eirl registre depot
     */
    void setEirlRegistreDepot(String eirlRegistreDepot);

    /**
     * Set le eirl registre lieu.
     *
     * @param eirlRegistreLieu
     *            le nouveau eirl registre lieu
     */
    void setEirlRegistreLieu(String eirlRegistreLieu);

    /**
     * Set le destination patrimoine affecte.
     *
     * @param destinationPatrimoineAffecte
     *            le nouveau destination patrimoine affecte
     */
    void setDestinationPatrimoineAffecte(String destinationPatrimoineAffecte);

    /**
     * Set le non sedentarite qualite.
     *
     * @param nonSedentariteQualite
     *            le nouveau non sedentarite qualite
     */
    void setNonSedentariteQualite(String nonSedentariteQualite);

    /**
     * Set le message alerte.
     *
     * @param messageAlerte
     *            le nouveau message alerte
     */
    void setMessageAlerte(String messageAlerte);

    /**
     * Set le lieu de depôt d'Impot.
     *
     * @param lieuDepotImpot
     *            le nouveau nombre dirigeants
     */

    void setLieuDepotImpot(final String lieuDepotImpot);

    /**
     * Set autre etablissement..
     *
     * @param existenceAutreEtablissement
     *            le nouveau nombre dirigeants
     */

    void setExistenceAutreEtablissement(final String existenceAutreEtablissement);

    /**
     * Set le nombre autre etablissement.
     *
     * @param nombreAutreEtablissement
     *            le nombre autre etablissement
     */
    void setNombreAutreEtablissement(final String nombreAutreEtablissement);

}
