package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;


/**
 * Le Interface IU40.
 */
@ResourceXPath("/U40")
public interface IU40 {
	
	
	
	/**
	   * Get le U401.
	   *
	   * @return le U401
	   */
	  @FieldXPath("U40.1")
	  String getU401();

	  /**
	   * Set le U401.
	   *
	   * @param U401
	   *          le nouveau U401
	   */
	  void setU401(String U401);

		
	  /**
	   * Get le U402.
	   *
	   * @return le U402
	   */
	  @FieldXPath("U40.2")
	  IU402[] getU402();

	  /**
	   * Ajoute le to U402.
	   *
	   * @return le i U402
	   */
	  IU402 addToU402();

	  /**
	   * Set le P76.
	   *
	   * @param P76
	   *          le nouveau U402
	   */
	  void setU402(IU402[] U402);
	  
	  
	  
	  	  
	  
	  
  
  
	

}
