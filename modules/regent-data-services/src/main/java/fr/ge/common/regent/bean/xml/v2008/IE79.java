package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IE79.
 */
@ResourceXPath("/E79")
public interface IE79 {

  /**
   * Get le e791.
   *
   * @return le e791
   */
  @FieldXPath("E79.1")
  String getE791();

  /**
   * Set le e791.
   *
   * @param E791
   *          le nouveau e791
   */
  void setE791(String E791);

  /**
   * Get le e792.
   *
   * @return le e792
   */
  @FieldXPath("E79.2")
  String getE792();

  /**
   * Set le e792.
   *
   * @param E792
   *          le nouveau e792
   */
  void setE792(String E792);

}
