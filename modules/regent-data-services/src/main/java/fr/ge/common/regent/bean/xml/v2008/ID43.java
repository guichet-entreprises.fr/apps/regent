package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IC403.
 */
@ResourceXPath("/D43")
public interface ID43 {

  /**
   * Get le D43.1.
   *
   * @return le D43.1
   */
  @FieldXPath("D43.1")
  String getD431();

  /**
   * Set le D43.1.
   *
   * @param D431
   *          le nouveau D431
   */
  void setD431(String D431);

  /**
   * Get le D43.2.
   *
   * @return le D43.2
   */
  @FieldXPath("D43.2")
  String getD432();

  /**
   * Set le D43.2.
   *
   * @param D432
   *          le nouveau D432
   */
  void setD432(String D432);

  /**
   * Get le D43.3.
   *
   * @return le D43.3
   */
  @FieldXPath("D43.3")
  String getD433();

  /**
   * Set le D43.3.
   *
   * @param D433
   *          le nouveau D43.3
   */
  void setD433(String D433);

  /**
   * Get le D43.4.
   *
   * @return le D43.4
   */
  @FieldXPath("D43.4")
  String getD434();

  /**
   * Set le D43.4.
   *
   * @param D434
   *          le nouveau D43.4
   */
  void setD434(String D434);
}
