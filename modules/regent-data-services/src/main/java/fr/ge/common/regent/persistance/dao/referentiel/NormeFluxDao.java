package fr.ge.common.regent.persistance.dao.referentiel;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import fr.ge.common.regent.bean.modele.referentiel.ENormeFlux;
import fr.ge.common.regent.constante.enumeration.DestinataireXmlRegentEnum;
import fr.ge.common.regent.persistance.dao.AbstractDao;

/**
 * Le Class NormeFluxDao.
 */
public class NormeFluxDao extends AbstractDao < ENormeFlux, String > {

  /**
   * Instancie un nouveau norme flux dao.
   *
   * @param type
   *          le type
   */
  public NormeFluxDao(Class < ENormeFlux > type) {
    super(type);
  }

  /**
   * Get le flux by evenement and destinataire.
   *
   * @param destinataire
   *          le destinataire
   * @param evenement
   *          le evenement
   * @return le flux by evenement and destinataire
   */
  @SuppressWarnings("unchecked")
  public List < ENormeFlux > getFluxByEvenementAndDestinataire(final DestinataireXmlRegentEnum destinataire,
    final List < String > evenement) {

    Criteria criteria = this.getSession().createCriteria(this.getClazz());
    criteria.add(Restrictions.in("evenement", evenement));
    // criteria.setProjection(Projections.groupProperty(destinataire.getNomAttribut()));
    criteria.addOrder(Order.asc("poid"));

    return criteria.list();

  }
}
