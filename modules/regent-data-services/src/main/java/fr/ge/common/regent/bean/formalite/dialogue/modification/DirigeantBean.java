package fr.ge.common.regent.bean.formalite.dialogue.modification;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import fr.ge.common.regent.bean.formalite.dialogue.AbstractDirigeantBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.AdresseBean;

/**
 * Le Class DirigeantBean.
 * 
 * @author hhichri
 */
public class DirigeantBean extends AbstractDirigeantBean<ConjointBean> implements IFormaliteModificationVue {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -7854891332644736396L;

    /** Le id technique. */
    private String idTechnique;

    /** Le modif identite nom prenom. */
    private List<String> modifIdentiteNomPrenom = new ArrayList<String>();

    /** Le modif date identite. */
    private String modifDateIdentite;

    /** Le modif ancien nom naissance. */
    private String modifAncienNomNaissance;

    /** Le modif ancien nom usage. */
    private String modifAncienNomUsage1;

    /** Le modif ancien nom usage. */
    private String modifAncienNomUsage2;

    /** Le modif ancien nom usage. */
    private String modifAncienNomUsage;

    /** Le modif ancien prenoms1. */
    private String modifAncienPrenoms1;

    /** Le modif ancien prenoms2. */
    private String modifAncienPrenoms2;

    /** Le modif ancien prenoms3. */
    private String modifAncienPrenoms3;

    /** Le modif ancien prenoms4. */
    private String modifAncienPrenoms4;

    /** Le modif ancien pseudonyme. */
    private String modifAncienPseudonyme;

    /** Le modif date domicile. */
    private String modifDateDomicile;

    /** Le modif date nationalite. */
    private String modifDateNationalite;

    /** Le modif ancien domicile. */
    private AdresseBean modifAncienDomicile = new AdresseBean();

    /** Le dept ancein domicile. */
    private String deptAncienDomicile;

    /** Le pp adresse. */
    private AdresseBean pmAdresse = new AdresseBean();

    /** Le pp adresse. */
    private AdresseBean pmRepresentantAdresse = new AdresseBean();

    /**
     * Get le pp adresse.
     * 
     * @return the ppAdresse
     */
    public AdresseBean getPmRepresentantAdresse() {
        return pmRepresentantAdresse;
    }

    /**
     * Set le pp adresse.
     * 
     * @param ppAdresse
     *            the ppAdresse to set
     */
    public void setPmRepresentantAdresse(AdresseBean pmRepresentantAdresse) {
        this.pmRepresentantAdresse = pmRepresentantAdresse;
    }

    /**
     * Get le pp adresse.
     * 
     * @return the ppAdresse
     */
    public AdresseBean getPmAdresse() {
        return pmAdresse;
    }

    /**
     * Set le pp adresse.
     * 
     * @param ppAdresse
     *            the ppAdresse to set
     */
    public void setPmAdresse(AdresseBean pmAdresse) {
        this.pmAdresse = pmAdresse;
    }

    /**
     * Get le id technique.
     * 
     * @return the idTechnique
     */
    public String getIdTechnique() {
        return this.idTechnique;
    }

    /**
     * Set le id technique.
     * 
     * @param idTechnique
     *            the idTechnique to set
     */
    public void setIdTechnique(String idTechnique) {
        this.idTechnique = idTechnique;
    }

    /**
     * Get le modif identite nom prenom.
     * 
     * @return the modifIdentiteNomPrenom
     */
    public List<String> getModifIdentiteNomPrenom() {
        return this.modifIdentiteNomPrenom;
    }

    /**
     * Set le modif identite nom prenom.
     * 
     * @param modifIdentiteNomPrenom
     *            the modifIdentiteNomPrenom to set
     */
    public void setModifIdentiteNomPrenom(List<String> modifIdentiteNomPrenom) {
        this.modifIdentiteNomPrenom = modifIdentiteNomPrenom;
    }

    /**
     * Get le modif date identite.
     * 
     * @return the modifDateIdentite
     */
    public String getModifDateIdentite() {
        return this.modifDateIdentite;
    }

    /**
     * Set le modif date identite.
     * 
     * @param modifDateIdentite
     *            the modifDateIdentite to set
     */
    public void setModifDateIdentite(String modifDateIdentite) {
        this.modifDateIdentite = modifDateIdentite;
    }

    /**
     * Get le modif ancien nom naissance.
     * 
     * @return the modifAncienNomNaissance
     */
    public String getModifAncienNomNaissance() {
        return this.modifAncienNomNaissance;
    }

    /**
     * Set le modif ancien nom naissance.
     * 
     * @param modifAncienNomNaissance
     *            the modifAncienNomNaissance to set
     */
    public void setModifAncienNomNaissance(String modifAncienNomNaissance) {
        this.modifAncienNomNaissance = modifAncienNomNaissance;
    }

    /**
     * Get le modif ancien nom usage.
     * 
     * @return the modifAncienNomUsage1
     */
    public String getModifAncienNomUsage1() {
        return this.modifAncienNomUsage1;
    }

    /**
     * Set le modif ancien nom usage.
     * 
     * @param modifAncienNomUsage1
     *            the modifAncienNomUsage1 to set
     */
    public void setModifAncienNomUsage1(String modifAncienNomUsage1) {
        this.modifAncienNomUsage1 = modifAncienNomUsage1;
    }

    /**
     * Get le modif ancien nom usage.
     * 
     * @return the modifAncienNomUsage2
     */
    public String getModifAncienNomUsage2() {
        return this.modifAncienNomUsage2;
    }

    /**
     * Set le modif ancien nom usage.
     * 
     * @param modifAncienNomUsage2
     *            the modifAncienNomUsage2 to set
     */
    public void setModifAncienNomUsage2(String modifAncienNomUsage2) {
        this.modifAncienNomUsage2 = modifAncienNomUsage2;
    }

    /**
     * Get le modif ancien nom usage.
     * 
     * @return the modifAncienNomUsage
     */
    public String getModifAncienNomUsage() {
        return this.modifAncienNomUsage;
    }

    /**
     * Set le modif ancien nom usage.
     * 
     * @param modifAncienNomUsage
     *            the modifAncienNomUsage to set
     */
    public void setModifAncienNomUsage(String modifAncienNomUsage) {
        this.modifAncienNomUsage = modifAncienNomUsage;
    }

    /**
     * Get le modif ancien prenoms1.
     * 
     * @return the modifAncienPrenoms1
     */
    public String getModifAncienPrenoms1() {
        return this.modifAncienPrenoms1;
    }

    /**
     * Set le modif ancien prenoms1.
     * 
     * @param modifAncienPrenoms1
     *            the modifAncienPrenoms1 to set
     */
    public void setModifAncienPrenoms1(String modifAncienPrenoms1) {
        this.modifAncienPrenoms1 = modifAncienPrenoms1;
    }

    /**
     * Get le modif ancien prenoms2.
     * 
     * @return the modifAncienPrenoms2
     */
    public String getModifAncienPrenoms2() {
        return this.modifAncienPrenoms2;
    }

    /**
     * Set le modif ancien prenoms2.
     * 
     * @param modifAncienPrenoms2
     *            the modifAncienPrenoms2 to set
     */
    public void setModifAncienPrenoms2(String modifAncienPrenoms2) {
        this.modifAncienPrenoms2 = modifAncienPrenoms2;
    }

    /**
     * Get le modif ancien prenoms3.
     * 
     * @return the modifAncienPrenoms3
     */
    public String getModifAncienPrenoms3() {
        return this.modifAncienPrenoms3;
    }

    /**
     * Set le modif ancien prenoms3.
     * 
     * @param modifAncienPrenoms3
     *            the modifAncienPrenoms3 to set
     */
    public void setModifAncienPrenoms3(String modifAncienPrenoms3) {
        this.modifAncienPrenoms3 = modifAncienPrenoms3;
    }

    /**
     * Get le modif ancien prenoms4.
     * 
     * @return the modifAncienPrenoms4
     */
    public String getModifAncienPrenoms4() {
        return this.modifAncienPrenoms4;
    }

    /**
     * Set le modif ancien prenoms4.
     * 
     * @param modifAncienPrenoms4
     *            the modifAncienPrenoms4 to set
     */
    public void setModifAncienPrenoms4(String modifAncienPrenoms4) {
        this.modifAncienPrenoms4 = modifAncienPrenoms4;
    }

    /**
     * Get le modif ancien domicile.
     * 
     * @return the modifAncienDomicile
     */
    public AdresseBean getModifAncienDomicile() {
        return this.modifAncienDomicile;
    }

    /**
     * Set le modif ancien domicile.
     * 
     * @param modifAncienDomicile
     *            the modifAncienDomicile to set
     */
    public void setModifAncienDomicile(AdresseBean modifAncienDomicile) {
        this.modifAncienDomicile = modifAncienDomicile;
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see fr.guichetentreprises.vue.AbstractDirigeantBean#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see fr.guichetentreprises.vue.IConjointFactory#createNewConjoint()
     */
    @Override
    public ConjointBean createNewConjoint() {
        return new ConjointBean();
    }

    /**
     * Get le modif ancien pseudonyme.
     * 
     * @return the modifAncienPseudonyme
     */
    public String getModifAncienPseudonyme() {
        return this.modifAncienPseudonyme;
    }

    /**
     * Set le modif ancien pseudonyme.
     * 
     * @param modifAncienPseudonyme
     *            the modifAncienPseudonyme to set
     */
    public void setModifAncienPseudonyme(String modifAncienPseudonyme) {
        this.modifAncienPseudonyme = modifAncienPseudonyme;
    }

    /**
     * Get le modif date domicile.
     * 
     * @return the modifDateDomicile
     */
    public String getModifDateDomicile() {
        return this.modifDateDomicile;
    }

    /**
     * Set le modif date domicile.
     * 
     * @param modifDateDomicile
     *            the modifDateDomicile to set
     */
    public void setModifDateDomicile(String modifDateDomicile) {
        this.modifDateDomicile = modifDateDomicile;
    }

    /**
     * Accesseur sur l'attribut {@link #deptAncienDomicile}.
     * 
     * @return String deptAncienDomicile
     */
    public String getDeptAncienDomicile() {
        return deptAncienDomicile;
    }

    /**
     * Mutateur sur l'attribut {@link #deptAncienDomicile}.
     * 
     * @param deptAncienDomicile
     *            la nouvelle valeur de l'attribut deptAncienDomicile
     */
    public void setDeptAncienDomicile(String deptAncienDomicile) {
        this.deptAncienDomicile = deptAncienDomicile;
    }

    /**
     * Get le modif date nationalite.
     * 
     * @return the modifDateNationalite
     */
    public String getModifDateNationalite() {
        return this.modifDateNationalite;
    }

    /**
     * Set le modif date nationalite.
     * 
     * @param modifDateNationalite
     *            the modifDateNationalite to set
     */
    public void setModifDateNationalite(String modifDateNationalite) {
        this.modifDateNationalite = modifDateNationalite;
    }

}
