/**
 * 
 */
package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Interface PersonneMoraleDirigeante.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
@ResourceXPath("/PersonneMoraleDirigeante")
public interface PersonneMoraleDirigeante {

  /**
   * Accesseur sur l'attribut {@link #ICR}.
   *
   * @return icr
   */
  @FieldXPath("ICR")
  ICR[] getICR();

  /**
   * Adds the to ICR.
   *
   * @return ICR
   */
  ICR addToICR();

  /**
   * Mutateur sur l'attribut {@link #ICR}.
   *
   * @param ICR
   *          la nouvelle valeur de l'attribut ICR
   */
  void setICR(ICR[] ICR);

  /**
   * Accesseur sur l'attribut {@link #JGR}.
   *
   * @return icr
   */
  @FieldXPath("JGR")
  JGR[] getJGR();

  /**
   * Adds the to JGR.
   *
   * @return JGR
   */
  JGR addToJGR();

  /**
   * Mutateur sur l'attribut {@link #JGR}.
   *
   * @param JGR
   *          la nouvelle valeur de l'attribut JGR
   */
  void setJGR(JGR[] JGR);

  /**
   * Get le DIU.
   *
   * @return le DIU
   */
  @FieldXPath("DIU")
  IDIU[] getDIU();

  /**
   * Ajoute le to DIU.
   *
   * @return le DIU
   */
  IDIU addToDIU();

  /**
   * Set le DIU.
   *
   * @param DIU
   *          le nouveau DIU
   */
  void setDIU(IDIU[] DIU);

  /**
   * Get le IDU.
   *
   * @return le IDU
   */
  @FieldXPath("IDU")
  IIDU[] getIDU();

  /**
   * Ajoute le to IDU.
   *
   * @return le IDU
   */
  IIDU addToIDU();

  /**
   * Set le IDU.
   *
   * @param IDU
   *          le nouveau IDU
   */
  void setIDU(IIDU[] IDU);

}
