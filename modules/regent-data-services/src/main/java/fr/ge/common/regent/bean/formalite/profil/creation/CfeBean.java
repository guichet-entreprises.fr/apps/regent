package fr.ge.common.regent.bean.formalite.profil.creation;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import fr.ge.common.regent.bean.formalite.FormaliteVueBean;

/**
 * Le Class CfeBean.
 */
public class CfeBean extends FormaliteVueBean {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 858312207794712761L;

    /** Le adresse1. */
    private String adresse1;

    /** Le adresse2. */
    private String adresse2;

    /** Le adresse3. */
    private String adresse3;

    /** Le code. */
    private String code;

    /** Le fax. */
    private String fax;

    /** Le label activites simplifiees. */
    private String labelActivitesSimplifiees;

    /** Le lien agence presse le. */
    private String lienAgencePresseLE;

    /** Le lien agence presse lps. */
    private String lienAgencePresseLPS;

    /** Le lien agent voyage le. */
    private String lienAgentVoyageLE;

    /** Le lien agent voyage lps. */
    private String lienAgentVoyageLPS;

    /** Le lien collecte dechets huile. */
    private String lienCollecteDechetsHuile;

    /** Le lien collecte dechets pneus. */
    private String lienCollecteDechetsPneus;

    /** Le lien controleur ascenceur le. */
    private String lienControleurAscenceurLE;

    /** Le lien controleur ascenceur lps. */
    private String lienControleurAscenceurLPS;

    /** Le lien debit tabac. */
    private String lienDebitTabac;

    /** Le lien diagnostiqueur immobilier le. */
    private String lienDiagnostiqueurImmobilierLE;

    /** Le lien diagnostiqueur immobilier lps. */
    private String lienDiagnostiqueurImmobilierLPS;

    /** Le lien editeur. */
    private String lienEditeur;

    /** Le lien exploration plateau continental le. */
    private String lienExplorationPlateauContinentalLE;

    /** Le lien exploration plateau continental lps. */
    private String lienExplorationPlateauContinentalLPS;

    /** Le lien genie climatique le. */
    private String lienGenieClimatiqueLE;

    /** Le lien genie climatique lps. */
    private String lienGenieClimatiqueLPS;

    /** Le lien gestion dechets. */
    private String lienGestionDechets;

    /** Le lien publication periodiques. */
    private String lienPublicationPeriodiques;

    /** Le lien terrassement. */
    private String lienTerrassement;

    /** Le lien traitement dechets huile. */
    private String lienTraitementDechetsHuile;

    /** Le lien traitement dechets pneus. */
    private String lienTraitementDechetsPneus;

    /** Le mail. */
    private String mail;

    /** Le nom. */
    private String nom;

    /** Le observation. */
    private String observation;

    /** Le prise connaissance. */
    private String priseConnaissance;

    /** Le site. */
    private String site;

    /** Le telephone. */
    private String telephone;

    // Profil V2 demo : NE PAS SUPPR

    // champs tampon, pour permettre l'affichage dans recapitulatif.html
    /** le activitePrincipaleRecap. */
    private String activitePrincipaleRecap;

    /** le activiteSecondaireRecap. */
    private String activiteSecondaireRecap;

    // FIN Profil V2 demo : NE PAS SUPPR

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    /**
     * Get le serialversionuid.
     * 
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    /**
     * Get le adresse1.
     * 
     * @return the adresse1
     */
    public String getAdresse1() {
        return this.adresse1;
    }

    /**
     * Get le adresse2.
     * 
     * @return the adresse2
     */
    public String getAdresse2() {
        return this.adresse2;
    }

    /**
     * Get le adresse3.
     * 
     * @return the adresse3
     */
    public String getAdresse3() {
        return this.adresse3;
    }

    /**
     * Getter de l'attribut code.
     * 
     * @return la valeur de code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * Get le fax.
     * 
     * @return the fax
     */
    public String getFax() {
        return this.fax;
    }

    /**
     * Getter de l'attribut labelActivitesSimplifiees.
     * 
     * @return la valeur de labelActivitesSimplifiees
     */
    public String getLabelActivitesSimplifiees() {
        return this.labelActivitesSimplifiees;
    }

    /**
     * Getter de l'attribut lienAgencePresseLE.
     * 
     * @return la valeur de lienAgencePresseLE
     */
    public String getLienAgencePresseLE() {
        return this.lienAgencePresseLE;
    }

    /**
     * Getter de l'attribut lienAgencePresseLPS.
     * 
     * @return la valeur de lienAgencePresseLPS
     */
    public String getLienAgencePresseLPS() {
        return this.lienAgencePresseLPS;
    }

    /**
     * Getter de l'attribut lienAgentVoyageLE.
     * 
     * @return la valeur de lienAgentVoyageLE
     */
    public String getLienAgentVoyageLE() {
        return this.lienAgentVoyageLE;
    }

    /**
     * Getter de l'attribut lienAgentVoyageLPS.
     * 
     * @return la valeur de lienAgentVoyageLPS
     */
    public String getLienAgentVoyageLPS() {
        return this.lienAgentVoyageLPS;
    }

    /**
     * Getter de l'attribut lienCollecteDechetsHuile.
     * 
     * @return la valeur de lienCollecteDechetsHuile
     */
    public String getLienCollecteDechetsHuile() {
        return this.lienCollecteDechetsHuile;
    }

    /**
     * Getter de l'attribut lienCollecteDechetsPneus.
     * 
     * @return la valeur de lienCollecteDechetsPneus
     */
    public String getLienCollecteDechetsPneus() {
        return this.lienCollecteDechetsPneus;
    }

    /**
     * Getter de l'attribut lienControleurAscenceurLE.
     * 
     * @return la valeur de lienControleurAscenceurLE
     */
    public String getLienControleurAscenceurLE() {
        return this.lienControleurAscenceurLE;
    }

    /**
     * Getter de l'attribut lienControleurAscenceurLPS.
     * 
     * @return la valeur de lienControleurAscenceurLPS
     */
    public String getLienControleurAscenceurLPS() {
        return this.lienControleurAscenceurLPS;
    }

    /**
     * Getter de l'attribut lienDebitTabac.
     * 
     * @return la valeur de lienDebitTabac
     */
    public String getLienDebitTabac() {
        return this.lienDebitTabac;
    }

    /**
     * Getter de l'attribut lienDiagnostiqueurImmobilierLE.
     * 
     * @return la valeur de lienDiagnostiqueurImmobilierLE
     */
    public String getLienDiagnostiqueurImmobilierLE() {
        return this.lienDiagnostiqueurImmobilierLE;
    }

    /**
     * Getter de l'attribut lienDiagnostiqueurImmobilierLPS.
     * 
     * @return la valeur de lienDiagnostiqueurImmobilierLPS
     */
    public String getLienDiagnostiqueurImmobilierLPS() {
        return this.lienDiagnostiqueurImmobilierLPS;
    }

    /**
     * Getter de l'attribut lienEditeur.
     * 
     * @return la valeur de lienEditeur
     */
    public String getLienEditeur() {
        return this.lienEditeur;
    }

    /**
     * Getter de l'attribut lienExplorationPlateauContinentalLE.
     * 
     * @return la valeur de lienExplorationPlateauContinentalLE
     */
    public String getLienExplorationPlateauContinentalLE() {
        return this.lienExplorationPlateauContinentalLE;
    }

    /**
     * Getter de l'attribut lienExplorationPlateauContinentalLPS.
     * 
     * @return la valeur de lienExplorationPlateauContinentalLPS
     */
    public String getLienExplorationPlateauContinentalLPS() {
        return this.lienExplorationPlateauContinentalLPS;
    }

    /**
     * Getter de l'attribut lienGenieClimatiqueLE.
     * 
     * @return la valeur de lienGenieClimatiqueLE
     */
    public String getLienGenieClimatiqueLE() {
        return this.lienGenieClimatiqueLE;
    }

    /**
     * Getter de l'attribut lienGenieClimatiqueLPS.
     * 
     * @return la valeur de lienGenieClimatiqueLPS
     */
    public String getLienGenieClimatiqueLPS() {
        return this.lienGenieClimatiqueLPS;
    }

    /**
     * Getter de l'attribut lienGestionDechets.
     * 
     * @return la valeur de lienGestionDechets
     */
    public String getLienGestionDechets() {
        return this.lienGestionDechets;
    }

    /**
     * Getter de l'attribut lienPublicationPeriodiques.
     * 
     * @return la valeur de lienPublicationPeriodiques
     */
    public String getLienPublicationPeriodiques() {
        return this.lienPublicationPeriodiques;
    }

    /**
     * Getter de l'attribut lienTerrassement.
     * 
     * @return la valeur de lienTerrassement
     */
    public String getLienTerrassement() {
        return this.lienTerrassement;
    }

    /**
     * Getter de l'attribut lienTraitementDechetsHuile.
     * 
     * @return la valeur de lienTraitementDechetsHuile
     */
    public String getLienTraitementDechetsHuile() {
        return this.lienTraitementDechetsHuile;
    }

    /**
     * Getter de l'attribut lienTraitementDechetsPneus.
     * 
     * @return la valeur de lienTraitementDechetsPneus
     */
    public String getLienTraitementDechetsPneus() {
        return this.lienTraitementDechetsPneus;
    }

    /**
     * Get le mail.
     * 
     * @return the mail
     */
    public String getMail() {
        return this.mail;
    }

    /**
     * Get le nom.
     * 
     * @return the nom
     */
    public String getNom() {
        return this.nom;
    }

    /**
     * Get le observation.
     * 
     * @return the observation
     */
    public String getObservation() {
        return this.observation;
    }

    /**
     * getPriseConnaissance.
     * 
     * @return priseConnaissance
     */
    public String getPriseConnaissance() {
        return this.priseConnaissance;
    }

    /**
     * Get le site.
     * 
     * @return the site
     */
    public String getSite() {
        return this.site;
    }

    /**
     * Get le telephone.
     * 
     * @return the telephone
     */
    public String getTelephone() {
        return this.telephone;
    }

    /**
     * Set le adresse1.
     * 
     * @param adresse1
     *            the adresse1 to set
     */
    public void setAdresse1(String adresse1) {
        this.adresse1 = adresse1;
    }

    /**
     * Set le adresse2.
     * 
     * @param adresse2
     *            the adresse2 to set
     */
    public void setAdresse2(String adresse2) {
        this.adresse2 = adresse2;
    }

    /**
     * Set le adresse3.
     * 
     * @param adresse3
     *            the adresse3 to set
     */
    public void setAdresse3(String adresse3) {
        this.adresse3 = adresse3;
    }

    /**
     * Setter de l'attribut code.
     * 
     * @param code
     *            la nouvelle valeur de code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Set le fax.
     * 
     * @param fax
     *            the fax to set
     */
    public void setFax(String fax) {
        this.fax = fax;
    }

    /**
     * Setter de l'attribut labelActivitesSimplifiees.
     * 
     * @param labelActivitesSimplifiees
     *            la nouvelle valeur de labelActivitesSimplifiees
     */
    public void setLabelActivitesSimplifiees(String labelActivitesSimplifiees) {
        this.labelActivitesSimplifiees = labelActivitesSimplifiees;
    }

    /**
     * Setter de l'attribut lienAgencePresseLE.
     * 
     * @param lienAgencePresseLE
     *            la nouvelle valeur de lienAgencePresseLE
     */
    public void setLienAgencePresseLE(String lienAgencePresseLE) {
        this.lienAgencePresseLE = lienAgencePresseLE;
    }

    /**
     * Setter de l'attribut lienAgencePresseLPS.
     * 
     * @param lienAgencePresseLPS
     *            la nouvelle valeur de lienAgencePresseLPS
     */
    public void setLienAgencePresseLPS(String lienAgencePresseLPS) {
        this.lienAgencePresseLPS = lienAgencePresseLPS;
    }

    /**
     * Setter de l'attribut lienAgentVoyageLE.
     * 
     * @param lienAgentVoyageLE
     *            la nouvelle valeur de lienAgentVoyageLE
     */
    public void setLienAgentVoyageLE(String lienAgentVoyageLE) {
        this.lienAgentVoyageLE = lienAgentVoyageLE;
    }

    /**
     * Setter de l'attribut lienAgentVoyageLPS.
     * 
     * @param lienAgentVoyageLPS
     *            la nouvelle valeur de lienAgentVoyageLPS
     */
    public void setLienAgentVoyageLPS(String lienAgentVoyageLPS) {
        this.lienAgentVoyageLPS = lienAgentVoyageLPS;
    }

    /**
     * Setter de l'attribut lienCollecteDechetsHuile.
     * 
     * @param lienCollecteDechetsHuile
     *            la nouvelle valeur de lienCollecteDechetsHuile
     */
    public void setLienCollecteDechetsHuile(String lienCollecteDechetsHuile) {
        this.lienCollecteDechetsHuile = lienCollecteDechetsHuile;
    }

    /**
     * Setter de l'attribut lienCollecteDechetsPneus.
     * 
     * @param lienCollecteDechetsPneus
     *            la nouvelle valeur de lienCollecteDechetsPneus
     */
    public void setLienCollecteDechetsPneus(String lienCollecteDechetsPneus) {
        this.lienCollecteDechetsPneus = lienCollecteDechetsPneus;
    }

    /**
     * Setter de l'attribut lienControleurAscenceurLE.
     * 
     * @param lienControleurAscenceurLE
     *            la nouvelle valeur de lienControleurAscenceurLE
     */
    public void setLienControleurAscenceurLE(String lienControleurAscenceurLE) {
        this.lienControleurAscenceurLE = lienControleurAscenceurLE;
    }

    /**
     * Setter de l'attribut lienControleurAscenceurLPS.
     * 
     * @param lienControleurAscenceurLPS
     *            la nouvelle valeur de lienControleurAscenceurLPS
     */
    public void setLienControleurAscenceurLPS(String lienControleurAscenceurLPS) {
        this.lienControleurAscenceurLPS = lienControleurAscenceurLPS;
    }

    /**
     * Setter de l'attribut lienDebitTabac.
     * 
     * @param lienDebitTabac
     *            la nouvelle valeur de lienDebitTabac
     */
    public void setLienDebitTabac(String lienDebitTabac) {
        this.lienDebitTabac = lienDebitTabac;
    }

    /**
     * Setter de l'attribut lienDiagnostiqueurImmobilierLE.
     * 
     * @param lienDiagnostiqueurImmobilierLE
     *            la nouvelle valeur de lienDiagnostiqueurImmobilierLE
     */
    public void setLienDiagnostiqueurImmobilierLE(String lienDiagnostiqueurImmobilierLE) {
        this.lienDiagnostiqueurImmobilierLE = lienDiagnostiqueurImmobilierLE;
    }

    /**
     * Setter de l'attribut lienDiagnostiqueurImmobilierLPS.
     * 
     * @param lienDiagnostiqueurImmobilierLPS
     *            la nouvelle valeur de lienDiagnostiqueurImmobilierLPS
     */
    public void setLienDiagnostiqueurImmobilierLPS(String lienDiagnostiqueurImmobilierLPS) {
        this.lienDiagnostiqueurImmobilierLPS = lienDiagnostiqueurImmobilierLPS;
    }

    /**
     * Setter de l'attribut lienEditeur.
     * 
     * @param lienEditeur
     *            la nouvelle valeur de lienEditeur
     */
    public void setLienEditeur(String lienEditeur) {
        this.lienEditeur = lienEditeur;
    }

    /**
     * Setter de l'attribut lienExplorationPlateauContinentalLE.
     * 
     * @param lienExplorationPlateauContinentalLE
     *            la nouvelle valeur de lienExplorationPlateauContinentalLE
     */
    public void setLienExplorationPlateauContinentalLE(String lienExplorationPlateauContinentalLE) {
        this.lienExplorationPlateauContinentalLE = lienExplorationPlateauContinentalLE;
    }

    /**
     * Setter de l'attribut lienExplorationPlateauContinentalLPS.
     * 
     * @param lienExplorationPlateauContinentalLPS
     *            la nouvelle valeur de lienExplorationPlateauContinentalLPS
     */
    public void setLienExplorationPlateauContinentalLPS(String lienExplorationPlateauContinentalLPS) {
        this.lienExplorationPlateauContinentalLPS = lienExplorationPlateauContinentalLPS;
    }

    /**
     * Setter de l'attribut lienGenieClimatiqueLE.
     * 
     * @param lienGenieClimatiqueLE
     *            la nouvelle valeur de lienGenieClimatiqueLE
     */
    public void setLienGenieClimatiqueLE(String lienGenieClimatiqueLE) {
        this.lienGenieClimatiqueLE = lienGenieClimatiqueLE;
    }

    /**
     * Setter de l'attribut lienGenieClimatiqueLPS.
     * 
     * @param lienGenieClimatiqueLPS
     *            la nouvelle valeur de lienGenieClimatiqueLPS
     */
    public void setLienGenieClimatiqueLPS(String lienGenieClimatiqueLPS) {
        this.lienGenieClimatiqueLPS = lienGenieClimatiqueLPS;
    }

    /**
     * Setter de l'attribut lienGestionDechets.
     * 
     * @param lienGestionDechets
     *            la nouvelle valeur de lienGestionDechets
     */
    public void setLienGestionDechets(String lienGestionDechets) {
        this.lienGestionDechets = lienGestionDechets;
    }

    /**
     * Setter de l'attribut lienPublicationPeriodiques.
     * 
     * @param lienPublicationPeriodiques
     *            la nouvelle valeur de lienPublicationPeriodiques
     */
    public void setLienPublicationPeriodiques(String lienPublicationPeriodiques) {
        this.lienPublicationPeriodiques = lienPublicationPeriodiques;
    }

    /**
     * Setter de l'attribut lienTerrassement.
     * 
     * @param lienTerrassement
     *            la nouvelle valeur de lienTerrassement
     */
    public void setLienTerrassement(String lienTerrassement) {
        this.lienTerrassement = lienTerrassement;
    }

    /**
     * Setter de l'attribut lienTraitementDechetsHuile.
     * 
     * @param lienTraitementDechetsHuile
     *            la nouvelle valeur de lienTraitementDechetsHuile
     */
    public void setLienTraitementDechetsHuile(String lienTraitementDechetsHuile) {
        this.lienTraitementDechetsHuile = lienTraitementDechetsHuile;
    }

    /**
     * Setter de l'attribut lienTraitementDechetsPneus.
     * 
     * @param lienTraitementDechetsPneus
     *            la nouvelle valeur de lienTraitementDechetsPneus
     */
    public void setLienTraitementDechetsPneus(String lienTraitementDechetsPneus) {
        this.lienTraitementDechetsPneus = lienTraitementDechetsPneus;
    }

    /**
     * Set le mail.
     * 
     * @param mail
     *            the mail to set
     */
    public void setMail(String mail) {
        this.mail = mail;
    }

    /**
     * Set le nom.
     * 
     * @param nom
     *            the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Set le observation.
     * 
     * @param observation
     *            the observation to set
     */
    public void setObservation(String observation) {
        this.observation = observation;
    }

    /**
     * setPriseConnaissance.
     * 
     * @param priseConnaissance
     *            priseConnaissance
     */
    public void setPriseConnaissance(String priseConnaissance) {
        this.priseConnaissance = priseConnaissance;
    }

    /**
     * Set le site.
     * 
     * @param site
     *            the site to set
     */
    public void setSite(String site) {
        this.site = site;
    }

    /**
     * Set le telephone.
     * 
     * @param telephone
     *            the telephone to set
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    // Profil V2 demo : NE PAS SUPPR
    /**
     * Getter de l'activitePrincipaleRecap.
     * 
     * @return the activitePrincipaleRecap
     */
    public String getActivitePrincipaleRecap() {
        return this.activitePrincipaleRecap;
    }

    /**
     * Setter de l'activitePrincipaleRecap.
     * 
     * @param activitePrincipaleRecap
     *            the activitePrincipaleRecap to set
     */
    public void setActivitePrincipaleRecap(String activitePrincipaleRecap) {
        this.activitePrincipaleRecap = activitePrincipaleRecap;
    }

    /**
     * Getter de l'activiteSecondaireRecap.
     * 
     * @return the activiteSecondaireRecap
     */
    public String getActiviteSecondaireRecap() {
        return this.activiteSecondaireRecap;
    }

    /**
     * Setter de l'activiteSecondaireRecap.
     * 
     * @param activiteSecondaireRecap
     *            the activiteSecondaireRecap to set
     */
    public void setActiviteSecondaireRecap(String activiteSecondaireRecap) {
        this.activiteSecondaireRecap = activiteSecondaireRecap;
    }
    // FIN Profil V2 demo : NE PAS SUPPR

}
