package fr.ge.common.regent.bean.xml.formalite.dialogue.modification;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

import fr.ge.common.regent.bean.xml.formalite.IFormalite;

/**
 * La classe XML Field EntrepriseBean régularisation.
 * 
 * @author roveda
 * 
 */
@ResourceXPath("/formalite")
public interface IEntrepriseModification extends IFormalite {

    /** La constante MODEL_VERSION. */
    static final int MODEL_VERSION = 1;

    /**
     * Get le siren.
     *
     * @return le siren
     */
    @FieldXPath(value = "siren")
    String getSiren();

    /**
     * Get le exercice double activite.
     *
     * @return le exercice double activite
     */
    @FieldXPath(value = "exerciceDoubleActivite")
    String getExerciceDoubleActivite();

    /**
     * Get le greffe immat.
     *
     * @return le greffe immat
     */
    @FieldXPath(value = "greffeImmat")
    String getGreffeImmat();

    /**
     * Get le cma immat.
     *
     * @return le cma immat
     */
    @FieldXPath(value = "cmaImmat")
    String getCmaImmat();

    /**
     * Get le greffe secondaire oui non.
     *
     * @return le greffe secondaire oui non
     */
    @FieldXPath(value = "greffeSecondaireOuiNon")
    String getGreffeSecondaireOuiNon();

    /**
     * Get le greffe secondaire nombre.
     *
     * @return le greffe secondaire nombre
     */
    @FieldXPath(value = "greffeSecondaireNombre")
    Integer getGreffeSecondaireNombre();

    /**
     * Get le greffe secondaire libelle.
     *
     * @return le greffe secondaire libelle
     */
    @FieldXPath(value = "greffeSecondaireLibelle")
    String getGreffeSecondaireLibelle();

    /**
     * Get le adresse entreprise pp situation.
     *
     * @return le adresse entreprise pp situation
     */
    @FieldXPath(value = "adresseEntreprisePPSituation")
    String getAdresseEntreprisePPSituation();

    /**
     * Get le nombre dirigeants.
     *
     * @return le nombre dirigeants
     */
    @FieldXPath(value = "nombreDirigeants")
    String getNombreDirigeants();

    /**
     * Set le nombre dirigeants.
     *
     * @param nombreDirigeants
     *            le nouveau nombre dirigeants
     */
    void setNombreDirigeants(String nombreDirigeants);

    /**
     * Get le entreprise liee domiciliation.
     *
     * @return le entreprise liee domiciliation
     */
    @FieldXPath(value = "entrepriseLieeDomiciliation")
    IEntrepriseLieeModification getEntrepriseLieeDomiciliation();

    /**
     * Set le entreprise liee domiciliation.
     *
     * @param entrepriseLieeDomiciliation
     *            le nouveau entreprise liee domiciliation
     */
    void setEntrepriseLieeDomiciliation(IEntrepriseLieeModification entrepriseLieeDomiciliation);

    /**
     * New entreprise liee domiciliation.
     *
     * @return le i entreprise liee modification
     */
    IEntrepriseLieeModification newEntrepriseLieeDomiciliation();

    /**
     * Get le entreprise liee loueur mandant.
     *
     * @return le entreprise liee loueur mandant
     */
    @FieldXPath(value = "entrepriseLieeLoueurMandant")
    IEntrepriseLieeModification getEntrepriseLieeLoueurMandant();

    /**
     * Set le entreprise liee loueur mandant.
     *
     * @param entrepriseLieeLoueurMandant
     *            le nouveau entreprise liee loueur mandant
     */
    void setEntrepriseLieeLoueurMandant(IEntrepriseLieeModification entrepriseLieeLoueurMandant);

    /**
     * New entreprise liee loueur mandant.
     *
     * @return le i entreprise liee modification
     */
    IEntrepriseLieeModification newEntrepriseLieeLoueurMandant();

    /**
     * Get le entreprises liees precedents exploitants.
     *
     * @return le entreprises liees precedents exploitants
     */
    @FieldXPath(value = "entreprisesLieesPrecedentsExploitants")
    IEntrepriseLieeModification[] getEntreprisesLieesPrecedentsExploitants();

    /**
     * Set le entreprises liees precedents exploitants.
     *
     * @param entrepriseLieesPrecedentsExploitants
     *            le nouveau entreprises liees precedents exploitants
     */
    void setEntreprisesLieesPrecedentsExploitants(IEntrepriseLieeModification[] entrepriseLieesPrecedentsExploitants);

    /**
     * Ajoute le to entreprises liees precedents exploitants.
     *
     * @return le i entreprise liee modification
     */
    IEntrepriseLieeModification addToEntreprisesLieesPrecedentsExploitants();

    /**
     * Retire le from entreprises liees precedents exploitants.
     *
     * @param entrepriseLieesPrecedentsExploitants
     *            le entreprise liees precedents exploitants
     */
    void removeFromEntreprisesLieesPrecedentsExploitants(IEntrepriseLieeModification entrepriseLieesPrecedentsExploitants);

    /**
     * Get le etablissement.
     *
     * @return le etablissement
     */
    @FieldXPath(value = "etablissement")
    IEtablissementModification getEtablissement();

    /**
     * Set le etablissement.
     *
     * @param etablissement
     *            le nouveau etablissement
     */
    void setEtablissement(IEtablissementModification etablissement);

    /**
     * New etablissement.
     *
     * @return le i etablissement modification
     */
    IEtablissementModification newEtablissement();

    /**
     * Get le dirigeants.
     *
     * @return le dirigeants
     */
    @FieldXPath(value = "dirigeants")
    IDirigeantModification[] getDirigeants();

    /**
     * Set le dirigeants.
     *
     * @param dirigeants
     *            le nouveau dirigeants
     */
    void setDirigeants(IDirigeantModification[] dirigeants);

    /**
     * Retire le from dirigeants.
     *
     * @param dirigeants
     *            le dirigeants
     */
    void removeFromDirigeants(IDirigeantModification dirigeants);

    /**
     * Ajoute le to dirigeants.
     *
     * @return le i dirigeant modification
     */
    IDirigeantModification addToDirigeants();

    /**
     * Get le eirl.
     *
     * @return le eirl
     */
    @FieldXPath(value = "eirl")
    IEirlModification getEirl();

    /**
     * Set le eirl.
     *
     * @param eirl
     *            le nouveau eirl
     */
    void setEirl(IEirlModification eirl);

    /**
     * New eirl.
     *
     * @return le i eirl modification
     */
    IEirlModification newEirl();

    /**
     * Set le siren.
     *
     * @param siren
     *            le nouveau siren
     */
    void setSiren(String siren);

    /**
     * Set le exercice double activite.
     *
     * @param exerciceDoubleActivite
     *            le nouveau exercice double activite
     */
    void setExerciceDoubleActivite(String exerciceDoubleActivite);

    /**
     * Set le greffe immat.
     *
     * @param greffeImmat
     *            le nouveau greffe immat
     */
    void setGreffeImmat(String greffeImmat);

    /**
     * Set le cma immat.
     *
     * @param cmaImmat
     *            le nouveau cma immat
     */
    void setCmaImmat(String cmaImmat);

    /**
     * Set le greffe secondaire oui non.
     *
     * @param greffeSecondaireOuiNon
     *            le nouveau greffe secondaire oui non
     */
    void setGreffeSecondaireOuiNon(String greffeSecondaireOuiNon);

    /**
     * Set le greffe secondaire nombre.
     *
     * @param greffeSecondaireNombre
     *            le nouveau greffe secondaire nombre
     */
    void setGreffeSecondaireNombre(Integer greffeSecondaireNombre);

    /**
     * Set le greffe secondaire libelle.
     *
     * @param greffeSecondaireLibelle
     *            le nouveau greffe secondaire libelle
     */
    void setGreffeSecondaireLibelle(String greffeSecondaireLibelle);

    /**
     * Set le adresse entreprise pp situation.
     *
     * @param adresseEntreprisePPSituation
     *            le nouveau adresse entreprise pp situation
     */
    void setAdresseEntreprisePPSituation(String adresseEntreprisePPSituation);

    /**
     * Get le insaisissabilite Renonciation RP.
     *
     * @return le insaisissabilite Renonciation RP
     */
    @FieldXPath(value = "insaisissabiliteRenonciationRP")
    String getInsaisissabiliteRenonciationRP();

    /**
     * Set le insaisissabilite Renonciation RP.
     *
     * @param insaisissabiliteRenonciationRP
     *            le insaisissabilite Renonciation RP
     */
    void setInsaisissabiliteRenonciationRP(String insaisissabiliteRenonciationRP);

    /**
     * Get le insaisissabilite publication Renonciation RP.
     *
     * @return le insaisissabilite publication Renonciation RP
     */
    @FieldXPath(value = "insaisissabilitePublicationRenonciationRP")
    String getInsaisissabilitePublicationRenonciationRP();

    /**
     * Set le insaisissabilite publication Renonciation RP.
     *
     * @param insaisissabilitePublicationRenonciationRP
     *            le insaisissabilite publication Renonciation RP
     */
    void setInsaisissabilitePublicationRenonciationRP(String insaisissabilitePublicationRenonciationRP);

    /**
     * Get le insaisissabilite revocation RP.
     *
     * @return le insaisissabilite revocation RP
     */
    @FieldXPath(value = "insaisissabiliteRevocationRP")
    String getInsaisissabiliteRevocationRP();

    /**
     * Set le insaisissabilite revocation RP.
     *
     * @param insaisissabiliteRevocationRP
     *            le insaisissabilite revocation RP
     */
    void setInsaisissabiliteRevocationRP(String insaisissabiliteRevocationRP);

    /**
     * Get le insaisissabilite publication revocation RP.
     *
     * @return le insaisissabilite publication revocation RP
     */
    @FieldXPath(value = "insaisissabilitePublicationRevocationRP")
    String getInsaisissabilitePublicationRevocationRP();

    /**
     * Set le insaisissabilite publication revocation RP.
     *
     * @param insaisissabilitePublicationRevocationRP
     *            le insaisissabilite publication revocation RP
     */
    void setInsaisissabilitePublicationRevocationRP(String insaisissabilitePublicationRevocationRP);

    /**
     * Get le insaisissabilite Declaration Autres Biens.
     *
     * @return le insaisissabilit eDeclaration Autres Biens
     */
    @FieldXPath(value = "insaisissabiliteDeclarationAutresBiens")
    String getInsaisissabiliteDeclarationAutresBiens();

    /**
     * Set le insaisissabilite Declaration Autres Biens.
     *
     * @param insaisissabiliteDeclarationAutresBiens
     *            le insaisissabilite Declaration AutresBiens
     */
    void setInsaisissabiliteDeclarationAutresBiens(String insaisissabiliteDeclarationAutresBiens);

    /**
     * Get le insaisissabilite Publication Declaration Autres Biens.
     *
     * @return le insaisissabilite Publication Declaration Autres Biens
     */
    @FieldXPath(value = "insaisissabilitePublicationDeclarationAutresBiens")
    String getInsaisissabilitePublicationDeclarationAutresBiens();

    /**
     * Set le insaisissabilite Publication Declaration Autres Biens.
     *
     * @param insaisissabilitePublicationDeclarationAutresBiens
     *            le insaisissabilite Publication Declaration AutresBiens
     */
    void setInsaisissabilitePublicationDeclarationAutresBiens(String insaisissabilitePublicationDeclarationAutresBiens);

    /**
     * Get le insaisissabilite Renonciation Declaration Autres Biens.
     *
     * @return le insaisissabilite Renonciation Declaration Autres Biens
     */
    @FieldXPath(value = "insaisissabiliteRenonciationDeclarationAutresBiens")
    String getInsaisissabiliteRenonciationDeclarationAutresBiens();

    /**
     * Set le insaisissabilite Renonciation Declaration Autres Biens.
     *
     * @param insaisissabiliteRenonciationDeclarationAutresBiens
     *            le insaisissabilite Renonciation Declaration Autres Biens
     */
    void setInsaisissabiliteRenonciationDeclarationAutresBiens(String insaisissabiliteRenonciationDeclarationAutresBiens);

    /**
     * Get le insaisissabilite Publication Renonciation Declaration AutresBiens.
     *
     * @return le insaisissabilite Publication Renonciation Declaration
     *         AutresBiens
     */
    @FieldXPath(value = "insaisissabilitePublicationRenonciationDeclarationAutresBiens")
    String getInsaisissabilitePublicationRenonciationDeclarationAutresBiens();

    /**
     * Set le insaisissabilite Publication Renonciation Declaration AutresBiens.
     *
     * @param insaisissabilitePublicationRenonciationDeclarationAutresBiens
     *            le insaisissabilite Publication Renonciation Declaration
     *            AutresBiens
     */
    void setInsaisissabilitePublicationRenonciationDeclarationAutresBiens(String insaisissabilitePublicationRenonciationDeclarationAutresBiens);

    /**
     * Get le lieu Depot Impot.
     *
     * @return le lieuDepotImpot
     */
    @FieldXPath(value = "lieuDepotImpot")
    String getLieuDepotImpot();

    /**
     * Set le lieu Depot Impot.
     *
     * @param lieuDepotImpot
     *            le lieu Depot Impot
     */
    void setLieuDepotImpot(String lieuDepotImpot);
}
