package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;
import org.xmlfield.core.types.XmlString;

/**
 * Le Interface AQU.
 */
@ResourceXPath("/RFU")
public interface RFU {

  /**
   * Get le U30.
   *
   * @return le U30
   */
  @FieldXPath("U30")
  String getU30();

  /**
   * Set le U30.
   *
   * @param U30
   *          le nouveau U30
   */
  void setU30(String U30);
  

  /**
   * Get le U31.
   *
   * @return le U31
   */
  @FieldXPath("U31")
  XmlString[] getU31();

  /**
   * Ajoute le to U31.
   *
   * @return le xml string
   */
  XmlString addToU31();

  /**
   * Set le U31.
   *
   * @param U31
   *          le nouveau U31
   */
  void setU31(XmlString[] U31);

  

  /**
   * Get le U32.
   *
   * @return le U32
   */
  @FieldXPath("U32")
  XmlString[] getU32();

  /**
   * Ajoute le to U32.
   *
   * @return le xml string
   */
  XmlString addToU32();

  /**
   * Set le U31.
   *
   * @param U31
   *          le nouveau U31
   */
  void setU32(XmlString[] U32);

  /**
   * Get le U33.
   *
   * @return le U33
   */
  @FieldXPath("U33")
  IU33[] getU33();

  /**
   * Ajoute le to U33.
   *
   * @return le U33
   */
  IU33 addToU33();

  /**
   * Set le U33.
   *
   * @param U33
   *          le nouveau U33
   */
  void setU33(IU33[] U33);

  /**
   * Get le U37.
   *
   * @return le U37
   */
  @FieldXPath("U37")
  String getU37();

  /**
   * Set le U37.
   *
   * @param U37
   *          le nouveau U37
   */
  void setU37(String U37);

}
