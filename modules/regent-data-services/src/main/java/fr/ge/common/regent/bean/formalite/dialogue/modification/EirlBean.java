package fr.ge.common.regent.bean.formalite.dialogue.modification;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import fr.ge.common.regent.bean.formalite.dialogue.AbstractEirlBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.AdresseBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.IFormaliteVue;

/**
 * Le Class EirlBean.
 */
public class EirlBean extends AbstractEirlBean implements IFormaliteVue {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 8165788173506711655L;

    /** Le activites accessoires bicbnc. */
    private String activitesAccessoiresBICBNC;

    /** Le date cloture exercice comptable. */
    private String dateClotureExerciceComptable;

    /** Le denomination. */
    private String denomination;

    /** Le objet. */
    private String objet;

    /** Le objet portant sur ensemble activites. */
    private String objetPortantSurEnsembleActivites;

    /** Le precedent eirl denomination. */
    private String precedentEIRLDenomination;

    /** Le precedent eirl lieu immatriculation. */
    private String precedentEIRLLieuImmatriculation;

    /** Le precedent eirl registre. */
    private String precedentEIRLRegistre;

    /** Le precedent eirlsiren. */
    private String precedentEIRLSIREN;

    /** Le registre depot. */
    private String registreDepot;

    /** Le registre depot2. */
    private String registreDepot2;

    /** Le statut eirl. */
    private String statutEIRL;

    /** Le modif statut eir l1. */
    private String modifStatutEIRL1;

    /** Le modif date eirl. */
    private String modifDateEIRL;

    /** Le modif adresse e irl. */
    private AdresseBean modifAdresseEIrl = new AdresseBean();

    /** Le lieu registre depot. */
    private String lieuRegistreDepot;

    /** Le destination patrimoine affecte. */
    private String destinationPatrimoineAffecte;

    /** Le versement liberatoire oui non. */
    private String versementLiberatoireOuiNon;

    /** Le definition patrimoine. */
    private List<String> definitionPatrimoine = new ArrayList<String>();

    /**
     * Getter de l'attribut activitesAccessoiresBICBNC.
     * 
     * @return la valeur de activitesAccessoiresBICBNC
     */
    public String getActivitesAccessoiresBICBNC() {
        return this.activitesAccessoiresBICBNC;
    }

    /**
     * Getter de l'attribut dateClotureExerciceComptable.
     * 
     * @return la valeur de dateClotureExerciceComptable
     */
    public String getDateClotureExerciceComptable() {
        return this.dateClotureExerciceComptable;
    }

    /**
     * Getter de l'attribut denomination.
     * 
     * @return la valeur de denomination
     */
    public String getDenomination() {
        return this.denomination;
    }

    /**
     * Getter de l'attribut objet.
     * 
     * @return la valeur de objet
     */
    public String getObjet() {
        return this.objet;
    }

    /**
     * Getter de l'attribut objetPortantSurEnsembleActivites.
     * 
     * @return la valeur de objetPortantSurEnsembleActivites
     */
    public String getObjetPortantSurEnsembleActivites() {
        return this.objetPortantSurEnsembleActivites;
    }

    /**
     * Getter de l'attribut precedentEIRLDenomination.
     * 
     * @return la valeur de precedentEIRLDenomination
     */
    public String getPrecedentEIRLDenomination() {
        return this.precedentEIRLDenomination;
    }

    /**
     * Getter de l'attribut precedentEIRLLieuImmatriculation.
     * 
     * @return la valeur de precedentEIRLLieuImmatriculation
     */
    public String getPrecedentEIRLLieuImmatriculation() {
        return this.precedentEIRLLieuImmatriculation;
    }

    /**
     * Getter de l'attribut precedentEIRLRegistre.
     * 
     * @return la valeur de precedentEIRLRegistre
     */
    public String getPrecedentEIRLRegistre() {
        return this.precedentEIRLRegistre;
    }

    /**
     * Getter de l'attribut precedentEIRLSIREN.
     * 
     * @return la valeur de precedentEIRLSIREN
     */
    public String getPrecedentEIRLSIREN() {
        return this.precedentEIRLSIREN;
    }

    /**
     * /** Getter de l'attribut registreDepot.
     * 
     * @return la valeur de registreDepot
     */
    public String getRegistreDepot() {
        return this.registreDepot;
    }

    /**
     * Getter de l'attribut statutEIRL.
     * 
     * @return la valeur de statutEIRL
     */
    public String getStatutEIRL() {
        return this.statutEIRL;
    }

    /**
     * Setter de l'attribut activitesAccessoiresBICBNC.
     * 
     * @param activitesAccessoiresBICBNC
     *            la nouvelle valeur de activitesAccessoiresBICBNC
     */
    public void setActivitesAccessoiresBICBNC(String activitesAccessoiresBICBNC) {
        this.activitesAccessoiresBICBNC = activitesAccessoiresBICBNC;
    }

    /**
     * Setter de l'attribut dateClotureExerciceComptable.
     * 
     * @param dateClotureExerciceComptable
     *            la nouvelle valeur de dateClotureExerciceComptable
     */
    public void setDateClotureExerciceComptable(String dateClotureExerciceComptable) {
        this.dateClotureExerciceComptable = dateClotureExerciceComptable;
    }

    /**
     * Setter de l'attribut denomination.
     * 
     * @param denomination
     *            la nouvelle valeur de denomination
     */
    public void setDenomination(String denomination) {
        this.denomination = denomination;
    }

    /**
     * Setter de l'attribut objet.
     * 
     * @param objet
     *            la nouvelle valeur de objet
     */
    public void setObjet(String objet) {
        this.objet = objet;
    }

    /**
     * Setter de l'attribut objetPortantSurEnsembleActivites.
     * 
     * @param objetPortantSurEnsembleActivites
     *            la nouvelle valeur de objetPortantSurEnsembleActivites
     */
    public void setObjetPortantSurEnsembleActivites(String objetPortantSurEnsembleActivites) {
        this.objetPortantSurEnsembleActivites = objetPortantSurEnsembleActivites;
    }

    /**
     * Setter de l'attribut precedentEIRLDenomination.
     * 
     * @param precedentEIRLDenomination
     *            la nouvelle valeur de precedentEIRLDenomination
     */
    public void setPrecedentEIRLDenomination(String precedentEIRLDenomination) {
        this.precedentEIRLDenomination = precedentEIRLDenomination;
    }

    /**
     * Setter de l'attribut precedentEIRLLieuImmatriculation.
     * 
     * @param precedentEIRLLieuImmatriculation
     *            la nouvelle valeur de precedentEIRLLieuImmatriculation
     */
    public void setPrecedentEIRLLieuImmatriculation(String precedentEIRLLieuImmatriculation) {
        this.precedentEIRLLieuImmatriculation = precedentEIRLLieuImmatriculation;
    }

    /**
     * Setter de l'attribut precedentEIRLRegistre.
     * 
     * @param precedentEIRLRegistre
     *            la nouvelle valeur de precedentEIRLRegistre
     */
    public void setPrecedentEIRLRegistre(String precedentEIRLRegistre) {
        this.precedentEIRLRegistre = precedentEIRLRegistre;
    }

    /**
     * Setter de l'attribut precedentEIRLSIREN.
     * 
     * @param precedentEIRLSIREN
     *            la nouvelle valeur de precedentEIRLSIREN
     */
    public void setPrecedentEIRLSIREN(String precedentEIRLSIREN) {
        this.precedentEIRLSIREN = precedentEIRLSIREN;
    }

    /**
     * Setter de l'attribut registreDepot.
     * 
     * @param registreDepot
     *            la nouvelle valeur de registreDepot
     */
    public void setRegistreDepot(String registreDepot) {
        this.registreDepot = registreDepot;
    }

    /**
     * Setter de l'attribut statutEIRL.
     * 
     * @param statutEIRL
     *            la nouvelle valeur de statutEIRL
     */
    public void setStatutEIRL(String statutEIRL) {
        this.statutEIRL = statutEIRL;
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    /**
     * Get le registre depot2.
     *
     * @return the registreDepot2
     */
    public String getRegistreDepot2() {
        return registreDepot2;
    }

    /**
     * Set le registre depot2.
     *
     * @param registreDepot2
     *            the registreDepot2 to set
     */
    public void setRegistreDepot2(String registreDepot2) {
        this.registreDepot2 = registreDepot2;
    }

    /**
     * Get le modif statut eir l1.
     *
     * @return the modifStatutEIRL1
     */
    public String getModifStatutEIRL1() {
        return modifStatutEIRL1;
    }

    /**
     * Set le modif statut eir l1.
     *
     * @param modifStatutEIRL1
     *            the modifStatutEIRL1 to set
     */
    public void setModifStatutEIRL1(String modifStatutEIRL1) {
        this.modifStatutEIRL1 = modifStatutEIRL1;
    }

    /**
     * Get le modif date eirl.
     *
     * @return the modifDateEIRL
     */
    public String getModifDateEIRL() {
        return modifDateEIRL;
    }

    /**
     * Set le modif date eirl.
     *
     * @param modifDateEIRL
     *            the modifDateEIRL to set
     */
    public void setModifDateEIRL(String modifDateEIRL) {
        this.modifDateEIRL = modifDateEIRL;
    }

    /**
     * Get le modif adresse e irl.
     *
     * @return the modifAdresseEIrl
     */
    public AdresseBean getModifAdresseEIrl() {
        return modifAdresseEIrl;
    }

    /**
     * Set le modif adresse e irl.
     *
     * @param modifAdresseEIrl
     *            the modifAdresseEIrl to set
     */
    public void setModifAdresseEIrl(AdresseBean modifAdresseEIrl) {
        this.modifAdresseEIrl = modifAdresseEIrl;
    }

    /**
     * Get le lieu registre depot.
     *
     * @return the lieuRegistreDepot
     */
    public String getLieuRegistreDepot() {
        return lieuRegistreDepot;
    }

    /**
     * Set le lieu registre depot.
     *
     * @param lieuRegistreDepot
     *            the lieuRegistreDepot to set
     */
    public void setLieuRegistreDepot(String lieuRegistreDepot) {
        this.lieuRegistreDepot = lieuRegistreDepot;
    }

    /**
     * Get le destination patrimoine affecte.
     *
     * @return the destinationPatrimoineAffecte
     */
    public String getDestinationPatrimoineAffecte() {
        return destinationPatrimoineAffecte;
    }

    /**
     * Set le destination patrimoine affecte.
     *
     * @param destinationPatrimoineAffecte
     *            the destinationPatrimoineAffecte to set
     */
    public void setDestinationPatrimoineAffecte(String destinationPatrimoineAffecte) {
        this.destinationPatrimoineAffecte = destinationPatrimoineAffecte;
    }

    /**
     * Get le versement liberatoire oui non.
     *
     * @return the versementLiberatoireOuiNon
     */
    public String getVersementLiberatoireOuiNon() {
        return versementLiberatoireOuiNon;
    }

    /**
     * Set le versement liberatoire oui non.
     *
     * @param versementLiberatoireOuiNon
     *            the versementLiberatoireOuiNon to set
     */
    public void setVersementLiberatoireOuiNon(String versementLiberatoireOuiNon) {
        this.versementLiberatoireOuiNon = versementLiberatoireOuiNon;
    }

    /**
     * Get le definition patrimoine.
     *
     * @return the definitionPatrimoine
     */
    public List<String> getDefinitionPatrimoine() {
        return definitionPatrimoine;
    }

    /**
     * Set le definition patrimoine.
     *
     * @param definitionPatrimoine
     *            the definitionPatrimoine to set
     */
    public void setDefinitionPatrimoine(List<String> definitionPatrimoine) {
        this.definitionPatrimoine = definitionPatrimoine;
    }

}
