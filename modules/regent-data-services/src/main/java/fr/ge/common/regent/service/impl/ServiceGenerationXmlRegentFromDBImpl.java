package fr.ge.common.regent.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.formiz.context.ElContextBuilder;
import org.json.JSONObject;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.xmlfield.core.XmlField;
import org.xmlfield.core.XmlFieldFactory;
import org.xmlfield.core.exception.XmlFieldParsingException;
import org.xmlfield.core.types.XmlString;

import com.google.common.collect.Multimap;

import fr.ge.common.regent.bean.IRoot;
import fr.ge.common.regent.bean.RootObject;
import fr.ge.common.regent.bean.formalite.dialogue.cessation.DialogueCessationBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.DialogueCreationBean;
import fr.ge.common.regent.bean.formalite.dialogue.modification.DialogueModificationBean;
import fr.ge.common.regent.bean.formalite.dialogue.regularisation.DialogueRegularisationBean;
import fr.ge.common.regent.bean.formalite.profil.cessation.ProfilCessationBean;
import fr.ge.common.regent.bean.formalite.profil.creation.ProfilCreationBean;
import fr.ge.common.regent.bean.formalite.profil.modification.ProfilModificationBean;
import fr.ge.common.regent.bean.formalite.profil.regularisation.ProfilRegularisationBean;
import fr.ge.common.regent.bean.xml.formalite.dialogue.cessation.IDialogueCessation;
import fr.ge.common.regent.bean.xml.formalite.dialogue.creation.IDialogueCreation;
import fr.ge.common.regent.bean.xml.formalite.dialogue.modification.IDialogueModification;
import fr.ge.common.regent.bean.xml.formalite.dialogue.regularisation.IDialogueRegularisation;
import fr.ge.common.regent.bean.xml.formalite.profil.cessation.IProfilCessation;
import fr.ge.common.regent.bean.xml.formalite.profil.creation.IProfilCreation;
import fr.ge.common.regent.bean.xml.formalite.profil.modification.IProfilModification;
import fr.ge.common.regent.bean.xml.formalite.profil.regularisation.IProfilRegularisation;
import fr.ge.common.regent.constante.TypeDossierEnum;
import fr.ge.common.regent.constante.VersionXmlRegentEnum;
import fr.ge.common.regent.context.function.FonctionRegent;
import fr.ge.common.regent.service.ServiceGeneationRegentFromDB;
import fr.ge.common.regent.service.exception.XmlFatalException;
import fr.ge.common.regent.service.exception.XmlInvalidException;
import fr.ge.common.regent.service.referentiel.ReferentielService;
import fr.ge.common.regent.services.declenchement.IDeclenchementRubriqueXmlRegentService;
import fr.ge.common.regent.util.mapper.formalite.dialogue.MapperDialogueCessationUtils;
import fr.ge.common.regent.util.mapper.formalite.dialogue.MapperDialogueCreationUtils;
import fr.ge.common.regent.util.mapper.formalite.dialogue.MapperDialogueModificationUtils;
import fr.ge.common.regent.util.mapper.formalite.dialogue.MapperDialogueRegularisationUtils;
import fr.ge.common.regent.util.mapper.formalite.profil.MapperProfilCessationUtils;
import fr.ge.common.regent.util.mapper.formalite.profil.MapperProfilCreationUtils;
import fr.ge.common.regent.util.mapper.formalite.profil.MapperProfilModificationUtils;
import fr.ge.common.regent.util.mapper.formalite.profil.MapperProfilRegularisationUtils;
import fr.ge.common.regent.ws.v1.bean.RegentResultBean;
import fr.ge.common.regent.xml.XmlFieldElement;
import fr.ge.common.regent.xml.impl.XmlWriter;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * Class ServiceGenerationXmlRegentFromDBImpl.
 */
public class ServiceGenerationXmlRegentFromDBImpl implements ServiceGeneationRegentFromDB {

    /** Le logger fonctionnel. */
    private static final Logger LOGGER_FONC = LoggerFactory.getLogger(ServiceGenerationXmlRegentFromDBImpl.class);

    /** La constante EXTENSION_XML. */
    private static final String EXTENSION_XML = ".xml";

    /** La constante GROUPID. */
    private static final String GROUPID = "regent";

    /** La constante TYPEFLUX. */
    private static final String TYPEFLUX = "typeFlux";

    /** La constante FLUX_LIST_EVENEMENTS. */
    private static final String FLUX_LIST_EVENEMENTS = "ListeEvenementsFlux";

    /** La constante LIST_EVENEMENTS. */
    private static final String LIST_EVENEMENTS = "listevenements";

    /** La constante NUMERO_LIASSE. */
    private static final String NUMERO_LIASSE = "numeroLiasse";

    /** La constante CODE_EDI. */
    private static final String CODE_EDI = "codeEdi";

    /** La constante OCCURENCE_CROURANT_RUBRIQUECOMPLEXE. */
    private static final String OCCURENCE_COURANTE_RUBRIQUECOMPLEXE = "occurenceCourant";

    /** La constante VERSION_XML. */
    private static final String VERSION_XML = "version";

    /** La constante VERSION_MESSAGE_REGENT. */
    private static final String VERSION_MESSAGE_REGENT = "Message_Version_Regent";

    /** La constante NAME_SPACE_XML. */
    private static final String NAME_SPACE_XML_TEMPLATE = "<REGENT-XML xmlns:xsi=" + '"' + "http://www.w3.org/2001/XMLSchema-instance" + '"' + " " + "xsi:noNamespaceSchemaLocation=" + '"'
            + VERSION_MESSAGE_REGENT + '"' + ">";

    /** La constante ENCODING. */
    private static final String ENCODING = "<?xml" + " " + "version=" + '"' + 1.0 + '"' + " " + "encoding=" + '"' + "UTF-8" + '"' + " " + "standalone=" + '"' + "no" + '"' + "?>";

    /** separateur evenement. */
    private final String SEPARATEUR_EVENEMENT;

    /** el context builder. */
    @Autowired
    private ElContextBuilder elContextBuilder;

    /** xml field factory. */
    @Autowired
    private XmlFieldFactory xmlFieldFactory;

    /** ref service. */
    @Autowired
    private ReferentielService refService;

    /** declenchement rubrique xml regent. */
    @Autowired
    private IDeclenchementRubriqueXmlRegentService declenchementRubriqueXmlRegent;

    /** xml writer. */
    @Autowired
    private XmlWriter xmlWriter;

    /** Le profil entreprise xml to bean converteur. */
    @Autowired
    private MapperProfilCreationUtils profilEntrepriseXmlToBeanConverteur;

    /** Mapper Profil Cessation. **/
    @Autowired
    private MapperProfilCessationUtils mapperProfilCessationUtils;

    /** Mapper Profil Regularisation. **/
    @Autowired
    private MapperProfilRegularisationUtils mapperProfilRegularisationUtils;

    /** Mapper Profil Modification. **/
    @Autowired
    private MapperProfilModificationUtils mapperProfilModificationUtils;

    /** mapper formalite creation utils. */
    @Autowired
    private MapperDialogueCreationUtils mapperFormaliteCreationUtils;

    /** mapper formalite cessation utils. */
    @Autowired
    private MapperDialogueCessationUtils mapperFormaliteCessationUtils;

    /** Le mapper formalite regularisation utils. */
    @Autowired
    private MapperDialogueRegularisationUtils mapperFormaliteRegularisationUtils;

    /** mapper formalite modification utils. */
    @Autowired
    private MapperDialogueModificationUtils mapperFormaliteModificationUtils;

    /** Le validateur regent map. */
    private final Map<String, Validateur> validateurRegentMap = new HashMap<String, Validateur>();

    /** La constante RES_DIR. */
    private static final String RES_DIR = ServiceGenerationXmlRegentFromDBImpl.class.getResource("/xsd").getFile();

    /**
     * Constructeur.
     */
    public ServiceGenerationXmlRegentFromDBImpl() {
        super();
        this.SEPARATEUR_EVENEMENT = "SEPARATOR_UNDEFINED";

    }

    /**
     * Instantie un nouveau service generation xml regent from db impl.
     *
     * @param separateurEvenemnt
     *            separateur evenemnt
     */
    public ServiceGenerationXmlRegentFromDBImpl(final String separateurEvenemnt) throws TechnicalException, FileNotFoundException, IOException {
        super();
        this.SEPARATEUR_EVENEMENT = separateurEvenemnt;
        this.initValidatorRegent();
    }

    /**
     * Initialise les validators regent.
     * 
     * @throws IOException
     * @throws FileNotFoundException
     *
     * @throws TechniqueException
     *             le erreur technique exception
     */
    private void initValidatorRegent() throws TechnicalException, FileNotFoundException, IOException {
        for (VersionXmlRegentEnum versionXmlRegent : VersionXmlRegentEnum.values()) {
            Validateur validatorXml = new Validateur(RES_DIR + File.separator + versionXmlRegent.getXsdMessageRegent(), RES_DIR + File.separator + versionXmlRegent.getXsdTypeRegent());
            this.validateurRegentMap.put(versionXmlRegent.getVersion(), validatorXml);
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @throws XmlInvalidException
     * @throws XmlFatalException
     */
    @Override
    public String getRegentXmlCap(final List<String> listCodeEdi, final Object profil, final Object formalite, final String typeFormalite, final String codeEdi, final String roleDestinataire,
            final List<String> evenements, final String version, final String numeroLiasse, final String typeFlux) throws TechnicalException, SecurityException, IllegalArgumentException,
            NoSuchMethodException, IllegalAccessException, InvocationTargetException, XmlFieldParsingException, XmlInvalidException, XmlFatalException {
        String xmlGenerer = null;

        // Prépartion du context
        StandardEvaluationContext context = new StandardEvaluationContext();
        context.setVariable("cfe", formalite);
        context.setVariable("profil", profil);
        context.setVariable("typeFormalite", typeFormalite);
        context.setVariable("destinataires", listCodeEdi);

        String codeDestinataireString = codeEdi.substring(0, 1);

        IRoot root = new RootObject();
        root.setBeanRacine(null);
        context.setRootObject(root);
        context.setVariable(TYPEFLUX, typeFlux);
        context.setVariable(FLUX_LIST_EVENEMENTS, evenements);
        context.setVariable(LIST_EVENEMENTS, evenements);
        context.setVariable(NUMERO_LIASSE, numeroLiasse);
        context.setVariable(CODE_EDI, codeEdi);
        context.setVariable(VERSION_XML, version);
        this.elContextBuilder.build(context);
        XmlField XF = this.xmlFieldFactory.getXmlField();
        try {

            // récupération de la version de l'xmlRegent qui sera générer par le
            // moteur
            Object xmlRegent = XF.newObject(FonctionRegent.MAP_VERSION_XMLREGENT.get(version));

            // Récupérer la list des rubriques nonOrdonne qui seront générées
            // par
            // le moteur
            List<String> rubriques = this.declenchementRubriqueXmlRegent.getListRubriqueForXmlRegent(version, evenements, roleDestinataire, codeDestinataireString);

            List<XmlFieldElement> elementsNonOrdonneByRubrique = new ArrayList<XmlFieldElement>();
            // la liste des XmlFiledElements de toutes les rubriques
            List<XmlFieldElement> elementsNonOrdonneGlobal = new ArrayList<XmlFieldElement>();
            for (String rubrique : rubriques) {
                elementsNonOrdonneByRubrique = this.xmlWriter.getXmlFieldElement(GROUPID, rubrique, version);
                elementsNonOrdonneGlobal.addAll(elementsNonOrdonneByRubrique);

            }
            // récupérer la liste de toutes les XmlFieldElement qui existe dans
            // la table regles_regent
            List<XmlFieldElement> elementsFromDBOrdonne = this.xmlWriter.getXmlFieldElement(GROUPID, version);
            // la liste des XMlFieldElements ordonné qui sera génerer par le
            // moteur
            List<XmlFieldElement> elements = this.xmlWriter.getListRubriqueOrdonne(elementsFromDBOrdonne, elementsNonOrdonneGlobal);

            // construire une Map<rubrique,List<XmlFieldElement>> qui va nous
            // permettre de gérer le bourrage sur les balises obligatoire
            Multimap<String, XmlFieldElement> multiMapRubriqueBalise = this.xmlWriter.constructMapRubriqueBalises(elements);

            // Construire une Map<rubrique,Bourage> qui permet de nous indiquer
            // les rubriques ou on doit mettre le bourrage pour les balises
            // obligatoire
            Map<String, Boolean> mettreBourageSurUneBalise = this.xmlWriter.bourageSurBaliseObligatoireVide(context, multiMapRubriqueBalise);
            // rubrique conditionnelle ou obligatoire

            Map<String, Boolean> rubriqueConditionnelle = this.refService.getRubriqueOblogatoireOuConditionnelle(version, evenements);

            List<String> rubriqueOrdonne = new ArrayList<String>();
            // Une rubrique est considérée complexe elle a plusieurs occurences
            Map<String, Boolean> mapRubriqueComplexite = new HashMap<String, Boolean>();

            // Une Sous rubrique qui sera pas traité par le moteur car cette
            // rubrique il été déja traité dans la rubrique complexe
            Map<String, Boolean> mapSousRubriqueNonTraite = new HashMap<String, Boolean>();

            // construction de la map des rubriques complexe ainsi que la liste
            // des rubriques ordonné

            for (XmlFieldElement xmlFielElement : elements) {
                if (!mapRubriqueComplexite.containsKey(xmlFielElement.getRubrique()) && rubriques.contains(xmlFielElement.getRubrique())) {
                    Integer nbOccurence = xmlFielElement.getOccurences();
                    mapRubriqueComplexite.put(xmlFielElement.getRubrique(), !xmlFielElement.getEndPath() && nbOccurence != null && nbOccurence > 0);
                    // mapSousRubriqueNonTraite.put(xmlFielElement.getRubrique(),
                    // !xmlFielElement.getEndPath()
                    // && nbOccurence == null);
                    rubriqueOrdonne.add(xmlFielElement.getRubrique());
                }

            }

            // la saisie du contenu de la balise de chaque rubrique sur le
            // fichier
            for (String rubrique : rubriqueOrdonne) {
                List<XmlFieldElement> elementsRubrique = this.xmlWriter.getXmlFieldElement(GROUPID, rubrique, version);

                // traitement des rubriques avec occurences
                if (mapRubriqueComplexite.get(rubrique)) {

                    this.traitementRubriqueComplexe(context, elementsRubrique, rubriqueConditionnelle, mapSousRubriqueNonTraite, mapRubriqueComplexite, rubriqueOrdonne, xmlRegent, StringUtils.EMPTY);

                } else {// traitement des rubriques sans occurences

                    boolean vrai = this.getRubriqueTraite(mapRubriqueComplexite, rubrique);
                    if (!mapSousRubriqueNonTraite.containsKey(rubrique) && !vrai) {
                        for (XmlFieldElement balise : elementsRubrique) {
                            Boolean bourrageBalise = mettreBourageSurUneBalise.get(balise.getRubrique());
                            Boolean rubriqueConditionnelleOuObligatoire = rubriqueConditionnelle.get(balise.getRubrique());
                            if (rubriqueConditionnelleOuObligatoire != null) {
                                LOGGER_FONC.debug("la rubrique qui sera traitée est :{}", rubrique);
                                this.saisieXml(context, xmlRegent, balise, null, bourrageBalise, rubriqueConditionnelleOuObligatoire);
                                mapSousRubriqueNonTraite.put(balise.getRubrique(), true);
                            }
                        }
                    }

                }
            }

            xmlGenerer = this.addShemaLocation(XF.objectToXml(xmlRegent), VersionXmlRegentEnum.fromVersion(version).getXsdMessageRegent());

        } catch (XmlFieldParsingException e) {
            LOGGER_FONC.error(e.getMessage(), e);
            throw e;
        }

        return xmlGenerer;
    }

    /**
     * Accesseur sur l'attribut {@link #rubrique traite}.
     *
     * @param mapRubriqueComplexe
     *            map rubrique complexe
     * @param rubrique
     *            qui sera taraité par le moteur
     * @return true or false si la rubrique sera traité par la rubrique complexe
     *         ou non
     */
    private boolean getRubriqueTraite(final Map<String, Boolean> mapRubriqueComplexe, final String rubrique) {
        boolean vrai = false;
        for (String string : mapRubriqueComplexe.keySet()) {
            if (StringUtils.contains(string, rubrique)) {
                if (mapRubriqueComplexe.get(string).equals(Boolean.TRUE)) {
                    vrai = true;
                    break;
                }
            }
        }
        return vrai;
    }

    /**
     * Convertit une liste non modifiable présente dans la SFD du xml regent
     * exemple : {'.','.'} de type UnmodifiableRandomAccessList en List<String>.
     *
     * @param value
     *            value
     * @return list
     */
    private List<String> convertToList(final Object value) {

        List<String> result = new ArrayList<String>();
        if (value instanceof Iterable<?>) {
            for (final Object obj : (Iterable<?>) value) {
                result.add(obj.toString());
            }
        }
        return result;

    }

    /**
     * cette méthode permet de traiter les rubriques avec des occurrences avec
     * un appel récursive à la même méthode si la rubrique en cours de
     * traitement contient d'autre rubrique avec occurrences.
     * 
     * Traitement rubrique complexe = une rubrique avec des occurrences.
     *
     * @param context
     *            context c'est le StandardEvaluationContext qui contient une
     *            liste des variables et des méthodes qui seront utilisé lors de
     *            l'évaluation des règles Spel
     * @param elementsRubrique
     *            elements rubrique la liste des balises qui seront traité dans
     *            cette rubrique Complexe
     * @param rubriqueConditionnelle
     *            rubrique conditionnelle une map <Rubrique,ObligatoireOuPas>
     *            qui nous renvoie si la rubrique est Obligatoire
     * @param mapSousRubriqueNonTraite
     *            map sous rubrique non traite permet d'indiqué les sous
     *            Rubrique qui été traité par la rubrique Complexe qui doivent
     *            pas être traiter comme une rubrique simple
     * @param mapRubriqueComplexite
     *            map rubrique complexite contient la liste des rubriques
     *            complexe
     * @param rubriqueOrdonne
     *            rubrique ordonne la liste des rubriques ordonnées
     * @param xmlRegent
     *            xml regent l'objet qui correspond à l'xml
     * @param chemainRelatifSousRubrique
     *            chemain relatif sous rubrique
     * @throws NoSuchMethodException
     *             no such method exception
     * @throws SecurityException
     *             security exception
     * @throws IllegalAccessException
     *             illegal access exception
     * @throws IllegalArgumentException
     *             illegal argument exception
     * @throws InvocationTargetException
     *             invocation target exception
     */
    private void traitementRubriqueComplexe(final StandardEvaluationContext context, final List<XmlFieldElement> elementsRubrique, final Map<String, Boolean> rubriqueConditionnelle,
            final Map<String, Boolean> mapSousRubriqueNonTraite, final Map<String, Boolean> mapRubriqueComplexite, final List<String> rubriqueOrdonne, final Object xmlRegent,
            final String chemainRelatifSousRubrique) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {

        XmlFieldElement racineRubrique = elementsRubrique.get(0);
        elementsRubrique.remove(racineRubrique);

        List<Object> occurences = (List<Object>) racineRubrique.getValue(context);

        if (!CollectionUtils.isEmpty(occurences)) {
            Object rubriqueComplexe = null;
            if (!StringUtils.isEmpty(chemainRelatifSousRubrique)) {
                rubriqueComplexe = this.xmlWriter.constructObjectToInvoc(chemainRelatifSousRubrique, xmlRegent);
            } else {
                rubriqueComplexe = this.xmlWriter.constructObjectToInvoc(racineRubrique.getPath(), xmlRegent);
            }
            for (Object occurenceCourantValue : occurences) {
                context.setVariable(OCCURENCE_COURANTE_RUBRIQUECOMPLEXE, occurenceCourantValue);

                // construire une Map<rubrique,List<XmlFieldElement>> qui va
                // nous
                // permettre de gérer le bourrage sur les balises obligatoire
                Multimap<String, XmlFieldElement> multiMapRubriqueOccurenceBalise = this.xmlWriter.constructMapRubriqueBalises(elementsRubrique);
                // Construire une Map<rubrique,Bourage> qui permet de nous
                // indiquer
                // les rubriques ou on doit mettre le bourrage pour les balises
                // obligatoire
                Map<String, Boolean> mettreBourageSurUneBaliseAvecOccurence = this.xmlWriter.bourageSurBaliseObligatoireVide(context, multiMapRubriqueOccurenceBalise);
                String prefixMethodSousRubrique = "addTo" + this.xmlWriter.constructBaliseFromPath(racineRubrique.getPath());

                Method m = rubriqueComplexe.getClass().getMethod(prefixMethodSousRubrique);
                Object rubrique1 = m.invoke(rubriqueComplexe, null);

                for (XmlFieldElement balise : elementsRubrique) {
                    // vérification si la balise doit étre générer dans la
                    // rubrique complexe
                    if (rubriqueOrdonne.contains(balise.getRubrique())) {
                        // vérification si la balise n'est pas une rubrique
                        // complexe
                        if (!mapRubriqueComplexite.get(balise.getRubrique()) || StringUtils.equals(racineRubrique.getRubrique(), balise.getRubrique())) {
                            LOGGER_FONC.info("la rubrique qui sera traité est :{}", balise.getRubrique());
                            Boolean bourrageBalise = mettreBourageSurUneBaliseAvecOccurence.get(balise.getRubrique());
                            BooleanUtils.toBooleanDefaultIfNull(bourrageBalise, false);
                            Boolean rubriqueConditionnelleOuObligatoire = rubriqueConditionnelle.get(balise.getRubrique());
                            BooleanUtils.toBooleanDefaultIfNull(rubriqueConditionnelleOuObligatoire, false);
                            this.saisieXml(context, rubrique1, balise, racineRubrique.getPath(), bourrageBalise, rubriqueConditionnelleOuObligatoire);
                            mapSousRubriqueNonTraite.put(balise.getRubrique(), true);
                        } else {
                            // le cas d'une sous rubrique conplexe dans rubrique
                            // complexe
                            List<XmlFieldElement> elementsSousRubriqueComplexe = this.xmlWriter.getXmlFieldElement(GROUPID, balise.getRubrique(), balise.getVersion());
                            // chemain relatif de la sous rubrique Complexe
                            String chemainRelatif = StringUtils.substringAfterLast(racineRubrique.getPath(), "/")
                                    + StringUtils.substringAfterLast(balise.getPath(), StringUtils.substringAfterLast(racineRubrique.getPath(), "/"));
                            this.traitementRubriqueComplexe(context, elementsSousRubriqueComplexe, rubriqueConditionnelle, mapSousRubriqueNonTraite, mapRubriqueComplexite, rubriqueOrdonne, rubrique1,
                                    chemainRelatif);
                            context.setVariable(OCCURENCE_COURANTE_RUBRIQUECOMPLEXE, occurenceCourantValue);
                        }
                    }
                }

            }
            context.setVariable(OCCURENCE_COURANTE_RUBRIQUECOMPLEXE, null);
        } else {
            for (XmlFieldElement xmlFieldElement : elementsRubrique) {
                mapSousRubriqueNonTraite.put(xmlFieldElement.getRubrique(), true);
            }
        }

    }

    /**
     * cette méthode permet d'écrire dans l'xmlRegent.
     *
     * @param context
     *            le context speel pour evaluer la value de l'xmlFielElement
     * @param xmlRegent
     *            l'objet xmlField qui contient la lite de toutes les balises
     *            qui seront présenté dans l'xmlRegent
     * @param xmlFielElement
     *            l'objet xmlField qui contient le path de la balise sur lequel
     *            on se base pour construire le setter qui coorrespant à la
     *            balise
     * @param racinePath
     *            racine path
     * @param bourrageBalise
     *            bourrage balise
     * @param rubriqueConditionnelle
     *            rubrique conditionnelle
     */
    private void saisieXml(final StandardEvaluationContext context, Object xmlRegent, final XmlFieldElement xmlFielElement, final String racinePath, final Boolean bourrageBalise,
            final Boolean rubriqueConditionnelle) {
        Object value = null;
        String path = null;
        value = xmlFielElement.getValue(context);

        try {
            Method m;
            if (value != null && !(value instanceof String && StringUtils.isEmpty((String) value)) && !(value instanceof Boolean) && !(StringUtils.equals(value.toString(), "[]"))) {
                String prefixMethod = this.xmlWriter.constructMethodToInvok(context, xmlFielElement);
                if (racinePath != null) {
                    String chemain = StringUtils.substringAfterLast(racinePath, "/");
                    path = chemain + StringUtils.substringAfterLast(xmlFielElement.getPath(), racinePath);
                } else {
                    path = xmlFielElement.getPath();
                }
                xmlRegent = this.xmlWriter.constructObjectToInvoc(path, xmlRegent);
                if (!(value instanceof List)) {
                    this.saisieXmlValueList(xmlRegent, value, prefixMethod);
                } else {
                    m = xmlRegent.getClass().getMethod(prefixMethod);
                    LOGGER_FONC.debug("la value est de type ={} prefixMethod ={} valeur={}", value.getClass().toString(), prefixMethod, value.toString());
                    List<String> list = null;
                    try {
                        list = (ArrayList<String>) value;
                    } catch (ClassCastException e) {
                        list = this.convertToList(value);// cas bourrage type
                                                         // liste
                    }
                    if (!list.isEmpty()) {
                        for (String string : list) {
                            string = this.suppressionCaracPointInterrogationSiBourrage(string);
                            Object xmlString = m.invoke(xmlRegent, null);
                            XmlString balise = (XmlString) xmlString;
                            LOGGER_FONC.debug("la value de la liste est  de type ={} valeur ={}", string.getClass().toString(), string);
                            balise.setString(string);
                        }
                    } else {
                        // mettre le bourrage sur les listes vide
                        if (rubriqueConditionnelle) {
                            if (xmlFielElement.getObligatoire() && bourrageBalise) {
                                Object xmlString = m.invoke(xmlRegent, null);
                                XmlString balise = (XmlString) xmlString;
                                balise.setString(xmlFielElement.getBourrage());
                            }
                        } else {
                            if (xmlFielElement.getObligatoire()) {
                                Object xmlString = m.invoke(xmlRegent, null);
                                XmlString balise = (XmlString) xmlString;
                                balise.setString(xmlFielElement.getBourrage());
                            }

                        }
                    }
                }

            } else {
                if (rubriqueConditionnelle) {
                    if (xmlFielElement.getObligatoire() && bourrageBalise) {
                        String prefixMethod = this.xmlWriter.constructMethodToInvok(context, xmlFielElement);
                        if (racinePath != null) {
                            String chemain = StringUtils.substringAfterLast(racinePath, "/");
                            path = chemain + StringUtils.substringAfterLast(xmlFielElement.getPath(), racinePath);
                        } else {
                            path = xmlFielElement.getPath();
                        }
                        xmlRegent = this.xmlWriter.constructObjectToInvoc(path, xmlRegent);

                        if (prefixMethod.contains("addTo")) {
                            m = xmlRegent.getClass().getMethod(prefixMethod);
                            Object xmlString = m.invoke(xmlRegent, null);
                            XmlString balise = (XmlString) xmlString;
                            balise.setString(xmlFielElement.getBourrage());
                        } else {
                            m = xmlRegent.getClass().getMethod(prefixMethod, String.class);
                            LOGGER_FONC.debug("le bourrage sera effectué  avec cette methode= {}", prefixMethod);
                            m.invoke(xmlRegent, xmlFielElement.getBourrage());
                        }
                    }
                } else {
                    if (xmlFielElement.getObligatoire()) {
                        String prefixMethod = this.xmlWriter.constructMethodToInvok(context, xmlFielElement);
                        if (racinePath != null) {
                            String chemain = StringUtils.substringAfterLast(racinePath, "/");
                            path = chemain + StringUtils.substringAfterLast(xmlFielElement.getPath(), racinePath);
                        } else {
                            path = xmlFielElement.getPath();
                        }
                        xmlRegent = this.xmlWriter.constructObjectToInvoc(path, xmlRegent);

                        if (prefixMethod.contains("addTo")) {
                            m = xmlRegent.getClass().getMethod(prefixMethod);
                            Object xmlString = m.invoke(xmlRegent, null);
                            XmlString balise = (XmlString) xmlString;
                            balise.setString(xmlFielElement.getBourrage());
                        } else {
                            m = xmlRegent.getClass().getMethod(prefixMethod, String.class);
                            LOGGER_FONC.debug("le bourrage sera effectué  avec cette methode= {}", prefixMethod);
                            m.invoke(xmlRegent, xmlFielElement.getBourrage());
                        }
                    }

                }
            }

            // LOGGER_FONC.info("la valeur l'xmlRegent:{}",
            // XF.objectToXml(xmlRegent));

        } catch (IllegalArgumentException e) {
            LOGGER_FONC.error(e.getMessage(), e);
        } catch (SecurityException e) {
            LOGGER_FONC.error(e.getMessage(), e);
        } catch (IllegalAccessException e) {
            LOGGER_FONC.error(e.getMessage(), e);
        } catch (InvocationTargetException e) {
            LOGGER_FONC.error(e.getMessage(), e);
        } catch (NoSuchMethodException e) {
            LOGGER_FONC.error(e.getMessage(), e);
        }
    }

    /**
     * saisieXmlValueStrin.
     *
     * @param xmlRegent
     *            xml regent
     * @param value
     *            value
     * @param prefixMethod
     *            prefix method
     * @throws NoSuchMethodException
     *             no such method exception
     * @throws IllegalAccessException
     *             illegal access exception
     * @throws InvocationTargetException
     *             invocation target exception
     */
    private void saisieXmlValueList(final Object xmlRegent, Object value, final String prefixMethod) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        Method m;
        if (StringUtils.isNotEmpty(value.toString())) {
            LOGGER_FONC.debug("la value est de type ={} prefixMethod ={} valeur={}", value.getClass().toString(), prefixMethod, value.toString());
            m = xmlRegent.getClass().getMethod(prefixMethod, String.class);
            value = suppressionCaracPointInterrogationSiBourrage(value.toString());

            m.invoke(xmlRegent, (String) value);
        }
    }

    /**
     * pour les valeurs ou on force le bourage pour les balises qui sont pas
     * obligatoire(ex:P41.1,P25.2).
     *
     * @param value
     *            value
     * @return string
     */
    private String suppressionCaracPointInterrogationSiBourrage(String value) {
        if ("?.".equals(value)) {
            value = StringUtils.remove(value, '?');
        }
        return value;
    }

    /**
     * Adds the shema location.
     *
     * @param xmlRegent
     *            xml regent
     * @param xsdMessageRegent
     *            xsd message regent
     * @return string
     */
    private String addShemaLocation(final String xmlRegent, final String xsdMessageRegent) {
        String nameSpaceXml = NAME_SPACE_XML_TEMPLATE.replace(VERSION_MESSAGE_REGENT, xsdMessageRegent);
        String xml = xmlRegent.replace("<REGENT-XML>", nameSpaceXml);
        StringBuilder xmlGenerer = new StringBuilder(xml);
        xmlGenerer.insert(0, ENCODING);
        return xmlGenerer.toString();
    }

    /**
     * Récupère le profil sous forme d'objet JAVA
     * 
     * @param typeFormalite
     *            le type de formalite
     * @param dialogueJson
     *            le profil au format JSON
     * @return le profil sous forme d'objet JAVA
     */
    private Object getProfilBean(final String typeFormalite, final String profilJson) {
        JSONObject json = new JSONObject(profilJson);
        String xml = ENCODING + XML.toString(json);
        if (typeFormalite.equals(TypeDossierEnum.CREATION.getFlowId())) {
            IProfilCreation iprofilCreation = null;
            try {
                iprofilCreation = new XmlField().xmlToObject(xml, IProfilCreation.class);
            } catch (XmlFieldParsingException e) {
                String errorMsg = String.format("Unable to parse profile content to generate Regent XML for version'");
                throw new TechnicalException(errorMsg, e);
            }
            ProfilCreationBean profilCreatiBean = new ProfilCreationBean();
            this.profilEntrepriseXmlToBeanConverteur.mapper(iprofilCreation, profilCreatiBean);
            return profilCreatiBean;
        } else if (typeFormalite.equals(TypeDossierEnum.CESSATION.getFlowId())) {
            IProfilCessation iformaliteCessation = null;
            try {
                iformaliteCessation = new XmlField().xmlToObject(xml, IProfilCessation.class);
            } catch (XmlFieldParsingException e) {
                String errorMsg = String.format("Unable to parse profile content to generate Regent XML for version'");
                throw new TechnicalException(errorMsg, e);
            }
            ProfilCessationBean profilCessationBean = new ProfilCessationBean();
            mapperProfilCessationUtils.mapper(iformaliteCessation, profilCessationBean);
            return profilCessationBean;
        } else if (typeFormalite.equals(TypeDossierEnum.REGULARISATION.getFlowId())) {
            IProfilRegularisation iprofilRegularisation = null;
            try {
                iprofilRegularisation = new XmlField().xmlToObject(xml, IProfilRegularisation.class);
            } catch (XmlFieldParsingException e) {
                String errorMsg = String.format("Unable to parse profile content to generate Regent XML for version'");
                throw new TechnicalException(errorMsg, e);
            }
            ProfilRegularisationBean profilRegularisationBean = new ProfilRegularisationBean();
            mapperProfilRegularisationUtils.mapper(iprofilRegularisation, profilRegularisationBean);
            return profilRegularisationBean;
        } else if (typeFormalite.equals(TypeDossierEnum.MODIFICATION.getFlowId())) {
            IProfilModification iformaliteModification = null;
            try {
                iformaliteModification = new XmlField().xmlToObject(xml, IProfilModification.class);
            } catch (XmlFieldParsingException e) {
                String errorMsg = String.format("Unable to parse profile content to generate Regent XML for version'");
                throw new TechnicalException(errorMsg, e);
            }
            ProfilModificationBean profilModificationBean = new ProfilModificationBean();
            mapperProfilModificationUtils.mapper(iformaliteModification, profilModificationBean);
            return profilModificationBean;
        }
        return null;
    }

    /**
     * Récupère le dialogue sous forme d'objet JAVA
     * 
     * @param typeFormalite
     *            le type de formalite
     * @param dialogueJson
     *            le dialogue au format JSON
     * @return le dialogue sous forme d'objet JAVA
     */
    private Object getDialogueBean(final String typeFormalite, final String dialogueJson) {
        JSONObject json = new JSONObject(dialogueJson);
        String xml = ENCODING + XML.toString(json);
        if (typeFormalite.equals(TypeDossierEnum.CREATION.getFlowId())) {
            IDialogueCreation iformaliteCreation;
            try {
                iformaliteCreation = new XmlField().xmlToObject(xml, IDialogueCreation.class);
            } catch (XmlFieldParsingException e) {
                String errorMsg = String.format("Unable to parse profile content to generate Regent XML for version'");
                throw new TechnicalException(errorMsg, e);
            }
            DialogueCreationBean formaliteCreation = new DialogueCreationBean();
            mapperFormaliteCreationUtils.mapper(iformaliteCreation, formaliteCreation);
            return formaliteCreation;
        } else if (typeFormalite.equals(TypeDossierEnum.CESSATION.getFlowId())) {
            IDialogueCessation iformaliteCessation;
            try {
                iformaliteCessation = new XmlField().xmlToObject(xml, IDialogueCessation.class);
            } catch (XmlFieldParsingException e) {
                String errorMsg = String.format("Unable to parse profile content to generate Regent XML for version'");
                throw new TechnicalException(errorMsg, e);
            }
            final DialogueCessationBean formalite = new DialogueCessationBean();
            mapperFormaliteCessationUtils.mapper(iformaliteCessation, formalite);
            return formalite;
        } else if (typeFormalite.equals(TypeDossierEnum.REGULARISATION.getFlowId())) {
            IDialogueRegularisation iformaliteRegularisation;
            try {
                iformaliteRegularisation = new XmlField().xmlToObject(xml, IDialogueRegularisation.class);
            } catch (XmlFieldParsingException e) {
                String errorMsg = String.format("Unable to parse profile content to generate Regent XML for version'");
                throw new TechnicalException(errorMsg, e);
            }
            final DialogueRegularisationBean formalite = new DialogueRegularisationBean();
            mapperFormaliteRegularisationUtils.mapper(iformaliteRegularisation, formalite);
            return formalite;
        } else if (typeFormalite.equals(TypeDossierEnum.MODIFICATION.getFlowId())) {
            IDialogueModification iformaliteModification;
            try {
                iformaliteModification = new XmlField().xmlToObject(xml, IDialogueModification.class);
            } catch (XmlFieldParsingException e) {
                String errorMsg = String.format("Unable to parse profile content to generate Regent XML for version'");
                throw new TechnicalException(errorMsg, e);
            }
            final DialogueModificationBean formalite = new DialogueModificationBean();
            mapperFormaliteModificationUtils.mapper(iformaliteModification, formalite);
            return formalite;
        }
        return null;
    }

    @Override
    public RegentResultBean getRegentXmlCap(List<String> listCodeEdi, String profil, String formalite, String codeEdi, String roleDestinataire, List<String> evenements, String version,
            String numeroLiasse, String typeFlux) throws TechnicalException {
        String regentXml = null;
        TypeDossierEnum typeDossierEnum = TypeDossierEnum.getTypeDossierByEvenement(evenements);
        if (null == typeDossierEnum) {
            throw new TechnicalException(String.format("Unknown formality type from list events {}", evenements));
        }
        try {
            final String typeFormalite = typeDossierEnum.getFlowId();
            final Object formaliteBean = getDialogueBean(typeFormalite, formalite);
            final Object profilBean = getProfilBean(typeFormalite, profil);
            regentXml = this.getRegentXmlCap(listCodeEdi, profilBean, formaliteBean, typeFormalite, codeEdi, roleDestinataire, evenements, version, numeroLiasse, typeFlux);
        } catch (Exception e) {
            throw new TechnicalException(e);
        }

        RegentResultBean regentResult = new RegentResultBean();
        regentResult.setContent(regentXml.getBytes());
        try {
            // -->Validation XSD
            this.validateurRegentMap.get(version).validate(regentXml);
        } catch (XmlInvalidException e) {
            LOGGER_FONC.warn(e.getMessage(), e);
            regentResult.setError(e.getMessage());
        } catch (XmlFatalException e) {
            LOGGER_FONC.error(e.getMessage(), e);
            throw new TechnicalException(e);
        }
        return regentResult;
    }

    /**
     * 
     * {@inheritDoc}
     */
    public void validateXmlRegent(String version, String regentXml) {
        try {
            // -->Validation XSD
            this.validateurRegentMap.get(version).validate(regentXml);
        } catch (XmlInvalidException e) {
            LOGGER_FONC.warn("Erreur lors de la validation du XML regent", e.getMessage(), e);
            throw new TechnicalException("Erreur de la validation du XML regent", e);
        } catch (XmlFatalException e) {
            LOGGER_FONC.error(e.getMessage(), e);
            throw new TechnicalException(e);
        }
    }

    /**
     * 
     * {@inheritDoc}
     */
    public LinkedHashMap<String, Object> fillMandatoryTags(String codeEdi, String roleDestinataire, List<String> evenements, String version, String numeroLiasse,
            LinkedHashMap<String, Object> mappedRegent) {

        // remplissage des balises par défault
        DateFormat dateHeureFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        String todayHeure = dateHeureFormatter.format(new Date());
        String todayDate = dateFormatter.format(new Date());
        mappedRegent.put("/REGENT-XML/Emetteur", "Z1611");
        mappedRegent.put("/REGENT-XML/DateHeureEmission", todayHeure);
        mappedRegent.put("/REGENT-XML/VersionMessage", version);
        mappedRegent.put("/REGENT-XML/VersionNorme", version);
        mappedRegent.put("/REGENT-XML/ServiceApplicatif/Specification/NomService", "ReceptionLiasseDéclarant");
        mappedRegent.put("/REGENT-XML/ServiceApplicatif/Specification/VersionService", version);
        mappedRegent.put("/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C04", todayDate);
        mappedRegent.put("/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C01", todayDate);
        mappedRegent.put("/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02", numeroLiasse);

        // Récupération du type de flux
        String codeDestinataireString = codeEdi.substring(0, 1);
        // Récupérer la list des rubriques nonOrdonne qui seront générées
        // par
        // le moteur
        List<String> rubriques = this.declenchementRubriqueXmlRegent.getListRubriqueForXmlRegent(version, evenements, roleDestinataire, codeDestinataireString);

        List<XmlFieldElement> elementsNonOrdonneByRubrique = new ArrayList<XmlFieldElement>();
        // la liste des XmlFiledElements de toutes les rubriques
        List<XmlFieldElement> elementsNonOrdonneGlobal = new ArrayList<XmlFieldElement>();
        for (String rubrique : rubriques) {
            elementsNonOrdonneByRubrique = this.xmlWriter.getXmlFieldElement(GROUPID, rubrique, version);
            elementsNonOrdonneGlobal.addAll(elementsNonOrdonneByRubrique);

        }
        // récupérer la liste de toutes les XmlFieldElement qui existe dans
        // la table regles_regent
        List<XmlFieldElement> elementsFromDBOrdonne = this.xmlWriter.getXmlFieldElement(GROUPID, version);
        // la liste des XMlFieldElements ordonné qui sera génerer par le
        // moteur
        List<XmlFieldElement> elements = this.xmlWriter.getListRubriqueOrdonne(elementsFromDBOrdonne, elementsNonOrdonneGlobal);

        // constituer une HashMap<Xpath, Rubrique> pour pouvoir retrouver les
        // balise obligatoires et leurs bourrages à partir du Xpath
        LinkedHashMap<String, Object> balisesRubrique = new LinkedHashMap<String, Object>();

        for (XmlFieldElement rubrique : elements) {
            String pathFromMap = String.format("/REGENT-XML%s", rubrique.getPath().substring(7));
            balisesRubrique.put(pathFromMap, rubrique);
        }

        // contruire une Map<Xpath d'une balise, valeur> en mettant
        // le bourrage sur les balises obligatoires vides
        LinkedHashMap<String, Object> balisesValeurs = new LinkedHashMap<String, Object>();
        for (String cheminBalise : mappedRegent.keySet()) {
            String cheminSansCrochets = cheminBalise.replaceAll("\\[[0-9]*\\]", "");
            XmlFieldElement rubrique = (XmlFieldElement) balisesRubrique.get(cheminSansCrochets);
            Object value = mappedRegent.get(cheminBalise);

            // si la balise est une feuille (endpath == true) et qu'elle est
            // présente dans le fichier mapping reçu
            if (rubrique != null && rubrique.getEndPath()) {

                if (rubrique.getObligatoire() && value == null) {

                    // si la balise est obligatoire et qu'elle est égale à null
                    // on met le bourrage associé
                    balisesValeurs.put(cheminBalise, rubrique.getBourrage());
                } else {

                    if (!(value instanceof List)) {
                        // si la valeur n'est pas de type liste on l'affecte à
                        // la balise
                        balisesValeurs.put(cheminBalise, value);
                    } else {
                        // si la balise est de type liste on la caste en liste
                        // et on crée plusieur instances de cette balise avec
                        // les différente valeurs de la liste
                        List<String> list = getValueAsList(value);
                        for (int i = 0; i < list.size(); i++) {
                            balisesValeurs.put(cheminBalise.concat("[").concat(String.valueOf(i)).concat("]"), list.get(i));
                        }
                    }
                }
            } else {
                // If the rubrique is not present in the Database
                // we need to check if the value is a list
                // it need to have as many instance as value sin the list
                // Else take the value as it is

                if (!(value instanceof List)) {
                    // si la valeur n'est pas de type liste on l'affecte à
                    // la balise
                    balisesValeurs.put(cheminBalise, value);
                } else {
                    // si la balise est de type liste on la caste en liste
                    // et on crée plusieur instances de cette balise avec
                    // les différente valeurs de la liste
                    List<String> list = getValueAsList(value);
                    for (int i = 0; i < list.size(); i++) {
                        balisesValeurs.put(cheminBalise.concat("[").concat(String.valueOf(i)).concat("]"), list.get(i));
                    }
                }
                // balisesValeurs.put(cheminBalise,
                // mappedRegent.get(cheminBalise));
            }
        }

        return balisesValeurs;

    }

    /**
     * casts an Object value instance of List to a list of String.
     * 
     * @param value
     *            the Object value instance of List
     * @return a List<String> of different elements in the value Object
     */
    private List<String> getValueAsList(Object value) {

        List<String> list = null;
        try {
            list = (ArrayList<String>) value;
        } catch (ClassCastException e) {
            list = this.convertToList(value);// cas bourrage type
                                             // liste
        }

        return list;
    }

    /**
     * {@inheritDoc}
     */
    public List<XmlFieldElement> getTagsByEventsAndVersion(List<String> evenements, String version) {

        // récupérer la liste des rubriques par rapport à une version et une
        // liste d'évènements
        List<String> rubriques = this.declenchementRubriqueXmlRegent.getListRubriqueByEventAndVersion(evenements, version);

        List<XmlFieldElement> elementsNonOrdonneByRubrique = new ArrayList<XmlFieldElement>();
        // la liste des XmlFiledElements de toutes les rubriques
        List<XmlFieldElement> elementsNonOrdonneGlobal = new ArrayList<XmlFieldElement>();
        for (String rubrique : rubriques) {
            elementsNonOrdonneByRubrique = this.xmlWriter.getXmlFieldElement(GROUPID, rubrique, version);
            elementsNonOrdonneGlobal.addAll(elementsNonOrdonneByRubrique);

        }

        // récupérer la liste de toutes les XmlFieldElement ordonnées tel que
        // dans la table regles_regent
        List<XmlFieldElement> elementsFromDBOrdonne = this.xmlWriter.getXmlFieldElement(GROUPID, version);
        // la liste des XMlFieldElements ordonné qui sera génerer par le
        // moteur
        List<XmlFieldElement> elements = this.xmlWriter.getListRubriqueOrdonne(elementsFromDBOrdonne, elementsNonOrdonneGlobal);

        return elements;
    }
}
