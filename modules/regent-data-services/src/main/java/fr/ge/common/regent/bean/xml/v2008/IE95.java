package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IE95.
 */
@ResourceXPath("/E95")
public interface IE95 {

  /**
   * Get le e951.
   *
   * @return le e951
   */
  @FieldXPath("E95.1")
  String getE951();

  /**
   * Set le e951.
   *
   * @param E951
   *          le nouveau e951
   */
  void setE951(String E951);

  /**
   * Get le e952.
   *
   * @return le e952
   */
  @FieldXPath("E95.2")
  String getE952();

  /**
   * Set le e952.
   *
   * @param E952
   *          le nouveau e952
   */
  void setE952(String E952);

  /**
   * Get le e953.
   *
   * @return le e953
   */
  @FieldXPath("E95.3")
  String getE953();

  /**
   * Set le e953.
   *
   * @param E953
   *          le nouveau e953
   */
  void setE953(String E953);

  /**
   * Get le e954.
   *
   * @return le e954
   */
  @FieldXPath("E95.4")
  String getE954();

  /**
   * Set le e954.
   *
   * @param E954
   *          le nouveau e954
   */
  void setE954(String E954);

}
