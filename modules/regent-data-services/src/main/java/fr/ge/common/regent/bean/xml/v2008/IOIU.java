package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Interface IOIU.
 */
@ResourceXPath("/OIU")
public interface IOIU {

  /**
   * Get le u75.
   *
   * @return le u75
   */
  @FieldXPath("U75")
  String getU75();

  /**
   * Set le u75
   *
   * @param u75
   *          le nouveau u75
   */
  void setU75(String U75);
}
