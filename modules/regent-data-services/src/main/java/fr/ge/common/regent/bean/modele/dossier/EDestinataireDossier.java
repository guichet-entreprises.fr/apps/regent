package fr.ge.common.regent.bean.modele.dossier;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 *
 * Entité de la table DESTINATAIRE_DOSSIER.
 *
 */
@Entity
@Table(name = "DESTINATAIRE_DOSSIER")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class EDestinataireDossier implements Serializable {

  /** La constante serialVersionUID. */
  private static final long serialVersionUID = -8171035653794756891L;

  /** Le code edi. */
  @Column(name = "CODE_EDI", nullable = false)
  private String codeEdi;

  /** Le id. */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID", nullable = false)
  private Long id;

  /** Le id dossier. */
  @Column(name = "ID_DOSSIER", nullable = false)
  private Long idDossier;

  /** Le role. */
  @Column(name = "ROLE", nullable = false)
  private String role;

  /**
   * Constructeur.
   */
  public EDestinataireDossier() {
  }

  /**
   * {@inheritDoc}
   */
  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    HashCodeBuilder hashCodeBuilder = new HashCodeBuilder(1, 31);
    hashCodeBuilder.append(codeEdi);
    hashCodeBuilder.append(idDossier);
    hashCodeBuilder.append(role);

    return hashCodeBuilder.toHashCode();

  }

  /**
   * {@inheritDoc}
   */
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (!(obj instanceof EDestinataireDossier)) {
      return false;
    }
    EDestinataireDossier other = (EDestinataireDossier) obj;
    return new EqualsBuilder().append(codeEdi, other.codeEdi).append(idDossier, other.idDossier).append(role, other.role)
      .isEquals();
  }

  /**
   * getCodeEdi.
   *
   * @return codeEdi
   */
  public String getCodeEdi() {
    return this.codeEdi;
  }

  /**
   * getId.
   *
   * @return id
   */
  public Long getId() {
    return this.id;
  }

  /**
   * getIdDossier.
   *
   * @return idDossier
   */
  public Long getIdDossier() {
    return this.idDossier;
  }

  /**
   * getRole.
   *
   * @return role
   */
  public String getRole() {
    return this.role;
  }

  /**
   * setCodeEdi.
   *
   * @param codeEdi
   *          codeEdi
   */
  public void setCodeEdi(String codeEdi) {
    this.codeEdi = codeEdi;
  }

  /**
   * setId.
   *
   * @param id
   *          id
   */
  public void setId(Long id) {
    this.id = id;
  }

  /**
   * setIdDossier.
   *
   * @param idDossier
   *          idDossier
   */
  public void setIdDossier(Long idDossier) {
    this.idDossier = idDossier;
  }

  /**
   * setRole.
   *
   * @param role
   *          role
   */
  public void setRole(String role) {
    this.role = role;
  }

}
