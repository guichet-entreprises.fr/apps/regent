package fr.ge.common.regent.bean.xml.formalite.dialogue.creation;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;
import org.xmlfield.core.types.XmlString;

import fr.ge.common.regent.bean.formalite.dialogue.creation.RegimeFiscalBean;
import fr.ge.common.regent.bean.xml.formalite.IFormalite;
import fr.ge.common.regent.bean.xml.transverse.IAdresse;

/**
 * Le Interface de l'entité entreprise
 * 
 * 
 * 
 */
@ResourceXPath("/formalite")
public interface IEntrepriseCreation extends IFormalite {

    /**
     * Getter de l'attribut activitesPrincipales.
     * 
     * @return la valeur de activitesPrincipales
     */
    @FieldXPath(value = "activitesPrincipales")
    String getActivitesPrincipales();

    /**
     * Getter de l'attribut adresse.
     * 
     * @return la valeur de adresse
     */
    @FieldXPath(value = "adresse")
    IAdresse getAdresse();

    /**
     * New adresse signature.
     *
     * @return le i adresse
     */
    IAdresse newAdresse();

    /**
     * Getter de l'attribut adresseEntreprisePMSituation.
     * 
     * @return la valeur de adresseEntreprisePMSituation
     */
    @FieldXPath(value = "adresseEntreprisePMSituation")
    String getAdresseEntreprisePMSituation();

    /**
     * Getter de l'attribut associeUniqueEstPresident.
     * 
     * @return la valeur de associeUniqueEstPresident
     */
    @FieldXPath(value = "associeUniqueEstPresident")
    String getAssocieUniqueEstPresident();

    /**
     * Getter de l'attribut capitalApportsNature.
     * 
     * @return la valeur de capitalApportsNature
     */
    @FieldXPath(value = "capitalApportsNature")
    XmlString[] getCapitalApportsNature();

    /**
     * Setter de l'attribut capitalApportsNature.
     * 
     * @param capitalApportsNature
     *            la nouvelle valeur de capitalApportsNature
     */
    void setCapitalApportsNature(XmlString[] capitalApportsNature);

    /**
     * Retire le from capitalApportsNature.
     *
     * @param capitalApportsNature
     *            le capitalApportsNature
     */
    void removeFromCapitalApportsNature(XmlString capitalApportsNature);

    /**
     * Ajoute un indice à la liste des capitalApportsNature.
     *
     * @return XmlString
     */
    XmlString addToCapitalApportsNature();

    /**
     * Getter de l'attribut capitalApportsNatureSuperieur30000.
     * 
     * @return la valeur de capitalApportsNatureSuperieur30000
     */
    @FieldXPath(value = "capitalApportsNatureSuperieur30000")
    String getCapitalApportsNatureSuperieur30000();

    /**
     * Getter de l'attribut capitalApportsNatureSuperieurMoitieCapital.
     * 
     * @return la valeur de capitalApportsNatureSuperieurMoitieCapital
     */
    @FieldXPath(value = "capitalApportsNatureSuperieurMoitieCapital")
    String getCapitalApportsNatureSuperieurMoitieCapital();

    /**
     * Getter de l'attribut capitalDevise.
     * 
     * @return la valeur de capitalDevise
     */
    @FieldXPath(value = "capitalDevise")
    String getCapitalDevise();

    /**
     * Getter de l'attribut capitalMontant.
     * 
     * @return la valeur de capitalMontant
     */
    @FieldXPath(value = "capitalMontant")
    String getCapitalMontant();

    /**
     * Getter de l'attribut capitalVariable.
     * 
     * @return la valeur de capitalVariable
     */
    @FieldXPath(value = "capitalVariable")
    String getCapitalVariable();

    /**
     * Getter de l'attribut capitalVariableMinimum.
     * 
     * @return la valeur de capitalVariableMinimum
     */
    @FieldXPath(value = "capitalVariableMinimum")
    String getCapitalVariableMinimum();

    /**
     * Getter de l'attribut contratAppuiDateFin.
     * 
     * @return la valeur de contratAppuiDateFin
     */
    @FieldXPath(value = "contratAppuiDateFin")
    String getContratAppuiDateFin();

    /**
     * Getter de l'attribut contratAppuiPresence.
     * 
     * @return la valeur de contratAppuiPresence
     */
    @FieldXPath(value = "contratAppuiPresence")
    String getContratAppuiPresence();

    /**
     * Getter de l'attribut dateAgrementGAEC.
     * 
     * @return la valeur de dateAgrementGAEC
     */
    @FieldXPath(value = "dateAgrementGAEC")
    String getDateAgrementGAEC();

    /**
     * Getter de l'attribut dateClotureExerciceSocial.
     * 
     * @return la valeur de dateClotureExerciceSocial
     */
    @FieldXPath(value = "dateClotureExerciceSocial")
    String getDateClotureExerciceSocial();

    /**
     * Getter de l'attribut dateCloturePremierExerciceSocial.
     * 
     * @return la valeur de dateCloturePremierExerciceSocial
     */
    @FieldXPath(value = "dateCloturePremierExerciceSocial")
    String getDateCloturePremierExerciceSocial();

    /**
     * Getter de l'attribut demandeACCRE.
     * 
     * @return la valeur de demandeACCRE
     */
    @FieldXPath(value = "demandeACCRE")
    String getDemandeACCRE();

    /**
     * Getter de l'attribut denomination.
     * 
     * @return la valeur de denomination
     */
    @FieldXPath(value = "denomination")
    String getDenomination();

    /**
     * Getter de l'attribut duree.
     * 
     * @return la valeur de duree
     */
    @FieldXPath(value = "duree")
    Integer getDuree();

    /**
     * Getter de l'attribut eirl.
     * 
     * @return la valeur de eirl
     */
    @FieldXPath(value = "eirl")
    IEirlCreation getEirl();

    /**
     * New EirlCreation.
     *
     * @return le i EirlCreation
     */
    IEirlCreation newEirl();

    /**
     * getEntrepriseLieeContratAppui.
     * 
     * @return entrepriseLieeContratAppui
     */
    @FieldXPath(value = "entrepriseLieeContratAppui")
    IEntrepriseLieeCreation getEntrepriseLieeContratAppui();

    /**
     * New entrepriseLieeContratAppui.
     *
     * @return le i EirlCreation création
     */
    IEntrepriseLieeCreation newEntrepriseLieeContratAppui();

    /**
     * Getter de l'attribut entreprisesLieesFusionScission.
     * 
     * @return la valeur de entreprisesLieesFusionScission
     */
    @FieldXPath(value = "entreprisesLieesFusionScission")
    IEntrepriseLieeCreation[] getEntreprisesLieesFusionScission();

    /**
     * Retire le from EntreprisesLieesFusionScission.
     *
     * @param EntreprisesLieesFusionScission
     *            le EntreprisesLieesFusionScission
     */
    void removeFromEntreprisesLieesFusionScission(final IEntrepriseLieeCreation iEntrepriseLieeCreation);

    /**
     * Ajoute le to entreprisesLieesFusionScission.
     *
     * @return les entreprisesLieesFusionScission
     */
    IEntrepriseLieeCreation addToEntreprisesLieesFusionScission();

    /**
     * Getter de l'attribut entreprisesLieesPrecedentsExploitants.
     * 
     * @return la valeur de entreprisesLieesPrecedentsExploitants
     */
    @FieldXPath(value = "entreprisesLieesPrecedentsExploitants/entreprisesLieesPrecedentsExploitant")
    IEntrepriseLieeCreation[] getEntreprisesLieesPrecedentsExploitants();

    /**
     * Retire le from entreprisesLieesPrecedentsExploitants.
     *
     * @param entreprisesLieesPrecedentsExploitants
     *            le entreprisesLieesPrecedentsExploitants
     */
    void removeFromEntreprisesLieesPrecedentsExploitants(final IEntrepriseLieeCreation iEntrepriseLieeCreation);

    /**
     * Ajoute le to entreprisesLieesPrecedentsExploitants.
     *
     * @return les entreprisesLieesPrecedentsExploitants
     */
    IEntrepriseLieeCreation addToEntreprisesLieesPrecedentsExploitants();

    /**
     * Get le entreprise liee domiciliation.
     *
     * @return le entreprise liee domiciliation
     */
    @FieldXPath(value = "entrepriseLieeDomiciliation")
    IEntrepriseLieeCreation getEntrepriseLieeDomiciliation();

    /**
     * Set le entreprise liee domiciliation.
     *
     * @param entrepriseLieeDomiciliation
     *            le nouveau entreprise liee domiciliation
     */
    void setEntrepriseLieeDomiciliation(IEntrepriseLieeCreation entrepriseLieeDomiciliation);

    /**
     * New entreprise liee domiciliation.
     *
     * @return le i entreprise liee modification
     */
    IEntrepriseLieeCreation newEntrepriseLieeDomiciliation();

    /**
     * Get le entreprise liee loueur mandant.
     *
     * @return le entreprise liee loueur mandant
     */
    @FieldXPath(value = "entrepriseLieeLoueurMandant")
    IEntrepriseLieeCreation getEntrepriseLieeLoueurMandant();

    /**
     * Set le entreprise liee loueur mandant.
     *
     * @param entrepriseLieeLoueurMandant
     *            le nouveau entreprise liee loueur mandant
     */
    void setEntrepriseLieeLoueurMandant(IEntrepriseLieeCreation entrepriseLieeLoueurMandant);

    /**
     * New entreprise liee loueur mandant.
     *
     * @return le i entreprise liee modification
     */
    IEntrepriseLieeCreation newEntrepriseLieeLoueurMandant();

    /**
     * Getter de l'attribut estActiviteArtisanaleOption.
     * 
     * @return la valeur de estActiviteArtisanaleOption
     */
    @FieldXPath(value = "estActiviteArtisanaleOption")
    String getEstActiviteArtisanaleOption();

    /**
     * Getter de l'attribut estActiviteArtisanalePrincipale.
     * 
     * @return la valeur de estActiviteArtisanalePrincipale
     */
    @FieldXPath(value = "estActiviteArtisanalePrincipale")
    String getEstActiviteArtisanalePrincipale();

    /**
     * Getter de l'attribut estInscritRCS.
     * 
     * @return la valeur de estInscritRCS
     */
    @FieldXPath(value = "estInscritRCS")
    String getEstInscritRCS();

    /**
     * Getter de l'attribut estInscritRM.
     * 
     * @return la valeur de estInscritRM
     */
    @FieldXPath(value = "estInscritRM")
    String getEstInscritRM();

    /**
     * Getter de l'attribut estProprietaireTerresExploitees.
     * 
     * @return la valeur de estProprietaireTerresExploitees
     */
    @FieldXPath(value = "estProprietaireTerresExploitees")
    String getEstProprietaireTerresExploitees();

    /**
     * Getter de l'attribut formeGroupementPastoral.
     * 
     * @return la valeur de formeGroupementPastoral
     */
    @FieldXPath(value = "formeGroupementPastoral")
    String getFormeGroupementPastoral();

    /**
     * Getter de l'attribut fusionScission.
     * 
     * @return la valeur de fusionScission
     */
    @FieldXPath(value = "fusionScission")
    String getFusionScission();

    /**
     * Getter de l'attribut fusionScissionNombre.
     * 
     * @return la valeur de fusionScissionNombre
     */
    @FieldXPath(value = "fusionScissionNombre")
    Integer getFusionScissionNombre();

    /**
     * Getter de l'attribut informationDepotFonds.
     * 
     * @return la valeur de informationDepotFonds
     */
    @FieldXPath(value = "informationDepotFonds")
    String getInformationDepotFonds();

    /**
     * Getter de l'attribut natureGerance.
     * 
     * @return la valeur de natureGerance
     */
    @FieldXPath(value = "natureGerance")
    String getNatureGerance();

    /**
     * Getter de l'attribut natureGeranceSocieteAssociee.
     * 
     * @return la valeur de natureGeranceSocieteAssociee
     */
    @FieldXPath(value = "natureGeranceSocieteAssociee")
    String getNatureGeranceSocieteAssociee();

    /**
     * Getter de l'attribut nombreDirigeants.
     * 
     * @return la valeur de nombreDirigeants
     */
    @FieldXPath(value = "nombreDirigeants")
    Integer getNombreDirigeants();

    /**
     * Getter de l'attribut personnePouvoirNombre.
     * 
     * @return la valeur de personnePouvoirNombre
     */
    @FieldXPath(value = "personnePouvoirNombre")
    Integer getPersonnePouvoirNombre();

    /**
     * Getter de l'attribut personnePouvoirPresence.
     * 
     * @return la valeur de personnePouvoirPresence
     */
    @FieldXPath(value = "personnePouvoirPresence")
    String getPersonnePouvoirPresence();

    /**
     * Getter de l'attribut regimeFiscalEirl.
     * 
     * @return la valeur de regimeFiscalEirl
     */
    @FieldXPath(value = "regimeFiscalEirl")
    IRegimeFiscal getRegimeFiscalEirl();

    /**
     * New regimeFiscalEirl.
     *
     * @return le i adresse
     */
    IRegimeFiscal newRegimeFiscalEirl();

    /**
     * Getter de l'attribut regimeFiscalEirlSecondaire.
     * 
     * @return la valeur de regimeFiscalEirlSecondaire
     */
    @FieldXPath(value = "regimeFiscalEirlSecondaire")
    IRegimeFiscal getRegimeFiscalEirlSecondaire();

    /**
     * New regimeFiscalEirlSecondaire.
     *
     * @return le i adresse
     */
    IRegimeFiscal newRegimeFiscalEirlSecondaire();

    /**
     * Getter de l'attribut regimeFiscalEtablissement.
     * 
     * @return la valeur de regimeFiscalEtablissement
     */
    @FieldXPath(value = "regimeFiscalEtablissement")
    IRegimeFiscal getRegimeFiscalEtablissement();

    /**
     * New regimeFiscalEtablissement.
     *
     * @return le i adresse
     */
    IRegimeFiscal newRegimeFiscalEtablissement();

    /**
     * Getter de l'attribut regimeFiscalEtablissementSecondaire.
     * 
     * @return la valeur de regimeFiscalEtablissementSecondaire
     */
    @FieldXPath(value = "regimeFiscalEtablissementSecondaire")
    IRegimeFiscal getRegimeFiscalEtablissementSecondaire();

    /**
     * New regimeFiscalEtablissementSecondaire.
     *
     * @return le i adresse
     */
    IRegimeFiscal newRegimeFiscalEtablissementSecondaire();

    /**
     * Getter de l'attribut sigle.
     * 
     * @return la valeur de sigle
     */
    @FieldXPath(value = "sigle")
    String getSigle();

    /**
     * Getter de l'attribut statutLegalParticulier.
     * 
     * @return la valeur de statutLegalParticulier
     */
    @FieldXPath(value = "statutLegalParticulier")
    String getStatutLegalParticulier();

    /**
     * Getter de l'attribut statutsTypes.
     * 
     * @return la valeur de statutsTypes
     */
    @FieldXPath(value = "statutsTypes")
    String getStatutsTypes();

    /**
     * Setter de l'attribut activitesPrincipales.
     * 
     * @param activitesPrincipales
     *            la nouvelle valeur de activitesPrincipales
     */
    void setActivitesPrincipales(String activitesPrincipales);

    /**
     * Setter de l'attribut adresse.
     * 
     * @param adresse
     *            la nouvelle valeur de adresse
     */
    void setAdresse(IAdresse adresse);

    /**
     * Setter de l'attribut adresseEntreprisePMSituation.
     * 
     * @param adresseEntreprisePMSituation
     *            la nouvelle valeur de adresseEntreprisePMSituation
     */
    void setAdresseEntreprisePMSituation(String adresseEntreprisePMSituation);

    /**
     * Setter de l'attribut associeUniqueEstPresident.
     * 
     * @param associeUniqueEstPresident
     *            la nouvelle valeur de associeUniqueEstPresident
     */
    void setAssocieUniqueEstPresident(String associeUniqueEstPresident);

    /**
     * Setter de l'attribut capitalApportsNatureSuperieur30000.
     * 
     * @param capitalApportsNatureSuperieur30000
     *            la nouvelle valeur de capitalApportsNatureSuperieur30000
     */
    void setCapitalApportsNatureSuperieur30000(String capitalApportsNatureSuperieur30000);

    /**
     * Setter de l'attribut capitalApportsNatureSuperieurMoitieCapital.
     * 
     * @param capitalApportsNatureSuperieurMoitieCapital
     *            la nouvelle valeur de
     *            capitalApportsNatureSuperieurMoitieCapital
     */
    void setCapitalApportsNatureSuperieurMoitieCapital(String capitalApportsNatureSuperieurMoitieCapital);

    /**
     * Setter de l'attribut capitalDevise.
     * 
     * @param capitalDevise
     *            la nouvelle valeur de capitalDevise
     */
    void setCapitalDevise(String capitalDevise);

    /**
     * Setter de l'attribut capitalMontant.
     * 
     * @param capitalMontant
     *            la nouvelle valeur de capitalMontant
     */
    void setCapitalMontant(String capitalMontant);

    /**
     * Setter de l'attribut capitalVariable.
     * 
     * @param capitalVariable
     *            la nouvelle valeur de capitalVariable
     */
    void setCapitalVariable(String capitalVariable);

    /**
     * Setter de l'attribut capitalVariableMinimum.
     * 
     * @param capitalVariableMinimum
     *            la nouvelle valeur de capitalVariableMinimum
     */
    void setCapitalVariableMinimum(String capitalVariableMinimum);

    /**
     * Setter de l'attribut contratAppuiDateFin.
     * 
     * @param contratAppuiDateFin
     *            la nouvelle valeur de contratAppuiDateFin
     */
    void setContratAppuiDateFin(String contratAppuiDateFin);

    /**
     * Setter de l'attribut contratAppuiPresence.
     * 
     * @param contratAppuiPresence
     *            la nouvelle valeur de contratAppuiPresence
     */
    void setContratAppuiPresence(String contratAppuiPresence);

    /**
     * Setter de l'attribut dateAgrementGAEC.
     * 
     * @param dateAgrementGAEC
     *            la nouvelle valeur de dateAgrementGAEC
     */
    void setDateAgrementGAEC(String dateAgrementGAEC);

    /**
     * Setter de l'attribut dateClotureExerciceSocial.
     * 
     * @param dateClotureExerciceSocial
     *            la nouvelle valeur de dateClotureExerciceSocial
     */
    void setDateClotureExerciceSocial(String dateClotureExerciceSocial);

    /**
     * Setter de l'attribut dateCloturePremierExerciceSocial.
     * 
     * @param dateCloturePremierExerciceSocial
     *            la nouvelle valeur de dateCloturePremierExerciceSocial
     */
    void setDateCloturePremierExerciceSocial(String dateCloturePremierExerciceSocial);

    /**
     * Setter de l'attribut demandeACCRE.
     * 
     * @param demandeACCRE
     *            la nouvelle valeur de demandeACCRE
     */
    void setDemandeACCRE(String demandeACCRE);

    /**
     * Setter de l'attribut denomination.
     * 
     * @param denomination
     *            la nouvelle valeur de denomination
     */
    void setDenomination(String denomination);

    /**
     * Setter de l'attribut duree.
     * 
     * @param duree
     *            la nouvelle valeur de duree
     */
    void setDuree(Integer duree);

    /**
     * Setter de l'attribut eirl.
     * 
     * @param eirl
     *            la nouvelle valeur de eirl
     */
    void setEirl(IEirlCreation eirl);

    /**
     * setEntrepriseLieeContratAppui.
     * 
     * @param entrepriseLieeContratAppui
     *            entrepriseLieeContratAppui
     */
    void setEntrepriseLieeContratAppui(IEntrepriseLieeCreation entrepriseLieeContratAppui);

    /**
     * Setter de l'attribut entreprisesLieesFusionScission.
     * 
     * @param entreprisesLieesFusionScission
     *            la nouvelle valeur de entreprisesLieesFusionScission
     */
    void setEntreprisesLieesFusionScission(IEntrepriseLieeCreation[] entreprisesLieesFusionScission);

    /**
     * Setter de l'attribut entreprisesLieesPrecedentsExploitants.
     * 
     * @param entreprisesLieesPrecedentsExploitants
     *            la nouvelle valeur de entreprisesLieesPrecedentsExploitants
     */
    void setEntreprisesLieesPrecedentsExploitants(IEntrepriseLieeCreation[] entreprisesLieesPrecedentsExploitants);

    /**
     * Setter de l'attribut estActiviteArtisanaleOption.
     * 
     * @param estActiviteArtisanaleOption
     *            la nouvelle valeur de estActiviteArtisanaleOption
     */
    void setEstActiviteArtisanaleOption(String estActiviteArtisanaleOption);

    /**
     * Setter de l'attribut estActiviteArtisanalePrincipale.
     * 
     * @param estActiviteArtisanalePrincipale
     *            la nouvelle valeur de estActiviteArtisanalePrincipale
     */
    void setEstActiviteArtisanalePrincipale(String estActiviteArtisanalePrincipale);

    /**
     * Setter de l'attribut estInscritRCS.
     * 
     * @param estInscritRCS
     *            la nouvelle valeur de estInscritRCS
     */
    void setEstInscritRCS(String estInscritRCS);

    /**
     * Setter de l'attribut estInscritRM.
     * 
     * @param estInscritRM
     *            la nouvelle valeur de estInscritRM
     */
    void setEstInscritRM(String estInscritRM);

    /**
     * Setter de l'attribut estProprietaireTerresExploitees.
     * 
     * @param estProprietaireTerresExploitees
     *            la nouvelle valeur de estProprietaireTerresExploitees
     */
    void setEstProprietaireTerresExploitees(String estProprietaireTerresExploitees);

    /**
     * Setter de l'attribut formeGroupementPastoral.
     * 
     * @param formeGroupementPastoral
     *            la nouvelle valeur de formeGroupementPastoral
     */
    void setFormeGroupementPastoral(String formeGroupementPastoral);

    /**
     * Setter de l'attribut fusionScission.
     * 
     * @param fusionScission
     *            la nouvelle valeur de fusionScission
     */
    void setFusionScission(String fusionScission);

    /**
     * Setter de l'attribut fusionScissionNombre.
     * 
     * @param fusionScissionNombre
     *            la nouvelle valeur de fusionScissionNombre
     */
    void setFusionScissionNombre(Integer fusionScissionNombre);

    /**
     * Setter de l'attribut informationDepotFonds.
     * 
     * @param informationDepotFonds
     *            la nouvelle valeur de informationDepotFonds
     */
    void setInformationDepotFonds(String informationDepotFonds);

    /**
     * Setter de l'attribut natureGerance.
     * 
     * @param natureGerance
     *            la nouvelle valeur de natureGerance
     */
    void setNatureGerance(String natureGerance);

    /**
     * Setter de l'attribut natureGeranceSocieteAssociee.
     * 
     * @param natureGeranceSocieteAssociee
     *            la nouvelle valeur de natureGeranceSocieteAssociee
     */
    void setNatureGeranceSocieteAssociee(String natureGeranceSocieteAssociee);

    /**
     * Setter de l'attribut nombreDirigeants.
     * 
     * @param nombreDirigeants
     *            la nouvelle valeur de nombreDirigeants
     */
    void setNombreDirigeants(Integer nombreDirigeants);

    /**
     * Setter de l'attribut personnePouvoirNombre.
     * 
     * @param personnePouvoirNombre
     *            la nouvelle valeur de personnePouvoirNombre
     */
    void setPersonnePouvoirNombre(Integer personnePouvoirNombre);

    /**
     * Setter de l'attribut personnePouvoirPresence.
     * 
     * @param personnePouvoirPresence
     *            la nouvelle valeur de personnePouvoirPresence
     */
    void setPersonnePouvoirPresence(String personnePouvoirPresence);

    /**
     * Setter de l'attribut regimeFiscalEirl.
     * 
     * @param regimeFiscalEirl
     *            la nouvelle valeur de regimeFiscalEirl
     */
    void setRegimeFiscalEirl(RegimeFiscalBean regimeFiscalEirl);

    /**
     * Setter de l'attribut regimeFiscalEirlSecondaire.
     * 
     * @param regimeFiscalEirlSecondaire
     *            la nouvelle valeur de regimeFiscalEirlSecondaire
     */
    void setRegimeFiscalEirlSecondaire(RegimeFiscalBean regimeFiscalEirlSecondaire);

    /**
     * Setter de l'attribut regimeFiscalEtablissement.
     * 
     * @param regimeFiscalEtablissement
     *            la nouvelle valeur de regimeFiscalEtablissement
     */
    void setRegimeFiscalEtablissement(RegimeFiscalBean regimeFiscalEtablissement);

    /**
     * Setter de l'attribut regimeFiscalEtablissementSecondaire.
     * 
     * @param regimeFiscalEtablissementSecondaire
     *            la nouvelle valeur de regimeFiscalEtablissementSecondaire
     */
    void setRegimeFiscalEtablissementSecondaire(RegimeFiscalBean regimeFiscalEtablissementSecondaire);

    /**
     * Setter de l'attribut sigle.
     * 
     * @param sigle
     *            la nouvelle valeur de sigle
     */
    void setSigle(String sigle);

    /**
     * Setter de l'attribut statutLegalParticulier.
     * 
     * @param statutLegalParticulier
     *            la nouvelle valeur de statutLegalParticulier
     */
    void setStatutLegalParticulier(String statutLegalParticulier);

    /**
     * Setter de l'attribut statutsTypes.
     * 
     * @param statutsTypes
     *            la nouvelle valeur de statutsTypes
     */
    void setStatutsTypes(String statutsTypes);

    /**
     * Get le forme juridique.
     *
     * @return the formeJuridique
     */
    @FieldXPath(value = "formeJuridique")
    String getFormeJuridique();

    /**
     * Get le forme juridique Libelle.
     *
     * @return the formeJuridique
     */
    @FieldXPath(value = "formeJuridiqueLibelle")
    String getFormeJuridiqueLibelle();

    /**
     * Set le forme juridique.
     *
     * @param formeJuridique
     *            the formeJuridique to set
     */
    void setFormeJuridique(String formeJuridique);

    /**
     * Mutateur sur l'attribut {@link #formeJuridiqueLibelle}.
     *
     * @param formeJuridiqueLibelle
     *            la nouvelle valeur de l'attribut formeJuridiqueLibelle
     */
    void setFormeJuridiqueLibelle(String formeJuridiqueLibelle);

    /**
     * Get le associe unique.
     *
     * @return the associeUnique
     */
    @FieldXPath(value = "associeUnique")
    String getAssocieUnique();

    /**
     * Set le associe unique.
     *
     * @param associeUnique
     *            the associeUnique to set
     */
    void setAssocieUnique(String associeUnique);

    /**
     * Get le personne liee.
     *
     * @return le personne liee
     */
    @FieldXPath(value = "personneLiee/personneLiee")
    IPersonneLieeCreation[] getPersonneLiee();

    /**
     * Ajoute le to personne liee.
     *
     * @return le i personne liee regularisation
     */
    IPersonneLieeCreation addToPersonneLiee();

    /**
     * Retire le from capitalApportsNature.
     *
     * @param capitalApportsNature
     *            le capitalApportsNature
     */
    void removeFromPersonneLiee(IPersonneLieeCreation personneLiee);

    /**
     * Set le personne liee.
     *
     * @param personneLiee
     *            le nouveau personne liee
     */
    void setPersonneLiee(IPersonneLieeCreation[] personneLiee);

    /**
     * Get le etablissement.
     *
     * @return le etablissement
     */
    @FieldXPath(value = "etablissement")
    IEtablissementCreation getEtablissement();

    /**
     * Set le etablissement.
     *
     * @param etablissement
     *            le nouveau etablissement
     */
    void setEtablissement(IEtablissementCreation etablissement);

    /**
     * New etablissement.
     *
     * @return le i etablissement modification
     */
    IEtablissementCreation newEtablissement();

    /**
     * Get le etablissements ue.
     *
     * @return le etablissements ue
     */
    @FieldXPath(value = "etablissementsUE/etablissementUE")
    IAutreEtablissementUECreation[] getEtablissementsUE();

    /**
     * Ajoute le to etablissements ue.
     *
     * @return le i autre etablissement ue
     */
    IAutreEtablissementUECreation addToEtablissementsUE();

    /**
     * Set le etablissements ue.
     *
     * @param etablissementsUE
     *            le nouveau etablissements ue
     */
    void setEtablissementsUE(IAutreEtablissementUECreation[] etablissementsUE);

    /**
     * Retire le from etablissements ue.
     *
     * @param etablissementsUE
     *            le etablissements ue
     */
    void removeFromEtablissementsUE(IAutreEtablissementUECreation etablissementsUE);

    /**
     * Get le dirigeants.
     *
     * @return le dirigeants
     */
    @FieldXPath(value = "dirigeants/dirigeant")
    IDirigeantCreation[] getDirigeants();

    /**
     * Ajoute le to dirigeants.
     *
     * @return le i dirigeant modification
     */
    IDirigeantCreation addToDirigeants();

    /**
     * Set le dirigeants.
     *
     * @param dirigeants
     *            le nouveau dirigeants
     */
    void setDirigeants(IDirigeantCreation[] dirigeants);

    /**
     * Retire le from dirigeants.
     *
     * @param dirigeants
     *            le dirigeants
     */
    void removeFromDirigeants(IDirigeantCreation dirigeants);

    /**
     * Get le exercice double activite.
     *
     * @return the exerciceDoubleActivite
     */
    @FieldXPath(value = "exerciceDoubleActivite")
    String getExerciceDoubleActivite();

    /**
     * Set le exercice double activite.
     *
     * @param exerciceDoubleActivite
     *            the exerciceDoubleActivite to set
     */
    void setExerciceDoubleActivite(String exerciceDoubleActivite);

    /**
     * Get le insaisissabilite declaration.
     *
     * @return the insaisissabiliteDeclaration
     */
    @FieldXPath(value = "insaisissabiliteDeclaration")
    String getInsaisissabiliteDeclaration();

    /**
     * Set le insaisissabilite declaration.
     *
     * @param insaisissabiliteDeclaration
     *            the insaisissabiliteDeclaration to set
     */
    void setInsaisissabiliteDeclaration(String insaisissabiliteDeclaration);

    /**
     * Get le insaisissabilite publication.
     *
     * @return the insaisissabilitePublication
     */
    @FieldXPath(value = "insaisissabilitePublication")
    String getInsaisissabilitePublication();

    /**
     * Set le insaisissabilite publication.
     *
     * @param insaisissabilitePublication
     *            the insaisissabilitePublication to set
     */
    void setInsaisissabilitePublication(String insaisissabilitePublication);

    /**
     * Get le insaisissabilite Renonciation RP.
     *
     * @return the insaisissabilite Renonciation RP
     */
    @FieldXPath(value = "insaisissabiliteRenonciationRP")
    String getInsaisissabiliteRenonciationRP();

    /**
     * Set le insaisissabilite Renonciation RP.
     *
     * @param insaisissabiliteRenonciationRP
     *            the insaisissabiliteRenonciationRP to set
     */
    void setInsaisissabiliteRenonciationRP(String insaisissabiliteRenonciationRP);

    /**
     * Get le insaisissabilite Declaration Autres Biens.
     *
     * @return the insaisissabiliteDeclarationAutresBiens
     */
    @FieldXPath(value = "insaisissabiliteDeclarationAutresBiens")
    String getInsaisissabiliteDeclarationAutresBiens();

    /**
     * Set le insaisissabilite Declaration Autres Biens.
     *
     * @param insaisissabiliteDeclarationAutresBiens
     *            the insaisissabiliteDeclarationAutresBiens to set
     */
    void setInsaisissabiliteDeclarationAutresBiens(String insaisissabiliteDeclarationAutresBiens);

    /**
     * Get le insaisissabilite Publication Renonciation RP.
     *
     * @return the insaisissabilitePublicationRenonciationRP
     */
    @FieldXPath(value = "insaisissabilitePublicationRenonciationRP")
    String getInsaisissabilitePublicationRenonciationRP();

    /**
     * Set le insaisissabilite Publication Renonciation RP.
     *
     * @param insaisissabilitePublicationRenonciationRP
     *            the insaisissabilitePublicationRenonciationRP to set
     */
    void setInsaisissabilitePublicationRenonciationRP(String insaisissabilitePublicationRenonciationRP);

    /**
     * Get le insaisissabilite Publication Declaration Autres Biens.
     *
     * @return the insaisissabilitePublicationDeclarationAutresBiens
     */
    @FieldXPath(value = "insaisissabilitePublicationDeclarationAutresBiens")
    String getInsaisissabilitePublicationDeclarationAutresBiens();

    /**
     * Set le insaisissabilite Publication Declaration Autres Biens.
     *
     * @param insaisissabilitePublicationDeclarationAutresBiens
     *            the insaisissabilitePublicationDeclarationAutresBiens to set
     */
    void setInsaisissabilitePublicationDeclarationAutresBiens(String insaisissabilitePublicationDeclarationAutresBiens);

    /**
     * Get le autre etablissement ue.
     *
     * @return the autreEtablissementUE
     */
    @FieldXPath(value = "autreEtablissementUE")
    String getAutreEtablissementUE();

    /**
     * Set le autre etablissement ue.
     *
     * @param autreEtablissementUE
     *            the autreEtablissementUE to set
     */
    void setAutreEtablissementUE(String autreEtablissementUE);

    /**
     * Get le autre etablissement ue nombre.
     *
     * @return the autreEtablissementUENombre
     */
    @FieldXPath(value = "autreEtablissementUENombre")
    Integer getAutreEtablissementUENombre();

    /**
     * Set le autre etablissement ue nombre.
     *
     * @param autreEtablissementUENombre
     *            the autreEtablissementUENombre to set
     */
    void setAutreEtablissementUENombre(Integer autreEtablissementUENombre);

    /**
     * Get le adresse entreprise pp situation.
     *
     * @return the adresseEntreprisePPSituation
     */
    @FieldXPath(value = "adresseEntreprisePPSituation")
    String getAdresseEntreprisePPSituation();

    /**
     * Set le adresse entreprise pp situation.
     *
     * @param adresseEntreprisePPSituation
     *            the adresseEntreprisePPSituation to set
     */
    void setAdresseEntreprisePPSituation(String adresseEntreprisePPSituation);

}
