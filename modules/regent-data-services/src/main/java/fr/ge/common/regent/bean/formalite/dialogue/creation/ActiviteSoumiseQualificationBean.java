/**
 * 
 */
package fr.ge.common.regent.bean.formalite.dialogue.creation;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import fr.ge.common.regent.bean.formalite.dialogue.AbstractActiviteSoumiseQualification;

/**
 * La Classe Activité soumise à qualification.
 * 
 * @author $Author: hhichri $
 * @version $Revision: 0 $
 */
public class ActiviteSoumiseQualificationBean extends AbstractActiviteSoumiseQualification implements IFormaliteVue {

    /** serialVersionUID. **/
    private static final long serialVersionUID = -2192830048368541109L;

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see fr.guichetentreprises.vue.AbstractDirigeantBean#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

}
