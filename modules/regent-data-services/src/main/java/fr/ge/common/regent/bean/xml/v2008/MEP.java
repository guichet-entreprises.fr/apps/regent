package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface NAP.
 */
@ResourceXPath("/MEP")
public interface MEP {

  /**
   * Get le P22.
   *
   * @return le P22
   */
  @FieldXPath("P22")
  String getP22();

  /**
   * Set le P22.
   *
   * @param P22
   *          le nouveau P22
   */
  void setP22(String P22);

}
