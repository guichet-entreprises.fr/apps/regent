package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface A22.
 */
@ResourceXPath("/A22")
public interface IA22 {

  /**
   * Get le A223.
   *
   * @return le A223
   */
  @FieldXPath("A22.3")
  String getA223();

  /**
   * Set le A223.
   *
   * @param A223
   *          le nouveau A223
   */
  void setA223(String A223);

  /**
   * Get le A224.
   *
   * @return le A224
   */
  @FieldXPath("A22.4")
  String getA224();

  /**
   * Set le A224.
   *
   * @param A224
   *          le nouveau A224
   */
  void setA224(String A224);

}
