/**
 * 
 */
package fr.ge.common.regent.bean.formalite.dialogue;

import fr.ge.common.regent.bean.formalite.dialogue.creation.IFormaliteVue;

/**
 * le bean InsaisissabiliteBean pour la rubrique ISP.
 * 
 * @author $Author: aboidard $
 * @version $Revision: 0 $
 */
public class InsaisissabiliteBean implements IFormaliteVue {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = -2044679694042782151L;

    /**
     * la modalite.
     */
    private String modalite;

    /**
     * le lieu de la modalite.
     */
    private String lieu;

    /**
     * Accesseur sur l'attribut {@link #modalite}.
     *
     * @return String modalite
     */
    public String getModalite() {
        return modalite;
    }

    /**
     * Mutateur sur l'attribut {@link #modalite}.
     *
     * @param modalite
     *            la nouvelle valeur de l'attribut modalite
     */
    public void setModalite(final String modalite) {
        this.modalite = modalite;
    }

    /**
     * Accesseur sur l'attribut {@link #lieu}.
     *
     * @return String lieu
     */
    public String getLieu() {
        return lieu;
    }

    /**
     * Mutateur sur l'attribut {@link #lieu}.
     *
     * @param lieu
     *            la nouvelle valeur de l'attribut lieu
     */
    public void setLieu(final String lieu) {
        this.lieu = lieu;
    }
}
