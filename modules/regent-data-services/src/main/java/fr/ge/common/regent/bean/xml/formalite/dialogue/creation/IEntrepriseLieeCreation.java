package fr.ge.common.regent.bean.xml.formalite.dialogue.creation;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

import fr.ge.common.regent.bean.xml.formalite.IFormalite;
import fr.ge.common.regent.bean.xml.transverse.IAdresse;

/**
 * Le Interface de l'entité entreprise liées à l'entreprise
 * 
 * 
 * 
 */
@ResourceXPath("/formalite")
public interface IEntrepriseLieeCreation extends IFormalite {

    /**
     * String getNumeroDetenteur() { return this.numeroDetenteur; }
     * 
     * /** Getter de l'attribut numeroExploitation.
     * 
     * @return la valeur de numeroExploitation
     */
    @FieldXPath(value = "numeroExploitation")
    String getNumeroExploitation();

    /**
     * Getter de l'attribut numeroDetenteur.
     * 
     * @return la valeur de numeroDetenteur
     */
    @FieldXPath(value = "numeroDetenteur")
    String getNumeroDetenteur();

    /**
     * Get le nom.
     *
     * @return the nom
     */
    @FieldXPath(value = "nom")
    String getNom();

    /**
     * Set le nom.
     *
     * @param nom
     *            the nom to set
     */
    void setNom(String nom);

    /**
     * Get le adresse.
     *
     * @return le adresse
     */
    @FieldXPath(value = "adresse")
    IAdresse getAdresse();

    /**
     * New adresse.
     *
     * @return le i adresse
     */
    IAdresse newAdresse();

    /**
     * Set le adresse.
     *
     * @param adresse
     *            le nouveau adresse
     */
    void setAdresse(IAdresse adresse);

    /**
     * Getter de l'attribut domiciliataireMemeGreffe.
     * 
     * @return la valeur de domiciliataireMemeGreffe
     */
    @FieldXPath(value = "domiciliataireMemeGreffe")
    String getDomiciliataireMemeGreffe();

    /**
     * Getter de l'attribut entreprisePMDenomination.
     * 
     * @return la valeur de entreprisePMDenomination
     */
    @FieldXPath(value = "entreprisePMDenomination")
    String getEntreprisePMDenomination();

    /**
     * Getter de l'attribut entreprisePMFormeJuridique.
     * 
     * @return la valeur de entreprisePMFormeJuridique
     */
    @FieldXPath(value = "entreprisePMFormeJuridique")
    String getEntreprisePMFormeJuridique();

    /**
     * Getter de l'attribut entreprisePPNomNaissance.
     * 
     * @return la valeur de entreprisePPNomNaissance
     */
    @FieldXPath(value = "entreprisePPNomNaissance")
    String getEntreprisePPNomNaissance();

    /**
     * Getter de l'attribut entreprisePPNomUsage.
     * 
     * @return la valeur de entreprisePPNomUsage
     */
    @FieldXPath(value = "entreprisePPNomUsage")
    String getEntreprisePPNomUsage();

    /**
     * Getter de l'attribut entreprisePPPrenom1.
     * 
     * @return la valeur de entreprisePPPrenom1
     */
    @FieldXPath(value = "entreprisePPPrenom1")
    String getEntreprisePPPrenom1();

    /**
     * Getter de l'attribut entreprisePPPrenom2.
     * 
     * @return la valeur de entreprisePPPrenom2
     */
    @FieldXPath(value = "entreprisePPPrenom2")
    String getEntreprisePPPrenom2();

    /**
     * Getter de l'attribut entreprisePPPrenom3.
     * 
     * @return la valeur de entreprisePPPrenom3
     */
    @FieldXPath(value = "entreprisePPPrenom3")
    String getEntreprisePPPrenom3();

    /**
     * Getter de l'attribut entreprisePPPrenom4.
     * 
     * @return la valeur de entreprisePPPrenom4
     */
    @FieldXPath(value = "entreprisePPPrenom4")
    String getEntreprisePPPrenom4();

    /**
     * Getter de l'attribut greffeImmatriculation.
     * 
     * @return la valeur de greffeImmatriculation
     */
    @FieldXPath(value = "greffeImmatriculation")
    String getGreffeImmatriculation();

    /**
     * Getter de l'attribut nature.
     * 
     * @return la valeur de nature
     */
    @FieldXPath(value = "nature")
    String getNature();

    /**
     * getNomDomiciliataire.
     * 
     * @return nomDomiciliataire
     */
    @FieldXPath(value = "nomDomiciliataire")
    String getNomDomiciliataire();

    /**
     * Getter de l'attribut siren.
     * 
     * @return la valeur de siren
     */
    @FieldXPath(value = "siren")
    String getSiren();

    /**
     * Setter de l'attribut domiciliataireMemeGreffe.
     * 
     * @param domiciliataireMemeGreffe
     *            la nouvelle valeur de domiciliataireMemeGreffe
     */
    void setDomiciliataireMemeGreffe(String domiciliataireMemeGreffe);

    /**
     * Setter de l'attribut entreprisePMDenomination.
     * 
     * @param entreprisePMDenomination
     *            la nouvelle valeur de entreprisePMDenomination
     */
    void setEntreprisePMDenomination(String entreprisePMDenomination);

    /**
     * Setter de l'attribut entreprisePMFormeJuridique.
     * 
     * @param entreprisePMFormeJuridique
     *            la nouvelle valeur de entreprisePMFormeJuridique
     */
    void setEntreprisePMFormeJuridique(String entreprisePMFormeJuridique);

    /**
     * Setter de l'attribut entreprisePPNomNaissance.
     * 
     * @param entreprisePPNomNaissance
     *            la nouvelle valeur de entreprisePPNomNaissance
     */
    void setEntreprisePPNomNaissance(String entreprisePPNomNaissance);

    /**
     * Setter de l'attribut entreprisePPNomUsage.
     * 
     * @param entreprisePPNomUsage
     *            la nouvelle valeur de entreprisePPNomUsage
     */
    void setEntreprisePPNomUsage(String entreprisePPNomUsage);

    /**
     * Setter de l'attribut entreprisePPPrenom1.
     * 
     * @param entreprisePPPrenom1
     *            la nouvelle valeur de entreprisePPPrenom1
     */
    void setEntreprisePPPrenom1(String entreprisePPPrenom1);

    /**
     * Setter de l'attribut entreprisePPPrenom2.
     * 
     * @param entreprisePPPrenom2
     *            la nouvelle valeur de entreprisePPPrenom2
     */
    void setEntreprisePPPrenom2(String entreprisePPPrenom2);

    /**
     * Setter de l'attribut entreprisePPPrenom3.
     * 
     * @param entreprisePPPrenom3
     *            la nouvelle valeur de entreprisePPPrenom3
     */
    void setEntreprisePPPrenom3(String entreprisePPPrenom3);

    /**
     * Setter de l'attribut entreprisePPPrenom4.
     * 
     * @param entreprisePPPrenom4
     *            la nouvelle valeur de entreprisePPPrenom4
     */
    void setEntreprisePPPrenom4(String entreprisePPPrenom4);

    /**
     * Setter de l'attribut greffeImmatriculation.
     * 
     * @param greffeImmatriculation
     *            la nouvelle valeur de greffeImmatriculation
     */
    void setGreffeImmatriculation(String greffeImmatriculation);

    /**
     * Setter de l'attribut nature.
     * 
     * @param nature
     *            la nouvelle valeur de nature
     */
    void setNature(String nature);

    /**
     * setNomDomiciliataire.
     * 
     * @param nomDomiciliataire
     *            nomDomiciliataire
     */
    void setNomDomiciliataire(String nomDomiciliataire);

    /**
     * Setter de l'attribut siren.
     * 
     * @param siren
     *            la nouvelle valeur de siren
     */
    void setSiren(String siren);

    /**
     * Setter de l'attribut numeroDetenteur.
     * 
     * @param numeroDetenteur
     *            la nouvelle valeur de numeroDetenteur
     */
    void setNumeroDetenteur(String numeroDetenteur);

    /**
     * Setter de l'attribut numeroExploitation.
     * 
     * @param numeroExploitation
     *            la nouvelle valeur de numeroExploitation
     */
    void setNumeroExploitation(String numeroExploitation);
}
