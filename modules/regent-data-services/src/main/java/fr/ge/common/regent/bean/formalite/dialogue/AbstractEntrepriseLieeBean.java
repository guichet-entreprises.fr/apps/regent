package fr.ge.common.regent.bean.formalite.dialogue;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import fr.ge.common.regent.bean.formalite.FormaliteVueBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.AdresseBean;

/**
 * Le Class AbstractEntrepriseLieeBean.
 *
 * @author roveda
 */
public abstract class AbstractEntrepriseLieeBean extends FormaliteVueBean {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -8811514009462322002L;

    /** Le nature. */
    private String nature;

    /** Le entreprise pp nom naissance. */
    private String entreprisePPNomNaissance;

    /** Le entreprise pp nom usage. */
    private String entreprisePPNomUsage;

    /** Le entreprise pp prenom1. */
    private String entreprisePPPrenom1;

    /** Le entreprise pp prenom2. */
    private String entreprisePPPrenom2;

    /** Le entreprise pp prenom3. */
    private String entreprisePPPrenom3;

    /** Le entreprise pp prenom4. */
    private String entreprisePPPrenom4;

    /** Le entreprise pm denomination. */
    private String entreprisePMDenomination;

    /** Le entreprise pm forme juridique. */
    private String entreprisePMFormeJuridique;

    /** Le nom domiciliataire. */
    private String nomDomiciliataire;

    /** Le adresse. */
    private AdresseBean adresse = new AdresseBean();

    /** Le siren. */
    private String siren;

    /** Le greffe immatriculation. */
    private String greffeImmatriculation;

    /** Le domiciliataire meme greffe. */
    private String domiciliataireMemeGreffe;

    /** Le type occurence. */
    private String typeOccurence;

    /** Le nom. */
    private String nom;

    /**
     * Get le nom.
     *
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * Set le nom.
     *
     * @param nom
     *            the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Getter de l'attribut adresse.
     * 
     * @return la valeur de adresse
     */
    public AdresseBean getAdresse() {
        return this.adresse;
    }

    /**
     * Getter de l'attribut domiciliataireMemeGreffe.
     * 
     * @return la valeur de domiciliataireMemeGreffe
     */
    public String getDomiciliataireMemeGreffe() {
        return this.domiciliataireMemeGreffe;
    }

    /**
     * Getter de l'attribut entreprisePMDenomination.
     * 
     * @return la valeur de entreprisePMDenomination
     */
    public String getEntreprisePMDenomination() {
        return this.entreprisePMDenomination;
    }

    /**
     * Getter de l'attribut entreprisePMFormeJuridique.
     * 
     * @return la valeur de entreprisePMFormeJuridique
     */
    public String getEntreprisePMFormeJuridique() {
        return this.entreprisePMFormeJuridique;
    }

    /**
     * Getter de l'attribut entreprisePPNomNaissance.
     * 
     * @return la valeur de entreprisePPNomNaissance
     */
    public String getEntreprisePPNomNaissance() {
        return this.entreprisePPNomNaissance;
    }

    /**
     * Getter de l'attribut entreprisePPNomUsage.
     * 
     * @return la valeur de entreprisePPNomUsage
     */
    public String getEntreprisePPNomUsage() {
        return this.entreprisePPNomUsage;
    }

    /**
     * Getter de l'attribut entreprisePPPrenom1.
     * 
     * @return la valeur de entreprisePPPrenom1
     */
    public String getEntreprisePPPrenom1() {
        return this.entreprisePPPrenom1;
    }

    /**
     * Getter de l'attribut entreprisePPPrenom2.
     * 
     * @return la valeur de entreprisePPPrenom2
     */
    public String getEntreprisePPPrenom2() {
        return this.entreprisePPPrenom2;
    }

    /**
     * Getter de l'attribut entreprisePPPrenom3.
     * 
     * @return la valeur de entreprisePPPrenom3
     */
    public String getEntreprisePPPrenom3() {
        return this.entreprisePPPrenom3;
    }

    /**
     * Getter de l'attribut entreprisePPPrenom4.
     * 
     * @return la valeur de entreprisePPPrenom4
     */
    public String getEntreprisePPPrenom4() {
        return this.entreprisePPPrenom4;
    }

    /**
     * Getter de l'attribut greffeImmatriculation.
     * 
     * @return la valeur de greffeImmatriculation
     */
    public String getGreffeImmatriculation() {
        return this.greffeImmatriculation;
    }

    /**
     * Getter de l'attribut nature.
     * 
     * @return la valeur de nature
     */
    public String getNature() {
        return this.nature;
    }

    /**
     * getNomDomiciliataire.
     * 
     * @return nomDomiciliataire
     */
    public String getNomDomiciliataire() {
        return nomDomiciliataire;
    }

    /**
     * Getter de l'attribut siren.
     * 
     * @return la valeur de siren
     */
    public String getSiren() {
        return this.siren;
    }

    /**
     * Setter de l'attribut adresse.
     * 
     * @param adresse
     *            la nouvelle valeur de adresse
     */
    public void setAdresse(AdresseBean adresse) {
        this.adresse = adresse;
    }

    /**
     * Setter de l'attribut domiciliataireMemeGreffe.
     * 
     * @param domiciliataireMemeGreffe
     *            la nouvelle valeur de domiciliataireMemeGreffe
     */
    public void setDomiciliataireMemeGreffe(String domiciliataireMemeGreffe) {
        this.domiciliataireMemeGreffe = domiciliataireMemeGreffe;
    }

    /**
     * Setter de l'attribut entreprisePMDenomination.
     * 
     * @param entreprisePMDenomination
     *            la nouvelle valeur de entreprisePMDenomination
     */
    public void setEntreprisePMDenomination(String entreprisePMDenomination) {
        this.entreprisePMDenomination = entreprisePMDenomination;
    }

    /**
     * Setter de l'attribut entreprisePMFormeJuridique.
     * 
     * @param entreprisePMFormeJuridique
     *            la nouvelle valeur de entreprisePMFormeJuridique
     */
    public void setEntreprisePMFormeJuridique(String entreprisePMFormeJuridique) {
        this.entreprisePMFormeJuridique = entreprisePMFormeJuridique;
    }

    /**
     * Setter de l'attribut entreprisePPNomNaissance.
     * 
     * @param entreprisePPNomNaissance
     *            la nouvelle valeur de entreprisePPNomNaissance
     */
    public void setEntreprisePPNomNaissance(String entreprisePPNomNaissance) {
        this.entreprisePPNomNaissance = entreprisePPNomNaissance;
    }

    /**
     * Setter de l'attribut entreprisePPNomUsage.
     * 
     * @param entreprisePPNomUsage
     *            la nouvelle valeur de entreprisePPNomUsage
     */
    public void setEntreprisePPNomUsage(String entreprisePPNomUsage) {
        this.entreprisePPNomUsage = entreprisePPNomUsage;
    }

    /**
     * Setter de l'attribut entreprisePPPrenom1.
     * 
     * @param entreprisePPPrenom1
     *            la nouvelle valeur de entreprisePPPrenom1
     */
    public void setEntreprisePPPrenom1(String entreprisePPPrenom1) {
        this.entreprisePPPrenom1 = entreprisePPPrenom1;
    }

    /**
     * Setter de l'attribut entreprisePPPrenom2.
     * 
     * @param entreprisePPPrenom2
     *            la nouvelle valeur de entreprisePPPrenom2
     */
    public void setEntreprisePPPrenom2(String entreprisePPPrenom2) {
        this.entreprisePPPrenom2 = entreprisePPPrenom2;
    }

    /**
     * Setter de l'attribut entreprisePPPrenom3.
     * 
     * @param entreprisePPPrenom3
     *            la nouvelle valeur de entreprisePPPrenom3
     */
    public void setEntreprisePPPrenom3(String entreprisePPPrenom3) {
        this.entreprisePPPrenom3 = entreprisePPPrenom3;
    }

    /**
     * Setter de l'attribut entreprisePPPrenom4.
     * 
     * @param entreprisePPPrenom4
     *            la nouvelle valeur de entreprisePPPrenom4
     */
    public void setEntreprisePPPrenom4(String entreprisePPPrenom4) {
        this.entreprisePPPrenom4 = entreprisePPPrenom4;
    }

    /**
     * Setter de l'attribut greffeImmatriculation.
     * 
     * @param greffeImmatriculation
     *            la nouvelle valeur de greffeImmatriculation
     */
    public void setGreffeImmatriculation(String greffeImmatriculation) {
        this.greffeImmatriculation = greffeImmatriculation;
    }

    /**
     * Setter de l'attribut nature.
     * 
     * @param nature
     *            la nouvelle valeur de nature
     */
    public void setNature(String nature) {
        this.nature = nature;
    }

    /**
     * setNomDomiciliataire.
     * 
     * @param nomDomiciliataire
     *            nomDomiciliataire
     */
    public void setNomDomiciliataire(String nomDomiciliataire) {
        this.nomDomiciliataire = nomDomiciliataire;
    }

    /**
     * Setter de l'attribut siren.
     * 
     * @param siren
     *            la nouvelle valeur de siren
     */
    public void setSiren(String siren) {
        this.siren = siren;
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    /**
     * Get le type occurence.
     *
     * @return the typeOccurence
     */
    public String getTypeOccurence() {
        return typeOccurence;
    }

    /**
     * Set le type occurence.
     *
     * @param typeOccurence
     *            the typeOccurence to set
     */
    public void setTypeOccurence(String typeOccurence) {
        this.typeOccurence = typeOccurence;
    }

}
