package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Interface IP953.
 */
@ResourceXPath("/P95.3")
public interface IP953 {
	  /**
	   * Get le P9533.
	   *
	   * @return le P9533
	   */
	  @FieldXPath("P95.3.3")
	  String getP9533();

	  /**
	   * Set le P9533.
	   *
	   * @param P9533
	   *          le nouveau P9533
	   */
	  void setP9533(String P9533);

	  /**
	   * Get le P9534.
	   *
	   * @return le P9534
	   */
	  @FieldXPath("P95.3.4")
	  String getP9534();

	  /**
	   * Set le P9534.
	   *
	   * @param P9534
	   *          le nouveau P9534
	   */
	  void setP9534(String P9534);

	  /**
	   * Get le P9535.
	   *
	   * @return le P9535
	   */
	  @FieldXPath("P95.3.5")
	  String getP9535();

	  /**
	   * Set le P9535.
	   *
	   * @param P9535
	   *          le nouveau P9535
	   */
	  void setP9535(String P9535);

	  /**
	   * Get le P9536.
	   *
	   * @return le P9536
	   */
	  @FieldXPath("P95.3.6")
	  String getP9536();

	  /**
	   * Set le P9536.
	   *
	   * @param P9536
	   *          le nouveau P9536
	   */
	  void setP9536(String P9536);

	  /**
	   * Get le P9537.
	   *
	   * @return le P9537
	   */
	  @FieldXPath("P95.3.7")
	  String getP9537();

	  /**
	   * Set le P9537.
	   *
	   * @param P9537
	   *          le nouveau P9537
	   */
	  void setP9537(String P9537);

	  /**
	   * Get le P9538.
	   *
	   * @return le P9538
	   */
	  @FieldXPath("P95.3.8")
	  String getP9538();

	  /**
	   * Set le P9538.
	   *
	   * @param P9538
	   *          le nouveau P9538
	   */
	  void setP9538(String P9538);

	  /**
	   * Get le P9539.
	   *
	   * @return le P9539
	   */
	  @FieldXPath("P95.3.9")
	  String getP9539();

	  /**
	   * Set le P9539.
	   *
	   * @param P9539
	   *          le nouveau P9539
	   */
	  void setP9539(String P9539);

	  /**
	   * Get le P95310.
	   *
	   * @return le P95310
	   */
	  @FieldXPath("P95.3.10")
	  String getP95310();

	  /**
	   * Set le P95310.
	   *
	   * @param P95310
	   *          le nouveau P95310
	   */
	  void setP95310(String P95310);

	  /**
	   * Get le P95311.
	   *
	   * @return le P95311
	   */
	  @FieldXPath("P95.3.11")
	  String getP95311();

	  /**
	   * Set le P95311.
	   *
	   * @param P95311
	   *          le nouveau P95311
	   */
	  void setP95311(String P95311);

	  /**
	   * Get le P95312.
	   *
	   * @return le P95312
	   */
	  @FieldXPath("P95.3.12")
	  String getP95312();

	  /**
	   * Set le P95312.
	   *
	   * @param P95312
	   *          le nouveau P95312
	   */
	  void setP95312(String P95312);

	  /**
	   * Get le P95313.
	   *
	   * @return le P95313
	   */
	  @FieldXPath("P95.3.13")
	  String getP95313();

	  /**
	   * Set le P95313.
	   *
	   * @param P95313
	   *          le nouveau P95313
	   */
	  void setP95313(String P95313);

	  /**
	   * Get le P95314.
	   *
	   * @return le P95314
	   */
	  @FieldXPath("P95.3.14")
	  String getP95314();

	  /**
	   * Set le P95314.
	   *
	   * @param P95314
	   *          le nouveau P95314
	   */
	  void setP95314(String P95314);


}
