package fr.ge.common.regent.context.function;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.referentiel.bean.modele.ESupraActivite;
import fr.ge.common.referentiel.bean.modele.ESupraCFE;
import fr.ge.common.referentiel.bean.modele.ESupraCFECommune;
import fr.ge.common.referentiel.bean.modele.ESupraCommunes;
import fr.ge.common.referentiel.bean.modele.ESupraDepartements;
import fr.ge.common.referentiel.constante.ReseauCFEEnum;
import fr.ge.common.referentiel.service.ReferentielSupraService;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * Le Class FonctionsPdf.
 */
public class FonctionPdf {

    /** Nature Personne Physique. **/
    private static final String NATURE_PERSONNE_PHYSIQUE = "1";

    /** La constante COMMUNE_ARR. */
    private static final String COMMUNE_ARR = "commune-arr";

    /** Le logger fonctionnel. */
    private static final Logger LOGGER_FONC = LoggerFactory.getLogger(FonctionPdf.class);

    /** Le service. */
    private static ReferentielSupraService referentielSupraService = null;

    /** La constante REF_TYPE_COMMUNE. */
    private static final String REF_TYPE_COMMUNE = "commune";

    /** La constante REF_TYPE_PAYS. */
    private static final String REF_TYPE_PAYS = "pays";

    /** La constante REF_TYPE_PAYS. */
    private static final String REF_TYPE_PAYS_INSSE = "pays-insee";

    /** La constante REF_TYPE_DEPARTEMENTS. */
    private static final String REF_TYPE_DEPARTEMENTS = "departement";

    /** La constante REF_TYPE_PREFECTURE_CODE. */
    private static final String REF_TYPE_PREFECTURE_CODE = "prefectureCode";

    /** La constante REF_TYPE_PREFECTURE_LIBELLE. */
    private static final String REF_TYPE_PREFECTURE_LIBELLE = "prefectureLibelle";

    /** La constante REF_TYPE_PREFECTURE_LIBELLE. */
    private static final String REF_TYPE_PREFECTURE_LIBELLE_NR = "prefectureLibelleNR";

    /** La constante REF_TYPE_VOIE. */
    private static final String REF_TYPE_VOIE = "typeVoie";

    /** La constante REF_ACTIVITE. */
    private static final String REF_ACTIVITE = "activite";

    /** La constante SEPARATOR. */
    private static final String SEPARATOR = " ";

    /** La constante SIGNATURE. */
    private static final String SIGNATURE = "Mention <<Cette déclaration respecte" + '\n' + "les attendus de l'article A123-4" + '\n' + "du code de commerce>>";

    /** La constante M. */
    public static final String M = "M";

    /** La constante P. */
    public static final String P = "P";

    /** Aide familial. */
    public static final String AIDE_FAMILIAL = "aideFamilial";

    /** Associé exploitation. */
    public static final String ASSOCIE_EXPLOITATION = "associeExploitation";

    /**
     * Mapping entre l'id du bean (unique) et son map correspondant au nombre de
     * caractères écrit dans une ligne.
     */
    private static Map<Integer, HashMap<Integer, Integer>> objectIdMap = new HashMap<Integer, HashMap<Integer, Integer>>();

    /** La constante MAP_INDICE_REPETITION. */
    private static final Map<String, String> MAP_INDICE_REPETITION = createMap();

    /**
     * Cree le map.
     * 
     * @return le map
     */
    private static Map<String, String> createMap() {
        Map<String, String> result = new HashMap<String, String>();
        result.put("B", "Bis");
        result.put("T", "Ter");
        result.put("Q", "Quatre");
        result.put("C", "cinq");
        return Collections.unmodifiableMap(result);
    }

    /**
     * I Doit être instancié via Spring pour fonctionner.
     */
    public FonctionPdf() {

    }

    /**
     * Recuperer nationalite.
     * 
     * @param codeNationaliteTronque
     *            le code nationalite tronque
     * @return le string
     * @throws TechnicalException
     */
    public static String recupererNationalite(String codeNationaliteTronque) throws TechnicalException {
        return referentielSupraService.recupererNationalite(codeNationaliteTronque);
    }

    /**
     * cette fonction doit permettre de calculer le code EDI d'un destinataire à
     * partir d'un code commune et pour un réseau donné (le code réseau est sur
     * un caractère : 'G' pour les greffes, 'C' pour les CCI, 'M' pour les CMA,
     * 'U' pour les Urssaf...)
     * 
     * @param codeReseau
     *            exp:'G' pour les greffes
     * @param codeCommune
     *            exp:42000
     * @return le string
     * @throws SecurityException
     *             le security exception
     * @throws NoSuchMethodException
     *             le no such method exception
     * @throws IllegalArgumentException
     *             le illegal argument exception
     * @throws IllegalAccessException
     *             le illegal access exception
     * @throws InvocationTargetException
     *             le invocation target exception
     */
    public static String refCodeEdi(String codeReseau, String codeCommune)
            throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        if (StringUtils.isNoneEmpty(codeReseau, codeCommune)) {

            ESupraCFECommune supraCfeCommune = referentielSupraService.getCFECommune(codeCommune);
            ReseauCFEEnum reseauCfe = ReseauCFEEnum.recupererParCode(codeReseau);
            String getterName = "getCfe" + StringUtils.capitalize(reseauCfe.getNom());
            Method method = supraCfeCommune.getClass().getMethod(getterName);
            String codeEdi = new String((String) method.invoke(supraCfeCommune));

            return codeEdi;
        } else {
            return null;
        }
    }

    /**
     * Retourne une nouvelle chaîne de caractère contenu dans l'objet bean. La
     * nouvelle chaîne commence à l'index <code>startIndex</code> et fini à
     * l'index <code>endIndex-1</code>.<br>
     * Exemples :<br>
     * soit <code>formalite[autorisation.telephone] = 0123456789</code><br>
     * alors <code>#substring(formalite[autorisation.telephone], 0, 1)</code>
     * retourne <code>0</code> <br>
     * et <code>#substring(formalite[autorisation.telephone], 2, 3)</code>
     * retourne <code>2</code><br>
     * 
     * @param bean
     *            string à couper
     * @param startIndex
     *            index de début
     * @param endIndex
     *            index de fin
     * @return le string
     */
    public static String substring(Object bean, int startIndex, int endIndex) {
        if (bean == null || StringUtils.EMPTY.equals(bean.toString())) {
            return StringUtils.EMPTY;
        }
        if (bean.toString().length() < endIndex) {
            return bean.toString().substring(startIndex, bean.toString().length());
        }
        return bean.toString().substring(startIndex, endIndex);
    }

    /**
     * Cette fonction doit permettre de déterminer le code département à partir
     * du code commune. La table du référentiel supra "communes" permet de faire
     * ce lien
     * 
     * @param codeCommune
     *            le code de la commune
     * @return le code du departement
     */
    public static String refDepartement(String codeCommune) {
        return referentielSupraService.chercherDepartementParCodeCommune(codeCommune);
    }

    /**
     * Ref to string.
     * 
     * @param typeRef
     *            le type ref
     * @param idRef
     *            le id ref
     * @return le string
     */
    public static String toString(String typeRef) {
        return typeRef.toString();

    }

    /**
     * Ref to string.
     * 
     * @param typeRef
     *            le type ref
     * @param idRef
     *            le id ref
     * @return le string
     */
    public static String refToString(String typeRef, Object idRef) {
        if (idRef == null || StringUtils.EMPTY.equals(typeRef)) {
            return StringUtils.EMPTY;
        }
        String idRefAsString = idRef.toString();
        if (REF_TYPE_COMMUNE.equals(typeRef)) {
            if (!StringUtils.EMPTY.equals(idRefAsString)) {
                // trace si code pays différent de la France
                if (idRefAsString.startsWith("99")) {
                    LOGGER_FONC.info("Ici, le code commune : " + idRefAsString + " représente un codePays étranger ou la commune de MONACO");
                }
                ESupraCommunes commune = referentielSupraService.getCommune(idRefAsString);
                if (commune != null) {
                    return commune.getElementLabel();
                } else {
                    LOGGER_FONC.warn("Le code commune : " + idRefAsString + " ne retourne aucun résultat");
                }
            } else {
                LOGGER_FONC.warn("Le code commune passé pour récupérer le libellé vaut une chaine vide");
            }
            return StringUtils.EMPTY;
        }
        if (REF_TYPE_PAYS.equals(typeRef)) {
            return referentielSupraService.getPays(idRefAsString).getElementLabel();
        }
        if (REF_TYPE_PAYS_INSSE.equals(typeRef)) {
            return referentielSupraService.getPays(idRefAsString).getLibInsee();
        }
        if (REF_TYPE_DEPARTEMENTS.equals(typeRef)) {
            return referentielSupraService.getDepartement(idRefAsString).getElementLabel();

        }
        if (REF_TYPE_VOIE.equals(typeRef)) {
            return referentielSupraService.getTypesVoieParCode(idRefAsString);
        }
        if (COMMUNE_ARR.equals(typeRef)) {
            if (!StringUtils.EMPTY.equals(idRefAsString)) {
                // trace si code pays différent de la France
                if (idRefAsString.startsWith("99")) {
                    LOGGER_FONC.info("Ici, le code commune : " + idRefAsString + " représente un codePays étranger ou la commune de MONACO");
                }
                ESupraCommunes commune = referentielSupraService.getCommune(idRefAsString);
                if (commune != null) {
                    return commune.getLibelleComplet();
                } else {
                    LOGGER_FONC.warn("Le code commune : " + idRefAsString + " ne retourne aucun résultat");
                }
            } else {
                LOGGER_FONC.warn("Le code commune passé pour récupérer le libellé vaut une chaine vide");
            }
            return StringUtils.EMPTY;
        }
        if (REF_TYPE_PREFECTURE_CODE.equals(typeRef)) {
            if (!StringUtils.EMPTY.equals(idRefAsString)) {
                ESupraDepartements prefecture = referentielSupraService.getDepartement(idRefAsString);
                if (null != prefecture && null != prefecture.getCodeChefLieu()) {
                    ESupraCommunes commune = referentielSupraService.getCommune(prefecture.getCodeChefLieu());
                    if (commune != null) {
                        return commune.getElementId();
                    }
                }
            }
            return StringUtils.EMPTY;
        }
        if (REF_TYPE_PREFECTURE_LIBELLE.equals(typeRef)) {
            if (!StringUtils.EMPTY.equals(idRefAsString)) {
                ESupraDepartements prefecture = referentielSupraService.getDepartement(idRefAsString);
                if (null != prefecture && null != prefecture.getCodeChefLieu()) {
                    ESupraCommunes commune = referentielSupraService.getCommune(prefecture.getCodeChefLieu());
                    if (commune != null) {
                        return commune.getLibelle();
                    }
                }
            }
        }
        if (REF_TYPE_PREFECTURE_LIBELLE_NR.equals(typeRef)) {
            if (!StringUtils.EMPTY.equals(idRefAsString)) {
                ESupraDepartements prefecture = referentielSupraService.getDepartement(idRefAsString);
                if (null != prefecture && null != prefecture.getCodeChefLieu()) {
                    ESupraCommunes commune = referentielSupraService.getCommune(prefecture.getCodeChefLieu());
                    if (commune != null) {
                        return commune.getLibelleComplet();
                    }
                }
            }
        }
        if (REF_ACTIVITE.equals(typeRef)) {
            ESupraActivite activite = referentielSupraService.getActivite(idRefAsString);
            String result = activite.getElementLabel();
            return result;
        }
        return StringUtils.EMPTY;
    }

    /**
     * Méthode qui fait le retour à la ligne.
     * 
     * @return '\n'
     */
    public static String retourLigne() {
        return "\n";
    }

    /**
     * Recuperer libelle commune.
     * 
     * @param commune
     *            le commune
     * @return le string
     */
    public static String recupererLibelleCommune(String commune) {
        ESupraCommunes eCommune = referentielSupraService.getCommune(commune);
        if (commune != null) {
            return eCommune.getElementLabel();
        }
        return StringUtils.EMPTY;
    }

    /**
     * Get le libelle edi.
     * 
     * @param codeEdi
     *            le code edi
     * @return le libelle edi
     */
    public static String getLibelleEdi(String codeEdi) {
        ESupraCFE cfe = referentielSupraService.getCFE(codeEdi);
        if (cfe != null) {
            return cfe.getNom();
        }
        return StringUtils.EMPTY;
    }

    /**
     * setter.
     * 
     * @param referentielSupraService
     *            le nouveau ref service
     */
    @Autowired
    public void setReferentielSupraService(ReferentielSupraService referentielSupraService) {
        FonctionPdf.referentielSupraService = referentielSupraService;
    }

}