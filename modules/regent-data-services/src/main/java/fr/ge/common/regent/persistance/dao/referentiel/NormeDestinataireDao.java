package fr.ge.common.regent.persistance.dao.referentiel;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import fr.ge.common.regent.bean.modele.referentiel.ENormeDestinataire;
import fr.ge.common.regent.persistance.dao.AbstractDao;

/**
 * Le Class NormeDestinataireDao.
 */
public class NormeDestinataireDao extends AbstractDao < ENormeDestinataire, String > {

  /**
   * Instancie un nouveau norme destinataire dao.
   *
   * @param type
   *          le type
   */
  public NormeDestinataireDao(Class < ENormeDestinataire > type) {
    super(type);
  }

  /**
   * Get le rubrique by role and destinataire.
   *
   * @param role
   *          le role
   * @param destinataire
   *          le destinataire
   * @return le rubrique by role and destinataire
   */
  public List < String > getRubriqueByRoleAndDestinataire(String role, String destinataire) {

    Criteria criteria = this.getSession().createCriteria(this.getClazz());
    criteria.add(Restrictions.eq("role", role));
    criteria.add(Restrictions.eq("destinataire", destinataire));
    criteria.setProjection(Projections.property("rubrique"));

    return criteria.list();

  }

}
