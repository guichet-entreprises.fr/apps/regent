/**
 * 
 */
package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * L'interface IM55.
 */
@ResourceXPath("/M55")
public interface IM55 {

  /**
   * Get le m551.
   *
   * @return le m551
   */
  @FieldXPath("M55.1")
  String getM551();

  /**
   * Set le m551.
   *
   * @param m551
   *          le nouveau m551
   */
  void setM551(String m551);

  /**
   * Get le m552.
   *
   * @return le m552
   */
  @FieldXPath("M55.2")
  String getM552();

  /**
   * Set le m552.
   *
   * @param m552
   *          le nouveau m552
   */
  void setM552(String m552);

  /**
   * Get le m553.
   *
   * @return le m553
   */
  @FieldXPath("M55.3")
  IM553[] getM553();

  /**
   * Ajoute le to m553.
   *
   * @return le m553
   */
  IM553 addToM553();

  /**
   * Set le m553.
   *
   * @param m553
   *          le m553
   */
  void setM553(IM553[] m553);

  /**
   * Get le m554.
   *
   * @return le m554
   */
  @FieldXPath("M55.4")
  String getM554();

  /**
   * Set le m554.
   *
   * @param m554
   *          le nouveau m554
   */
  void setM554(String m554);

  /**
   * Get le m556.
   *
   * @return le m556
   */
  @FieldXPath("M55.6")
  String getM556();

  /**
   * Set le m556.
   *
   * @param m556
   *          le nouveau m556
   */
  void setM556(String m556);
}
