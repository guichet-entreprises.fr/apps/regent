package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IE94.
 */
@ResourceXPath("/E94")
public interface IE94 {

  /**
   * Get le e943.
   *
   * @return le e943
   */
  @FieldXPath("E94.3")
  String getE943();

  /**
   * Set le e943.
   *
   * @param E943
   *          le nouveau e943
   */
  void setE943(String E943);

  /**
   * Get le e945.
   *
   * @return le e945
   */
  @FieldXPath("E94.5")
  String getE945();

  /**
   * Set le e945.
   *
   * @param E945
   *          le nouveau e945
   */
  void setE945(String E945);

  /**
   * Get le e946.
   *
   * @return le e946
   */
  @FieldXPath("E94.6")
  String getE946();

  /**
   * Set le e946.
   *
   * @param E946
   *          le nouveau e946
   */
  void setE946(String E946);

  /**
   * Get le e947.
   *
   * @return le e947
   */
  @FieldXPath("E94.7")
  String getE947();

  /**
   * Set le e947.
   *
   * @param E947
   *          le nouveau e947
   */
  void setE947(String E947);

  /**
   * Get le e948.
   *
   * @return le e948
   */
  @FieldXPath("E94.8")
  String getE948();

  /**
   * Set le e948.
   *
   * @param E948
   *          le nouveau e948
   */
  void setE948(String E948);

  /**
   * Get le e9410.
   *
   * @return le e9410
   */
  @FieldXPath("E94.10")
  String getE9410();

  /**
   * Set le e9410.
   *
   * @param E9410
   *          le nouveau e9410
   */
  void setE9410(String E9410);

  /**
   * Get le e9411.
   *
   * @return le e9411
   */
  @FieldXPath("E94.11")
  String getE9411();

  /**
   * Set le e9411.
   *
   * @param E9411
   *          le nouveau e9411
   */
  void setE9411(String E9411);

  /**
   * Get le e9412.
   *
   * @return le e9412
   */
  @FieldXPath("E94.12")
  String getE9412();

  /**
   * Set le e9412.
   *
   * @param E9412
   *          le nouveau e9412
   */
  void setE9412(String E9412);

  /**
   * Get le e9413.
   *
   * @return le e9413
   */
  @FieldXPath("E94.13")
  String getE9413();

  /**
   * Set le e9413.
   *
   * @param E9413
   *          le nouveau e9413
   */
  void setE9413(String E9413);

  /**
   * Get le e9414.
   *
   * @return le e9414
   */
  @FieldXPath("E94.14")
  String getE9414();

  /**
   * Set le e9414.
   *
   * @param E9414
   *          le nouveau e9414
   */
  void setE9414(String E9414);

}
