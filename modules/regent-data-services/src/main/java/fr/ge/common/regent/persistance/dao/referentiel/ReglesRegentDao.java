package fr.ge.common.regent.persistance.dao.referentiel;

import fr.ge.common.regent.bean.modele.referentiel.ERegleRegent;
import fr.ge.common.regent.persistance.dao.AbstractDao;

/**
 * Le Class ReglesRegentDao.
 */
public class ReglesRegentDao extends AbstractDao < ERegleRegent, String > {

  /**
   * Instancie un nouveau regles regent dao.
   *
   * @param clazz
   *          le clazz
   */
  public ReglesRegentDao(Class < ERegleRegent > clazz) {
    super(clazz);
    // TODO Auto-generated constructor stub
  }

}
