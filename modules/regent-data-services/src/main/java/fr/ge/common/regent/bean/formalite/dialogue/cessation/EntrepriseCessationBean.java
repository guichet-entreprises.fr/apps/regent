package fr.ge.common.regent.bean.formalite.dialogue.cessation;

/**
 * Le Class EntrepriseCessationBean.
 */
public class EntrepriseCessationBean extends AbstractEntrepriseCessationBean<EtablissementCessationBean, DirigeantCessationBean> {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -2648740373219124044L;

    /** Le exercice double activite. */
    private String exerciceDoubleActivite;

    /** Le siren. */
    private String siren;

    /** Le cma immat. */
    private String cmaImmat;

    /** greffeImmat **/
    private String greffeImmat;

    /** Le date radiation. */
    private String dateRadiation;

    /** Le cessation suite deces. */
    private String cessationSuiteDeces;

    /** Le date cessation emploi salarie. */
    private String dateCessationEmploiSalarie;

    /** Le lieuDepotImpot. */
    private String lieuDepotImpot;
    /** Le eirl declaration. */
    private String eirlDeclaration;

    /** Le eirl denomination. */
    private String eirlDenomination;

    /** Le eirl registre depot. */
    private String eirlRegistreDepot;

    /** Le eirl registre lieu. */
    private String eirlRegistreLieu;

    /** Le destination patrimoine affecte. */
    private String destinationPatrimoineAffecte;

    /** Le non sedentarite qualite. */
    private String nonSedentariteQualite;

    /** Le message alerte. */
    private String messageAlerte;

    /** Le nombre dirigeants. */
    private String nombreDirigeants;

    /** S'il existe ou pas un autre etablissement. */
    private String existenceAutreEtablissement;

    /** Le nombre AutreEtablissement. */
    private String nombreAutreEtablissement;

    /**
     * Get le exercice double activite.
     *
     * @return the adresse1
     */
    public String getExerciceDoubleActivite() {
        return this.exerciceDoubleActivite;
    }

    /**
     * Set le exercice double activite.
     *
     * @param exerciceDoubleActivite
     *            le nouveau exercice double activite
     */
    public void setExerciceDoubleActivite(final String exerciceDoubleActivite) {
        this.exerciceDoubleActivite = exerciceDoubleActivite;
    }

    /**
     * Get le siren.
     *
     * @return le siren
     */
    public String getSiren() {
        return this.siren;
    }

    /**
     * Set le siren.
     *
     * @param siren
     *            le nouveau siren
     */
    public void setSiren(final String siren) {
        this.siren = siren;
    }

    /**
     * Get le greffe immat.
     *
     * @return the greffeImmat
     */

    /**
     * Set le greffe immat.
     *
     * @param greffeImmat
     *            the greffeImmat to set
     */

    /**
     * Get le cma immat.
     *
     * @return the cmaImmat
     */
    public String getCmaImmat() {
        return this.cmaImmat;
    }

    /**
     * Set le cma immat.
     *
     * @param cmaImmat
     *            the cmaImmat to set
     */
    public void setCmaImmat(final String cmaImmat) {
        this.cmaImmat = cmaImmat;
    }

    /**
     * Get le date radiation.
     *
     * @return the dateRadiation
     */
    public String getDateRadiation() {
        return this.dateRadiation;
    }

    /**
     * Set le date radiation.
     *
     * @param dateRadiation
     *            the dateRadiation to set
     */
    public void setDateRadiation(final String dateRadiation) {
        this.dateRadiation = dateRadiation;
    }

    /**
     * Get le cessation suite deces.
     *
     * @return the cessationSuiteDeces
     */
    public String getCessationSuiteDeces() {
        return this.cessationSuiteDeces;
    }

    /**
     * Set le cessation suite deces.
     *
     * @param cessationSuiteDeces
     *            the cessationSuiteDeces to set
     */
    public void setCessationSuiteDeces(final String cessationSuiteDeces) {
        this.cessationSuiteDeces = cessationSuiteDeces;
    }

    /**
     * Get le date cessation emploi salarie.
     *
     * @return the dateCessationEmploiSalarie
     */
    public String getDateCessationEmploiSalarie() {
        return this.dateCessationEmploiSalarie;
    }

    /**
     * Set le date cessation emploi salarie.
     *
     * @param dateCessationEmploiSalarie
     *            the dateCessationEmploiSalarie to set
     */
    public void setDateCessationEmploiSalarie(final String dateCessationEmploiSalarie) {
        this.dateCessationEmploiSalarie = dateCessationEmploiSalarie;
    }

    /**
     * Get le eirl declaration.
     *
     * @return the eirlDeclaration
     */
    public String getEirlDeclaration() {
        return this.eirlDeclaration;
    }

    /**
     * Set le eirl declaration.
     *
     * @param eirlDeclaration
     *            the eirlDeclaration to set
     */
    public void setEirlDeclaration(final String eirlDeclaration) {
        this.eirlDeclaration = eirlDeclaration;
    }

    /**
     * Get le eirl denomination.
     *
     * @return the eirlDenomination
     */
    public String getEirlDenomination() {
        return this.eirlDenomination;
    }

    /**
     * Set le eirl denomination.
     *
     * @param eirlDenomination
     *            the eirlDenomination to set
     */
    public void setEirlDenomination(final String eirlDenomination) {
        this.eirlDenomination = eirlDenomination;
    }

    /**
     * Get le eirl registre depot.
     *
     * @return the eirlRegistreDepot
     */
    public String getEirlRegistreDepot() {
        return this.eirlRegistreDepot;
    }

    /**
     * Set le eirl registre depot.
     *
     * @param eirlRegistreDepot
     *            the eirlRegistreDepot to set
     */
    public void setEirlRegistreDepot(final String eirlRegistreDepot) {
        this.eirlRegistreDepot = eirlRegistreDepot;
    }

    /**
     * Get le eirl registre lieu.
     *
     * @return the eirlRegistreLieu
     */
    public String getEirlRegistreLieu() {
        return this.eirlRegistreLieu;
    }

    /**
     * Set le eirl registre lieu.
     *
     * @param eirlRegistreLieu
     *            the eirlRegistreLieu to set
     */
    public void setEirlRegistreLieu(final String eirlRegistreLieu) {
        this.eirlRegistreLieu = eirlRegistreLieu;
    }

    /**
     * Get le destination patrimoine affecte.
     *
     * @return the destinationPatrimoineAffecte
     */
    public String getDestinationPatrimoineAffecte() {
        return this.destinationPatrimoineAffecte;
    }

    /**
     * Set le destination patrimoine affecte.
     *
     * @param destinationPatrimoineAffecte
     *            the destinationPatrimoineAffecte to set
     */
    public void setDestinationPatrimoineAffecte(final String destinationPatrimoineAffecte) {
        this.destinationPatrimoineAffecte = destinationPatrimoineAffecte;
    }

    /**
     * Get le non sedentarite qualite.
     *
     * @return the nonSedentariteQualite
     */
    public String getNonSedentariteQualite() {
        return this.nonSedentariteQualite;
    }

    /**
     * Set le non sedentarite qualite.
     *
     * @param nonSedentariteQualite
     *            the nonSedentariteQualite to set
     */
    public void setNonSedentariteQualite(final String nonSedentariteQualite) {
        this.nonSedentariteQualite = nonSedentariteQualite;
    }

    /**
     * Get le message alerte.
     *
     * @return the messageAlerte
     */
    public String getMessageAlerte() {
        return this.messageAlerte;
    }

    /**
     * Set le message alerte.
     *
     * @param messageAlerte
     *            the messageAlerte to set
     */
    public void setMessageAlerte(final String messageAlerte) {
        this.messageAlerte = messageAlerte;
    }

    /**
     * Get le nombre dirigeants.
     *
     * @return le nombre dirigeants
     */
    public String getNombreDirigeants() {
        return nombreDirigeants;
    }

    /**
     * Set le nombre dirigeants.
     *
     * @param nombreDirigeants
     *            le nouveau nombre dirigeants
     */
    public void setNombreDirigeants(final String nombreDirigeants) {
        this.nombreDirigeants = nombreDirigeants;
    }

    /**
     * Get le lieu de depôt d'Impot.
     *
     * @return lieuDepotImpot
     */

    public String getLieuDepotImpot() {
        return this.lieuDepotImpot;
    }

    /**
     * Set le lieu de depôt d'Impot.
     *
     * @param lieuDepotImpot
     *            le lieu de lieuDepotImpot
     */

    public void setLieuDepotImpot(final String lieuDepotImpot) {
        this.lieuDepotImpot = lieuDepotImpot;
    }

    /**
     * Get autre etablissement.
     *
     * @return existenceAutreEtablissement
     */

    public String getExistenceAutreEtablissement() {
        return this.existenceAutreEtablissement;
    }

    /**
     * Set autre etablissement..
     *
     * @param existenceAutreEtablissement
     *            le existenceAutreEtablissement
     */

    public void setExistenceAutreEtablissement(final String existenceAutreEtablissement) {
        this.existenceAutreEtablissement = existenceAutreEtablissement;
    }

    /**
     * Get le nombre autre etablissement.
     *
     * @return nombreAutreEtablissement
     */
    public String getNombreAutreEtablissement() {
        return this.nombreAutreEtablissement;
    }

    /**
     * Set le nombre autre etablissement.
     *
     * @param nombreAutreEtablissement
     *            le nombre autre etablissement
     */
    public void setNombreAutreEtablissement(final String nombreAutreEtablissement) {
        this.nombreAutreEtablissement = nombreAutreEtablissement;
    }

    /**
     * @return the greffeImmat
     */
    public String getGreffeImmat() {
        return greffeImmat;
    }

    /**
     * @param greffeImmat
     *            the greffeImmat to set
     */
    public void setGreffeImmat(final String greffeImmat) {
        this.greffeImmat = greffeImmat;
    }

}
