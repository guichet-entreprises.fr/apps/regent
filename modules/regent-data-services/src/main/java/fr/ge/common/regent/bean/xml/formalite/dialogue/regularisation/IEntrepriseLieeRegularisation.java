package fr.ge.common.regent.bean.xml.formalite.dialogue.regularisation;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

import fr.ge.common.regent.bean.xml.formalite.IFormalite;
import fr.ge.common.regent.bean.xml.transverse.IAdresse;

/**
 * La classe XML Field de Formalité Régularisation.
 * 
 * @author roveda
 *
 */
@ResourceXPath("/formalite")
public interface IEntrepriseLieeRegularisation extends IFormalite {

    /** La constante MODEL_VERSION. */
    static final int MODEL_VERSION = 1;

    /**
     * Get le nature.
     *
     * @return le nature
     */
    @FieldXPath(value = "nature")
    String getNature();

    /**
     * Get le entreprise pp nom naissance.
     *
     * @return le entreprise pp nom naissance
     */
    @FieldXPath(value = "entreprisePPNomNaissance")
    String getEntreprisePPNomNaissance();

    /**
     * Get le entreprise pp nom usage.
     *
     * @return le entreprise pp nom usage
     */
    @FieldXPath(value = "entreprisePPNomUsage")
    String getEntreprisePPNomUsage();

    /**
     * Get le entreprise pp prenom1.
     *
     * @return le entreprise pp prenom1
     */
    @FieldXPath(value = "entreprisePPPrenom1")
    String getEntreprisePPPrenom1();

    /**
     * Get le entreprise pp prenom2.
     *
     * @return le entreprise pp prenom2
     */
    @FieldXPath(value = "entreprisePPPrenom2")
    String getEntreprisePPPrenom2();

    /**
     * Get le entreprise pp prenom3.
     *
     * @return le entreprise pp prenom3
     */
    @FieldXPath(value = "entreprisePPPrenom3")
    String getEntreprisePPPrenom3();

    /**
     * Get le entreprise pp prenom4.
     *
     * @return le entreprise pp prenom4
     */
    @FieldXPath(value = "entreprisePPPrenom4")
    String getEntreprisePPPrenom4();

    /**
     * Get le entreprise pm denomination.
     *
     * @return le entreprise pm denomination
     */
    @FieldXPath(value = "entreprisePMDenomination")
    String getEntreprisePMDenomination();

    /**
     * Get le entreprise pm forme juridique.
     *
     * @return le entreprise pm forme juridique
     */
    @FieldXPath(value = "entreprisePMFormeJuridique")
    String getEntreprisePMFormeJuridique();

    /**
     * Get le nom domiciliataire.
     *
     * @return le nom domiciliataire
     */
    @FieldXPath(value = "nomDomiciliataire")
    String getNomDomiciliataire();

    /**
     * Get le modif date adresse2.
     *
     * @return le modif date adresse2
     */
    @FieldXPath(value = "modifDateAdresse2")
    String getModifDateAdresse2();

    /**
     * Get le siren.
     *
     * @return le siren
     */
    @FieldXPath(value = "siren")
    String getSiren();

    /**
     * Get le greffe immatriculation.
     *
     * @return le greffe immatriculation
     */
    @FieldXPath(value = "greffeImmatriculation")
    String getGreffeImmatriculation();

    /**
     * Get le domiciliataire meme greffe.
     *
     * @return le domiciliataire meme greffe
     */
    @FieldXPath(value = "domiciliataireMemeGreffe")
    String getDomiciliataireMemeGreffe();

    /**
     * Get le type occurence.
     *
     * @return le type occurence
     */
    @FieldXPath(value = "typeOccurence")
    String getTypeOccurence();

    /**
     * Get le nom.
     *
     * @return le nom
     */
    @FieldXPath(value = "nom")
    String getNom();

    /**
     * Get le adresse.
     *
     * @return le adresse
     */
    @FieldXPath(value = "adresse")
    IAdresse getAdresse();

    /**
     * New adresse.
     *
     * @return le i adresse
     */
    IAdresse newAdresse();

    /**
     * Set le adresse.
     *
     * @param adresse
     *            le nouveau adresse
     */
    void setAdresse(IAdresse adresse);

    /**
     * Get le modif ancienne adresse2.
     *
     * @return le modif ancienne adresse2
     */
    @FieldXPath(value = "modifAncienneAdresse2")
    IAdresse getModifAncienneAdresse2();

    /**
     * New modif ancienne adresse2.
     *
     * @return le i adresse
     */
    IAdresse newModifAncienneAdresse2();

    /**
     * Set le modif ancienne adresse2.
     *
     * @param modifAncienneAdresse2
     *            le nouveau modif ancienne adresse2
     */
    void setModifAncienneAdresse2(IAdresse modifAncienneAdresse2);

    /**
     * Set le nature.
     *
     * @param nature
     *            le nouveau nature
     */
    void setNature(String nature);

    /**
     * Set le entreprise pp nom naissance.
     *
     * @param entreprisePPNomNaissance
     *            le nouveau entreprise pp nom naissance
     */
    void setEntreprisePPNomNaissance(String entreprisePPNomNaissance);

    /**
     * Set le entreprise pp nom usage.
     *
     * @param entreprisePPNomUsage
     *            le nouveau entreprise pp nom usage
     */
    void setEntreprisePPNomUsage(String entreprisePPNomUsage);

    /**
     * Set le entreprise pp prenom1.
     *
     * @param entreprisePPPrenom1
     *            le nouveau entreprise pp prenom1
     */
    void setEntreprisePPPrenom1(String entreprisePPPrenom1);

    /**
     * Set le entreprise pp prenom2.
     *
     * @param entreprisePPPrenom2
     *            le nouveau entreprise pp prenom2
     */
    void setEntreprisePPPrenom2(String entreprisePPPrenom2);

    /**
     * Set le entreprise pp prenom3.
     *
     * @param entreprisePPPrenom3
     *            le nouveau entreprise pp prenom3
     */
    void setEntreprisePPPrenom3(String entreprisePPPrenom3);

    /**
     * Set le entreprise pp prenom4.
     *
     * @param entreprisePPPrenom4
     *            le nouveau entreprise pp prenom4
     */
    void setEntreprisePPPrenom4(String entreprisePPPrenom4);

    /**
     * Set le entreprise pm denomination.
     *
     * @param entreprisePMDenomination
     *            le nouveau entreprise pm denomination
     */
    void setEntreprisePMDenomination(String entreprisePMDenomination);

    /**
     * Set le entreprise pm forme juridique.
     *
     * @param entreprisePMFormeJuridique
     *            le nouveau entreprise pm forme juridique
     */
    void setEntreprisePMFormeJuridique(String entreprisePMFormeJuridique);

    /**
     * Set le nom domiciliataire.
     *
     * @param nomDomiciliataire
     *            le nouveau nom domiciliataire
     */
    void setNomDomiciliataire(String nomDomiciliataire);

    /**
     * Set le modif date adresse2.
     *
     * @param modifDateAdresse2
     *            le nouveau modif date adresse2
     */
    void setModifDateAdresse2(String modifDateAdresse2);

    /**
     * Set le siren.
     *
     * @param siren
     *            le nouveau siren
     */
    void setSiren(String siren);

    /**
     * Set le greffe immatriculation.
     *
     * @param greffeImmatriculation
     *            le nouveau greffe immatriculation
     */
    void setGreffeImmatriculation(String greffeImmatriculation);

    /**
     * Set le domiciliataire meme greffe.
     *
     * @param domiciliataireMemeGreffe
     *            le nouveau domiciliataire meme greffe
     */
    void setDomiciliataireMemeGreffe(String domiciliataireMemeGreffe);

    /**
     * Set le type occurence.
     *
     * @param typeOccurence
     *            le nouveau type occurence
     */
    void setTypeOccurence(String typeOccurence);

    /**
     * Set le nom.
     *
     * @param nom
     *            le nouveau nom
     */
    void setNom(String nom);

}
