package fr.ge.common.regent.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import fr.ge.common.regent.service.exception.XmlFatalException;
import fr.ge.common.regent.service.exception.XmlInvalidException;
import fr.ge.common.regent.service.handler.SimpleErrorHandler;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * Classe de validation Xml.
 * 
 */
public class Validateur {

    /** Le logger fonctionnel. */
    private static final Logger LOGGER_FONC = LoggerFactory.getLogger(Validateur.class);

    /** Le schema files. */
    private final List<Source> schemaFiles = new ArrayList<Source>();

    /**
     * Construit le validateur.
     * 
     * @param xsdFilepath
     *            les chemins absolut de fichiers xsd à utiliser comme source
     * @throws TechnicalException
     *             si l'un des fichier xsd n'a pas pu être lu
     * @throws IOException
     * @throws FileNotFoundException
     * @throws NullPointerException
     *             si l'un des xsdFilepath est null
     */
    public Validateur(String... xsdFilepath) throws TechnicalException, FileNotFoundException, IOException {

        for (String xsd : xsdFilepath) {
            LOGGER_FONC.debug("Ajout du fichier {} à la liste des schéma XSD à utiliser losr de la validation.", xsd);
            this.schemaFiles.add(new StreamSource(new File(xsd)));
        }
    }

    /**
     * Valide le xml.
     *
     * @param xml
     *            le xml à valider
     * @throws TechnicalException
     *             le erreur technique exception
     * @throws XmlInvalidException
     *             si le XML contient des erreurs
     * @throws XmlFatalException
     *             si le XML contient une erreur fatale
     */
    public void validate(final String xml) throws TechnicalException, XmlInvalidException, XmlFatalException {
        this.validate(new StreamSource(new StringReader(xml)));
    }

    /**
     * Valide le xml contenu par un fichier.
     *
     * @param file
     *            le fichier xml à valider
     * @throws TechnicalException
     *             le erreur technique exception
     * @throws XmlInvalidException
     *             si le XML contient des erreurs
     * @throws XmlFatalException
     *             si le XML contient une erreur fatale
     */
    public void validate(final File file) throws TechnicalException, XmlInvalidException, XmlFatalException {
        this.validate(new StreamSource(file));
    }

    /**
     * Valide le xml.
     *
     * @param source
     *            le xml à valider
     * @throws TechnicalException
     *             le erreur technique exception
     * @throws XmlInvalidException
     *             si le XML contient des erreurs
     * @throws XmlFatalException
     *             si le XML contient une erreur fatale
     */
    private void validate(final StreamSource source) throws TechnicalException, XmlInvalidException, XmlFatalException {
        try {
            Validator v = this.buildValidator();
            v.validate(source);

            if (((SimpleErrorHandler) v.getErrorHandler()).hasError()) {
                throw new XmlInvalidException("Le XML ne respecte pas les XSD définies pour les raisons suivantes : " + ((SimpleErrorHandler) v.getErrorHandler()).printErrors());
            }
            LOGGER_FONC.info("Validation du XML... OK");

        } catch (IOException e) {
            throw new TechnicalException("Le XML n'a pas pu être lu pour validation XSD.", e);

        } catch (SAXException e) {
            throw new XmlFatalException("Le XML n'es pas conforme aux XSD. Une erreur fatale est survenue lors de la validation.", e);

        }
    }

    /**
     * Construit un nouveau validateur à partir des XSD fournit.
     *
     * @return Validator
     * @throws TechnicalException
     *             le erreur technique exception
     */
    private Validator buildValidator() throws TechnicalException {
        try {

            Schema schema = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema(this.schemaFiles.toArray(new Source[this.schemaFiles.size()]));

            Validator v = schema.newValidator();
            v.setErrorHandler(new SimpleErrorHandler());

            return v;

        } catch (SAXException e) {
            throw new TechnicalException("Une source XSD défini n'a pas pu être lu.", e);
        }
    }
}
