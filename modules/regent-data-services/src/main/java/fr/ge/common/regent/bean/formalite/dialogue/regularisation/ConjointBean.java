package fr.ge.common.regent.bean.formalite.dialogue.regularisation;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import fr.ge.common.regent.bean.formalite.dialogue.AbstractConjointBean;

/**
 * Le Class ConjointBean.
 */
public class ConjointBean extends AbstractConjointBean implements IFormaliteRegularisationVue {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -3148141275362250293L;

    /**
     * Instancie un nouveau conjoint bean.
     */
    public ConjointBean() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see fr.guichetentreprises.vue.AbstractConjointBean#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

}
