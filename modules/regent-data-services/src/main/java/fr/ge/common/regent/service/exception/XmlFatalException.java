package fr.ge.common.regent.service.exception;

/**
 * Exception signifiant que le XML contient des erreurs non fatales.
 * 
 */
public class XmlFatalException extends AbstractXmlException {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 5206019442082420664L;

    /**
     * Constructeur.
     * 
     * @param message
     *            message d'erreur
     * @param throwable
     *            l'erreur fatale
     */
    public XmlFatalException(String message, Throwable throwable) {
        super(message, throwable);
    }

}
