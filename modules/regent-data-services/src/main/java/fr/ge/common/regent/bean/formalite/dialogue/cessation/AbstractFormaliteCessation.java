package fr.ge.common.regent.bean.formalite.dialogue.cessation;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import fr.ge.common.regent.bean.formalite.FormaliteVueBean;
import fr.ge.common.regent.bean.formalite.dialogue.IFormalite;

/**
 * Le Class AbstractFormaliteCessation.
 * 
 * @param <T>
 *            le type generique
 */
public abstract class AbstractFormaliteCessation<T extends AbstractEntrepriseCessationBean> extends FormaliteVueBean implements IFormalite {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -4533551972596379593L;

    /** Le entreprise. */
    private T entreprise;

    /**
     * Cree le new entreprise.
     * 
     * @return le t
     */
    public abstract T createNewEntreprise();

    /** Le numero formalite. */
    private String numeroFormalite;

    /** Le reseau cfe. */
    private String reseauCFE;

    /** Le dossier id. */
    private Long dossierId;

    /** Le signataire qualite. */
    private String signataireQualite;

    /**
     * Instancie un nouveau abstract formalite cessation.
     */
    public AbstractFormaliteCessation() {
        super();
        if (entreprise == null) {
            entreprise = createNewEntreprise();
        }
    }

    /**
     * Get le entreprise.
     * 
     * @return le entreprise
     */
    public T getEntreprise() {
        return this.entreprise;
    }

    /**
     * Set le entreprise.
     * 
     * @param entreprise
     *            le nouveau entreprise
     */
    public void setEntreprise(final T entreprise) {
        this.entreprise = entreprise;
    }

    /**
     * Get le numero formalite.
     * 
     * @return the numeroFormalite
     */
    public String getNumeroFormalite() {
        return this.numeroFormalite;
    }

    /**
     * Set le numero formalite.
     * 
     * @param numeroFormalite
     *            the numeroFormalite to set
     */
    public void setNumeroFormalite(final String numeroFormalite) {
        this.numeroFormalite = numeroFormalite;
    }

    /**
     * Get le reseau cfe.
     * 
     * @return the reseauCFE
     */
    public String getReseauCFE() {
        return this.reseauCFE;
    }

    /**
     * Set le reseau cfe.
     * 
     * @param reseauCFE
     *            the reseauCFE to set
     */
    public void setReseauCFE(final String reseauCFE) {
        this.reseauCFE = reseauCFE;
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    /**
     * Accesseur sur l'attribut dossier id.
     *
     * @return dossier id
     */
    public Long getDossierId() {
        return dossierId;
    }

    /**
     * Mutateur sur l'attribut dossier id.
     *
     * @param dossierId
     *            le nouveau dossier id
     */
    public void setDossierId(final Long dossierId) {
        this.dossierId = dossierId;
    }

    /**
     * Accesseur sur l'attribut {@link #signataire qualite}.
     *
     * @return signataire qualite
     */
    public String getSignataireQualite() {
        return signataireQualite;
    }

    /**
     * Mutateur sur l'attribut {@link #signataire qualite}.
     *
     * @param signataireQualite
     *            la nouvelle valeur de l'attribut signataire qualite
     */
    public void setSignataireQualite(String signataireQualite) {
        this.signataireQualite = signataireQualite;
    }

}
