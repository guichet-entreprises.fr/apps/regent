package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IU72.
 */
@ResourceXPath("/U72")
public interface IU72 {

  /**
   * Get le U72.1.
   *
   * @return le U72.1
   */
  @FieldXPath("U72.1")
  String getU721();

  /**
   * Set le U72.1.
   *
   * @param U721
   *          .1 le nouveau U72.1
   */
  void setU721(String U721);

  /**
   * Get le U72.2.
   *
   * @return le U72.2
   */
  @FieldXPath("U72.2")
  String getU722();

  /**
   * Set le U72.2.
   *
   * @param U722
   *          .2 le nouveau U72.2
   */
  void setU722(String U722);

  /**
   * Get le U72.3.
   *
   * @return le U72.3
   */
  @FieldXPath("U72.3")
  String getU723();

  /**
   * Set le U72.3.
   *
   * @param U723
   *          .2 le nouveau U72.3
   */
  void setU723(String U723);

}
