package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IC10.
 */
@ResourceXPath("/IPD")
public interface IIPD {
	
	  /**
	   * Get le D01.
	   *
	   * @return le D01
	   */
	  @FieldXPath("D01")
	  ID01[] getD01();

	  /**
	   * Ajoute le to D01.
	   *
	   * @return le i D01
	   */
	  ID01 addToD01();

	  /**
	   * Set le D01.
	   *
	   * @param D01
	   *          le nouveau D01
	   */
	  void setD01(ID01[] D01);
	  
	  /**
	   * Get le D02.
	   *
	   * @return le D02
	   */
	  @FieldXPath("D02")
	  String getD02();

	  /**
	   * Set le D02.
	   *
	   * @param D02
	   *          le nouveau D02
	   */
	  void setD02(String D02);
	
		
	  /**
	   * Get le D03.
	   *
	   * @return le D03
	   */
	  @FieldXPath("D03")
	  ID03[] getD03();

	  /**
	   * Ajoute le to D03.
	   *
	   * @return le i D03
	   */
	  ID03 addToD03();

	  /**
	   * Set le D03.
	   *
	   * @param D01
	   *          le nouveau D03
	   */
	  void setD03(ID03[] D01);
		
	  /**
	   * Get le D04.
	   *
	   * @return le D04
	   */
	  @FieldXPath("D04")
	  ID04[] getD04();

	  /**
	   * Ajoute le to D04.
	   *
	   * @return le i c10D04
	   */
	  ID04 addToD04();

	  /**
	   * Set le D04.
	   *
	   * @param D04
	   *          le nouveau D04
	   */
	  void setD04(ID04[] D04);

	  /**
	   * Get le D06.
	   *
	   * @return le D06
	   */
	  @FieldXPath("D06")
	  String getD06();

	  /**
	   * Ajoute le to D06.
	   *
	   * @return le i D06
	   */
	  String addToD06();

	  /**
	   * Set le D06.
	   *
	   * @param D06
	   *          le nouveau D06
	   */
	  void setD06(String D06);

	
	
}
