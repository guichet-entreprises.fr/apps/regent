package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface IP04.
 */
@ResourceXPath("/P04")
public interface IP04 {

  /**
   * Get le p043.
   *
   * @return le p043
   */
  @FieldXPath("P04.3")
  String getP043();

  /**
   * Set le p043.
   *
   * @param P043
   *          le nouveau p043
   */
  void setP043(String P043);

  /**
   * Get le p045.
   *
   * @return le p045
   */
  @FieldXPath("P04.5")
  String getP045();

  /**
   * Set le p045.
   *
   * @param P045
   *          le nouveau p045
   */
  void setP045(String P045);

  /**
   * Get le p046.
   *
   * @return le p046
   */
  @FieldXPath("P04.6")
  String getP046();

  /**
   * Set le p046.
   *
   * @param P046
   *          le nouveau p046
   */
  void setP046(String P046);

  /**
   * Get le p047.
   *
   * @return le p047
   */
  @FieldXPath("P04.7")
  String getP047();

  /**
   * Set le p047.
   *
   * @param P047
   *          le nouveau p047
   */
  void setP047(String P047);

  /**
   * Get le p048.
   *
   * @return le p048
   */
  @FieldXPath("P04.8")
  String getP048();

  /**
   * Set le p048.
   *
   * @param P048
   *          le nouveau p048
   */
  void setP048(String P048);

  /**
   * Get le p0410.
   *
   * @return le p0410
   */
  @FieldXPath("P04.10")
  String getP0410();

  /**
   * Set le p0410.
   *
   * @param P0410
   *          le nouveau p0410
   */
  void setP0410(String P0410);

  /**
   * Get le p0411.
   *
   * @return le p0411
   */
  @FieldXPath("P04.11")
  String getP0411();

  /**
   * Set le p0411.
   *
   * @param P0411
   *          le nouveau p0411
   */
  void setP0411(String P0411);

  /**
   * Get le p0412.
   *
   * @return le p0412
   */
  @FieldXPath("P04.12")
  String getP0412();

  /**
   * Set le p0412.
   *
   * @param P0412
   *          le nouveau p0412
   */
  void setP0412(String P0412);

  /**
   * Get le p0413.
   *
   * @return le p0413
   */
  @FieldXPath("P04.13")
  String getP0413();

  /**
   * Set le p0413.
   *
   * @param P0413
   *          le nouveau p0413
   */
  void setP0413(String P0413);

  /**
   * Get le p0414.
   *
   * @return le p0414
   */
  @FieldXPath("P04.14")
  String getP0414();

  /**
   * Set le p0414.
   *
   * @param P0414
   *          le nouveau p0414
   */
  void setP0414(String P0414);

}
