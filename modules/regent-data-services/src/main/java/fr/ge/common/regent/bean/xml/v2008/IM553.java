/**
 * 
 */
package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * L'interface IM21.
 */
@ResourceXPath("/M55.3")
public interface IM553 {

  /**
   * Get le m5533.
   *
   * @return le m5533
   */
  @FieldXPath("M55.3.3")
  String getM5533();

  /**
   * Set le m5533.
   *
   * @param m5533
   *          le nouveau m5533
   */
  void setM5533(String m5533);

  /**
   * Get le m5534.
   *
   * @return le m5534
   */
  @FieldXPath("M55.3.4")
  String getM5534();

  /**
   * Set le m5534.
   *
   * @param m5534
   *          le nouveau m5534
   */
  void setM5534(String m5534);

  /**
   * Get le m5535.
   *
   * @return le m5535
   */
  @FieldXPath("M55.3.5")
  String getM5535();

  /**
   * Set le m5535.
   *
   * @param m5535
   *          le nouveau m5535
   */
  void setM5535(String m5535);

  /**
   * Get le m5536.
   *
   * @return le m5536
   */
  @FieldXPath("M55.3.6")
  String getM5536();

  /**
   * Set le m5536.
   *
   * @param m5536
   *          le nouveau m5536
   */
  void setM5536(String m5536);

  /**
   * Get le m5537.
   *
   * @return le m5537
   */
  @FieldXPath("M55.3.7")
  String getM5537();

  /**
   * Set le m5537.
   *
   * @param m5537
   *          le nouveau m5537
   */
  void setM5537(String m5537);

  /**
   * Get le m5538.
   *
   * @return le m5538
   */
  @FieldXPath("M55.3.8")
  String getM5538();

  /**
   * Set le m5538.
   *
   * @param m5538
   *          le nouveau m5538
   */
  void setM5538(String m5538);

  /**
   * Get le m5539.
   *
   * @return le m5539
   */
  @FieldXPath("M55.3.9")
  String getM5539();

  /**
   * Set le m5539.
   *
   * @param m5539
   *          le nouveau m5539
   */
  void setM5539(String m5539);

  /**
   * Get le m55310.
   *
   * @return le m55310
   */
  @FieldXPath("M55.3.10")
  String getM55310();

  /**
   * Set le m55310.
   *
   * @param m55310
   *          le nouveau m55310
   */
  void setM55310(String m55310);

  /**
   * Get le m55311.
   *
   * @return le m55311
   */
  @FieldXPath("M55.3.11")
  String getM55311();

  /**
   * Set le m55311.
   *
   * @param m55311
   *          le nouveau m55311
   */
  void setM55311(String m55311);

  /**
   * Get le m55312.
   *
   * @return le m55312
   */
  @FieldXPath("M55.3.12")
  String getM55312();

  /**
   * Set le m55312.
   *
   * @param m55312
   *          le nouveau m55312
   */
  void setM55312(String m55312);

  /**
   * Get le m55313.
   *
   * @return le m55313
   */
  @FieldXPath("M55.3.13")
  String getM55313();

  /**
   * Set le m55313.
   *
   * @param m55313
   *          le nouveau m55313
   */
  void setM55313(String m55313);

  /**
   * Get le m55314.
   *
   * @return le m55314
   */
  @FieldXPath("M55.3.14")
  String getM55314();

  /**
   * Set le m55314.
   *
   * @param m55314
   *          le nouveau m55314
   */
  void setM55314(String m55314);

}
