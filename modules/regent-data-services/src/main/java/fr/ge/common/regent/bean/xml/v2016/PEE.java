package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Le Interface PEE.
 */
@ResourceXPath("/PEE")
public interface PEE {

  /**
   * Get le e34.
   *
   * @return le e34
   */
  @FieldXPath("E34")
  IE34[] getE34();

  /**
   * Ajoute le to e34.
   *
   * @return le i e34
   */
  IE34 addToE34();

  /**
   * Set le e34.
   *
   * @param E34
   *          le nouveau e34
   */
  void setE34(IE34[] E34);

}
