package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;


@ResourceXPath("/U40.2")
public interface IU402 {
	
	/**
	   * Get le U4021.
	   *
	   * @return le U4021
	   */
	  @FieldXPath("U40.2.1")
	  String getU4021();

	  /**
	   * Set le U4021.
	   *
	   * @param U4021
	   *          le nouveau U4021
	   */
	  void setU4021(String U4021);
	  
		
	  /**
	   * Get le U4022.
	   *
	   * @return le U4022
	   */
	  @FieldXPath("U40.2.2")
	  IU4022[] getU4022();

	  /**
	   * Ajoute le to U4022.
	   *
	   * @return le i U4022
	   */
	  IU4022 addToU4022();

	  /**
	   * Set le P76.
	   *
	   * @param P76
	   *          le nouveau P76
	   */
	  void setU4022(IU4022[] U4022);
	  
	  
		/**
	   * Get le U4024.
	   *
	   * @return le U4024
	   */
	  @FieldXPath("U40.2.4")
	  String getU4024();

	  /**
	   * Set le c401.
	   *
	   * @param C401
	   *          le nouveau c401
	   */
	  void setU4024(String U4024);
	  
	  
		/**
	   * Get le U4025.
	   *
	   * @return le U4025
	   */
	  @FieldXPath("U40.2.5")
	  String getU4025();

	  /**
	   * Set le U4025.
	   *
	   * @param U4025
	   *          le nouveau U4025
	   */
	  void setU4025(String U4025);
	  
	  

}
