package fr.ge.common.regent.bean.xml.formalite.dialogue.modification;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

import fr.ge.common.regent.bean.xml.formalite.IFormalite;
import fr.ge.common.regent.bean.xml.transverse.IAdresse;

/**
 * La classe XML Field de Formalité Régularisation.
 * 
 * @author roveda
 * 
 */
@ResourceXPath("/formalite")
public interface IEtablissementModification extends IFormalite {

    /**
     * Get le domiciliation.
     *
     * @return le domiciliation
     */
    @FieldXPath(value = "domiciliation")
    String getDomiciliation();

    /**
     * Set le domiciliation.
     *
     * @param domiciliation
     *            le nouveau domiciliation
     */
    void setDomiciliation(String domiciliation);

    /**
     * Get le modif date adresse.
     *
     * @return le modif date adresse
     */
    @FieldXPath(value = "modifDateAdresse")
    String getModifDateAdresse();

    /**
     * Set le modif date adresse.
     *
     * @param modifDateAdresse
     *            le nouveau modif date adresse
     */
    void setModifDateAdresse(String modifDateAdresse);

    /**
     * Get le modif date adresse1.
     *
     * @return le modif date adresse1
     */
    @FieldXPath(value = "modifDateAdresse1")
    String getModifDateAdresse1();

    /**
     * Set le modif date adresse1.
     *
     * @param modifDateAdresse1
     *            le nouveau modif date adresse1
     */
    void setModifDateAdresse1(String modifDateAdresse1);

    /**
     * Get le modif evenements3.
     *
     * @return le modif evenements3
     */
    @FieldXPath(value = "modifEvenements3")
    String getModifEvenements3();

    /**
     * Set le modif evenements3.
     *
     * @param modifEvenements3
     *            le nouveau modif evenements3
     */
    void setModifEvenements3(String modifEvenements3);

    /**
     * Get le adresse.
     *
     * @return le adresse
     */
    @FieldXPath(value = "adresse")
    IAdresse getAdresse();

    /**
     * New adresse.
     *
     * @return le i adresse
     */
    IAdresse newAdresse();

    /**
     * Set le adresse.
     *
     * @param adresse
     *            le nouveau adresse
     */
    void setAdresse(IAdresse adresse);

    /**
     * Get le modif ancienne adresse1.
     *
     * @return le modif ancienne adresse1
     */
    @FieldXPath(value = "modifAncienneAdresse1")
    IAdresse getModifAncienneAdresse1();

    /**
     * New modif ancienne adresse1.
     *
     * @return le i adresse
     */
    IAdresse newModifAncienneAdresse1();

    /**
     * Set le modif ancienne adresse1.
     *
     * @param modifAncienneAdresse1
     *            le nouveau modif ancienne adresse1
     */
    void setModifAncienneAdresse1(IAdresse modifAncienneAdresse1);

    /**
     * Get le destination etablissement principal.
     *
     * @return le destination etablissement principal
     */
    @FieldXPath(value = "destinationEtablissementPrincipal")
    String getDestinationEtablissementPrincipal();

    /**
     * Set le destination etablissement principal.
     *
     * @param destinationEtablissementPrincipal
     *            le nouveau destination etablissement principal
     */
    void setDestinationEtablissementPrincipal(String destinationEtablissementPrincipal);

    /**
     * Get le destination autre1.
     *
     * @return le destination autre1
     */
    @FieldXPath(value = "destinationAutre1")
    String getDestinationAutre1();

    /**
     * Set le destination autre1.
     *
     * @param destinationAutre1
     *            le nouveau destination autre1
     */
    void setDestinationAutre1(String destinationAutre1);

    /**
     * Get le activites1.
     *
     * @return le activites1
     */
    @FieldXPath(value = "activites1")
    String getActivites1();

    /**
     * Set le aestination autre1.
     *
     * @param activites1
     *            le nouveau aestination autre1
     */
    void setActivites1(String activites1);

    /**
     * Get le modif date activite.
     *
     * @return le modif date activite
     */
    @FieldXPath(value = "modifDateActivite")
    String getModifDateActivite();

    /**
     * Set le modif date activite.
     *
     * @param modifDateActivite
     *            le nouveau modif date activite
     */
    void setModifDateActivite(String modifDateActivite);

    /**
     * Get le activites2.
     *
     * @return le activites2
     */
    @FieldXPath(value = "activites2")
    String getActivites2();

    /**
     * Set le activite2.
     *
     * @param activites2
     *            le nouveau activite2
     */
    void setActivites2(String activites2);

    /**
     * Set le aestination autre2.
     *
     * @param activites2
     *            le nouveau aestination autre2
     */
    void setAestinationAutre2(String activites2);

    /**
     * Get le suppression activite par.
     *
     * @return le suppression activite par
     */
    @FieldXPath(value = "suppressionActivitePar")
    String getSuppressionActivitePar();

    /**
     * Set le suppression activite par.
     *
     * @param suppressionActivitePar
     *            le nouveau suppression activite par
     */
    void setSuppressionActivitePar(String suppressionActivitePar);

    /**
     * Get le suppression activite autre.
     *
     * @return le suppression activite autre
     */
    @FieldXPath(value = "suppressionActiviteAutre")
    String getSuppressionActiviteAutre();

    /**
     * Set le suppression activite autre.
     *
     * @param suppressionActiviteAutre
     *            le nouveau suppression activite autre
     */
    void setSuppressionActiviteAutre(String suppressionActiviteAutre);

    /**
     * Get le activite plus importante.
     *
     * @return le activite plus importante
     */
    @FieldXPath(value = "activitePlusImportante")
    String getActivitePlusImportante();

    /**
     * Set le activite plus importante.
     *
     * @param activitePlusImportante
     *            le nouveau activite plus importante
     */
    void setActivitePlusImportante(String activitePlusImportante);

    /**
     * Get le activite nature.
     *
     * @return le activite nature
     */
    @FieldXPath(value = "activiteNature")
    String getActiviteNature();

    /**
     * Set le activite nature.
     *
     * @param activiteNature
     *            le nouveau activite nature
     */
    void setActiviteNature(String activiteNature);

    /**
     * Get le activite nature autre.
     *
     * @return le activite nature autre
     */
    @FieldXPath(value = "activiteNatureAutre")
    String getActiviteNatureAutre();

    /**
     * Set le activite nature autre.
     *
     * @param activiteNatureAutre
     *            le nouveau activite nature autre
     */
    void setActiviteNatureAutre(String activiteNatureAutre);

    /**
     * Get le activite lieu exercice magasin.
     *
     * @return le activite lieu exercice magasin
     */
    @FieldXPath(value = "activiteLieuExerciceMagasin")
    Integer getActiviteLieuExerciceMagasin();

    /**
     * Set le activite lieu exercice magasin.
     *
     * @param activiteLieuExerciceMagasin
     *            le nouveau activite lieu exercice magasin
     */
    void setActiviteLieuExerciceMagasin(Integer activiteLieuExerciceMagasin);

    /**
     * Get le activite permanente saisonniere.
     *
     * @return le activite permanente saisonniere
     */
    @FieldXPath(value = "activitePermanenteSaisonniere")
    String getActivitePermanenteSaisonniere();

    /**
     * Set le activite permanente saisonniere.
     *
     * @param activitePermanenteSaisonniere
     *            le nouveau activite permanente saisonniere
     */
    void setActivitePermanenteSaisonniere(String activitePermanenteSaisonniere);

    /**
     * Get le non sedentarite qualite.
     *
     * @return le non sedentarite qualite
     */
    @FieldXPath(value = "nonSedentariteQualite")
    String getNonSedentariteQualite();

    /**
     * Set le non sedentarite qualite.
     *
     * @param nonSedentariteQualite
     *            le nouveau non sedentarite qualite
     */
    void setNonSedentariteQualite(String nonSedentariteQualite);

    /**
     * Get le aqpa situation.
     *
     * @return le aqpa situation
     */
    @FieldXPath(value = "aqpaSituation")
    String getAqpaSituation();

    /**
     * Set le aqpa situation.
     *
     * @param aqpaSituation
     *            le nouveau aqpa situation
     */
    void setAqpaSituation(String aqpaSituation);

    /**
     * Get le aqpa intitule diplome.
     *
     * @return le aqpa intitule diplome
     */
    @FieldXPath(value = "aqpaIntituleDiplome")
    String getAqpaIntituleDiplome();

    /**
     * getteur aqpaSalarieQualifie.
     * 
     * @return aqpaSalarieQualifie
     */
    @FieldXPath(value = "aqpaSalarieQualifie")
    String getAqpaSalarieQualifie();

    /**
     * Setteur aqpaSalarieQualifie.
     * 
     * @param aqpaSalarieQualifie
     *            aqpaSalarieQualifie
     */
    void setAqpaSalarieQualifie(String aqpaSalarieQualifie);

    /**
     * Set le aqpa intitule diplome.
     *
     * @param aqpaIntituleDiplome
     *            le nouveau aqpa intitule diplome
     */
    void setAqpaIntituleDiplome(String aqpaIntituleDiplome);

    /**
     * Get le nom commercial professionnel.
     *
     * @return le nom commercial professionnel
     */
    @FieldXPath(value = "nomCommercialProfessionnel")
    String getNomCommercialProfessionnel();

    /**
     * Set le nom commercial professionnel.
     *
     * @param nomCommercialProfessionnel
     *            le nouveau nom commercial professionnel
     */
    void setNomCommercialProfessionnel(String nomCommercialProfessionnel);

    /**
     * Get le enseigne.
     *
     * @return le enseigne
     */
    @FieldXPath(value = "enseigne")
    String getEnseigne();

    /**
     * Set le enseigne.
     *
     * @param enseigne
     *            le nouveau enseigne
     */
    void setEnseigne(String enseigne);

    /**
     * Get le origine fonds.
     *
     * @return le origine fonds
     */
    @FieldXPath(value = "origineFonds")
    String getOrigineFonds();

    /**
     * Set le origine fonds.
     *
     * @param origineFonds
     *            le nouveau origine fonds
     */
    void setOrigineFonds(String origineFonds);

    /**
     * Get le loueur mandant du fonds debut contrat.
     *
     * @return le loueur mandant du fonds debut contrat
     */
    @FieldXPath(value = "loueurMandantDuFondsDebutContrat")
    String getLoueurMandantDuFondsDebutContrat();

    /**
     * Set le loueur mandant du fonds debut contrat.
     *
     * @param loueurMandantDuFondsDebutContrat
     *            le nouveau loueur mandant du fonds debut contrat
     */
    void setLoueurMandantDuFondsDebutContrat(String loueurMandantDuFondsDebutContrat);

    /**
     * Get le loueur mandant du fonds duree contrat indeterminee.
     *
     * @return le loueur mandant du fonds duree contrat indeterminee
     */
    @FieldXPath(value = "loueurMandantDuFondsDureeContratIndeterminee")
    String getLoueurMandantDuFondsDureeContratIndeterminee();

    /**
     * Set le loueur mandant du fonds duree contrat indeterminee.
     *
     * @param loueurMandantDuFondsDureeContratIndeterminee
     *            le nouveau loueur mandant du fonds duree contrat indeterminee
     */
    void setLoueurMandantDuFondsDureeContratIndeterminee(String loueurMandantDuFondsDureeContratIndeterminee);

    /**
     * Get le loueur mandant du fonds fin contrat.
     *
     * @return le loueur mandant du fonds fin contrat
     */
    @FieldXPath(value = "loueurMandantDuFondsFinContrat")
    String getLoueurMandantDuFondsFinContrat();

    /**
     * Set le loueur mandant du fonds fin contrat.
     *
     * @param loueurMandantDuFondsFinContrat
     *            le nouveau loueur mandant du fonds fin contrat
     */
    void setLoueurMandantDuFondsFinContrat(String loueurMandantDuFondsFinContrat);

    /**
     * Get le loueur mandant du fonds renouvellement tacite reconduction.
     *
     * @return le loueur mandant du fonds renouvellement tacite reconduction
     */
    @FieldXPath(value = "loueurMandantDuFondsRenouvellementTaciteReconduction")
    String getLoueurMandantDuFondsRenouvellementTaciteReconduction();

    /**
     * Set le loueur mandant du fonds renouvellement tacite reconduction.
     *
     * @param loueurMandantDuFondsRenouvellementTaciteReconduction
     *            le nouveau loueur mandant du fonds renouvellement tacite
     *            reconduction
     */
    void setLoueurMandantDuFondsRenouvellementTaciteReconduction(String loueurMandantDuFondsRenouvellementTaciteReconduction);

    /**
     * Get le plan cession.
     *
     * @return le plan cession
     */
    @FieldXPath(value = "planCession")
    String getPlanCession();

    /**
     * Set le plan cession.
     *
     * @param planCession
     *            le nouveau plan cession
     */
    void setPlanCession(String planCession);

    /**
     * Get le fonds uniquement artisanal.
     *
     * @return le fonds uniquement artisanal
     */
    @FieldXPath(value = "fondsUniquementArtisanal")
    String getFondsUniquementArtisanal();

    /**
     * Set le fonds uniquement artisanal.
     *
     * @param fondsUniquementArtisanal
     *            le nouveau fonds uniquement artisanal
     */
    void setFondsUniquementArtisanal(String fondsUniquementArtisanal);

    /**
     * Get le journal annonces legales nom.
     *
     * @return le journal annonces legales nom
     */
    @FieldXPath(value = "journalAnnoncesLegalesNom")
    String getJournalAnnoncesLegalesNom();

    /**
     * Set le journal annonces legales nom.
     *
     * @param journalAnnoncesLegalesNom
     *            le nouveau journal annonces legales nom
     */
    void setJournalAnnoncesLegalesNom(String journalAnnoncesLegalesNom);

    /**
     * Get le journal annonces legales date parution.
     *
     * @return le journal annonces legales date parution
     */
    @FieldXPath(value = "journalAnnoncesLegalesDateParution")
    String getJournalAnnoncesLegalesDateParution();

    /**
     * Set le journal annonces legales date parution.
     *
     * @param journalAnnoncesLegalesDateParution
     *            le nouveau journal annonces legales date parution
     */
    void setJournalAnnoncesLegalesDateParution(String journalAnnoncesLegalesDateParution);

    /**
     * Get le effectif salarie presence.
     *
     * @return le effectif salarie presence
     */
    @FieldXPath(value = "effectifSalariePresence")
    String getEffectifSalariePresence();

    /**
     * Set le effectif salarie presence.
     *
     * @param effectifSalariePresence
     *            le nouveau effectif salarie presence
     */
    void setEffectifSalariePresence(String effectifSalariePresence);

    /**
     * Get le effectif salarie nombre.
     *
     * @return le effectif salarie nombre
     */
    @FieldXPath(value = "effectifSalarieNombre")
    Integer getEffectifSalarieNombre();

    /**
     * Set le effectif salarie nombre.
     *
     * @param effectifSalarieNombre
     *            le nouveau effectif salarie nombre
     */
    void setEffectifSalarieNombre(Integer effectifSalarieNombre);

    /**
     * Get le effectif salarie apprentis.
     *
     * @return le effectif salarie apprentis
     */
    @FieldXPath(value = "effectifSalarieApprentis")
    Integer getEffectifSalarieApprentis();

    /**
     * Set le effectif salarie apprentis.
     *
     * @param effectifSalarieApprentis
     *            le nouveau effectif salarie apprentis
     */
    void setEffectifSalarieApprentis(Integer effectifSalarieApprentis);

    /**
     * Get le effectif salarie vrp.
     *
     * @return le effectif salarie vrp
     */
    @FieldXPath(value = "effectifSalarieVrp")
    Integer getEffectifSalarieVrp();

    /**
     * Set le effectif salarie vrp.
     *
     * @param effectifSalarieVrp
     *            le nouveau effectif salarie vrp
     */
    void setEffectifSalarieVrp(Integer effectifSalarieVrp);

    /**
     * Accesseur sur l'attribut {@link #periode activite saisonniere1 date
     * debut}.
     *
     * @return the effectifsalariePresence
     */
    @FieldXPath(value = "periodeActiviteSaisonniere1DateDebut")
    String getPeriodeActiviteSaisonniere1DateDebut();

    /**
     * Mutateur sur l'attribut {@link #periode activite saisonniere1 date
     * debut}.
     *
     * @param periodeActiviteSaisonniere1DateDebut
     *            the periodeActiviteSaisonniere1DateDebut to set
     */
    void setPeriodeActiviteSaisonniere1DateDebut(String periodeActiviteSaisonniere1DateDebut);

    /**
     * Accesseur sur l'attribut {@link #periode activite saisonniere1 date fin}.
     *
     * @return the periodeActiviteSaisonniere1DateFin
     */
    @FieldXPath(value = "periodeActiviteSaisonniere1DateFin")
    String getPeriodeActiviteSaisonniere1DateFin();

    /**
     * Mutateur sur l'attribut {@link #periode activite saisonniere1 date fin}.
     *
     * @param periodeActiviteSaisonniere1DateFin
     *            la nouvelle valeur de l'attribut periode activite saisonniere1
     *            date fin
     */

    void setPeriodeActiviteSaisonniere1DateFin(String periodeActiviteSaisonniere1DateFin);

    /**
     * Accesseur sur l'attribut {@link #periode activite saisonniere2 date
     * debut}.
     *
     * @return the periodeActiviteSaisonniere2DateDebut
     */
    @FieldXPath(value = "periodeActiviteSaisonniere2DateDebut")
    String getPeriodeActiviteSaisonniere2DateDebut();

    /**
     * Mutateur sur l'attribut {@link #periode activite saisonniere2 date
     * debut}.
     *
     * @param periodeActiviteSaisonniere2DateDebut
     *            the periodeActiviteSaisonniere2DateDebut to set
     */
    void setPeriodeActiviteSaisonniere2DateDebut(String periodeActiviteSaisonniere2DateDebut);

    /**
     * Accesseur sur l'attribut {@link #periode activite saisonniere2 date fin}.
     *
     * @return the periodeActiviteSaisonniere2DateFin
     */
    @FieldXPath(value = "periodeActiviteSaisonniere2DateFin")
    String getPeriodeActiviteSaisonniere2DateFin();

    /**
     * Mutateur sur l'attribut {@link #periode activite saisonniere2 date fin}.
     *
     * @param periodeActiviteSaisonniere2DateFin
     *            the periodeActiviteSaisonniere2DateFin to set
     */
    void setPeriodeActiviteSaisonniere2DateFin(String periodeActiviteSaisonniere2DateFin);

    /**
     * Accesseur sur l'attribut {@link #transfert meme departement}.
     *
     * @return l'attribut transfertMemeDepartement
     */
    @FieldXPath(value = "transfertMemeDepartement")
    String getTransfertMemeDepartement();

    /**
     * Mutateur sur l'attribut {@link #transfert meme departement}.
     *
     * @param transfertMemeDepartement
     *            transfertMemeDepartement
     */
    void setTransfertMemeDepartement(String transfertMemeDepartement);

    /**
     * Getter de l'attribut nombreActivitesSoumisesQualification.
     * 
     * @return la valeur de nombreActivitesSoumisesQualification
     */
    @FieldXPath(value = "nombreActivitesSoumisesQualification")
    Integer getNombreActivitesSoumisesQualification();

    /**
     * Setteur nombreActivitesSoumisesQualification.
     * 
     * @param nombreActivitesSoumisesQualification
     *            nombre des Activivites Soumises à Qualification
     */
    void setNombreActivitesSoumisesQualification(Integer nombreActivitesSoumisesQualification);

    /**
     * Get le activiteSoumiseQualification.
     *
     * @return le activiteSoumiseQualification
     */
    @FieldXPath(value = "activitesSoumisesQualification/activiteSoumiseQualification")
    IActiviteSoumiseQualificationModification[] getActivitesSoumisesQualification();

    /**
     * Ajoute le to activiteSoumiseQualification.
     *
     * @return le i activiteSoumiseQualification
     */
    IActiviteSoumiseQualificationModification addToActivitesSoumisesQualification();

    /**
     * Set le ActivitesSoumisesQualification.
     *
     * @param activitesSoumisesQualification
     *            le nouveau ActivitesSoumisesQualification
     */
    void setActivitesSoumisesQualification(IActiviteSoumiseQualificationModification[] activitesSoumisesQualification);

    /**
     * Retire le from activitesSoumisesQualification.
     *
     * @param activitesSoumisesQualification
     *            le activitesSoumisesQualification
     */
    void removeFromActivitesSoumisesQualification(IActiviteSoumiseQualificationModification activitesSoumisesQualification);
}
