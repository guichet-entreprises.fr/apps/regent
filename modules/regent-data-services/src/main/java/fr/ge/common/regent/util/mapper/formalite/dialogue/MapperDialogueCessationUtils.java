package fr.ge.common.regent.util.mapper.formalite.dialogue;

import java.util.List;
import java.util.UUID;

import org.apache.commons.collections.CollectionUtils;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.ge.common.regent.bean.formalite.dialogue.cessation.DialogueCessationBean;
import fr.ge.common.regent.bean.formalite.dialogue.cessation.DirigeantCessationBean;
import fr.ge.common.regent.bean.formalite.dialogue.cessation.EntrepriseCessationBean;
import fr.ge.common.regent.bean.formalite.dialogue.cessation.EtablissementCessationBean;
import fr.ge.common.regent.bean.formalite.dialogue.creation.AdresseBean;
import fr.ge.common.regent.bean.xml.formalite.dialogue.cessation.IDialogueCessation;
import fr.ge.common.regent.bean.xml.formalite.dialogue.cessation.IDirigeantCessation;
import fr.ge.common.regent.bean.xml.formalite.dialogue.cessation.IEntrepriseCessation;
import fr.ge.common.regent.bean.xml.formalite.dialogue.cessation.IEtablissementCessation;
import fr.ge.common.regent.bean.xml.transverse.IAdresse;
import fr.ge.common.regent.util.mapper.formalite.IMapperFormaliteUtils;

/**
 * Le Class MapperFormaliteCessationUtils.
 */
@Component
public class MapperDialogueCessationUtils implements IMapperFormaliteUtils<IDialogueCessation, DialogueCessationBean> {

    /** Le formalite cessation mapper. */
    @Autowired
    private Mapper formaliteCessationMapper;

    /** Le adresse c mapper. */
    @Autowired
    private Mapper adresseCMapper;

    /** Le entreprise cessation mapper. */
    @Autowired
    private Mapper entrepriseCessationMapper;

    /** Le dirigeant cessation mapper. */
    @Autowired
    private Mapper dirigeantCessationMapper;

    /** Le etablissement cessation mapper. */
    @Autowired
    private Mapper etablissementCessationMapper;

    /** Le postal commune c mapper. */
    @Autowired
    private Mapper postalCommuneCMapper;

    /**
     * mapper pour préparer un bean depuis une interface (stocké en base).
     *
     * @param iFormaliteCessation
     *            le i formalite cessation
     * @param formaliteCessationBean
     *            le formalite cessation bean
     */

    public void mapper(IDialogueCessation iFormaliteCessation, DialogueCessationBean formaliteCessationBean) {

        // Mapper Adresse signature
        IAdresse iAdresseSignature = iFormaliteCessation.getAdresseSignature();
        if (iAdresseSignature != null) {
            AdresseBean adresseSignatureBean = formaliteCessationBean.getAdresseSignature();
            this.adresseCMapper.map(iAdresseSignature, adresseSignatureBean);
            if (iAdresseSignature.getCodePostalCommune() != null) {
                this.postalCommuneCMapper.map(iAdresseSignature.getCodePostalCommune(), adresseSignatureBean.getCodePostalCommune());
            }

        }

        // Mapper Correspondance Adresse
        IAdresse iCorrespondanceAdresse = iFormaliteCessation.getCorrespondanceAdresse();
        if (iCorrespondanceAdresse != null) {
            AdresseBean iCorrAdresseBean = formaliteCessationBean.getCorrespondanceAdresse();
            this.adresseCMapper.map(iCorrespondanceAdresse, iCorrAdresseBean);
            if (iCorrespondanceAdresse.getCodePostalCommune() != null) {
                this.postalCommuneCMapper.map(iCorrespondanceAdresse.getCodePostalCommune(), iCorrAdresseBean.getCodePostalCommune());
            }
        }

        // mapper que l'etablissement et le dirigeant

        // Mapper Entreprise
        IEntrepriseCessation iEntrepriseC = iFormaliteCessation.getEntreprise();
        if (iEntrepriseC != null) {
            EntrepriseCessationBean entrepriseBean = formaliteCessationBean.getEntreprise();

            entrepriseBean.getEtablissements().clear();

            for (IEtablissementCessation iEtablissement : iEntrepriseC.getEtablissements()) {
                EtablissementCessationBean etablissementsBean = new EtablissementCessationBean();
                this.etablissementCessationMapper.map(iEtablissement, etablissementsBean);
                entrepriseBean.getEtablissements().add(etablissementsBean);
            }

            entrepriseBean.getDirigeants().clear();
            for (IDirigeantCessation iDirigeantR : iEntrepriseC.getDirigeants()) {
                DirigeantCessationBean dirigeantBean = new DirigeantCessationBean();
                this.dirigeantCessationMapper.map(iDirigeantR, dirigeantBean);
                entrepriseBean.getDirigeants().add(dirigeantBean);
            }

            // mapping champs simples pour Entreprise
            this.entrepriseCessationMapper.map(iEntrepriseC, entrepriseBean);

        } // Fin mapping Entreprise

        /* mapping champs simples */
        this.formaliteCessationMapper.map(iFormaliteCessation, formaliteCessationBean);

    }

    // Mapper linterface to bean

    /**
     * Mapper.
     *
     * @param formaliteCessationBean
     *            le formalite cessation bean
     * @param iFormaliteCassation
     *            le i formalite cassation
     */
    public void mapper(DialogueCessationBean formaliteCessationBean, IDialogueCessation iFormaliteCassation) {

        // Mapper Adresse signature
        iFormaliteCassation.newAdresseSignature();
        IAdresse iAdresseSignature = iFormaliteCassation.getAdresseSignature();
        this.adresseCMapper.map(formaliteCessationBean.getAdresseSignature(), iAdresseSignature);
        iAdresseSignature.newCodePostalCommune();
        this.postalCommuneCMapper.map(formaliteCessationBean.getAdresseSignature().getCodePostalCommune(), iAdresseSignature.getCodePostalCommune());

        // Mapper Correspondance Adresse
        iFormaliteCassation.newCorrespondanceAdresse();
        IAdresse icorrespondanceAdresse = iFormaliteCassation.getCorrespondanceAdresse();
        this.adresseCMapper.map(formaliteCessationBean.getCorrespondanceAdresse(), icorrespondanceAdresse);
        icorrespondanceAdresse.newCodePostalCommune();
        this.postalCommuneCMapper.map(formaliteCessationBean.getCorrespondanceAdresse().getCodePostalCommune(), icorrespondanceAdresse.getCodePostalCommune());

        // DEBUT- Mapper Entreprise
        iFormaliteCassation.newEntreprise();
        IEntrepriseCessation iEntreprise = iFormaliteCassation.getEntreprise();

        EntrepriseCessationBean entrepriseBean = formaliteCessationBean.getEntreprise();
        if (entrepriseBean != null) {
            /* Mapper les listes pour entreprise */

            if (!CollectionUtils.isEmpty(entrepriseBean.getDirigeants())) {
                this.addDirigeant(entrepriseBean, iEntreprise);
            }

            if (!CollectionUtils.isEmpty(entrepriseBean.getEtablissements())) {
                this.addEtablissement(entrepriseBean, iEntreprise);
            }

            this.entrepriseCessationMapper.map(entrepriseBean, iEntreprise);
        }
        // FIN - Mapper Entreprise
        /* mapping champs simples FormaliteRegul. */
        this.formaliteCessationMapper.map(formaliteCessationBean, iFormaliteCassation);

    }

    /**
     * Ajoute le etablissement.
     *
     * @param entrepriseBean
     *            le entreprise bean
     * @param iEntreprise
     *            le i entreprise
     */
    private void addEtablissement(EntrepriseCessationBean entrepriseBean, IEntrepriseCessation iEntreprise) {

        /*
         * parcourir la liste des Etablissement existants sur l'interface XML et
         * les supprimer
         */
        IEtablissementCessation[] iEtablissementList = iEntreprise.getEtablissements();
        for (IEtablissementCessation iEtablissementCourant : iEtablissementList) {
            iEntreprise.removeFromEtablissements(iEtablissementCourant);
        }
        /*
         * parcourir la liste des Etablissement dans le Bean pour les rajouter à
         * l'interface
         */
        List<EtablissementCessationBean> etablissementListBean = entrepriseBean.getEtablissements();
        for (EtablissementCessationBean etablissementBean : etablissementListBean) {
            // Gestion idTechnique, nécessaire pour la gestion des impacts
            // (écran)
            if (etablissementBean.getIdTechnique() == null || etablissementBean.getIdTechnique().isEmpty()) {
                etablissementBean.setIdTechnique(UUID.randomUUID().toString());
            }

            IEtablissementCessation iEtablissement = iEntreprise.addToEtablissements();
            // DEBUT - Mapper Etablissement

            iEtablissement.newAdresse();
            iEtablissement.getAdresse().newCodePostalCommune();
            if (etablissementBean.getAdresse() == null) {
                etablissementBean.setAdresse(new AdresseBean());
            }
            this.adresseCMapper.map(etablissementBean.getAdresse(), iEtablissement.getAdresse());
            this.postalCommuneCMapper.map(etablissementBean.getAdresse().getCodePostalCommune(), iEtablissement.getAdresse().getCodePostalCommune());

            this.etablissementCessationMapper.map(etablissementBean, iEtablissement);
            // FIN - Mapper Etablissement
        }

    }

    /**
     * Ajoute le dirigeant.
     *
     * @param entrepriseBean
     *            le entreprise bean
     * @param iEntreprise
     *            le i entreprise
     */
    private void addDirigeant(EntrepriseCessationBean entrepriseBean, IEntrepriseCessation iEntreprise) {

        /*
         * parcourir la liste des Dirigeant existants sur l'interface XML et les
         * supprimer
         */
        IDirigeantCessation[] iDirigeantList = iEntreprise.getDirigeants();
        for (IDirigeantCessation iDirigeantCourant : iDirigeantList) {
            iEntreprise.removeFromDirigeants(iDirigeantCourant);
        }
        /*
         * parcourir la liste des Dirigeant dans le Bean pour les rajouter à
         * l'interface
         */
        List<DirigeantCessationBean> dirigeantListBean = entrepriseBean.getDirigeants();
        for (DirigeantCessationBean dirigeantBean : dirigeantListBean) {
            // Gestion idTechnique, nécessaire pour la gestion des impacts
            // (écran)
            if (dirigeantBean.getIdTechnique() == null || dirigeantBean.getIdTechnique().isEmpty()) {
                dirigeantBean.setIdTechnique(UUID.randomUUID().toString());
            }

            IDirigeantCessation iDirigeant = iEntreprise.addToDirigeants();
            // DEBUT - Mapper Dirigeant

            this.dirigeantCessationMapper.map(dirigeantBean, iDirigeant);
            // FIN - Mapper Dirigeant
        }

    }

    /**
     * Get le formalite cessation mapper.
     *
     * @return the formaliteCessationMapper
     */
    public Mapper getFormaliteCessationMapper() {
        return formaliteCessationMapper;
    }

    /**
     * Set le formalite cessation mapper.
     *
     * @param formaliteCessationMapper
     *            the formaliteCessationMapper to set
     */
    public void setFormaliteCessationMapper(Mapper formaliteCessationMapper) {
        this.formaliteCessationMapper = formaliteCessationMapper;
    }

    /**
     * Get le adresse c mapper.
     *
     * @return the adresseCMapper
     */

    public Mapper getAdresseCMapper() {
        return adresseCMapper;
    }

    /**
     * Set le adresse c mapper.
     *
     * @param adresseCMapper
     *            the adresseCMapper to set
     */

    public void setAdresseCMapper(Mapper adresseCMapper) {
        this.adresseCMapper = adresseCMapper;
    }

    /**
     * Get le entreprise cessation mapper.
     *
     * @return the entrepriseCessationMapper
     */
    public Mapper getEntrepriseCessationMapper() {
        return entrepriseCessationMapper;
    }

    /**
     * Set le entreprise cessation mapper.
     *
     * @param entrepriseCessationMapper
     *            the entrepriseCessationMapper to set
     */

    public void setEntrepriseCessationMapper(Mapper entrepriseCessationMapper) {
        this.entrepriseCessationMapper = entrepriseCessationMapper;
    }

    /**
     * Get le dirigeant cessation mapper.
     *
     * @return the dirigeantCessationMapper
     */
    public Mapper getDirigeantCessationMapper() {
        return dirigeantCessationMapper;
    }

    /**
     * Set le dirigeant cessation mapper.
     *
     * @param dirigeantCessationMapper
     *            the dirigeantCessationMapper to set
     */
    public void setDirigeantCessationMapper(Mapper dirigeantCessationMapper) {
        this.dirigeantCessationMapper = dirigeantCessationMapper;
    }

    /**
     * Get le etablissement cessation mapper.
     *
     * @return the etablissementCessationMapper
     */

    public Mapper getEtablissementCessationMapper() {
        return etablissementCessationMapper;
    }

    /**
     * Set le etablissement cessation mapper.
     *
     * @param etablissementCessationMapper
     *            the etablissementCessationMapper to set
     */
    public void setEtablissementCessationMapper(Mapper etablissementCessationMapper) {
        this.etablissementCessationMapper = etablissementCessationMapper;
    }

    /**
     * Get le postal commune c mapper.
     *
     * @return the postalCommuneCMapper
     */

    public Mapper getPostalCommuneCMapper() {
        return postalCommuneCMapper;
    }

    /**
     * Set le postal commune c mapper.
     *
     * @param postalCommuneCMapper
     *            the postalCommuneCMapper to set
     */
    public void setPostalCommuneCMapper(Mapper postalCommuneCMapper) {
        this.postalCommuneCMapper = postalCommuneCMapper;
    }

}
