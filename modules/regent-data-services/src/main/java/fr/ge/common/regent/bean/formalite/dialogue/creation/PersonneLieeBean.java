package fr.ge.common.regent.bean.formalite.dialogue.creation;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import fr.ge.common.regent.bean.formalite.dialogue.AbstractPersonneLieeBean;

/**
 * Le Class PersonneLieeBean.
 */
public class PersonneLieeBean extends AbstractPersonneLieeBean implements IFormaliteVue {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 590474845982062008L;

    /** Le id technique. */
    private String idTechnique;

    /**
     * Get le id technique.
     * 
     * @return the idTechnique
     */
    public String getIdTechnique() {
        return this.idTechnique;
    }

    /**
     * Set le id technique.
     * 
     * @param idTechnique
     *            the idTechnique to set
     */
    public void setIdTechnique(String idTechnique) {
        this.idTechnique = idTechnique;
    }

    /** Le personne pouvoir limite etablissement. */
    private String personnePouvoirLimiteEtablissement;

    /** Le pp civilite. */
    private String ppCivilite;

    /**
     * Getter de l'attribut personnePouvoirLimiteEtablissement.
     * 
     * @return la valeur de personnePouvoirLimiteEtablissement
     */
    public String getPersonnePouvoirLimiteEtablissement() {
        return this.personnePouvoirLimiteEtablissement;
    }

    /**
     * Getter de l'attribut ppCivilite.
     * 
     * @return la valeur de ppCivilite
     */
    public String getPpCivilite() {
        return this.ppCivilite;
    }

    /**
     * Setter de l'attribut personnePouvoirLimiteEtablissement.
     * 
     * @param personnePouvoirLimiteEtablissement
     *            la nouvelle valeur de personnePouvoirLimiteEtablissement
     */
    public void setPersonnePouvoirLimiteEtablissement(String personnePouvoirLimiteEtablissement) {
        this.personnePouvoirLimiteEtablissement = personnePouvoirLimiteEtablissement;
    }

    /**
     * Setter de l'attribut ppCivilite.
     * 
     * @param ppCivilite
     *            la nouvelle valeur de ppCivilite
     */
    public void setPpCivilite(String ppCivilite) {
        this.ppCivilite = ppCivilite;
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see fr.guichetentreprises.vue.AbstractPersonneLieeBean#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

}
