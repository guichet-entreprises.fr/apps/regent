/**
 * 
 */
package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Interface JGR.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
@ResourceXPath("/JGR")
public interface JGR {

  /**
   * Accesseur sur l'attribut {@link #j00}.
   *
   * @return j00
   */
  @FieldXPath("J00")
  String getJ00();

  /**
   * Mutateur sur l'attribut {@link #j00}.
   *
   * @param J00
   *          la nouvelle valeur de l'attribut j00
   */
  void setJ00(String J00);

}
