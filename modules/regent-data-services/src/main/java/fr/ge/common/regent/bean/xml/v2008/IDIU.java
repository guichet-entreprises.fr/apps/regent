package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Interface IDIU.
 */
@ResourceXPath("/DIU")
public interface IDIU {
  /**
   * Get le u50.
   *
   * @return le u50
   */
  @FieldXPath("U50")
  IU50[] getU50();

  /**
   * Ajoute le to u50.
   *
   * @return le u50
   */
  IU50 addToU50();

  /**
   * Set le u50.
   *
   * @param U50
   *          le nouveau u50
   */
  void setU50(IU50[] U50);

  /**
   * Get le u51.
   *
   * @return le u51
   */
  @FieldXPath("U51")
  IU51[] getU51();

  /**
   * Ajoute le to u51.
   *
   * @return le u51
   */
  IU51 addToU51();

  /**
   * Set le u51.
   *
   * @param U51
   *          le nouveau u51
   */
  void setU51(IU51[] U51);

  /**
   * Get le u52.
   *
   * @return le u52
   */
  @FieldXPath("U52")
  String getU52();

  /**
   * Set le u52.
   *
   * @param u52
   *          le nouveau u52
   */
  void setU52(String u52);
}
