package fr.ge.common.regent.bean.xml.v2016;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * L'Interface ISP.
 */
@ResourceXPath("/ISP")
public interface ISP {

  /**
   * Get le P90.
   *
   * @return le P90
   */
  @FieldXPath("P90")
  IP90[] getP90();

  /**
   * ajoute le p90.
   * 
   * @return le p90
   */
  IP90 addToP90();

  /**
   * Set le P90.
   *
   * @param P90
   *          le nouveau P90
   */
  void setP90(IP90[] P90);
}
