package fr.ge.common.regent.util.mapper.formalite.profil;

import java.util.ArrayList;

import org.apache.commons.collections.CollectionUtils;
import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xmlfield.core.types.XmlString;

import fr.ge.common.regent.bean.formalite.profil.creation.ProfilCreationBean;
import fr.ge.common.regent.bean.xml.formalite.profil.creation.IProfilCreation;
import fr.ge.common.regent.util.mapper.formalite.IMapperFormaliteUtils;

/**
 * Le Class ProfilEntrepriseXmlToBeanConverteur.
 */
@Component
public class MapperProfilCreationUtils implements IMapperFormaliteUtils<IProfilCreation, ProfilCreationBean> {

    /** Le profil entreprise mapper. */
    @Autowired
    private Mapper profilEntrepriseMapper;

    /** Le logger fonctionnel. */
    private static final Logger LOGGER_FONC = LoggerFactory.getLogger(MapperProfilCreationUtils.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public void mapper(IProfilCreation iFormalite, ProfilCreationBean formalite) {
        // mapper le profilbeanV2 vers profilBean
        this.profilEntrepriseMapper.map(iFormalite, formalite);

        // Mapper la liste des évènements
        if (formalite.getEvenement() != null) {
            formalite.getEvenement().clear();
        } else {
            formalite.setEvenement(new ArrayList<String>());
        }

        if (iFormalite.getEvenement() != null && iFormalite.getEvenement().length > 0) {
            for (XmlString evenement : iFormalite.getEvenement()) {
                formalite.getEvenement().add(evenement.getString());
            }
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mapper(ProfilCreationBean formalite, IProfilCreation iFormalite) {

        this.profilEntrepriseMapper.map(formalite, iFormalite);

        // Ajout des événements
        if (!CollectionUtils.isEmpty(formalite.getEvenement())) {
            LOGGER_FONC.info("Ajout des événements à la formalité {}.", formalite.getNumeroFormalite());
            this.addEvenements(formalite, iFormalite);
        }

    }

    /**
     * Ajoute les événements.
     *
     * @param formalite
     *            le bean affichable
     * @param iFormalite
     *            le bean XmlField
     */
    private void addEvenements(ProfilCreationBean formalite, IProfilCreation iFormalite) {
        // parcourir la liste des évenements existants sur l'interface XML et
        // les supprimer
        for (XmlString evenement : iFormalite.getEvenement()) {
            iFormalite.removeFromEvenement(evenement);
        }
        // parcourir la liste des évenements dans le Bean pour les rajouter à
        // l'interface
        for (String evenement : formalite.getEvenement()) {
            XmlString evenemntNew = iFormalite.addToEvenement();
            evenemntNew.setString(evenement);
            LOGGER_FONC.info("Ajout de l'événement {} à la formalité {}", evenement, iFormalite.getNumeroFormalite());
        }
    }
}