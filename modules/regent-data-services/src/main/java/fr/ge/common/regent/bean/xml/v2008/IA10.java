package fr.ge.common.regent.bean.xml.v2008;

import org.xmlfield.annotations.FieldXPath;
import org.xmlfield.annotations.ResourceXPath;

/**
 * Interface IA10.
 */
@ResourceXPath("/A10")
public interface IA10 {
	

	  /**
	   * Get le A101.
	   *
	   * @return le A101
	   */
	  @FieldXPath("A10.1")
	  String getA101();

	  /**
	   * Set le A101.
	   *
	   * @param A101
	   *          le nouveau A101
	   */
	  void setA101(String A101);

	  /**
	   * Get le A102.
	   *
	   * @return le A102
	   */
	  @FieldXPath("A10.2")
	  String getA102();

	  /**
	   * Set le A102.
	   *
	   * @param A102
	   *          le nouveau A102
	   */
	  void setA102(String A102);

}
