-- Table: norme_destinataire
-- DROP TABLE norme_destinataire;
CREATE TABLE norme_destinataire
(
  ch_num bigserial PRIMARY KEY,
  ch_role character varying(10),
  ch_destinataire character varying(5),
  ch_rubrique character varying(50)
);