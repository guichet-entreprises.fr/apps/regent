-- Table: normeflux
-- DROP TABLE normeflux;
CREATE SEQUENCE normeflux_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
 
CREATE TABLE normeflux
(
  ch_num bigint PRIMARY KEY,
  ch_poid integer,
  ch_evenement character varying(5),
  ch_cci character varying(3),
  ch_cma character varying(3),
  ch_urssaf character varying(3),
  ch_greffe character varying(3),
  ch_cnba character varying(3),
  ch_ca character varying(3),
  ch_role character varying(20),
  ch_type character varying(3),
  ch_libelle character varying(100),
  ch_agtcom character varying(3)
);