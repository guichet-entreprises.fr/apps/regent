CREATE TABLE regles_regent
(
  ch_num integer PRIMARY KEY,
  ch_path character varying(1048576) NOT NULL,
  ch_evaluation character varying(1048576),
  ch_occurences integer,
  ch_obligatoire boolean NOT NULL,
  ch_bourrage character varying(100),
  ch_version character varying(100),
  ch_rubrique character varying(100) NOT NULL,
  ch_endpath boolean NOT NULL
);