-- Table: norme_evenement
-- DROP TABLE norme_evenement;
CREATE TABLE norme_evenement
(
  ch_num bigserial PRIMARY KEY,
  ch_evenement character varying(100),
  ch_rubrique character varying(100),
  ch_version character varying(100),
  ch_condition boolean NOT NULL DEFAULT false
);