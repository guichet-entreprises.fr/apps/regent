-- Table: norme_evenement
-- Contains a list of the rubriques with a path for each one of them
-- DROP TABLE norme_rubriques;
CREATE TABLE norme_rubriques
(
	ch_num bigserial PRIMARY KEY,    -- Id of the rubrique
	ch_rubrique character varying(100), -- The rubrique
	ch_libelle character varying(500), -- Libelle of the rubrique
	ch_path character varying(500), -- the path of the Tag for this rubrique
	ch_groupe character varying(5), -- The group of the rubrique
	ch_condition character varying(5)-- Condition of the rubrique : VRAI or FALSE
);