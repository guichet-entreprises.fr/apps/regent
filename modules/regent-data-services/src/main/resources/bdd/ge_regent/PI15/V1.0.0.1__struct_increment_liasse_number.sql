-- Sequence: increment_liasse_number
-- A sequence for the numeber of liasse created

CREATE SEQUENCE "increment_liasse_number"
    INCREMENT 1
    START 1000000
    MINVALUE 1
    MAXVALUE 9999999
    CACHE 1;