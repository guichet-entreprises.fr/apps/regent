package org.formiz.context;

import org.springframework.expression.spel.support.StandardEvaluationContext;

/**
 * Configure le context d'évaluation EL.
 * <p>
 * Permet l'ajout de méthodes. Les méthodes sont déclarées dans le contexte Spring.
 * 
 * @author Nicolas Richeton
 * 
 */
public class ElContextBuilder extends AbstractContextBuilder {

  /**
   * Configure le contexte fourni en paramètre : ajout de méthodes, etc...
   * <p>
   * Cette fonctione doit être appelée sur tout nouveau contexte créé (StandardEvaluationContext).
   * <p>
   * Si le contexte est réutilisé, inutile d'appeler plusieurs fois cette méthode.
   * 
   * @param context
   *          contexte à configurer.
   */
  public void build(StandardEvaluationContext context) {
    for (String key : this.getDeclaredFunctions()) {
      context.registerFunction(key, this.getMethodes().get(key));
    }
  }

}
