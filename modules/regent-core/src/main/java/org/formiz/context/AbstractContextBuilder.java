package org.formiz.context;

import static org.apache.commons.lang3.StringUtils.substring;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Le Class AbstractContextBuilder.
 */
public abstract class AbstractContextBuilder {

    /** Le logger technique. */
    private static final Logger LOGGER_TECH = LoggerFactory.getLogger(AbstractContextBuilder.class);

    /** Le functions. */
    private Map<String, String> functions;

    /** Le methodes. */
    protected Map<String, Method> methodes;

    /**
     * Liste des fonctions déclarées.
     *
     * @return le declared functions
     */
    public Set<String> getDeclaredFunctions() {
        if (this.methodes == null) {
            return Collections.emptySet();
        }
        return this.methodes.keySet();
    }

    /**
     * Initialise le builder à partir de la configuration fournie via injection
     * (voir les setters).
     * <p>
     * Cette méthode doit être appelée automatiquement par spring (via
     * init-method)
     *
     * @throws ClassNotFoundException
     *             le class not found exception
     */
    public void init() throws ClassNotFoundException {
        this.methodes = new HashMap<String, Method>();

        for (String key : this.functions.keySet()) {
            String methodFullName = this.functions.get(key);

            String clazz = substring(methodFullName, 0, methodFullName.lastIndexOf('.'));
            String methodName = substring(methodFullName, methodFullName.lastIndexOf('.') + 1);

            Method method = extractMethodFromClass(clazz, methodName);

            LOGGER_TECH.debug("Enregistrement méthode {} -> {}.{}(...)", key, clazz, methodName);

            this.methodes.put(key, method);
        }
    }

    /**
     * Cherche une méthode au sein d'une classe.
     * 
     * @param clazz
     *            le nom de la classe dans laquelle chercher la méthode
     * @param methodName
     *            la méthode à chercher
     * @return une Method appelée methodName dans la class clazz
     * @throws ClassNotFoundException
     *             si la méthode ou la classe n'est pas trouvée
     */
    private Method extractMethodFromClass(final String clazz, final String methodName) throws ClassNotFoundException {
        Method[] allMethods = Class.forName(clazz).getMethods();
        for (Method m : allMethods) {
            if (StringUtils.equals(m.getName(), methodName)) {
                return m;
            }
        }

        throw new ClassNotFoundException("Methode " + methodName + " non trouvée sur la classe " + clazz);
    }

    /**
     * Liste des fonctions à injecter dans le contexte.
     * <p>
     * put( fonction , package.class.method)
     * <p>
     * Ex: put( "calculEstMajeurHorsUE",
     * "fr.guichetentreprises.context.FonctionsContexte.calculEstMajeurHorsUE"
     * );
     *
     * @param functions
     *            le functions
     */
    public void setFunctions(Map<String, String> functions) {
        this.functions = functions;
    }

    /**
     * Map des méthodes déclarées.
     *
     * @return le methodes
     */
    public Map<String, Method> getMethodes() {
        return methodes;
    }

}
