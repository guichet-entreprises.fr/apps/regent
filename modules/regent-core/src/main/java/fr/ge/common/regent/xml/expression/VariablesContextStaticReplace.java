package fr.ge.common.regent.xml.expression;

/**
 * <p>
 * Permet la compilation lors de l'utilisation des beans externes
 * </p>
 * .
 *
 * @author JISAVI
 */
public class VariablesContextStaticReplace extends BeansExternesStaticReplace {

  /** La constante FORM_REF_VARIABLE_STATIC_STATIC. */
  private static final String FORM_REF_VARIABLE_STATIC_STATIC = "#$1.$2";

  /** 
   * {@inheritDoc}
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.gipge.core.form.impl.expr.BeansExternesStaticReplace#getFormBeanExterneStatic()
   */
  @Override
  protected String getFormBeanExterneStatic() {
    return FORM_REF_VARIABLE_STATIC_STATIC;
  }

}
