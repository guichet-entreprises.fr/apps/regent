package fr.ge.common.regent.bean;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Le Class RootObject.
 */
public class RootObject implements IRoot {

    /** Le courant. */
    private Object courant;

    /** Le bean racine. */
    public Object beanRacine;

    /**
     * recupere le bean racine de type FormaliteBean.
     * 
     * @return bean racine
     */
    public Object getFormalite() {
        return this.beanRacine;
    }

    /**
     * Getter de l'attribut courant.
     * 
     * @return la valeur de courant
     */
    public Object getCourant() {
        return this.courant;
    }

    /**
     * Getter de l'attribut bean racine.
     * 
     * @return la valeur du bean racine
     */
    public Object getBeanRacine() {
        return this.beanRacine;
    }

    /**
     * Setter de l'attribut courant.
     * 
     * @param courant
     *            la nouvelle valeur de courant
     */
    @Override
    public void setCourant(Object courant) {
        this.courant = courant;
    }

    /**
     * Setter de l'attribut bean racine.
     * 
     * @param beanRacine
     *            la nouvelle valeur du bean racine
     */
    @Override
    public void setBeanRacine(Object beanRacine) {
        this.beanRacine = beanRacine;
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

}
