package fr.ge.common.regent.xml.expression;

/**
 * Remplace les patterns champ[ X ] par courant.X;
 * 
 * @author Nicolas Richeton
 * 
 */
public class ChampStaticReplace extends AbstractChampReplace {

  /**
   * Constructeur de classe.
   */
  public ChampStaticReplace() {
    super(FORM_REF, "courant.$1");
  }

}
