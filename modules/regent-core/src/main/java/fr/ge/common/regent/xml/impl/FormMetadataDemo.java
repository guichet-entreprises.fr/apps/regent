package fr.ge.common.regent.xml.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.expression.common.TemplateAwareExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;

import fr.ge.common.regent.core.form.DynamicVisibleElement;
import fr.ge.common.regent.core.form.FieldElement;
import fr.ge.common.regent.core.form.FormAjaxInputElement;
import fr.ge.common.regent.core.form.FormInputElement;
import fr.ge.common.regent.core.form.FormPopupInputElement;
import fr.ge.common.regent.core.form.SimpleFormElement;
import fr.ge.common.regent.core.form.ValidationElement;
import fr.ge.common.regent.core.input.InputSource;
import fr.ge.common.regent.core.navigation.Page;
import fr.ge.common.regent.xml.FormMetadata;
import fr.ge.common.regent.xml.SimpleElement;
import fr.ge.common.regent.xml.XmlFieldElement;

/**
 * Le Class FormMetadataDemo.
 */
public class FormMetadataDemo implements FormMetadata {

    /** Le logger technique. */
    private static final Logger LOGGER_TECH = LoggerFactory.getLogger(FormMetadataDemo.class);

    /** Le elements. */
    private final Map<String, SimpleElement> elements;

    /** Le elements by class. */
    private final Map<String, List<SimpleElement>> elementsByClass;

    /** Le elements by group. */
    private final Map<String, List<SimpleElement>> elementsByGroup;

    /** Le javascript template. */
    private String javascriptTemplate = "recupererValeur('$1')";

    /** Le parser. */
    private final SpelExpressionParser parser;

    /** Le sources. */
    private InputSource[] sources;

    /**
     * Constructeur de classe.
     */
    public FormMetadataDemo() {
        this.parser = new SpelExpressionParser();
        this.elements = new HashMap<String, SimpleElement>();
        this.elementsByGroup = new HashMap<String, List<SimpleElement>>();
        this.elementsByClass = new HashMap<String, List<SimpleElement>>();

    }

    /**
     * Ajoute un simple element.
     * 
     * @param el
     *            <code>SimpleElement</code>
     */
    private void addElement(final SimpleElement el) {
        String elementKey = el.getGroup() + "-" + el.getId();

        if (!this.elements.containsKey(elementKey)) {
            this.elements.put(elementKey, el);
            // Indexation par groupe
            List<SimpleElement> elementsOfGroup = this.elementsByGroup.get(el.getGroup());
            if (elementsOfGroup == null) {
                elementsOfGroup = new ArrayList<SimpleElement>();
                this.elementsByGroup.put(el.getGroup(), elementsOfGroup);
            }
            elementsOfGroup.add(el);

            // Indexation par classe
            String className = el.getClass().getName();
            List<SimpleElement> elementsOfClass = this.elementsByClass.get(className);
            if (elementsOfClass == null) {
                elementsOfClass = new ArrayList<SimpleElement>();
                this.elementsByClass.put(className, elementsOfClass);
            }
            elementsOfClass.add(el);
        } else {
            LOGGER_TECH.debug("L'element id:{} du groupe :{} est en doublon il n'as pas été inséré dans le modèle", el.getId(), el.getGroup());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addElement(final String id, final String group, final String visibility) {
        SimpleElement el = new SimpleFormElement(this, id, group, visibility);
        this.addElement(el);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addInputElement(final String id, final String group, final String label, final String aide, final String visibility, final String dynamicVisibility, final String type,
            final List<String> typeParam, final String mandatory, final String defaultValueDisplay, final String defaultValueDB, final List<DynamicVisibleElement> values,
            final String expressionListeFonction, final List<ValidationElement> validations) {
        SimpleElement el = new FormInputElement(this, id, group, label, aide, visibility, dynamicVisibility, type, typeParam, mandatory, defaultValueDisplay, defaultValueDB, values,
                expressionListeFonction, validations);

        this.addElement(el);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addAjaxInputElement(final String id, final String group, final String label, final String aide, final String visibility, final String dynamicVisibility, final String type,
            final List<String> typeParam, final String mandatory, final String defaultValueDisplay, final String defaultValueDB, final String valuesEl, final List<ValidationElement> validations) {
        SimpleElement el = new FormAjaxInputElement(this, id, group, label, aide, visibility, dynamicVisibility, type, typeParam, mandatory, defaultValueDisplay, defaultValueDB, valuesEl,
                validations);

        this.addElement(el);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addAjaxInputElement(final String id, final String group, final String label, final String aide, final String visibility, String dynamicVisibility, final String type,
            final List<String> typeParam, final String mandatory, final String defaultValueDisplay, final String defaultValueDB, final List<DynamicVisibleElement> valuesEl,
            List<ValidationElement> validations) {
        SimpleElement el = new FormAjaxInputElement(this, id, group, label, aide, visibility, dynamicVisibility, type, typeParam, mandatory, defaultValueDisplay, defaultValueDB, valuesEl,
                validations);

        this.addElement(el);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addPopupInputElement(final String id, final String group, final String label, final String aide, final String visibility, final String dynamicVisibility, final String type,
            final List<String> typeParam, final String mandatory, final String defaultValueDisplay, final String defaultValueDB, final List<DynamicVisibleElement> values,
            final String expressionListeFonction, final List<ValidationElement> validations, final String lienPopup) {
        SimpleElement el = new FormPopupInputElement(this, id, group, label, aide, visibility, dynamicVisibility, type, typeParam, mandatory, defaultValueDisplay, defaultValueDB, values,
                expressionListeFonction, validations, lienPopup);
        this.addElement(el);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addPage(final String id, final String group, final String inputGroup, final String label, final String aide, final String visibility, final String occurenceAnnounced,
            final String occurenceLimits, final String courant, final String initialisations, final String idFlow, final String preEnregistrements) {
        SimpleElement el = new Page(this, id, group, inputGroup, label, aide, visibility, occurenceAnnounced, null, occurenceLimits, courant, initialisations, idFlow, preEnregistrements);

        this.addElement(el);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addValidationElement(final String id, final String group, final String param, final String visibility) {
        SimpleElement el = new ValidationElement(this, id, group, param, visibility);
        this.addElement(el);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addVisibleElement(final String id, final String group, final String label, final String aide, final String visibility, final String dynamicVisibility) {
        SimpleElement el = new DynamicVisibleElement(this, id, group, label, aide, visibility, dynamicVisibility);

        this.addElement(el);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addFieldElement(final String id, final String group, final String valeur) {
        FieldElement el = new FieldElement(this, id, group, valeur);

        this.addElement(el);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addXmlFieldElement(final String id, final String group, final String valeur, final String path, final Integer occurence, final String rubrique, final Boolean obligatoire,
            final String bourrage, final boolean endPath, final String version) {
        final XmlFieldElement el = new XmlFieldElement(this, id, group, valeur, path, occurence, rubrique, obligatoire, bourrage, endPath, version);
        this.addElement(el);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SimpleElement getElement(final String group, final String id) {
        return this.elements.get(group + "-" + id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SimpleElement> getElementOfType(final String className, final String id) {
        List<SimpleElement> aElement = this.elementsByClass.get(className);

        if (aElement == null) {
            return null;
        }

        List<SimpleElement> result = new ArrayList<SimpleElement>();

        for (SimpleElement fe : aElement) {
            if (StringUtils.equals(id, fe.getId())) {
                result.add(fe);
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SimpleElement> getElementsByGroup(final String group) {
        return this.elementsByGroup.get(group);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SimpleElement> getElementsByGroupAndFlow(final String group, final String idFlow) {
        List<SimpleElement> list = this.elementsByGroup.get(group);
        List<SimpleElement> ret = new ArrayList<SimpleElement>();
        for (SimpleElement elem : list) {
            if (((Page) elem).getIdFlow().equals(idFlow)) {
                ret.add(elem);
            }
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SimpleElement> getElementsByGroupAndRubrique(final String group, final String rubrique) {

        List<SimpleElement> list = this.elementsByGroup.get(group);
        List<SimpleElement> ret = new ArrayList<SimpleElement>();
        for (SimpleElement elem : list) {
            if (((XmlFieldElement) elem).getRubrique().equals(rubrique)) {
                ret.add(elem);
            }
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SimpleElement> getElementsByGroupAndStartWithRubrique(final String group, final String rubrique, final String version) {

        List<SimpleElement> list = this.elementsByGroup.get(group);
        List<SimpleElement> ret = new ArrayList<SimpleElement>();
        for (SimpleElement elem : list) {
            if (StringUtils.startsWithIgnoreCase(((XmlFieldElement) elem).getRubrique(), rubrique) && version.equals(((XmlFieldElement) elem).getVersion())) {
                ret.add(elem);
            }
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getJavascriptTemplate() {
        return this.javascriptTemplate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TemplateAwareExpressionParser getParser() {
        return this.parser;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("boxing")
    @Override
    public void init() throws IOException {
        if (null != this.sources) {
            for (InputSource source : this.sources) {
                source.setFormMetadata(this);
                source.load();
            }

            LOGGER_TECH.debug("Init success: {} elements", this.elements.size());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setInputSources(final InputSource... sources) {
        this.sources = sources;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setJavascriptTemplate(final String jsTemplate) {
        this.javascriptTemplate = jsTemplate;

    }

}
