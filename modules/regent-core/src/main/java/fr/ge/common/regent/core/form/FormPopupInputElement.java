package fr.ge.common.regent.core.form;

import java.util.List;

import org.springframework.expression.Expression;

import fr.ge.common.regent.xml.FormMetadata;
import fr.ge.common.regent.xml.util.ExpressionParserUtil;

/**
 * Classe représentant un champ de type LabelLien.
 */
public class FormPopupInputElement extends FormInputElement {

    /** Le lien popup. */
    private final Expression lienPopup;

    /**
     * Constructeur.
     *
     * @param parent
     *            l'élément parent du champ
     * @param id
     *            l'id du champ
     * @param group
     *            le groupe parent du champ
     * @param label
     *            le libellé d'un champ
     * @param aide
     *            texte d'aide relative au champ
     * @param visibility
     *            visibilité static d'un champ
     * @param dynamicVisibility
     *            visibilité dynamique d'un champ
     * @param type
     *            le type du champ
     * @param typeParam
     *            paramètre du ty du champ
     * @param mandatory
     *            indique si le champ est obligatoire ou non
     * @param defaultValueDisplay
     *            valeur par défaut à l'affichage
     * @param defaultValueDB
     *            le default value db
     * @param values
     *            liste, s'il y en a, les valeurs possible à la selection d'un
     *            champ pour les radio bouton, checkbox, liste déroulante...
     * @param expressionListeFonction
     *            expression de fonction de recherche de la liste
     * @param validations
     *            liste, s'il y en a, les éléments modèlisant les conditions de
     *            validation de l'entrée d'un champ
     * @param lienPopup
     *            l'adresse vers laquelle il faut rediriger
     */
    public FormPopupInputElement(FormMetadata parent, String id, String group, String label, String aide, String visibility, String dynamicVisibility, String type, List<String> typeParam,
            String mandatory, String defaultValueDisplay, String defaultValueDB, List<DynamicVisibleElement> values, String expressionListeFonction, List<ValidationElement> validations,
            String lienPopup) {
        super(parent, id, group, label, aide, visibility, dynamicVisibility, type, typeParam, mandatory, defaultValueDisplay, defaultValueDB, values, expressionListeFonction, validations);
        this.lienPopup = ExpressionParserUtil.parser(id, group, parent.getParser(), lienPopup, performStaticBeansReplacements(lienPopup));
    }

    /**
     * Getter de l'attribut lienPopup.
     * 
     * @return la valeur de lienPopup
     */
    public Expression getLienPopup() {
        return this.lienPopup;
    }

}
