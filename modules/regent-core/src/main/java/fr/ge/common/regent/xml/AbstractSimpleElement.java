package fr.ge.common.regent.xml;

/**
 * Le Class AbstractSimpleElement.
 */
public class AbstractSimpleElement {

  /** Le group. */
  protected final String group;

  /** Le id. */
  protected final String id;

  /**
   * Instancie un nouveau abstract simple element.
   *
   * @param initGroup
   *          le init group
   * @param initId
   *          le init id
   */
  public AbstractSimpleElement(String initGroup, String initId) {
    super();
    group = initGroup;
    id = initId;
  }

  /**
   * Getter de l'attribut group.
   * 
   * @return la valeur de group
   */
  public String getGroup() {
    return this.group;
  }

  /**
   * Getter de l'attribut id.
   * 
   * @return la valeur de id
   */
  public String getId() {
    return this.id;
  }

}
