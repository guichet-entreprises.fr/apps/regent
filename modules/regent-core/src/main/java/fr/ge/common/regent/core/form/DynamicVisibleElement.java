package fr.ge.common.regent.core.form;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;

import fr.ge.common.regent.xml.FormMetadata;
import fr.ge.common.regent.xml.expression.BeansExternesStaticReplace;
import fr.ge.common.regent.xml.util.ExpressionParserUtil;

/**
 * Représente une entrée dans une liste de choix de réponses possibles dans un
 * champ de formulaire.
 * <p>
 * Ajoute les attributs nécessaire à la consultation des dépendances statiques
 * et dynamiques d'autre champs et à l'affichage dynamique côte front et côté
 * serveur.
 * 
 * 
 */
public class DynamicVisibleElement extends VisibleElement {

    /** La constante ACCOLADE_FERMANTE. */
    private static final String ACCOLADE_FERMANTE = "\\}";

    /** La constante ACCOLADE_OUVRANTE. */
    private static final String ACCOLADE_OUVRANTE = "\\{";

    /** La constante CROCHET_FERMANT. */
    private static final String CROCHET_FERMANT = "]";

    /** La constante CROCHET_OUVRANT. */
    private static final String CROCHET_OUVRANT = "[";

    /** La constante FORM_VISIBLE_REF. */
    protected static final Pattern FORM_VISIBLE_REF = Pattern.compile("champ\\[([A-Za-z0-9_\\.]+)\\]");

    /** La constante POINT. */
    protected static final String POINT = "\\.";

    /** La constante SIMPLE_QUOTE. */
    private static final String SIMPLE_QUOTE = "'";

    /** La constante TIRET. */
    protected static final String TIRET = "-";

    /** La constante VAR_1. */
    private static final String VAR_1 = "$1";

    /** La constante VIRGULE. */
    private static final String VIRGULE = ",";

    /**
     * Référence dynamique d'un champ.
     */
    private List<String> dynReferences;

    /** Visibilité dynamique d'un champ (pour le javascript). */
    private String dynVisibility;

    /**
     * Visibilité dynamique.
     */
    private final String dynVisibilityRaw;

    /**
     * Expression de la visibité du champ côté serveur (visibilité dynamique et
     * statique).
     */
    private Expression serverSideDynVisibility;

    /** Map des références statique du champ. */
    private final Map<String, Expression> staticReferences;

    /**
     * Constructeur.
     * 
     * @param parent
     *            les méta données du formulaire contenant l'élément
     * @param id
     *            identifiant de l'élément
     * @param group
     *            groupe d'appartenance de l'éléments
     * @param label
     *            libéllé à afficher de l'élément
     * @param aide
     *            aide relative à l'élément
     * @param visiilbity
     *            expression d'affichage statique de l'élément
     * @param dynamicVisibility
     *            expression d'affichage dynamique de l'élément
     */
    public DynamicVisibleElement(FormMetadata parent, String id, String group, String label, String aide, String visiilbity, String dynamicVisibility) {
        super(parent, id, group, visiilbity, label, aide);
        setParent(parent);

        // Préparer la visibilitée dynamique
        dynVisibilityRaw = dynamicVisibility;

        // Preparer la visibilité coté serveur
        if (dynamicVisibility != null) {
            setServerSideDynVisibility(ExpressionParserUtil.parser(id, group, parent.getParser(), dynamicVisibility, performStaticBeansReplacements(dynamicVisibility)));
        }

        staticReferences = new TreeMap<String, Expression>();

        if (dynamicVisibility != null) {
            StringBuffer sinit = new StringBuffer();

            Matcher m = BEAN_REF.matcher(dynamicVisibility);
            while (m.find()) {
                String grp = m.group(1);

                getStaticReferences().put(grp, ExpressionParserUtil.parser(id, group, parent.getParser(), grp, BEAN_REF_STATIC.replace(VAR_1, grp)));
            }

            m = CURRENT_BEAN_REF.matcher(dynamicVisibility);
            while (m.find()) {
                String grp = m.group(1);

                getStaticReferences().put(grp, ExpressionParserUtil.parser(id, group, parent.getParser(), grp, CURRENT_BEAN_REF_STATIC.replace(VAR_1, grp)));
            }

            m = PROFIL_BEAN_REF_WITH_FLOW.matcher(dynamicVisibility);
            while (m.find()) {
                String flow = m.group(1);
                String grp = m.group(2);

                getStaticReferences().put(flow + "." + grp, ExpressionParserUtil.parser(id, group, parent.getParser(), grp, new BeansExternesStaticReplace().perform(m.group())));
            }

            setDynReferences(new ArrayList<String>());
            m = FORM_VISIBLE_REF.matcher(dynamicVisibility);
            while (m.find()) {
                String grp = m.group(1).replaceAll(POINT, TIRET);
                getDynReferences().add(grp);
                m.appendReplacement(sinit, parent.getJavascriptTemplate());
            }
            m.appendTail(sinit);
            setDynVisibility(sinit.toString());
        }

    }

    /**
     * Récupère les références dynamiques d'un champ.
     * 
     * @param c
     *            le contexte d'évaluation
     * @return les références dynamiques
     */
    public List<String> getDynReferences(EvaluationContext c) {
        return this.dynReferences;
    }

    /**
     * Récupère la visibilité dynamique d'un champ (pour le Javascript).
     * 
     * @param c
     *            le contexte d'évaluation
     * @return la visibilité dynamique
     */
    public String getDynVisibility(EvaluationContext c) {
        if (this.dynVisibility == null) {
            return null;
        }

        Matcher m = BEAN_REF.matcher(this.dynVisibility.replaceAll(ACCOLADE_OUVRANTE, CROCHET_OUVRANT).replaceAll(ACCOLADE_FERMANTE, CROCHET_FERMANT));
        StringBuffer sinit = new StringBuffer();
        while (m.find()) {
            String grp = m.group(1);
            Object value = this.getStaticReferences().get(grp).getValue(c, Object.class);
            m.appendReplacement(sinit, transformerEvaluationEnString(value));
        }
        m.appendTail(sinit);

        StringBuffer sinit2 = new StringBuffer();
        Matcher current = CURRENT_BEAN_REF.matcher(sinit.toString().replaceAll(ACCOLADE_OUVRANTE, CROCHET_OUVRANT).replaceAll(ACCOLADE_FERMANTE, CROCHET_FERMANT));
        while (current.find()) {
            String grp = current.group(1);
            Object value = this.getStaticReferences().get(grp).getValue(c, Object.class);
            current.appendReplacement(sinit2, transformerEvaluationEnString(value));
        }
        current.appendTail(sinit2);

        StringBuffer sinit3 = new StringBuffer();
        Matcher profil = PROFIL_BEAN_REF_WITH_FLOW.matcher(sinit2.toString().replaceAll(ACCOLADE_OUVRANTE, CROCHET_OUVRANT).replaceAll(ACCOLADE_FERMANTE, CROCHET_FERMANT));
        while (profil.find()) {
            String flow = profil.group(1);
            String grp = profil.group(2);
            Object value = this.getStaticReferences().get(flow + "." + grp).getValue(c, Object.class);
            profil.appendReplacement(sinit3, transformerEvaluationEnString(value));
        }
        profil.appendTail(sinit3);

        return sinit3.toString();
    }

    /**
     * Transforme l'évaluation par le contexte en String.
     * 
     * @param evaluation
     *            l'évaluation par le contexte
     * @return la transformation en String
     */
    @SuppressWarnings("unchecked")
    private String transformerEvaluationEnString(Object evaluation) {
        String transformationString = StringUtils.EMPTY;
        if (evaluation instanceof Boolean || evaluation instanceof Number) {
            transformationString = "" + evaluation;
        } else if (evaluation instanceof List) {
            List<Object> listeEvaluee = (List<Object>) evaluation;
            StringBuilder tableauString = new StringBuilder();
            tableauString.append(CROCHET_OUVRANT);
            for (int i = 0; i < listeEvaluee.size(); ++i) {
                if (i != 0) {
                    tableauString.append(VIRGULE);
                }
                tableauString.append(transformerEvaluationEnString(listeEvaluee.get(i)));
            }
            tableauString.append(CROCHET_FERMANT);
            transformationString = tableauString.toString();
        } else {
            transformationString = SIMPLE_QUOTE + evaluation + SIMPLE_QUOTE;
        }
        return transformationString;
    }

    /**
     * <b>/!\ Ne doit être utilisé que dans CSVInputElementInputSource.load.
     * /!\</b>
     * <p>
     * Getter pour dynVisibilityRaw.
     * </p>
     * 
     * @return la visibilité dynamique
     */
    public String getDynVisibilityRaw() {
        return this.dynVisibilityRaw;
    }

    /**
     * Getter de l'attribut serverSideDynVisibility.
     * 
     * @return la valeur de serverSideDynVisibility
     */
    public Expression getServerSideDynVisibility() {
        return this.serverSideDynVisibility;
    }

    /**
     * Getter de l'attribut staticReferences.
     * 
     * @return la valeur de staticReferences
     */
    public Map<String, Expression> getStaticReferences() {
        return this.staticReferences;
    }

    /**
     * Vérifie la visibilité de l'élément côté serveur.
     * 
     * @param c
     *            contexte d'évaluation Spring
     * @return boolean
     */
    @SuppressWarnings("boxing")
    public boolean isVisibleServerSide(EvaluationContext c) {
        if (this.getServerSideDynVisibility() == null) {
            return isVisible(c);
        }

        return isVisible(c) && this.getServerSideDynVisibility().getValue(c, Boolean.class);

    }

    /**
     * Get le dyn references.
     *
     * @return the dynReferences
     */
    public List<String> getDynReferences() {
        return dynReferences;
    }

    /**
     * Set le dyn references.
     *
     * @param dynReferences
     *            the dynReferences to set
     */
    public void setDynReferences(List<String> dynReferences) {
        this.dynReferences = dynReferences;
    }

    /**
     * Get le dyn visibility.
     *
     * @return the dynVisibility
     */
    public String getDynVisibility() {
        return dynVisibility;
    }

    /**
     * Set le dyn visibility.
     *
     * @param dynVisibility
     *            the dynVisibility to set
     */
    public void setDynVisibility(String dynVisibility) {
        this.dynVisibility = dynVisibility;
    }

    /**
     * Set le server side dyn visibility.
     *
     * @param serverSideDynVisibility
     *            the serverSideDynVisibility to set
     */
    public void setServerSideDynVisibility(Expression serverSideDynVisibility) {
        this.serverSideDynVisibility = serverSideDynVisibility;
    }

    /**
     * Get le ajax dynamic element.
     *
     * @param id
     *            le id
     * @param label
     *            le label
     * @return le ajax dynamic element
     */
    public static DynamicVisibleElement getAjaxDynamicElement(String id, String label) {
        return new DynamicVisibleElement(null, id, "ajax", label, null, null, null);
    }

    /**
     * Get le ajax dynamic element.
     *
     * @return le ajax dynamic element
     */
    public static DynamicVisibleElement getAjaxDynamicElement() {
        return getAjaxDynamicElement("", "--");
    }

}
