package fr.ge.common.regent.bean;

/**
 * Le Interface IXmlRegent.
 *
 * @author bsadil
 * 
 *         Interface pour uniformiser les beans représentant un xmlélément.
 */
public interface IXmlRegent {

  /**
   * Get le id.
   *
   * @return l'id de la balise XMl
   */
  String getId();

  /**
   * Get le path.
   *
   * @return le chemain de balise de l'xml
   */
  String getPath();

  /**
   * Get le evaluation.
   *
   * @return le spel associé à la balsie
   */
  String getEvaluation();

  /**
   * Get le obligatoire.
   *
   * @return la balise si obligatoire ou pas
   */
  boolean getObligatoire();

  /**
   * Get le rubrique.
   *
   * @return la rubrique de chaque balise
   */
  String getRubrique();

  /**
   * Get le occurences.
   *
   * @return le nombre d'occurences de la balise
   */
  Integer getOccurences();

  /**
   * Get le bourrage.
   *
   * @return le Type de bourage de la balise
   */
  String getBourrage();

  /**
   * Get le end path.
   *
   * @return indique si c'est une balise ou une rubrique.
   */
  boolean getEndPath();

  /**
   * Accesseur sur l'attribut {@link #version}.
   *
   * @return version
   */
  String getVersion();

}
