package fr.ge.common.regent.xml;

import java.util.List;
import java.util.Map;

import org.springframework.expression.spel.support.StandardEvaluationContext;

import com.google.common.collect.Multimap;

/**
 * 
 * @author mtakerra
 *
 */
public interface IXmlWriter {

    /**
     * cette methode permet de retourner le setter de la balise à invoquer (exp:
     * setC02 pour la balise C02).
     * 
     * @param context
     *            le context speel pur evaluer la value de l'xmlFielElement
     * @param xmlFielElement
     *            l'objet xmlField qui contient le path de la balise sur lequel
     *            on se base pour construire le setter qui coorrespant à la
     *            balise
     * @return le setter de balise courante
     */
    String constructMethodToInvok(StandardEvaluationContext context, XmlFieldElement xmlFielElement);

    /**
     * cette methode permet de retourner le nom d'une balise xml à partir de son
     * chemain complet.
     * 
     * @param path
     *            le chemain complet de la balise xml
     * @return le nom de la balise
     */
    String constructBaliseFromPath(String path);

    /**
     * Récupère le flux XML en fonction de son group id, de sa version et de la
     * rubrique passée en paramètre.
     *
     * @param groupId
     *            pour notre cas ça sera tout le temps XmlRegent
     * @param rubrique
     *            la rubrique qui contient plusieurs balises
     * @param version
     *            version
     * @return le xml field element
     */
    List<XmlFieldElement> getXmlFieldElement(String groupId, String rubrique, String version);

    /**
     * Récupère le flux XML d'une version d'un group id.
     * 
     * @param groupId
     *            pour notre cas ça sera tout le temps XmlRegent
     * @param version
     *            la version du flux XML à récupérer
     * @return la list des xmlFieldOrdonnee à partir de la base
     */
    List<XmlFieldElement> getXmlFieldElement(String groupId, String version);

    /**
     * Get le list rubrique ordonne.
     * 
     * @param listRubriqueOrdonneFromDB
     *            la liste des xmlFieldElements ordonner par id à partir de la
     *            table regle_regent
     * @param listRubriqueByEvenementAndRole
     *            la liste des xmlFieldElements à partir des deux table
     *            norme_evenement et norme_destinataire
     * @return la liste des xmlFieldElements ordonné à generer par le moteur
     */
    List<XmlFieldElement> getListRubriqueOrdonne(List<XmlFieldElement> listRubriqueOrdonneFromDB, List<XmlFieldElement> listRubriqueByEvenementAndRole);

    /**
     * construire une map<rubrique,list<balise>> qui va nous permettre de gérer
     * le bourrage.
     * 
     * @param list
     *            de toutes les balises de l'xmlRegent
     * @return map<rubrique,list<balise>>
     */
    Multimap<String, XmlFieldElement> constructMapRubriqueBalises(List<XmlFieldElement> list);

    /**
     * @param context
     *            le context speel pur evaluer la value de l'xmlFielElement
     * @param multiMapRubriqueBalise
     *            map<rubrique,list<balise>>
     *
     * @return si on applique le bourrage pour une rubrique ou pas
     */
    Map<String, Boolean> bourageSurBaliseObligatoireVide(StandardEvaluationContext context, Multimap<String, XmlFieldElement> multiMapRubriqueBalise);

}
