package fr.ge.common.regent.core.form;

import java.util.regex.Pattern;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.EvaluationException;
import org.springframework.expression.Expression;

import fr.ge.common.regent.xml.FormMetadata;
import fr.ge.common.regent.xml.SimpleElement;
import fr.ge.common.regent.xml.expression.AutorisationsStaticReplace;
import fr.ge.common.regent.xml.expression.CourantStaticReplace;
import fr.ge.common.regent.xml.expression.FormaliteStaticReplace;
import fr.ge.common.regent.xml.expression.NullSafeStaticReplace;
import fr.ge.common.regent.xml.expression.VariablesContextStaticReplace;
import fr.ge.common.regent.xml.util.ExpressionParserUtil;

/**
 * Représente un champ éditable d'un PDF.
 * <p>
 * Ajoute les attributs nécessaire modèlisation d'un élément :
 * <ul>
 * <li>Identifiant</li>
 * <li>groupe d'appartenance</li>
 * <li>valeur du champ</li>
 * </ul>
 * 
 */
public class FieldElement extends SimpleElement {

    /** La constante A_STRING_BEAN_REF_STATIC. */
    private static final String A_STRING_BEAN_REF_STATIC = "formalite.$1";

    /** La constante ASTRING_BEAN_REF. */
    private static final String ASTRING_BEAN_REF = "formalite\\[([A-Za-z0-9_\\.]+)\\]";

    /**
     * formalite\\[([A-Za-z0-9_\\.]+)\\]
     */
    protected static final Pattern BEAN_REF = Pattern.compile(ASTRING_BEAN_REF);

    /**
     * formalite.$1
     */
    protected static final String BEAN_REF_STATIC = A_STRING_BEAN_REF_STATIC;

    /** Le value. */
    private Expression value;

    /**
     * Constructeur de classe. Il parse la valeur pour en faire une expression
     * interpretable par SPEL
     *
     * @param parent
     *            les méta données du formulaire contenant l'élément
     * @param id
     *            identifiant de l'élément
     * @param group
     *            groupe d'appartenance de l'éléments
     * @param value
     *            le value
     */
    public FieldElement(FormMetadata parent, String id, String group, String value) {
        super(parent, id, group);

        // Preparer la valeur
        if (value != null) {
            this.setValue(ExpressionParserUtil.parser(id, group, parent.getParser(), value, performStaticBeansReplacements(value)));
        }
    }

    /**
     * Remplace formalite[].
     * 
     * @param expression
     *            expression a parser
     * @return l'expression traité
     */
    protected static String performStaticBeansReplacements(String expression) {
        String result = expression;
        result = new FormaliteStaticReplace().perform(result); // formalite[]
        result = new VariablesContextStaticReplace().perform(result); // cfe[]
        result = new CourantStaticReplace().perform(result); // //result[]
        // ailleurs
        // que dans
        // le dialgue
        // cfe
        result = new AutorisationsStaticReplace().perform(result);
        result = new NullSafeStaticReplace().perform(result);
        return result;
    }

    /**
     * Getter.
     * 
     * @return l'expression de value
     */
    public Expression getValue() {
        return this.value;
    }

    /**
     * Getter de l'attribut value dans le context avec.
     *
     * @param c
     *            le c
     * @return la valeur de value
     * @throws EvaluationException
     *             le evaluation exception
     */
    public Object getValue(EvaluationContext c) throws EvaluationException {
        return this.getValue().getValue(c);
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see fr.gipge.core.SimpleElement#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    /**
     * Set le value.
     *
     * @param value
     *            the value to set
     */
    public void setValue(Expression value) {
        this.value = value;
    }

}
