package fr.ge.common.regent.xml.expression;

import java.util.regex.Pattern;

/**
 * Classe commune pour les remplacements basés sur champ[].
 * 
 * @author Nicolas Richeton
 * 
 */
public abstract class AbstractChampReplace extends GenericReplace {

  /** Pattern à remplacer. */
  protected static final Pattern FORM_REF = Pattern.compile("champ\\[([A-Za-z0-9_\\.]+)\\]");

  /**
   * Constructeur de classe.
   * 
   * @param pattern
   *          pattern à remplacer
   * @param replacement
   *          chaine de replacement
   */
  public AbstractChampReplace(Pattern pattern, String replacement) {
    super(pattern, replacement);
  }

}
