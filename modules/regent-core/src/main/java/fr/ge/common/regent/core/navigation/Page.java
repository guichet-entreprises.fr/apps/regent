package fr.ge.common.regent.core.navigation;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;

import fr.ge.common.regent.core.form.VisibleElement;
import fr.ge.common.regent.xml.FormMetadata;
import fr.ge.common.regent.xml.util.ExpressionParserUtil;

/**
 * Représente un page d'un formulaire.
 * <p>
 * Ajoute les attributs nécessaire à la présentation et la navigation sur une
 * page :
 * <ul>
 * <li>Nom de l'écran courant</li>
 * <li>Opération d'initialisation de la page</li>
 * <li>Opération à réaliser avant la sauvegarde de la page</li>
 * <li>Nombre d'occurence prévu</li>
 * <li>Occurence maximale (Informatif)</li>
 * <li>Occurence déjà réalisé (en session ou en base)</li>
 * <li>Identifiant du flow auquel appartient la page</li>
 * </ul>
 * 
 * @author Nicolas Richeton
 * 
 * 
 */
public class Page extends VisibleElement {

    /**
     * Nom de l'écran ou les information doivent être lues.
     */
    private String courant;

    /**
     * Expressions à utiliser sur l'objet courant à l'initialisation.
     */
    private List<Expression> initialisations;

    /**
     * Expressions à utiliser sur l'objet courant avant l'enregistrement.
     */
    private List<Expression> preEnregistrements;

    /**
     * nombre d'occurence prévu de la page.
     */
    private Expression occurenceAnnounced;

    /**
     * Nombre maximum d'affichage de l'écran.
     */
    private final String occurenceLimits;

    /**
     * Nombre d'occurence déjà présente de la page.
     */
    private Expression occurenceNoticed;

    /**
     * Identifiant du flow (enchainement d'écran) auquel la page appartient.
     */
    private final String idFlow;

    /**
     * identifiant du group des champs associé à la page.
     */
    private final String inputGroup;

    /**
     * Constructeur de la classe.
     * 
     * @param parent
     *            les méta données du formulaire contenant l'élément
     * @param id
     *            identifiant de la page
     * @param group
     *            représente la hierarchie d'écrans séparés par des point Ex
     *            pour AyantDroit1, group=dirigeant.declarationSociale -->
     *            l'écran ayantDroit1 est dans l'arborescence
     *            dirigeant/declarationSociale
     * @param inputGroup
     *            l'identifiant du groupe des champs de la page. Cette valeur
     *            peut être null
     * @param label
     *            titre de la page
     * @param aide
     *            aide relative à une page toute entière
     * @param visibility
     *            condition staitque d'affichage de la page
     * @param occurenceAnnounced
     *            expression du nombre d'occurences(appartition) prévus pour cet
     *            écran
     * @param occurenceNoticed
     *            nombre d'occurence de la page détecté (déjà présente en base
     *            ou en session)
     * @param occurenceLimits
     *            nombre d'occurence possible maximum (non utilisé)
     * @param courant
     *            nom du bean dans lequel les infos de la page doivent être lues
     * @param initialisations
     *            expression des actions à effectuer à l'initialisation de la
     *            page
     * @param idFlow
     *            identifiant flow courant
     * @param preEnregistrements
     *            expression des actions à effectuer avant l'enregistrement de
     *            la page
     */
    public Page(FormMetadata parent, String id, String group, String inputGroup, String label, String aide, String visibility, String occurenceAnnounced, String occurenceNoticed,
            String occurenceLimits, String courant, String initialisations, String idFlow, String preEnregistrements) {
        super(parent, id, group, visibility, label, aide);
        this.setParent(parent);
        this.setCourant(courant);
        this.occurenceLimits = occurenceLimits;
        this.idFlow = idFlow;
        this.inputGroup = inputGroup;

        // Preparer l'espression du nombre d'occurence annoncé
        if (occurenceAnnounced != null) {
            this.setOccurenceAnnounced(ExpressionParserUtil.parser(id, group, parent.getParser(), occurenceAnnounced, performStaticBeansReplacements(occurenceAnnounced)));
        }

        // Preparer l'espression du nombre d'occurence constaté
        if (occurenceNoticed != null) {
            this.setOccurenceNoticed(ExpressionParserUtil.parser(id, group, parent.getParser(), occurenceNoticed, performStaticBeansReplacements(occurenceNoticed)));
        }

        // Préparer les expressions des initialisations à
        // effectuerparent.getParser()
        if (initialisations != null) {
            String[] tabInitialisations = initialisations.split("\n");
            this.setInitialisations(new ArrayList<Expression>());
            for (String init : tabInitialisations) {
                this.initialisations.add(ExpressionParserUtil.parser(id, group, parent.getParser(), init, performStaticBeansReplacements(init)));
            }
        }

        // Préparer les expressions des actions à effectuer avant enregistrement
        if (preEnregistrements != null) {
            String[] tabPreEnregistrements = preEnregistrements.split("\n");
            this.setPreEnregistrements(new ArrayList<Expression>());
            for (String preEnregistrement : tabPreEnregistrements) {

                this.preEnregistrements.add(ExpressionParserUtil.parser(id, group, parent.getParser(), preEnregistrement, performStaticBeansReplacements(preEnregistrement)));
            }
        }

    }

    /**
     * Getter de l'attribut inputGroup.
     * 
     * @return l'inputGroup
     */
    public String getInputGroup() {
        return this.inputGroup;
    }

    /**
     * Getter de l'attribur idFlow.
     *
     * @return la valeur de l'idFlow
     */
    public String getIdFlow() {
        return this.idFlow;
    }

    /**
     * Getter de l'attribut courant.
     * 
     * @return la valeur de courant
     */
    public String getCourant() {
        return this.courant;
    }

    /**
     * Getter de l'attribut initialisations.
     * 
     * @return la valeur des initialisations
     */
    public List<String> getInitialisations() {
        List<String> listeExpe = new ArrayList<String>();
        if (CollectionUtils.isNotEmpty(this.initialisations)) {
            for (Expression expr : this.initialisations) {
                listeExpe.add(expr.getExpressionString());
            }
        }
        return listeExpe;
    }

    /**
     * Getter de l'attribut preEnregistrements.
     * 
     * @return la valeur des pre-enregistrements
     */
    public List<String> getPreEnregistrements() {
        List<String> listeExpe = new ArrayList<String>();
        if (CollectionUtils.isNotEmpty(this.preEnregistrements)) {
            for (Expression expr : this.preEnregistrements) {
                listeExpe.add(expr.getExpressionString());
            }
        }
        return listeExpe;
    }

    /**
     * Récupère le nombre total d'itération prévu sur la page.
     * 
     * @param c
     *            contexte d'valuation Spring
     * @return nombre d'itération total
     */
    public int getNbTotalIterationAnnonce(EvaluationContext c) {
        Integer value = this.getOccurenceAnnounced().getValue(c, Integer.class);
        if (value != null) {
            return value.intValue();
        } else {
            return 0;
        }

    }

    /**
     * Indique combien d'itération ont été réalisées sur la page.
     * 
     * @param c
     *            le contexte d'évaluation Spring
     * @return nombre d'occurence déjà réalisée dans le contexte
     */
    public int getNbTotalIterationRealise(EvaluationContext c) {
        List<?> liste = this.getOccurenceNoticed().getValue(c, List.class);
        return liste.size();
    }

    /**
     * Getter de l'attribut occurenceLimits.
     * 
     * @return la valeur de occurenceLimits
     */
    public String getOccurenceLimits() {
        return this.occurenceLimits;
    }

    /**
     * Indique si une page est itérable.
     * 
     * @return boolean
     */
    public boolean isIterable() {
        return this.getOccurenceAnnounced() != null;
    }

    /**
     * {@inheritDoc}
     */
    /*
     * (non-Javadoc)
     * 
     * @see fr.gipge.core.form.SimpleFormElement#isVisible(org.springframework.
     * expression.EvaluationContext )
     */
    @Override
    public boolean isVisible(EvaluationContext c) {
        boolean conditionVisibility = super.isVisible(c);
        boolean iterationVisibility = true;
        if (this.getOccurenceAnnounced() != null && this.getOccurenceNoticed() != null) {
            iterationVisibility = this.getNbTotalIterationRealise(c) < this.getNbTotalIterationAnnonce(c);
        }
        if (this.getOccurenceAnnounced() != null && this.getOccurenceAnnounced().getValue(c, Integer.class) != null && this.getNbTotalIterationAnnonce(c) < 1) {
            iterationVisibility = false;
        }
        return conditionVisibility && iterationVisibility;

    }

    /**
     * Setter de l'attribut courant.
     * 
     * @param courant
     *            la nouvelle valeur de courant
     */
    public void setCourant(String courant) {
        this.courant = courant;
    }

    /**
     * Get le occurence announced.
     *
     * @return the occurenceAnnounced
     */
    public Expression getOccurenceAnnounced() {
        return this.occurenceAnnounced;
    }

    /**
     * Set le occurence announced.
     *
     * @param occurenceAnnounced
     *            the occurenceAnnounced to set
     */
    public void setOccurenceAnnounced(Expression occurenceAnnounced) {
        this.occurenceAnnounced = occurenceAnnounced;
    }

    /**
     * Get le occurence noticed.
     *
     * @return the occurenceNoticed
     */
    public Expression getOccurenceNoticed() {
        return this.occurenceNoticed;
    }

    /**
     * Set le occurence noticed.
     *
     * @param occurenceNoticed
     *            the occurenceNoticed to set
     */
    public void setOccurenceNoticed(Expression occurenceNoticed) {
        this.occurenceNoticed = occurenceNoticed;
    }

    /**
     * Set le initialisations.
     *
     * @param initialisations
     *            the initialisations to set
     */
    public void setInitialisations(List<Expression> initialisations) {
        this.initialisations = initialisations;
    }

    /**
     * Set le pre enregistrements.
     *
     * @param preEnregistrements
     *            the preEnregistrements to set
     */
    public void setPreEnregistrements(List<Expression> preEnregistrements) {
        this.preEnregistrements = preEnregistrements;
    }
}
