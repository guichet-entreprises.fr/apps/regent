package fr.ge.common.regent.xml.expression;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Remplace les patterns parent[ X.Y ] par getParent(X).Y;
 * 
 */
public class ParentReplace {

  /**
   * parent\\[([A-Za-z0-9_\\.]+)\\. Ex formalite[reseauCFE]!='A' &&
   * parent[dirigeant.conjointPresence]=='oui'
   */
  protected static final Pattern FORM_REF_PARENT = Pattern.compile("parent\\[([A-Za-z0-9_\\.]+)\\.");

  /**
   * formalite.reseauCFE!='A' && getParent('dirigeant').conjointPresence=='oui'
   */
  protected static final String FORM_REF_PARENT_STATIC = "getParent('$1').";

  /**
   * (stack\\.\\?\\[class.name\\.contains\\(#capitalize\\(
   * '[A-Za-z0-9_\\.]+'\\)\\)\\]\\[0\\]\\.[A-Za-z0-9_\\.]+)\\] Ex : formalite.reseauCFE!='A' &&
   * stack.?[class.name=='dirigeant'].conjointPresence]=='oui'
   */
  private static final Pattern FORM_REF_PARENT_STATIC_2 = Pattern
    .compile("(getParent\\('[A-Za-z0-9_\\.]+'\\)\\.[A-Za-z0-9_\\.]+)\\]");

  /** La constante VAR_1. */
  private static final String VAR_1 = "$1"; 

  /**
   * Execute les remplassement sur l'expression.
   *
   * @param expr
   *          expression a reformatter
   * @return le string
   */
  public String perform(String expr) {
    String result;
    // gestion des parents
    Matcher m = FORM_REF_PARENT.matcher(expr);
    result = m.replaceAll(FORM_REF_PARENT_STATIC);

    m = FORM_REF_PARENT_STATIC_2.matcher(result);
    result = m.replaceAll(VAR_1);

    return result;
  }
}
