package fr.ge.common.regent.bean;

/**
 * Le Interface IRoot.
 */
public interface IRoot {

  /**
   * Set le courant.
   *
   * @param courant
   *          le nouveau courant
   */
  void setCourant(Object courant);

  /**
   * Set le bean racine.
   *
   * @param beanRacine
   *          le nouveau bean racine
   */
  void setBeanRacine(Object beanRacine);

}
