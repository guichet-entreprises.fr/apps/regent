package fr.ge.common.regent.core.form;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;

import fr.ge.common.regent.xml.FormMetadata;
import fr.ge.common.regent.xml.expression.AutorisationsStaticReplace;
import fr.ge.common.regent.xml.expression.BeansExternesStaticReplace;
import fr.ge.common.regent.xml.expression.ChampAjaxReplace;
import fr.ge.common.regent.xml.expression.ChampStaticReplace;
import fr.ge.common.regent.xml.expression.FormaliteStaticReplace;
import fr.ge.common.regent.xml.expression.GenericReplace;
import fr.ge.common.regent.xml.expression.ProfilStaticReplace;
import fr.ge.common.regent.xml.util.ExpressionParserUtil;

/**
 * Champ dont les valeurs sont calculées dynamiquement coté serveur.
 * <p>
 * Aujourd'hui, seuls les listes sont supportées et les champs dynamiques ajax.
 * Les éléments contenant à la fois des champs dynamiques ajax et des listes
 * ajax ne sont pas supportées, refacto à venir
 * 
 * @author Nicolas Richeton
 * 
 */
public class FormAjaxInputElement extends FormInputElement {

    /** Le ajax dyn references. */
    List<String> ajaxDynReferences;

    /** Le ajax value raw. */
    private String ajaxValueRaw;

    /** Le ajax values. */
    private Expression ajaxValues;

    /** Le values. */
    private Expression values;

    /**
     * Expression de la visibilité ajax du champ, 'courant' n'est pas remplacé
     * dans cette expression.
     */
    private Expression ajaxServerSideDynVisibility;

    /**
     * Construteur.
     * 
     * @param parent
     *            les méta données du formulaire contenant l'élément
     * @param id
     *            identifiant de l'élément
     * @param group
     *            groupe d'appartenance de l'éléments
     * @param label
     *            libéllé à afficher de l'élément
     * @param aide
     *            aide relative à l'élément
     * @param visibility
     *            expression d'affichage statique de l'élément
     * @param dynamicVisibility
     *            expression d'affichage dynamique de l'élément
     * @param type
     *            le format du champ. Il doit correspondre au type supporté par
     *            FormMetadata dans <code>InputType</code>
     * @param typeParam
     *            paramètre du format
     * @param mandatory
     *            expression d'obligation du champ
     * @param defaultValueDisplay
     *            expression de la valeur par defaut à l'affichage du champ
     * @param defaultValueDB
     *            expression de la valeur du champ s'il n'est pas affiché ou
     *            laissé vide
     * @param values
     *            expression de la methode static à appeler pour calculer les
     *            valeurs
     * @param validations
     *            les validations à effectuer sur le champ
     */
    public FormAjaxInputElement(FormMetadata parent, String id, String group, String label, String aide, String visibility, String dynamicVisibility, String type, List<String> typeParam,
            String mandatory, String defaultValueDisplay, String defaultValueDB, String values, List<ValidationElement> validations) {
        // Créer un input element sans liste.
        super(parent, id, group, label, aide, visibility, dynamicVisibility, type, typeParam, mandatory, defaultValueDisplay, defaultValueDB, null, null, validations);

        if (values != null && values.contains("#")) {
            // Ajax version
            this.ajaxValueRaw = values;
            GenericReplace op = new ChampAjaxReplace();
            String newValue = op.perform(values);
            this.ajaxDynReferences = op.getMatchedItems();
            newValue = new FormaliteStaticReplace().perform(newValue);
            // Profil Entreprise FIXME Revue N. Richeton
            newValue = new BeansExternesStaticReplace().perform(newValue);
            newValue = new AutorisationsStaticReplace().perform(newValue);

            this.ajaxValues = ExpressionParserUtil.parser(id, group, parent.getParser(), newValue, newValue);

            // Server-side version
            newValue = new ChampStaticReplace().perform(values);
            newValue = new FormaliteStaticReplace().perform(newValue);
            newValue = new BeansExternesStaticReplace().perform(newValue);

            // Profil Entreprise
            newValue = new ProfilStaticReplace().perform(newValue);

            this.values = ExpressionParserUtil.parser(id, group, parent.getParser(), newValue, newValue);

        }

    }

    /**
     * Constructeur avec l'expression dynamique uniquement de type ajax.Pas
     * besoin d'évaluer les fonctions enregistrées si la visibilité dynamique
     * contient des appels ajax, toute la visibilité dynamique sera évaluée en
     * ajax côté serveur dans ajaxController
     * 
     * @param parent
     *            les méta données du formulaire contenant l'élément
     * @param id
     *            identifiant de l'élément
     * @param group
     *            groupe d'appartenance de l'éléments
     * @param label
     *            libéllé à afficher de l'élément
     * @param aide
     *            aide relative à l'élément
     * @param visibility
     *            expression d'affichage statique de l'élément
     * @param dynamicVisibility
     *            expression d'affichage dynamique de l'élément
     * @param type
     *            le format du champ. Il doit correspondre au type supporté par
     *            FormMetadata dans <code>InputType</code>
     * @param typeParam
     *            paramètre du format
     * @param mandatory
     *            indique si le champ est obligatoire ou non
     * @param defaultValueDisplay
     *            expression de la valeur par defaut à l'affichage du champ
     * @param defaultValueDB
     *            expression de la valeur du champ s'il n'est pas affiché ou
     *            laissé vide
     * @param values
     *            liste des éléments de la liste
     * @param validations
     *            les validations à effectuer sur le champ
     */
    public FormAjaxInputElement(FormMetadata parent, String id, String group, String label, String aide, String visibility, String dynamicVisibility, String type, List<String> typeParam,
            String mandatory, String defaultValueDisplay, String defaultValueDB, List<DynamicVisibleElement> values, List<ValidationElement> validations) {
        // Créer un input element sans liste et sans visibilite dynamique.
        super(parent, id, group, label, aide, visibility, dynamicVisibility, type, typeParam, mandatory, defaultValueDisplay, defaultValueDB, values, null, validations);
        if (StringUtils.isNotBlank(dynamicVisibility) && dynamicVisibility.contains("#")) {
            // On a besoin de performer courant et formalite dans l'expression
            // de visibilité
            // dynamique, afin de remplacer champ avec les valeurs saisies à
            // l'écran
            GenericReplace op = new ChampAjaxReplace();
            String newValue = op.perform(dynamicVisibility);
            newValue = new FormaliteStaticReplace().perform(newValue);
            newValue = new BeansExternesStaticReplace().perform(newValue);
            newValue = new AutorisationsStaticReplace().perform(newValue);
            this.ajaxServerSideDynVisibility = ExpressionParserUtil.parser(id, group, parent.getParser(), newValue, newValue);

        }

    }

    /**
     * retourne null Si il s'agit d'une liste ajax, la visibilité si il s'agit
     * d'une règle dynamique ajax.
     *
     * @return le ajax server side dyn visibility
     */
    public Expression getAjaxServerSideDynVisibility() {
        return this.ajaxServerSideDynVisibility;
    }

    /**
     * Recalcul de la visibilité serveur side pour Ajax, l'expression dynamique
     * évaluée ici ne contient pas de "courant" mais les valeurs réellement
     * saisies et passées à Ajax controlleur.
     *
     * @param c
     *            le c
     * @return true, si c'est ajax server side dyn visibility
     */
    public boolean isAjaxServerSideDynVisibility(EvaluationContext c) {
        if (this.ajaxServerSideDynVisibility == null) {
            return isVisible(c);
        }

        return isVisible(c) && this.ajaxServerSideDynVisibility.getValue(c, Boolean.class);

    }

    /**
     * Getter de l'attribut ajaxDynReferences.
     * 
     * @return la valeur de ajaxDynReferences
     */
    public List<String> getAjaxDynReferences() {
        return this.ajaxDynReferences;
    }

    /**
     * Retourne la liste des champs succeptibles de modifier l'affichage de ce
     * champ.
     * <p>
     * En complément d'implémentation de
     * {@link DynamicVisibleElement#getDynReferences(EvaluationContext)}, cette
     * implémentation complète la liste avec les références utilisées pour
     * calculer les valeurs dans le cadre d'appels AJAX. la page.
     *
     * @param c
     *            le c
     * @return le dyn references
     * @see fr.gipge.core.form.DynamicVisibleElement#getDynReferences(org.springframework.expression.EvaluationContext)
     */
    @Override
    public List<String> getDynReferences(EvaluationContext c) {
        List<String> refs = super.getDynReferences(c);

        if (this.ajaxDynReferences == null) {
            return refs;
        }

        refs = refs == null ? new ArrayList<String>() : new ArrayList<String>(refs);
        refs.addAll(this.ajaxDynReferences);

        return refs;
    }

    /**
     * Retourne les valeurs possible dans le contexte fourni.
     *
     * @param c
     *            le c
     * @return liste des valeurs possible.
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<DynamicVisibleElement> getValues(EvaluationContext c) {
        // Cas des élément ajax dans des champs dynamiques pour une liste
        if (this.values == null) {
            return super.getValues();
        }
        return this.values.getValue(c, List.class);

    }

    /**
     * Retourne les valeurs possible dans le contexte fourni.
     * <p>
     * A utiliser dans le contexte d'une requête Ajax.
     *
     * @param c
     *            le c
     * @return liste des valeurs possible.
     */
    @SuppressWarnings("unchecked")
    public List<DynamicVisibleElement> getValuesAjax(EvaluationContext c) {
        return this.ajaxValues.getValue(c, List.class);
    }

    /**
     * <b>/!\ Ne doit être utilisé que dans CSVInputElementInputSource.load.
     * /!\</b>
     * <p>
     * Getter pour values, dans le cadre d'un element AJAX.
     * </p>
     *
     * @return le values expression
     */
    public String getValuesExpression() {
        return this.ajaxValueRaw;
    }

}
