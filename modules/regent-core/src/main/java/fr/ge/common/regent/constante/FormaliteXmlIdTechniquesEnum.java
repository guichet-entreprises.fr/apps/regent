package fr.ge.common.regent.constante;

/**
 * Le Enum FormaliteXmlIdTechniquesEnum.
 */
public enum FormaliteXmlIdTechniquesEnum {

  /** Le cfe. */
  CFE("formalites"),

  /** Le profil. */
  PROFIL("profilEntreprises"),

  /** Le profil regularisation. */
  PROFIL_REGULARISATION("profilEntrepriseR"),

  /** Le profil cessation. */
  PROFIL_CESSATION("profilEntrepriseC"),

  /** Le formalite regularisation. */
  FORMALITE_REGULARISATION("formalitesRegularisation"),

  /** Le debit boisson. */
  DEBIT_BOISSON("demande_licence_demande_debit_boisson"),

  /** Le autorisation desossage. */
  AUTORISATION_DESOSSAGE("autorisation_desossage"),

  /** Le delcaration sanitaire. */
  DELCARATION_SANITAIRE("declaration_sanitaire"),

  /** Le delcaration de location de chambre d hote. */
  DELCARATION_DE_LOCATION_DE_CHAMBRE_D_HOTE("declaration_de_location_de_chambre_d_hote"),

  /** Le delcaration des meubles de tourisme. */
  DELCARATION_DES_MEUBLES_DE_TOURISME("declaration_des_meubles_de_tourisme"),

  /** Le declaration antiquaire. */
  DECLARATION_ANTIQUAIRE("declaration_prealable_tenue_objets_mobiliers"),

  /** Le declaration domiciliation. */
  DECLARATION_DOMICILIATION("declaration_domiciliation"),

  /** Le profil modification. */
  PROFIL_MODIFICATION("profilEntrepriseM"),

  /** Le formalite modification. */
  FORMALITE_CREATION("formalites"),

  /** profil creation. */
  PROFIL_CREATION("profilEntreprises"),

  /** Le formalite modification. */
  FORMALITE_MODIFICATION("formalitesModification"),

  /** Le formalite cessation. */
  FORMALITE_CESSATION("formalitesRadiation"),;

  /** Le id technique. */
  private String idTechnique;

  /**
   * Instancie un nouveau formalite xml id techniques enum.
   *
   * @param idTechnique
   *          le id technique
   */
  FormaliteXmlIdTechniquesEnum(final String idTechnique) {
    this.idTechnique = idTechnique;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    return this.idTechnique;
  }

  /**
   * From id technique.
   *
   * @param id
   *          l'id recherché
   * @return FormaliteXmlIdTechniquesEnum avec l'id recherché, null sinon.
   */
  public static FormaliteXmlIdTechniquesEnum fromIdTechnique(final String id) {
    for (FormaliteXmlIdTechniquesEnum idTech : FormaliteXmlIdTechniquesEnum.values()) {
      if (idTech.toString().equals(id)) {
        return idTech;
      }
    }
    return null;
  }

}
