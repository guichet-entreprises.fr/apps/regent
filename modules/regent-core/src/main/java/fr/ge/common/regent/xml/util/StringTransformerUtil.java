package fr.ge.common.regent.xml.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

/**
 * Le Class StringTransformer.
 */
public final class StringTransformerUtil {

    /** La constante SEPARATOR_KEY_VALUE. */
    private static final String SEPARATOR_KEY_VALUE = ":";

    /** La constante SEPARATOR. */
    private static final String SEPARATOR = ";";

    /**
     * Constructeur de la classe.
     *
     */
    private StringTransformerUtil() {
        super();
    }

    /**
     * Transforme une string en une map.<br>
     * Le format de la chaîne de caractères <code>couples</code> doit être :<br>
     * <code>key1:value1;key2:value2;key3:value3</code><br>
     * <br>
     * La map retournée sera initialisé en utilisant les valeurs de la chaîne de
     * caractères :<br>
     * <code>mapToInit[key1] = value1</code><br>
     * <code>mapToInit[key2] = value2</code><br>
     * <code>mapToInit[key3] = value3</code><br>
     * <br>
     * Si <code>couples</code> est null ou vide, la map sera initialisé mais
     * sera vide.<br>
     *
     * @param couples
     *            chaîne de caractère contenant les clefs et les valeurs
     *            correspondantes
     * @param separatorCouples
     *            separateur custom entre les couples de valeurs
     * @param separatorKeyValue
     *            separateur custom entre la clef et la valeur correspondante
     * @param keyUpperCase
     *            si true la clef subit un upperCase
     * @param valueUpperCase
     *            si true la valeur subit un upperCase
     * @return une map contenant les valeurs de la chaîne de caractères
     * @throws IllegalArgumentException
     *             le illegal argument exception
     */
    public static Map<String, String> stringToMap(final String couples, final String separatorCouples, final String separatorKeyValue, final boolean keyUpperCase, final boolean valueUpperCase)
            throws IllegalArgumentException {
        Map<String, String> mapToInit = new HashMap<String, String>();

        if (!StringUtils.isBlank(couples)) {
            String[] coupleKeyValue = couples.split(separatorCouples);
            if (coupleKeyValue.length != 0) {
                String key = null;
                String value = null;
                for (String detailsKeyValue : coupleKeyValue) {
                    String[] details = detailsKeyValue.split(separatorKeyValue);
                    if (details.length == 2) {
                        key = keyUpperCase ? details[0].toUpperCase() : details[0];
                        value = valueUpperCase ? details[1].toUpperCase() : details[1];
                        mapToInit.put(key.trim(), value.trim());
                    } else {
                        throw new IllegalArgumentException(
                                "Erreur lors de l'initialisation d'une map. " + "La chaîne de caractères n'est pas au bon format : " + couples + ". Format attendue : \"key1:value1;key2:value2\"");
                    }
                }
            }
        }

        return mapToInit;
    }

    /**
     * Appel la méthode
     * <code>Transformer.stringToMap(String, false, false).</code>
     *
     * @param couples
     *            le couples
     * @return le map
     * @throws IllegalArgumentException
     *             le illegal argument exception
     * @see #stringToMap(String, boolean, boolean)
     */
    public static Map<String, String> stringToMap(final String couples) throws IllegalArgumentException {
        return stringToMap(couples, false, false);
    }

    /**
     * Appel la méthode
     * <code>Transformer.stringToMap(String, SEPARATOR, SEPARATOR_KEY_VALUE, boolean, boolean).</code>
     *
     * @param couples
     *            le couples
     * @param keyUpperCase
     *            le key upper case
     * @param valueUpperCase
     *            le value upper case
     * @return le map
     * @throws IllegalArgumentException
     *             le illegal argument exception
     * @see #stringToMap(String, String, String, boolean, boolean)
     */
    public static Map<String, String> stringToMap(final String couples, final boolean keyUpperCase, final boolean valueUpperCase) throws IllegalArgumentException {
        return stringToMap(couples, SEPARATOR, SEPARATOR_KEY_VALUE, keyUpperCase, valueUpperCase);
    }

    /**
     * Transforme une string en une liste.<br>
     * Le format de la chaîne de caractères <code>couples</code> doit être :<br>
     * <code>value1;value2;value3</code><br>
     * <br>
     * La liste <code>listToInit</code> sera initialisé en utilisant les valeurs
     * de la chaîne de caractères :<br>
     * <code>liste.get(0) = value1</code><br>
     * <code>liste.get(1) = value2</code><br>
     * <code>liste.get(2) = value3</code><br>
     * <br>
     * Si <code>couples</code> est null ou vide, la liste sera initialisé mais
     * sera vide.<br>
     *
     * @param couples
     *            chaîne de caractère contenant les valeurs
     * @param separator
     *            separateur custom entre les valeurs
     * @param valueUpperCase
     *            si true la valeur subit un upperCase
     * @return une liste contenant les valeurs de la chaîne de caractères
     * @throws IllegalArgumentException
     *             le illegal argument exception
     */
    public static List<String> stringToList(final String couples, final String separator, final boolean valueUpperCase) throws IllegalArgumentException {
        List<String> listToInit = new ArrayList<String>();

        if (!StringUtils.isBlank(couples)) {
            String[] codesEdi = couples.split(separator);
            String value = null;
            for (String codeEdi : codesEdi) {
                if (!StringUtils.isEmpty(codeEdi)) {
                    value = valueUpperCase ? codeEdi.toUpperCase() : codeEdi;
                    listToInit.add(value.trim());
                } else {
                    throw new IllegalArgumentException(
                            "Erreur lors de l'initialisation d'une liste. " + "La chaîne de caractères n'est pas au bon format : " + couples + ". Format attendue : \"value1;value2\"");
                }
            }
        }
        return listToInit;
    }

    /**
     * Appel la méthode <code>Transformer.stringToList(String, false).</code>
     *
     * @param couples
     *            le couples
     * @return le list
     * @throws IllegalArgumentException
     *             le illegal argument exception
     * @see #stringToList(String, boolean)
     */
    public static List<String> stringToList(final String couples) throws IllegalArgumentException {
        return stringToList(couples, false);
    }

    /**
     * Appel la méthode
     * <code>Transformer.stringToList(String, SEPARATOR, boolean).</code>
     *
     * @param couples
     *            le couples
     * @param valueUpperCase
     *            le value upper case
     * @return le list
     * @throws IllegalArgumentException
     *             le illegal argument exception
     * @see #stringToList(String, String, boolean)
     */
    public static List<String> stringToList(final String couples, final boolean valueUpperCase) throws IllegalArgumentException {
        return stringToList(couples, SEPARATOR, valueUpperCase);
    }
}
