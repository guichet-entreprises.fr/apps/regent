/**
 * 
 */
package fr.ge.common.regent.constante;

/**
 * Propertiers en communs : valeur par défault.
 * 
 * @author $Author: hhichri $
 * @version $Revision: 0 $
 */
public interface ICommonConstante {

    /** jdbc.regent.jndi.name=java:comp/env/jdbc/ge_regent. **/
    String JDBC_REGENT_JNDI_NAME = "java:comp/env/jdbc/ge_regent";

    /** balise.c05.correspondance.liasse=Z:M;Y:C. **/
    String BALISE_C05_CORRESPONDANCE_LIASSE = "Z:M;Y:C";

    /** balise.c05.correspondance.liasse=Z:M;Y:C. **/
    String BALISE_C05_CORRESPONDANCE_LIASSE_RESEAU = "M;C";

    /** balise.c43.correpondance.liasse. **/
    String BALISE_C43_CORRESPONDANCE_LIASSE = "Z";

    /** xml.regent.separateur.evenement=,. **/
    String XML_REGENT_SEPERATEUR_EVENEMENT = ",";

}
