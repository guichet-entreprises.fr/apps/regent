/**
 * 
 */
package fr.ge.common.regent.constante;

/**
 * Enumération des toutes les règles Regent et leur XSD correspondantes en
 * fonction de leur version.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
public enum VersionXmlRegentEnum {

    /** v2008 11. */
    V2008_11("V2008.11", "Type_REGENT_V2008-11.xsd", "Message_Regent_V2008-11.xsd"),

    /** v2016 01. */
    V2016_02("V2016.02", "Type_REGENT_V2016-02.xsd", "Message_Regent_V2016-02.xsd");

    /** version. */
    private String version;

    /** xsd type regent. */
    private String xsdTypeRegent;

    /** xsd message regent. */
    private String xsdMessageRegent;

    /**
     * Constructeur de la classe.
     *
     * @param version
     *            version
     * @param xsdTypeRegent
     *            xsd type regent
     * @param xsdMessageRegent
     *            xsd message regent
     */
    VersionXmlRegentEnum(final String version, final String xsdTypeRegent, final String xsdMessageRegent) {
        this.version = version;
        this.xsdTypeRegent = xsdTypeRegent;
        this.xsdMessageRegent = xsdMessageRegent;
    }

    /**
     * Accesseur sur l'attribut {@link #version}.
     *
     * @return String version
     */
    public String getVersion() {
        return this.version;
    }

    /**
     * Accesseur sur l'attribut {@link #xsdTypeRegent}.
     *
     * @return String xsdTypeRegent
     */
    public String getXsdTypeRegent() {
        return this.xsdTypeRegent;
    }

    /**
     * Accesseur sur l'attribut {@link #xsdMessageRegent}.
     *
     * @return String xsdMessageRegent
     */
    public String getXsdMessageRegent() {
        return this.xsdMessageRegent;
    }

    /**
     * Récupère l'enum en fonction de sa version.
     *
     * @param version
     *            version
     * @return version xml regent enum
     */
    public static VersionXmlRegentEnum fromVersion(final String version) {
        for (VersionXmlRegentEnum versionXmlRegent : VersionXmlRegentEnum.values()) {
            if (versionXmlRegent.getVersion().equals(version)) {
                return versionXmlRegent;
            }
        }
        return null;
    }
}
