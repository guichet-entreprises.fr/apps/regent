package fr.ge.common.regent.constante;

import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

/**
 * Enumeration définissant les types de dossiers pris encharge par le guichet
 * entreprise.
 * 
 */
public enum TypeDossierEnum {

    /** Le creation. */
    CREATION("C", "Dossier de création", "creation"),
    /** Le regularisation. */

    REGULARISATION("R", "Dossier de régularisation", "regularisation"),
    /** Le modification. */

    MODIFICATION("M", "Dossier de modification", "modification"),

    /** Le cessation. */
    CESSATION("F", "Dossier de cessation", "cessation");

    /** Le type technique. */
    private String typeTechnique;

    /** Le type fonctionnel. */
    private String typeFonctionnel;

    /** Le id flow. */
    private String idFlow;

    /** The Constant PATTERN_CREATION. */
    private static final Pattern PATTERN_CREATION = Pattern.compile("^(0[0-9][MP])");

    /** The Constant PATTERN_MODIFICATION. */
    private static final Pattern PATTERN_MODIFICATION = Pattern.compile("^([1-9][0-9][MP])");

    /**
     * Constructeur.
     *
     * @param typeTechnique
     *            le type technique
     * @param typeFonctionnel
     *            le type fonctionnel
     * @param idFlow
     *            le id flow
     */
    TypeDossierEnum(final String typeTechnique, final String typeFonctionnel, final String idFlow) {
        this.typeTechnique = typeTechnique;
        this.typeFonctionnel = typeFonctionnel;
        this.idFlow = idFlow;
    }

    /**
     * Récupère le type technique.
     *
     * @return le type technique
     */
    public String getTypeTechnique() {
        return this.typeTechnique;
    }

    /**
     * Récupère le type fonctionnel.
     *
     * @return le type fonctionnel
     */
    public String getTypeFonctionnel() {
        return this.typeFonctionnel;
    }

    /**
     * Récupère le flow.
     *
     * @return le flow id
     */
    public String getFlowId() {
        return this.idFlow;
    }

    public static TypeDossierEnum getTypeDossierByFlowId(final String typeFormalite) {
        for (TypeDossierEnum item : TypeDossierEnum.values()) {
            if (typeFormalite.equals(item.getFlowId())) {
                return item;
            }
        }
        return null;
    }

    /**
     * Determine type formality according to event identifier
     * 
     * @param evenement
     *            the event identifier
     * @return the formality type
     */
    public static TypeDossierEnum getTypeDossierByEvenement(final List<String> events) {
        if (null == events || events.isEmpty()) {
            return null;
        }

        if (events.contains("24P")) {
            return TypeDossierEnum.REGULARISATION;
        }

        for (String event : events) {
            if (StringUtils.isEmpty(event) || event.length() != 3) {
                return null;
            }
            if ("41P".equals(event)) {
                return TypeDossierEnum.CESSATION;
            } else if (PATTERN_CREATION.matcher(event).matches()) {
                return TypeDossierEnum.CREATION;
            } else if (PATTERN_MODIFICATION.matcher(event).matches()) {
                return TypeDossierEnum.MODIFICATION;
            }
        }
        return null;
    }
}
