package fr.ge.common.regent.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Le Class DonneeProfilBean.
 */
public class DonneeProfilBean {

    /** Le evenements. */
    List<String> evenements;

    /** Le Type personne. */
    private final String typePersonne;

    /** Le secteur cfe. */
    private final String secteurCfe;

    /**
     * Instancie un nouveau donnee profil bean.
     *
     * @param evenements
     *            le evenements
     * @param nomDossier
     *            le nom dossier
     * @param typePersonne
     *            le type personne
     * @param typeDossier
     *            le type dossier
     * @param secteurCfe
     *            le secteur cfe
     */
    public DonneeProfilBean(List<String> evenements, String typePersonne, String secteurCfe) {
        super();
        this.evenements = evenements;
        this.secteurCfe = secteurCfe;
        this.typePersonne = typePersonne;
    }

    /**
     * Get le evenements.
     *
     * @return le evenements
     */
    public List<String> getEvenements() {
        if (evenements == null) {
            evenements = new ArrayList<String>();
        }
        return evenements;
    }

    /**
     * Set le evenements.
     *
     * @param evenements
     *            le nouveau evenements
     */
    public void setEvenements(List<String> evenements) {
        this.evenements = evenements;
    }

    /**
     * Get le type personne.
     *
     * @return le type personne
     */
    public String getTypePersonne() {
        return typePersonne;
    }

    /**
     * Get le secteur cfe.
     *
     * @return le secteur cfe
     */
    public String getSecteurCfe() {
        return secteurCfe;
    }

}
