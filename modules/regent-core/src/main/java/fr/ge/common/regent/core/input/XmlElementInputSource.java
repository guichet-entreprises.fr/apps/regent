package fr.ge.common.regent.core.input;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.regent.bean.IXmlRegent;
import fr.ge.common.regent.xml.FormMetadata;
import fr.ge.common.regent.xml.SimpleElement;

/**
 * Le Class XmlElementInputSource.
 *
 * @author bsadil
 */
public class XmlElementInputSource implements InputSource {

    /** Id du champ à laquelle la liste appartient. */
    private final String group;

    /**
     * Liste des elements à charger.
     */
    private final List<IXmlRegent> elements;

    /** Le fm. */
    @Autowired
    protected FormMetadata fm;

    /**
     * Charge les definitions d'une inputsource depuis une liste Java d'
     * <code>IXmlRegent</code>.
     *
     * @param group
     *            l'identifiant du groupe des elemont chargé
     * @param elements
     *            le elements
     * @See {@link IXmlRegent}
     */
    public XmlElementInputSource(String group, List<IXmlRegent> elements) {
        this.elements = elements;
        this.group = group;
    }

    /**
     * Charge depuis un accès en base une liste de valeur possible pour le champ
     * correspondant au group sous forme de XmlElementInputSource.
     *
     * @throws IOException
     *             Signale l'apparition d'une I/O exception.
     */
    @Override
    public void load() throws IOException {
        // Une entré vide
        if (this.elements != null) {
            for (IXmlRegent bean : this.elements) {
                this.fm.addXmlFieldElement(bean.getId(), this.group, bean.getEvaluation(), bean.getPath(), bean.getOccurences(), bean.getRubrique(), bean.getObligatoire(), bean.getBourrage(),
                        bean.getEndPath(), bean.getVersion());
            }
        }
        List<SimpleElement> elementsByGroup = this.fm.getElementsByGroup(this.group);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setFormMetadata(FormMetadata fm) {
        this.fm = fm;
    }

}
