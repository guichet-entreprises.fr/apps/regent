package fr.ge.common.regent.xml;

import java.util.regex.Pattern;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.EvaluationException;

import fr.ge.common.regent.xml.expression.AutorisationsStaticReplace;
import fr.ge.common.regent.xml.expression.CourantStaticReplace;
import fr.ge.common.regent.xml.expression.FormaliteStaticReplace;
import fr.ge.common.regent.xml.expression.NullSafeStaticReplace;
import fr.ge.common.regent.xml.expression.VariablesContextStaticReplace;
import fr.ge.common.regent.xml.util.ExpressionParserUtil;

/**
 * Le Class XmlFieldElement.
 */
public class XmlFieldElement extends SimpleElement {

    /** La constante A_STRING_BEAN_REF_STATIC. */
    private static final String A_STRING_BEAN_REF_STATIC = "formalite.$1";

    /** La constante ASTRING_BEAN_REF. */
    private static final String ASTRING_BEAN_REF = "formalite\\[([A-Za-z0-9_\\.]+)\\]";

    /**
     * formalite\\[([A-Za-z0-9_\\.]+)\\].
     */
    protected static final Pattern BEAN_REF = Pattern.compile(ASTRING_BEAN_REF);

    /**
     * formalite.$1.
     */
    protected static final String BEAN_REF_STATIC = A_STRING_BEAN_REF_STATIC;

    /** Le logger technique. */
    private static final Logger LOGGER_TECH = LoggerFactory.getLogger(XmlFieldElement.class);

    /** Le value. */
    protected org.springframework.expression.Expression value;

    /** Le occurences. */
    private Integer occurences;

    /** Le path. */
    private String path;

    /** Le rubrique. */
    private String rubrique;

    /** Le obligatoire. */
    private boolean obligatoire;

    /** Le bourrage. */
    private String bourrage;

    /** Le end path. */
    private boolean endPath;

    /**
     * La version du XML field (dans l'exemple du regent la version de la norme
     * regent).
     */
    private String version;

    /**
     * Instancie un nouveau xml field element.
     *
     * @param parent
     *            le parent
     * @param id
     *            le id
     * @param group
     *            le group
     * @param value
     *            le value
     * @param path
     *            le path
     * @param occurences
     *            le occurences
     * @param rubrique
     *            le rubrique
     * @param obligatoire
     *            le obligatoire
     * @param bourrage
     *            le bourrage
     * @param endPath
     *            le end path
     * @param version
     *            La version du XML field (dans l'exemple du regent la version
     *            de la norme regent).
     */
    public XmlFieldElement(final FormMetadata parent, final String id, final String group, final String value, final String path, final Integer occurences, final String rubrique,
            final boolean obligatoire, final String bourrage, final boolean endPath, final String version) {
        super(parent, id, group);
        this.occurences = occurences;
        this.path = path;
        this.rubrique = rubrique;
        this.obligatoire = obligatoire;
        this.bourrage = bourrage;
        this.endPath = endPath;
        this.version = version;
        // Preparer la valeur
        if (value != null) {
            this.value = ExpressionParserUtil.parser(id, group, parent.getParser(), value, performStaticBeansReplacements(value));
        }

    }

    /**
     * Remplace formalite[].
     * 
     * @param expression
     *            expression a parser
     * @return l'expression traité
     */
    protected static String performStaticBeansReplacements(final String expression) {
        String result = expression;
        result = new FormaliteStaticReplace().perform(result); // formalite[]
        result = new VariablesContextStaticReplace().perform(result); // cfe[]
        result = new CourantStaticReplace().perform(result); // //result[]
        // ailleurs
        // que dans
        // le dialgue
        // cfe
        result = new AutorisationsStaticReplace().perform(result);
        result = new NullSafeStaticReplace().perform(result);
        return result;
    }

    /**
     * Getter.
     * 
     * @return l'expression de value
     */
    public org.springframework.expression.Expression getValue() {
        return this.value;
    }

    /**
     * Get le occurences.
     *
     * @return le nombre d'occurences de la balise
     */
    public Integer getOccurences() {
        return this.occurences;
    }

    /**
     * Get le path.
     *
     * @return le path de la balise xml
     */
    public String getPath() {
        return this.path;
    }

    /**
     * Get le obligatoire.
     *
     * @return si la balise obligatoire
     */
    public boolean getObligatoire() {
        return this.obligatoire;
    }

    /**
     * Get le rubrique.
     *
     * @return le rubrique
     */
    public String getRubrique() {
        return this.rubrique;
    }

    /**
     * Get le bourrage.
     *
     * @return bourrage
     */
    public String getBourrage() {
        return this.bourrage;
    }

    /**
     * Get le end path.
     *
     * @return le end path
     */
    public boolean getEndPath() {
        return this.endPath;
    }

    /**
     * Accesseur sur l'attribut {@link #version}.
     *
     * @return String version
     */
    public String getVersion() {
        return this.version;
    }

    /**
     * Getter de l'attribut value dans le context avec.
     *
     * @param c
     *            le c
     * @return la valeur de value
     * @throws EvaluationException
     *             le evaluation exception
     */
    public Object getValue(final EvaluationContext c) {
        LOGGER_TECH.debug("Eval: {}  |  ID:{} PATH:{} RUBRIQUE:{} getValue()", this.value.getExpressionString(), this.getId(), this.path, this.rubrique);
        try {
            return this.value.getValue(c);
        } catch (EvaluationException e) {
            LOGGER_TECH.debug("Eval: {}  |  ID:{} PATH:{} RUBRIQUE:{} Erreur:{}", this.value.getExpressionString(), this.getId(), this.path, this.rubrique, e.getMessage());
            LOGGER_TECH.info("Impossible de récupérer la valeur pour la balise {} ", this.path, e.getLocalizedMessage());
            return false;
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

}
