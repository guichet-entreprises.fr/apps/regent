package fr.ge.common.regent.core.form;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;

/**
 * Type de format disponible pour les champs à utiliser avec <code>FromInputElement</code>.
 * <p>
 * 
 * Le type configuré sert à la génération des balise html des champs. Certain formats peuvent
 * prendre en plus un paramètre.
 * 
 * <ul>
 * <li>Bloc - Un encadré avec un titre</li>
 * <li>Checkbox - Un libellé avec des checkbox</li>
 * <li>Checkboxouinon - un libellé avec une seule checkbox qui renvoie oui si elle a été cochée</li>
 * <li>Date - Un libellé qui attend une réponse au format date.
 * <p>
 * La date est au format JJ/MM/AAAA par défaut mais peut être configuré au format JJ/MM avec le
 * paramètre dd/MM</li>
 * <li>Liste - Un libellé avec une liste déroulante</li>
 * <li>Radio - Un libellé avec des radio boutons</li>
 * <li>Texte - libellé avec un champ input. Il peut prendre un paramètre pour la taille du champ
 * affiché</li>
 * <li>Textearea - libellé avec un textarea</li>
 * <li>Label - Un libellé et la valeur d'un champ</li>
 * <li>MessageInformatif - Un libellé</li>
 * </ul>
 * 
 */
public class InputType {

  /** La constante TYPE_BLOC. */
  public static final String TYPE_BLOC = "bloc"; 

  /** La constante TYPE_CHECKBOX. */
  public static final String TYPE_CHECKBOX = "checkbox"; 

  /** La constante TYPE_CHECKBOX_OUINON. */
  public static final String TYPE_CHECKBOX_OUINON = "checkboxouinon"; 

  /** La constante TYPE_DATE. */
  public static final String TYPE_DATE = "date"; 
  
  /** La constante TYPE_DATE_DDMM. */
  public static final String TYPE_DATE_DDMM = "date_ddMM"; 

  /** La constante TYPE_HIDDEN. */
  public static final String TYPE_HIDDEN = "hidden"; 

  /** La constante TYPE_LIST. */
  public static final String TYPE_LIST = "liste"; 

  /** La constante TYPE_RADIO. */
  public static final String TYPE_RADIO = "radio"; 

  /** La constante TYPE_TEXT. */
  public static final String TYPE_TEXT = "texte"; 

  /** La constante TYPE_TEXTAREA. */
  public static final String TYPE_TEXTAREA = "textarea"; 

  /** La constante TYPE_LABEL. */
  public static final String TYPE_LABEL = "label"; 

  /** La constante TYPE_MSG_INFORMATIF. */
  public static final String TYPE_MSG_INFORMATIF = "message"; 

  /** La constante TYPE_LABEL_LIEN. */
  public static final String TYPE_LABEL_LIEN = "labelLien"; 

  /** La constante TYPE_TEXT_AUTOCOMPLETE. */
  public static final String TYPE_TEXT_AUTOCOMPLETE = "texteAutocomplete"; 

  /** La constante TYPES. */
  private static final String[] TYPES = new String[] {TYPE_LIST, TYPE_RADIO, TYPE_TEXT, TYPE_TEXTAREA, TYPE_CHECKBOX, TYPE_DATE,
    TYPE_BLOC, TYPE_CHECKBOX_OUINON, TYPE_LABEL, TYPE_MSG_INFORMATIF, TYPE_LABEL_LIEN, TYPE_TEXT_AUTOCOMPLETE, TYPE_HIDDEN };

  /**
   * Nom du type.
   */
  private final String id;

  /**
   * Paramètre du type.
   */
  private final List < String > params;

  /**
   * Constructeur de classe. L'identifiant du type doit être définit dans <code>TYPES</code>
   *
   * @param id
   *          identifiant du type
   * @param params
   *          les paramètre du type
   * @param groupe
   *          le groupe
   */
  public InputType(String id, List < String > params, String groupe) {
    this.id = id;
    if (params == null) {
      this.params = new ArrayList < String >();
    } else {
      this.params = params;
    }

    if (!ArrayUtils.contains(TYPES, id)) {
      throw new IllegalArgumentException("Groupe : " + groupe + ", Type " + id + " n'est pas une valeur acceptée");  
    }
  }

  /**
   * Getter de l'attribut id.
   * 
   * @return la valeur de id
   */
  public String getId() {
    return this.id;
  }

  /**
   * Getter de l'attribut param.
   * 
   * @return la valeur de param
   */
  public List < String > getParams() {
    return this.params;
  }

  /**
   * Get le types.
   *
   * @return le types
   */
  public static String[] getTypes() {
    return TYPES.clone();
  }

}
