package fr.ge.common.regent.xml.expression;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Remplace les patterns *autorisations* par *#autorisations(#numeroDossier)*;===>
 * "#autorisations(#numeroDossier).?[Nationality == 'Serbian']"
 * 
 * @author Nicolas Richeton
 * 
 */
public class AutorisationsStaticReplace {

  /** La constante AUTORISATIONS_PERFORM_REF. */
  private static final String AUTORISATIONS_PERFORM_REF = "#recupererAutorisations(#numeroDossier)";

  /** autorisations. */
  private static final Pattern AUTORISATIONS_REF = Pattern.compile("autorisations");

  /**
   * Perform.
   *
   * @param expr
   *          le expr
   * @return le string
   */
  public String perform(String expr) {
    String result;
    // gestion des parents
    Matcher m = AUTORISATIONS_REF.matcher(expr);
    result = m.replaceAll(AUTORISATIONS_PERFORM_REF);
    return result;
  }

}
