/**
 * 
 */
package fr.ge.common.regent.constante;

/**
 * Enumération des XSD correspondantes au XML-TC en fonction de leur version.
 * 
 * @author mtakerra
 * @version $Revision: 0
 *
 */
public enum VersionXmlTcEnum {

	/** V2012.02. */
	V2012_02("V2012.02", "guen_types_de_base.xsd", "guen_dmtdu.xsd"),
	/** V2009.01 **/
	V2009_01("V2009.01", "fw_types_de_base.xsd", "mgun_demat.xsd");

	/** version. */
	private String version;

	/** xsd type xml-tc. */
	private String xsdTypeTc;

	/** xsd message xml-tc. */
	private String xsdMessageTc;

	/**
	 * Constructeur de la classe.
	 *
	 * @param version      version
	 * @param xsdTypeTc    xsd type regent
	 * @param xsdMessageTc xsd message regent
	 */
	VersionXmlTcEnum(final String version, final String xsdTypeTc, final String xsdMessageTc) {
		this.version = version;
		this.xsdTypeTc = xsdTypeTc;
		this.xsdMessageTc = xsdMessageTc;
	}

	/**
	 * Accesseur sur l'attribut {@link #version}.
	 *
	 * @return String version
	 */
	public String getVersion() {
		return this.version;
	}

	/**
	 * Accesseur sur l'attribut {@link #xsdTypeTc}.
	 *
	 * @return String xsdTypeTc
	 */
	public String getXsdTypeXmlTc() {
		return this.xsdTypeTc;
	}

	/**
	 * Accesseur sur l'attribut {@link #xsdMessageTc}.
	 *
	 * @return String xsdMessageTc
	 */
	public String getXsdMessageXmlTc() {
		return this.xsdMessageTc;
	}

	/**
	 * Récupère l'enum en fonction de sa version.
	 *
	 * @param version version
	 * @return version xml tc enum
	 */
	public static VersionXmlTcEnum fromVersion(final String version) {
		for (VersionXmlTcEnum versionXmlTc : VersionXmlTcEnum.values()) {
			if (versionXmlTc.getVersion().equals(version)) {
				return versionXmlTc;
			}
		}
		return null;
	}
}
