package fr.ge.common.regent.xml.expression;

/**
 * Ajoute un prefixe au contenu des patterns champ[]:
 * <p>
 * 
 * champ[X] devient champ[prefix.X]
 * 
 * @author Nicolas Richeton
 * 
 */
public class ChampAddPrefix extends AbstractChampReplace {

  /**
   * Constructeur de classe.
   * 
   * @param prefix
   *          préfix à ajouter
   */
  public ChampAddPrefix(String prefix) {
    super(FORM_REF, "champ[" + prefix + ".$1]");
  }
}
