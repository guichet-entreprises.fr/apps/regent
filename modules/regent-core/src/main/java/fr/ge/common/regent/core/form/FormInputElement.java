package fr.ge.common.regent.core.form;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.spel.SpelEvaluationException;

import fr.ge.common.regent.xml.FormMetadata;
import fr.ge.common.regent.xml.util.ExpressionParserUtil;

/**
 * Représente un champ éditable d'un formulaire.
 * <p>
 * Ajoute les attributs nécessaire à l'édition et à la validation :
 * <ul>
 * <li>Type</li>
 * <li>Valeur par défaut à l'affichage</li>
 * <li>Valeur par défaut à l'enregistrement en base</li>
 * <li>Obligatoire</li>
 * <li>Règles de validation</li>
 * <li>Listes de valeurs, dans le cas de champs select, checkbox ou radio</li>
 * </ul>
 * 
 * @author Nicolas Richeton
 * 
 * 
 */
public class FormInputElement extends DynamicVisibleElement {

    /** La constante MSG_EXCEPTION_DEFAULT_VALUE. */
    private static final String MSG_EXCEPTION_DEFAULT_VALUE = "getDefaultValue: exception group:{} id: {} ";

    /** La constante MSG_EXCEPTION_REF_JS_1. */
    private static final String MSG_EXCEPTION_REF_JS_1 = "Impossible d'utiliser une référence javascript ";

    /** La constante MSG_EXCEPTION_REF_JS_2. */
    private static final String MSG_EXCEPTION_REF_JS_2 = "dans une valeur par défaut group:";

    /** La constante MSG_EXCEPTION_REF_JS_3. */
    private static final String MSG_EXCEPTION_REF_JS_3 = " id:";

    /** La constante MSG_EXCEPTION_REF_JS_4. */
    private static final String MSG_EXCEPTION_REF_JS_4 = " valeurDefaut:";

    /** La constante ESPACE. */
    private static final String ESPACE = " ";

    /** La constante NBSP. */
    private static final String NBSP = "&nbsp;";

    /** La constante INLINE. */
    private static final String INLINE = "inline";

    /** La constante CHAMP_OBLIGATOIRE. */
    private static final String CHAMP_OBLIGATOIRE = "X";

    /**
     * Valeur initialisé à l'affichage.
     */
    private Expression defaultValueDisplay;

    /** Expression du champ obligatoire. */
    private Expression expObligatoire;

    /** Le str obligatoire. */
    private String strObligatoire;

    /**
     * Valeur par défaut à la sauvegarde (si l'écran n'est pas affiché ou qu'il
     * n'a pas été rempli).
     */
    private Expression onSaveRules;

    /**
     * Format du champ.
     */
    private final InputType type;

    /**
     * Règles de validation à réaliser sur le champ.
     */
    private final List<ValidationElement> validations;

    /** Listes de valeurs, dans le cas de champs select, checkbox ou radio. */
    private final List<DynamicVisibleElement> values;

    /** Valeurs listes fonction. */
    private Expression expressionListeFonction;

    /**
     * Constructeur. Créer la représentation d'un champ éditable d'un
     * formulaire.
     * 
     * @param parent
     *            l'élément parent du champ
     * @param id
     *            l'id du champ
     * @param group
     *            le groupe parent du champ
     * @param label
     *            le libellé d'un champ
     * @param aide
     *            texte d'aide relative au champ
     * @param visiilbity
     *            visibilité static d'un champ
     * @param dynamicVisibility
     *            visibilité dynamique d'un champ
     * @param strType
     *            le type du champ
     * @param typeParam
     *            paramètre du ty du champ
     * @param mandatory
     *            indique si le champ est obligatoire ou non
     * @param strDefaultValueDisplay
     *            le str default value display
     * @param defaultValueDB
     *            le default value db
     * @param initValues
     *            liste, s'il y en a, les valeurs possible à la selection d'un
     *            champ pour les radio bouton, checkbox, liste déroulante...
     * @param strExpressionListeFonction
     *            expression de fonction de recherche de la liste
     * @param initValidations
     *            liste, s'il y en a, les éléments modèlisant les conditions de
     *            validation de l'entrée d'un champ
     */
    public FormInputElement(FormMetadata parent, String id, String group, String label, String aide, String visiilbity, String dynamicVisibility, String strType, List<String> typeParam,
            String mandatory, String strDefaultValueDisplay, String defaultValueDB, List<DynamicVisibleElement> initValues, String strExpressionListeFonction,
            List<ValidationElement> initValidations) {
        super(parent, id, group, label, aide, visiilbity, dynamicVisibility);
        // gestion d'un mode inline pour les champs qui sont côte à côte :
        // le
        // libellé du premier champ peut dépasser au-dessus du deuxième
        // champ
        if (typeParam != null && typeParam.contains(INLINE)) {
            this.setLabel(this.getLabel().replaceAll(ESPACE, NBSP));
        }
        // Default value
        if (strDefaultValueDisplay != null) {

            if (FORM_REF.matcher(strDefaultValueDisplay).find()) {
                throw new IllegalArgumentException(MSG_EXCEPTION_REF_JS_1 + MSG_EXCEPTION_REF_JS_2 + group + MSG_EXCEPTION_REF_JS_3 + id + MSG_EXCEPTION_REF_JS_4 + strDefaultValueDisplay);
            }

            this.defaultValueDisplay = ExpressionParserUtil.parser(id, group, parent.getParser(), strDefaultValueDisplay, performStaticBeansReplacements(strDefaultValueDisplay));
        }

        if (defaultValueDB != null) {

            if (FORM_REF.matcher(defaultValueDB).find()) {
                throw new IllegalArgumentException(MSG_EXCEPTION_REF_JS_1 + MSG_EXCEPTION_REF_JS_2 + group + MSG_EXCEPTION_REF_JS_3 + id + MSG_EXCEPTION_REF_JS_4 + defaultValueDB);
            }

            this.onSaveRules = ExpressionParserUtil.parser(id, group, parent.getParser(), defaultValueDB, performStaticBeansReplacements(defaultValueDB));
        }

        this.values = initValues;
        this.validations = initValidations;
        this.type = new InputType(strType, typeParam, group);

        if (StringUtils.isBlank(mandatory) || StringUtils.equalsIgnoreCase(mandatory, CHAMP_OBLIGATOIRE)) {
            this.strObligatoire = mandatory;
        } else {
            this.expObligatoire = ExpressionParserUtil.parser(id, group, parent.getParser(), mandatory, performStaticBeansReplacements(mandatory));
        }

        if (StringUtils.isNotBlank(strExpressionListeFonction)) {
            this.expressionListeFonction = ExpressionParserUtil.parser(id, group, parent.getParser(), strExpressionListeFonction, performStaticBeansReplacements(strExpressionListeFonction));
        }
    }

    /**
     * <b>/!\ Ne doit être utilisé que dans CSVInputElementInputSource.load.
     * /!\</b>
     * <p>
     * Getter pour defaultValueDisplay.
     * </p>
     * 
     * @return le default value display
     */
    public String getDefaultValueDisplay() {
        if (this.defaultValueDisplay == null) {
            return null;
        }
        return this.defaultValueDisplay.getExpressionString();
    }

    /**
     * Retourne la valeur à afficher dans le context fourni.
     * 
     * @param c
     *            le c
     * @return la valeur par defaut s'il y en a une, null sinon
     */
    public String getDefaultValueDisplay(EvaluationContext c) {
        if (this.defaultValueDisplay == null) {
            return null;
        }
        try {
            return this.defaultValueDisplay.getValue(c, String.class);
        } catch (SpelEvaluationException e) {
            throw e;
        }
    }

    /**
     * Retourne la liste des champs succeptibles de modifier l'affichage de ce
     * champ.
     * <p>
     * En complément d'implémentation de
     * {@link DynamicVisibleElement#getDynReferences(EvaluationContext)}, cette
     * implémentation complète la liste avec les références dynamiques des
     * valeurs de ce champ. Exemple: dans le cas d'un champ select, les valeurs
     * proposées dans la liste peuvent dépendre de la valeur d'autres champs de
     * la page.
     * 
     * @param c
     *            le c
     * @return le dyn references
     * @see fr.gipge.core.form.DynamicVisibleElement#getDynReferences(org.springframework.expression.EvaluationContext)
     */
    @Override
    public List<String> getDynReferences(EvaluationContext c) {
        List<String> allReferences = new ArrayList<String>();
        if (CollectionUtils.isNotEmpty(super.getDynReferences())) {
            allReferences.addAll(super.getDynReferences());
        }

        List<DynamicVisibleElement> aValues = this.getValues(c);
        if (CollectionUtils.isNotEmpty(aValues)) {
            for (DynamicVisibleElement e : aValues) {
                List<String> dynReferences = e.getDynReferences();
                if (dynReferences != null) {
                    allReferences.addAll(dynReferences);
                }
            }
        }

        return allReferences;
    }

    /**
     * <b>/!\ Ne doit être utilisé que dans CSVInputElementInputSource.load.
     * /!\</b>
     * <p>
     * Getter pour onSaveRules.
     * </p>
     * 
     * @return le on save rules
     */
    public String getOnSaveRules() {
        if (this.onSaveRules == null) {
            return null;
        }
        return this.onSaveRules.getExpressionString();
    }

    /**
     * Retourne la valeur du champ après application des règles métier.
     * 
     * @param c
     *            le c
     * @return La nouvelle valeur calculée, ou null si la valeur ne doit pas
     *         être modifiée.
     */
    public String getOnSaveRules(EvaluationContext c) {
        if (this.onSaveRules == null) {
            return null;
        }
        try {
            return this.onSaveRules.getValue(c, String.class);
        } catch (SpelEvaluationException e) {
            throw e;
        }
    }

    /**
     * Retourne le type du champ représenté.
     * 
     * @return type du champ
     */
    public InputType getType() {
        return this.type;
    }

    /**
     * <b>/!\ Ne doit être utilisé que dans CSVInputElementInputSource.load.
     * /!\</b>
     * <p>
     * Getter pour validations.
     * </p>
     * 
     * @return le validations
     */
    public List<ValidationElement> getValidations() {
        return this.validations;
    }

    /**
     * Retourne les validations à effectuer dans le contexte fourni.
     * 
     * @param context
     *            le context
     * @return liste des validations à effectuer, liste vide si aucune
     *         validation.
     */
    public List<ValidationElement> getValidations(EvaluationContext context) {
        List<ValidationElement> result = new ArrayList<ValidationElement>();

        if (this.validations == null) {
            return result;
        }
        for (ValidationElement val : this.validations) {
            if (val.isVisible(context)) {
                result.add(val);
            }
        }

        return result;

    }

    /**
     * <b>/!\ Ne doit être utilisé que dans CSVInputElementInputSource.load.
     * /!\</b>
     * <p>
     * Getter pour values.
     * </p>
     * 
     * @return le values
     */
    public List<DynamicVisibleElement> getValues() {
        return this.values;
    }

    /**
     * <b>/!\ Ne doit être utilisé que dans CSVInputElementInputSource.load.
     * /!\</b>
     * <p>
     * Getter pour expressionListeFonction.
     * </p>
     * 
     * @return le expression liste fonction
     */
    public String getExpressionListeFonction() {

        if (this.expressionListeFonction == null) {
            return null;
        }
        return this.expressionListeFonction.getExpressionString();
    }

    /**
     * Retourne les valeurs possible dans le contexte fourni.
     * 
     * @param c
     *            le c
     * @return liste des valeurs possible, éventuellement vide mais non null.
     */
    public List<DynamicVisibleElement> getValues(final EvaluationContext c) {
        List<DynamicVisibleElement> result = new ArrayList<DynamicVisibleElement>();

        if (this.values != null) {
            for (DynamicVisibleElement val : this.values) {
                if (val.isVisible(c)) {
                    result.add(val);
                }
            }
        }
        if (this.expressionListeFonction != null) {
            result = this.expressionListeFonction.getValue(c, List.class);
        }

        return result;
    }

    /**
     * Retourne le caractère obligatoire d'un champ.
     * 
     * @param context
     *            le context
     * @return booleen.
     */
    @SuppressWarnings("boxing")
    public boolean isObligatoire(final EvaluationContext context) {
        // strObligatoire et expObligatoire ne sont jamais remplis à la fois.
        if (this.expObligatoire != null) {
            return this.expObligatoire.getValue(context, Boolean.class);
        }
        return this.strObligatoire.equalsIgnoreCase(CHAMP_OBLIGATOIRE);

    }

    /**
     * Get le str obligatoire.
     * 
     * @return le str obligatoire
     */
    public String getStrObligatoire() {
        return this.strObligatoire;
    }
}
