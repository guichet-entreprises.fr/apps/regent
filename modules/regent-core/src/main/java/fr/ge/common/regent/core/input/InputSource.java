package fr.ge.common.regent.core.input;

import java.io.IOException;

import fr.ge.common.regent.xml.FormMetadata;

/**
 * Interface décrivant une inputSource pouvant être utilisée par FormMetadata
 * pour charger des information relatives à un formulaire.
 * 
 */
public interface InputSource {

    /**
     * Charge les informations depuis une InputSource.
     *
     * @throws IOException
     *             Signale l'apparition d'une I/O exception.
     */
    void load() throws IOException;

    /**
     * Setter de l'attribut formMetadata.
     * 
     * @param fm
     *            la nouvelle valeur de formMetadata
     */
    void setFormMetadata(FormMetadata fm);

}
