package fr.ge.common.regent.core.form;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import fr.ge.common.regent.xml.FormMetadata;

/**
 * Classe modèlisant un élément visible.
 * <p>
 * Elle contient les attribut nécessaire à son affichage:
 * <p>
 * <ul>
 * <li>Label</li>
 * <li>Une aide à affiché si nécessaire</li>
 * </ul>
 * 
 */
public class VisibleElement extends SimpleFormElement {

    /** Le label. */
    private String label;

    /** Le aide. */
    private final String aide;

    /**
     * Constructeur de classe.
     * 
     * @param parent
     *            les méta données du formulaire contenant l'élément
     * @param id
     *            identifiant de l'élément
     * @param group
     *            groupe d'appartenance de l'éléments
     * @param visibility
     *            expression d'affichage statique de l'élément
     * @param label
     *            libéllé à afficher de l'élément
     * @param aide
     *            aide relative à l'élément
     */
    public VisibleElement(FormMetadata parent, String id, String group, String visibility, String label, String aide) {
        super(parent, id, group, visibility);
        // il faut échapper les caractères spéciaux contenus dans les libellés
        // et les textes d'aide, seulement si on n'est pas en train de charger
        // un champ "expand", auquel cas on échapperait 2 fois les caractères et
        // ferait donc apparaître les entités HTML à l'écran
        if (StringUtils.isNotBlank(id) && !id.contains(".")) {
            this.setLabel(StringEscapeUtils.escapeHtml4(label));
            this.aide = StringEscapeUtils.escapeHtml4(aide);
        } else {
            this.setLabel(label);
            this.aide = aide;
        }
    }

    /**
     * Getter de l'attribut label.
     * 
     * @return la valeur de label
     */
    public String getLabel() {
        return this.label;
    }

    /**
     * Getter de l'attribut aide.
     * 
     * @return la valeur de aide
     */
    public String getAide() {
        return this.aide;
    }

    /**
     * Set le label.
     *
     * @param label
     *            the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }

}
