package fr.ge.common.regent.xml.impl;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import fr.ge.common.regent.xml.FormMetadata;
import fr.ge.common.regent.xml.IXmlWriter;
import fr.ge.common.regent.xml.SimpleElement;
import fr.ge.common.regent.xml.XmlFieldElement;
import fr.ge.common.regent.xml.util.ExpressionParserUtil;

/**
 * 
 * @author mtakerra
 *
 */
public class XmlWriter implements IXmlWriter {

    /** La constante METHOD. */
    private static final String METHOD = "set";

    /** La constante ADDTO. */
    private static final String ADDTO = "addTo";

    /** La constante POINT. */
    private static final String POINT = ".";

    /** Le logger technique. */
    private static final Logger LOGGER_TECH = LoggerFactory.getLogger(ExpressionParserUtil.class);

    /** Le xml metadata. */
    @Autowired
    private FormMetadata xmlMetadata;

    /**
     * {@inheritDoc}
     */
    @Override
    public String constructMethodToInvok(final StandardEvaluationContext context, final XmlFieldElement xmlFielElement) {
        Object value = null;
        String prefixMethod = null;
        String baliseXmlRegent = this.constructBaliseFromPath(xmlFielElement.getPath());
        value = xmlFielElement.getValue(context);
        LOGGER_TECH.debug("value ={} baliseXmlRegent ={}", value, baliseXmlRegent);
        // on construit la method à invoquer en fonction du type de la value
        if (value != null && !(value instanceof Boolean)) {
            if (!(value instanceof List)) {
                prefixMethod = METHOD + baliseXmlRegent;
            } else {
                prefixMethod = ADDTO + baliseXmlRegent;
            }
        } else {
            if (xmlFielElement.getObligatoire() && xmlFielElement.getOccurences() != null) {
                prefixMethod = ADDTO + baliseXmlRegent;
            } else {
                prefixMethod = METHOD + baliseXmlRegent;
            }
        }
        LOGGER_TECH.debug("la méthode qui sera invoquer methode=:{} ", prefixMethod);

        return prefixMethod;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String constructBaliseFromPath(final String path) {
        // sppression des point dans le path de la balise xml
        String balisePath = StringUtils.remove(path, POINT);
        // récupération de la balise xml
        String baliseXmlRegent = StringUtils.substringAfterLast(balisePath, "/");
        LOGGER_TECH.debug("la balise construite à partir du path est={}: ", baliseXmlRegent);
        return baliseXmlRegent;
    }

    /**
     * Construct object to invoc.
     * 
     * @param PATH
     *            le path
     * @param xmlRegent
     *            le xml regent
     * @return le object
     * @throws NoSuchMethodException
     *             le no such method exception
     * @throws SecurityException
     *             le security exception
     * @throws IllegalAccessException
     *             le illegal access exception
     * @throws IllegalArgumentException
     *             le illegal argument exception
     * @throws InvocationTargetException
     *             le invocation target exception
     */
    public Object constructObjectToInvoc(final String PATH, Object xmlRegent)
            throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        String PathComplet = StringUtils.remove(PATH, '.');
        String[] tabObject = StringUtils.split(PathComplet, "/");
        if (tabObject.length <= 2 || StringUtils.contains(PATH, "NomService") || StringUtils.contains(PATH, "VersionService")) {
            return xmlRegent;
        } else {
            Object[] rubriques = null;
            for (int i = 1; i < tabObject.length - 1; i++) {
                Method m = xmlRegent.getClass().getMethod("get" + tabObject[i]);
                Method mAdd = xmlRegent.getClass().getMethod("addTo" + tabObject[i]);
                rubriques = (Object[]) m.invoke(xmlRegent, null);
                if (rubriques.length == 0) {
                    xmlRegent = mAdd.invoke(xmlRegent, null);
                } else {
                    xmlRegent = rubriques[0];
                }
            }
            LOGGER_TECH.debug("l'objet qui sera invoquer est ={}: ", xmlRegent.toString());
            return xmlRegent;

        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<XmlFieldElement> getXmlFieldElement(final String groupId, final String rubrique, final String version) {
        List<XmlFieldElement> fieldsMetadata = new ArrayList<XmlFieldElement>();
        List<SimpleElement> elements = this.xmlMetadata.getElementsByGroupAndStartWithRubrique(groupId, rubrique, version);
        if (elements == null) {
            return null;
        }
        for (SimpleElement element : elements) {
            if (element instanceof XmlFieldElement) {
                fieldsMetadata.add((XmlFieldElement) element);
            }
        }
        return fieldsMetadata;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Multimap<String, XmlFieldElement> constructMapRubriqueBalises(final List<XmlFieldElement> list) {
        Multimap<String, XmlFieldElement> multiMapRubriqueBalise = ArrayListMultimap.create();
        for (XmlFieldElement xmlFieldElement : list) {
            multiMapRubriqueBalise.put(xmlFieldElement.getRubrique(), xmlFieldElement);
        }
        return multiMapRubriqueBalise;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<XmlFieldElement> getXmlFieldElement(final String groupId, final String version) {
        List<SimpleElement> elements = this.xmlMetadata.getElementsByGroup(groupId);
        if (elements == null) {
            return null;
        }
        List<XmlFieldElement> fieldsMetadata = new ArrayList<XmlFieldElement>();
        for (SimpleElement element : elements) {
            if (element instanceof XmlFieldElement && version.equals(((XmlFieldElement) element).getVersion())) {
                fieldsMetadata.add((XmlFieldElement) element);
            }
        }
        return fieldsMetadata;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<XmlFieldElement> getListRubriqueOrdonne(final List<XmlFieldElement> listRubriqueOrdonneFromDB, final List<XmlFieldElement> listRubriqueByEvenementAndRole) {
        List<XmlFieldElement> listRubriqueGenererParLeMoteur = new ArrayList<XmlFieldElement>();

        for (XmlFieldElement xmlFieldElement : listRubriqueOrdonneFromDB) {
            if (listRubriqueByEvenementAndRole.contains(xmlFieldElement)) {
                listRubriqueGenererParLeMoteur.add(xmlFieldElement);
            }
        }

        return listRubriqueGenererParLeMoteur;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, Boolean> bourageSurBaliseObligatoireVide(final StandardEvaluationContext context, final Multimap<String, XmlFieldElement> multiMapRubriqueBalise) {
        Map<String, Boolean> mapRubriqueBourage = new HashMap<String, Boolean>();
        for (String rubrique : multiMapRubriqueBalise.keySet()) {
            List<XmlFieldElement> listBaliseDeLaRubrique = (List<XmlFieldElement>) multiMapRubriqueBalise.get(rubrique);
            mapRubriqueBourage.put(rubrique, false);
            if (isBourrageObligatoire(context, listBaliseDeLaRubrique)) {
                LOGGER_TECH.debug("le bourrage est obligatoire pour cette rubrique rubrique{}", rubrique);
                mapRubriqueBourage.put(rubrique, true);
            }
        }
        return mapRubriqueBourage;
    }

    /**
     * VÃ©rifie si le bourrage est obligatoire.
     * 
     * @param context
     *            contexte d'Ã©valuation
     * @param listBaliseDeLaRubrique
     *            la liste des balise
     * @return true si le bourrage est obligatoire
     */
    private boolean isBourrageObligatoire(final StandardEvaluationContext context, final List<XmlFieldElement> listBaliseDeLaRubrique) {

        for (XmlFieldElement xmlFieldElement : listBaliseDeLaRubrique) {
            Object value = xmlFieldElement.getValue(context);
            if (value != null && !(value instanceof Boolean) && !(StringUtils.isEmpty(value.toString())) && !(StringUtils.equals(value.toString(), "[]"))) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get le xml metadata.
     * 
     * @return le xml metadata
     */
    public FormMetadata getXmlMetadata() {
        return xmlMetadata;
    }

    /**
     * Set le xml metadata.
     * 
     * @param xmlMetadata
     *            le nouveau xml metadata
     */
    public void setXmlMetadata(final FormMetadata xmlMetadata) {
        this.xmlMetadata = xmlMetadata;
    }

}
