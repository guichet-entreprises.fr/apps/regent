package fr.ge.common.regent.core.form;

import java.util.regex.Pattern;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;

import fr.ge.common.regent.xml.FormMetadata;
import fr.ge.common.regent.xml.SimpleElement;
import fr.ge.common.regent.xml.expression.AutorisationsStaticReplace;
import fr.ge.common.regent.xml.expression.BeansExternesStaticReplace;
import fr.ge.common.regent.xml.expression.ChampStaticReplace;
import fr.ge.common.regent.xml.expression.CourantStaticReplace;
import fr.ge.common.regent.xml.expression.FormaliteStaticReplace;
import fr.ge.common.regent.xml.expression.ParentReplace;
import fr.ge.common.regent.xml.expression.ProfilStaticReplace;
import fr.ge.common.regent.xml.util.ExpressionParserUtil;

/**
 * Représente un élément d'un formulaire.
 * <p>
 * Ajoute les attributs nécessaire modèlisation d'un élément :
 * <ul>
 * <li>Identifiant</li>
 * <li>groupe d'appartenance</li>
 * <li>visibilité statique</li>
 * </ul>
 * 
 * @author Nicolas Richeton
 * 
 * 
 */
public class SimpleFormElement extends SimpleElement {

    /** La constante A_STRING_BEAN_REF_STATIC. */
    private static final String A_STRING_BEAN_REF_STATIC = "formalite.$1";

    /** La constante A_STRING_PROFIL_BEAN_REF_STATIC. */
    private static final String A_STRING_PROFIL_BEAN_REF_STATIC = "profil.$1";

    /** La constante A_STRING_CURRENT_BEAN_REF. */
    private static final String A_STRING_CURRENT_BEAN_REF = "courant\\[([A-Za-z0-9_\\.]+)\\]";

    /** La constante A_STRING_CURRENT_BEAN_REF_STATIC. */
    private static final String A_STRING_CURRENT_BEAN_REF_STATIC = "courant.$1";

    /** La constante A_STRING_FORM_REF. */
    private static final String A_STRING_FORM_REF = "champ\\[([A-Za-z0-9_\\.]+)\\]";

    /** La constante ASTRING_BEAN_REF. */
    private static final String ASTRING_BEAN_REF = "formalite\\[([A-Za-z0-9_\\.]+)\\]";

    /** La constante ASTRING_PROFIL_BEAN_REF. */
    private static final String ASTRING_PROFIL_BEAN_REF = "profil\\[([A-Za-z0-9_\\.]+)\\]";

    /** La constante ASTRING_PROFIL_BEAN_REF_WITH_FLOW. */
    private static final String ASTRING_PROFIL_BEAN_REF_WITH_FLOW = "(profil)\\[([A-Za-z0-9_\\.]+)\\]";

    /**
     * formalite\\[([A-Za-z0-9_\\.]+)\\]
     */
    protected static final Pattern BEAN_REF = Pattern.compile(ASTRING_BEAN_REF);

    /**
     * formalite.$1
     */
    protected static final String BEAN_REF_STATIC = A_STRING_BEAN_REF_STATIC;

    /**
     * profil\\[([A-Za-z0-9_\\.]+)\\]
     */
    protected static final Pattern PROFIL_BEAN_REF = Pattern.compile(ASTRING_PROFIL_BEAN_REF);

    /**
     * (profil)\\[([A-Za-z0-9_\\.]+)\\]
     */
    protected static final Pattern PROFIL_BEAN_REF_WITH_FLOW = Pattern.compile(ASTRING_PROFIL_BEAN_REF_WITH_FLOW);

    /**
     * profil.$1
     */
    protected static final String PROFIL_BEAN_REF_STATIC = A_STRING_PROFIL_BEAN_REF_STATIC;

    /**
     * courant\\[([A-Za-z0-9_\\.]+)\\]
     */
    protected static final Pattern CURRENT_BEAN_REF = Pattern.compile(A_STRING_CURRENT_BEAN_REF);

    /**
     * courant.$1
     */
    protected static final String CURRENT_BEAN_REF_STATIC = A_STRING_CURRENT_BEAN_REF_STATIC;

    /**
     * champ\\[([A-Za-z0-9_\\.]+)\\]
     */
    protected static final Pattern FORM_REF = Pattern.compile(A_STRING_FORM_REF);

    /** Le visibility. */
    private Expression visibility;

    /**
     * Constructeur de classe. Il parse la visibiliter pour en faire une
     * expression interpretable par SPEL
     * 
     * @param parent
     *            les méta données du formulaire contenant l'élément
     * @param id
     *            identifiant de l'élément
     * @param group
     *            groupe d'appartenance de l'éléments
     * @param visibility
     *            expression d'affichage statique de l'élément
     */
    public SimpleFormElement(FormMetadata parent, String id, String group, String visibility) {
        super(parent, id, group);

        // Preparer la visibilité
        if (visibility != null) {
            this.setVisibility(ExpressionParserUtil.parser(id, group, parent.getParser(), visibility, performStaticBeansReplacements(visibility)));
        }
    }

    /**
     * Remplace formalite[], profil[], parent[], champ[] et courant[].
     * 
     * @param expression
     *            expression a parser
     * @return l'expression traité
     */
    protected static String performStaticBeansReplacements(String expression) {
        String result = expression;
        result = new FormaliteStaticReplace().perform(result); // formalite[]
        result = new ProfilStaticReplace().perform(result); // profil[]
        result = new ParentReplace().perform(result); // parent[]
        result = new ChampStaticReplace().perform(result); // champ[]
        result = new CourantStaticReplace().perform(result); // courant[]
        result = new BeansExternesStaticReplace().perform(result); // cfe[]
                                                                   // ailleurs
                                                                   // que dans
                                                                   // le dialgue
                                                                   // cfe
        result = new AutorisationsStaticReplace().perform(result);
        // GROS FIX ME CAS DES LISTES DE CHECKBOX
        if (expression.contains("champ")) {
            result = result.replaceAll("length", "size()");
        }

        return result;
    }

    /**
     * Getter de l'attribut visibility.
     * 
     * @return la valeur de visibility
     */
    public Expression getVisibility() {
        return this.visibility;
    }

    /**
     * Vérifie si le champ doit être affiché lors de la génération de la page.
     *
     * @param c
     *            le c
     * @return true, si c'est visible
     */
    @SuppressWarnings("boxing")
    public boolean isVisible(EvaluationContext c) {
        if (this.getVisibility() == null) {
            return true;
        }

        return this.getVisibility().getValue(c, Boolean.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    /**
     * Set le visibility.
     *
     * @param visibility
     *            the visibility to set
     */
    public void setVisibility(Expression visibility) {
        this.visibility = visibility;
    }

}
