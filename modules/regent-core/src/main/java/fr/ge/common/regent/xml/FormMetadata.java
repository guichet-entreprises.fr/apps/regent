package fr.ge.common.regent.xml;

import java.io.IOException;
import java.util.List;

import org.springframework.expression.common.TemplateAwareExpressionParser;

import fr.ge.common.regent.core.form.DynamicVisibleElement;
import fr.ge.common.regent.core.form.ValidationElement;
import fr.ge.common.regent.core.input.InputSource;

/**
 * Modélisation des formulaires.
 * <p>
 * Les implémentations de cette classe permettent de charger la modélisation de
 * champs de formulaires, (champs, listes de valeurs, règles de validations,
 * règle de navigation ...) depuis différentes sources et de les restituer à la
 * demande.
 * <p>
 * Il est possible de déclarer des champs manuellement (un par un) via les
 * fonction addXXXElement, ou de les charger par bloc via des implémentations de
 * {@link InputSource}.
 * <p>
 * Les implémentations de cette classe doivent être thread-safe.
 * <p>
 * Pour une utilisation standard :
 * 
 * <pre>
 * FormMetadata metadata = new FormMetadataImpl();
 * metadata.setInputSources( ...);
 * metadata.init();
 * SimpleElement el = metadata.getElement( group, id);
 * 
 * //Si type plus évolué qu'un simple SimpleElement (champ, element visible, ...)
 * XXXElement xxxel = (XXXElement) el;
 * xxxel.getXXX()
 * </pre>
 * 
 * @author Nicolas Richeton
 * 
 */
public interface FormMetadata {

    /**
     * Ajoute un élément générique. Peut être utilisé pour les règles de
     * validation ou les élément visible.
     * 
     * @param id
     *            identifiant de l'élément
     * @param group
     *            groupe d'appartenance de l'élément
     * @param visibility
     *            expression de visibilité static de l'élément
     */
    void addElement(String id, String group, String visibility);

    /**
     * Ajoute un élément de type input.
     *
     * @param id
     *            l'id du champ
     * @param group
     *            le groupe parent du champ. Par ex, un champ appartenant à la
     *            page "Entreprise" aura pour groupe l'identifiant de la
     *            <code>Page</code> la représentant.
     * @param label
     *            le libellé d'un champ
     * @param aide
     *            aide relative à un champ
     * @param visibility
     *            le visibility
     * @param dynamicVisibility
     *            visibilité dynamique d'un champ
     * @param type
     *            le type du champ. Il doit correspondre au type supporté par
     *            FormMetadata dans <code>InputType</code>
     * @param typeParam
     *            paramètre du ty du champ
     * @param mandatory
     *            indique si le champ est obligatoire ou non
     * @param defaultValueDisplay
     *            valeur par défaut à l'affichage
     * @param defaultValueDB
     *            valeur par défaut à enregistrer en base
     * @param values
     *            liste, s'il y en a, les valeurs possible à la selection d'un
     *            champ pour les radio bouton, checkbox, liste déroulante...
     * @param expressionListeFonction
     *            expression de la liste renvoyée en apellant une fonction
     * @param validations
     *            liste, s'il y en a, les éléments modèlisant les conditions de
     *            validation de l'entrée d'un champ
     */
    void addInputElement(String id, String group, String label, String aide, String visibility, String dynamicVisibility, String type, List<String> typeParam, String mandatory,
            String defaultValueDisplay, String defaultValueDB, List<DynamicVisibleElement> values, String expressionListeFonction, List<ValidationElement> validations);

    /**
     * Ajoute un élément de type input.
     *
     * @param id
     *            l'id du champ
     * @param group
     *            le groupe parent du champ. Par ex, un champ appartenant à la
     *            page "Entreprise" aura pour groupe l'identifiant de la
     *            <code>Page</code> la représentant.
     * @param label
     *            le libellé d'un champ
     * @param aide
     *            aide relative à un champ
     * @param visibility
     *            le visibility
     * @param dynamicVisibility
     *            visibilité dynamique d'un champ
     * @param type
     *            le type du champ. Il doit correspondre au type supporté par
     *            FormMetadata dans <code>InputType</code>
     * @param typeParam
     *            paramètre du ty du champ
     * @param mandatory
     *            indique si le champ est obligatoire ou non
     * @param defaultValueDisplay
     *            valeur par défaut à l'affichage
     * @param defaultValueDB
     *            valeur par défaut à enregistrer en base
     * @param values
     *            liste, s'il y en a, les valeurs possible à la selection d'un
     *            champ pour les radio bouton, checkbox, liste déroulante...
     * @param expressionListeFonction
     *            expression de la liste renvoyée en apellant une fonction
     * @param validations
     *            liste, s'il y en a, les éléments modèlisant les conditions de
     *            validation de l'entrée d'un champ
     * @param lienPopup
     *            l'adresse vers laquelle il faut rediriger
     */
    void addPopupInputElement(String id, String group, String label, String aide, String visibility, String dynamicVisibility, String type, List<String> typeParam, String mandatory,
            String defaultValueDisplay, String defaultValueDB, List<DynamicVisibleElement> values, String expressionListeFonction, List<ValidationElement> validations, String lienPopup);

    /**
     * Ajoute un élément de type input, qui sera rafraichit en ajax.
     *
     * @param id
     *            identifiant de l'élément
     * @param group
     *            groupe d'appartenance de l'éléments. Par ex, un champ
     *            appartenant à la page "Entreprise" aura pour groupe
     *            l'identifiant de la <code>Page</code> la représentant
     * @param label
     *            libéllé à afficher de l'élément
     * @param aide
     *            aide relative à l'élément
     * @param visibility
     *            expression d'affichage statique de l'élément
     * @param dynamicVisibility
     *            expression d'affichage dynamique de l'élément
     * @param type
     *            le format du champ. Il doit correspondre au type supporté par
     *            FormMetadata dans <code>InputType</code>
     * @param typeParam
     *            paramètre du format
     * @param mandatory
     *            indique si le champ est obligatoire ou non
     * @param defaultValueDisplay
     *            expression de la valeur par defaut à l'affichage du champ
     * @param defaultValueDB
     *            expression de la valeur du champ s'il n'est pas affiché ou
     *            laissé vide
     * @param valuesEl
     *            le values el
     * @param validations
     *            les validations à effectuer sur le champ
     */
    void addAjaxInputElement(String id, String group, String label, String aide, String visibility, String dynamicVisibility, String type, List<String> typeParam, String mandatory,
            String defaultValueDisplay, String defaultValueDB, String valuesEl, List<ValidationElement> validations);

    /**
     * Ajoute le ajax input element.
     *
     * @param id
     *            le id
     * @param group
     *            le group
     * @param label
     *            le label
     * @param aide
     *            le aide
     * @param visibility
     *            le visibility
     * @param dynamicVisibility
     *            le dynamic visibility
     * @param type
     *            le type
     * @param typeParam
     *            le type param
     * @param mandatory
     *            le mandatory
     * @param defaultValueDisplay
     *            le default value display
     * @param defaultValueDB
     *            le default value db
     * @param valuesEl
     *            le values el
     * @param validations
     *            le validations
     */
    void addAjaxInputElement(String id, String group, String label, String aide, String visibility, String dynamicVisibility, String type, List<String> typeParam, String mandatory,
            String defaultValueDisplay, String defaultValueDB, List<DynamicVisibleElement> valuesEl, List<ValidationElement> validations);

    /**
     * Ajoute un élément de résolution de navigation.
     *
     * @param id
     *            identifiant de la page
     * @param group
     *            représente la hierarchie d'écrans séparés par des point Ex
     *            pour AyantDroit1, group=dirigeant.declarationSociale -->
     *            l'écran ayantDroit1 est dans l'arborescence
     *            dirigeant/declarationSociale
     * @param inputGroup
     *            l'identifiant du groupe des champs de la page. Cette valeur
     *            peut être null
     * @param label
     *            titre de la page
     * @param aide
     *            aide relative à une page toute entière
     * @param visibility
     *            condition staitque d'affichage de la page
     * @param occurenceAnnounced
     *            expression du nombre d'occurences(appartition) prévus pour cet
     *            écran
     * @param occurenceLimits
     *            nombre d'occurence possible maximum (non utilisé)
     * @param courant
     *            nom du bean dans lequel les infos de la page doivent être lues
     * @param initialisations
     *            expression des actions à effectuer à l'initialisation de la
     *            page
     * @param idFlow
     *            identifiant flow courant
     * @param preEnregistrements
     *            expression des actions à effectuer avant l'enregistrement de
     *            la page
     */
    void addPage(String id, String group, String inputGroup, String label, String aide, String visibility, String occurenceAnnounced, String occurenceLimits, String courant, String initialisations,
            String idFlow, String preEnregistrements);

    /**
     * Ajoute un élément de validation.
     * 
     * @param id
     *            identifiant de l'élément. (correspond au nom de la validation
     *            a effectuer dans <code>ValidationElement</code>)
     * @param group
     *            groupe d'appartenance de l'éléments. Par ex, un champ
     *            appartenant à la page "Entreprise" aura pour groupe
     *            l'identifiant de la <code>Page</code> la représentant
     * @param param
     *            paramètre pour la validation
     * @param visibility
     *            expression de la visibilité statique de la validation
     */
    void addValidationElement(String id, String group, String param, String visibility);

    /**
     * Ajoute un élément visible. Peut être utilisé pour les valeurs de listes.
     * 
     * @param id
     *            identifiant de l'élément
     * @param group
     *            groupe d'appartenance de l'éléments. Par ex, un champ
     *            appartenant à la page "Entreprise" aura pour groupe
     *            l'identifiant de la <code>Page</code> la représentant
     * @param label
     *            libéllé de l'élément
     * @param aide
     *            aide relative à l'élément
     * @param visibility
     *            visibilité statique de l'élément
     * @param dynamicVisibility
     *            visibilité dynamique de l'élément
     */
    void addVisibleElement(String id, String group, String label, String aide, String visibility, String dynamicVisibility);

    /**
     * Ajoute un élément champ PDF. Peut être utilisé pour definir la valeur
     * d'un champ PDF
     * 
     * @param id
     *            identifiant de l'élément
     * @param group
     *            nom du pdf
     * @param valeur
     *            expression de la valeur du champ
     */
    void addFieldElement(String id, String group, String valeur);

    /**
     * Ajoute un élément d'un champ xml ,Peut être utilisé pour definir la
     * valeur d'un champ xml.
     *
     * @param id
     *            identifiant de l'élément
     * @param group
     *            nom de l'xml générer
     * @param valeur
     *            expression de la valeur du champ
     * @param path
     *            chemain de la balise xml
     * @param occurence
     *            nombre d'occurence de la balise
     * @param rubrique
     *            la rubrique de la balise
     * @param obligatoire
     *            la balise est obligatoire (oui ou non)
     * @param bourrage
     *            le bourrage
     * @param endPath
     *            indique s'il s'agit d'une balise ou d'une rubrique
     * @param version
     *            version
     */
    void addXmlFieldElement(String id, String group, String valeur, String path, Integer occurence, String rubrique, Boolean obligatoire, String bourrage, boolean endPath, String version);

    /**
     * Récupère un élément pour un identifiant et d'un groupe.
     * 
     * @param group
     *            groupe d'appartenance de l'éléments. Par ex, un champ
     *            appartenant à la page "Entreprise" aura pour groupe
     *            l'identifiant de la <code>Page</code> la représentant
     * @param id
     *            identifiant de l'élément
     * @return l'élément correspondant
     */
    SimpleElement getElement(String group, String id);

    /**
     * Recupere la page dans le fichier de navigation correspondant à un écran.
     * 
     * @param className
     *            le nom classe, permet de filtrer su FormMetaDataDem pour ne
     *            récupérer que des éléments de type Page
     * @param id
     *            identifiant de l'écran (attribut identifiant dans les csv de
     *            navigation)
     * @return la liste de pages
     */
    List<SimpleElement> getElementOfType(String className, String id);

    /**
     * Retourne l'ensemble des éléments d'un groupe donné.
     * <p>
     * Permet de lister l'ensemble des champs d'une page ou de lister toutes les
     * valeurs d'une liste.
     *
     * @param valuesGroup
     *            le values group
     * @return le elements by group
     */
    List<SimpleElement> getElementsByGroup(String valuesGroup);

    /**
     * Retourne l'ensemble des éléments d'un groupe dans un flow.
     * 
     * @param valuesGroup
     *            ecran. nom du bean parent dans l'arborescence des beans
     * @param idFlow
     *            identifiant unique du flow
     * @return liste des éléments
     */
    List<SimpleElement> getElementsByGroupAndFlow(String valuesGroup, String idFlow);

    /**
     * retourne la liste des xmlFieldElements par Rubrique.
     *
     * @param valuesGroup
     *            le values group
     * @param rubrique
     *            le rubrique
     * @return le elements by group and rubrique
     */
    List<SimpleElement> getElementsByGroupAndRubrique(String valuesGroup, String rubrique);

    /**
     * retourne la liste des xmlFieldElements par Rubrique Avec occurence.
     *
     * @param valuesGroup
     *            le values group
     * @param rubrique
     *            le rubrique
     * @param version
     *            version
     * @return le elements by group and start with rubrique
     */
    List<SimpleElement> getElementsByGroupAndStartWithRubrique(String valuesGroup, String rubrique, String version);

    /**
     * Getter de l'attribut javascriptTemplate.
     * 
     * @return la valeur de javascriptTemplate
     */
    String getJavascriptTemplate();

    /**
     * Retourne l'implémentation du parser Spring EL utilisée.
     * <p>
     * Cette méthode permet aux implémentations d'element de formulaires
     * d'utiliser le même parser pour des raisons d'optimisation. En dehors de
     * ce cas, cette méthode ne devrait pas être utilisée.
     * 
     * 
     * @return le parser Sprint EL utilisé.
     */
    TemplateAwareExpressionParser getParser();

    /**
     * Charge les metadonnées des formulaires depuis l'ensemble des sources
     * définies en inputsource.
     *
     * @throws IOException
     *             Signale l'apparition d'une I/O exception.
     * @see #setInputSources(InputSource...)
     */
    void init() throws IOException;

    /**
     * Définit les sources de données à utiliser pour charger les métadonnées
     * des formulaires.
     * <p>
     * Par exemple : une source de donnée par fichier CSV, une par table
     * provenant de la base de données, etc.
     * 
     * @param source
     *            liste des sources à utiliser.
     */
    void setInputSources(InputSource... source);

    /**
     * Template Javascript à utiliser pour la référence aux valeurs des champs
     * input dans la page courante.
     * <p>
     * Le nom du champ sera inséré à la place des caractères <em>$1</em>. Permet
     * d'adapter le code généré au framework JS utilisé.
     * <p>
     * Cette fonction est à utiliser avant de charger les métadonnées via
     * {@link FormMetadata#init()}, car la génération du code javascript est
     * effectuée une seule fois à l'initialistation, pour optimiser les
     * performances.
     * 
     * @param jsTemplate
     *            Code javascript à utiliser.
     */
    void setJavascriptTemplate(String jsTemplate);

}
